<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
            <link rel="stylesheet" type="text/css" href="css\orangeEn.css"/> 
            <script type="text/javascript" src="css/passStrength.js"></script>
      <title>ForgetPassword</title>
   
    </head>
    <body><h:form>
        
         <center>
 

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/forget-final_04.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			<table  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="139" height="19" align="left" >

            <h:outputLabel rendered="#{ForgetPass_Bean.renderGetPass}"  value="User name : *"/>    
            <h:outputLabel rendered="#{ForgetPass_Bean.renderConfrim}" value="Activation Code:"/>
            </td>

            <td width="86"  align="left">
            <h:inputText rendered="#{ForgetPass_Bean.renderConfrim}" value="#{ForgetPass_Bean.activeCode}"/>
              <h:inputText rendered="#{ForgetPass_Bean.renderGetPass}" id="userNameId" value="#{ForgetPass_Bean.userName}">
              <f:converter converterId="forgertValidator"/>
              
              </h:inputText>
              </td>
	<td align="left">
        
        <h:message rendered="#{ForgetPass_Bean.renderGetPass}" for="userNameId" style="color:red;" />        </td>
          </tr>
          <!--<tr>
            <td height="19" align="left">

            <h:outputLabel rendered="#{ForgetPass_Bean.renderGetPass}"  value="POS Code : *"/>            </td>

            <td align="left">
              <h:inputText rendered="#{ForgetPass_Bean.renderGetPass}" id="pos_ID"  value="#{ForgetPass_Bean.posCode}">            
              <f:converter converterId="forgertValidator"/>
              
              </h:inputText>
              </td>
            <td align="left"><h:message rendered="#{ForgetPass_Bean.renderGetPass}" style="color:red;" for="pos_ID" /></td>
          </tr>
          <tr>
            <td height="19" align="left">

            <h:outputLabel rendered="#{ForgetPass_Bean.renderGetPass}"  value="Phone number : *"/>            </td>

            <td align="left">
              <h:inputText  rendered="#{ForgetPass_Bean.renderGetPass}" id="phoneID"  value="#{ForgetPass_Bean.phoneNumber}">            
              <f:converter converterId="forgertValidator"/>
                            
              </h:inputText>
              </td>
            <td align="left"><h:message rendered="#{ForgetPass_Bean.renderGetPass}" style="color:red;" for="phoneID" /></td>
          </tr>
          <tr>
            <td height="19" align="left" >

            <h:outputLabel rendered="#{ForgetPass_Bean.renderGetPass}" value="E-mail : *"/>            </td>

            <td align="left">
              <h:inputText id="emailId" rendered="#{ForgetPass_Bean.renderGetPass}"  value="#{ForgetPass_Bean.email}">            
              <f:converter converterId="forgertValidator"/>
              <f:converter converterId="mailValidator"/>
              <f:attribute name="email" value="email"/>
              </h:inputText>
              </td>
	 <td align="left">
         <h:message rendered="#{ForgetPass_Bean.renderGetPass}" for="emailId" style="color:red;"/>
         </td>
          </tr>-->
          <tr>
            <td height="19" align="left">

            <h:outputLabel rendered="#{ForgetPass_Bean.renderNewPass}"  value="Password:"/>            </td>

            <td align="left">
              <h:inputSecret  rendered="#{ForgetPass_Bean.renderNewPass}" id="passwordId"  value="#{ForgetPass_Bean.password}"/>            
               </td>
            <td align="left"></td>
          </tr>
          <tr>
            <td height="19" align="left">

            <h:outputLabel rendered="#{ForgetPass_Bean.renderNewPass}"  value="Confirm Password:"/>            </td>

            <td align="left">
              <h:inputSecret  rendered="#{ForgetPass_Bean.renderNewPass}" id="confirmPassId"  value="#{ForgetPass_Bean.confirmPassword}"/>            
              
              </td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td height="32" align="left" >
            </td>
		    <td ><h:outputLabel value="#{ForgetPass_Bean.msgWarn}"/> </td>
		    <td align="left">&nbsp;</td>
            </tr>
			</table>
			<table width="241" height="66">
         
         <tr>
          <td>
            <t:commandLink rendered="#{ForgetPass_Bean.renderGetPass}" action="#{ForgetPass_Bean.sendPassword}">
            <t:graphicImage border="0" url="images/Buttons/getPassword.gif"/>
            </t:commandLink>
            <t:commandLink rendered="#{ForgetPass_Bean.renderConfrim}" action="#{ForgetPass_Bean.confirmedAction}">
            <t:graphicImage border="0" url="images/Buttons/Activate.gif"/>
            </t:commandLink>
            <t:commandLink rendered="#{ForgetPass_Bean.renderNewPass}" action="#{ForgetPass_Bean.reloginAction}">
            <t:graphicImage border="0" url="images/Buttons/backRelogin.gif"/>
            </t:commandLink>
            </td>
			
			
			<td >           
			
			<h:outputLink  style="text-decoration: none;" value="Login.jsp">
              <t:graphicImage border="0"  url="images/Buttons/back.gif"/>
            </h:outputLink></td>
			
			<td>&nbsp; </td>
            </tr>
            <tr>
            <td>
            <h:outputLabel value="#{ForgetPass_Bean.msgWarn1}"/>
            </td>
            </tr>
        </table>
			
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>




    </center>
      </h:form></body>
  </html>
</f:view>

 