<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
      <title>AdminForgetPassword</title>
      <link rel="stylesheet" type="text/css" href="../css/Login.css"/> 
      <script type="text/javascript" src="../css/showReportss.js"></script>
      <script type="text/javascript" src="../css/passStrength.js"></script>
   
    </head>
    <body> <center>
     <h:form id="AdminForgetId">
   
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Retrieve Password</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			&nbsp;
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		  <h:outputLabel value="#{AdminForgetPassword_Bean.emailConfirmLabel}" />
                <h:inputText value="#{AdminForgetPassword_Bean.inputValue}"/><br><br>
                <h:commandButton rendered="#{AdminForgetPassword_Bean.renderGetPassButton}" value="Get Password" action="#{AdminForgetPassword_Bean.adminConfirmation_Action}"/><br>
                <h:commandButton rendered="#{AdminForgetPassword_Bean.renderConfirmButton}" value="Confirm" action="#{AdminForgetPassword_Bean.confirmedAction}"/><br>
                <h:outputLabel value="#{AdminForgetPassword_Bean.msgWarn}"/><br><br>
                
                <table>
                <tr>
                <td align="left">
                <h:outputLabel value="Password" rendered="#{AdminForgetPassword_Bean.renderNewPass}"/></td><td>
                <h:inputSecret id="newPasswordId"  value="#{AdminForgetPassword_Bean.password}" rendered="#{AdminForgetPassword_Bean.renderNewPass}" onkeyup="testPassword(document.getElementById('AdminForgetId:newPasswordId').value,document.getElementById('AdminForgetId:verdict'))" /><h:inputText rendered="#{AdminForgetPassword_Bean.renderNewPass}" size="10" readonly="true" id="verdict"/>
                
                </td></tr>
                <tr><td>
                <h:outputLabel value="Confirm Password" rendered="#{AdminForgetPassword_Bean.renderNewPass}"/></td><td><h:inputSecret value="#{AdminForgetPassword_Bean.confirmPassword}" rendered="#{AdminForgetPassword_Bean.renderNewPass}"/>
                </td>
                </tr>
                
                </table>
                
                <h:commandButton value="Relogin" rendered="#{AdminForgetPassword_Bean.renderRelogin}" action="#{AdminForgetPassword_Bean.reloginAction}"/><br>
                <h:outputLabel value="#{AdminForgetPassword_Bean.msgWarn1}"/>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>

    
    
    
    
    
    
    
    
    
      </h:form></center></body>
  </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AdminForgetPassword_Bean--%>