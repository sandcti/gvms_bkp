<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>User</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
      <script type="text/javascript" src="../../css/selectallcheckbox.js"></script>
   <script type="text/javascript">
  function CallPrint(strid)
{
var prtContent = document.getElementById(strid);
var WinPrint =
window.open('','','left=0,top=0,width=1,height=1,t oolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();
prtContent.innerHTML=strOldOne;
}
  </script>

   
    <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
   
  </head>
  <body>
    <center>
     
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;">Manage Users</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
        <h:form id="userFormID">
       <h:outputLabel value="Search for"/> &nbsp; 
       <h:selectOneMenu rendered="#{User_Bean.menuRender}" style="width:25%" value="#{User_Bean.userFiledSelected}">
            <f:selectItem  itemLabel="*" itemValue="0"/>            
            <f:selectItem  itemLabel="Name" itemValue="1"/>
            <f:selectItem itemLabel="Email" itemValue="4"/>
            <f:selectItem itemLabel="Phone" itemValue="5"/>            
           </h:selectOneMenu> 
           
           <h:selectOneMenu rendered="#{!User_Bean.menuRender}" style="width:25%" value="#{User_Bean.userFiledSelected}">
            <f:selectItem  itemLabel="*" itemValue="0"/>
            <f:selectItem  itemLabel="Name" itemValue="1"/>
            <f:selectItem  itemLabel="Type" itemValue="2"/>
            <f:selectItem  itemLabel="POS Code" itemValue="3"/>
            <f:selectItem itemLabel="Email" itemValue="4"/>
            <f:selectItem itemLabel="Phone" itemValue="5"/>            
           </h:selectOneMenu>
           
           
           &nbsp;  
           
           <h:inputText value="#{User_Bean.valueForSearch}"/>
           <br>
           <h:commandButton value="Search" action="#{User_Bean.search}"/><br>
           
           
       
         <h:commandLink value="Export to CSV" action="#{User_Bean.exportToCSV}"
                        style="color:rgb(0,0,255);"/><br>
       
        <h:dataTable width="100%" rowClasses="row1,row2" border="1" 
                     rows="5" value="#{User_Bean.userArray}"
                     var="Dtvar" binding="#{User_Bean.dataTable1}"
                     style="text-align:center; vertical-align:middle;"
                     id="dataTable1">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.User.AdminClass>-->
         <h:column rendered="#{User_Bean.adminCheck}">
         <f:facet name="header">
           <h:panelGroup>
			<h:selectBooleanCheckbox id="mainCheckBox" onclick="checkAll('userFormID:dataTable1',':list')"/>
            
           </h:panelGroup>
          </f:facet>
          <h:selectBooleanCheckbox id="list" value="#{Dtvar.selected}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getName" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.name}"/>
         </h:column>
         <h:column rendered="#{User_Bean.dtUserRender}">
          <f:facet name="header">
          <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getUserType" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Type"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.userType}"/>
         </h:column>
         <h:column rendered="#{User_Bean.dtUserRender}">
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getPos" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="POS"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.pos}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getEmail" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Email"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.email}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getPhone" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Phone"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.phone}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getStatus" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Status"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.status}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{User_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getUserLocked" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Lock"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.userLocked}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText value="Edit"/>
          </f:facet>
          <h:commandButton value="Edit" action="#{User_Bean.edit_Action}"/>
         </h:column>
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{User_Bean.pageFirst}"
                            disabled="#{User_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{User_Bean.pagePrevious}"
                            disabled="#{User_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{User_Bean.pageNext}"
                            disabled="#{User_Bean.dataTable1.first + User_Bean.dataTable1.rows                         >= User_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{User_Bean.pageLast}"
                            disabled="#{User_Bean.dataTable1.first + User_Bean.dataTable1.rows                         >= User_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>
       
        <h:inputHidden value="#{User_Bean.sortField}" />
        <h:inputHidden value="#{User_Bean.sortAscending}" />
        <h:commandButton value="#{User_Bean.newUser}"
                         action="#{User_Bean.newUser_Action}"/>
        <h:commandButton rendered="#{User_Bean.adminCheck}" value="Remove Selected User"
                         action="#{User_Bean.getSelectedItems}"/>
        <h:commandButton value="Back" action="#{User_Bean.back_Action}"/>
        </h:form>
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
     
     
     
     
     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:User_Bean--%>