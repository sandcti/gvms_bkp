<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>AddUser</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/>
   <script type="text/javascript" src="../../css/showReportss.js"></script>
   
  
  </head>
  <body>
    <center>
    
         
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> User</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
		
		<h:form>
        <table width="540">
         <tr>
          <td align="right" width="55%">
           <h:outputLabel value="System User Type"/>
          </td>
          <td width="45%">
           <div align="left">
            <h:selectOneMenu value="#{AddUser_Bean.systemUserSelected}"
                             id="selectOneMenuSystemUser">
             <f:selectItem itemLabel="Admin" itemValue="admin"
                           id="selectItem3"/>
             <f:selectItem itemLabel="POS" itemValue="user" id="selectItem4"/>
             <f:selectItem itemLabel="Customer Support" itemValue="cs" id="selectItem5"/>
             <f:selectItem itemLabel="Reporter" itemValue="reporter" id="selectItem6"/>
            </h:selectOneMenu>
           </div>
          </td>
         </tr>
         <tr>
          <td align="center" width="55%" colspan="2">
           <h:commandButton value="View" action="#{AddUser_Bean.view_Action}" id="addUser_cmd"/>
          </td>
         </tr>
        </table>
        </h:form>
        
        
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


     
     
     
     
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AddUser_Bean--%>