<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>Add Customer Support</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
   <script type="text/javascript" src="../../css/passStrength.js"></script>
  
  </head>
  <body>
    <center>
     
     
<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Save CS</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		<h:form binding="#{AddCS_Bean.form1}" id="CSFormId">
        <table width="83%" height="232">
         <tr>
          <td width="29%" align="left">User Name*</td>
          <td width="71%" align="left">                  <h:inputText style="width:50%" value="#{AddCS_Bean.userNameInput}"
                        id="userNameInput"/>                </td>
         </tr>
         <tr>
          <td width="29%" align="left">
           </td>
          <td width="71%" align="left">                       </td>
         </tr>
         <tr>
          <td width="29%" align="left"><h:outputText value="#{AddCS_Bean.passLabel}"/></td>
          <td width="71%" align="left">                  <h:inputSecret style="width:50%" onkeyup="testPassword(document.getElementById('CSFormId:passwordInput').value,document.getElementById('CSFormId:verdict'))" value="#{AddCS_Bean.passwordInput}"
                          id="passwordInput"/>  <h:inputText size="6" readonly="true" id="verdict"/>              </td>
         </tr>
         <tr>
          <td width="29%" align="left">Confirm <h:outputText value="#{AddCS_Bean.passLabel}"/></td>
          <td width="71%" align="left">                  <h:inputSecret style="width:50%" value="#{AddCS_Bean.confirmPasswordInput}"
                          id="confirmPasswordInput"/>                </td>
         </tr>
         <tr>
          <td width="29%" align="left">Full Name</td>
          <td width="71%" align="left">                  <h:inputText style="width:50%" value="#{AddCS_Bean.fullNameInput}"
                        id="fullNameInput"/>                </td>
         </tr>
         <tr>
          <td width="29%" align="left">User Email</td>
          <td width="71%" align="left">                  <h:inputText style="width:50%" value="#{AddCS_Bean.emailInput}"
                        id="emailInput"/>                </td>
         </tr>
         <tr>
          <td width="29%" align="left">Phone</td>
          <td width="71%" align="left">                  <h:inputText style="width:50%" value="#{AddCS_Bean.phoneInput}"
                        id="phoneInput"/>                </td>
         </tr>        
         
         <tr>
          <td width="29%" align="left">User Status</td>
          <td width="71%" align="left">
           <h:selectOneMenu style="width:50%" value="#{AddCS_Bean.userStatusSelected}"
                            rendered="true">             
                    <f:selectItem  itemLabel="--Select One--" itemValue="0"/>
            <f:selectItem itemLabel="ACTIVE" itemValue="1"/>
            <f:selectItem itemLabel="IN ACTIVE" itemValue="2"/>
                </h:selectOneMenu></td>
         </tr>
         <tr>
          <td width="29%" align="left">Lock</td>
          <td width="71%" align="left">
           <h:selectOneMenu style="width:50%" value="#{AddCS_Bean.userLockSelected}">
            <f:selectItem  itemLabel="--Select One--" itemValue="0"/>
            <f:selectItem itemLabel="Locked" itemValue="#{AddCS_Bean.maximumLockNum}"/>
            <f:selectItem itemLabel="Not Locked" itemValue="0"/>
           </h:selectOneMenu>         </td>
         </tr>
         <tr>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td align="left"><h:commandButton value="Add" rendered="#{AddCS_Bean.renderAdd}"
                         binding="#{AddCS_Bean.addUser_cmd}"
                         id="addUser_cmd"
                         action="#{AddCS_Bean.AddUser}"/>
        <h:commandButton value="Update"
                         rendered="#{AddCS_Bean.renderUpdate}"
                         action="#{AddCS_Bean.update_Action}"/></td>
           <td align="left"><h:commandButton value="Back"
                         action="#{AddCS_Bean.back_Action}"/></td>
         </tr>
        </table>
        
        
        <h:outputLabel value="#{AddCS_Bean.msgWarn}" id="msgWarn"/>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AddCS_Bean--%>