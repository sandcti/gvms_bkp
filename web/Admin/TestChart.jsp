<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp" prefix="c" %>	
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
      <title>TestChart</title>
    </head>
    <body><h:form>
        
                 
      <c:chart id="chart1" binding="#{TestChart_Bean.chart1}" datasource="#{TestChart_Bean.pieDataset}" type="pie" is3d="true" antialias="true" title="Summary Report" xlabel="X Label" ylabel="Y Label" height="300" width="400"></c:chart>
      </h:form>
    
    </body>
  </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:TestChart_Bean--%>