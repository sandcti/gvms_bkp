<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8"/>
      <title>AdminUnredeem</title>
      <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
   <script type="text/javascript" src="../../css/showReportss.js"></script>
   <script language="javascript">
	function onlyNumbers(){
		
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			event.returnValue = false;
		}
		}
	</script>
    <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  
    </head>
    <body>
    <center>
    
     
     
     
     
     
     
     
     
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Voucher Settings</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
		 <h:form>
      <table>
      <tr><td><t:outputText value="Dial Number * "/></td><td><t:inputText  maxlength="12" onkeydown="onlyNumbers();" value="#{AdminUnredeem_Bean.dialNumber}"/></td></tr>
      <tr><td><t:outputText value="Voucher Number * "/></td><td><t:inputText  maxlength="15" onkeydown="onlyNumbers();" value="#{AdminUnredeem_Bean.voucherNumber}"/></td></tr>
      <tr><td align="center"" colspan="2">* Indicates to mandatory fields.</td></tr>
      </table>
      
      <br>
      
        
      
        <h:commandButton value="Unredeem"  onclick="if (!confirm('Do you want to unredeem this voucher?')) return false" action="#{AdminUnredeem_Bean.unredeem_Action}" />&nbsp;        
        <h:commandButton value="Delete"  onclick="if (!confirm('Do you want to delete this voucher?')) return false" action="#{AdminUnredeem_Bean.delete_Action}" />
        
        <br>
        <h:outputLabel style="color:red;" value="#{AdminUnredeem_Bean.warnMsg}"/>
        </h:form>
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>

     
     
     
     
     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Gift_Bean--%>
