<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>Gift</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
   <script type="text/javascript" src="../../css/showReportss.js"></script>
      <script type="text/javascript" src="../../css/selectallcheckbox.js"></script>
    <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  
  </head>
  <body>
    <center>
     

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Gift</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		 <h:form binding="#{Gift_Bean.form1}" id="form1">
        <h:dataTable rowClasses="row1,row2" value="#{Gift_Bean.giftArray}" var="Dtvar"
                     binding="#{Gift_Bean.dataTable1}" id="dataTable1"
                     border="1" style="text-align:center; vertical-align:middle;"
                     rows="#{Gift_Bean.rowCount}">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.GiftClass>-->
         <h:column>
         <f:facet name="header">
           <h:panelGroup>
			<h:selectBooleanCheckbox id="mainCheckBox" onclick="checkAll('form1:dataTable1',':list')"/>
            
           </h:panelGroup>
          </f:facet>
          <h:selectBooleanCheckbox id="list" value="#{Dtvar.selected}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{Gift_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftName" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{Gift_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftType" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Type"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftType}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{Gift_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftValue" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Value"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftValue}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{Gift_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftStatus" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Status"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftStatus}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{Gift_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getCreation" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Creation Date"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.creation}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText value="DESCRIPTION"/>
          </f:facet>
          <h:outputText value="#{Dtvar.giftDescription}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:outputText value="Edit"/>
          </f:facet>
          <h:commandButton value="Edit" action="#{Gift_Bean.editGift}"/>
         </h:column>
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{Gift_Bean.pageFirst}"
                            disabled="#{Gift_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{Gift_Bean.pagePrevious}"
                            disabled="#{Gift_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{Gift_Bean.pageNext}"
                            disabled="#{Gift_Bean.dataTable1.first + Gift_Bean.dataTable1.rows                         >= Gift_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{Gift_Bean.pageLast}"
                            disabled="#{Gift_Bean.dataTable1.first + Gift_Bean.dataTable1.rows                         >= Gift_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>
        <h:inputHidden value="#{Gift_Bean.sortField}" />
        <h:inputHidden value="#{Gift_Bean.sortAscending}" />
        <h:commandButton value="Create new gift" id="createGift"
                         action="#{Gift_Bean.newGift}"/>
        <h:commandButton value="Remove Selected Gift"
                         action="#{Gift_Bean.getSelectedItems}"/>
                         <br>                         
       <h:outputLabel value="#{Gift_Bean.msgWarn}"/>
                         </h:form>
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>

     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Gift_Bean--%>