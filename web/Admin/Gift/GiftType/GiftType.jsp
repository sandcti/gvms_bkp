<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>GiftType</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/>
   <script type="text/javascript" src="../../../css/showReportss.js"></script>
      <script type="text/javascript" src="../../css/selectallcheckbox.js"></script>
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  
  </head>
  <body>
    <center>
    
    
    
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Gift Type</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
	<h:form id="giftTypeFormId">
        <h:dataTable width="100%" rowClasses="row1,row2"
                     value="#{GiftType_Bean.giftTypeArray}" var="Dtvar"
                     binding="#{GiftType_Bean.dataTable1}" id="dataTable1"
                     style="text-align:center; vertical-align:middle;"
                     border="1" rows="#{GiftType_Bean.rowCount}">
                     
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.GiftTypeClass>-->
         <h:column>
         <f:facet name="header">
           <h:panelGroup>
            <h:selectBooleanCheckbox id="mainCheckBox" onclick="checkAll('giftTypeFormId:dataTable1',':list')"/>
            
           </h:panelGroup>
          </f:facet>
          <h:selectBooleanCheckbox id="list" value="#{Dtvar.selected}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{GiftType_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftTypeName" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftTypeName}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{GiftType_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftTypeStatus" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Status"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftTypeStatus}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:outputText value="DESCRIPTION"/>
          </f:facet>
          <h:outputText value="#{Dtvar.giftTypeDescription}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:outputText value="Edit"/>
          </f:facet>
          <h:commandButton value="Edit" action="#{GiftType_Bean.editGiftType}"/>
         </h:column>
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{GiftType_Bean.pageFirst}"
                            disabled="#{GiftType_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{GiftType_Bean.pagePrevious}"
                            disabled="#{GiftType_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{GiftType_Bean.pageNext}"
                            disabled="#{GiftType_Bean.dataTable1.first + GiftType_Bean.dataTable1.rows>= GiftType_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{GiftType_Bean.pageLast}"
                            disabled="#{GiftType_Bean.dataTable1.first + GiftType_Bean.dataTable1.rows>= GiftType_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>
        <h:inputHidden value="#{GiftType_Bean.sortField}" />
        <h:inputHidden value="#{GiftType_Bean.sortAscending}" />
        <h:commandButton value="Create new GiftType" id="createGiftType"
                         action="#{GiftType_Bean.newGiftType}"/>
        <h:commandButton value="Remove Selected Gift Type"
                         action="#{GiftType_Bean.getSelectedItems}"/><br>
        <h:outputLabel value="#{GiftType_Bean.msgWarn}"/>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:GiftType_Bean--%>