<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>CreateGiftType</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
   
   
  </head>
  <body>
    <center>
    
    
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> New Gift Type</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
	 <h:form binding="#{CreateGiftType_Bean.form1}" id="form1">
        <table width="68%" height="75">
         <tr>
          <td width="16%" align="left" >Name*</td>
          <td width="84%" align="left">
           <h:inputText style="width:50%" binding="#{CreateGiftType_Bean.giftTypeNameInput}"
                        id="giftTypeNameInput"/>          </td>
         </tr>
         <tr>
          <td width="16%" align="left">Description</td>
          <td width="84%" align="left">
           <h:inputTextarea style="width:50%" binding="#{CreateGiftType_Bean.giftTypeDesTexArea}"
                            id="giftTypeDesTexArea"/>          </td>
         </tr>
         <tr>
          <td width="16%" align="left">Status*</td>
          <td width="84%" align="left">
           <t:selectOneMenu style="width:50%" value="#{CreateGiftType_Bean.status}">
            <f:selectItem  itemLabel="--Select One--" itemValue="0"/>
            <f:selectItem itemLabel="ACTIVE" itemValue="1"/>
            <f:selectItem itemLabel="IN ACTIVE" itemValue="2"/>
           </t:selectOneMenu>             </td>
         </tr>
         <tr>
           <td align="left">&nbsp;</td>
           <td align="left">&nbsp;</td>
         </tr>
         <tr>
           <td align="left"><h:commandButton value="Add "
                          binding="#{CreateGiftType_Bean.addGT_cmd}"
                          id="addGT_cmd"
                          action="#{CreateGiftType_Bean.AddGiftType}"/></td>
           <td align="left"><h:commandButton value="Back"
                          action="#{CreateGiftType_Bean.back_Action}"
                          id="commandButton1"/></td>
         </tr>
        </table>
        <p>
         
          
         <h:outputLabel binding="#{CreateGiftType_Bean.msgWarn}" id="msgWarn"/>
          
         
        </p>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:CreateGiftType_Bean--%>