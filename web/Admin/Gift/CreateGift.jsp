<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1256"/>
   <title>CreateGift</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
   
  
  </head>
  <body>
    <center>
     
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> New Gift </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
		<h:form binding="#{CreateGift_Bean.form1}" id="form1">
        <table width="73%" height="163">
         <tr>
          <td width="26%" align="left">Gift name*</td>
          <td width="74%" align="left" >
           <h:inputText style="width:50%" binding="#{CreateGift_Bean.giftNameInput}"
                        id="giftNameInput"/>          </td>
         </tr>
         <tr>
          <td width="26%" align="left">Gift name (Arabic)*</td>
          <td width="74%" align="left" >
           <h:inputText style="width:50%" binding="#{CreateGift_Bean.giftNameArabicInput}"
                        id="giftNameArabicInput"/>          </td>
         </tr>
         <tr>
          <td width="26%" align="left">Gift Value*</td>
          <td width="74%" align="left">
           <h:inputText style="width:50%" binding="#{CreateGift_Bean.giftValueInput}"
                        id="giftValueInput"/>          </td>
         </tr>
         <tr>
          <td width="26%" align="left">Gift Discription</td>
          <td width="74%" align="left">
           <h:inputTextarea style="width:50%" binding="#{CreateGift_Bean.inputTextarea1}"
                            id="inputTextarea1"/>          </td>
         </tr>
         <tr>
          <td width="26%" align="left">Gift Type</td>
          <td width="74%" align="left">
           <h:selectOneMenu style="width:50%" value="#{CreateGift_Bean.giftTypeSelected}">
            <f:selectItems value="#{CreateGift_Bean.giftTypeItem}"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
          <td width="26%" align="left">Gift Status</td>
          <td width="74%" align="left">
           <h:selectOneMenu style="width:50%" value="#{CreateGift_Bean.giftStatusSelected}">
            <f:selectItem  itemLabel="--Select One--" itemValue="0"/>
            <f:selectItem itemLabel="ACTIVE" itemValue="1"/>
            <f:selectItem itemLabel="IN ACTIVE" itemValue="2"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
           <td align="left">&nbsp;</td>
           <td align="left">&nbsp;</td>
         </tr>
         <tr>
           <td align="left"><h:commandButton value="Create" action="#{CreateGift_Bean.createGift}"/></td>
           <td align="left"><h:commandButton value="Back" binding="#{CreateGift_Bean.backToGift}"
                         id="backToGift"
                         action="#{CreateGift_Bean.back_Action}"/></td>
         </tr>
        </table>


        <h:outputLabel binding="#{CreateGift_Bean.msgWarn}" id="msgWarn"/>
        </h:form>
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


     
     
     
     
     
     
     
     
     
     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:CreateGift_Bean--%>