<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <title>Generating New Password</title>
      <link rel="stylesheet" type="text/css" href="../css/Login.css"/> 
      <script type="text/javascript" src="../css/showReportss.js"></script>
      <script type="text/javascript" src="../css/passStrength.js"></script>
   <style type="text/css">
   a {
   
  TEXT-DECORATION: NONE;
  color: white; 
  font-weight: noraml ;
  font-size: 16px;
   
  
   }
   </style>
    </head>
   <body><h:form id="AdminForgetId">
    <center>
        
        
        
        
        
        
<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Generate New Password</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			&nbsp;
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		<table cellspacing="3" cellpadding="2" width="64%" height="118">
         <tr>
          <td width="44%">Password</td>
          <td width="56%">
           <h:inputSecret binding="#{genNewPass_Bean.passwordId}"
                          id="passwordId" value="#{genNewPass_Bean.password}"/>
          </td>
         </tr>
         <tr>
          <td width="44%">Confirm Password</td>
          <td width="56%">
           <h:inputSecret binding="#{genNewPass_Bean.confirmPasswordId}"
                          id="confirmPasswordId"
                          value="#{genNewPass_Bean.confirmPassword}"/>
          </td>
         </tr>
         <tr align="center">
          <td>
           <h:commandButton value="Submit new password" action="#{genNewPass_Bean.genNewPassAction}"
                            binding="#{genNewPass_Bean.commandButton1}"
                            id="commandButton1"/>
          </td>
          <td>
           <h:commandButton value="Back"
                            binding="#{genNewPass_Bean.commandButton2}"
                            id="commandButton2"
                            action="#{genNewPass_Bean.backAction}"/>
          </td>
         </tr>
         <tr align="center">
          <td colspan="2">
           <h:outputLabel
                          binding="#{genNewPass_Bean.outputLabel1}"
                          id="outputLabel1" value="#{genNewPass_Bean.msgWarn}"/>
          </td>
         </tr>
        </table>
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>

    
    
        
        
        
        
        
        
        
        </center>
      </h:form></body>
  </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:genNewPass_Bean--%>