<%@page import="java.util.HashMap"%>
<%@page import="com.mobinil.gvms.system.admin.ws.model.SystemServerModel"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>New Server</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/>


  </head>
  <body>
    <center>




<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> New Server</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">

			   <table width="100%">


         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>

            </td>
          </tr>

        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">


		<!-- cut from here -->
	 <h:form id="newServerform">
        <table width="70%" height="71">
         <tr>
          <td width="28%" align="left">
           <h:outputText value="User Name*" id="outputTextUserName"/>          </td>
          <td width="72%" align="left">
           <h:inputText style="width:50%" binding="#{WSManagement_Bean.userName}"
                         id="userNameInputText"/>          </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
         </tr>
         <tr>
          <td align="left">
           <h:outputText value="Password" id="outputText3"/>          </td>
          <td align="left">
           <h:inputText style="width:50%" binding="#{WSManagement_Bean.password}"                            
                            id="passwordInputText"/>          </td>
         </tr>
         <tr>
           <td align="left"><h:outputText value="Campaign*"/></td>
           <td align="left">
            <t:selectOneMenu style="width:50%"  value="#{WSManagement_Bean.campId}">
               <f:selectItems value="#{WSManagement_Bean.campaignList}" />
           </t:selectOneMenu>     </td>
         </tr>
         <tr>
           <td align="left"><h:commandButton value="Create"
                         id="createButton"
                         action="#{WSManagement_Bean.newServer}"/></td>
           <td align="left">  <h:commandButton value="Back"
                         id="backBtn"
                         action="#{WSManagement_Bean.back_Action}"/></td>
         </tr>
        </table>

        <h:outputLabel binding="#{WSManagement_Bean.msgWarn}" id="msgWarn"/>
      </h:form>

       <!-- cut to here -->
		        </td>

</tr>


	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>














    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:EditGiftType_Bean--%>