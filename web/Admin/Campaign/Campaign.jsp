<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>Campaign</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/>
   <script type="text/javascript" src="../../css/showReportss.js"></script>
   <script type="text/javascript" src="../../css/campaignFilter.js"></script>
   <script type="text/javascript" src="../../css/selectallcheckbox.js"></script>
   		<script type="text/javascript" src="../../css/common.js"></script>
		<script type="text/javascript" src="../../css/css.js"></script>
		<script type="text/javascript" src="../../css/standardista-table-sorting.js"></script>
   
   
   <style type="text/css">
   a {
  color: #000000; 
  font-weight: noraml ;
  font-size: 16px;
   
  }
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    <center>
     
     
     
     
     

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 36px;"> Campaign</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
			
<h:form id="campaignForm">
       
      
       <table width="100%" height="71" border="0">
       <tr>
          <td width="10%" height="34" align="left" >       
       <h:outputLabel value="Filter"/>       </td>
       <td width="10%"  align="left">
        <h:selectOneMenu id="columnNameList"  onchange="javascript:switcid(this);"
                         value="#{Campaign_Bean.divValue}">
         <f:selectItem itemLabel="All" itemValue="*"/>
         <f:selectItem itemLabel="Campaign Name" itemValue="x_2"/>
         <f:selectItem itemLabel="Creation Date" itemValue="x_0"/>
         <f:selectItem itemLabel="Start Date" itemValue="x_3"/>
         <f:selectItem itemLabel="End Date" itemValue="x_4"/>
         <f:selectItem itemLabel="Voucher Type" itemValue="x_5"/>
         <f:selectItem itemLabel="Gift" itemValue="x_6"/>
         <f:selectItem itemLabel="Status" itemValue="x_7"/>
         <f:selectItem itemLabel="User" itemValue="x_1"/>
        </h:selectOneMenu>        </td>
        
        <td width="80%" align="left"><h:commandButton action="#{Campaign_Bean.filter_Action}"
                         value="Filter"/></td>
        </tr>
        
        <tr>
        <td colspan="3">
        <div id="x_0" style="display:none;">
         From &nbsp;<t:inputDate value="#{Campaign_Bean.fromDate}" id="fromId"
                                 type="date" popupCalendar="true" ampm="true"/>
          &nbsp;&nbsp;&nbsp; To &nbsp;<t:inputDate value="#{Campaign_Bean.toDate}"
                                                   id="toId" type="date"
                                                   popupCalendar="true"
                                                   ampm="true"/>
        </div>
        
        <div id="x_1" style="display:none;">
         From &nbsp;<h:inputText value="#{Campaign_Bean.inputFromInt}"/>
          &nbsp;&nbsp;&nbsp; To &nbsp;<h:inputText value="#{Campaign_Bean.inputToInt}"/>
        </div>
        
        <div id="x_2" style="display:none;">
         <h:inputText value="#{Campaign_Bean.inputString}"/>
        </div></td>
        </tr>
        </table>
        <h:dataTable styleClass="sortable"  rowClasses="row1,row2" 
                     value="#{Campaign_Bean.currCampaign}" var="Dtvar"
                     binding="#{Campaign_Bean.dataTable1}" id="dataTable1"
                     rows="10" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
         <h:column>
          <f:facet name="header">
           <h:panelGroup>
            &nbsp;<h:selectBooleanCheckbox id="mainCheckBox" onclick="checkAll('campaignForm:dataTable1',':list')"/>
           </h:panelGroup>
          </f:facet>
          <h:selectBooleanCheckbox id="list" value="#{Dtvar.selected}"/>
         </h:column>
       
         <h:column>
          <f:facet name="header">           
            <h:outputText value="Campaign Name"/>           
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.campaignName}"/>
         </h:column>
         <h:column>
          <f:facet name="header">

            <h:outputText value="Creation Date"/>

          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.creationDate}"/>
         </h:column>
         <h:column>
<f:facet name="header">

            <h:outputText value="Start Date"/>

          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.startDate}"/>
         </h:column>
         <t:column style="nowrap();">
          <f:facet name="header">


            <h:outputText value="Closed Date"/>

          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.endDate}"/>
         </t:column>
         <h:column>
          <f:facet name="header">

				<h:outputText value="Gift"/>


          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         <h:column>
          <f:facet name="header">

            <h:outputText value="Voucher Type"/>

          </f:facet>
          <h:outputText value="#{Dtvar.voucherTypeName}"/>
         </h:column>
         <h:column>
          <f:facet name="header">


            <h:outputText value="Status"/>

          </f:facet>
          <h:outputText style="#{Dtvar.campaignColor} "
                        value="#{Dtvar.campaignStatus}"/>
         </h:column>
         <h:column>
          <f:facet name="header">

            <h:outputText value="Customers"/>

          </f:facet>
          
           <h:commandLink action="#{Campaign_Bean.getVoucherFile}" value="#{Dtvar.campaignUsers}"/>
          
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText style="color: black; text-decoration:none;cursor:default;" title="" value="Edit"/>
          </f:facet>
          <h:commandButton  value="Edit" rendered="#{!Dtvar.isClosed}" action="#{Campaign_Bean.Edit_cmd}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText style="color: black; text-decoration:none;cursor:default;" title="" value="Dial/Voucher List"/>
          </f:facet>
          <h:commandButton value="Upload" rendered="#{Dtvar.renderUpload}"
                           action="#{Campaign_Bean.upload_voucherList}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText style="color: black; text-decoration:none;cursor:default;" title="" value="Additional Field"/>
          </f:facet>
          <h:commandButton value="Details" rendered="#{!Dtvar.isClosed}"
                           action="#{Campaign_Bean.addField_cmd}"/>
          <h:inputHidden value="#{Dtvar.campaignId}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:outputText style="color: black; text-decoration:none;cursor:default;" title="" value="Close"/>
          </f:facet>
          <h:commandButton value="Close" onclick="if (!confirm('Do you want to close this campaign?')) return false" rendered="#{!Dtvar.isClosed}"
                           action="#{Campaign_Bean.closeCampaign}"/>
          <h:inputHidden value="#{Dtvar.campaignId}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:outputText style="color: black; text-decoration:none;cursor:default;" title="" value="Invalid Vouchers"/>
          </f:facet>
          <h:commandButton  value="download" rendered="#{Dtvar.renderErrorFile}"
                           action="#{Campaign_Bean.downloadErrorFile}"/>
         </h:column>
         
         <f:facet name="footer">
          <h:panelGroup >
           <h:commandButton value="First" action="#{Campaign_Bean.pageFirst}"
                            disabled="#{Campaign_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{Campaign_Bean.pagePrevious}"
                            disabled="#{Campaign_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{Campaign_Bean.pageNext}"
                            disabled="#{Campaign_Bean.dataTable1.first + Campaign_Bean.dataTable1.rows>= Campaign_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{Campaign_Bean.pageLast}"
                            disabled="#{Campaign_Bean.dataTable1.first + Campaign_Bean.dataTable1.rows>= Campaign_Bean.dataTable1.rowCount}"/>
          <br />
		  <br />
		  <h:commandButton  value="Create New Campaign"
                         binding="#{Campaign_Bean.createCampaign}"
                         id="createCampaign"
                         action="#{Campaign_Bean.newCampaign}"/>
<h:commandButton value="Remove Selected Campaign"
                         action="#{Campaign_Bean.getSelectedItems}"/>
		  </h:panelGroup>
          
         </f:facet>
        </h:dataTable>
        <h:inputHidden value="#{Campaign_Bean.sortField}"/>
        <h:inputHidden value="#{Campaign_Bean.sortAscending}"/>
       

        
        <br>
        
        <h:outputLabel value="#{Campaign_Bean.msgWarn}"/> 
        </h:form>      
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


     
     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Campaign_Bean--%>