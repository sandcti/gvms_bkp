<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>EditAdditionalField</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/>
   
  </head>
  <body>
    <center>
    
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Edit Field </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		 <h:form>
        <table width="67%" height="140">
        <tr>
          <td width="31%" align="left">Arabic Field Name</td>
          <td width="69%" align="left">
           <h:inputText disabled="#{EditAdditionalField_Bean.disableTypeAndControl}" style="width:50%"
                        value="#{EditAdditionalField_Bean.fieldInfo[4]}"
                        id="arnameInputText"/>
          </td>
         </tr>
         <tr>
          <td width="31%" align="left">English Field Name</td>
          <td width="69%" align="left">
           <h:inputText disabled="#{EditAdditionalField_Bean.disableTypeAndControl}" style="width:50%"
                        value="#{EditAdditionalField_Bean.fieldInfo[0]}"
                        id="nameInputText"/>
          </td>
         </tr>
         <tr>
          <td width="31%" align="left">Type</td>
          <td width="69%" align="left">
           <h:selectOneMenu  disabled="#{EditAdditionalField_Bean.disableTypeAndControl}" style="width:35%"
                            value="#{EditAdditionalField_Bean.fieldInfo[1]}">
            <f:selectItem itemLabel="String" itemValue="String"/>
            <f:selectItem itemLabel="Integer" itemValue="Integer"/>
            <f:selectItem itemLabel="Date" itemValue="Date"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td width="31%" align="left">Order (Replace) </td>
          <td width="69%" align="left">
           <h:selectOneMenu style="width:35%"
                            value="#{EditAdditionalField_Bean.currentOrder}">
            <f:selectItems value="#{EditAdditionalField_Bean.fieldName}"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td width="31%" align="left">Unique</td>
          <td width="69%" align="left">
           <h:selectOneMenu style="width:35%"
                            value="#{EditAdditionalField_Bean.fieldInfo[2]}">
            <f:selectItem itemLabel="Yes" itemValue="Yes"/>
            <f:selectItem itemLabel="No" itemValue="No"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td width="31%" align="left">Mandatory</td>
          <td width="69%" align="left">
           <h:selectOneMenu style="width:35%"
                            value="#{EditAdditionalField_Bean.fieldInfo[3]}">
            <f:selectItem itemLabel="Yes" itemValue="Yes"/>
            <f:selectItem itemLabel="No" itemValue="No"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td width="31%"  align="left">Field Control</td>
          <td width="69%" align="left">
           <h:selectOneMenu disabled="#{EditAdditionalField_Bean.disableTypeAndControl}" style="width:35%"
                            value="#{EditAdditionalField_Bean.fieldsControl}">
            <f:selectItem itemLabel="List" itemValue="List"/>
            <f:selectItem itemLabel="One-Line" itemValue="One-Line"/>
            <f:selectItem itemLabel="Multi-Line" itemValue="Multi-Line"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
           <td width="31%" align="left">Status</td>
           <td width="69%" align="left">
           <h:selectOneMenu style="width:35%" value="#{EditAdditionalField_Bean.fieldInfo[5]}" 
                            id="selectOneMenu4">
            
            <f:selectItem itemLabel="ACTIVE" itemValue="1"/>
            <f:selectItem itemLabel="IN ACTIVE" itemValue="2"/>            
           </h:selectOneMenu>         </td>
         </tr>
         <tr>
          <td align="left">&nbsp;</td>
          <td align="left">&nbsp;</td>
         </tr>
         <tr>
          <td align="left">
           <h:commandButton value="Update"
                            binding="#{EditAdditionalField_Bean.update1}"
                            id="update1"
                            action="#{EditAdditionalField_Bean.update_Action}"/>
          </td>
          <td align="left">
           <h:commandButton value="Back"
                            action="#{EditAdditionalField_Bean.back_Action}"/>
          </td>
         </tr>
        </table>
        <h:outputLabel binding="#{EditAdditionalField_Bean.msgWarn}"
                       id="msgWarn"/>
                       </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:EditAdditionalField_Bean--%>