<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1256"/>
   <title>AddAdditionalField</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
   
  
   <script language="javascript">
   function defaultValues() 
    {

	   
	   
	   document.getElementById('newFieldForm:selectOneMenu1').remove(0);
	   document.getElementById('newFieldForm:selectOneMenu1').remove(0);
	   document.getElementById('newFieldForm:selectOneMenu1').remove(0);
	   document.getElementById('newFieldForm:selectOneMenu2').remove(0);
	   document.getElementById('newFieldForm:selectOneMenu2').remove(0);
	   
	   if(document.getElementById('newFieldForm:selectOneMenu0').selectedIndex ==0)
	   {	
		   
		   var elOptNewTypeString = document.createElement('option');
		   var elOptNewTypeInteger = document.createElement('option');
		   var elOptNewTypeDate = document.createElement('option');
		   var elOptNewTypeYes = document.createElement('option');
		   var elOptNewTypeNo = document.createElement('option');
		   
		   elOptNewTypeString.text = 'String';
		   elOptNewTypeString.value = 'String';
		   elOptNewTypeInteger.text = 'Integer';
			elOptNewTypeInteger.value = 'Integer';
			elOptNewTypeDate.text = 'Date';
			elOptNewTypeDate.value = 'Date';
			elOptNewTypeYes.text = 'Yes';
			elOptNewTypeYes.value = 'Yes';	
			elOptNewTypeNo.text = 'No';
			elOptNewTypeNo.value = 'No';

			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeString);	
			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeInteger);
			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeDate);
			document.getElementById('newFieldForm:selectOneMenu2').add(elOptNewTypeYes);
			document.getElementById('newFieldForm:selectOneMenu2').add(elOptNewTypeNo);
			document.getElementById('newFieldForm:selectOneMenu1').selectedIndex = 0;
			document.getElementById('newFieldForm:selectOneMenu2').selectedIndex = 0;
	   }
	   if(document.getElementById('newFieldForm:selectOneMenu0').selectedIndex ==1)
	   {
		   
		   var elOptNewTypeString = document.createElement('option');
		   var elOptNewTypeInteger = document.createElement('option');		   
		   var elOptNewTypeNo = document.createElement('option');
		   
		   elOptNewTypeString.text = 'String';
		   elOptNewTypeString.value = 'String';
		   elOptNewTypeInteger.text = 'Integer';
			elOptNewTypeInteger.value = 'Integer';				
			elOptNewTypeNo.text = 'No';
			elOptNewTypeNo.value = 'No';

			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeString);	
			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeInteger);			
			document.getElementById('newFieldForm:selectOneMenu2').add(elOptNewTypeNo);
			document.getElementById('newFieldForm:selectOneMenu1').selectedIndex = 0;
			document.getElementById('newFieldForm:selectOneMenu2').selectedIndex = 0;
		}   
	   if(document.getElementById('newFieldForm:selectOneMenu0').selectedIndex ==2)
	   {
		   
		   var elOptNewTypeString = document.createElement('option');		   
		   var elOptNewTypeYes = document.createElement('option');
		   var elOptNewTypeNo = document.createElement('option');
		   
		   elOptNewTypeString.text = 'String';
		   elOptNewTypeString.value = 'String';
			elOptNewTypeYes.text = 'Yes';
			elOptNewTypeYes.value = 'Yes';	
			elOptNewTypeNo.text = 'No';
			elOptNewTypeNo.value = 'No';

			document.getElementById('newFieldForm:selectOneMenu1').add(elOptNewTypeString);			
			document.getElementById('newFieldForm:selectOneMenu2').add(elOptNewTypeYes);
			document.getElementById('newFieldForm:selectOneMenu2').add(elOptNewTypeNo);
			
			document.getElementById('newFieldForm:selectOneMenu1').selectedIndex = 0;
			document.getElementById('newFieldForm:selectOneMenu2').selectedIndex = 0;
		}
    }

   </script>
  </head>
  <body onload="document.getElementById('newFieldForm:selectOneMenu4').selectedIndex = 1;">
    <center>
     
     
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> New Field </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		 <h:form id="newFieldForm">
        <table width="76%" height="219">
         
         <tr>
          <td width="38%" align="left">Field Name in Arabic</td>
          <td width="62%" align="left">
           <h:inputText binding="#{AddAdditionalField_Bean.arabicInput}"
                        id="arabicInput"/>          </td>
         </tr>
         <tr>
          <td width="38%" align="left">Field Name in English</td>
          <td width="62%" align="left">
           <h:inputText value="#{AddAdditionalField_Bean.englishInput}"
                        id="englishInput"/>          </td>
         </tr>

         <tr>
          <td width="38%" align="left">Field Control</td>
          <td width="62%" align="left">
           <h:selectOneMenu id="selectOneMenu0" onchange="defaultValues();" style=" width:35%" value="#{AddAdditionalField_Bean.fieldControl}">
            <f:selectItems value="#{AddAdditionalField_Bean.fieldControlArray}"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
		 <td width="38%" align="left">Field Type</td>
          <td width="62%" align="left">
           <h:selectOneMenu style=" width:35%" value="#{AddAdditionalField_Bean.type}"
                            id="selectOneMenu1">
            <f:selectItem itemLabel="String" itemValue="String"/>
            <f:selectItem itemLabel="Integer" itemValue="Integer"/>
            <f:selectItem itemLabel="Date" itemValue="Date"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
          <td width="38%" align="left">Unique</td>
          <td width="62%" align="left">
           <h:selectOneMenu style=" width:35%" value="#{AddAdditionalField_Bean.unique}"
                            id="selectOneMenu2">
            <f:selectItem itemLabel="Yes" itemValue="Yes"/>
            <f:selectItem itemLabel="No" itemValue="No"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
          <td width="38%" align="left">Mandatory</td>
          <td width="62%" align="left">
           <h:selectOneMenu style=" width:35%" value="#{AddAdditionalField_Bean.mandatory}"
                            id="selectOneMenu3">
            <f:selectItem itemLabel="Yes" itemValue="Yes"/>
            <f:selectItem itemLabel="No" itemValue="No"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
           <td width="38%" align="left">Status</td>
           <td width="62%" align="left">
           <h:selectOneMenu style=" width:35%" value="#{AddAdditionalField_Bean.fieldStatus}"
                            id="selectOneMenu4">
            <f:selectItem itemLabel="--Select One--" itemValue="0"/>
            <f:selectItem itemLabel="ACTIVE" itemValue="1"/>
            <f:selectItem itemLabel="IN ACTIVE" itemValue="2"/>            
           </h:selectOneMenu>         </td>
         </tr>
         <tr>
           <td align="center"><h:commandButton value="Add"
                         action="#{AddAdditionalField_Bean.addField_Action}"/>&nbsp;&nbsp;
        </td> <td align="left"><h:commandButton value="Back"
                         action="#{AddAdditionalField_Bean.back_Action}"/></td>
           </tr>
        </table>
        
        <h:outputText binding="#{AddAdditionalField_Bean.msgWarn}"
                      id="msgWarn"/>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
     
     
     
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AddAdditionalField_Bean--%>