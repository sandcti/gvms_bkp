<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>AdditionalField</title>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
   <script type="text/javascript" src="../../css/selectallcheckbox.js"></script>
   
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    <center>
     
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Manage Fields </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		<h:form binding="#{AdditionalField_Bean.form1}" id="form1">
       
       
      <h:outputLabel style="font-size:large;color:black;" value="Manage fields to #{AdditionalField_Bean.campaignName}"/>
      <br>
       
        <t:dataTable width="100%" rowClasses="row1,row2" value="#{AdditionalField_Bean.fieldArray}" var="Dtvar"
                     binding="#{AdditionalField_Bean.dataTable1}"
                     id="dataTable1" border="1" cellpadding="2" cellspacing="3"
                     style="text-align:center; vertical-align:middle;"
                     rows="#{AdditionalField_Bean.rowCount}" >
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.AdditionalFieldClass>-->
         <t:column >
         <f:facet name="header">
           <h:panelGroup>
            <h:selectBooleanCheckbox disabled="#{AdditionalField_Bean.disableDataTableCol}" id="mainCheckBox" onclick="checkAll('form1:dataTable1',':list')"/>
           </h:panelGroup>
          </f:facet>
          <h:selectBooleanCheckbox id="list" value="#{Dtvar.selected}" disabled="#{AdditionalField_Bean.disableDataTableCol}"/>
         </t:column>
         
         <t:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getEnglishName" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.englishName}" />
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getFieldType" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Type"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.fieldType}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getOrder" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Order"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.order}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getUnique" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Unique"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.unique}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getMandatory" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Mandatory"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.mandatory}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
          <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getTextArea" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Field Control"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.fieldControl}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
          <h:commandLink  actionListener="#{AdditionalField_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getStatus" />
           <h:outputText style="color:rgb(0,0,0); font-size:small;" value="Status"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.status}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:outputText value="Edit"/>
          </f:facet>
          <h:commandButton value="Edit"
                           action="#{AdditionalField_Bean.editAdd_Action}" disabled="#{!AdditionalField_Bean.activateRemoveFields}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:outputText value="List"/>
          </f:facet>
          <h:commandButton value="Update" rendered="#{Dtvar.renderUpdate}" disabled="#{AdditionalField_Bean.activateRemoveFields}"
                           action="#{AdditionalField_Bean.validationListUpdate}" />
          <h:outputLabel value="#{Dtvar.listCount}"
                         rendered="#{Dtvar.renderUpdate}"/>
         </t:column>
         <f:facet name="footer">
          <h:panelGroup rendered="#{AdditionalField_Bean.renderButtonNavigation}">
           <h:commandButton value="First"
                            action="#{AdditionalField_Bean.pageFirst}"
                            disabled="#{AdditionalField_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev"
                            action="#{AdditionalField_Bean.pagePrevious}"
                            disabled="#{AdditionalField_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next"
                            action="#{AdditionalField_Bean.pageNext}"
                            disabled="#{AdditionalField_Bean.dataTable1.first + AdditionalField_Bean.dataTable1.rows                         >= Campaign_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last"
                            action="#{AdditionalField_Bean.pageLast}"
                            disabled="#{AdditionalField_Bean.dataTable1.first + AdditionalField_Bean.dataTable1.rows                         >= Campaign_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </t:dataTable>
      
        
        
        <h:inputHidden value="#{AdditionalField_Bean.sortField}" />
        <h:inputHidden value="#{AdditionalField_Bean.sortAscending}" />
        
        <h:commandButton value="New Field" action="addField" disabled="#{AdditionalField_Bean.disableDataTableCol}"/>
        <h:commandButton  value="Remove Selected Fields"
                         action="#{AdditionalField_Bean.getSelectedItems}" disabled="#{!AdditionalField_Bean.activateRemoveFields}"/>
        <h:commandButton value="Back"
                         action="#{AdditionalField_Bean.back_Action}"/>
        <h:outputLabel value="#{AdditionalField_Bean.msgWarn}"/>
        <table>
        <tr>
        <td>
       <h:commandLink action="#{AdditionalField_Bean.exportHtmlTableAsText}">
       <h:outputText style="color:black;" value="Export to Text"/>       
       </h:commandLink>
        </td>
        </tr>
        </table>
        
        </h:form>
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
     
     
     
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AdditionalField_Bean--%>