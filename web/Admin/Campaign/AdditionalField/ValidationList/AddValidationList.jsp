<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>AddValidationList</title>
   
      <link rel="stylesheet" type="text/css" href="../../../../css/Login.css"/> 
   
  
  </head>
  <body>
    <center>
    
    
    
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Upload List </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="middle"  width="578" height="414">
		
		
		<!-- cut from here -->
		<h:form id="uploadForm" enctype="multipart/form-data">
     <h:panelGrid columns="3">
         <h:outputLabel for="file" value="Select file"/>
         <t:inputFileUpload required="true" id="file" value="#{myBean.uploadedFile}"
                            />
         <h:message for="file" style="color: red;"/>
         <h:panelGroup/>
         <h:commandButton value="Submit" action="#{myBean.submit}"/>          
         <br>
         <h:message for="uploadForm" infoStyle="color: green;"
                    errorStyle="color: red;"/>
        </h:panelGrid>
        <h:selectBooleanCheckbox value="#{myBean.validtyValue}"
                                 id="selectBooleanCheckbox1"/>
        <h:outputLabel value="List validity"/>
     </h:form>
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    
    
    
    
    
     
    </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:AddValidationList_Bean--%>