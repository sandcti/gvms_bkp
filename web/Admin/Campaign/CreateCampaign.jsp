<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>CreateCampaign</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/> 
  <script type="text/javascript" src="../../css/options.js"></script>
   <style type="text/css">
   a {
   
  TEXT-DECORATION: NONE;
  color: white; 
  font-weight: noraml ;
  font-size: 16px;
   
  
   }
   </style>
   
  <script language="javascript">
  function onlyNumbers(){
  	
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			event.returnValue = false;
		}
		}
  </script>
  </head>
  <body onload="createAllList();">
    <center>
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Save Campaign </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
		 <h:form id="NewCampaignForm" onsubmit="createAllList();">
        <table width="74%" height="211">
         <tr>
          <td align="left" nowrap="nowrap"><label>Campaign Name</label></td>
          <td align="left" >
           <h:inputText maxlength="20" disabled="#{CreateCampaign_Bean.disableExpiredCampaign}" style="width:50%"value="#{CreateCampaign_Bean.campaignName}"/>          </td>
         </tr>
             <tr>
         
          <td width="34%" align="left">
           <p><label>Start-Date</label></p>          </td>
          <td width="66%" align="left">
           <t:inputDate disabled="#{CreateCampaign_Bean.disableStartDate}"  value="#{CreateCampaign_Bean.startDate}"
                        id="startDateId" type="date" popupCalendar="true"
                        ampm="true"/>          </td>
         </tr> 
         
         <tr>
          <td width="34%" align="left">
           <p><label>Voucher Expire Days</label></p>          </td>
          <td width="66%" align="left">
          
          <h:inputText maxlength="3" onkeypress="return onlyNumbers()" disabled="#{CreateCampaign_Bean.disableExpiredCampaign}" style="width:50%" value="#{CreateCampaign_Bean.daysToExpire}"/>          </td>
                   
         </tr>      
                   
         <tr>
          <td width="34%" align="left"><label>Voucher Type</label></td>
          <td width="66%" align="left">
           <h:selectOneMenu disabled="#{CreateCampaign_Bean.disableExpiredCampaign}"  style="width:50%" value="#{CreateCampaign_Bean.VTValue}"
                            id="VoucherValueID">
            <f:selectItems value="#{CreateCampaign_Bean.VTArray}"
                           id="restVoucherValueID"/>
           </h:selectOneMenu>          </td>
         </tr>
         <tr>
          <td width="34%" height="21" align="left"><label>Campaign Description</label></td>
          <td height="21" width="66%" align="left">
                
		   
		   <t:inputTextarea disabled="#{CreateCampaign_Bean.disableExpiredCampaign}" wrap="true" style="width:50%" value="#{CreateCampaign_Bean.campaignDescription}"/>          
		   
		   </td>
         </tr>
         <tr>
          <td width="34%" align="left"><label>Gift</label> </td>
          <td width="66%" align="left">
            <table width="100%" border="0">
  <tr>
    <td width="43%" rowspan="4">
    <t:selectManyListbox disabled="#{CreateCampaign_Bean.disableEndDate}" value="#{CreateCampaign_Bean.giftValue}"  size="6" style="width:100pt;" 
                                id="availableOptionsGifts" >
            <f:selectItems value="#{CreateCampaign_Bean.giftLabels}"/>
           </t:selectManyListbox>     </td>
    <td width="20%"><h:commandButton value=">>>" onclick="createListGift();"  action="#{CreateCampaign_Bean.addAllGifts}" type="submit" />
    </td>
    <td width="37%" rowspan="4">
    
    <t:selectManyListbox disabled="#{CreateCampaign_Bean.disableEndDate}" value="#{CreateCampaign_Bean.giftSelected}" valueChangeListener="#{CreateCampaign_Bean.selectionChanged}"  size="6"   style="width:100pt;"  
                                id="selectedOptionsGifts">
               <f:selectItems  value="#{CreateCampaign_Bean.giftSelectedLabels}"/>
              </t:selectManyListbox>
            
            </td>
  </tr>
  <tr>
    <td><h:commandButton onclick="createListGift();" value=">"  action="#{CreateCampaign_Bean.addAttrGift}" type="submit" />
    
    
    </td>
    </tr>
  <tr>
    <td><h:commandButton value="<"  action="#{CreateCampaign_Bean.removeAttrGift}" type="submit" />
    </td>
    </tr>
  <tr>
    <td><h:commandButton value="<<<"  onclick="createListGift();" action="#{CreateCampaign_Bean.removeAllGifts}" type="submit" />
    </td>
    </tr>
</table>        </td>
         </tr>
         
         <tr>
          <td align="left"><label>User Type</label></td>
          <td align="left">
           
           
           <table width="100%" border="0">
  <tr>
    <td width="43%" rowspan="4">
    <t:selectManyListbox disabled="#{CreateCampaign_Bean.disableEndDate}" value="#{CreateCampaign_Bean.UTValue}"  size="6" style="width:100pt;" 
                                id="availableOptions" >
            <f:selectItems value="#{CreateCampaign_Bean.UTArray}"/>
           </t:selectManyListbox>     </td>
    <td width="20%"><h:commandButton value=">>>" onclick="createListObjects();"  action="#{CreateCampaign_Bean.addAll}" type="submit" />
    </td>
    <td width="37%" rowspan="4">
    
    <t:selectManyListbox disabled="#{CreateCampaign_Bean.disableEndDate}" value="#{CreateCampaign_Bean.UTSelectedValue}" valueChangeListener="#{CreateCampaign_Bean.selectionChanged}"  size="6"   style="width:100pt;"  
                                id="selectedOptions">
               <f:selectItems  value="#{CreateCampaign_Bean.UTSelectedArray}"/>
              </t:selectManyListbox>
            
            </td>
  </tr>
  <tr>
    <td><h:commandButton onclick="createListObjects();" value=">"  action="#{CreateCampaign_Bean.addAttr}" type="submit" />
    
    
    </td>
    </tr>
  <tr>
    <td><h:commandButton value="<"  action="#{CreateCampaign_Bean.removeAttr}" disabled="#{CreateCampaign_Bean.disableStartDate}" type="submit" />
    </td>
    </tr>
  <tr>
    <td><h:commandButton value="<<<"  onclick="createListObjects();" action="#{CreateCampaign_Bean.removeAll}" disabled="#{CreateCampaign_Bean.disableStartDate}" type="submit" />
    </td>
    </tr>
</table>

           
           </td>
         </tr>
         <tr>
           <td align="left">&nbsp;</td>
           <td align="left">&nbsp;</td>
         </tr>
         <tr>
           <td height="21" align="left">
       <h:commandButton  value="Create" action="#{CreateCampaign_Bean.create_Action}"  id="createCampaign"
                         rendered="#{CreateCampaign_Bean.renderCreate}"/>
        <h:commandButton value="Update" id="updateButton" action="#{CreateCampaign_Bean.update_cmd}"
                         onclick="#{CreateCampaign_Bean.javaScriptConfirmDeleteVouchers}" rendered="#{CreateCampaign_Bean.renderUpdate}"/>
			    </td>
           <td align="left"><h:commandButton value="Back" id="commandButton1" 
                         action="#{CreateCampaign_Bean.back_Action}"/></td>
         </tr>
        </table>
        
        
        <h:outputText escape="false" value="#{CreateCampaign_Bean.javaScriptAlert}"/>
        <h:outputLabel value="#{CreateCampaign_Bean.createLbl}"
                      id="createLbl"/><br>
                      <h:messages id="allerrors"/>
                      </h:form>
        
        
        
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


     
     
     
     
    </center>
   </body>
 </html>
</f:view>