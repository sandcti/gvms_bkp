<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <title>Voucher</title>
   <link rel="stylesheet" type="text/css" href="../../css/Login.css"/>
   <script type="text/javascript" src="../../css/showReportss.js"></script>
   <script type="text/javascript" src="../../css/campaignFilter.js"></script>
   <script type="text/javascript">


function textCounter(field,cntfield) {

   cntfield.value = field.value.length;
}


</script>

   
   
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    
       
       
        
        
        
        
        <center>
   
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Campaign Customers </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		
		
		
		<h:outputLabel value="Campagin #{Vouchers_Bean.campaignName} Customers"/>
       <h:form id="campaignForm">
       <table>
       <tr>
       <td align="center">
       
       <t:selectOneMenu value="#{Vouchers_Bean.selectedValueRedeemed}">
       <f:selectItem itemValue="*" itemLabel="--Select One--"/>
       <f:selectItem itemValue="1" itemLabel="Redeemed"/>
       <f:selectItem itemValue="2" itemLabel="Unredeemed"/>
       <f:selectItem itemValue="3" itemLabel="Expired"/>
       <f:selectItem itemValue="4" itemLabel="New"/>
       </t:selectOneMenu>
       <t:commandButton value="Filter" action="#{Vouchers_Bean.filter_Action}" />
       </td>
       <td>&nbsp;
              
       </td>
       </tr>
       <tr align="center">
       <td width="100%">       
       <t:commandLink rendered="#{Vouchers_Bean.renderDialCol}" action="#{Vouchers_Bean.ExportToSms}" style="color:blue;" value="Export to SMS Batch "  />
       </td>
      
       </tr>
       <tr>
       <td height="120" align="center">
       <t:inputTextarea rendered="#{Vouchers_Bean.renderDialCol}" onkeydown="textCounter(document.getElementById('campaignForm:message'),document.getElementById('campaignForm:textCountId'))"
        onkeyup="textCounter(document.getElementById('campaignForm:message'),document.getElementById('campaignForm:textCountId'))"  style="width:60%; height:100%;" wrap="true"  id="message" value="#{Vouchers_Bean.smsMessage}" binding="#{Vouchers_Bean.messageControl}"/> &nbsp; <h:inputText id="textCountId" value="0" style="width:20pt;" readonly="true" rendered="#{Vouchers_Bean.renderDialCol}"/> <br>
       <h:outputLabel value="#{Vouchers_Bean.msgWarn}"/>
       
       </td>
       <td>
       <t:selectOneMenu rendered="#{Vouchers_Bean.renderDialCol}" value="#{Vouchers_Bean.selectLang}">
       <f:selectItem itemLabel="Arabic" itemValue="ar"/>
       <f:selectItem itemLabel="English" itemValue="en"/>
       </t:selectOneMenu>
       </td>
       </tr>
       <tr>
       <td>
       <t:commandLink style="color:blue;" value="Export to CSV" action="#{Vouchers_Bean.ExportToCSV}" />
       </td>
       </tr>
       <tr>
       <td align="center">
       
       
         <t:dataTable rowClasses="row1,row2"
                     value="#{Vouchers_Bean.voucherArray}" var="Dtvar"
                     binding="#{Vouchers_Bean.dataTable1}" id="dataTable1"
                     rows="10" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
               
         <t:column rendered="#{Vouchers_Bean.renderDialCol}">
          <f:facet name="header">
           <h:commandLink actionListener="#{Vouchers_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getDial"/>
            <h:outputLabel style="color:rgb(0,0,0); font-size:small;"
                          value="Dial"/>
           </h:commandLink>
          </f:facet>
          <h:outputLabel value="#{Dtvar.dial}"/>
         </t:column>
         <t:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{Vouchers_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getVoucherNumber"/>
            <h:outputLabel style="color:rgb(0,0,0); font-size:small;"
                          value="Voucher Number"/>
           </h:commandLink>
          </f:facet>
          <h:outputLabel value="#{Dtvar.voucherNumber}"/>
         </t:column>
         
         <t:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{Vouchers_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getVoucherStatus"/>
            <h:outputLabel style="color:rgb(0,0,0); font-size:small;"
                          value="Voucher Status"/>
           </h:commandLink>
          </f:facet>
          <h:outputLabel value="#{Dtvar.voucherStatus}"/>
         </t:column>
         
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{Vouchers_Bean.pageFirst}"
                            disabled="#{Vouchers_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{Vouchers_Bean.pagePrevious}"
                            disabled="#{Vouchers_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{Vouchers_Bean.pageNext}"
                            disabled="#{Vouchers_Bean.dataTable1.first + Vouchers_Bean.dataTable1.rows>= Vouchers_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{Vouchers_Bean.pageLast}"
                            disabled="#{Vouchers_Bean.dataTable1.first + Vouchers_Bean.dataTable1.rows>= Vouchers_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </t:dataTable>
        </td>
        </tr>
        <tr>
        <td align="center">
        <h:commandButton value="Back" action="#{Vouchers_Bean.backAction}"/>
        </td>
        
        </tr>
        </table>        
        <h:inputHidden value="#{Vouchers_Bean.sortField}"/>
        <h:inputHidden value="#{Vouchers_Bean.sortAscending}"/>
        </h:form>   
        
        
        
        
        
        
        
        
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


     
     
     
     
     
     
    </center>
        
        
        
    
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Vouchers_Bean--%>