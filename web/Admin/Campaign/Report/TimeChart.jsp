<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp" prefix="c"%>
<%@page import="com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO"%>
<%
HttpSession sess= request.getSession(false);
LoginDTO ldto = (LoginDTO) sess.getValue("adminInfo");
        ldto = ldto == null ? (LoginDTO) sess.getValue("repInfo") : ldto;
        String type = ldto.getUserType();
        ////System.out.println("type iss "+type);

%>

<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/>
   <script type="text/javascript" src="../../../css/showReportss.js"></script>
   <title>Time Chart</title>
   
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    <center>
    
    
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Time Report </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
          <%if (type.compareTo("admin")==0){%>
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
         <%} else if (type.compareTo("reporter")==0){%>
         <%@ include file="/Admin/RepLefMenu.jsp"%>
         <%}%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		 <h:form id="posForm">
           <div align="center"> <h:outputLabel value="Time Chart"/></div>
        <table width="568" height="184">
         <tr>
          <td width="79">
           <h:outputLabel value="From  "/>
          </td>
          <td align="left" colspan="3">
           <t:inputDate value="#{CampaignReport_Bean.filterFromDate}"
                        id="inputDateFrom" type="date" popupCalendar="true"
                        ampm="true"/>
          </td>
		  </tr>
		  <tr>
          <td>
           <h:outputLabel value="To  "/>
          </td>
          <td align="left" colspan="3">
           <t:inputDate value="#{CampaignReport_Bean.filterToDate}"
                        id="inputDateTo" type="date" popupCalendar="true"
                        ampm="true"/>
           <br></br>
          </td>
         </tr>
         <tr>
          <td>
           <h:outputLabel value="Campaigns  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;" value="#{CampaignReport_Bean.selectedItemCampaignName}">
            <f:selectItems value="#{CampaignReport_Bean.campaignItems}"/>
           </h:selectOneMenu>
          </td>
          <td>
           <h:outputLabel value="Sales Channel  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;" value="#{CampaignReport_Bean.selectedItemUserTypeName}">
            <f:selectItems value="#{CampaignReport_Bean.userTypeItems}"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td>
           <h:outputLabel value="Gift  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;" value="#{CampaignReport_Bean.selectedItemGiftName}">
            <f:selectItems value="#{CampaignReport_Bean.giftItems}"/>
           </h:selectOneMenu>
          </td>
		   <td>
           <h:outputLabel 
                          value="Gift Type"/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;"
                            value="#{CampaignReport_Bean.selectedItemGiftTypeName}">
            <f:selectItems value="#{CampaignReport_Bean.giftTypeItems}"/>
           </h:selectOneMenu>
          </td>
          
         </tr>
         <tr>
          
          <td>
           <h:outputLabel value="Gift Options"/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;" value="#{CampaignReport_Bean.selectedItemGiftOption}">
            <f:selectItem itemLabel="Gift Count" itemValue="count"/>
            <f:selectItem itemLabel="Gift Value" itemValue="value"/>
           </h:selectOneMenu>
          </td>
		  <td>
           <h:outputLabel value="Time Options"/>
          </td>
          <td>
           <h:selectOneMenu style="width:70px;" value="#{CampaignReport_Bean.selectedItemTimeOption}">
            <f:selectItem itemLabel="Day" itemValue="day"/>
            <f:selectItem itemLabel="Week" itemValue="week"/>
            <f:selectItem itemLabel="Month" itemValue="month"/>
            <f:selectItem itemLabel="Year" itemValue="year"/>
           </h:selectOneMenu>
          </td>
         </tr>
         
         
         
         <tr>
          <td colspan="4" align="center">
           <h:commandButton value="Filter"
                            action="#{CampaignReport_Bean.timeChart_Action}"/>
          </td>
         </tr>
         
        </table>
        <table>
        <tr>
        <td>
        <c:chart id="chart1" rendered="#{CampaignReport_Bean.renderBarChart}"
                    datasource="#{CampaignReport_Bean.categoryDataset}" type="bar"
                    is3d="true" antialias="true" title="Campaign Report"
                    xlabel="Time" ylabel="Amount" height="300" width="600"></c:chart>
        </td>
        </tr>
        
        <tr>
        <td>
        <h:commandLink rendered="#{CampaignReport_Bean.renderDataTable}" value="Export to CSV" action="#{CampaignReport_Bean.exportToCSVVoucher}"
                        style="color:rgb(0,0,255);"/>
        </td>
        </tr>
        
         <tr>
          <td>
           <h:dataTable width="100%" rendered="#{CampaignReport_Bean.renderDataTable}" rowClasses="row1,row2"
                        value="#{CampaignReport_Bean.timeResult}" var="Dtvar"
                        binding="#{CampaignReport_Bean.dataTable4}"
                        id="timeChartTable" rows="100" border="1"
                        style="text-align:center; vertical-align:middle;">
            <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
            <h:column>
             <f:facet name="header">
              <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable3}">
               <f:attribute name="sortField" value="getDayOfWeekName"/>
               <h:outputText style="color:rgb(0,0,0); font-size:small;"
                             value="#{CampaignReport_Bean.timeTableName}"/>
              </h:commandLink>
             </f:facet>
             <h:outputText value="#{Dtvar.dayOfWeekName}"/>
            </h:column>
            <h:column>
             <f:facet name="header">
              <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable3}">
               <f:attribute name="sortField" value="getGiftValue"/>
               <h:outputText style="color:rgb(0,0,0); font-size:small;"
                             value="#{CampaignReport_Bean.timeTableValue}"/>
              </h:commandLink>
             </f:facet>
             <h:outputText value="#{Dtvar.giftValue}"/>
            </h:column>
            <f:facet name="footer">
             <h:panelGroup>
              <h:commandButton value="First"
                               action="#{CampaignReport_Bean.pageFirst}"
                               disabled="#{CampaignReport_Bean.dataTable4.first == 0}"/>
              <h:commandButton value="Prev"
                               action="#{CampaignReport_Bean.pagePrevious}"
                               disabled="#{CampaignReport_Bean.dataTable4.first == 0}"/>
              <h:commandButton value="Next"
                               action="#{CampaignReport_Bean.pageNext}"
                               disabled="#{CampaignReport_Bean.dataTable4.first + CampaignReport_Bean.dataTable4.rows>= CampaignReport_Bean.dataTable4.rowCount}"/>
              <h:commandButton value="Last"
                               action="#{CampaignReport_Bean.pageLast}"
                               disabled="#{CampaignReport_Bean.dataTable4.first + CampaignReport_Bean.dataTable4.rows>= CampaignReport_Bean.dataTable4.rowCount}"/>
             </h:panelGroup>
            </f:facet>
           </h:dataTable>
          </td>
         </tr>
        </table>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>