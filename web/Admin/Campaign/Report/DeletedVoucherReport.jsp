<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@page import="com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO"%>
<%
HttpSession sess= request.getSession(false);
LoginDTO ldto = (LoginDTO) sess.getValue("adminInfo");
        ldto = ldto == null ? (LoginDTO) sess.getValue("repInfo") : ldto;
        String type = ldto.getUserType();
        ////System.out.println("type iss "+type);

%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
            <script type="text/javascript" src="../../../css/showReportss.js"></script>
      <title>Deleted Voucher Report</title>
    
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
		
      </style>
      <script language="javascript">
    function pulldownChanged(){
        document.getElementById('posForm:myUpdateButton').click();
    }
    function pulldownChangedSubChannels(){
        document.getElementById('posForm:subChannelButton').click();
    }
    function onlyNumbers(){
    	
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			event.returnValue = false;
		}
		}
		
		function onlyNumbersAccountNumber(){
    	
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			
			if (event.keyCode == 46){event.returnValue = true;}
			else event.returnValue = false;
			
			
		}
		
		}
		
</script>
  </head>
  <body>
    <center>
    
     
      

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Deleted Voucher Report </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
          <%if (type.compareTo("admin")==0){%>
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
         <%} else if (type.compareTo("reporter")==0){%>
         <%@ include file="/Admin/RepLefMenu.jsp"%>
         <%}%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		
		<h:form id="posForm">
           <div align="center"> <h:outputLabel value="Deleted Voucher Report"/></div>
        <table width="568" height="184">         
         <tr>
          <td align="left">
           <h:outputLabel 
                          value="Campaigns  "/>
          </td>
          <td>
           <h:selectOneMenu onchange="pulldownChanged();"
                            value="#{CampaignReport_Bean.selectedCampaign}">
            <f:selectItems value="#{CampaignReport_Bean.campaigns}"/>
           </h:selectOneMenu>
          </td>
             <td colspan="2">&nbsp;
             </td>
         
         </tr>
         
         
         
         <tr>
          <td align="left">
           <h:outputLabel 
                          value="Gift  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:100%;"
                            value="#{CampaignReport_Bean.selectedGift}">
            <f:selectItems value="#{CampaignReport_Bean.gifts}"/>
           </h:selectOneMenu>
          </td>          
          <td>
               &nbsp;  </td>
           <td>
            &nbsp;
          </td>
          
         </tr>
         <tr>
         <td align="left">
           <h:outputLabel 
                          value="Gift Type"/>
          </td>
          <td>
           <h:selectOneMenu style="width:100%;"
                            value="#{CampaignReport_Bean.selectedItemGiftTypeName}">
            <f:selectItems value="#{CampaignReport_Bean.giftTypeItems}"/>
           </h:selectOneMenu>
          </td>          
          <td>
              <%--               <h:outputLabel value="Account Number"/>  </td>--%>&nbsp;
           <td>
               &nbsp;<%--<h:inputText onkeypress="return onlyNumbers()" maxlength="40" style="width:95%;" value="#{CampaignReport_Bean.accountNumber}"/>--%>
          </td>
          
         </tr>
         <tr>
         <td align="left">
           <h:outputLabel 
                          value="Channel"/>
          </td>
          <td>
           <h:selectOneMenu style="width:100%;" onchange="pulldownChangedSubChannels();"
                            value="#{CampaignReport_Bean.selectedChannel}">
          <f:selectItems value="#{CampaignReport_Bean.channels}"/>
           </h:selectOneMenu>  
          </td>

          <td>
            <h:outputLabel value="Dials more than"/>
          </td>
          <td>
           <h:inputText onkeypress="return onlyNumbers()" maxlength="3" style="width:95%;" value="#{CampaignReport_Bean.moreVoucherValue}" />
          </td>
           
          
         </tr>
         <tr>
                              <td align="left">
           <h:outputLabel 
                          value="Sub Channel"/>
          </td>
          <td>
           <h:selectOneMenu style="width:100%;"
                            value="#{CampaignReport_Bean.selectedSubChannel}">
            <f:selectItems value="#{CampaignReport_Bean.subChannels}"/>
           </h:selectOneMenu>  
          </td>
       
         
         <td>
            <h:outputLabel value="Dial"/>
         </td>
         <td>
         <h:inputText onkeypress="return onlyNumbers()" maxlength="12" value="#{CampaignReport_Bean.dialValue}"/>
         </td>
         
         
         </tr>
         <tr>
         
         <td colspan="4" align="center">
           <h:commandButton value="Find"
                            action="#{CampaignReport_Bean.find_Action}"/>
                            <h:commandButton id="myUpdateButton" action="#{CampaignReport_Bean.getCampaigGift}" style="display: none;"/>
                            <h:commandButton id="subChannelButton" action="#{CampaignReport_Bean.getSubChannel}" style="display: none;"/>
                            
         
         </td>
         </tr>
         
         
         </table>
         <table>
         <tr>
         <td >
         <h:commandLink rendered="#{CampaignReport_Bean.renderDataTable2}" value="Export to CSV" action="#{CampaignReport_Bean.exportToCSVVoucher}"
                        style="color:rgb(0,0,255);"/>
         </td>
         </tr>
         <tr>
         <td>
        <h:dataTable rendered="#{CampaignReport_Bean.renderDataTable2}" rowClasses="row1,row2"
                     value="#{CampaignReport_Bean.voucherResult}" var="Dtvar"
                     binding="#{CampaignReport_Bean.dataTable2}" id="dataTable2"
                     rows="10" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
        
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getDialNumber"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Dial Number"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.dialNumber}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getCampaignName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Campaign Name"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.campaignName}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getGiftName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Gift Name"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getGiftValue"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Gift Value"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftValue}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getCreationDate"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Creation Date"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.creationDate}"/>
         </h:column>
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getRedemptionDate"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Redemption Date"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.redemptionDate}"/>
         </h:column>        
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getRedeemcount"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Redeem count"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.redeemcount}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getPersonName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Deleted By"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.personName}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getDeletedDate"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Deleted Date"/>

           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.deletedDate}"/>
         </h:column>
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getChannel"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Channel"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.channel}"/>
         </h:column>
         
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getSubChannel"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Sub Channel"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.subChannel}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable2}">
            <f:attribute name="sortField" value="getStatus"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Voucher Status"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.status}"/>
         </h:column>
        
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{CampaignReport_Bean.pageFirst}"
                            disabled="#{CampaignReport_Bean.dataTable2.first == 0}"/>
           <h:commandButton value="Prev" action="#{CampaignReport_Bean.pagePrevious}"
                            disabled="#{CampaignReport_Bean.dataTable2.first == 0}"/>
           <h:commandButton value="Next" action="#{CampaignReport_Bean.pageNext}"
                            disabled="#{CampaignReport_Bean.dataTable2.first + CampaignReport_Bean.dataTable2.rows>= CampaignReport_Bean.dataTable2.rowCount}"/>
           <h:commandButton value="Last" action="#{CampaignReport_Bean.pageLast}"
                            disabled="#{CampaignReport_Bean.dataTable2.first + CampaignReport_Bean.dataTable2.rows>= CampaignReport_Bean.dataTable2.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>
        
        
        
        
        
        
        
        </td></tr>
        </table>
        </h:form>
        
        
        
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
     
     
    </center>
   </body>
 </html>
</f:view>
