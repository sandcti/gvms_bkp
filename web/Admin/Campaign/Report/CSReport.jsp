<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@page import="com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO"%>
<%
HttpSession sess= request.getSession(false);
LoginDTO ldto = (LoginDTO) sess.getValue("adminInfo");
        ldto = ldto == null ? (LoginDTO) sess.getValue("repInfo") : ldto;
        String type = ldto.getUserType();
        ////System.out.println("type iss "+type);

%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
            <script type="text/javascript" src="../../../css/showReportss.js"></script>
      <title>CS Report</title>
     
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
      <script language="javascript">
function pulldownChanged(){
    document.getElementById('csReportFormID:myUpdateButton').click();
}
function onlyNumbers(){
	
	if ( event.keyCode < 48 || event.keyCode >57 ) {
		event.returnValue = false;
	}
	}
	
	function onlyNumbersAccountNumber(){
	
	if ( event.keyCode < 48 || event.keyCode >57 ) {
		alert(event.keyCode);
		if (event.keyCode == 46){event.returnValue = true;}
		else event.returnValue = false;
		
		
	}
	
	}

    function pulldownChangedSubChannels(){
        document.getElementById('csReportFormID:subChannelButton').click();
    }
    function pulldownChangedEditSubChannels(){
        document.getElementById('historyFormId:subChannelEditButton').click();
    }
        
</script>
      
  </head>
  <body>
    <center>
    
     
     
     
     
     
     
     
     
     
     
     

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> CS Report </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
          <%if (type.compareTo("admin")==0){%>
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
         <%} else if (type.compareTo("reporter")==0){%>
         <%@ include file="/Admin/RepLefMenu.jsp"%>
         <%}%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		<h:form id="csReportFormID">
           <div align="center"> <h:outputLabel value="CS Report"/></div>
        <table>
        <tr>
          <td width="31" align="left" height="19">
           <h:outputLabel value="From  "/>
		   
          </td>
          <td align="left" colspan="3">
           <t:inputDate onkeypress="onlyNumbers();"  value="#{CSHistory_Bean.fromDate}"
                        id="inputDateFrom" type="date" popupCalendar="true"
                        ampm="true"/>
						
						
          </td>
		  </tr>
		  <tr>
          <td width="31" align="left" height="38">
           <h:outputLabel 
                          value="To  "/>
          </td>
          <td align="left" colspan="3">
           <t:inputDate onkeypress="onlyNumbers();"
                        value="#{CSHistory_Bean.toDate}"
                        id="inputDateTo" type="date" popupCalendar="true"
                        ampm="true"/>
          </td>
		  </tr>

         <tr>
          <td align="left" height="19">
           <h:outputLabel 
                          value="Campaigns"/>
          </td>
          <td align="left" width="25%">
           <h:selectOneMenu onchange="pulldownChanged();" style="width:99%;"
                            value="#{CSHistory_Bean.selectedCampaign}">
            <f:selectItems value="#{CSHistory_Bean.campaigns}"/>
           </h:selectOneMenu>
           <h:commandButton id="myUpdateButton" action="#{CSHistory_Bean.getCampaigGift}" style="display: none;"/>
           <h:commandButton id="subChannelButton" action="#{CSHistory_Bean.getSubChannel}" style="display: none;"/>
           <h:commandButton id="subChannelEditButton" action="#{CSHistory_Bean.getEditSubChannel}" style="display: none;"/>
          </td>
          <td align="left" width="179" >
           <h:outputLabel 
                          value="Gifts"/>
          </td>
          <td align="left" width="241">
           <h:selectOneMenu style="width:155px;" value="#{CSHistory_Bean.selectedGift}">
            <f:selectItems value="#{CSHistory_Bean.gifts}"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td height="19" align="left" nowrap="nowrap">
           <h:outputLabel 
                          value="Channels"/>
          </td>
          <td align="left">
           <h:selectOneMenu onchange="pulldownChangedSubChannels();" style="width:99%;" value="#{CSHistory_Bean.selectedChannel}">
            <f:selectItems value="#{CSHistory_Bean.channels}"/>
           </h:selectOneMenu>
           
          </td>
          <td align="left" nowrap="nowrap">
           <h:outputLabel 
                          value="Dial No"/></td><td align="left">
                          <h:inputText style="width:150px;" onkeypress="onlyNumbers();"  value="#{CSHistory_Bean.dialNumber}"/>           
          </td>
          <td width="4">&nbsp;          </td>
         </tr>
         <tr>
          <td height="19" align="left" nowrap="nowrap">
           <h:outputLabel 
                          value="Sub Channels"/>
          </td>
          <td align="left">
           <h:selectOneMenu  style="width:99%;" value="#{CSHistory_Bean.selectedSubChannel}">
            <f:selectItems value="#{CSHistory_Bean.subChannels}"/>
           </h:selectOneMenu>
           
          </td>
          <td align="left" nowrap="nowrap">
           <h:outputLabel 
                          value="Voucher No"/></td><td align="left">
                          <t:inputText style="width:150px;" onkeypress="onlyNumbers();" value="#{CSHistory_Bean.voucherNumber}"/>           
          </td>
          <td width="4">&nbsp;          </td>
         </tr>
            <tr>
          <td height="19" nowrap="nowrap">
           <h:outputLabel 
                          value="CS Name"/>
          </td>
          <td align="left">
          
           <t:inputText style="width:95%;white-space: nowrap;" value="#{CSHistory_Bean.adminSearchCSName}"/>
           
          </td>
          
          <td align="left" nowrap="nowrap">
           <h:outputLabel 
                          value="Account No"/></td><td align="left">
                          <t:inputText style="width:150px;" onkeypress="onlyNumbersAccountNumber();" value="#{CSHistory_Bean.accountNumber}"/>           
          </td>
          <td width="4">&nbsp;          </td>
         </tr>
         <tr>
         <td colspan="4" align="center">
		 <h:commandLink value="Search" style="color:rgb(240,187,152);font-weight: bold;font-family: Verdana;font-size: 12px;" action="#{CSHistory_Bean.filterCSLogs}" />


                            
          </td>
         </tr>
        
        </table>
         <table>
         <tr>
         <td >
         <h:commandLink  value="Export to CSV" action="#{CSHistory_Bean.exportToCSVCustSupport}"
                        style="color:rgb(0,0,255);"/>
         </td>
         </tr>
         <tr>
         <td>
        <h:dataTable  rowClasses="row1,row2"
                     value="#{CSHistory_Bean.csLogs}" var="Dtvar"
                     binding="#{CSHistory_Bean.dataTable1}" id="dataTable1"
                     rows="10" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getCampaign_name"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Campaign Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.campaign_name}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="setGift_name"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Gift Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.gift_name}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getVoucher_number"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Voucher NO"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.voucher_number}"/>
         </h:column>
         <t:column style="nowrap();">
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getDail_number"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Dial No"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.dail_number}"/>
         </t:column>
         <t:column style="nowrap();">
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getAccount_number"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Account No"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.account_number}"/>
         </t:column>
         <t:column style="nowrap();">
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getUser_type_name"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Channel"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.user_type_name}"/>
         </t:column>
         
         <t:column style="nowrap();">
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getUser_name"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Sub Channel"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.user_name}"/>
         </t:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getAction_text"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Action"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.action_text}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getRedemprion_date"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Redemption Date"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.redemprion_date}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getAction_date"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Creation Date"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.action_date}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CSHistory_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getCs_name"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="CS Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText style="white-space: nowrap;" value="#{Dtvar.cs_name}"/>
         </h:column>
         
         
         <f:facet name="footer">
          <h:panelGroup >
          
           <h:commandButton value="First" action="#{CSHistory_Bean.pageFirst}"
                            disabled="#{CSHistory_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{CSHistory_Bean.pagePrevious}"
                            disabled="#{CSHistory_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{CSHistory_Bean.pageNext}"
                            disabled="#{CSHistory_Bean.dataTable1.first + CSHistory_Bean.dataTable1.rows>= CSHistory_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{CSHistory_Bean.pageLast}"
                            disabled="#{CSHistory_Bean.dataTable1.first + CSHistory_Bean.dataTable1.rows>= CSHistory_Bean.dataTable1.rowCount}"/>          
		  </h:panelGroup>
          
         </f:facet>
        </h:dataTable>
        </td></tr>
        </table>
        </h:form>
        
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    </center>
   </body>
 </html>
</f:view>