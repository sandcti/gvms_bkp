<%@page import="com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp" prefix="c"%>
<%
HttpSession sess= request.getSession(false);
LoginDTO ldto = (LoginDTO) sess.getValue("adminInfo");
        ldto = ldto == null ? (LoginDTO) sess.getValue("repInfo") : ldto;
        String type = ldto.getUserType();
        ////System.out.println("type iss "+type);

%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
            <script type="text/javascript" src="../../../css/showReportss.js"></script>
      <title>Campaign Report</title>
   
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    <center>
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Campaign Report </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
                <%if (type.compareTo("admin")==0){%>
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
         <%} else if (type.compareTo("reporter")==0){%>
         <%@ include file="/Admin/RepLefMenu.jsp"%>
         <%}%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		 <h:form>
        <div align="center"><h:outputLabel value="Campaign Chart"/></div>
        <h:selectOneMenu value="#{CampaignReport_Bean.selectedItemCampaignName}">
         <f:selectItems value="#{CampaignReport_Bean.campaignItems}"/>
        </h:selectOneMenu>
        <h:commandButton value="Chart" action="#{CampaignReport_Bean.chart_Action}"/>
        <table>
         <tr align="center">
          <td colspan="2">
           <c:chart id="chart1" rendered="#{CampaignReport_Bean.renderChart}"
                    datasource="#{CampaignReport_Bean.pieDataset}" type="pie"
                    is3d="true" antialias="true" title="Campaign Report"
                    xlabel="X Label" ylabel="Y Label" height="300" width="600"></c:chart>
          </td>
         </tr>
         <tr>
         <td colspan="2" align="right">
          <h:commandButton value="Export to excel" rendered="#{CampaignReport_Bean.renderChart}" action="#{CampaignReport_Bean.exportHtmlTableAsExcel}"/>
       
       
         
         </td>
         </tr>
         <tr>
          <td width="216" align="left">
           <h:outputLabel value="Total Vouchers = "
                          rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
          <td width="346" align="left">
           <h:outputText value="#{CampaignReport_Bean.totalVoucher}"
                         rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
         </tr>
         <tr>
          <td width="216" align="left">
           <h:outputLabel value="Redeemed Vouchers ="
                          rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
          <td width="346" align="left">
           <h:outputText value="#{CampaignReport_Bean.redeemVoucher}"
                         rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
         </tr>
         <tr>
          <td width="216" align="left">
           <h:outputLabel value="#{CampaignReport_Bean.statusValue} ="
                          rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
          <td width="346" align="left">
           <h:outputText value="#{CampaignReport_Bean.expiredVoucher}"
                         rendered="#{CampaignReport_Bean.renderChart}"/>
          </td>
         </tr>
         
        </table>
       </h:form>   
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>