<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@page import="com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO"%>
<%
HttpSession sess= request.getSession(false);
LoginDTO ldto = (LoginDTO) sess.getValue("adminInfo");
        ldto = ldto == null ? (LoginDTO) sess.getValue("repInfo") : ldto;
        String type = ldto.getUserType();
        ////System.out.println("type iss "+type);

%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" type="text/css" href="../../../css/Login.css"/> 
            <script type="text/javascript" src="../../../css/showReportss.js"></script>
      <title>User Report</title>
   
   
   <style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
  </head>
  <body>
    <center>
    
    

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> User Report </label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../../../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			
			   <table width="100%">
          
         
         <tr>
            <td >
          <%if (type.compareTo("admin")==0){%>
         <%@ include file="/Admin/AdminLefMenu.jsp"%>
         <%} else if (type.compareTo("reporter")==0){%>
         <%@ include file="/Admin/RepLefMenu.jsp"%>
         <%}%>
              
            </td>
          </tr>
          
        </table>
			</td>
		<td colspan="3" align="center" valign="top"  width="578" height="414">
		
		
		<!-- cut from here -->
		<h:form id="userForm">
           <div align="center"> <h:outputLabel value="User Report"/></div>
        <table width="568" height="184">
         <tr>
          <td width="79">
           <h:outputLabel 
                          value="From  "/>
          </td>
          <td align="left" colspan="3">
           <t:inputDate 
                        value="#{CampaignReport_Bean.filterFromDate}"
                        id="inputDateFrom" type="date" popupCalendar="true"
                        ampm="true"/>
          </td>
		  </tr>
		  <tr>
          <td width="79">
           <h:outputLabel 
                          value="To  "/>
          </td>
		  
          <td align="left" colspan="3">
           <t:inputDate 
                        value="#{CampaignReport_Bean.filterToDate}"
                        id="inputDateTo" type="date" popupCalendar="true"
                        ampm="true"/>
           <br></br>
          </td>
         </tr>
         <tr>
          <td>
           <h:outputLabel 
                          value="Campaigns  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;"
                            value="#{CampaignReport_Bean.selectedItemCampaignName}">
            <f:selectItems value="#{CampaignReport_Bean.campaignItems}"/>
           </h:selectOneMenu>
          </td>
          <td>
           <h:outputLabel 
                          value="Sales Channel  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;"
                            value="#{CampaignReport_Bean.selectedItemUserTypeName}">
            <f:selectItems value="#{CampaignReport_Bean.userTypeItems}"/>
           </h:selectOneMenu>
          </td>
         </tr>
         <tr>
          <td>
           <h:outputLabel 
                          value="Gift  "/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;"
                            value="#{CampaignReport_Bean.selectedItemGiftName}">
            <f:selectItems value="#{CampaignReport_Bean.giftItems}"/>
           </h:selectOneMenu>
          </td>
          <td>
           <h:outputLabel 
                          value="User Name"/></td><td>
                          <h:inputText style="width:145px;" value="#{CampaignReport_Bean.userValue}"/>           
          </td>
          </tr>
          <tr>
          <td>
           <h:outputLabel 
                          value="Gift Type"/>
          </td>
          <td>
           <h:selectOneMenu style="width:150px;"
                            value="#{CampaignReport_Bean.selectedItemGiftTypeName}">
            <f:selectItems value="#{CampaignReport_Bean.giftTypeItems}"/>
           </h:selectOneMenu>
          </td>
          
          <td>
           <h:outputLabel 
                          value="POS Code"/></td><td>
                          <h:inputText style="width:145px;" value="#{CampaignReport_Bean.posValue}"/>           
          </td>
          <td>&nbsp;
           
          </td>
         </tr>
         
         
         
         
         

         
         <tr>
         <td colspan="4" align="center">
           <h:commandButton value="Filter"
                            action="#{CampaignReport_Bean.goFilter_Action}"
                            />
                            
          </td>
         </tr>
         
         </table>
         <table>
          <tr>
         <td>
         <h:commandLink rendered="#{CampaignReport_Bean.renderExportToCSV}" value="Export to CSV" action="#{CampaignReport_Bean.exportToCSVVoucher}"
                        style="color:rgb(0,0,255);"/>
         </td>
         </tr>
         <tr>
         <td>
        <h:dataTable rendered="#{CampaignReport_Bean.renderDataTable}" rowClasses="row1,row2"
                     value="#{CampaignReport_Bean.filterResult}" var="Dtvar"
                     binding="#{CampaignReport_Bean.dataTable1}" id="DataTableUser"
                     rows="100" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
        
         <h:column>
          <f:facet name="header">
           <h:panelGroup>
            <h:outputText value="#"/>
           </h:panelGroup>
          </f:facet>
          
          <div align="center">
           <h:outputLabel value="#{CampaignReport_Bean.dataTable1.rowIndex + 1}"/>
          </div>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getCampaignName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Campaign Name"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.campaignName}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getGiftType"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Gift Type"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftType}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getGiftName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Gift Name"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getDate"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Date"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.date}"/>
         </h:column>
                 
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getTotalCost"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Value"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.totalCost}"/>
         </h:column>
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getUserTypeName"/>
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="Sales Channels"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.userTypeName}"/>
         </h:column>
         
         <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getUserName"/>
            
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="User Name"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.userName}"/>
          
            </h:column>
            
            <h:column >
          <f:facet name="header">
           <h:commandLink actionListener="#{CampaignReport_Bean.sortDataTable}">
            <f:attribute name="sortField" value="getPosCode"/>
            
            <h:outputText style="color:rgb(0,0,0); font-size:small;"
                          value="POS CODE"/>
            
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.posCode}"/>
           
         </h:column>
         
         
         
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{CampaignReport_Bean.pageFirst}"
                            disabled="#{CampaignReport_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{CampaignReport_Bean.pagePrevious}"
                            disabled="#{CampaignReport_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{CampaignReport_Bean.pageNext}"
                            disabled="#{CampaignReport_Bean.dataTable1.first + CampaignReport_Bean.dataTable1.rows>= CampaignReport_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{CampaignReport_Bean.pageLast}"
                            disabled="#{CampaignReport_Bean.dataTable1.first + CampaignReport_Bean.dataTable1.rows>= CampaignReport_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable></td></tr>
        </table>
        </h:form>
        
       <!-- cut to here -->
		        </td>
        
</tr>
        
	
	<%@ include file="../../Adminfooter.jsp" %>
	<tr>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../../../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
     
    
    
    
    
    
    
    
    
    </center>
   </body>
 </html>
</f:view>