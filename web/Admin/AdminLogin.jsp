<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"></meta>
   <title>Login</title>
   <script type="text/javascript">
   function checkEnter(e){ 
        var characterCode;
        
        if(e && e.which){ 
          e = e;
          characterCode = e.which; 
        }
        else{
          e = event;
          characterCode = e.keyCode; 
        }
        
        if(characterCode == 13){ 
          document.getElementById("LoginForm:Login").click(); 
          return false;
        }
        else{
           return true;
        }

}
   </script>
   <link rel="stylesheet" type="text/css" href="../css/Login.css"/>
   <style type="text/css">
   a {
   
  TEXT-DECORATION: NONE;
  color: white; 
  font-weight: noraml ;
  font-size: 16px;
   
  
   }
   </style>
  </head>
  <body><h:form id="LoginForm">
    <center>
    
    <table id="Table_01" width="800" height="600" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<img src="../images/AdminLogin/new/english-admit-for-CS_01.gif" width="800" height="234" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="../images/AdminLogin/new/english-admit-for-CS_02.gif" width="335" height="328" alt=""></td>
		<td bgcolor="#e7e7e7" valign="top" width="465" height="220">
			
			
			<table cellspacing="0" cellpadding="0" border="0" width="76%">
            <tr>
             <td width="36%" align="left">
              <h:outputLabel value=""/>
             </td>
             <td width="64%" align="left">
              <h:outputLabel value="" binding="#{AdminLogin_Bean.error}"/>
             </td>
            </tr>
            <tr>
             <td align="left">
              <h:outputLabel value="User name:" styleClass="label"/>
             </td>
             <td align="left">
              <h:inputText style="width:92%"
                           binding="#{AdminLogin_Bean.userName}" value=""/>
             </td>
            </tr>
            <tr>
             <td align="left">
              <h:outputLabel value="Password :"/>
             </td>
             <td align="left">
              <h:inputSecret onkeypress="checkEnter(event);" style="width:92%"
                             binding="#{AdminLogin_Bean.password}"/>
             </td>
            </tr>
            <tr>
             <td>&nbsp;</td>
            </tr>
            <tr>
             <td>&nbsp;</td>
             <td align="left" valign="top">
              <h:commandLink id="Login"
                             action="#{AdminLogin_Bean.checkAdminInfo}">
              <h:outputLabel style=" font-size: 13pt; font-weight:bold;color: #FD8B3F;" value="Login"/>
              </h:commandLink>
             </td>
            </tr>
            <tr>
             <td>&nbsp;</td>
             <td align="left" valign="top">
             <h:commandLink action="#{AdminLogin_Bean.forgetPass}">
               <h:outputLabel style=" font-size: 13pt; font-weight:bold;color: #FD8B3F;" value="Forgot Password ?"/>
              </h:commandLink>
             </td>
            </tr>
            <tr>
             <td>&nbsp;</td>
             <td align="left" valign="top">
             <%--h:commandLink action="#{AdminLogin_Bean.cleanDB}">
               <h:outputLabel style=" font-size: 13pt; font-weight:bold;color: #FD8B3F;" value="Clean DB"/>
              </h:commandLink--%>
             </td>
            </tr>
           </table>
			
			</td>
	</tr>
	<tr>
		<td>
			<img src="../images/AdminLogin/new/english-admit-for-CS_04.gif" width="465" height="108" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="800" height="38" align="center" background="../images/AdminLogin/new/english-admit-for-CS_05.gif">
		<h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px; color:#FCFCFC;" value="�Egyptian Company For Mobile Services (Mobinil) 2010, All rights reserved.
"/>
			<br>
		<h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px; color:#FCFCFC;" value="GVMS Developed By"/>
                
                <h:outputLink  style=" text-decoration: none; cursor:pointer;" value="http://www.sandcti.com">
                <h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px;  cursor:pointer ; color:#FCFCFC;" value="SAND S.A.E."/>
                </h:outputLink>
			</td>
	</tr>
</table>
    
    
    
    
    </center>
   </h:form></body>
 </html>
</f:view>