
<h:form id="leftMenuForm">
    <table width="100%">
         <tr>
            <td align="center">
              <h:outputLabel value="Welcome"/><br>
              <h:outputLabel value="#{leftMenu_Bean.userNameLbl}"/>
              
            </td>
          </tr>
          
         
          </table>
          
          <div id="showReports">
          <table align="center" width="100%">
           <tr>
          
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
            
              
               <h:outputLabel style=" font-weight: noraml; font-size: 16px;" id="reportId" value="Reports"/>
              
              
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              <h:commandLink action="#{CampaignReport_Bean.cleanValuesCampaignReport}">
               &nbsp;&nbsp; <h:outputText value="Campaign" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
            
             <h:commandLink action="#{CampaignReport_Bean.cleanValuesPOSReport}">
               &nbsp;&nbsp; <h:outputText value="POS" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesUserReport}">
               &nbsp;&nbsp; <h:outputText value="User" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesUSERSummaryReport}">
               &nbsp;&nbsp; <h:outputText value="User Summary" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesPOSMONEYReport}">
               &nbsp;&nbsp; <h:outputText value="POS Money" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesVoucherReport}">
               &nbsp;&nbsp; <h:outputText value="Voucher" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesDeletedVoucherReport}">
               &nbsp;&nbsp; <h:outputText value="Deleted Voucher" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesTimeChart}">
               &nbsp;&nbsp; <h:outputText value="Time Chart" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">
              
           
            <h:commandLink action="#{CampaignReport_Bean.cleanValuesCSReport}">
               &nbsp;&nbsp; <h:outputText value="CS Report" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left">


            <h:commandLink action="#{CampaignReport_Bean.cleanValuesWS}">
               &nbsp;&nbsp; <h:outputText value="WS Report" style="color:rgb(99,99,99);;font-size:small;"/>
              </h:commandLink>
            </td>
          </tr>
         
          </table> </div>
          <table width="100%">
          <tr>
          <td style="width:25%">&nbsp;
          </td>
            <td align="left" >            
              <h:commandLink  action="#{leftMenu_Bean.logout_Action}">
                <h:outputText style="color:rgb(99,99,99);; font-weight: bold; font-size: 16px;" value="Logout"/>
            </h:commandLink>
              
            </td>
          </tr>
         
        </table>
        </h:form>
