<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
   <link rel="stylesheet" type="text/css" href="css\orangeEn.css"/>
  
  <script type="text/javascript">
  function CallPrint(strid)
{
var prtContent = document.getElementById(strid);
var WinPrint =
window.open('','','left=0,top=0,width=1,height=1,t oolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();

}
  </script>
   <title>Redeemed</title>
  </head>
  <body><h:form binding="#{Redeemed_Bean.form1}" id="form1">
 
        
        
        <center><h:inputHidden value="#{Redeemed_Bean.usrIdHiden}"/>
    








<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/Redeemed-final_04.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<table width="100%">
         <%@ include file="/leftMenu.jsp" %>
         <tr>
          <td>
          <div align="center" >
           
            <h:commandLink style="font-family:Arial;"   action="#{Login_Bean.exitEn}">
           <f:verbatim>Logout</f:verbatim>          
           
           </h:commandLink></div>
           </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
         </tr>
         
        </table>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			<table>
 
 <tr align="center">
 <td>
         <h:outputLabel  value="This  voucher number "
                        binding="#{Redeemed_Bean.outputLabel4}"
                        id="outputLabel4"/> 
          
         <h:outputLabel value="#{Redeemed_Bean.voucherNumber}"
                        binding="#{Redeemed_Bean.outputLabel1}"
                        id="outputLabel1"
                        style="font-style:italic; color:rgb(0,128,0);"/>
          
         <h:outputLabel  value="was redeemed."
                        binding="#{Redeemed_Bean.outputLabel5}"
                        id="outputLabel5"/>
                        
                        </td></tr>
         <tr align="center">
                        <td>
                        <h:outputLabel  value="Redeemed by #{Redeemed_Bean.userName}"/>
                        </td>
                        </tr>
                        
                        <tr align="center">
                        <td>
                        <h:outputLabel  value="Redeemed on #{Redeemed_Bean.voucherDate}"/>
                        </td>
                        </tr>
         
       <tr align="center">
                        <td>
                        <h:outputLabel  value="POS Code: #{Redeemed_Bean.posUser}"/>
                        </td>
                        </tr>
                        
                        <tr align="center">
                        <td>
                        <h:outputLabel  value="Gift : #{Redeemed_Bean.giftName}"/>
                        </td>
                        </tr>
        <tr>
         </table>
         </div>
        <table>
        <tr>
        <td colspan="3"><div align="center"><h:commandLink action="#{Redeemed_Bean.reSearchEn}">
        
        
         <t:graphicImage url="/images/Buttons/back.gif"                         
                        alt="" border="0"/></h:commandLink>&nbsp; <t:commandLink onclick="javascript:CallPrint('divIdentyCard');">
                         <t:graphicImage url="/images/Buttons/printButton.gif" alt="" border="0"/>
                        </t:commandLink>
                        </div></td>
						</tr>
       </table>
        
        
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>




    </center>
       
   </h:form></body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Redeemed_Bean--%>