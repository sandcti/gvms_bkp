<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1256"/>
   <link rel="stylesheet" type="text/css" href="css\orangeEn.css"/>
   <script type="text/javascript">
            
            function disp_a(id)
           {
           var param = id.value;
          alert("Value of 'Param' inside the function " + id);
            }
            
            function load()
            {
              
              var yy=document.getElementById('unredeemForm:dataTable1:1:ihidden');
              alert(yy.value);
              var ff=document.getElemenyById('unredeemForm:dataTable1:7:inputTextControlId');
              alert(ff.value);
            }

            </script>
   <title>Unredeemed</title>
  </head>
  <body>
    <center><h:inputHidden value="#{Unredeemed_Bean.usrIdHiden}"/>
     
   
   
   
   
   
   
   
   
   
   
   
   
<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/Voucher-Unredeemed-of-final_04 copy.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<h:form id="form_ID">
			<table width="100%">
         <%@ include file="/leftMenu.jsp" %>
         <tr>
          <td>
          <div align="center" >
           
            <h:commandLink style="font-family:Arial;"   action="#{Login_Bean.exitEn}">
           <f:verbatim>Logout</f:verbatim>          
           
           </h:commandLink></div>
           </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
         </tr>
         
        </table>
        </h:form>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			
			 <h:form id="unredeemForm">
      <table width="100%" border="0">
       <tr>
        <td width="23%" align="left">
         <h:outputLabel style="color:black" value="Voucher Number :"
                        binding="#{Unredeemed_Bean.outputLabel2}"
                        id="outputLabel2"/>
        </td>
        <td align="left" width="50%">
         <h:outputLabel style="color:black"
                        binding="#{Unredeemed_Bean.voucherOutputLabel1}"
                        id="outputLabel1"
                        value="#{Unredeemed_Bean.voucher_Number}"/>
        </td>
        <td>&nbsp;</td>
       </tr>
       <tr>
        <td width="23%" align="left">
         <h:outputLabel style="color:black" value="Gift Name :"/>
        </td>
        <td align="left" width="50%">
         <t:outputLabel rendered="#{Unredeemed_Bean.renderGift}" style="color:orange" value="#{Unredeemed_Bean.giftName}"/>
         
         <h:selectOneMenu rendered="#{!Unredeemed_Bean.renderGift}"  value="#{Unredeemed_Bean.giftName}" 
                            id="selectGiftId">
            <f:selectItems value="#{Unredeemed_Bean.gifts}"
                           id="giftItemsId"/>
    </h:selectOneMenu>
         
        </td>
        <td>&nbsp;</td>
       </tr>
       
              
       <tr>
        <td width="23%" align="left">
         <h:outputLabel style="color:black" value="Dial Number :"
                        rendered="#{Unredeemed_Bean.renderDialNumber}"
                        binding="#{Unredeemed_Bean.dialOutputLabel}"
                        id="field1"/>
        </td>
        <td width="50%" align="left">
         <h:inputText style="width:50%" value="#{Unredeemed_Bean.dialInputText}"
                      id="field1IT" required="true"
                      rendered="#{Unredeemed_Bean.renderDialNumber}">
          <f:converter converterId="dialConverter"/>
         </h:inputText>
        </td>
        <td>
         <h:outputText binding="#{Unredeemed_Bean.checkLbl}" id="checkLbl"
                       rendered="#{Unredeemed_Bean.renderDialNumber}"
                       style="color:rgb(255,0,0);"/>
         <h:message for="field1IT" style="color: red;"/>
        </td>
       </tr>
       <tr>
        <td width="23%" align="left">
         &nbsp;
        </td>
        <td width="50%" align="left">
         <h:outputLabel style="font-size:13px" value="ex: 123215683"
                        rendered="#{Unredeemed_Bean.renderDialNumber}"                        
                        id="fieldExample"/>
        </td>
        <td>
&nbsp;
        </td>
       </tr>
      </table>
      <hr></hr>
      <div align="left">
      <t:dataTable cellspacing="0" cellpadding="0" width="100%"
                   value="#{Unredeemed_Bean.dt}" var="Dtvar" id="dataTable1">       
       <t:column width="20%">
        <h:outputLabel style="color:black" value="#{Dtvar.value} :"/>
       </t:column>
       <t:column width="55%">
        <t:inputDate required="true" id="dateControlId"
                     converter="userConverter"
                     rendered="#{Dtvar.renderInputDate}" type="date"
                     popupCalendar="true" ampm="true" value="#{Dtvar.date}">
         <t:inputHidden rendered="#{Dtvar.renderInputDate}" id="hiddenDate"
                        value="#{Dtvar.id}"/>
         <f:converter converterId="userConverter"/>
        </t:inputDate>
        <h:inputText style="width:50%" id="inputTextControlId"
                     rendered="#{Dtvar.renderInputText}"
                     value="#{Dtvar.inputValue}">
         <f:validator validatorId="userValidator"/>
         <t:inputHidden rendered="#{Dtvar.renderInputText}" id="hiddenText"
                        value="#{Dtvar.id}"/>
         <f:converter converterId="userConverter"/>
        </h:inputText>
        <h:inputTextarea style="width:50%" id="inputTextAreaControlId"
                         value="#{Dtvar.textValue}"
                         rendered="#{Dtvar.renderTextarea}">
         <f:validator validatorId="userValidator"/>
         <t:inputHidden rendered="#{Dtvar.renderTextarea}" id="hiddenArea"
                        value="#{Dtvar.id}"/>
         <f:converter converterId="userConverter"/>
        </h:inputTextarea>
        <h:selectOneMenu style="width:50%" id="selectMenuControlId"
                         value="#{Dtvar.comboValue}"
                         rendered="#{Dtvar.renderCombo}">
         <f:selectItems value="#{Dtvar.compoList}"/>
         <t:inputHidden rendered="#{Dtvar.renderCombo}" id="hiddenMenu"
                        value="#{Dtvar.id}"/>
         <f:converter converterId="userConverter"/>
        </h:selectOneMenu>
       </t:column>
       <t:column>
        <h:message for="selectMenuControlId"/>
        <h:message for="inputTextAreaControlId"/>
        <h:message for="inputTextControlId"/>
        <h:message for="dateControlId"/>
       </t:column>
      </t:dataTable>
      </div>
      <br>
      <div align="center">
       <h:commandLink action="#{Unredeemed_Bean.redeemAction}">
        <t:graphicImage alt="" border="0" url="/images/Buttons/redeem.gif"/>
       </h:commandLink>
       <br></br>
        
       <br></br>
        
       <h:outputLabel value="#{Unredeemed_Bean.redeemFail}" id="redeemFail"/>
      </div>
      </h:form>
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>
   
   
   
   
   
   
   
   </center>
   </body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Unredeemed_Bean--%>