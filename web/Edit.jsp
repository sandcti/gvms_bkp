<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
            <link rel="stylesheet" type="text/css" href="css/orangeEn.css"/>
            <script type="text/javascript" src="css/passStrength.js"></script>
      <title>Edit</title>
      
    </head>
    <body><h:form id="editUserId">
        
        <center><h:inputHidden value="#{Edit_Bean.usrIdHiden}"/>
     
    





<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/edit-of-final_04.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<table width="100%">
         <%@ include file="/leftMenu.jsp" %>
         <tr>
          <td>
          <div align="center" >
           
            <h:commandLink style="font-family:Arial;"   action="#{Login_Bean.exitEn}">
           <f:verbatim>Logout</f:verbatim>          
           
           </h:commandLink></div>
           </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
         </tr>
         
        </table>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			<table cellspacing="0" cellpadding="0" border="0" height="414" >
		 <tr height="20">
           
            <td colspan=2  align="left">&nbsp;              
            </td>
          </tr>
          <tr height="20">
           
            <td colspan=2  align="left">&nbsp;              
            </td>
          </tr>
          <tr height="20">
            <td  align="left" >
           
            <h:outputLabel value="Old Password :"/>
            </td>
            <td  align="left">
              <h:inputSecret value="#{Edit_Bean.oldPassword}"/>
              <h:outputText value="#{Edit_Bean.msgWarn1}"/>
            </td>
          </tr>
          <tr height="20">
            <td  align="left">
           <h:outputLabel  value="New Password :"/>
           </td>
            <td  align="left">
                <table border="0">
                    <tr>
                        <td>
              <h:inputSecret id="newPasswordId"  value="#{Edit_Bean.newPassword}" onkeyup="testPassword(document.getElementById('editUserId:newPasswordId').value,document.getElementById('editUserId:verdict'))" /></td><td><h:inputText size="10" readonly="true" id="verdict"/></td>
              </tr>
              </table>
            </td>
          </tr>
          <tr height="20">
            <td  align="left" height="20">           
            <h:outputLabel value="Re-type New Password:"/>             
            </td>
            <td  align="left">
              <h:inputSecret  value="#{Edit_Bean.reNewPassword}"/>
            </td>
          </tr>
         <tr height="20">
          <td width="45%">&nbsp;</td>
          <td width="55%">
           <h:outputText style="#{Edit_Bean.colorMsgWarn}" value="#{Edit_Bean.msgWarn}"/>
          </td>
         </tr>		 
		 <tr height="20"><td colspan="2" align="center">
		 <h:commandLink action="#{Edit_Bean.editPassword}">
           <t:graphicImage alt="" border="0" url="/images/Buttons/saveChanges.gif"/>
          </h:commandLink>
		 </td></tr>
                 <tr><td colspan="2" align="left">
           <h:outputLabel rendered="#{Edit_Bean.displayWarnigs}" >
           <%@include file="/passWarning.jsp" %>
              </h:outputLabel>
              
           
          </td></tr>
        </table>
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


    </center>
      </h:form></body>
  </html>
</f:view>