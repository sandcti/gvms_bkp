<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
<html>
<head>
<title>Forgot Password</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
label{
	color:rgb(99,99,99);
	font-family:Verdana;
	font-weight:bold;
	font-size: 13pt;
}
</style>
</head>

<body bgcolor="#FFFFFF">
<center>
<!-- ImageReady Slices (login Customer Support 3.jpg) -->














<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Retrieve Password</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			&nbsp;
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			<table cellpadding="0" cellspacing="0" width="100%" height="361" border="0">

  <tr>
    <td align="left">&nbsp;</td>
    <td height="61" align="left">&nbsp;</td>
    <td width="351"align="right" colspan="2" valign="top">&nbsp;</td>
  </tr>
  <h:form>
	<tr>
		<td  valign="top" align="right" width="232"><span style="color: #666666">
		  <label style=""><strong>Email*&nbsp;&nbsp; </strong></label>
		</span></td>
		<td colspan="3">
		<h:inputText id="forgetterPasswordMail" style="width:70%;" value="#{CSForget_Bean.email}" />	</td>		
		<td width="92" rowspan="6">
			<img src="../images/login-Customer-Support-3_11.gif" width="92" height="265" alt=""></td>
		<td>
			<img src="../images/spacer.gif" width="1" height="22" alt=""></td>
	</tr>
	<tr>
	  <td  valign="top" align="right" width="232">&nbsp;</td>
		<td colspan="3"width="249">&nbsp;</td>
		<td>
			<img src="../images/spacer.gif" width="1" height="17" alt=""></td>
	</tr>
	<tr>
	  <td  valign="top" align="right" width="232">&nbsp;</td>
		<td colspan="3">&nbsp;</td>
		<td>
			<img src="../images/spacer.gif" width="1" height="19" alt=""></td>
	</tr>
	<tr>
	  <td  valign="top" align="right" width="232">&nbsp;</td>
		<td colspan="3">
			<img src="../images/login-Customer-Support-3_14.gif" width="249" height="32" alt=""></td>
		<td>
			<img src="../images/spacer.gif" width="1" height="32" alt=""></td>
	</tr>
	
	<tr>
	  <td  valign="top" align="right" width="232">
	  <h:commandLink action="#{CSForget_Bean.sendActivation}">
                <t:graphicImage url="../images/Buttons/getPassword.gif" border="0" width="200" height="36"/>
              </h:commandLink>	  </td>
		<td width="85" >&nbsp;</td>		
		<td width="160">				
			<h:commandLink action="backToLogin">
                <t:graphicImage url="../images/Buttons/back.gif" width="202" height="36" border="0"/>
              </h:commandLink>			</td>
		<td width="1">
			<img src="../images/spacer.gif" width="1" height="36" alt=""></td>
	</tr>
	</h:form>
	<tr>
	  <td colspan="4" align="center"  valign="top"><h:outputText style="color:#FF0000;font-size:14px" value="#{CSForget_Bean.error}"/></td>
		<td>
			<img src="../images/spacer.gif" width="1" height="139" alt=""></td>
	</tr>
</table>
<br>
        </td>
	</tr>
	<%@ include file="Userfooter.jsp" %>
	<tr>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


<!-- End ImageReady Slices -->
</center>
</body>
</html>
</f:view>