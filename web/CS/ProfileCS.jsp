<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <link rel="stylesheet" type="text/css" href="../css/Login.css"/> 
<title>Vouchers</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
<!--
.style1 {color: #CC6633}
.style2 {color: #666666}

-->
</style>
<style type="text/css">
      .yearRowStyle {
                 background-color: #A8D1E8;
                 color: green;
                 text-align: center;
                 font-weight: bold;
                 font-style:italic;
                }
            .weekRowStyle {
                 background-color: #D6EBFC;
                }
            .selectedDayCellStyle {
                background-color: #ECD5D2;
                }
      
      .row1 {
    background-color: #ddd;
        }
      .row2 {
    background-color: #bbb;
        }
      </style>
</head>
<body bgcolor="#FFFFFF">
<center>
<!-- ImageReady Slices (login Customer Support 3.jpg) -->


<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Profile</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<jsp:include page="incLinks.jsp"/>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			<table cellpadding="0" cellspacing="0" width="100%" height="361" border="0">

  <tr>
    <td align="left">&nbsp;</td>
    <td height="61" align="left">&nbsp;</td>
    <td width="351"align="right" colspan="2" valign="top">&nbsp;</td>
  </tr>
  <h:form>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>User Name </label>      <label><span class="style1">*</span></label>
    <div align="center"></div></td>
    <td colspan="2" align="left">
    <h:inputText style="width:70%;" value="#{CSProfile_Bean.userName}"  />
     
    </td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Password</label>
      <label></label></td>
    <td colspan="2" align="left">
    <h:inputSecret style="width:70%;"  value="#{CSProfile_Bean.password}"  />
    
    
    </td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left" nowrap="nowrap"><label>Confirm Password </label></td>
    <td colspan="2" align="left"><h:inputSecret style="width:70%;"  value="#{CSProfile_Bean.confirmPassword}"  /></td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Name<span class="style1">*</span></label></td>
    <td colspan="2" align="left">
    <h:inputText style="width:70%;" value="#{CSProfile_Bean.name}"  /></td>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Email<span class="style1">*</span> </label>
      <label></label></td>
    <td colspan="2" align="left"><h:inputText style="width:70%;" value="#{CSProfile_Bean.email}"  /></td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Phone<span class="style1">*</span> </label>
      <label></label></td>
    <td colspan="2" align="left"><h:inputText style="width:70%;" value="#{CSProfile_Bean.phone}"  /></td>
    </tr>
  
  <tr>
    <td colspan="4"><span class="style1">*</span> <span class="style2">indicates mandatory fields. </span></td>
    </tr>
  <tr>
    <td colspan="4"><div align="center">
    <h:commandLink value="Save" style="color:rgb(240,187,152);font-weight: bold;font-family: Verdana;font-size: 12px;" action="#{CSProfile_Bean.saveAction}" />
    <br>
    <h:outputText style="color:#FF0000;font-size:14px" value="#{CSProfile_Bean.error}"/>
      
    </div></td>
    </tr>
    </h:form>
</table>
<br>
        </td>
	</tr>
	<%@ include file="Userfooter.jsp" %>
	<tr>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>





<!-- End ImageReady Slices -->
</center>
</body>
</html>
</f:view>