<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
<html>

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <link rel="stylesheet" type="text/css" href="../css/Login.css"/> 
<title>Vouchers</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">


<!--

-->
</style>

<script language="javascript">
    function pulldownChanged(){
        document.getElementById('voucherFormId:myUpdateButton').click();
    }
    function pulldownChangedSubChannels(){
        document.getElementById('voucherFormId:subChannelButton').click();
    }
    function onlyNumbers(){
    	
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			event.returnValue = false;
		}
		}
		
		function onlyNumbersAccountNumber(){
    	
		if ( event.keyCode < 48 || event.keyCode >57 ) {
			
			if (event.keyCode == 46){event.returnValue = true;}
			else event.returnValue = false;
			
			
		}
		
		}
		
</script>
</head>
<body bgcolor="#FFFFFF">
<center>
<!-- ImageReady Slices (login Customer Support 3.jpg) -->




<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3" bgcolor="#988f86" align="left" width="512" height="84">
			<label style="color: white;font-family: Arial;font-weight: bold;font-size: 30px;"> Home</label></td>
		<td colspan="2" bgcolor="#988f86">
			<img src="../images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<jsp:include page="incLinks.jsp"/>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			<table cellpadding="0" cellspacing="0" width="100%" height="361" border="0">

  <tr>
    <td align="left">&nbsp;</td>
    <td height="61" align="left">&nbsp;</td>
    <td width="351"align="right" colspan="2" valign="top">&nbsp;</td>
  </tr>
  <h:form id="voucherFormId">
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Campaigns *</label>      <label></label><div align="center"></div></td>
    <td colspan="2" align="left">
    <h:selectOneMenu  onchange="pulldownChanged();" style="width:50%" value="#{CSVoucher_Bean.selectedCampaign}" 
                            id="selectCampaignId">
            <f:selectItems value="#{CSVoucher_Bean.campaigns}"
                           id="campaignItemsId"/>
    </h:selectOneMenu>   
    <h:commandButton id="myUpdateButton" action="#{CSVoucher_Bean.getCampaigGift}" style="display: none;"/>
    
    
    
    </td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left"><label>Dial *</label></td>
    <td colspan="2" align="left">
    <h:inputText onkeypress="return onlyNumbers()" maxlength="12" style="width:50%" id="inputDial" title="ex:121111111" value="#{CSVoucher_Bean.dialNumber}"/>
    </td>
    </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
    </tr>    
    <tr>
    <td align="left" nowrap="nowrap">&nbsp;</td>
    <td align="left" nowrap="nowrap"><label>Account Number  </label></td>
    <td colspan="2" align="left">    
    <h:inputText style="width:50%" onkeypress="return onlyNumbersAccountNumber()" maxlength="40" title="ex:5.12345.11" value="#{CSVoucher_Bean.accountNumber}" />      
    </td>
    </tr>
    <tr>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
    </tr>  
    
  <tr>
    <td width="154" align="left">&nbsp;</td>
    <td width="154" align="left"><label>Gifts *</label></td>
    <td colspan="2" align="left">
    <h:selectOneMenu   style="width:50%" disabled="#{CSVoucher_Bean.disableGiftAndChannel}" value="#{CSVoucher_Bean.selectedGift}" 
                            id="selectGiftId">
            <f:selectItems value="#{CSVoucher_Bean.gifts}"
                           id="giftItemsId"/>
    </h:selectOneMenu>
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
    
  <tr>
    <td align="left" nowrap="nowrap">&nbsp;</td>
    <td align="left" nowrap="nowrap"><label>Channels *</label></td>
    <td colspan="2" align="left">
    <h:selectOneMenu style="width:50%;" disabled="#{CSVoucher_Bean.disableGiftAndChannel}" onchange="pulldownChangedSubChannels();" value="#{CSVoucher_Bean.selectedChannel}">
            <f:selectItems value="#{CSVoucher_Bean.channels}"/>
           </h:selectOneMenu>
           <h:commandButton id="subChannelButton" action="#{CSVoucher_Bean.getSubChannel}" style="display: none;"/>
    </td>
    </tr><tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
    <td align="left" nowrap="nowrap">&nbsp;</td>
    <td align="left" nowrap="nowrap"><label>Sub Channels *</label></td>
    <td colspan="2" align="left">    
    <h:selectOneMenu style="width:50%;" disabled="#{CSVoucher_Bean.disableSubChannel}" value="#{CSVoucher_Bean.selectedSubChannel}">
            <f:selectItems value="#{CSVoucher_Bean.subChannels}"/>
           </h:selectOneMenu>
    </td>
    </tr>
    
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="4">
    <div align="center">
    <h:commandLink style="color:rgb(240,187,152);font-weight: bold;font-family: Verdana;font-size: 12px;" value="Send Voucher" action="#{CSVoucher_Bean.generateVouchers}" />
       &nbsp; &nbsp; 
	  
	  <br>
	  <h:outputText style="color:#FF0000;font-size:14px" value="#{CSVoucher_Bean.error}"/>
    </div></td>
    </tr>
    
    <tr>
    <td colspan="4">
    <br>
    <div align="center">
    <label style="font-size:13px">* indicates mandatory fields. </label>
    </div></td>
    </tr>
    </h:form>
</table>
<br>
        </td>
	</tr>
	<%@ include file="Userfooter.jsp" %>
	<tr>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="../images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>




<!-- End ImageReady Slices -->
</center>
</body>
</html>
</f:view>