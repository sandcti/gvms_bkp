<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ page pageEncoding="windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1256"/>
      <title>insertArabic</title>
    </head>
    <body><h:form binding="#{insertArabic.form1}" id="form1"></h:form>
    
    <h:inputTextarea value="#{insertArabic.insertValue}"/>
    <h:inputText value="#{insertArabic.pageValue}"/>
    
    <t:commandButton action="#{insertArabic.insertAction}" value="insert"/>
    
    </body>
  </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:insertArabic--%>