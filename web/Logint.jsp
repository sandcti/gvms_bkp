<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=CP1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<f:view>

  <html>
  <head>
  <!--
    <meta http-equiv="Content-Type" content="text/html; charset=CP1256"></meta>
  -->
    
    <link rel="stylesheet" type="text/css" href="css\Login.css"/> 
    
    <title>Login</title>
  
  </head>
  <body>
  <h:form id="LoginForm"><center>
  
  
              <table id="Table_01" width="801" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="images/Login/sign-in-page_01.gif" width="800" height="48" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="48" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="images/Login/sign-in-page_02.gif" width="41" height="73" alt=""></td>
		<td>
			<img src="images/Login/sign-in-page_03.gif" width="180" height="73" alt=""></td>
		<td colspan="2" rowspan="2">
			<img src="images/Login/sign-in-page_04.gif" width="546" height="75" alt=""></td>
		<td rowspan="2">
			<img src="images/Login/sign-in-page_05.gif" width="33" height="75" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="73" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<img src="images/Login/sign-in-page_06.gif" width="41" height="387" alt=""></td>
		<td rowspan="2" background="images/Login/sign-in-page_07.gif" width="100%" valign="top" align="center">
			
            
            
                        </td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td><img src="images/Login/sign-in-page_08.gif" width="154" height="385" alt=""></td>
		<td>
		
		<table width="324" height="342">
		
		<tr>
		<td width="300" align="left" valign="bottom"><table width="100%" height="190" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="40%"><h:outputLabel value=""/>            </td>
            <td width="60%" align="left" ><h:outputLabel style="color:red; font-size:12px;"  value="#{Login_Bean.error}"/>            </td>
          </tr>
          <tr>
            <td align="left"><h:outputLabel value="User Name:" />            </td>
            <td align="left"><h:inputText style="width:95%;" value="#{Login_Bean.userName}"/>            </td>
          </tr>
          <tr>
            <td align="left"><h:outputLabel  value="Password:" />            </td>
            <td align="left"><h:inputSecret  style="width:95%;" value="#{Login_Bean.password}"/>            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right" >&nbsp;</td>
            <td align="left"><h:commandLink action="#{Login_Bean.checkUserInfo}">
                <t:graphicImage border="0" url="images/Login/login_button.gif"/>
              </h:commandLink>
                <br>
              <br>
              <h:commandLink action="#{Login_Bean.forgetPassAction}">
                  <h:outputLabel value="Forgot your password?" style="color: #FD8B3F; font-size: 13pt; font-weight:bold;"/>
                </h:commandLink>   </td>
          </tr>
        </table></td>
		</tr>
		<tr>
		<td>
				<table width="167">
		
		<tr>
		<td align="right" valign="bottom">
		<h:commandLink  action="#{Login_Bean.gotoArabicLogin}"> 
                <h:outputLabel value="#{Login_Bean.namesArray[0].arabic}"/>
                </h:commandLink>
                		</td>
		</tr>
		</table>		</td>
		</tr>
		</table>
		
		
		
		
		</td>
		<td>
			<img src="images/Login/sign-in-page_10.gif" width="33" height="385" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="385" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img src="images/Login/sign-in-page_11.gif" width="800" height="2" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/Login/sign-in-page_12.gif" width="40" height="58" alt=""></td>
				<td align="left" colspan="4" background="images/Login/sign-in-page_13.gif" width="100%">	
		<%@ include file="/Userfooter.jsp" %>
		
		
		  </td>
		<td>
			<img src="images/Login/sign-in-page_14.gif" width="33" height="58" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="58" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img src="images/Login/sign-in-page_15.gif" width="800" height="32" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="32" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/Login/spacer.gif" width="40" height="1" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="1" height="1" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="180" height="1" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="154" height="1" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="392" height="1" alt=""></td>
		<td>
			<img src="images/Login/spacer.gif" width="33" height="1" alt=""></td>
		<td></td>
	</tr>
</table>
              
              
              </center>
      </h:form>
  </body>
</html>
</f:view>