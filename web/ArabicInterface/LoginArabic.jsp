<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<f:view >
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=windows-1256"/>
  <title dir="ltr"><h:outputText value="#{Login_Bean.namesArray[0].titleEnter}"/></title>    
    <link rel="stylesheet" type="text/css" href="../css/loginAr.css"/> 
   
  
  </head>
  <body><h:form >
  
    <center>
                   
      
    



<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="../images/WelcomeArabic/new/arabic_01.jpg" width="800" height="244" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="../images/WelcomeArabic/new/arabic_02.jpg" width="266" height="256" alt=""></td>
		<td colspan="2" width="534" valign="top" height="256">
			
			
		<table cellspacing="0" cellpadding="0" border="0" >
          <tr>
          <td colspan=2  height="20px" align="center" >
          <h:outputLabel style="color:red; font-size: 16px;"  value="#{Login_Bean.error}"/>            </td>
            
          </tr>
          <tr>
            <td align="right"><h:inputText id="userArabic" dir="RTL" lang="AR"  value="#{Login_Bean.userName}" style="width:150px"/></td>
            <td nowrap="nowrap" align="left"><t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].userName}"  /> </td>
          </tr>
          <tr>
          <td  colspan="2"><br></td>
            
          </tr>
          <tr>
            <td align="right">            <h:inputSecret dir="RTL"   value="#{Login_Bean.password}" style="width:150px"/></td>
            <td nowrap="nowrap" align="left"><t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].password}" />           </td>
          </tr>
          
        <tr>
          <td  colspan="2"><br></td>
            
          </tr>
          <tr>
          
            <td colspan=2 align="left" >
            
			  <h:commandLink  action="#{Login_Bean.checkUserInfo}">
                  <h:outputLabel  value="#{Login_Bean.namesArray[0].titleEnter}" style="color: #FD8B3F; font-size: 15pt; font-weight:bold;font-family: Tahoma;"/>
                </h:commandLink> 
              &nbsp;&nbsp;&nbsp;
                <h:commandLink  action="#{Login_Bean.forgetPassAction}">
                  <h:outputLabel  value="#{Login_Bean.namesArray[0].forgetPassword}" style="color: #FD8B3F; font-size: 15pt; font-weight:bold;font-family: Tahoma;"/>
                </h:commandLink>  
                 
                
                
                      
                
                   </td>
                        
                        
                        
          </tr>
          <tr>
          <td  colspan="2"><br></td>
            
          </tr>
          <tr>
          <td  valign="bottom" align="center" colspan="2"><t:commandLink style="font-family: Arial;color:rgb(99,99,99);" action="#{Login_Bean.gotoEnglishLogin}" value="English"/></td>
            
          </tr>
        </table>         
			
			</td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="../images/WelcomeArabic/new/arabic_04.jpg" width="800" height="58" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/new/arabic_05.jpg" width="136" height="42" alt=""></td>
		<td colspan="2" align="center" width="653" height="42" background="../images/WelcomeArabic/new/arabic_06.jpg">
			
			<h:outputLink  style=" text-decoration: none; cursor:pointer;" value="http://www.sandcti.com">
                <h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px;  cursor:pointer ; color:#FCFCFC;" value=" S.A.E. SAND"/>
                </h:outputLink>
                <h:outputLabel style="font-family: Tahoma; font-size: 12px;" value="#{Login_Bean.namesArray[0].logoArabic}"/>
                          
			</td>
		<td>
			<img src="../images/WelcomeArabic/new/arabic_07.jpg" width="11" height="42" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/new/spacer.gif" width="136" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/spacer.gif" width="130" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/spacer.gif" width="523" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/spacer.gif" width="11" height="1" alt=""></td>
	</tr>
</table>


    </center>
   </h:form></body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Welcome_Bean--%>