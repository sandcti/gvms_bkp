<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ page pageEncoding="windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1256"/>
      <title><h:outputText value="#{Login_Bean.namesArray[0].titleUnredeemed}"/></title>
      <script type="text/javascript">
            
            function disp_a(id)
           {
           var param = id.value;
          alert("Value of 'Param' inside the function " + id);
            }
            
            function load()
            {
              
              var yy=document.getElementById('unredeemForm:dataTable1:1:ihidden');
              alert(yy.value);
              var ff=document.getElemenyById('unredeemForm:dataTable1:7:inputTextControlId');
              alert(ff.value);
            }

            </script>
      <link rel="stylesheet" type="text/css" href="../css/orange.css"/>
      <link rel="stylesheet" type="text/css" href="../css/Login.css"/>
    </head>
    <body>
    
    
        <center>
            
          

          
          
          

<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<img src="../images/WelcomeArabic/new/int_arabic_01.jpg" width="251" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/VoucherUnredeemHeader.gif" width="297" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_arabic_03.jpg" width="252" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" valign="top">
		<br>
		<h:form id="secondForm">
			<%@ include  file="/rightMenu.jsp"%>
			</h:form>
			
			
			</td>
		<td colspan="3" width="578" height="416" >			
			
			<h:form id="unredeemedForm">
                  <h:inputHidden value="#{Unredeemed_Bean.usrIdHiden}"/>
                <table border="0" width="91%" height="58">
                  <tr>
                    <td width="43%">&nbsp;</td>
                    <td width="37%" align="right">
                      <h:outputLabel binding="#{Unredeemed_Bean.voucherOutputLabel1}"
                                     id="outputLabel1"
                                     value="#{Unredeemed_Bean.voucher_Number}"/>                    </td>
                    <td width="20%" align="right" colspan="2">
                      <h:outputLabel value=": #{Login_Bean.namesArray[0].voucherNumber_} "
                                     binding="#{Unredeemed_Bean.outputLabel2}"
                                     id="outputLabel2"/>                    </td>
                  </tr>
                  <tr>
				  
                    <td width="43%" align="right">
                   &nbsp;
                       </td>
                    <td align="right" width="17%">
                    <t:outputLabel rendered="#{Unredeemed_Bean.renderGift}" 
                                     value="#{Unredeemed_Bean.giftName}"/>  
                                     
          <h:selectOneMenu rendered="#{!Unredeemed_Bean.renderGift}"  value="#{Unredeemed_Bean.giftName}" id="selectGiftId">
            <f:selectItems value="#{Unredeemed_Bean.gifts}" id="giftItemsId"/>
    	  </h:selectOneMenu>                 
                                         </td>
                   
                    <td align="right" width="20%"><h:outputLabel 
                                     value=": #{Login_Bean.namesArray[0].redeemedGift} "/> </td>
                  </tr>
                  <tr>
                    <td width="43%" align="right">
                     <h:message  for="field1IT" style="color:red;"/>                    </td>
                    <td width="37%" align="right">
                      <h:inputText value="#{Unredeemed_Bean.dialInputText}"
                                   id="field1IT" required="true"
                                   rendered="#{Unredeemed_Bean.renderDialNumber}">
                        <f:converter converterId="dialConverter"/>
                      </h:inputText>                    </td>
                    <td width="20%" align="right">
                      <h:outputLabel value=": #{Login_Bean.namesArray[0].mobileNumber} "
                                     rendered="#{Unredeemed_Bean.renderDialNumber}"
                                     binding="#{Unredeemed_Bean.dialOutputLabel}"
                                     id="field1"/>                    </td>
                  </tr>
                  <tr>
                    <td width="43%" align="right">
                     &nbsp; </td>
                    <td width="37%" align="right">
                      <h:outputLabel style="font-size:13px;" value="#{Login_Bean.namesArray[0].mobileforexample} "
                                     rendered="#{Unredeemed_Bean.renderDialNumber}"                                     
                                     id="fieldForExample"/>                </td>
                    <td width="20%" align="right">
                      &nbsp;</td>
                  </tr>
                </table>
                <hr></hr>
                <table border="0" cellpadding="0" cellspacing="0" width="490"
                       height="69">
                  <tr>
                    <td align="right">
                      <t:dataTable dir="RTL" cellspacing="0" cellpadding="0"
                                   width="100%" value="#{Unredeemed_Bean.dt}"
                                   var="Dtvar" id="dataTable1">
                        <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList-->
                        <t:column width="20%">
                          <h:outputLabel value=" #{Dtvar.arabicValue} : "/>
                        </t:column>
                        <t:column width="60%">
                          <t:inputDate dir="RTL" converter="userConverter" id="dateControlId" rendered="#{Dtvar.renderInputDate}"
                                     type="date"
                                       popupCalendar="true" ampm="true"
                                       value="#{Dtvar.date}">
                            <t:inputHidden rendered="#{Dtvar.renderInputDate}"
                                           id="hiddenDate" value="#{Dtvar.id}"/>
                            <f:converter converterId="userConverter"/>
                          </t:inputDate>
                          <t:inputText style="width:70%" id="inputTextControlId"
                                     
                                       rendered="#{Dtvar.renderInputText}"
                                       value="#{Dtvar.inputValue}">
                                       <f:validator validatorId="userValidator"/>
           <t:inputHidden rendered="#{Dtvar.renderInputText}" id="hiddenText"
                          value="#{Dtvar.id}"/>
           <f:converter converterId="userConverter"/>
                          </t:inputText>
                          <h:inputTextarea style="width:70%;" id="inputTextAreaControlId"                                     
                                           value="#{Dtvar.textValue}"
                                           rendered="#{Dtvar.renderTextarea}">
                                           <f:validator validatorId="userValidator"/>
           <t:inputHidden rendered="#{Dtvar.renderTextarea}" id="hiddenArea"
                          value="#{Dtvar.id}"/>
           <f:converter converterId="userConverter"/>
                          </h:inputTextarea>
                          
                          
                          <h:selectOneMenu style="width:72%" id="selectMenuControlId"
                                           value="#{Dtvar.comboValue}"
                                           rendered="#{Dtvar.renderCombo}">
                            <f:selectItems value="#{Dtvar.compoList}"/>
                            <t:inputHidden rendered="#{Dtvar.renderCombo}" id="hiddenMenu"
                          value="#{Dtvar.id}"/>
           <f:converter converterId="userConverter"/>
                          </h:selectOneMenu>
                        </t:column>
                        <t:column width="20%">
         <h:message style="color:red;" for="selectMenuControlId"/>
         <h:message style="color:red;" for="inputTextAreaControlId"/>
         <h:message style="color:red;" for="inputTextControlId"/>
         <h:message style="color:red;" for="dateControlId"/>
         </t:column>
                      </t:dataTable>
                    </td>
                  </tr>
                </table>
                <table align="center" width="437" height="33">
                  <tr>
                    <td align="center">
                      <t:commandLink action="#{Unredeemed_Bean.redeemAction}">
                        <t:graphicImage border="0" alt=""
                                        url="/images/Buttons/VoucherUnredeemButton.gif"/>
                      </t:commandLink><br>
                      <h:outputLabel value="#{Unredeemed_Bean.redeemFail}"/>
                    </td>
                  </tr>
                </table>
              
              
              </h:form><br>
			
			</td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="../images/WelcomeArabic/new/int_arabic_06.jpg" width="138" height="100" alt=""></td>
		<td colspan="4">
			<img src="../images/WelcomeArabic/new/int_arabic_07.jpg" width="662" height="58" alt=""></td>
	</tr>
	<tr>
		<td align="center" colspan="4" width="662" height="42" background="../images/WelcomeArabic/new/int_arabic_08.jpg">
			
			<%@ include file="/ArabicInterface/arabicUserFooter.jsp" %>
			</td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="138" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="29" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="297" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="252" height="1" alt=""></td>
	</tr>
</table>
          
          
          
        </center>
      </body>
  </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Welcome_Bean--%>