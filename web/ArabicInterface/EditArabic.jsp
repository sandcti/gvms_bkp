<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1256"/>
      <title><h:outputText value="#{Login_Bean.namesArray[0].editInfo}"/></title>
    <style type="text/css">
   a { 
    
    font-family:"Tahoma"; 
	 font-weight:  normal;
	font-size: 16px;
    text-decoration: none;
    cursor: pointer;
	color: blue;	 
}

a:hover { 
	 cursor:pointer ;
	text-decoration: underline ;	
}

.footerPhrase{
	color: #C0C0C0;
	font-family: "Tahoma";
	font-weight: bold;
	font-size: 10px;
}

.ReverseTDDirection{
	text-align: right;
}

.TDDirection{
	float: left;
}

.firstLevel{
	background-color: #F5FFFF;
}

.secondLevel{
	background-color: #DEF6EF;
	
}

.topBar{
	background-image: url(../../Images/English/Login/Bar_Up.gif);
}

.midLevel{
	background-image: url(../../Images/English/Login/BG_Body.gif);
}

.bottomBar{
	background-image: url(../../Images/English/Login/Bar_Down.gif);
}

label{
	color:rgb(99,99,99);
	font-family:"Tahoma";
	font-weight:bold;
	font-size: 13pt;
	
}

.subLabel{
	color: White;
	font-family: "Tahoma";
	font-weight: normal;
	font-size: 12px;
}

.overImage{
	cursor: pointer;
}

div{
	color:rgb(240,187,152);
   font-weight: bold;
	font-family: "Tahoma";
	font-size: 12px;
}

.loginTitle{
	color: #EB6F0A;
	font-family: "Tahoma";
	font-size: 20px;
	font-weight: bold;
}

.bodyTable{
	float: right;
}

.loginButton{
	float:left;
	cursor: pointer;
	background-image: url(../../Images/Icon_Login.gif);
}
   </style> 
   
 
  </head>
  <body><h:form>
    <center><h:inputHidden value="#{Edit_Bean.usrIdHiden}"/>
                   
  
    






<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<img src="../images/WelcomeArabic/new/int_arabic_01.jpg" width="251" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/EditAccountHeader.gif" width="297" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_arabic_03.jpg" width="252" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" valign="top">
		<br>
			<%@ include  file="/rightMenu.jsp"%>
			
			</td>
		<td colspan="3" width="578" height="416" >			
			
			<table width="74%" height="108" border="0" cellpadding="0" cellspacing="0">
						<tr>
						
						<td align="right">
                                                <h:outputLabel style="color:red;" value="#{Edit_Bean.msgWarn1}"/>
                                                <h:outputLabel style="color:red;" value="#{Edit_Bean.msgWarn}"/>
                                                
                                                </td>
						<td>&nbsp;</td>
						</tr>
          <tr>
            <td width="65%" height="27" align="right">
              <h:inputSecret value="#{Edit_Bean.oldPassword}"/>
               </td>
            <td align="center" width="35%">
           
            <h:outputLabel value="#{Login_Bean.namesArray[0].currentPassword_}"/>            </td>
          </tr>
          <tr>
          
          <td width="65%" height="27" align="right">
              <h:inputSecret value="#{Edit_Bean.newPassword}"/>            </td>
            <td align="center" width="35%">
           
            <h:outputLabel value="#{Login_Bean.namesArray[0].newPassword_}"/>           </td>
          </tr>
          <tr>
          <td width="65%" height="27" align="right">
              <h:inputSecret value="#{Edit_Bean.reNewPassword}"/>            </td>
            <td align="center" width="35%">
           
            <h:outputLabel value="#{Login_Bean.namesArray[0].confirmPassword_}"/>           </td>
          </tr>
         <tr>
          <td width="65%"></td>
          <td width="35%">&nbsp;                </td>
         </tr>
		 <tr>
		 
		 <td colspan="2" align="center">
		  <h:commandLink action="#{Edit_Bean.editPassword}">
           <t:graphicImage border="0" alt="" url="/images/Buttons/EditAccountButton.gif"/>
          </h:commandLink>		 </td>
		 </tr>
                 <tr>
                 <td align="right">
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.header}"/>&nbsp;&nbsp;<br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line1}"/><br>                  
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line2}"/><br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line3}"/><br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line4}"/><br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line5}"/><br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line6}"/><br>
              <h:outputText rendered="#{Edit_Bean.displayWarnigsAr}" style="font-size: 12px;" value="#{Edit_Bean.line7}"/>
                 </td>
                 </tr>
                 
        </table><br>
			
			</td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="../images/WelcomeArabic/new/int_arabic_06.jpg" width="138" height="100" alt=""></td>
		<td colspan="4">
			<img src="../images/WelcomeArabic/new/int_arabic_07.jpg" width="662" height="58" alt=""></td>
	</tr>
	<tr>
		<td align="center" colspan="4" width="662" height="42" background="../images/WelcomeArabic/new/int_arabic_08.jpg">
			
			<%@ include file="/ArabicInterface/arabicUserFooter.jsp" %>
			</td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="138" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="29" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="297" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="252" height="1" alt=""></td>
	</tr>
</table>

 

    </center>
   </h:form></body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Welcome_Bean--%>