<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1256"/>
      <title><h:outputText value="#{Login_Bean.namesArray[0].titleHistory}"/></title>
    
   <style type="text/css">
   a { 
    
    font-family:"Thoma"; 
	 font-weight:  normal;
	font-size: 16px;
    text-decoration: none;
    cursor: pointer;
	color: blue;	 
}

a:hover { 
	 cursor:pointer ;
	text-decoration: underline ;	
}

.footerPhrase{
	color: #C0C0C0;
	font-family: Tahoma;
	font-weight: bold;
	font-size: 10px;
}

.ReverseTDDirection{
	text-align: right;
}

.TDDirection{
	float: left;
}

.firstLevel{
	background-color: #F5FFFF;
}

.secondLevel{
	background-color: #DEF6EF;
	
}

.topBar{
	background-image: url(../../Images/English/Login/Bar_Up.gif);
}

.midLevel{
	background-image: url(../../Images/English/Login/BG_Body.gif);
}

.bottomBar{
	background-image: url(../../Images/English/Login/Bar_Down.gif);
}

label{
	color:rgb(99,99,99);
	font-family:"Tahoma";
	font-weight:bold;
	font-size: 13pt;
	
}

.subLabel{
	color: White;
	font-family: Tahoma;
	font-weight: normal;
	font-size: 12px;
}

.overImage{
	cursor: pointer;
}

div{
	color:rgb(240,187,152);
   font-weight: bold;
	font-family: Tahoma;
	font-size: 12px;
}

.loginTitle{
	color: #EB6F0A;
	font-family: Tahoma;
	font-size: 20px;
	font-weight: bold;
}

.bodyTable{
	float: right;
}

.loginButton{
	float:left;
	cursor: pointer;
	background-image: url(../../Images/Icon_Login.gif);
}
   </style>
  
  </head>
  <body><h:form>
    <center><h:inputHidden value="#{History_Bean.usrIdHiden}"/>
                   
      
     







<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<img src="../images/WelcomeArabic/new/int_arabic_01.jpg" width="251" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/HistoryHeader.gif" width="297" height="84" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_arabic_03.jpg" width="252" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" valign="top">
		<br>
			<%@ include  file="/rightMenu.jsp"%>
			
			</td>
		<td colspan="3" width="578" height="416" >			
			
			 <table  border="0" cellpadding="0" cellspacing="0">
          <tr>
            
            <td width="76%" align="center">
            <t:inputDate type="date" popupCalendar="true"
                        ampm="true" value="#{History_Bean.filterFromDate}"/>
            </td>
            
            <td width="24%" align="left">
           <h:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].from}  "/>
          </td>
            </tr>
            <tr>
           
          <td align="center">
            <t:inputDate type="date" popupCalendar="true"
                        ampm="true" value="#{History_Bean.filterToDate}"/>
            </td>
            
             <td align="left">
           <h:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].to}  "/>
          </td>
            </tr>
            <tr>
              <td colspan="2" align="center">&nbsp;
            
              </td>
            </tr><tr>
              <td colspan="2" align="center">
            <t:commandLink action="#{History_Bean.goFilter_Action}">
            <t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].search}"/>
            </t:commandLink>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">&nbsp;
            
              </td>
            </tr>
            
          <tr>
            <td height="27" colspan="2" align="center">
           <h:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].titleHistory}"/>
                        </td>
            </tr>
            
            <tr>
            <td align="center" colspan="2" valign="top">
            
            
            <h:dataTable rowClasses="row1,row2"
                     value="#{History_Bean.historyArray}" var="Dtvar"
                     binding="#{History_Bean.dataTable1}" id="dataTable1"
                     rows="#{History_Bean.rowCount}" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
         
         
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getDialNumber" />
           <h:outputLabel style="font-family: Tahoma;color:rgb(0,0,0); font-size:small;" value="#{Login_Bean.namesArray[0].phoneNumber}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.dialNumber}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getVoucherDate" />
           <h:outputLabel style="font-family: Tahoma;color:rgb(0,0,0); font-size:small;" value="#{Login_Bean.namesArray[0].redeemtionDate}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.voucherDate}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftName" />
           <h:outputLabel style="color:rgb(0,0,0);font-family: Tahoma; font-size:small;" value="#{Login_Bean.namesArray[0].gift}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         
         <f:facet name="footer">
          <h:panelGroup>
           <t:commandLink  action="#{History_Bean.pageFirst}"
                            disabled="#{History_Bean.dataTable1.first == 0}">
          <t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].first}"/>
                            </t:commandLink>
           <t:commandLink  action="#{History_Bean.pagePrevious}"
                            disabled="#{History_Bean.dataTable1.first == 0}">
                            <t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].previous}"/>
                            </t:commandLink>
           <t:commandLink   action="#{History_Bean.pageNext}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}">
                            <t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].next}"/>
                            </t:commandLink>
           <t:commandLink   action="#{History_Bean.pageLast}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}">
                            <t:outputLabel style="font-family: Tahoma;" value="#{Login_Bean.namesArray[0].last}"/>
                            </t:commandLink>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>            </td>
          </tr>
         
        
         
        </table><br>
			
			</td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="../images/WelcomeArabic/new/int_arabic_06.jpg" width="138" height="100" alt=""></td>
		<td colspan="4">
			<img src="../images/WelcomeArabic/new/int_arabic_07.jpg" width="662" height="58" alt=""></td>
	</tr>
	<tr>
		<td align="center" colspan="4" width="662" height="42" background="../images/WelcomeArabic/new/int_arabic_08.jpg">
			
			<%@ include file="/ArabicInterface/arabicUserFooter.jsp" %>
			</td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="138" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="29" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="297" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/new/int_spacer.gif" width="252" height="1" alt=""></td>
	</tr>
</table>



    </center>
   </h:form></body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Welcome_Bean--%>