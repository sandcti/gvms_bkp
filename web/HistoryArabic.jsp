<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1256"/>
      <title><h:outputText value="#{Login_Bean.namesArray[0].titleHistory}"/></title>
    <link rel="stylesheet" type="text/css" href="../css/orange.css"/> 
    <link rel="stylesheet" type="text/css" href="../css/Login.css"/>
   
  
  </head>
  <body><h:form>
    <center><h:inputHidden value="#{History_Bean.usrIdHiden}"/>
                   
      
     <table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="../images/WelcomeArabic/Welcome_01.gif" width="800" height="48" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/Welcome_02.gif" width="39" height="75" alt=""></td>
		<td colspan="3">
			
                        
<table id="Table_01" width="729" height="75" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="../images/WelcomeArabic/HistoryHeader.gif" width="550" height="75" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/mobinilLogoArabic.gif" width="180" height="75" alt=""></td>
	</tr>
</table>
                        
                        </td>
		<td colspan="2">
			<img src="../images/WelcomeArabic/Welcome_04.gif" width="32" height="75" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/Welcome_05.gif" width="39" height="384" alt=""></td>
		<td colspan="2" background="../images/WelcomeArabic/Welcome_06.gif">
                <table width="100%" height="199" border="0" cellpadding="0" cellspacing="0">
          <tr>
            
            <td width="76%" align="center">
            <t:inputDate type="date" popupCalendar="true"
                        ampm="true" value="#{History_Bean.filterFromDate}"/>
            </td>
            
            <td width="24%" align="left">
           <h:outputLabel value="#{Login_Bean.namesArray[0].from}  "/>
          </td>
            </tr>
            <tr>
           
          <td align="center">
            <t:inputDate type="date" popupCalendar="true"
                        ampm="true" value="#{History_Bean.filterToDate}"/>
            </td>
            
             <td align="left">
           <h:outputLabel value="#{Login_Bean.namesArray[0].to}  "/>
          </td>
            </tr>
            <tr>
              <td colspan="2" align="center">&nbsp;
            
              </td>
            </tr><tr>
              <td colspan="2" align="center">
            <t:commandLink action="#{History_Bean.goFilter_Action}">
            <t:outputLabel value="#{Login_Bean.namesArray[0].search}"/>
            </t:commandLink>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">&nbsp;
            
              </td>
            </tr>
            
          <tr>
            <td height="27" colspan="2" align="center">
           <h:outputLabel value="#{Login_Bean.namesArray[0].titleHistory}"/>
                        </td>
            </tr>
            
            <tr>
            <td align="center" colspan="2" valign="top">
            
            
            <h:dataTable rowClasses="row1,row2"
                     value="#{History_Bean.historyArray}" var="Dtvar"
                     binding="#{History_Bean.dataTable1}" id="dataTable1"
                     rows="#{History_Bean.rowCount}" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
         
         
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getDialNumber" />
           <h:outputLabel style="color:rgb(0,0,0); font-size:small;" value="#{Login_Bean.namesArray[0].phoneNumber}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.dialNumber}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getVoucherDate" />
           <h:outputLabel style="color:rgb(0,0,0); font-size:small;" value="#{Login_Bean.namesArray[0].redeemtionDate}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.voucherDate}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftName" />
           <h:outputLabel style="color:rgb(0,0,0); font-size:small;" value="#{Login_Bean.namesArray[0].gift}"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
         
         <f:facet name="footer">
          <h:panelGroup>
           <t:commandLink  action="#{History_Bean.pageFirst}"
                            disabled="#{History_Bean.dataTable1.first == 0}">
          <t:outputLabel value="#{Login_Bean.namesArray[0].first}"/>
                            </t:commandLink>
           <t:commandLink  action="#{History_Bean.pagePrevious}"
                            disabled="#{History_Bean.dataTable1.first == 0}">
                            <t:outputLabel value="#{Login_Bean.namesArray[0].previous}"/>
                            </t:commandLink>
           <t:commandLink   action="#{History_Bean.pageNext}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}">
                            <t:outputLabel value="#{Login_Bean.namesArray[0].next}"/>
                            </t:commandLink>
           <t:commandLink   action="#{History_Bean.pageLast}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}">
                            <t:outputLabel value="#{Login_Bean.namesArray[0].last}"/>
                            </t:commandLink>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>            </td>
          </tr>
         
        
         
        </table>
                
			</td>
		<td align="center" valign="top" background="../images/WelcomeArabic/Welcome_07.gif">
         
         <table >
                       <tr valign="top">
          <td height="82" valign="top">
           <div align="center">
            <p>
             <h:outputLabel style="font-size: 18px;" value="#{Login_Bean.namesArray[0].welcome}"/>
            </p><p>
             <h:outputLabel style="font-size: 16px;" value="#{Welcome_Bean.userNameValue}"
                           
                           id="userNameLbl"/>
            </p>
           </div></td>
         </tr>
         <tr>
          <td>
           <div align="center">
            <h:graphicImage height="3" width="145"
                            url="/images/Welcome/breaklines.jpg"
                            id="graphicImage1"/>
           </div>          </td>
         </tr>
         <tr>
          <td>
           <div align="center">
           <t:commandLink action="#{Login_Bean.searchAr}">
           <h:outputLabel value="#{Login_Bean.namesArray[0].search}"/>
            </t:commandLink>
           </div></td>
         </tr>
         <tr>
          <td>
           <div align="center">
            <h:graphicImage height="3" width="145"
                            url="/images/Welcome/breaklines.jpg"
                            id="graphicImage2"/>
           </div>          </td>
         </tr>
         <tr>
          <td>
          <div align="center">
           <t:commandLink action="#{Login_Bean.historyAr}">
            <h:outputLabel value="#{Login_Bean.namesArray[0].history}"/>
            </t:commandLink>
           </div></td>
         </tr>
         <tr>
          <td>
           <div align="center">
            <h:graphicImage height="3" width="145"
                            url="/images/Welcome/breaklines.jpg"
                            id="graphicImage3"/>
           </div>          </td>
         </tr>
         <tr>
          <td>
           <div align="center">
           <t:commandLink action="#{Login_Bean.accountAr}">
            <h:outputLabel value="#{Login_Bean.namesArray[0].account}"/>
            </t:commandLink>
           </div></td>
         </tr>
         <tr>
          <td>
           <div align="center">
            <h:graphicImage height="3" width="145"
                            url="/images/Welcome/breaklines.jpg"
                            id="graphicImage4"/>
           </div>          </td>
          </tr>
                <tr>
                  <td>
                  
                  <div align="center">
           <t:commandLink action="#{Login_Bean.exitAr}">           
           <h:outputLabel value="#{Login_Bean.namesArray[0].exit}"/>  
           </t:commandLink>
           </div></td>
                </tr>
            </table>
         
                
			</td>
		<td colspan="2">
			<img src="../images/WelcomeArabic/Welcome_08.gif" width="32" height="384" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img src="../images/WelcomeArabic/Welcome_09.gif" width="800" height="7" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="../images/WelcomeArabic/Welcome_10.gif" width="42" height="58" alt=""></td>
		<td colspan="3" background="../images/WelcomeArabic/Welcome_11.gif">
			<%@ include file="/ArabicInterface/arabicUserFooter.jsp" %> 
                        
          </td>
		<td>
			<img src="../images/WelcomeArabic/Welcome_12.gif" width="31" height="58" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img src="../images/WelcomeArabic/Welcome_13.gif" width="800" height="28" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="3" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="546" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="180" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="1" height="1" alt=""></td>
		<td>
			<img src="../images/WelcomeArabic/spacer.gif" width="31" height="1" alt=""></td>
	</tr>
</table>
    </center>
   </h:form></body>
 </html>
</f:view>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:Welcome_Bean--%>