<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
            <link rel="stylesheet" type="text/css" href="css\orangeEn.css"/> 
      <title>Search</title>
     
  
    </head>
    <body><h:form>
        
         <center><h:inputHidden value="#{Search_Bean.usrIdHiden}"/>
     



<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/Search-final_04.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<table width="100%">
         <%@ include file="/leftMenu.jsp" %>
         <tr>
          <td>
          <div align="center" >
           
            <h:commandLink style="font-family:Arial;"   action="#{Login_Bean.exitEn}">
           <f:verbatim>Logout</f:verbatim>          
           
           </h:commandLink></div>
           </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
         </tr>
         
        </table>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			
			
			<table width="100%" height="205" border="0" cellpadding="0" cellspacing="0">
         
          <tr>
            <td height="53" align="right" valign="top" >
           
            <h:outputLabel value="Voucher number : " />&nbsp;            </td>
            <td align="left" valign="top" style=" width : 311px;">
              <h:inputText value="#{Search_Bean.voucherNumber}" style=" width : 139px;"/>            </td>
          </tr>
           <tr>
            
            <td align="center" colspan=2 valign="bottom">
              <t:outputText style="color:red;"  value="#{Search_Bean.error}"/>           
              <t:outputText style="color:red;"  binding="#{Search_Bean.errorEn}"/>          
              </td>
          </tr>
         
          <tr>
            
            <td align="center" colspan=2 valign="bottom">
              <br>          
              </td>
          </tr>
         
         <tr>
          <td height="80" valign="top" colspan="2" align="center">

              <h:commandLink action="#{Search_Bean.search}">
                <t:graphicImage alt="" border="0" url="/images/Buttons/search.gif"/>
                </h:commandLink>
			</td>
          </tr>
        </table>
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>


    </center>
      </h:form></body>
  </html>
</f:view>