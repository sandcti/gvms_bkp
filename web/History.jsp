<%@page errorPage="/error.jsp"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:view>
  <html>
    <head>
      <meta http-equiv="Content-Type"
            content="text/html; charset=windows-1252"/>
            <link rel="stylesheet" type="text/css" href="css\orangeEn.css"/> 
      <title>History</title>
          
    </head>
    <body><h:form>
        
         <center><h:inputHidden value="#{History_Bean.usrIdHiden}"/>
    








<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/Welcome/new/internal-english_01.gif" width="84" height="84" alt=""></td>
		<td colspan="3">
			<img src="images/Welcome/new/history-of-final_04.gif" width="512" height="84" alt=""></td>
		<td colspan="2">
			<img src="images/Welcome/new/internal-english_03.gif" width="204" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" valign="top" width="222" height="414">
			<br>
			<table width="100%">
         <%@ include file="/leftMenu.jsp" %>
         <tr>
          <td>
          <div align="center" >
           
            <h:commandLink style="font-family:Arial;"   action="#{Login_Bean.exitEn}">
           <f:verbatim>Logout</f:verbatim>          
           
           </h:commandLink></div>
           </td>
         </tr>
         <tr>
          <td>&nbsp;</td>
         </tr>
         
        </table>
			</td>
		<td colspan="3" align="center" width="578" height="414">
			
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
          <tr>
            <td>&nbsp;</td>
            <td>
              
            </td>
          </tr>
          <tr>
            <td align="right">
           <h:outputLabel value="From  "/>
          </td>
            <td align="center">
            <t:inputDate type="date" popupCalendar="true"
                         ampm="false" value="#{History_Bean.filterFromDate}" />
            </td>
            
            </tr>
            <tr>
            <td align="right">
           <h:outputLabel value="To  "/>
          </td>
          <td align="center">
            <t:inputDate type="date" popupCalendar="true"
                        ampm="true" value="#{History_Bean.filterToDate}" />
            </td>
            </tr>
            <tr>
              <td colspan="2" align="center">
            &nbsp;
              </td>
            </tr><tr>
              <td colspan="2" align="center">
            <t:commandButton value="Filter" action="#{History_Bean.goFilter_Action}"/>
              </td>
            </tr>
            
            <tr>
              <td colspan="2" align="center">
            <t:outputLabel style="color:red;" value="#{History_Bean.msgWarn}"/>
              </td>
            </tr>
          <tr>
            <td align="center" colspan="2">
           
            <h:outputLabel value="Redemption History Table"/>
           
            </td>
            </tr>
            
            <tr>
            <td align="center" colspan="2">
                
        <h:dataTable rowClasses="row1,row2"
                     value="#{History_Bean.historyArray}" var="Dtvar"
                     binding="#{History_Bean.dataTable1}" id="dataTable1"
                     rows="100" border="1"
                     style="text-align:center; vertical-align:middle;">
         <!--oracle-jdev-comment:Faces.RI.DT.Class.Key:java.util.ArrayList<Admin.View.campaignTableClass>-->
         
         
         <h:column>
          <f:facet name="header">
           <h:commandLink actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getDialNumber" />
           <h:outputLabel style="color:rgb(0,0,255); font-size:small; text-decoration:underline;" value="Dial Number"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.dialNumber}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getVoucherDate" />
           <h:outputLabel style="color:rgb(0,0,255); font-size:small; text-decoration:underline;" value="Redemption Date"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.voucherDate}"/>
         </h:column>
         <h:column>
          <f:facet name="header">
           <h:commandLink  actionListener="#{History_Bean.sortDataTable}">
           <f:attribute name="sortField" value="getGiftName" />
           <h:outputLabel style="color:rgb(0,0,255); font-size:small; text-decoration:underline;" value="Gift Name"/>
           </h:commandLink>
          </f:facet>
          <h:outputText value="#{Dtvar.giftName}"/>
         </h:column>
                
         
         <f:facet name="footer">
          <h:panelGroup>
           <h:commandButton value="First" action="#{History_Bean.pageFirst}"
                            disabled="#{History_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Prev" action="#{History_Bean.pagePrevious}"
                            disabled="#{History_Bean.dataTable1.first == 0}"/>
           <h:commandButton value="Next" action="#{History_Bean.pageNext}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}"/>
           <h:commandButton value="Last" action="#{History_Bean.pageLast}"
                            disabled="#{History_Bean.dataTable1.first + History_Bean.dataTable1.rows>= History_Bean.dataTable1.rowCount}"/>
          </h:panelGroup>
         </f:facet>
        </h:dataTable>
        
        <h:inputHidden value="#{History_Bean.sortField}"/>
        <h:inputHidden value="#{History_Bean.sortAscending}"/>
        
            </td>
          </tr>
          <tr>
            <td>
           
            </td>
            <td>&nbsp;</td>
          </tr>
         <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
         </tr>
         <tr>
          <td height="105">&nbsp;</td>
          <td height="105">&nbsp;</td>
         </tr>
         <tr>
          <td height="51">&nbsp;</td>
         
         </tr>
         <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
         </tr>
        </table>
			
        </td>
	</tr>
	<%@ include file="/Userfooter.jsp" %>
	<tr>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="84" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="87" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="374" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="197" height="1" alt=""></td>
		<td>
			<img src="images/Welcome/new/spacer.gif" width="7" height="1" alt=""></td>
	</tr>
</table>



    </center>
      </h:form></body>
  </html>
</f:view>