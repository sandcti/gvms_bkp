<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page errorPage="/error.jsp"%>
<%@ page pageEncoding="utf-8" %>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<f:view>

  <html>
  <head>
  <!--
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1256"></meta>
  -->
    
    <link rel="stylesheet" type="text/css" href="css\Login.css"/> 
    
    <title>Login</title>
  
  </head>
  <body>
  <h:form id="LoginForm"><center>
  
 
				<table id="Table_01" width="800" height="601" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="images/Login/new/english_01.gif" width="800" height="248" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="images/Login/new/english_02.gif" width="282" height="252" alt=""></td>
		<td colspan="2" valign="top" align="left" width="518" height="252">
			
			<table border="0" cellpadding="0" cellspacing="0">
          <tr>
            
            <td colspan=2 height="20px" align="center" ><h:outputLabel style="color:red; font-size:12px;"  value="#{Login_Bean.error}"/>            </td>
          </tr>
          <tr>
            <td nowrap="nowrap" align="right"><h:outputLabel value="User Name:" />            </td>
            <td align="left"><h:inputText  value="#{Login_Bean.userName}" style="width:150px" />            </td>            
          </tr>
          <tr>            
            <td colspan="2" align="left"><br></td>            
          </tr>
          <tr>
            <td align="right"><h:outputLabel  value="Password:" />            </td>
            <td align="left"><h:inputSecret  value="#{Login_Bean.password}" style="width:150px"/>            </td>
          </tr>
           <tr>            
            <td colspan="2" align="left"><br></td>            
          </tr>
          
          <tr>
            
            <td align="center" colspan=2>
            
              
              <h:commandLink action="#{Login_Bean.checkUserInfo}">
                  <h:outputLabel value="Login" style=" font-size: 13pt; font-weight:bold;color: #FD8B3F;"/>
                </h:commandLink>
                &nbsp;&nbsp;&nbsp;
              <h:commandLink action="#{Login_Bean.forgetPassAction}">
                  <h:outputLabel value="Forgot password?" style=" font-size: 13pt; font-weight:bold;color: #FD8B3F;"/>
                </h:commandLink>
                <br>
                <br>    
                <h:commandLink  action="#{Login_Bean.gotoArabicLogin}" > 
                <h:outputLabel value="#{Login_Bean.namesArray[0].arabic}" style= "font-family:Tahoma;"/>
        </h:commandLink>
                </td>
          </tr>
        </table>
			
			
			</td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="images/Login/new/english_04.gif" width="800" height="55" alt=""></td>
	</tr>
	<tr>
		<td valign="top">
			<img src="images/Login/new/english_05.gif" width="134" height="45" alt=""></td>
		<td colspan="2" align="center" valign="middle" style=" background-repeat:no-repeat;" background="images/Login/new/english_06.gif">			
			<h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px; color:#FCFCFC;" value="#{Login_Bean.namesArray[0].logoYear}"/>
			<br>
		<h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px; color:#FCFCFC;" value="GVMS Developed By"/>
                
                <h:outputLink  style=" text-decoration: none; cursor:pointer;" value="http://www.sandcti.com">
                <h:outputLabel style="font-family: Arial; font-weight: bold; font-size: 12px;  cursor:pointer ; color:#FCFCFC;" value="SAND S.A.E."/>
                </h:outputLink>
			<br>
			</td>
		<td valign="top">
			<img src="images/Login/new/english_07.gif" width="12" height="45" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/Login/new/spacer.gif" width="134" height="1" alt=""></td>
		<td>
			<img src="images/Login/new/spacer.gif" width="148" height="1" alt=""></td>
		<td>
			<img src="images/Login/new/spacer.gif" width="506" height="1" alt=""></td>
		<td>
			<img src="images/Login/new/spacer.gif" width="12" height="1" alt=""></td>
	</tr>
</table>
		
			
              
              
              </center>
      </h:form>
  </body>
</html>
</f:view>