
/**
 * Class def : contain methods that make CS to update his profile
 * 
 * #Class name in facesconfig.xml
 * CSProfile_Bean
 * 
 * #Class main variables * 
*  userName: CS user name
* password : CS password that will change
* confirmPassword : CS confirmantion password that will change
* email : CS email
* phone : CS phone
* error : output label demonstrate any error in input fields
* CSName,name : Stirng of CS Name.
 * csInfo : object of logged in user from session
 *  
 * @author Mahmoud Abdel aal 
 */


package com.mobinil.gvms.system.cs.beans;

import java.sql.SQLException;

import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.MD5Class;
import com.mobinil.gvms.system.utility.SecurityUtils;
import com.mobinil.gvms.system.utility.jsfUtils;

public class UpdateProfile {
public String userName;
public String password;
public String confirmPassword;
public String name;
public String email;
public String phone;
public String error;
public String CSName;

CSInfo csInfo =(CSInfo)jsfUtils.getFromSession("csInfo");


/**
 * constructor used to prepare data of logged in cs from session 
 */
public UpdateProfile(){
	HiddenBean.checkUserInSession();
	userName = csInfo.getUserName();
	name = csInfo.getFullName();
	email = csInfo.getUserEmail();
	phone = csInfo.getUserPhone();
	CSName = csInfo.getFullName();
}

/**
 * constructor used to prepare data of logged in cs from session 
 */
public String saveAction(){
    
	if (userName==null||userName.compareTo("")==0)
	{
		setError("please provide your user name.");             
        return null;
	}
	if (name==null||name.compareTo("")==0)
	{
		setError("please provide your name.");             
        return null;
	}
	if (email==null||email.compareTo("")==0)
	{
		setError("please provide your email.");             
        return null;
	}
	if (phone==null||phone.compareTo("")==0)
	{
		setError("please provide your phone.");             
        return null;
	}
	if (password==null||password.compareTo("")==0 )password = "";
	
	
	
          ////System.out.println(" step 1");
        try
          {
            String passEncripted = "";
            String SecurityError ="";  
            	if (password.compareTo("")!=0 )            		
            	{
            		//////System.out.println(" step 2 password is "+password );
            		passEncripted = MD5Class.MD5(password);
            		if (password.compareTo(confirmPassword)==0 )
            		{
            		//////System.out.println(" step 3 password "+password+" confirmPassword "+confirmPassword);
            		SecurityError = SecurityUtils.checkPass(password, passEncripted, 
            		csInfo.getUserName().trim(), 
            		csInfo.getSysUserId() + "");
            		//////System.out.println(" step 4 SecurityError is "+SecurityError);
            		}
            		else
            		{
            			//////System.out.println(" step 5");
            			setError("Password and confirmation must be equivalent.");             
                        return null;
            		}
            	}
            	//////System.out.println(" step 6 SecurityError is "+SecurityError);
            if (SecurityError.compareTo("")!=0)
            {
             setError(SecurityError);             
             return null;
            }
            else
            {
            	//////System.out.println(" step 7 ");
            	//////System.out.println(String.valueOf(csInfo.getSysUserId())+" "+ String.valueOf(csInfo.getUserId())+" "+ passEncripted+" "+ userName+" "+  email+" "+ phone+" "+ name+" "+name);
            	boolean flag = CSDao.updateCSInfo(String.valueOf(csInfo.getSysUserId()), String.valueOf(csInfo.getUserId()), passEncripted, userName,  email, phone, name,name );
            	//////System.out.println(" step 8 flag is "+flag);
             if(flag)
             {           
            	 ////System.out.println(" step 9");
             setError("Your new password was updated to your account.");             
             return null;
             }
             return null;
            }


          }
        catch (Exception e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);        	return null;
          }
        

	
}



public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getConfirmPassword() {
	return confirmPassword;
}
public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public void setError(String error) {
	this.error = error;
}
public String getError() {
	return error;
}
public void setCSName(String cSName) {
	CSName = cSName;
}
public String getCSName() {
	return CSName;
}


}
