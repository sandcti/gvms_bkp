
/**
 * Class def : contain methods that make CS to generate voucher to telephony customer
 * 
 * #Class name in facesconfig.xml
 * CSVoucher_Bean
 * 
 * #Class main variables * 
 * campaigns : select control which has only dialed and generated voucher status.
 * gifts : select control which has gifts per selected campaign.
 * channels : select control which has channel per selected campaign.
 * subChannels : select control which has sub channel per selected channel.
 * 
 * selectedCampaign : variable for selected campaign. 
 * selectedGift : variable for selected gift
 * selectedChannel : variable for selected channel
 * selectedSubChannel : variable for selected sub channel
 * accountNumber : variable for account number.
 * dialNumber : variable for customer dial number
 * campaignAndGifts : hashmap contains every dialed and generated voucher campaign and it's gifts
 * 
 * error : output label demonstrate any error in input fields
 * countOfGeneration : string for number of generation vouchers.
 * CSName : Stirng of CS Name.
 * disableGiftAndChannel : flag to disable and enable gift and channel select controls. 
 * disableSubChannel : flag to disable and enable sub channel select controls.
 * csInfo : object of logged in user from session
 *  
 * @author Mahmoud Abdel aal 
 */

package com.mobinil.gvms.system.cs.beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.cs.dto.CSLogsDTO;
import com.mobinil.gvms.system.utility.jsfUtils;

public class GenerateVoucher {
	//CSVoucher_Bean
	public ArrayList<SelectItem> campaigns = new ArrayList<SelectItem>();	
	public ArrayList<SelectItem> gifts = null;
	public ArrayList<SelectItem> channels = null;
	public ArrayList<SelectItem> subChannels = null;
	public String selectedCampaign;
    public String selectedGift;
    public String selectedChannel;
    public String selectedSubChannel;
    public String accountNumber;
    public String dialNumber;
    public HashMap campaignAndGifts;
      
	public String error;
	
	public String countOfGeneration;
	public String doFunctionGetCampaignGift;
	public String CSName;
	
	CSInfo csInfo =(CSInfo)jsfUtils.getFromSession("csInfo");
	
	public boolean disableGiftAndChannel;
	public boolean disableSubChannel;
	
	
/**
 * Constructor used to prepare campaigns and intilization objects.
 *  
 * */
	public GenerateVoucher(){
		
		HiddenBean.checkUserInSession();
		
		gifts = new ArrayList<SelectItem>();
//		gifts.add(0, new SelectItem("-1","*"));
		
		channels = new ArrayList<SelectItem>();
//        channels.add(0, new SelectItem("-1","*"));
        
        subChannels = new ArrayList<SelectItem>();
//        subChannels.add(0, new SelectItem("-1","*"));
		
        disableGiftAndChannel = true;
        disableSubChannel = true;
        
		
		try {    
	         campaignAndGifts = CSDao.getAllCampaign("home");
	      } catch (SQLException e) {
	         
	         new com.mobinil.gvms.system.utility.PrintException().printException(e);
	      } 
		
		CSName = csInfo.getFullName();
		
		campaigns = (ArrayList <SelectItem>)campaignAndGifts.get ( "all" );
		
	}

	/**
	 * Get campaign's gifts per selected campaign
	 * @return null == void
	 * */
	public String getCampaigGift(){
	   gifts=null;
		gifts = new ArrayList<SelectItem>();
      
      //System.out.println("here in getCampaigGift");
      if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
      {
      	selectedCampaign="0";
      }
      //System.out.println ("selected campaign id is "+ selectedCampaign);

      HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
      if (ff!=null)
      gifts = (ArrayList <SelectItem>)(ff).get ( "gifts" );
      
      getCampaigChannel();
   
   return null;
 }
	/** Get campaign's channels per selected campaign 
    * */
  public void getCampaigChannel(){
     channels = new ArrayList<SelectItem>();
      
      
      if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
      {
          selectedCampaign="0";
      }
      
      HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
      
      if (ff!=null){      
      channels = (ArrayList <SelectItem>)(ff).get ( "channels" );
      disableGiftAndChannel = false;
      disableSubChannel = true;
      }
      else{
         disableGiftAndChannel = true;
         disableSubChannel = true;
      }
      

   
   
 } 
  /** Get channel's sub channels per selected channel 
   * */
  public void getSubChannel(){
     subChannels = new ArrayList<SelectItem>();
     disableGiftAndChannel = false;
     
     if (selectedChannel!=null && selectedChannel.compareTo ( "-1" )!=0){
         try
        {
           subChannels = CSDao.getSubChannel ( selectedChannel,"home" );
           disableSubChannel = false;
        }
        catch ( SQLException e )
        {new com.mobinil.gvms.system.utility.PrintException().printException(e);
           subChannels.add(0, new SelectItem("-1","error"));
           disableSubChannel = true;
        }
     }
   
 }
	
	public void testGifts(String diffrance){
	  for ( int i = 0 ; i < gifts.size ( ) ; i ++ )
	  {
	     SelectItem sOne = gifts.get ( i ) ;
	     //System.out.println (diffrance+" label is "+sOne.getLabel ( )+" value is "+sOne.getValue ( ));
      } 
	}
	
	/**
	 * method of "Send Voucher" button. 
	 * @return String error message or sucessfully generation
	 * @see CSDao#insertDialGenerate(int, String, String, String, String, String, String, int);
	 */
	
	public String generateVouchers(){
		
		if (selectedCampaign.compareTo("-1")==0)
		{
			error = "please select campaign.";
			return null;	
		}
		if (selectedGift==null || selectedGift.compareTo("-1")==0||selectedGift.compareTo("")==0)
		{
			error = "please select gift.";
			return null;	
		}
		if (selectedChannel==null ||selectedChannel.compareTo("-1")==0||selectedChannel.compareTo("")==0)
        {
		   
            error = "please select channel.";
            return null;    
        }else
        {
           if (selectedSubChannel==null ||selectedSubChannel.compareTo("-1")==0||selectedSubChannel.compareTo("")==0)
           {
               error = "please select sub channel.";
               return null;    
           }
        }
		
		
		if (dialNumber==null ||dialNumber.compareTo("")==0)
		{
			error = "please type dial number.";
			return null;	
		}
		else
		{
		   if (dialNumber.length ( )<9)
		   {
		      error = "please enter a valid dial number.";
	            return null;
		   }
		}
//		if (accountNumber==null ||accountNumber.compareTo("")==0)
//        {
//            error = "please type account number.";
//            return null;    
//        }
//		else  based on haseb requirement to unvalidate account number
//		{
//		   String errorMsg = jsfUtils.isValidAccount ( accountNumber );
//		   if (errorMsg.compareTo ( "" )!=0)
//		   {
//		      error = errorMsg;
//		      return null;
//		   }
//		}  
		
//		if (countOfGeneration==null ||countOfGeneration.compareTo("")==0)
//		{
//			error = "please type voucher count.";
//			return null;	
//		}
//		
//			tempOfCountOfGeneration = Integer.parseInt(countOfGeneration);
//			if (tempOfCountOfGeneration<1){
//				error = "Invalid voucher count. the count must be greater than 0.";
//				return null;	
//			}

			int countOfInsertStatments = 0;
			try {
				countOfInsertStatments = CSDao.insertDialGenerate(/*tempOfCountOfGeneration*/1,selectedCampaign,dialNumber,selectedGift,selectedChannel,selectedSubChannel,accountNumber,csInfo.getSysUserId());
				if (countOfInsertStatments==-2)
				{
					error = "Voucher generation complete successfully and system fail to send sms.";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				new com.mobinil.gvms.system.utility.PrintException().printException(e);
			}
			
			if (countOfInsertStatments>0)
			{  
			error = "Voucher generation complete successfully.";
			}
			return null;

		
		
		
		
	}
	
	public String getLabelFromSelectItemsArray(ArrayList<SelectItem> selectedArrray,String itemValue){
	   for ( int i = 0 ; i < selectedArrray.size ( ) ; i ++ )
      {
	      SelectItem si = selectedArrray.get ( i );
	      if (((String)si.getValue ( )).compareTo ( itemValue )==0)return si.getLabel ( );
      }
	   return "";
	   
	}
	
	public ArrayList<SelectItem> getCampaigns() {
		return campaigns;
	}
	public void setCampaigns(ArrayList<SelectItem> campaigns) {
		this.campaigns = campaigns;
	}
	public ArrayList<SelectItem> getGifts() {
		return gifts;
	}
	public void setGifts(ArrayList<SelectItem> gifts) {
		this.gifts = gifts;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setSelectedCampaign(String selectedCampaign) {
		this.selectedCampaign = selectedCampaign;
	}

	public String getSelectedCampaign() {
		return selectedCampaign;
	}

	public void setSelectedGift(String selectedGift) {
		this.selectedGift = selectedGift;
	}

	public String getSelectedGift() {
		return selectedGift;
	}

	public void setDialNumber(String dialNumber) {
		this.dialNumber = dialNumber;
	}

	public String getDialNumber() {
		return dialNumber;
	}

	public void setCountOfGeneration(String countOfGeneration) {
		this.countOfGeneration = countOfGeneration;
	}

	public String getCountOfGeneration() {
		return countOfGeneration;
	}
	public String getDoFunctionGetCampaignGift() {
//		getCampaigGift();
		//System.out.println("here in get");
		return doFunctionGetCampaignGift;
	}

	public void setDoFunctionGetCampaignGift(String doFunctionGetCampaignGift) {
		//System.out.println("here in set");
		this.doFunctionGetCampaignGift = doFunctionGetCampaignGift;
	}

	public void setCSName(String cSName) {
		CSName = cSName;
	}

	public String getCSName() {
		return CSName;
	}
//public static void main(String[] args)
//{
//   ArrayList<SelectItem> ss = new ArrayList<SelectItem>();
//   SelectItem dd =new SelectItem("1","Ah");
//   
//   
//   
//   ss.add ( new SelectItem("0","Ahmed") );
//   ss.add (dd );
//   ss.add ( new SelectItem("2","Ahm") );
//   ss.add ( new SelectItem("3","Ahme") );
//   //System.out.println ("contains result is " +  );
//}


   public void setAccountNumber(String accountNumber)
   {
      this.accountNumber = accountNumber;
   }


   public String getAccountNumber()
   {
      return accountNumber;
   }


   public void setSelectedSubChannel(String selectedSubChannel)
   {
      this.selectedSubChannel = selectedSubChannel;
   }


   public String getSelectedSubChannel()
   {
      return selectedSubChannel;
   }


   public void setSelectedChannel(String selectedChannel)
   {
      this.selectedChannel = selectedChannel;
   }


   public String getSelectedChannel()
   {
      return selectedChannel;
   }
   public ArrayList < SelectItem > getChannels()
   {
      return channels;
   }


   public void setChannels(ArrayList < SelectItem > channels)
   {
      this.channels = channels;
   }


   public ArrayList < SelectItem > getSubChannels()
   {
      return subChannels;
   }


   public void setSubChannels(ArrayList < SelectItem > subChannels)
   {
      this.subChannels = subChannels;
   }


   public void setDisableGiftAndChannel(boolean disableGiftAndChannel)
   {
      this.disableGiftAndChannel = disableGiftAndChannel;
   }


   public boolean isDisableGiftAndChannel()
   {
      return disableGiftAndChannel;
   }


   public void setDisableSubChannel(boolean disableSubChannel)
   {
      this.disableSubChannel = disableSubChannel;
   }


   public boolean isDisableSubChannel()
   {
      return disableSubChannel;
   }

}
