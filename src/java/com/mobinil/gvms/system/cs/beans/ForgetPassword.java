package com.mobinil.gvms.system.cs.beans;

import java.sql.ResultSet;

import javax.faces.context.FacesContext;

import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.MD5Class;
import com.mobinil.gvms.system.utility.SecurityUtils;
import com.mobinil.gvms.system.utility.SendMail;
import com.mobinil.gvms.system.utility.jsfUtils;

public class ForgetPassword {
public String email;
public String error;
public String inputActivationCode;
public String newPassword;
public String confirmNewPassword;
public String sendActivation(){
	CSInfo csInfo = new CSInfo();
    String strUserMail ="";
    
    if (email!=null)strUserMail = email.trim();
    
    if (strUserMail != null )
    {
      if (strUserMail.compareTo("") == 0 )
      {
        error="Please provide youe mail. ";
        return null;
      }
    }
    else
    {
      error="Please provide user name and password. ";
      return null;
    }
    csInfo = CSDao.getUserByMail(strUserMail);
    String errorMessage = csInfo.getErrorMessage();

    if (errorMessage.compareTo("") == 0)
    {
    	jsfUtils.putInSession(csInfo, "csForgetPass");
      
      String from = "Mobinil";
      String to = email;
      String subject = "GVMS CS Code";
      
      StringBuffer message = new StringBuffer( "Dear Customer Support; ");
      message.append(System.getProperty("Line.separator"));
      message.append("This code will be valide for next 30 minutes.");
      message.append(System.getProperty("Line.separator"));
      message.append(" please do not close explorer till copy and paste that code.");
      message.append(" Your new Code is ");
      message.append(csInfo.getActivationCode());
      

      SendMail sendMail = new SendMail(from, to, subject, message.toString(),csInfo.getActivationCode());
      try{
    	  //System.out.println("activation code is "+csInfo.getActivationCode());
      sendMail.send();
      
      }
      catch(Exception e){new com.mobinil.gvms.system.utility.PrintException().printException(e);
    	  //System.out.println("activation code in exception is "+csInfo.getActivationCode());
      }
      return"sendActivation";
    }
    else{
    	error = "This email does not exists.";
      return null;
    }
    
  
	
}
public  String checkActivationCode(){
	Object CSInformObj = jsfUtils.getFromSession("csForgetPass");
	if (CSInformObj==null)
	{
		error = "This activation code has been expired.";
		return "";
	}
	
	CSInfo csInfoForget = (CSInfo)CSInformObj;
	if (inputActivationCode.compareTo(csInfoForget.getActivationCode())==0) return "checkCode";
	else{
		error = "Please check activation code.";
	return "";
	}
}

public String updatePassword(){
	Object CSInformObj = jsfUtils.getFromSession("csForgetPass");
	if (CSInformObj==null)
	{
		error = "Session has been expired.";
		return "";
	}
	CSInfo csInfoForget = (CSInfo)CSInformObj;

    
    if (newPassword.compareTo(confirmNewPassword) == 0)
      { 

        if (newPassword.compareTo("") == 0 || confirmNewPassword.compareTo("") == 0)
          {

            error = "Please provide password and confirm.";
            return null;
          }
        try
          {
            String passEncripted = MD5Class.MD5(newPassword);
            String SecurityError =  SecurityUtils.checkPass(newPassword, passEncripted, 
            		csInfoForget.getUserName().trim(), 
            		csInfoForget.getSysUserId() + "");
                                            
            if (SecurityError.compareTo("")!=0)
            {
             error = SecurityError;             
             return null;
            }
            else
            {
             if(UserManagementDAO.updateSystemUserPassword( passEncripted,csInfoForget.getSysUserId()+""))
             {
             UserManagementDAO.updateUserLastPass(csInfoForget.getSysUserId()+"",passEncripted);
             error = "Your new password was updated to your account.";             
             return null;
             }
             return null;
            }


          }
        catch (Exception e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        	return null;	
          }


      }
    else
      {
        error = "Please retype password and confirmation.";
        return null;


      }
   
    
  
}

public void setEmail(String email) {
	this.email = email;
}
public String getEmail() {
	return email;
}
public void setError(String error) {
	this.error = error;
}
public String getError() {
	return error;
}
public String getInputActivationCode() {
	return inputActivationCode;
}
public void setInputActivationCode(String inputActivationCode) {
	this.inputActivationCode = inputActivationCode;
}
public String getNewPassword() {
	return newPassword;
}
public void setNewPassword(String newPassword) {
	this.newPassword = newPassword;
}
public String getConfirmNewPassword() {
	return confirmNewPassword;
}
public void setConfirmNewPassword(String confirmNewPassword) {
	this.confirmNewPassword = confirmNewPassword;
}

}
