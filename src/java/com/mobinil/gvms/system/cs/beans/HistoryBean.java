package com.mobinil.gvms.system.cs.beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.mobinil.gvms.system.admin.beans.leftMenu;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.cs.dto.CSLogsDTO;
import com.mobinil.gvms.system.utility.CsvUtil;
import com.mobinil.gvms.system.utility.DTOComparator;
import com.mobinil.gvms.system.utility.FileUtil;
import com.mobinil.gvms.system.utility.HttpServletUtil;
import com.mobinil.gvms.system.utility.jsfUtils;

public class HistoryBean
{
   public ArrayList < CSLogsDTO > csLogs = new ArrayList < CSLogsDTO > ( );
   public ArrayList < CSLogsDTO > editVoucherArray = new ArrayList < CSLogsDTO > ( );
   CSLogsDTO editVoucherObj =null;
   
   public ArrayList<SelectItem> campaigns = new ArrayList<SelectItem>();   
   public ArrayList<SelectItem> gifts = null;
   public ArrayList<SelectItem> channels = null;
   public ArrayList<SelectItem> subChannels = null;
   
   public ArrayList<SelectItem> editChannels = null;
   public ArrayList<SelectItem> editSubChannels = null;
   public String voucherNumber;
   public String dialNumber;
   public String selectedCampaign;
   public String selectedGift;
   public String selectedChannel;
   public String selectedSubChannel;
   public String editSelectedChannel;
   public String editSelectedSubChannel;
   public String accountNumber;
   public Date fromDate;
   public Date toDate;
   public String CSName;
   public String adminSearchCSName;
   public HashMap campaignAndGifts = new HashMap();
   int pageIndex = 0;
   public HtmlDataTable dataTable1;
   public String sortField = null;
   public boolean sortAscending = true;
   public boolean renderEditTable = false;
   
   
   
   public boolean isSortAscending()
   {
      return sortAscending;
   }
   public void setSortAscending(boolean sortAscending)
   {
      this.sortAscending = sortAscending;
   }
   public String getSortField()
   {
      return sortField;
   }
   public void setSortField(String sortField)
   {
      this.sortField = sortField;
   }

   CSInfo csInfo=(CSInfo) jsfUtils.getFromSession("csInfo");
   public HistoryBean()
   {
      HttpServletRequest req = 
         (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
         
      try {    
         campaignAndGifts = CSDao.getAllCampaign("history");
      } catch (SQLException e) {
         
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      } 
         
         String path = req.getRequestURI();
         fromDate = new Date();
         toDate = new Date();
         gifts = new ArrayList<SelectItem>();
         gifts.add(0, new SelectItem("-1","*"));
         
         channels = new ArrayList<SelectItem>();
         channels.add(0, new SelectItem("-1","*"));
         
         subChannels = new ArrayList<SelectItem>();
         subChannels.add(0, new SelectItem("-1","*"));
         
         editChannels = new ArrayList<SelectItem>();
         editChannels.add(0, new SelectItem("-1","*"));
         
         editSubChannels = new ArrayList<SelectItem>();
         editSubChannels.add(0, new SelectItem("-1","*"));
                
            campaigns = (ArrayList <SelectItem>)campaignAndGifts.get ( "all" );
            String typeOfUser = (String) jsfUtils.getFromSession ("csReportFrom");
        //System.out.println ("typeOfUser  isss "+typeOfUser);
         if(typeOfUser.compareTo ( "admin" )==0)
         {
            new leftMenu().checkUserInSession();  
//            csLogs = CSDao.getCSLogs ( false , 0 , null );
            
         }
         if(typeOfUser.compareTo ( "cs" )==0)
         {
            HiddenBean.checkUserInSession ();
            
//            csLogs = CSDao.getCSLogs ( true , /*csInfo.getSysUserId ( )*/ 0, null );
            CSName = csInfo.getFullName();
            
         }
      
        
   }
   public void sendVoucherByFaxination()
   {  
      editVoucherObj = (CSLogsDTO) dataTable1.getRowData();
     try {
		CSDao.insertSMS ( editVoucherObj );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}     
   }
   public void unredeemVoucher()
   {  
      editVoucherObj = (CSLogsDTO) dataTable1.getRowData();
     CSDao.changeVoucherStatus ( editVoucherObj.getVoucher_number ( ) , editVoucherObj.UNREDEEMED );
     filterCSLogs();
   }
   public void deleteVoucher()
   {
       CSInfo csInfo=(CSInfo) jsfUtils.getFromSession("csInfo");
      editVoucherObj = (CSLogsDTO) dataTable1.getRowData();
     CSDao.changeVoucherStatus ( editVoucherObj.getVoucher_number ( ) , editVoucherObj.DELETED,csInfo.getSysUserId()+"" );
     filterCSLogs();
   }
   public void editVoucher()
   {
      editChannels = new ArrayList<SelectItem>();
      editSubChannels = new ArrayList<SelectItem>();
      
      renderEditTable= true;
      editVoucherObj = (CSLogsDTO) dataTable1.getRowData();
      editSelectedSubChannel = editVoucherObj.getUser_id ( );
      editSelectedChannel = editVoucherObj.getUser_type_id ( );
      
      
      HashMap ff =((HashMap)campaignAndGifts.get(editVoucherObj.getCampaign_id ( )));      
      editChannels= (ArrayList <SelectItem>)(ff).get ( "channels" );
      try
      {
         editSubChannels = CSDao.getSubChannel ( editVoucherObj.getUser_type_id ( ),"history" );
      }
      catch ( SQLException e )
      {
         // TODO Auto-generated catch block
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
      
   }
   public void saveEditableVoucher()
   {
      if (editSelectedChannel== null)editSelectedChannel="0";
      if (editSelectedChannel.compareTo ( "-1" )==0)editSelectedChannel="0";
         
      if (editSelectedSubChannel==null)editSelectedSubChannel="0";
      if (editSelectedSubChannel.compareTo ( "-1" )==0)editSelectedSubChannel="0";
         
      CSDao.updateVoucherChannelAndSubChannel(editVoucherObj.getId(),editSelectedChannel,editSelectedSubChannel);
      renderEditTable=false;
      filterCSLogs();
   }
   
   public void sortDataTable(ActionEvent event)
   {
     String sortFieldAttribute = getAttribute(event, "sortField");

     // Get and set sort field and sort order.
     if (sortField != null && sortField.equals(sortFieldAttribute))
     {
       sortAscending = !sortAscending;
     }
     else
     {
       sortField = sortFieldAttribute;
       sortAscending = true;
     }

     // Sort results.
     if (sortField != null)
     {
       Collections.sort(csLogs, 
                        new DTOComparator(sortField, sortAscending));
     }


   }
   public static String getAttribute(ActionEvent event, String name)
   {
     return (String) event.getComponent().getAttributes().get(name);
   }
   @SuppressWarnings("deprecation")
   public void filterCSLogs(){
      
      HttpServletRequest req = 
         (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
         String path = req.getRequestURI();
         
         //System.out.println ("path in filterCSLogs is "+path);
         
         //System.out.println(req.getContextPath());
         //System.out.println(req.getPathInfo());
         //System.out.println(req.getRequestURI());
         //System.out.println(req.getRequestURL());
         
      
      String [] filterPrarmters = new String [10];
      
      if (selectedCampaign==null || selectedCampaign.compareTo ( "" )==0 || selectedCampaign.compareTo ( "-1" )==0)filterPrarmters[0]="";
      else filterPrarmters[0]=selectedCampaign;
      
      if (voucherNumber==null || voucherNumber.compareTo ( "" )==0)filterPrarmters[1]="";
      else filterPrarmters[1]=voucherNumber;
      
      if (dialNumber==null || dialNumber.compareTo ( "" )==0)filterPrarmters[2]="";
      else filterPrarmters[2]=dialNumber;
      
      if (selectedGift==null || selectedGift.compareTo ( "" )==0|| selectedGift.compareTo ( "-1" )==0)filterPrarmters[3]="";
      else filterPrarmters[3]=selectedGift;
      
      if (fromDate==null)filterPrarmters[4]="";
      else filterPrarmters[4]=fromDate.getMonth ( )+1+"-"+fromDate.getDate ( )+"-"+(fromDate.getYear ( )+1900);
      
      if (toDate==null)filterPrarmters[5]="";
      else filterPrarmters[5]=toDate.getMonth ( )+1+"-"+toDate.getDate ( )+"-"+(toDate.getYear ( )+1900);
      
      if (adminSearchCSName==null || adminSearchCSName.compareTo ( "" )==0)filterPrarmters[6]="";
      else filterPrarmters[6]=adminSearchCSName;
      
      if (selectedChannel==null || selectedChannel.compareTo ( "" )==0|| selectedChannel.compareTo ( "-1" )==0)filterPrarmters[7]="";
      else filterPrarmters[7]=selectedChannel;
      
      if (selectedSubChannel==null || selectedSubChannel.compareTo ( "" )==0|| selectedSubChannel.compareTo ( "-1" )==0)filterPrarmters[8]="";
      else filterPrarmters[8]=selectedSubChannel;
      
      if (accountNumber==null || accountNumber.compareTo ( "" )==0)filterPrarmters[9]="";
      else filterPrarmters[9]=accountNumber;
      
      if(path.contains ( "/CSReport" ))
      {
         csLogs = CSDao.getCSLogs ( false , 0 , filterPrarmters );
         
      }
      if(path.contains ( "/CS/" ))
      {
         csLogs = CSDao.getCSLogs ( false , csInfo.getSysUserId ( ) , filterPrarmters );
         
      }
      
     
   }
   
   public String getCampaigGift(){
      gifts = new ArrayList<SelectItem>();
       
       if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
       {
           selectedCampaign="0";
       }
       
       HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
       if (ff!=null)
       gifts = (ArrayList <SelectItem>)(ff).get ( "gifts" );
       else
          gifts.add(0, new SelectItem("-1","*"));
          
       getCampaigChannel();
    
    return null;
  }
   
   public void getCampaigChannel(){
      channels = new ArrayList<SelectItem>();
       
       
       if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
       {
           selectedCampaign="0";
       }
       
       HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
       
       if (ff!=null)      
       channels = (ArrayList <SelectItem>)(ff).get ( "channels" );
       else
          channels.add(0, new SelectItem("-1","*"));
  }
   
   public void getSubChannel(){
      subChannels = new ArrayList<SelectItem>();
      
      if (selectedChannel!=null && selectedChannel.compareTo ( "-1" )!=0){
          try
         {
            subChannels = CSDao.getSubChannel ( selectedChannel,"history" );
         }
         catch ( SQLException e )
         {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            subChannels.add(0, new SelectItem("-1","error"));
         }
      }
    
  }
   public void getEditSubChannel(){
      editSubChannels = new ArrayList<SelectItem>();
      
      if (editSelectedChannel!=null){
          try
         {
             if (editSelectedChannel.compareTo ( "-1" )==0){  
                editSubChannels.clear ( );
                editSubChannels.add(0, new SelectItem("-1","*"));
             }
                
            editSubChannels = CSDao.getSubChannel ( editSelectedChannel,"history" );
         }
         catch ( SQLException e )
         {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            editSubChannels.add(0, new SelectItem("-1","error"));
         }
      }
    
  }
   
   public void pageFirst()
   {
     dataTable1.setFirst(0);
     pageIndex = 0;
   }

   public void pagePrevious()
   {
     dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
     pageIndex--;
   }

   public void pageNext()
   {
     dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
     pageIndex++;
     //System.out.println("get rows " + dataTable1.getRows());
     //System.out.println("get row count " + dataTable1.getRowCount());
   }

   public void pageLast()
   {
     pageIndex = dataTable1.getRowCount();
     int count = dataTable1.getRowCount();
     int rows = dataTable1.getRows();
     dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
   }
   
   public void exportToCSVCustSupport()
   {
     List<List<String>> csvList = new ArrayList<List<String>>();
     String reportName = "";

     
         reportName = "CS-Report";
         for (int counter = 0; counter < csLogs.size(); counter++)
           {
             if (counter == 0)
               {
                 csvList.add(Arrays.asList(new String[]
                       {"Campaign name", "Gift Name", "Voucher NO", 
                         "Dial No","Account No","Channel","Sub Channel","Action","Redemption Date","Creation Date","CS Name" }));
               }


             csvList.add(Arrays.asList(new String[]
                   { csLogs.get(counter).getCampaign_name ( ), 
                   csLogs.get(counter).getGift_name ( ), 
                   csLogs.get(counter).getVoucher_number ( ), 
                   csLogs.get(counter).getDail_number ( ),
                   csLogs.get(counter).getAccount_number ( ),
                   csLogs.get(counter).getUser_type_name ( ) ,
                   csLogs.get(counter).getUser_name ( ) ,                   
                   csLogs.get(counter).getAction_text ( ),
                   csLogs.get(counter).getRedemprion_date ( )==null ? "":csLogs.get(counter).getRedemprion_date ( ).toString ( ),                   
                   csLogs.get(counter).getAction_date ( ).toString ( ),
                   csLogs.get(counter).getCs_name ( )
                    }));
           }
       
    
     DateTime dt = new DateTime();
     DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
     String filename = dt.toString(fmt);


     
     FacesContext fc = FacesContext.getCurrentInstance();



     String templatePath = jsfUtils.getParamterFromIni("defualtPath");   


     //System.out.println("tempPath " + templatePath);
     //System.out.println("File Name Is =" + filename);
     filename = reportName + "-" + filename; // report name

     HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
     filename = filename + ".csv";


     InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
     try
       {
         FileUtil.write(new File(templatePath + "ReportsCSV/" + filename), csvInput);
         File csvFile = new File(templatePath + "ReportsCSV/", filename);
         //System.out.println("File report is " + csvFile.getAbsolutePath());
         HttpServletUtil.downloadFile(response, csvFile, false);

       }
     catch (IOException e)
       {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
         //System.out.println("Download file failed: " + e.getMessage());
          
       }
     finally{
     fc.responseComplete();
     }
//       //System.out.println("is contain "+jsfUtils.containInSession("CSHistory_Bean"));
//       try {
//       //System.out.println(jsfUtils.getFromApplication("CSHistory_Bean").getClass().getName());
//       } catch (Exception e) {
//           e.printStackTrace();
//       }
//     jsfUtils.removeFromSession("CSHistory_Bean");
//       try {
//       //System.out.println(jsfUtils.getFromApplication("CSHistory_Bean").getClass().getName());
//       } catch (Exception e) {
//           e.printStackTrace();
//       }
//       //System.out.println("is contain "+jsfUtils.containInSession("CSHistory_Bean"));
//       //System.out.println("after remove from session");
//     return "";
   }
   
   public void setCsLogs(ArrayList < CSLogsDTO > csLogs)
   {
      this.csLogs = csLogs;
   }
   public ArrayList < CSLogsDTO > getCsLogs()
   {
      return csLogs;
   }
   public ArrayList < SelectItem > getCampaigns()
   {
      return campaigns;
   }
   public void setCampaigns(ArrayList < SelectItem > campaigns)
   {
      this.campaigns = campaigns;
   }
   public ArrayList < SelectItem > getGifts()
   {
      return gifts;
   }
   public void setGifts(ArrayList < SelectItem > gifts)
   {
      this.gifts = gifts;
   }
   public String getVoucherNumber()
   {
      return voucherNumber;
   }
   public void setVoucherNumber(String voucherNumber)
   {
      this.voucherNumber = voucherNumber;
   }
   public String getDialNumber()
   {
      return dialNumber;
   }
   public void setDialNumber(String dialNumber)
   {
      this.dialNumber = dialNumber;
   }
   public String getSelectedCampaign()
   {
      return selectedCampaign;
   }
   public void setSelectedCampaign(String selectedCampaign)
   {
      this.selectedCampaign = selectedCampaign;
   }
   public String getSelectedGift()
   {
      return selectedGift;
   }
   public void setSelectedGift(String selectedGift)
   {
      this.selectedGift = selectedGift;
   }
   public Date getFromDate()
   {
      return fromDate;
   }
   public void setFromDate(Date fromDate)
   {
      this.fromDate = fromDate;
   }
   public Date getToDate()
   {
      return toDate;
   }
   public void setToDate(Date toDate)
   {
      this.toDate = toDate;
   }
   public void setCSName(String cSName)
   {
      CSName = cSName;
   }
   public String getCSName()
   {
      return CSName;
   }

   public void setDataTable1(HtmlDataTable dataTable1)
   {
      this.dataTable1 = dataTable1;
   }

   public HtmlDataTable getDataTable1()
   {
      return dataTable1;
   }
   public void setAdminSearchCSName(String adminSearchCSName)
   {
      this.adminSearchCSName = adminSearchCSName;
   }
   public String getAdminSearchCSName()
   {
      return adminSearchCSName;
   }
   public HashMap getCampaignAndGifts()
   {
      return campaignAndGifts;
   }
   public void setCampaignAndGifts(HashMap campaignAndGifts)
   {
      this.campaignAndGifts = campaignAndGifts;
   }
   public void setSubChannels(ArrayList<SelectItem> subChannels)
   {
      this.subChannels = subChannels;
   }
   public ArrayList<SelectItem> getSubChannels()
   {
      return subChannels;
   }
   public void setChannels(ArrayList<SelectItem> channels)
   {
      this.channels = channels;
   }
   public ArrayList<SelectItem> getChannels()
   {
      return channels;
   } 
   public void setSelectedChannel(String selectedChannel)
   {
      this.selectedChannel = selectedChannel;
   }
   public String getSelectedChannel()
   {
      return selectedChannel;
   }
   public void setSelectedSubChannel(String selectedSubChannel)
   {
      this.selectedSubChannel = selectedSubChannel;
   }
   public String getSelectedSubChannel()
   {
      return selectedSubChannel;
   }
   public void setAccountNumber(String accountNumber)
   {
      this.accountNumber = accountNumber;
   }
   public String getAccountNumber()
   {
      return accountNumber;
   }
   public void setRenderEditTable(boolean renderEditTable)
   {
      this.renderEditTable = renderEditTable;
   }
   public boolean isRenderEditTable()
   {
      return renderEditTable;
   }
   public void setEditSelectedChannel(String editSelectedChannel)
   {
      this.editSelectedChannel = editSelectedChannel;
   }
   public String getEditSelectedChannel()
   {
      return editSelectedChannel;
   }
   public void setEditSelectedSubChannel(String editSelectedSubChannel)
   {
      this.editSelectedSubChannel = editSelectedSubChannel;
   }
   public String getEditSelectedSubChannel()
   {
      return editSelectedSubChannel;
   }
   public void setEditChannels(ArrayList<SelectItem> editChannels)
   {
      this.editChannels = editChannels;
   }
   public ArrayList<SelectItem> getEditChannels()
   {
      return editChannels;
   }
   public void setEditSubChannels(ArrayList<SelectItem> editSubChannels)
   {
      this.editSubChannels = editSubChannels;
   }
   public ArrayList<SelectItem> getEditSubChannels()
   {
      return editSubChannels;
   }
   
}
