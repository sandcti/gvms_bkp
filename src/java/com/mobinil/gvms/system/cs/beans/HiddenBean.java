package com.mobinil.gvms.system.cs.beans;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.utility.jsfUtils;

public class HiddenBean {
	// main functions
	public String logoutCS(){
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	    HttpSession session = 
	      (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

	    String[] beanNames = session.getValueNames();
	    for (int x = 0; x < beanNames.length; x++)
	      {
	        
	                if (sessionMap.containsKey(beanNames[x]))
	                  {
	                    sessionMap.remove(beanNames[x]);
	                  }
	                }
		
		return "logout_cs";}
	public void homeCS(){
	   jsfUtils.removeFromSession("CSHistory_Bean");
		if(jsfUtils.containInSession("CSVoucher_Bean"))
		{
			jsfUtils.removeFromSession("CSVoucher_Bean");
			jsfUtils.putInSession( new GenerateVoucher(),"CSVoucher_Bean");
		}
		jsfUtils.redirect("CS/VoucherCS", "GVMS");
		}
	public String profileCS(){return "profile_CS";}
	public void historyCS(){
	   jsfUtils.removeFromSession("CSVoucher_Bean");
	   jsfUtils.putInSession( "cs","csReportFrom");
		if(jsfUtils.containInSession("CSHistory_Bean"))
		{
			jsfUtils.removeFromSession("CSHistory_Bean");
			jsfUtils.putInSession( new HistoryBean(),"CSHistory_Bean");
		}
		jsfUtils.redirect("CS/historyCS", "GVMS");
		}
	public static void checkUserInSession(){

		CSInfo ldto =(CSInfo) jsfUtils.getFromSession("csInfo");
		  boolean userChecking =  jsfUtils.containInSession("csInfo");
		  
		  if (ldto==null)jsfUtils.redirect("CS/Login", "GVMS");  
		  
		  if (!userChecking)jsfUtils.redirect("CS/Login", "GVMS");  

		  

		}

}
