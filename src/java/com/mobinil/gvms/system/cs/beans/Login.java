/**
 * Class Def : Login Bean used to authinticate customer support users
 * 
 * #Class name in facesconfig.xml
 * CSLogin_Bean
 * 
 * #Class main variables * 
 * userName : input field demonstrate CS user name
 * password : input field demonstrate CS password
 * error : output label demonstrate any error in userName or password
 * 
 * Class creation date : Monday, March 22, 2010, 11:27:29 AM
 * Class documentation date : April 14, 2010 11:27:53 AM
 *  
 * @author Mahmoud Abdel aal 
 */


package com.mobinil.gvms.system.cs.beans;

import java.sql.Connection;
import java.util.ArrayList;

import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.mobinil.gvms.system.admin.login.loginDAO.LoginDAO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.application.GVMSAppContextListener;
import com.mobinil.gvms.system.application.UserDestroyDTO;
import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.MD5Class;
import com.mobinil.gvms.system.utility.SecurityUtils;
import com.mobinil.gvms.system.utility.jsfUtils;




public class Login {
	  public String userName;	  
	public String password;
	  public String error;
	  
	  /**
	   * Authintcate user over the system by validating user name and password.
	   * 
	   * @return error message or navigation case string.
	   */
	public String checkCSInfo()
	  {    
		//System.out.println("checkCSInfo username="+userName+" password="+password);
	        
	    String pass =  password;
	    CSInfo csInfo = new CSInfo();
	    String strUserName ="";
	    
	    if (userName!=null)strUserName = userName.trim();
	    
	    if (strUserName != null || pass != null)
	    {
	      if (strUserName.compareTo("") == 0 || pass.compareTo("") == 0)
	      {
	        error="Please provide user name and password. ";
	        return null;
	      }
	    }
	    else
	    {
	      error="Please provide user name and password. ";
	      return null;
	    }
	    csInfo = CSDao.authUser(strUserName);
	    String errorMessage = csInfo.getErrorMessage();

	    if (errorMessage.compareTo("") == 0)
	    {
	      try
	      {


	        String encripteCurrPass = MD5Class.MD5(pass);
	        if (encripteCurrPass.compareTo(csInfo.getPassword()) == 0)
	        {
	          String securityError = 
	            SecurityUtils.checkPassInLogin(pass, encripteCurrPass, 
	                                           strUserName, 
	                                           csInfo.getSysUserId() + "");
	          if (securityError.compareTo("") == 0)
	          {
	            Connection con = DBConnection.getConnection();
	            LoginDAO.setIncrementLogin(con, csInfo.getSysUserId() + "");
	            LoginDAO.setDefaultLock(con, csInfo.getSysUserId() + "");
	            con.close();
	            jsfUtils.putInSession(csInfo, "csInfo");
	            jsfUtils.putInSession(new HiddenBean(), "Hidden_Bean");
	            String sessionId = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession(false).getId();
	            //System.out.println("session Id in login bean is "+sessionId);
	            
	            ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();
	            for (int i = 0; i < users.size(); i++) {
	            	UserDestroyDTO udd = users.get(i);
	            	if (udd.getSessionId().compareTo(sessionId) == 0 && udd.getDestroy()) {
	            		udd.setDestroy(false);
	            	}            	
	            	users.remove(i);
	            	users.add(udd);
	            }
	            GVMSAppContextListener.setUserNavigation(users);
	            return "welcomeCS";
	          }
	          else
	          {
	            error=securityError;
	            return null;
	          }        
	        
	        }
	        else
	        {
	          Connection con = DBConnection.getConnection();
	          LoginDAO.setIncrementLock(con, csInfo.getSysUserId() + "");
	          con.close();
	          error="Invalid password.";
	          return null;
	        }
	      }
	      catch (Exception e)
	      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);	        return null;
	      }

	    }
	    else
	    {
	      error=errorMessage;
	      return null;
	    }


	  }
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
