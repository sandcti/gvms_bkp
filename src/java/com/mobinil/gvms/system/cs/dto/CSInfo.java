package com.mobinil.gvms.system.cs.dto;

import java.util.Date;

public class CSInfo {
	public Date endDate;
	  public String password;
	  public String userName;
	  public String fullName;
	  public String sysUserType;
	  public Integer userTypeId;
	  public Integer userId;
	  public Integer loginCount;
	  public Integer sysUserId;
	  public String userLocked;
	  public String userEmail;
	  public String userPhone;
	  
	  public Integer passIndex;
	  public String userType;
	  public String activationCode;
	  public String errorMessage;
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getSysUserType() {
		return sysUserType;
	}
	public void setSysUserType(String sysUserType) {
		this.sysUserType = sysUserType;
	}
	public Integer getUserTypeId() {
		return userTypeId;
	}
	public void setUserTypeId(Integer userTypeId) {
		this.userTypeId = userTypeId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}
	public Integer getSysUserId() {
		return sysUserId;
	}
	public void setSysUserId(Integer sysUserId) {
		this.sysUserId = sysUserId;
	}
	public String getUserLocked() {
		return userLocked;
	}
	public void setUserLocked(String userLocked) {
		this.userLocked = userLocked;
	}
	public Integer getPassIndex() {
		return passIndex;
	}
	public void setPassIndex(Integer passIndex) {
		this.passIndex = passIndex;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserPhone() {
		return userPhone;
	}
}
