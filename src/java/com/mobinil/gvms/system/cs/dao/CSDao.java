package com.mobinil.gvms.system.cs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.cs.dto.CSInfo;
import com.mobinil.gvms.system.cs.dto.CSLogsDTO;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.DateClass;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.Notify;
import com.mobinil.gvms.system.utility.jsfUtils;

public class CSDao
{
   public static CSInfo authUser(String username)
   {
      CSInfo lc = new CSInfo ( );
      
      try
      {
         Connection conn = DBConnection.getConnection();
         StringBuilder sql = new StringBuilder (
               "SELECT   T1.SYSTEM_USER_NAME CSUSERNAME,T2.USER_ID,T2.USER_E_MAIL,T2.USER_PHONE_NUMBER, T1.SYSTEM_LOGIN_END_DATE," );
         sql
               .append ( " T1.SYSTEM_USER_PASSWORD, T1.SYSTEM_LOGIN_COUNT, T1.SYSTEM_USER_ID," );
         sql.append ( " T2.USER_FULL_NAME, IS_LOCKED," );
         sql.append ( " MAX (T4.PASSWORD_INDEX) SYSTEM_USER_PASS_INDEX" );
         sql.append ( " FROM GVMS_SYSTEM_USER T1 LEFT JOIN GVMS_CS T2" );
         sql.append ( " ON T1.SYSTEM_USER_ID = T2.SYSTEM_USER_ID" );
         sql
               .append ( " LEFT JOIN GVMS_PASS_TRACE T4 ON T1.SYSTEM_USER_ID = T4.SYSTEM_USER_ID" );
         sql.append ( " WHERE T1.SYSTEM_USER_NAME = lower('" );
         sql.append ( username );
         sql.append ( "') AND T1.SYSTEM_USER_TYPE='cs' AND T1.USER_STATUS_ID = 1" );
         sql.append ( " GROUP BY T1.SYSTEM_USER_NAME," );
         sql
               .append ( " T2.USER_E_MAIL,T2.USER_ID,T2.USER_PHONE_NUMBER,T1.SYSTEM_LOGIN_END_DATE," );
         sql.append ( " T1.SYSTEM_USER_PASSWORD," );
         sql.append ( " T1.SYSTEM_LOGIN_COUNT," );
         sql.append ( " T1.SYSTEM_USER_ID," );
         sql.append ( " T1.SYSTEM_USER_TYPE," );
         sql.append ( " T2.USER_FULL_NAME," );
         sql.append ( " IS_LOCKED" );
         ResultSet result = ExecuteQueries.executeQuery ( conn,sql.toString ( ) );
         
         if ( result.next ( ) )
         {
            
            lc.setEndDate ( result.getDate ( "SYSTEM_LOGIN_END_DATE" ) );
            lc.setPassword ( result.getString ( "SYSTEM_USER_PASSWORD" ) );
            lc.setLoginCount ( result.getInt ( "SYSTEM_LOGIN_COUNT" ) );
            lc.setPassIndex ( result.getInt ( "SYSTEM_USER_PASS_INDEX" ) );
            lc.setSysUserId ( result.getInt ( "SYSTEM_USER_ID" ) );
            lc.setUserLocked ( result.getString ( "IS_LOCKED" ) );
            lc.setFullName ( result.getString ( "USER_FULL_NAME" ) );
            lc.setUserName ( result.getString ( "CSUSERNAME" ) );
            lc.setUserEmail ( result.getString ( "USER_E_MAIL" ) );
            lc.setUserPhone ( result.getString ( "USER_PHONE_NUMBER" ) );
            lc.setUserId ( new Integer ( result.getString ( "USER_ID" ) ) );
            lc.setErrorMessage ( "" );
            result.getStatement ( ).close ( );
            result.close ( );
            
            
         }
         else
         {
            result.getStatement ( ).close ( );
            result.close ( );
            lc.setErrorMessage ( "Invalid User Name." );
            
            
         }
         DBConnection.closeConnections ( conn );
         return lc;
      }
      catch ( Exception sqle )
      {
          new com.mobinil.gvms.system.utility.PrintException().printException(sqle);
         lc.setErrorMessage ( "Invalid Login due to connection." );
         return lc;
      }
      
   }
   
   public static CSInfo getUserByMail(String userMail)
   {
      CSInfo lc = new CSInfo ( );
      
      try
      {
         Connection conn = DBConnection.getConnection();
         StringBuilder sql = new StringBuilder (
               "SELECT   dbms_random.String('x',15) str,T1.SYSTEM_USER_NAME CSUSERNAME, T1.SYSTEM_LOGIN_END_DATE," );
         sql
               .append ( " T1.SYSTEM_USER_PASSWORD, T1.SYSTEM_LOGIN_COUNT, T1.SYSTEM_USER_ID," );
         sql.append ( " T2.USER_FULL_NAME, IS_LOCKED," );
         sql.append ( " MAX (T4.PASSWORD_INDEX) SYSTEM_USER_PASS_INDEX" );
         sql.append ( " FROM GVMS_SYSTEM_USER T1 LEFT JOIN GVMS_CS T2" );
         sql.append ( " ON T1.SYSTEM_USER_ID = T2.SYSTEM_USER_ID" );
         sql
               .append ( " LEFT JOIN GVMS_PASS_TRACE T4 ON T1.SYSTEM_USER_ID = T4.SYSTEM_USER_ID" );
         sql.append ( " WHERE T2.USER_E_MAIL = '" );
         sql.append ( userMail );
         sql.append ( "' AND T1.USER_STATUS_ID = 1" );
         sql.append ( " GROUP BY T1.SYSTEM_USER_NAME," );
         sql.append ( " T1.SYSTEM_LOGIN_END_DATE," );
         sql.append ( " T1.SYSTEM_USER_PASSWORD," );
         sql.append ( " T1.SYSTEM_LOGIN_COUNT," );
         sql.append ( " T1.SYSTEM_USER_ID," );
         sql.append ( " T1.SYSTEM_USER_TYPE," );
         sql.append ( " T2.USER_FULL_NAME," );
         sql.append ( " IS_LOCKED" );
         ResultSet result = ExecuteQueries.executeQuery ( conn,sql.toString ( ) );
         
         if ( result.next ( ) )
         {
            lc.setActivationCode ( result.getString ( "str" ) );
            lc.setEndDate ( result.getDate ( "SYSTEM_LOGIN_END_DATE" ) );
            lc.setPassword ( result.getString ( "SYSTEM_USER_PASSWORD" ) );
            lc.setLoginCount ( result.getInt ( "SYSTEM_LOGIN_COUNT" ) );
            lc.setPassIndex ( result.getInt ( "SYSTEM_USER_PASS_INDEX" ) );
            lc.setSysUserId ( result.getInt ( "SYSTEM_USER_ID" ) );
            lc.setUserLocked ( result.getString ( "IS_LOCKED" ) );
            lc.setFullName ( result.getString ( "USER_FULL_NAME" ) );
            lc.setUserName ( result.getString ( "CSUSERNAME" ) );
            lc.setErrorMessage ( "" );
            result.getStatement ( ).close ( );
            result.close ( );
            
            
         }
         else
         {
            result.getStatement ( ).close ( );
            result.close ( );
            lc.setErrorMessage ( "Invalid User Name." );
         }
         DBConnection.closeConnections ( conn );
            return lc;
            
         
         
      }
      catch ( Exception sqle )
      {
          new com.mobinil.gvms.system.utility.PrintException().printException(sqle);
         lc.setErrorMessage ( "Invalid Login due to connection." );
         return lc;
      }
      
   }
   
   public static ArrayList<SelectItem> getSubChannel(String ChannelId,String typeOfCall) throws SQLException{
      Connection connection = null;
      Statement statement = null;      
      ArrayList < SelectItem > subChannels = new ArrayList < SelectItem > ( );
      if (typeOfCall.compareTo ( "home" )==0)
      {
         subChannels.add( new SelectItem("-1","---Select One---"));
         subChannels.add( new SelectItem("0","Any"));
      }         
      
      if (typeOfCall.compareTo ( "history" )==0)
         subChannels.add (new SelectItem ("-1" ,  "*") );
      
      String SQL = "select USER_ID,USER_POS_CODE from GVMS_USER where USER_TYPE_ID='" + ChannelId + "'";
      connection = DBConnection.getConnection ( );
      statement = connection.createStatement ( );
      ResultSet subChannelRS = statement.executeQuery ( SQL.toString ( ) );
      while (subChannelRS.next())
      {
         SelectItem si = new SelectItem(subChannelRS.getInt ( 1 ),subChannelRS.getString ( 2 ));
         subChannels.add ( si );
         
      }
      DBConnection.cleanResultSet ( subChannelRS );
      connection.close ( );
      connection=null;
      return subChannels;
   }

   public static void sendActivationSMS(String dial,String message) throws Exception{
	   
	      Notify.insertInSMSTable(dial, message);
	   }
   
public static int insertSMS(CSLogsDTO cslogs) throws Exception{
  Connection con = DBConnection.getConnectionForThread();
  int ret = insertSMS(con, cslogs);
  DBConnection.closeThreadConnections(con);
  return ret;
   }

public static int insertSMS(Connection conn ,CSLogsDTO cslogs) throws Exception{
   int ret = 0;    
   try
   {
      Statement statement = conn.createStatement ( );
      String SMSText = jsfUtils.getParamterFromIni("SMSText");
      String smsPOSName = cslogs.getUser_name ( );
      if (smsPOSName!=null&&smsPOSName.compareTo ( "Any" )==0)smsPOSName="mobinil shop";
      SMSText = SMSText.replace ( "#1" , smsPOSName );
      SMSText = SMSText.replace ( "#2" , cslogs.getVoucher_number ( ) );
      if (SMSText.contains("#3"))
        {
        SMSText = SMSText.replace("#3",String.valueOf(getVoucherExpiry(conn, cslogs.getCampaign_id())));
        }
      String sql = "insert into GVMS_SMS values(GVMS_CS_SMS_SEQ_ID.nextval,'Mobinil', '"+cslogs.getDail_number ( )+"', '"+SMSText+"', sysdate) ";
      //System.out.println ("sql is "+sql);
      statement.executeUpdate ( sql );
      sql = ( "update GVMS_CS_LOGS set ACTION_TEXT_ID='3' where VOUCHER_NUMBER='"+cslogs.getVoucher_number ( )+"'" );
      statement.executeUpdate ( sql );
      ret = Notify.insertInSMSTable(cslogs.getDail_number ( ), SMSText);
      statement.close();
      statement=null;
   }
   catch ( SQLException e )
   {
      // TODO Auto-generated catch block
      
     new com.mobinil.gvms.system.utility.PrintException().printException(e);
   }
   
   return ret;
      
   }
   
   public static void updateVoucherChannelAndSubChannel(String voucherId,String channel,String subChannel){
      
      ExecuteQueries.executeNoneQuery ( "update GVMS_CS_LOGS set ACTION_TEXT_ID='2',USER_TYPE_ID='"+channel+"', USER_ID='"+subChannel+"' where ID='"+voucherId+"'" );
      
   }
   
 public static void changeVoucherStatus(String voucherId,String status){
      
	 
	 if (status.compareTo("2")==0)	
		 ExecuteQueries.executeNoneQuery ( "update GVMS_CS_LOGS set ACTION_TEXT_ID='4' where VOUCHER_NUMBER='"+voucherId+"'" );
		 	 
      ExecuteQueries.executeNoneQuery ( "update GVMS_CURRENT_VOUCHER set VOUCHER_STATUS_ID='"+status+"' where CURRENT_VOUCHER_NUMBER='"+voucherId+"'" );
      
   }
 public static void changeVoucherStatus(String voucherId,String status,String sysUserId){


	 if (status.compareTo("2")==0)
		 ExecuteQueries.executeNoneQuery ( "update GVMS_CS_LOGS set ACTION_TEXT_ID='4' where VOUCHER_NUMBER='"+voucherId+"'" );

      ExecuteQueries.executeNoneQuery ( "update GVMS_CURRENT_VOUCHER set VOUCHER_STATUS_ID='"+status+"' , USER_ID='"+sysUserId+"' where CURRENT_VOUCHER_NUMBER='"+voucherId+"'" );

   }
   
   public static HashMap/* ArrayList < SelectItem > */getAllCampaign(String typeOfCall)
         throws SQLException
   {
      
      
      //if (jsfUtils.containInSession ( "allCampaignHashMap" )) return (HashMap)jsfUtils.getFromSession ( "allCampaignHashMap" );
         
      ArrayList < SelectItem > campaigns = new ArrayList < SelectItem > ( );      
      
      Connection connection = null;
      Statement statement = null;
      HashMap hm = new HashMap ( );
      StringBuilder SQL = new StringBuilder (
            "SELECT GC.CAMPAIGN_ID,GVMS_GET_GIFTS(GC.CAMPAIGN_ID)  giftIds,GVMS_GET_CAMPAIGN_USERS(GC.CAMPAIGN_ID) user_type_ids,TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') CAMPAIGN_START_DATE," );      
      SQL.append ( " TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') CAMPAIGN_CLOSED_DATE," );
      SQL.append ( " TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') CAMPAIGN_CREATION_DATE," );
      SQL.append ( " GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   " );
      SQL.append ( " GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED,  " );
      SQL.append ( " GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME , COUNT(GCV.CURRENT_VOUCHER_NUMBER) TOTAL_CURRENT_VOUCHER_NUMBER , GVT.VOUCHER_TYPE_NAME," );
      SQL.append ( " GCS.CAMPAIGN_STATUS    " );
      SQL.append ( " FROM GVMS_CAMPAIGN GC , GVMS_CURRENT_VOUCHER GCV , GVMS_VOUCHER_TYPE GVT , GVMS_CAMPAIGN_STATUS GCS" );
      SQL.append ( " WHERE GC.CAMPAIGN_STATUS_ID =2 and GC.CAMPAIGN_ID = GCV.CAMPAIGN_ID (+) and GC.VOUCHER_TYPE_ID = GVT.VOUCHER_TYPE_ID" );
      SQL.append ( " and GCS.CAMPAIGN_STATUS_ID = GC.CAMPAIGN_STATUS_ID" );
      SQL.append ( " and GVT.VOUCHER_TYPE_ID in ('3') ");
      SQL.append ( " group by     GC.CAMPAIGN_ID,   " );
      SQL.append ( " TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') ,  " );
      SQL.append ( " TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') ,  " );
      SQL.append ( " TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') ,  " );
      SQL.append ( " GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   " );
      SQL.append ( " GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED," );
      SQL.append ( " GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME, " );
      SQL.append ( " GVT.VOUCHER_TYPE_NAME , GCS.CAMPAIGN_STATUS" );
      connection = DBConnection.getConnection ( );
      statement = connection.createStatement ( );
      ResultSet campaignRS = statement.executeQuery ( SQL.toString ( ) );
      
      while ( campaignRS.next ( ) )
      {
         String camId =campaignRS.getString ( "CAMPAIGN_ID" );
         String camName =campaignRS.getString ( "CAMPAIGN_NAME" ); 
         campaigns.add ( new SelectItem (
               camId ,  camName) );
         
         HashMap dd = new HashMap();
         dd.put ( "gifts" , getGifts(connection,camId,typeOfCall) );
         dd.put ( "channels" , getChannels(connection,camId,typeOfCall) );
         
         hm.put(camId,dd);
         
         
      }
      if (typeOfCall.compareTo ( "home" )==0)
         campaigns.add (0, new SelectItem ("-1" ,  "---Select One---") );         
      
      if (typeOfCall.compareTo ( "history" )==0)
         campaigns.add (0, new SelectItem ("-1" ,  "*") );
      
      hm.put ( "all" , campaigns );
      jsfUtils.putInSession ( hm , "allCampaignHashMap" );
      DBConnection.cleanResultSet ( campaignRS );
      connection.close ( );
      connection=null;
      return hm;
   }
   
   public static ArrayList < SelectItem > getGifts(Connection conn,String campaginId)
         throws SQLException
   {
      ArrayList < SelectItem > gifts = new ArrayList < SelectItem > ( );
      String SQL = "Select GVMS_GET_GIFTS('" + campaginId
            + "') giftIds from dual ";
      ResultSet giftsRS = ExecuteQueries.executeQuery ( conn,SQL.toString ( ) );
      
      while ( giftsRS.next ( ) )
      {
         String strGifts = giftsRS.getString ( "giftIds" );
         //System.out.println ( "strGifts isssss " + strGifts + "result set is "
//               + giftsRS );
         if ( strGifts != null )
         {
            String giftsArray[] = strGifts.split ( "," );
            for ( int i = 0 ; i < giftsArray.length ; i ++ )
            {
               String giftsIdsAndNames = giftsArray[i];
               String giftIdsNamesArray[] = giftsIdsAndNames.split ( "-" );
               gifts.add ( new SelectItem ( giftIdsNamesArray[0] ,
                     giftIdsNamesArray[1] ) );
               
            }
         }
      }
      DBConnection.cleanResultSet ( giftsRS );
      return gifts;
   }
   
   public static ArrayList < SelectItem > getChannels(Connection con,
         String campaginId,String typeOfCall) throws SQLException
   {
      ArrayList < SelectItem > channels = new ArrayList < SelectItem > ( );
      if (typeOfCall.compareTo ( "home" )==0)
      {
         channels.add( new SelectItem("-1","---Select One---"));
         channels.add( new SelectItem("0","Any"));
      }         
      
      if (typeOfCall.compareTo ( "history" )==0)
         channels.add (new SelectItem ("-1" ,  "*") );
      
      
      String SQL = "Select GVMS_GET_CAMPAIGN_USERS('" + campaginId+ "') channelIds from dual ";
      Statement st = con.createStatement ( );
      ResultSet channelsRS = st.executeQuery ( SQL.toString ( ) );
      
      while ( channelsRS.next ( ) )
      {
         String strChannels = channelsRS.getString ( "channelIds" );         
         if ( strChannels != null )
         {
            String channelsArray[] = strChannels.split ( "," );
            for ( int i = 0 ; i < channelsArray.length ; i ++ )
            {
               String channelsIdsAndNames = channelsArray[i];
               String channelsNamesArray[] = channelsIdsAndNames.split ( "-" );
               channels.add ( new SelectItem ( channelsNamesArray[0] ,
                     channelsNamesArray[1] ) );
               
            }
         }
      }
      DBConnection.cleanResultSet ( channelsRS );
      return channels;
   }
   
   public static ArrayList < SelectItem > getGifts(Connection con,
         String campaginId,String typeOfCall) throws SQLException
   {
      ArrayList < SelectItem > gifts = new ArrayList < SelectItem > ( );
      if (typeOfCall.compareTo ( "home" )==0)
      {
         gifts.add( new SelectItem("-1","---Select One---"));
         gifts.add( new SelectItem("0","Any"));
      }         
      
      if (typeOfCall.compareTo ( "history" )==0)
         gifts.add (new SelectItem ("-1" ,  "*") );
      
      
      String SQL = "Select GVMS_GET_GIFTS('" + campaginId+ "') giftIds from dual ";
      Statement st = con.createStatement ( );
      ResultSet giftsRS = st.executeQuery ( SQL.toString ( ) );
      
      while ( giftsRS.next ( ) )
      {
         String strGifts = giftsRS.getString ( "giftIds" );
         //System.out.println ( "strGifts isssss " + strGifts + "result set is "
//               + giftsRS );
         if ( strGifts != null )
         {
            String giftsArray[] = strGifts.split ( "," );
            for ( int i = 0 ; i < giftsArray.length ; i ++ )
            {
               String giftsIdsAndNames = giftsArray[i];
               String giftIdsNamesArray[] = giftsIdsAndNames.split ( "-" );
               gifts.add ( new SelectItem ( giftIdsNamesArray[0] ,
                     giftIdsNamesArray[1] ) );               
            }
         }
      }
      
      DBConnection.cleanResultSet ( giftsRS );
      return gifts;
   }
   
   public static int insertDialGenerate(int countOfGeneration, String camp_id,
         String dial, String gift_id, String userTypeId,String posId,String accountNumber,int sysUserId) throws Exception
   {
      
      PreparedStatement ps = null;
      Statement st = null;
      ResultSet rs = null;
      ResultSet getPOSNameRS = null;
      Statement getPOSNameST = null;
      PreparedStatement psCsLog = null;
      String posName="";  
      Connection conn = null;
      int countOfExecution = 0;
      String sqlRandomNumber = "select round(dbms_random.value(10000000, 99999999)) ran from dual";
      
      StringBuilder insertCSLogs = new StringBuilder (
            "INSERT INTO GVMS_CS_LOGS ( ID, SYSTEM_USER_ID, ACTION_TEXT_ID, ACTION_DATE, GIFT_ID, CAMPAIGN_ID, " );
      insertCSLogs.append ( "VOUCHER_NUMBER, DAIL_NUMBER, USER_TYPE_ID, USER_ID, ACCOUNT_NUMBER ) VALUES (" );
      insertCSLogs.append ( "GVMS_CS_LOGS_SEQ_ID.nextval, " );
      insertCSLogs.append ( sysUserId );
      insertCSLogs.append ( ", 1, sysdate , " );
      insertCSLogs.append ( gift_id );
      insertCSLogs.append ( ", " );
      insertCSLogs.append ( camp_id );
      insertCSLogs.append ( ", ? , " );
      insertCSLogs.append ( dial );
      insertCSLogs.append ( ",'" );
      insertCSLogs.append ( userTypeId );
      insertCSLogs.append ( "','" );
      insertCSLogs.append ( posId );
      insertCSLogs.append ( "','" );
      insertCSLogs.append ( accountNumber==null || accountNumber.compareTo("")==0 ? "" : accountNumber );
      insertCSLogs.append ( "')" );
      
      StringBuilder SQL = new StringBuilder (
            "insert into GVMS_current_voucher(current_voucher_ID,campaign_id,voucher_status_id,gift_id,CURRENT_VOUCHER_END_DATE,current_voucher_dial_number, current_voucher_number,GIFT_TYPE_ID,USER_TYPE_ID,USER_ID,VOUCHER_EXPIRY_DATE,GIFT_VALUE) values (GVMS_CURRENT_VOUCHER_SEQ_ID.nextval, " );
      SQL.append ( camp_id );
      SQL.append ( ",2," );
      SQL.append ( gift_id );
      SQL.append ( " ,sysdate," );
      SQL.append ( dial );
      SQL.append ( ",?,(select GIFT_TYPE_ID from GVMS_GIFT where gift_id=" );
      SQL.append ( gift_id );
      SQL.append ( ")" );
      SQL.append ( ",'" );
      SQL.append ( userTypeId );
      SQL.append ( "','" );
      SQL.append ( posId );
      SQL.append ( "'," );
      SQL.append ( "(SYSDATE + (nvl( (select VOUCHER_EXPIRE_DAYS from GVMS_CAMPAIGN  where CAMPAIGN_ID='" );
      SQL.append ( camp_id );
      SQL.append ( "') ," );
      SQL.append ( jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" ) );
      SQL.append ( ")))" );
      SQL.append ( ",(select GIFT_VALUE from GVMS_GIFT where GIFT_ID= "  );
      SQL.append ( gift_id );
      SQL.append ( ")" );
      SQL.append ( ")" );
      
      try
      {
         conn = DBConnection.getConnection ( );
         ps = conn.prepareStatement ( SQL.toString ( ) );
         psCsLog = conn.prepareStatement ( insertCSLogs.toString ( ) );
         st = conn.createStatement ( );
         for ( int i = 0 ; i < countOfGeneration ; i ++ )
         {
            
            String randomNumber = "";
            rs = st.executeQuery ( sqlRandomNumber );
            if ( rs.next ( ) ) randomNumber = rs.getString ( 1 );
            DBConnection.cleanResultSet ( rs );
            // //System.out.println("randomNumber is   "+randomNumber);
             //System.out.println("insert voucher sql is   "+SQL);
             //System.out.println("insertCSLogs  is   "+insertCSLogs);
            ps.setString ( 1 , randomNumber );
            psCsLog.setString ( 1 , randomNumber );
            
            int validateInsertion = 0;
            
            validateInsertion = ps.executeUpdate ( );
            if ( validateInsertion > 0 ) 
            {
               psCsLog.executeUpdate ( );
               
               getPOSNameST = conn.createStatement ( );
               getPOSNameRS = getPOSNameST.executeQuery ( "SELECT USER_POS_CODE FROM GVMS_USER WHERE USER_ID='"+posId+"'" );
               if (getPOSNameRS.next ( ))posName=getPOSNameRS.getString ( 1 );
               
               getPOSNameST.close ( );
               getPOSNameRS.close ( );
               getPOSNameST=null;
               getPOSNameRS=null;
               
               CSLogsDTO cslog = new CSLogsDTO();
               cslog.setDail_number ( dial );
               cslog.setUser_name ( posName ) ;
               cslog.setVoucher_number ( randomNumber );
               cslog.setCampaign_id(camp_id);
               int SMSStatus = insertSMS ( conn,cslog );
               if (SMSStatus==-2)
            	   return SMSStatus;
            }
            
            countOfExecution += validateInsertion;
            
         }
         
      }
      catch ( SQLException e )
      {
         //System.out.println ( "Exception is " + e.getMessage ( ) );
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
         DBConnection.cleanResultSet ( rs );
         try
         {
            conn.close ( );
            ps.close ( );
            psCsLog.close ( );
            
         }
         catch ( SQLException e1 )
         {
            // TODO Auto-generated catch block
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
            
         }
         
         conn = null;
         ps = null;
         psCsLog = null;
         st = null;
         rs = null;
         if ( e.getMessage ( ).toLowerCase ( ).contains ( "unique" ) )
         {
            if ( countOfExecution == 0 ) countOfExecution = countOfGeneration;
            
            //System.out.println ( "here in second execution for unique voucher" );
            insertDialGenerate ( countOfExecution , camp_id , dial , gift_id ,userTypeId,posId,accountNumber,
                  sysUserId );
         }
         
      }
//      finally
//      {
//         
//         //DBConnection.cleanResultSet ( rs );
//         try
//         {
//            conn.close ( );
//            ps.close ( );
//            psCsLog.close ( );
//            
//         }
//         catch ( SQLException e1 )
//         {
//            // TODO Auto-generated catch block
//            
//         }
//         
//         conn = null;
//         ps = null;
//         psCsLog = null;
//         st = null;
//         rs = null;
//      }
      return countOfExecution;
      
   }
   
   
   public static boolean updateCSInfo(String argUserId, String adminId,
         String passwordInput, String userNameInput, String emailInput,
         String phoneInput, String fullNameInput, String userName)
   {
      try
      {
         //System.out.println ( " step 7.1 " );
         String pass = "";
         if ( passwordInput.compareTo ( "" ) != 0 )
            pass = ", SYSTEM_USER_PASSWORD='" + passwordInput + "' ";
         
         String SQL = "";
         String sql = "update GVMS_SYSTEM_USER set " + " SYSTEM_USER_NAME='"
               + userNameInput.trim ( ) + "'" + pass + " where SYSTEM_USER_ID="
               + argUserId;
         int dd = ExecuteQueries.executeNoneQuery ( sql );
         if ( dd > 0 )
         {
            UserManagementDAO.updateUserLastPass ( argUserId , passwordInput );
            SQL = "update GVMS_CS set USER_FULL_NAME='"
                  + fullNameInput
                  + "', USER_E_MAIL='"
                  + emailInput
                  + "', USER_PHONE_NUMBER='"
                  + phoneInput
                  + "' ,User_Modified_Date="
                  + new DateClass ( ).convertDate ( new DateClass ( )
                        .getDateTime ( ) ) + ", User_Modified_by='" + userName
                  + "' where USER_ID=" + adminId;
            
         }
         
         if ( ExecuteQueries.executeNoneQuery ( SQL ) > 0 && dd > 0 ) {

         return true; }
         
         return false;
      }
      catch ( Exception e )
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
         return false;
         // TODO: handle exception
      }
      
   }
   
   public static ArrayList < CSLogsDTO > getCSLogs(boolean constructorCall,
         Integer systemUserId, String[] filters)
   {
      
      ArrayList < CSLogsDTO > csLogsArrayList = new ArrayList < CSLogsDTO > ( );
      Connection con = null;
      Statement st = null;
      
      StringBuilder sql = new StringBuilder (
            "select GCL.ID, GCL.SYSTEM_USER_ID,GCS.USER_FULL_NAME, ACTION_TEXT_ID, ACTION_DATE,  " );
      sql
            .append ( " GCL.GIFT_ID, GCL.CAMPAIGN_ID,GC.CAMPAIGN_NAME,GG.GIFT_NAME,GCLT.TEXT, VOUCHER_NUMBER, DAIL_NUMBER,GCV.VOUCHER_STATUS_ID " );
      sql
      .append ( " ,GCL.USER_TYPE_ID,GCL.USER_ID,GCL.ACCOUNT_NUMBER,GUT.USER_TYPE_NAME,GU.USER_POS_CODE,GCV.CURRENT_VOUCHER_END_DATE REDEMPTION_DATE " );
      sql
            .append ( " FROM GVMS_CS_LOGS GCL,GVMS_CAMPAIGN GC,GVMS_GIFT GG,GVMS_CS_LOGS_TEXT GCLT,GVMS_CS GCS,GVMS_CURRENT_VOUCHER GCV, GVMS_USER_TYPE GUT , GVMS_USER GU   where 1=1 " );
      if ( systemUserId != 0 )
      {
         sql.append ( " AND (GCL.SYSTEM_USER_ID=" );
         sql.append ( systemUserId );
         sql.append ( " or gcl.system_user_id= -1 )" );
      }
      sql.append ( " AND GCV.CURRENT_VOUCHER_NUMBER = GCL.Voucher_Number" );      
      sql.append ( " AND GCL.USER_TYPE_ID = GUT.USER_TYPE_ID" );
      sql.append ( " AND GCL.USER_ID =GU.USER_ID" );      
      sql.append ( " AND GCL.CAMPAIGN_ID = GC.CAMPAIGN_ID" );
      sql.append ( " AND GCL.GIFT_ID = GG.GIFT_ID" );
      sql.append ( " AND GCL.ACTION_TEXT_ID = GCLT.ID" );
      sql.append ( " AND GCS.SYSTEM_USER_ID (+)= GCL.SYSTEM_USER_ID" );
      sql.append ( " AND GCV.VOUCHER_STATUS_ID!=5 " );
      
      if ( constructorCall ) sql.append ( " and Action_date >= (sysdate-1)" );
      
      if ( filters != null )
      {
         if ( filters[0].compareTo ( "" ) != 0 )
         {
            sql.append ( " and GCL.CAMPAIGN_ID=" );
            sql.append ( filters[0] );
         }
         
         if ( filters[1].compareTo ( "" ) != 0 )
         {
            sql.append ( " and VOUCHER_NUMBER = '" );
            sql.append ( filters[1] );
            sql.append ( "'" );
         }
         
         if ( filters[2].compareTo ( "" ) != 0 )
         {
            sql.append ( " and DAIL_NUMBER = '" );
            sql.append ( filters[2] );
            sql.append ( "'" );
         }
         if ( filters[3].compareTo ( "" ) != 0 )
         {
            sql.append ( " and GCL.GIFT_ID = '" );
            sql.append ( filters[3] );
            sql.append ( "'" );
         }
         
         if ( filters[4].compareTo ( "" ) != 0 )
         {
            sql.append ( " and Action_date   >= to_date('" );
            sql.append ( filters[4] );
            sql.append ( "','mm-dd-yyyy')" );
         }
         
         if ( filters[5].compareTo ( "" ) != 0 )
         {
            sql.append ( " and Action_date   < to_date('" );
            sql.append ( filters[5] );
            sql.append ( " 23:59:59','mm-dd-yyyy HH24:MI:SS')" );
         }
         if ( filters[6].compareTo ( "" ) != 0 )
         {
            sql.append ( " AND GCS.USER_FULL_NAME like ('" );
            sql.append ( filters[6] );
            sql.append ( "%')" );
         }
         
         if ( filters[7].compareTo ( "" ) != 0 )
         {
            sql.append ( " AND GCL.USER_TYPE_ID = '" );
            sql.append ( filters[7] );
            sql.append ( "'" );
         }
         if ( filters[8].compareTo ( "" ) != 0 )
         {
            sql.append ( " AND GCL.USER_ID = '" );
            sql.append ( filters[8] );
            sql.append ( "'" );
         }
         if ( filters[9].compareTo ( "" ) != 0 )
         {
            sql.append ( " AND GCL.ACCOUNT_NUMBER like ('" );
            sql.append ( filters[9] );
            sql.append ( "%')" );
         }
         
      }
      //System.out.println ("Sql is "+sql.toString ( ));
      try
      {
         con = DBConnection.getConnection ( );
         st = con.createStatement ( );
         
         for ( ResultSet rs = st.executeQuery ( sql.toString ( ) ) ; rs.next ( ) ; csLogsArrayList
               .add ( getCSLog ( rs ) ) )
            ;
         
      }
      catch ( SQLException e )
      {
         // TODO Auto-generated catch block
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
      finally
      {
         
         try
         {
            // rs.close ( );
            st.close ( );
            con.close ( );
         }
         catch ( SQLException e )
         {
            // TODO Auto-generated catch block
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
         }
         
         con = null;
         // rs=null;
         st = null;
         
      }
      
      //System.out.println ( "SQL is " + sql.toString ( ) );
      
      System.out
            .println ( "csLogsArrayList size is " + csLogsArrayList.size ( ) );
      return csLogsArrayList;
      
   }
   
   public static CSLogsDTO getCSLog(ResultSet rs) throws SQLException
   {
      CSLogsDTO cs = new CSLogsDTO ( );
      
      cs.setAction_date ( rs.getTimestamp ( "action_date" ) );
      cs.setAction_text_id ( rs.getString ( "ACTION_TEXT_ID" ) );
      cs.setId ( rs.getString ( "ID" ) );
      cs.setSystem_user_id ( rs.getString ( "SYSTEM_USER_ID" ) );
      cs.setGift_id ( rs.getString ( "GIFT_ID" ) );
      cs.setCampaign_id ( rs.getString ( "CAMPAIGN_ID" ) );
      cs.setAccount_number ( rs.getString ( "ACCOUNT_NUMBER" ) );
      cs.setUser_type_id ( rs.getString ( "user_type_id" ) );
      cs.setUser_id ( rs.getString ( "user_id" ) );      
      cs.setUser_type_name ( rs.getString ( "USER_TYPE_NAME" ) ); // user type name
      cs.setUser_name ( rs.getString ( "USER_POS_CODE" ) ); // pos name
      
      
      String voucherStatusId = rs.getString ( "VOUCHER_STATUS_ID" );
      cs.setVoucherStatus ( voucherStatusId );
      if ( voucherStatusId.compareTo ( cs.REDEEMED ) == 0 ) // redeemed
      {
         cs.setRedemprion_date ( rs.getTimestamp ( "REDEMPTION_DATE" ) );
         cs.setBlockCancelButton ( true );
         cs.setBlockSendButton ( true );
         cs.setBlockUnredeemedButton ( false );
         
      }
      if ( voucherStatusId.compareTo ( cs.UNREDEEMED ) == 0
            || voucherStatusId.compareTo ( cs.NEW ) == 0 ) // Unredeemed or new
      {
         cs.setBlockCancelButton ( false );
         cs.setBlockUnredeemedButton ( true );
         cs.setBlockSendButton ( false );
      }
      if ( voucherStatusId.compareTo ( cs.EXPIRED ) == 0 )// expired
      {
         cs.setBlockCancelButton ( true );
         cs.setBlockUnredeemedButton ( true );
         cs.setBlockSendButton ( true );
      }
      
      cs.setAction_text ( rs.getString ( "text" ) );
      cs.setCampaign_name ( rs.getString ( "CAMPAIGN_NAME" ) );
      cs.setGift_name ( rs.getString ( "GIFT_NAME" ) );
      cs.setCs_name ( rs.getString ( "USER_FULL_NAME" ) );
      
      cs.setVoucher_number ( rs.getString ( "VOUCHER_NUMBER" ) );
      cs.setDail_number ( rs.getString ( "DAIL_NUMBER" ) );
      return cs;
      
   }
   public static void main(String[] args)
   {
String [] filterPrarmters = new String [10];
      
//      if (selectedCampaign==null || selectedCampaign.compareTo ( "" )==0 || selectedCampaign.compareTo ( "-1" )==0)filterPrarmters[0]="";
       filterPrarmters[0]="2";
      java.util.Date toDate = new java.util.Date();
      java.util.Date fromDate = new java.util.Date();
//      if (voucherNumber==null || voucherNumber.compareTo ( "" )==0)filterPrarmters[1]="";
       filterPrarmters[1]="123456";
      
//      if (dialNumber==null || dialNumber.compareTo ( "" )==0)filterPrarmters[2]="";
       filterPrarmters[2]="789456";
      
//      if (selectedGift==null || selectedGift.compareTo ( "" )==0|| selectedGift.compareTo ( "-1" )==0)filterPrarmters[3]="";
       filterPrarmters[3]="3";
      
//      if (fromDate==null)filterPrarmters[4]="";
      
       filterPrarmters[4]=fromDate.getMonth ( )+1+"-"+fromDate.getDate ( )+"-"+(fromDate.getYear ( )+1900);
      
//      if (toDate==null)filterPrarmters[5]="";
       filterPrarmters[5]=toDate.getMonth ( )+1+"-"+toDate.getDate ( )+"-"+(toDate.getYear ( )+1900);
      
//      if (adminSearchCSName==null || adminSearchCSName.compareTo ( "" )==0)filterPrarmters[6]="";
       filterPrarmters[6]="";
      
//      if (selectedChannel==null || selectedChannel.compareTo ( "" )==0|| selectedChannel.compareTo ( "-1" )==0)filterPrarmters[7]="";
       filterPrarmters[7]="4";
      
//      if (selectedSubChannel==null || selectedSubChannel.compareTo ( "" )==0|| selectedSubChannel.compareTo ( "-1" )==0)filterPrarmters[8]="";
       filterPrarmters[8]="5";
      
      
      filterPrarmters[9]="152";
      
      
         getCSLogs ( false , 0 , filterPrarmters );
         
      
   }
   public static int getVoucherExpiry(Connection con, String campaginQueryId) throws SQLException {
        int voucherExpiryDays = 0;
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select nvl(VOUCHER_EXPIRE_DAYS," + jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" ) + ") expire_days from GVMS.GVMS_CAMPAIGN where campaign_id=" + campaginQueryId);
        if (rs.next()) {
            voucherExpiryDays = rs.getInt("expire_days");
        } else {
            voucherExpiryDays = 30;
        }
        try {
            rs.close();
            st.close();
        } catch (Exception e) {
        }
        return 0;
    }
   
}
