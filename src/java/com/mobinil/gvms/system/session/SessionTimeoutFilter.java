package com.mobinil.gvms.system.session;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.mobinil.gvms.system.application.GVMSAppContextListener;
import com.mobinil.gvms.system.application.UserDestroyDTO;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.db.DBUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;



public class SessionTimeoutFilter implements Filter {

	public String timeoutPage = "faces/Admin/AdminLogin.jsp";
        public static Vector<String> exceptReqName = null;

	public void init(FilterConfig filterConfig) throws ServletException {
            System.out.println("here in init filter");
            exceptReqName = new Vector<String>();
            exceptReqName.add("javax.faces.ViewState");
            exceptReqName.add("javax.faces.request.charset");
            exceptReqName.add("ArabicParamtersObj");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException,

	ServletException {
            if (request instanceof HttpServletRequest)
            {
//                try{
//                Connection con = DBConnection.getConnectionForThread();
//                if (con!=null)
//                {
//                String requestParamters=getRequestParamters((HttpServletRequest)request);
//                String sessionParamters=getSessionParamters((HttpServletRequest)request);
////                    System.out.println("requestParamters iss "+requestParamters);
////                    System.out.println("sessionParamters is "+sessionParamters);
//                    processRequestAndSession(con, requestParamters, sessionParamters);
//                    DBConnection.closeThreadConnections(con);
//                }
//
//
//                }
//                catch(Exception e){
////                new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
//                }
//            request.
            }
		if ((request instanceof HttpServletRequest)
				&& (response instanceof HttpServletResponse)) {			
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			// is session expire control required for this request?

			if (isSessionControlRequiredForThisResource(httpServletRequest)) {
				// is session invalid?

				if (isSessionInvalid(httpServletRequest)) {

					
					String timeoutUrl = httpServletRequest.getContextPath()
							+ "/" + getTimeoutPage();

					System.out
							.println("Session is invalid! redirecting to timeoutpage : "
									+ timeoutUrl);					
					// jsfUtils.redirect("GVMS", "Admin/AdminLogin");

					httpServletResponse.sendRedirect(timeoutUrl);					
					return;

				}
			}
		}		
		filterChain.doFilter(request, response);
		
	}

	/*
	 * 
	 * session shouldn't be checked for some pages. For example: for timeout
	 * page..
	 * 
	 * Since we're redirecting to timeout page from this filter,
	 * 
	 * if we don�t disable session control for it, filter will again redirect to
	 * it
	 * 
	 * and this will be result with an infinite loop.
	 */

	public boolean isSessionControlRequiredForThisResource(
			HttpServletRequest httpServletRequest) {
		////System.out.println("step 10");

		String requestPath = httpServletRequest.getRequestURI();
		////System.out.println("step 11 " + requestPath);
		if (requestPath.contains("/Admin"))timeoutPage = "faces/Admin/AdminLogin.jsp";
		
		if (requestPath.contains("/CS"))timeoutPage = "faces/CS/Login.jsp";
		
		if(!requestPath.contains ( "/Admin" ) && !requestPath.contains ( "/CS" )) timeoutPage = "faces/Login.jsp";
		boolean controlRequired = !StringUtils.contains(requestPath,
				getTimeoutPage());
		////System.out.println("step 12 " + controlRequired);

		return controlRequired;

	}

	public boolean isSessionInvalid(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession(false);
		boolean sessionInValid = false;
		ArrayList<UserDestroyDTO> users = GVMSAppContextListener
				.getUserNavigation();
		for (int i = 0; i < users.size(); i++) {
			UserDestroyDTO udd = users.get(i);
			if (udd.getSessionId().compareTo(session.getId()) == 0 && udd.getDestroy()) {
				sessionInValid=true;
			}
			
		}
		return sessionInValid;

	}

	public void destroy() {		
	}

	public String getTimeoutPage() {		
		return timeoutPage;

	}

	public void setTimeoutPage(String timeoutPage) {
		this.timeoutPage = timeoutPage;

	}

        public String getRequestParamters(HttpServletRequest request) {
        Enumeration objParameterNamesEnum = request.getParameterNames();
        String strParameterName = "";
        String strParameter = "";
        String allParams = "";

        while (objParameterNamesEnum.hasMoreElements()) {
            strParameterName = "";
            strParameterName = (String) objParameterNamesEnum.nextElement();
            strParameter = request.getParameter(strParameterName);
            if (!exceptReqName.contains(strParameterName)&& strParameterName.compareTo(strParameter)!=0 ){            
            allParams += strParameterName + "=" + strParameter+"  ";
            }
        }
        return allParams;
    }
        public String getSessionParamters(HttpServletRequest request) {

        String strParameter = "";
        String allParams = "";
        HttpSession sess = request.getSession(false);
        String sessNames[] = sess.getValueNames();
        for (String name : sessNames) {
            strParameter = sess.getValue(name).toString();
            if (!exceptReqName.contains(name) && name.compareTo(strParameter) != 0) {
                if (strParameter.contains(".")) {
                    try {
                      allParams +=  getClass(strParameter.split("@")[0], sess.getValue(name))+" ";
                    } catch (Exception e) {
                    }

                }
                allParams += name + "=" + strParameter + "  ";

            }
        }

        
        
        
        return allParams;
    }

        public String getClass(String classFullName,Object obj) throws ClassNotFoundException{
            String ss=" "+classFullName+" ";
        Class clazz = obj.getClass();
        Field [] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                ss+=field.getName()+"= ";
                
            try {
                ss+=field.get(obj)+" ";
            } catch (IllegalArgumentException ex) {                
            } catch (IllegalAccessException ex) {                
            }

            }

            return ss;
        }
        
        private String getMaxRequestId(Connection con){
        return DBUtil.executeQuerySingleValueString("select nvl(max(log_id)+1,0) log_id from GVMS.GVMS_LOGGING_REQUESTS", "log_id", con);
        }

        private int getNumberOfInsertion(String paramters)
        {
            return ((paramters.length())/(3999));
        }
        private static String [] getArrayOfParam(int len,String paramters)
        {
            len = len==0 ? 1 : len;
            String [] logs = new String [len];
            int startIndx = 0;
            int lastIndx = 0;
            for (int i = 0; i < len; i++) {
                lastIndx+=paramters.length()< 3999 ? paramters.length() : 3999;
                if (i==len-1)
                {
                lastIndx = paramters.length();
                }
                logs[i] =  paramters.substring(startIndx, lastIndx);
                startIndx = lastIndx;
            }

            return logs;
        }

        private void insertLog(Statement st,String request,String session,String logId){
        try {
            DBUtil.executeSQL("insert into GVMS.GVMS_LOGGING_REQUESTS values ('"+request+"','"+session+"','"+logId+"',sysdate) ", st);
        } catch (SQLException ex) {            
        }
        }
        private void processRequestAndSession(Connection con,String request,String session) throws SQLException{
        String maxLogId = getMaxRequestId(con);
        int numberOfRequestRows = getNumberOfInsertion(request);
        int numberOfSessionRows = getNumberOfInsertion(session);
        String [] arrOfRequestParams= getArrayOfParam(numberOfRequestRows,request);
        String [] arrOfSessionParams= getArrayOfParam(numberOfSessionRows,session);
        String [] arrOfParams = null;
        arrOfParams = arrOfRequestParams.length > arrOfSessionParams.length ? arrOfRequestParams : arrOfSessionParams;
        Statement st = con.createStatement();
            for (int i = 0; i < arrOfParams.length; i++) {
                insertLog(st, getStringFromArr(arrOfRequestParams,i), getStringFromArr(arrOfSessionParams,i), maxLogId);

            }
        st.close();


        }

        private String getStringFromArr(String [] params,int indx){
            try {
                return params[indx];
            } catch (Exception e) {
                return "";
            }
        }

//        public static void main (String [] args){
//        String dd = "ssdfsd ";
//        int len = dd.length()/10;
//        String [] ss= getArrayOfParam(len,dd);
//            for (int i = 0; i < ss.length; i++) {
//                String string = ss[i];
//                System.out.println(string);
//
//            }
//
//        }

}
