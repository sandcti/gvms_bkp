package com.mobinil.gvms.system.session;

import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.mobinil.gvms.system.application.GVMSAppContextListener;
import com.mobinil.gvms.system.application.UserDestroyDTO;
import com.mobinil.gvms.system.application.appBean;

public class MySessionListener implements HttpSessionListener {

	public MySessionListener() {
	}

	
	public void sessionCreated(HttpSessionEvent event) {
		//System.out.println("session created " + event.getSession().getId());
		
		ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();
		UserDestroyDTO udd = new UserDestroyDTO();
		udd.setSessionId(event.getSession().getId());
		udd.setDestroy(false);
		users.add(udd );
		GVMSAppContextListener.setUserNavigation(users);
	}

	
	public void sessionDestroyed(HttpSessionEvent event) {
		event.getSession().invalidate();		
		HttpSession session = event.getSession();
		//System.out.println("Current Session destroyed :" + session.getId()
//				+ " Logging out user");
		


		
		ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();		
		for (int i = 0; i < users.size(); i++) {
			UserDestroyDTO udd = users.get(i);
			if (udd.getSessionId().compareTo(session.getId())==0)
			{				
				udd.setDestroy(true);
			}
			users.remove(i);
			users.add(udd);
			
		}
		
		GVMSAppContextListener.setUserNavigation(users);
	
	}

	
}