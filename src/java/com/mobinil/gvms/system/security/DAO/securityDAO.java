package com.mobinil.gvms.system.security.DAO;

import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.security.DTO.*;
import java.sql.Connection;

import java.sql.*;
import java.sql.Statement;

import java.util.Vector;

public class securityDAO
{
  public securityDAO()
  {
  }
  
  public static Vector getSystemSetting(String securityTablePropName,String type)
      {
      Vector sdto = new Vector();
      try
        {
      Connection con = DBConnection.getConnection();
     Statement stat = con.createStatement();
      String SQL = "select * from " + securityTablePropName+ " where SECURITY_TYPE='"+type+"' and SECURITY_STATUS!=2";
      ResultSet rs = stat.executeQuery(SQL);
      
          while (rs.next())
            {
              securityDTO dto = new securityDTO();
              dto.setID(rs.getInt("ID") + "");
              dto.setSECUIRTY_NAME(rs.getString("SECUIRTY_NAME"));
              dto.setSECUIRTY_TYPE(Integer.valueOf(rs.getInt("SECURITY_TYPE")+""));
              Boolean b = Boolean.FALSE;
              if (rs.getInt("SECURITY_STATUS") == 1)
                {
                  b = Boolean.TRUE;
                }
              if (rs.getInt("SECURITY_STATUS") == 2)
                {
                  b = Boolean.FALSE;
                }
              dto.setSECURITY_STATUS(b);
              sdto.add(dto);

            }
            con.close();
          DBConnection.cleanResultSet(rs);            
          return sdto;
        }
      catch (SQLException e)
        {
          return null;
        }

    }

    public static Vector getSystemSetting(Connection con)
      {
      Vector sdto = new Vector();
      try
        {
  //    Connection con = Utility.getConnection();
     Statement stat = con.createStatement();
      String SQL = "select * from GVMS_SECURITY_CONTROLLER";
      ResultSet rs = stat.executeQuery(SQL);
      
          while (rs.next())
            {
              securityDTO dto = new securityDTO();
              dto.setID(rs.getInt("ID") + "");
              dto.setSECUIRTY_NAME(rs.getString("SECUIRTY_NAME"));
              dto.setSECUIRTY_TYPE(new Integer(rs.getInt("SECURITY_TYPE")));
              dto.setIntSECURITY_STATUS(new Integer(rs.getInt("SECURITY_STATUS")));

  //            Boolean b = Boolean.FALSE;
  //            if (rs.getInt("SECURITY_STATUS") == 1)
  //              {
  //                b = Boolean.TRUE;
  //              }
  //            if (rs.getInt("SECURITY_STATUS") == 2)
  //              {
  //                b = Boolean.FALSE;
  //              }
  //            dto.setSECURITY_STATUS(b);
              sdto.add(dto);

            }
  //          Utility.closeConnection(con);
            rs.getStatement().close();
            rs.close();
          return sdto;
        }
      catch (SQLException e)
        {
          return null;
        }

    }

  public static Integer getProps(String propName){
    Integer property=new Integer(0);

    try
        {
      Connection con = DBConnection.getConnection();
     Statement stat = con.createStatement();
     //System.out.println("propName="+propName);
      ResultSet propertyRS = stat.executeQuery("SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='"+propName+"'");
      
        if (propertyRS.next())
          {
            property = Integer.valueOf(propertyRS.getInt("PROPERTIES")+"");
    
          }
        con.close();
        propertyRS.getStatement().close();
        propertyRS.close();
        return property;
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        return Integer.valueOf(0+"");
      }



  }

  public static Vector getProps(Connection con){
    Vector properties=new Vector();

    try
        {
  //    Connection con = Utility.getConnection();
     Statement stat = con.createStatement();
      ResultSet propertyRS = stat.executeQuery("SELECT PROPERTIES from GVMS_PROPERTIES ");
      
        while (propertyRS.next())
          {
            properties.addElement(new Integer(propertyRS.getInt("PROPERTIES")));
    
          }
  //      Utility.closeConnection(con);
        propertyRS.getStatement().close();
        propertyRS.close();
        return properties;
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        return null;
      }



  }

  public static void updateProps(Connection con,Vector vec){


    try
        {
  //    Connection con = Utility.getConnection();
     Statement stat = con.createStatement();

     for(int i=0;i<vec.size();i++){
     
      int ss = (Integer.valueOf((String)vec.get(i))).intValue();
      if (i==2)
     {
     Integer lastPassCount =  getProps("LAST_PASSWORD_COUNT");
      if (lastPassCount.intValue()>ss)
        {
        updatePasswordHistory(con,lastPassCount.intValue(),ss);
        }
     }
      stat.executeUpdate("update GVMS_PROPERTIES  set PROPERTIES = '"+ss+"' where ID = "+(i+1));

     }

      stat.close();
        
        
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        
      }



  }
      public static void updatePasswordHistory(Connection con,int intLastPass,int intNewPass){
          String strGetUsersId = "select User_id from GEN_USER";
          String strGetUserHistory = "select MAX(PASSWORD_INDEX) passCount from GVMS_PASS_TRACE where SYSTEM_USER_ID=";
  try 
  {
         Statement stat = con.createStatement();
         Vector vec = new Vector();
         ResultSet userIdsRs = stat.executeQuery(strGetUsersId);
     
        while (userIdsRs.next())
          {
            vec.addElement(new Integer(userIdsRs.getInt(1)));  
          }        
        userIdsRs.close();
        for (int i=0;i<vec.size();i++)
        {      

        ResultSet userHistroyRs = stat.executeQuery(strGetUserHistory+((Integer) vec.get(i)));

        if (userHistroyRs.next())
        {
          if (userHistroyRs.getInt("passCount")!=0 && intLastPass>intNewPass)
          {  

          String SQL1 = "update GVMS_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-(select max(PASSWORD_INDEX)-"+intNewPass+" from GVMS_PASS_TRACE) where SYSTEM_USER_ID="+((Integer) vec.get(i));
          String SQL2 = "delete from GVMS_PASS_TRACE where PASSWORD_INDEX<=0 and SYSTEM_USER_ID="+((Integer) vec.get(i));
            stat.executeUpdate(SQL1);
            stat.executeUpdate(SQL2);

          }
        }
        userHistroyRs.close();
        }
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        
      }
        }      
  public static void updateSysSetting(Connection con,Vector vec){


    try
        {
  //    Connection con = Utility.getConnection();
     Statement stat = con.createStatement();

     for(int i=0;i<vec.size();i++){
  String settingId = ((securityDTO)vec.get(i)).getID();   
  int settingStatus = ((securityDTO)vec.get(i)).getIntSECURITY_STATUS().intValue();
  int settingType =((securityDTO)vec.get(i)).getSECUIRTY_TYPE().intValue(); 
      stat.executeUpdate("update GVMS_SECURITY_CONTROLLER  set SECURITY_STATUS = '"+settingStatus+"' ,SECURITY_TYPE = '"+settingType+"'  where ID = "+settingId);

     }

      stat.close();
        
        
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        
      }



  }


  public static Integer getMandatory(){
    Integer securityMandatoryCount=Integer.valueOf(0+"");
  try
        {
      Connection con = DBConnection.getConnection();
     Statement stat = con.createStatement();
  String SQL="select count (SECURITY_TYPE) SECURITY_TYPE from GVMS_SECURITY_CONTROLLER where SECURITY_TYPE=2";

      ResultSet mandatoryRS = stat.executeQuery(SQL);
     
        if (mandatoryRS.next())
          {
            securityMandatoryCount = Integer.valueOf (mandatoryRS.getInt("SECURITY_TYPE")+"");
    
          }
        con.close();
        mandatoryRS.getStatement().close();
        mandatoryRS.close();
      }
      catch (SQLException e)
      {
        e.printStackTrace();
        return Integer.valueOf(0+"");
      }
      return securityMandatoryCount;

  }
}
