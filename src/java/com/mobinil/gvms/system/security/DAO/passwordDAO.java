package com.mobinil.gvms.system.security.DAO;

import com.mobinil.gvms.system.utility.DBConnection;

import java.sql.*;
public class passwordDAO
{
  public passwordDAO()
  {
  }
  public static Boolean checkLastPassword(String passEncripted, String sysUserId)
    {
    Boolean aa = Boolean.TRUE;
    try
        {
    Connection con = DBConnection.getConnection();
     Statement stat = con.createStatement();
     String SQL="select SYSTEM_USER_ID from GVMS_PASS_TRACE  where PASSWORD='" + 
                                   passEncripted + "' and SYSTEM_USER_ID = " + sysUserId;
      ResultSet checkPassExists = stat.executeQuery(SQL);


      
          if (!checkPassExists.equals(null))
            {
              if (checkPassExists.next())
                {
                
                  return aa.FALSE;
                }
            }
          con.close();
          checkPassExists.getStatement().close();
          checkPassExists.close();

          return aa.TRUE;
        }
      catch (SQLException e)
        {
          return aa.TRUE;
        }


    }

    public static Boolean checkLock(String userId)
    {
      Boolean aa = Boolean.TRUE;
      Integer lockTimes = securityDAO.getProps("LOCK_TIMES");
      Integer loginFailTimes =new Integer(0);
  try
        {
      Connection con = DBConnection.getConnection();
     Statement stat = con.createStatement();
      String SQL = "select IS_LOCKED from GVMS_SYSTEM_USER where SYSTEM_USER_ID='" + userId + "'";
      ResultSet getUserLock = stat.executeQuery(SQL);
      
          if (getUserLock.next())
            {
              loginFailTimes = Integer.valueOf(getUserLock.getString("IS_LOCKED"))  ;
            }
          con.close();
          getUserLock.getStatement().close();
          getUserLock.close();
          if (loginFailTimes.intValue() >= lockTimes.intValue()){
          return aa.FALSE;
          }
          return aa.TRUE;
        }
      catch (SQLException e)
        {
          return aa.FALSE;
        }

    }
    public static Boolean checkExpire(String userId)
    {    
      Boolean aa = Boolean.TRUE;
      Date endLoginDate =null;
  try
        {
      Connection con = DBConnection.getConnection();
      Statement stat = con.createStatement();
      String SQL = "select SYSTEM_LOGIN_END_DATE from GVMS_SYSTEM_USER where SYSTEM_USER_ID='" + userId + "'";
      ResultSet getUserExpire = stat.executeQuery(SQL);
      
          if (getUserExpire.next())
            {
              endLoginDate = getUserExpire.getDate("SYSTEM_LOGIN_END_DATE") ;
            }
          con.close();
          getUserExpire.getStatement().close();
          getUserExpire.close();
          if (endLoginDate.before(new java.util.Date())){
          return aa.FALSE;
          }
          return aa.TRUE;
        }
      catch (SQLException e)
        {
          return aa.FALSE;
        }

    }
}
