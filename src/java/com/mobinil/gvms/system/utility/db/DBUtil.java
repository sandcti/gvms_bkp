package com.mobinil.gvms.system.utility.db;

//import application.core.adm.model.CategoryModel;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.MainModel;
import java.util.HashMap;
import java.util.Vector;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBUtil {

    public DBUtil() {
    }

    public static <T extends MainModel> Vector<T> executeSqlQueryMultiValue(String sql, Class<T> clazz, Connection con) {
        Vector<T> valueVector = new Vector<T>();
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            while (res.next()) {
                T tempModel = clazz.newInstance();
                tempModel.fillInstance(res);
                valueVector.add(tempModel);
                tempModel = null;
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return valueVector;
    }

    public static <T extends MainModel> ArrayList<T> executeSqlQueryMultiValueAL(String sql, Class<T> clazz, Connection con) {
        ArrayList<T> valueVector = new ArrayList<T>();
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            while (res.next()) {
                T tempModel = clazz.newInstance();
                tempModel.fillInstance(res);
                valueVector.add(tempModel);
                tempModel = null;
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return valueVector;
    }

    public static <T extends MainModel> T executeSqlQuerySingleValue(String sql, Class<T> clazz, Connection con) {
        T value = null;
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                value = clazz.newInstance();
                value.fillInstance(res);
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return value;
    }

    public static int executeSQL(String sql, Connection con) {
        Statement stat = null;
        int key = 0;
        try {
            stat = con.createStatement();
            key = stat.executeUpdate(sql);


        } catch (Exception e) {
            System.out.println("error in executing sql:" + sql);
//            e.printStackTrace();
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        } finally {
            close(stat);

        }
        return key;

    }

    public static void executeSQL(String sql, Statement st) throws SQLException {


        try {
        st.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(" e message is :" + e.getMessage());
                    System.out.println("sql:" + sql);

        }
        



    }

    public static boolean isColumnExistInRS(ResultSet rs, String columnName) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();

            int numColumns = rsmd.getColumnCount(); // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                if (/*rsmd.getColumnName(i).equals(columnName) || */rsmd.getColumnLabel(i).equals(columnName)) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            new com.mobinil.gvms.system.utility.PrintException().printException(ex);
        }
        return false;

    }

    public static void executeSQL(String sql) {
        Connection con = null;
        Statement stat = null;
        try {
            con = DBConnection.getConnection();
            stat = con.createStatement();
            stat.execute(sql);
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
            close(con);
        }
    }
    public static void executeSQLStat(String sql) {
        Connection con = null;
        Statement stat = null;
        try {
            con = DBConnection.getConnectionForThread();
            stat = con.createStatement();
            stat.execute(sql);
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
            close(con);
        }
    }

    public static boolean executeSQLExistCheck(String sql) {
        boolean checkFlag = false;
        Connection con = null;
        Statement stat = null;
        try {
            con = DBConnection.getConnection();
            stat = con.createStatement();

            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                checkFlag = true;
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
            close(con);
        }
        return checkFlag;
    }

    public static boolean executeSQLExistCheck(String sql, Connection con) {
        boolean checkFlag = false;
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                checkFlag = true;
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);

        }
        return checkFlag;
    }

    public static String executeQuerySingleValueString(String sql, String fieldName, Connection con) {
        String value = "";
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                value = res.getString(fieldName);
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);

        }
        return value;
    }

    public static String executeQuerySingleValueString(String sql, String fieldName) {
        String value = "";
        Connection con = null;
        Statement stat = null;

        try {
            con = DBConnection.getConnection();
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                value = res.getString(fieldName);
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
            close(con);

        }
        return value;
    }

    public static int executeQuerySingleValueInt(String sql, String fieldName, Connection con) {
        int value = -1;
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            if (res.next()) {
                value = res.getInt(fieldName);
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);

        }
        return value;
    }

    public static HashMap<String, String> getMap(Connection con, String strQuery) {
        HashMap<String, String> returnHashMap = new HashMap<String, String>();
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(strQuery);
            while (res.next()) {



                String strKey = res.getString(1);
                String strValue = res.getString(2);
                returnHashMap.put(strKey, strValue);
            }
            res.close();

        } catch (Exception e) {
            System.out.println(strQuery);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return returnHashMap;
    }

    public static HashMap<String, String> getMapInt(Connection con, String strQuery) {
        HashMap<String, String> returnHashMap = new HashMap<String, String>();
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(strQuery);
            while (res.next()) {


                String strKey = res.getInt(1) + "";
                String strValue = res.getString(2);
                returnHashMap.put(strKey, strValue);
            }
            res.close();

        } catch (Exception e) {
            System.out.println(strQuery);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return returnHashMap;
    }

    public static void close(Connection conn) {
        if (conn == null) {
            return;

        }
        try {
            if (conn != null) {
                conn.close();

            }
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
    }

    public static void close(Statement stmt) {
        if (stmt == null) {
            return;

        }
        try {
            stmt.close();
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
    }

    public static void close(ResultSet res) {
        if (res == null) {
            return;

        }
        try {
            res.close();
        } catch (SQLException e) {
        }
    }

    public static void close(PreparedStatement pStmt) {
        if (pStmt == null) {
            return;

        }
        try {
            pStmt.close();
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
    }

    public static void close(CallableStatement pStmt) {
        if (pStmt == null) {
            return;

        }
        try {
            pStmt.close();
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
    }

    public static Long getSequenceNextVal(Connection argConnection, String argSequenceName) throws SQLException {
        Long lNextVal = new Long(0);
        Statement newStatement = null;
        ResultSet newResultSet = null;
        String sql = "select " + argSequenceName + ".nextval from dual";
        try {

            newStatement = argConnection.createStatement();
            newResultSet = newStatement.executeQuery(sql);
            if (newResultSet.next()) {
                lNextVal = new Long((long) newResultSet.getInt(1));
            }

        } catch (Exception objExp) {
            System.out.println(sql);
            objExp.printStackTrace();
            //Utility.logger.debug("getSequenceNextVal:  "+ objExp.toString());
        } finally {
            close(newResultSet);
            close(newStatement);
        }
        return lNextVal;
    }//end of getSequenceNextVal

    public static Long getSequenceNextVal(Statement stat, String argSequenceName) throws SQLException {
        Long lNextVal = new Long(0);
        //Statement newStatement=null;
        ResultSet newResultSet = null;
        String sql = "select " + argSequenceName + ".nextval from dual";
        try {
            newResultSet = stat.executeQuery(sql);
            if (newResultSet.next()) {
                lNextVal = new Long((long) newResultSet.getInt(1));
            }

        } catch (Exception objExp) {
            System.out.println(sql);
            objExp.printStackTrace();
            //Utility.logger.debug("getSequenceNextVal:  "+ objExp.toString());
        } finally {
            close(newResultSet);

        }
        return lNextVal;
    }

    public static <T extends MainModel> HashMap<Integer, T> executeSqlQueryMultiValueHashMap(String sql, String fieldPrimaryKey, Class<T> clazz, Connection con) {
        HashMap<Integer, T> valueHashMap = new HashMap<Integer, T>();
        Statement stat = null;
        try {
            stat = con.createStatement();
            ResultSet res = stat.executeQuery(sql);
            T tempModel = null;
            while (res.next()) {
                tempModel = clazz.newInstance();
                tempModel.fillInstance(res);
                valueHashMap.put(res.getInt(fieldPrimaryKey), tempModel);
//                valueVector.add(tempModel);
                tempModel = null;
            }
            res.close();
        } catch (Exception e) {
            System.out.println("sql:" + sql);
            e.printStackTrace();
        } finally {
            close(stat);
        }
        return valueHashMap;
    }

//    public static void main (String [] args)
//    {
//
//   HashMap <Integer,CategoryModel> testHM = executeSqlQueryMultiValueHashMap("select * from ADM_CATEGORY","CATEGORY_ID",CategoryModel.class,DBConnection.getConnection());
//        for (Integer id : testHM.keySet()) {
//            System.out.println("Category_id iss "+id);
//            System.out.println("Category_NAME iss "+testHM.get(id).getCategoryName());
//
//        }
//    }
    public static boolean executePreparedStatment(String sqlText, Connection con,
            Object... params) {
        PreparedStatement pStmt = null;
        boolean resultBoolean = false;
        try {
            pStmt = getPreparedStatement(sqlText, con, params);
            resultBoolean = pStmt.execute();

        } catch (Exception e) {
            System.out.println("error in the prepared sql:" + sqlText);
            printArray(params);
            e.printStackTrace();

        } finally {
            close(pStmt);
            return resultBoolean;
        }
    }

    public static PreparedStatement getPreparedStatement(String sqlText,
            Connection con, Object... params) throws SQLException {
        if (sqlText == null) {
            throw new IllegalArgumentException("Null sqlText not allowed.");
        }
        if (sqlText.equals("")) {
            throw new IllegalArgumentException("Blank sqlText not allowed.");
        }
        PreparedStatement pStmt = null;
        pStmt = con.prepareStatement(sqlText);

        for (int i = 0; i < params.length; i++) {
            pStmt.setObject(i + 1, params[i]);
        }
        return pStmt;
    }

    public static void printArray(Object... params) {
        System.out.println("***********************");
        for (int i = 0; i < params.length; i++) {
            System.out.println("Param " + (i + 1) + " = " + params[i]);
        }
        System.out.println("***********************");
    }
}
