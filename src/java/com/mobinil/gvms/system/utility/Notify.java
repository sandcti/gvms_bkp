//1.   History information.
/*
 **  FILE: Notify.java
 **
 **  ABSTRACT:
 **    This class is responsible for inserting tuples in the EBL_Mail and
 **    EBL_SMS tables in the E-Bill Database. It contains two methods, one is
 **    responsible for the EBL_Mail table, and the other is responsible for
 **    the EBL_SMS table. They are called from an external source.
 **
 **  DOCUMENTS:
 **
 **  AUTHOR:
 **    Khalid Reda - KR
 **
 **  REVISOR:
 **    Ashraf Fouad - AF
 **
 **  CREATION DATE:
 **    22/04/2001
 **
 **  NOTES:
 **    No applicable notes.
 **
 **  HISTORY:
 **    000 - Apr 22 - KR  - Creation
 **    001 - Apr 24 - AF  - "insertInMailTable" revised
 **    002 - Apr 24 - AF  - "insertInSMSTable" modified
 **    003 - May 05 - AF  - "insertInMailTable" update sequence name
 **    004 - May 05 - AF  - "insertInSMSTable" update sequence name 
 **
 */

//2.   Package identifier.
package com.mobinil.gvms.system.utility;

//3.   Import statements.
import java.util.Vector;
import java.sql.*;

import oracle.jdbc.driver.OracleTypes;
import java.io.*;


//4.   Class JavaDoc comments.
/**
 *  A class responsible for inserting tubles in the EBL_Mail and EBL_SMS in
 *  the E-Bill Database.
 *  For example:
 *  <pre>
 *    DBMailSMS oDBMailSMS = new DBMailSMS();
 *	  oDBMailSMS.insertInMailTable();
 *    oDBMailSMS.insertInSMSTable();
 *  </pre>
 *
 *  @author 	Khalid Reda
 *
 *  @Desired Improvements
 *    The method called 'insertInMailTable' should take the MailTO field as a parameter.
 *    The method called 'insertInSMSTable' should take RECEIVER_MSISDN, MESSAGE_TEXT,
 *    DISPATCH_DATE, and MESSAGE_PRIORITY as parameters.
 *
 **/


//5.   Class opening.
public class Notify extends Object{

//6.   Constants.
//7.   Instance variables.
//8.  Static initializers.
//9.  Main method.
//10. Static class methods.
//11. Static factory methods.
//12. Instance initializers.
//13. Constructor methods.

    public Notify( )
    {
    }


    public static int insertInSMSTable ( String szreceiverMSISDN,
                                         String szmessageText )
        
    {
    	//System.out.println("SMS String isssssssssssssssssssssssssssss "+szmessageText);
        int myint = -1;
        try
        {
        
          Connection conn = null;
          String strUsername="ebill";
          String strPassword="llibepass";   
          int iSMSBatchID=38978;
            // Get Db connection
          
          conn = SMSDbHelper.getDbConnection( );
          
          String sql = "{call SMS4PC_SESSION_PKG.Login (?,?,?,?)}";
          CallableStatement stat = null;
          stat = conn.prepareCall(sql);
          stat.setString(1, strUsername);
          stat.setString(2, strPassword);   
          stat.registerOutParameter(3, OracleTypes.NUMBER); 
          stat.registerOutParameter(4, OracleTypes.VARCHAR);
          
          stat.execute(); 
          
          myint = stat.getInt(3);
          String mystring = stat.getString(4);


          sql = "{call ? := SMS4PC_USER_SERVICE_PKG.AddCustomPendingSMSToBatch(?,?,?)}";
                stat = conn.prepareCall(sql);
          szreceiverMSISDN =  szreceiverMSISDN.startsWith("0") ? szreceiverMSISDN : "0"+szreceiverMSISDN;        
          stat.registerOutParameter(1, OracleTypes.NUMBER); 
          stat.setInt(2,iSMSBatchID  );
          stat.setString(3, szmessageText/*strSMS*/  );
          stat.setString(4,szreceiverMSISDN/*strRecipientMSISDN*/  );

          stat.execute();
          
          myint = stat.getInt(1);

          sql = "{call SMS4PC_SESSION_PKG.Logout(?,?)}";
          stat = conn.prepareCall(sql);
          stat.registerOutParameter(1, OracleTypes.NUMBER);
          stat.registerOutParameter(2, OracleTypes.VARCHAR);

          stat.execute();

          myint = stat.getInt(1);
          mystring = stat.getString(2);

        }
        catch(Exception e)
        {
           return -2;
            
        }


        return myint;
    }



}  // class DBMailSMS


