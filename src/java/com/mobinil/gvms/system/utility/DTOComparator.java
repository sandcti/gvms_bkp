package com.mobinil.gvms.system.utility;

import java.util.*;


public class DTOComparator implements Comparator<Object>
{

  // Init --------------------------------------------------------------------------------------

  public List<String> getters;
  public boolean ascending;

  // Constructor -------------------------------------------------------------------------------

  /**
   * @param getter The name of the getter of the field to sort on.
   * @param ascending The sort order: true = ascending, false = descending.
   */
  public DTOComparator(String getter, boolean ascending)
  {
    this.getters = new ArrayList<String>();
    for (String name: getter.split("\\."))
      {
        if (!name.startsWith("get"))
          {
            name = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
          }
        this.getters.add(name);
      }
    this.ascending = ascending;
  }

  /**
   * @param getter The name of the getter of the field to sort on.
   */
  public DTOComparator(String getter)
  {
    this(getter, true);
  }

  // Actions -----------------------------------------------------------------------------------

  /**
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  public int compare(Object o1, Object o2)
  {
    try
      {
        Iterator<String> iter = getters.iterator();
        while (o1 != null && o2 != null && iter.hasNext())
          {
            String getter = iter.next();
            o1 = o1.getClass().getMethod(getter, new Class[0]).invoke(o1, new Object[0]);
            o2 = o2.getClass().getMethod(getter, new Class[0]).invoke(o2, new Object[0]);
          }
      }
    catch (Exception e)
      {
        // If this exception occurs, then it is usually a fault of the DTO developer.
        throw new RuntimeException("Cannot compare " + o1 + " with " + o2 + " on " + getters, e);
      }

    if (o1 == null)
      {
        return ascending? -1: 1; // If ascending, current null first.
      }
    else if (o2 == null)
      {
        return ascending? 1: -1; // If ascending, another null first.
      }

    if (ascending)
      {
        return ((Comparable<Object>) o1).compareTo(o2); // Ascending.
      }
    else
      {
        return ((Comparable<Object>) o2).compareTo(o1); // Descending.
      }
  }

}
