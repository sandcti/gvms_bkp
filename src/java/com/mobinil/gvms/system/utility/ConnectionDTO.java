package com.mobinil.gvms.system.utility;

import java.sql.Connection;

public class ConnectionDTO
{
   public Connection con;
   public String sessionId;
   public void setCon(Connection con)
   {
      this.con = con;
   }
   public Connection getCon()
   {
      return con;
   }
   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }
   public String getSessionId()
   {
      return sessionId;
   }
}
