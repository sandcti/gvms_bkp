package com.mobinil.gvms.system.utility;

import java.sql.Timestamp;

import java.util.Date;

public class DateClass
{
  public DateClass()
  {
  }

  public String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    
    
    //System.out.println("time stamp is "+new Timestamp(System.currentTimeMillis()));
    return strCurrentDate;

  }

  public String convertDate(String values)
  {
    if (values == null)
      {
        ;
        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY')";
        return values;
      }
    else
      {
        String[] toDate = values.split("/");

        values = 
            "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY HH24:MI:SS')";
        //System.out.println(values);
        return values;
      }
  }


  public String convertDate(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;
        //        int houre = undefineDate.getHours();
        //        int min = undefineDate.getMinutes();
        //        int sec = undefineDate.getSeconds();

        values = "to_date('" + month + "/" + day + "/" + year + "', 'MM/DD/YYYY')";

        return values;
      }
  }

  public String convertDateHorMinSec(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;
        int houre = undefineDate.getHours();
        int min = undefineDate.getMinutes();
        int sec = undefineDate.getSeconds();

        values = 
            "to_date('" + month + "/" + day + "/" + year + " " + houre + ":" + min + ":" + sec + 
            "', 'MM/DD/YYYY H24:MI:SS')";

        return values;
      }
  }
  public static void main(String[] args)
  {
    //System.out.println(new DateClass().getDateTime());
  }
  

}
