package com.mobinil.gvms.system.utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;



import oracle.jdbc.OraclePreparedStatement;


public class ExecuteQueries
{

  public static int executeNoneQuery(String sql)
  {
	  
     Connection connection = null;
    Statement statement = null;
    int rows = 0;
    try
      {
        try
          {
            connection = DBConnection.getConnection();
          }
        catch (SQLException e)
          {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
            connection = DBConnection.getConnection();
          }

        try
          {
            statement = connection.createStatement();
          }
        catch (SQLException e)
          {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
            statement = connection.createStatement();
          }

        try
          {
            rows = statement.executeUpdate(sql);
          }
        catch (SQLException e)
          {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
        FileLogger.log(sql);
        //System.out.println(sql);
        statement.close();
        DBConnection.closeConnections ( connection );
      }
    catch (SQLException e)
      {
        // TODO Auto-generated catch block
        new com.mobinil.gvms.system.utility.PrintException().printException(e);

      }
    
    return rows;
  }

  public static boolean executeNoneQueryPreparedOracle(Connection connection , String sql, String var)
  {
	  
     
    OraclePreparedStatement statement = null;
    boolean rows = false;
    try
      {
        try
          {
            connection = DBConnection.getConnection();
          }
        catch (SQLException e)
          {
            FileLogger.log("Exception: Couldn't get Connection.");
            connection = DBConnection.getConnection();
          }

        try
          {
            statement = (OraclePreparedStatement) connection.prepareStatement(sql);
          }
        catch (SQLException e)
          {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
            statement = (OraclePreparedStatement) connection.prepareStatement(sql);
          }

        try
          {
            // rows = statement.executeUpdate(sql);           
            statement.setFormOfUse(1, OraclePreparedStatement.FORM_NCHAR);
            statement.setString(1, var);

            rows = statement.execute();

          }
        catch (SQLException e)
          {
            FileLogger.log("Exception: Error in the query: " + sql);
          }
        FileLogger.log(sql);
        //System.out.println(sql);
        statement.close();
      }
    catch (SQLException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        // TODO Auto-generated catch block
        FileLogger.log(e);

      }
    return rows;
  }


  public static ResultSet executeQuery(Connection connection,String sql)
  {
	      
    Statement statement = null;
    ResultSet result = null;
    try
      {
        

        try
          {
            statement = 
                connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
          }
        catch (SQLException e)
          {
            FileLogger.log("Exception: Couldn't creat statement.");
            statement = connection.createStatement();
            e.getSQLState();
            e.getMessage();
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
        FileLogger.log(sql);
        //System.out.println(sql);
        try
          {
            result = statement.executeQuery(sql);
          }
        catch (SQLException e)
          {
            e.printStackTrace();
            FileLogger.log("Exception: Error in the query: " + sql);
            e.getSQLState();
            e.getMessage();
            new com.mobinil.gvms.system.utility.PrintException().printException(e);


          }
      }
    catch (SQLException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        // TODO Auto-generated catch blocknew com.mobinil.gvms.system.utility.PrintException().printException(e);
        FileLogger.log(e);
      }
    return result;
  }

  public static boolean execute(Connection connection , String sql)
  {
	  
     
    Statement statement = null;
    boolean rows = false;
    try
      {
        try
          {
            connection = DBConnection.getConnection();
          }
        catch (SQLException e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            FileLogger.log("Exception: Couldn't get Connection.");
            connection = DBConnection.getConnection();
          }

        try
          {
            statement = connection.createStatement();
          }
        catch (SQLException e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            FileLogger.log("Exception: Couldn't creat statement.");
            statement = connection.createStatement();
          }

        try
          {
            rows = statement.execute(sql);

          }
        catch (SQLException e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            FileLogger.log("Exception: Error in the query: " + sql);
          }
        FileLogger.log(sql);
        //System.out.println(sql);
        statement.close();
      }
    catch (SQLException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        // TODO Auto-generated catch block
        FileLogger.log(e);

      }
    return rows;
  }


	/**
	 * @param args
	 */
	public static HashMap getSelectedData(String sql){
		HashMap hd = new HashMap();
		handler handle = new handler();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement st   = con.prepareStatement( sql );
			ResultSet rs=null;
			
			for(rs = st.executeQuery();rs.next();hd.put(rs.getString(1),handle.fillValues(rs)));
			
			rs.close();
			st.close();
			con.close();
			rs=null;
			st=null;
			con=null;
			
		} catch (SQLException e) {
			new com.mobinil.gvms.system.utility.PrintException().printException(e);
		}
		
		
		
		return hd;		
	} 
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		String sql = "select * from sfr_sim where rownum < 200000 order by Sim_Id";
//		HashMap hm = getSelectedData(sql);
//		ArrayList ss = (ArrayList)hm.get("250");
//		for (int i = 0; i < ss.size(); i++) {
//			if (ss.get(i)==null)//System.out.println("null");
//			else 
//			{
//				//System.out.println(ss.get(i).toString());
//			}
//			//System.out.println("-----------------------------");
//		}
//		try {
//			Thread.sleep(60000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//	}
//	
	
	static class handler{
		
		static String []temp;
		
		
		
		public static String [] getColumnNames(ResultSet rs) throws SQLException {
			////System.out.println("here one time only");
		    if (rs == null) {
		      return null;
		    }
		    ResultSetMetaData rsMetaData = rs.getMetaData();
		    int numberOfColumns = rsMetaData.getColumnCount();
		    String [] columnNames= new String [numberOfColumns];
		    
		    for (int i = 1; i < numberOfColumns + 1; i++) {
		      String columnName = rsMetaData.getColumnName(i);		      
		      columnNames[i-1]=columnName;
		      
		    }
		    return columnNames;
		  }
		public static Object [] fillValues(ResultSet rs){
			
			
			Object [] ss = null;			
			try {				
				if (rs.isFirst())temp = getColumnNames(rs);
					
				ss=new Object [temp.length+1];
			
			for (int i = 0; i < temp.length; i++) ss[i]=rs.getObject(temp[i]);
				
//			//System.out.println(ss[3]);

			
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				new com.mobinil.gvms.system.utility.PrintException().printException(e1);
			}
			
			return ss;
		}

	}
	


}
