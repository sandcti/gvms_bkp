package com.mobinil.gvms.system.utility;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class SendMail
{

  public String from;
  public String to;
  public String subject;
  public String text;

  public SendMail(String from, String to, String subject, String text,String replacedString)
  {
    this.from = jsfUtils.getParamterFromIni("from");
    this.to = to;
    this.subject = jsfUtils.getParamterFromIni("subject");
    this.text = jsfUtils.getParamterFromIni("textmail");
    this.text = this.text.replace("#1", replacedString);
  }

  public void send()
  {

    Properties props = new Properties();
//      props.put("mail.smtp.host", "webmail.sandcti.com");
    props.put("mail.smtp.host", jsfUtils.getParamterFromIni("mailhost"));
// props.put("mail.smtp.host", "mobinil@mobinil.com");
    props.put("mail.smtp.port", jsfUtils.getParamterFromIni("mailport"));

    Session mailSession = Session.getDefaultInstance(props);
    Message simpleMessage = new MimeMessage(mailSession);

    InternetAddress fromAddress = null;
    InternetAddress toAddress = null;
    try
      {
        fromAddress = new InternetAddress(from);
        toAddress = new InternetAddress(to);
      }
    catch (AddressException e)
      {
        // TODO Auto-generated catch block
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    try
      {
        simpleMessage.setFrom(fromAddress);
        simpleMessage.setRecipient(RecipientType.TO, toAddress);
        simpleMessage.setSubject(subject);
        simpleMessage.setText(text);

        Transport.send(simpleMessage);
      }
    catch (MessagingException e)
      {
        // TODO Auto-generated catch block
       new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
  }
  
  
}

