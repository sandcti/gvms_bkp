package com.mobinil.gvms.system.utility;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author M.AbdelAal
 */
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.Application;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;



import com.mobinil.gvms.system.user.DTO.*;

public class jsfUtils
{
  public static void putInSession(Object value, String key)
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
  }

  public static Object getFromSession(String key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
  }
  public static Object getFromRequest(String key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(key);
  }
  

  public static void putInApplication(String key,Object value )
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
  }

  public static Object getFromApplication(String key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
  }

  public static void redirect(String pageName, String contextRootName)
  {
    try
      {
    	 //System.out.println("/" + contextRootName + 
//                 "/faces/" + pageName +
//                 ".jsp");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/" + contextRootName + 
                                                                        "/faces/" + pageName + 
                                                                        ".jsp");
        
       
      }
    catch (IOException e)
      {
//System.out.println("error in redirect "+e.getMessage());
      }

  }
  public static void redirectGVMS(String pageName)
  {
    try
      {

        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/" + pageName );


      }
    catch (IOException e)
      {
//System.out.println("error in redirect "+e.getMessage());
      }

  }
  public static void redirectErrorPage(){
	  redirect("error", "GVMS");
	  
  }
  
  public static void redirectSessionOut(String pageName, String contextRootName) 
  {
    //System.out.println("/" + contextRootName + 
//	         "/faces/" + pageName +
//	         ".jsp");
	 FacesContext fc = FacesContext.getCurrentInstance();
	 Application ac = fc.getApplication();
	 NavigationHandler nha = ac.getNavigationHandler();
	 nha.handleNavigation(fc, null, "logoutAdminSession");
//    	 javax.faces.context.ExternalContext ec = fc.
//        ec.redirect("/" + contextRootName +"/faces/" + pageName +".jsp");

  }
  public static void dispatch(String pageName, String contextRootName)
  {
    try
      {
    	 //System.out.println("/" + contextRootName + 
//                 "/faces/" + pageName +
//                 ".jsp");
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/" + contextRootName + 
                                                                        "/faces/" + pageName + 
                                                                        ".jsp");
        
       
      }
    catch (IOException e)
      {
//System.out.println("error in redirect "+e.getMessage());
      }

  }
  public static Boolean haveSpecialCharacter(String name)
  {
     Pattern p = Pattern.compile("\\W");
     Matcher m = p.matcher(name.replace ( " " , "" ));
     return m.find();
    
  }
  
  public static String isValidAccount(String accountNumber)
  {
     String [] accountArray = accountNumber.split ( "\\." );         
     String strReturn = "";
     int firstPart = 0;
     if (accountArray.length==0 )
     {
        strReturn = "Account number at least have one dot.";   
     }
     else
        try
        {
           firstPart = Integer.parseInt ( accountArray[0] );
           if (firstPart==0)strReturn = "Account number must start with number in range 1-9.";
        }
        catch ( Exception e )
        {
           strReturn = "Account number must start with number in range 1-9.";
           // TODO: handle exception
        }
        
    

     if (strReturn.compareTo ( "" )==0)
     {
       
        if (firstPart!=1){
        if (accountArray[1].length ( )!=firstPart)strReturn = "Invalid account number.";
        }
     }
     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     if (strReturn.compareTo ( "" )==0)
     {
        if (accountArray.length>1){
           for ( int i = 1 ; i < accountArray.length ; i ++ )
           {
              try
              {
                 Integer.parseInt ( accountArray[i] );   
              }
              catch ( Exception e )
              {
                 strReturn = "Invalid account number.";
                 break;
                 // TODO: handle exception
              }
              
           }
           
        }
     }
     
     
     return strReturn;
     
    
  }
  public static boolean containInSession(Object key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey(key);
  }
  
  
  public static String getIp (){
  HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
  return req.getLocalAddr();
  
  }

  public static Boolean checkInSession(String key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey(key);
  }

  public static Boolean checkInApplication(String key)
  {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey(key);
  }

  public static String getURI()
  {
    HttpServletRequest req = 
      (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    return req.getRequestURI();
  }

  public static String getURLRequestServletPath()
  {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath();
  }

  public static String getURLRequestPathInfo()
  {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestPathInfo();
  }


  public static void removeFromSession(String key)
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
  }

  public static void removeFromApplication(String key)
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
  }

  public static void clearSession()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
  }
  
  public static String getParamterFromIni(String paramter){
    IniEditor dbParams = new IniEditor();
String dd ="";
    try
    {
        String x = new File("GVMS/DB_Config.ini").getAbsolutePath();
    //          String x = new File("DB_Config.ini").getAbsolutePath();

        //System.out.println("ini path is "+x);
        dbParams.load(x);
        
        dd = dbParams.get("dbgvms-prod", paramter);                        

        //System.out.println("x is "+x);
    }
    catch(Exception e)
    {
       
    }
    return dd;
  
  }
  public static ArabicNamesDTO getArabicParamterFromIni(){
	  ArabicNamesDTO andto = new ArabicNamesDTO();
	    IniEditor dbParams = new IniEditor();
	
	    try
	    {
	        String x = new File("GVMS/ArabicParamters.ini").getAbsolutePath();
	    //          String x = new File("DB_Config.ini").getAbsolutePath();

	        //System.out.println("ini path is "+x);
	        dbParams.load(x);
	        
	                                

	              andto.setArabic(dbParams.get("dbgvms-arparam", "goToArabic"));
	              andto.setUserName(dbParams.get("dbgvms-arparam", "usernamelabel"));
	              andto.setPassword(dbParams.get("dbgvms-arparam", "passwordlabel"));
	              andto.setForgetPassword(dbParams.get("dbgvms-arparam", "forgetpassword"));
	              andto.setWelcome(dbParams.get("dbgvms-arparam", "welcome"));
	              andto.setSearch(dbParams.get("dbgvms-arparam", "searching"));
	              andto.setHistory(dbParams.get("dbgvms-arparam", "lastoperation"));
	              andto.setAccount(dbParams.get("dbgvms-arparam", "useracount"));
	              andto.setExit(dbParams.get("dbgvms-arparam", "logout"));
	              andto.setVoucherNumber(dbParams.get("dbgvms-arparam", "vouchernumberlabel"));
	              andto.setVoucherNumber_(dbParams.get("dbgvms-arparam", "vouchernumber"));
	              andto.setMobileNumber(dbParams.get("dbgvms-arparam", "mobilenumber"));
	              andto.setTitleUnredeemed(dbParams.get("dbgvms-arparam", "redeeminggift"));
	              andto.setTitleRedeemed(dbParams.get("dbgvms-arparam", "giftredeemed"));
	              andto.setThisNumber(dbParams.get("dbgvms-arparam", "thisnumber"));
	              andto.setTakeVoucher(dbParams.get("dbgvms-arparam", "thisnumber_con"));
	              andto.setTitleHistory(dbParams.get("dbgvms-arparam", "history"));
	              andto.setPhoneNumber(dbParams.get("dbgvms-arparam", "phonenumber"));
	              andto.setRedeemtionDate(dbParams.get("dbgvms-arparam", "dateofredemption"));
	              andto.setGift(dbParams.get("dbgvms-arparam", "gift"));
	              andto.setGiftValue(dbParams.get("dbgvms-arparam", "giftvalue"));
	              andto.setGetPassword(dbParams.get("dbgvms-arparam", "retrivepassword"));
	              andto.setUserName_(dbParams.get("dbgvms-arparam", "usernamelabel2"));
	              andto.setPhoneNumber_(dbParams.get("dbgvms-arparam", "phonenumberlabel2"));
	              andto.setPosCode_(dbParams.get("dbgvms-arparam", "poscodelabel"));
	              andto.setEmail_(dbParams.get("dbgvms-arparam", "emaillabel"));
	              andto.setVoucherDateExpired(dbParams.get("dbgvms-arparam", "voucherexpired"));	              
	              andto.setTheTimeGone(dbParams.get("dbgvms-arparam", "giftdateexpried"));
	              andto.setInDate(dbParams.get("dbgvms-arparam", "indate"));
	              andto.setEditInfo(dbParams.get("dbgvms-arparam", "editinfo"));
	              andto.setCurrentPassword_(dbParams.get("dbgvms-arparam", "currentpasswordlabel"));
	              andto.setNewPassword_(dbParams.get("dbgvms-arparam", "newpasswordlabel"));
	              andto.setConfirmPassword_(dbParams.get("dbgvms-arparam", "confirmpasswordlabel"));
	              andto.setTitleHello(dbParams.get("dbgvms-arparam", "hello"));
	              andto.setTitleEnter(dbParams.get("dbgvms-arparam", "login"));
	              andto.setNext(dbParams.get("dbgvms-arparam", "next"));
	              andto.setLast(dbParams.get("dbgvms-arparam", "last"));
	              andto.setPrevious(dbParams.get("dbgvms-arparam", "previous"));
	              andto.setFirst(dbParams.get("dbgvms-arparam", "first"));
	              andto.setFrom(dbParams.get("dbgvms-arparam", "from"));
	              andto.setTo(dbParams.get("dbgvms-arparam", "to"));
	              andto.setFilter(dbParams.get("dbgvms-arparam", "search"));
	              andto.setLogoYear(dbParams.get("dbgvms-arparam", "footer1"));
	              andto.setLogoArabic(dbParams.get("dbgvms-arparam", "fotter2"));
	              andto.setForgetUserName(dbParams.get("dbgvms-arparam", "plzinsertusername"));
	              andto.setForgetPos(dbParams.get("dbgvms-arparam", "pleaseinsertpos"));
	              andto.setForgetPhone(dbParams.get("dbgvms-arparam", "pleaseinsertphone"));
	              andto.setForgetEmail(dbParams.get("dbgvms-arparam", "pleaseinsertmail"));
	              andto.setForgetAll(dbParams.get("dbgvms-arparam", "invaliddata"));
	              andto.setInsertValue(dbParams.get("dbgvms-arparam", "pleaseinsertdata"));
	              andto.setEmailInvalid1(dbParams.get("dbgvms-arparam", "emailnotcontainat"));
	              andto.setEmailInvalid2(dbParams.get("dbgvms-arparam", "emailnotcontaindot"));
	              andto.setEmailInvalid3(dbParams.get("dbgvms-arparam", "emailnotcontainspace"));
	              andto.setInvalidValues(dbParams.get("dbgvms-arparam", "invalideinfo"));
	              andto.setRedeemedBy(dbParams.get("dbgvms-arparam", "redeemedthrought"));
	              andto.setRedeemedOn(dbParams.get("dbgvms-arparam", "attime"));
	              andto.setRedeemedGift(dbParams.get("dbgvms-arparam", "gifttype"));
	              andto.setNewPassword(dbParams.get("dbgvms-arparam", "newpassword"));
	              andto.setConfirmNewPassword(dbParams.get("dbgvms-arparam", "confnewpassword"));
	              andto.setActivationCode(dbParams.get("dbgvms-arparam", "activationnumber"));
	              andto.setMobileforexample(dbParams.get("dbgvms-arparam", "mobileforexample"));
	        //System.out.println("x is "+x);
	    }
	    catch(Exception e)
	    {
	       
	    }
	    return andto;
	  
	  }
  
  public static ArabicErrorDTO getArabicErrorFromIni(){
	  ArabicErrorDTO aedto = new ArabicErrorDTO();
	    IniEditor dbParams = new IniEditor();
	
	    try
	    {
	        String x = new File("GVMS/ArabicParamters.ini").getAbsolutePath();
	    //          String x = new File("DB_Config.ini").getAbsolutePath();

	        //System.out.println("ini path is "+x);
	        dbParams.load(x);
	        
	        aedto.setInvalidusernameorpass(dbParams.get("dbgvms-arerror", "invalidusernameorpass"));
	        aedto.setPleasetypeusername(dbParams.get("dbgvms-arerror", "pleasetypeusername"));
	        aedto.setThisuserlocked(dbParams.get("dbgvms-arerror", "thisuserlocked"));
	        aedto.setInvalidpassword(dbParams.get("dbgvms-arerror", "invalidpassword"));
	        aedto.setUserexpired(dbParams.get("dbgvms-arerror", "userexpired"));
	        aedto.setInvalidvouchernumber(dbParams.get("dbgvms-arerror", "invalidvouchernumber"));
	        aedto.setPleaseinsertvouchernumber(dbParams.get("dbgvms-arerror", "pleaseinsertvouchernumber"));
	        aedto.setUpdateinfocomplete(dbParams.get("dbgvms-arerror", "updateinfocomplete"));
	        aedto.setPassandconfnotequal(dbParams.get("dbgvms-arerror", "passandconfnotequal"));
	        aedto.setInvalidoldpass (dbParams.get("dbgvms-arerror", "invalidoldpass"));
	        aedto.setPasslessthanlength(dbParams.get("dbgvms-arerror", "passlessthanlength"));
	        aedto.setPasshavetwosimilarchar(dbParams.get("dbgvms-arerror", "passhavetwosimilarchar"));
	        aedto.setPassdoenothavenumorchar(dbParams.get("dbgvms-arerror", "passdoenothavenumorchar"));
	        aedto.setPasshavetwoseqchar(dbParams.get("dbgvms-arerror", "passhavetwoseqchar"));
	        aedto.setPleaseinsetvalue(dbParams.get("dbgvms-arerror", "pleaseinsetvalue"));
	        aedto.setPassinlasthistory(dbParams.get("dbgvms-arerror", "passinlasthistory"));
	        aedto.setFollowsteps(dbParams.get("dbgvms-arerror", "followsteps"));
	        aedto.setStep1(dbParams.get("dbgvms-arerror", "step1"));
	        aedto.setStep2(dbParams.get("dbgvms-arerror", "step2"));
	        aedto.setStep3(dbParams.get("dbgvms-arerror", "step3"));
	        aedto.setStep4(dbParams.get("dbgvms-arerror", "step4"));
	        aedto.setStep5(dbParams.get("dbgvms-arerror", "step5"));
	        aedto.setStep6(dbParams.get("dbgvms-arerror", "step6"));
	        aedto.setStep7(dbParams.get("dbgvms-arerror", "step7"));
	        aedto.setEditinvalidpass(dbParams.get("dbgvms-arerror", "editinvalidpass"));
	        aedto.setInvaliddate(dbParams.get("dbgvms-arerror", "invaliddate"));
	        aedto.setInvalidnumber(dbParams.get("dbgvms-arerror", "invalidnumber"));
	        aedto.setValuerequired(dbParams.get("dbgvms-arerror", "valuerequired"));
	        aedto.setThisvaluerepeated(dbParams.get("dbgvms-arerror", "thisvaluerepeated"));
	        aedto.setInvalidtext(dbParams.get("dbgvms-arerror", "invalidtext")); 
	        aedto.setPleaseinsertcusphone(dbParams.get("dbgvms-arerror", "pleaseinsertcusphone"));
	        aedto.setInvaliddailwiththisvoucher(dbParams.get("dbgvms-arerror", "invaliddailwiththisvoucher"));
	        aedto.setPleaseselectonefrommenu(dbParams.get("dbgvms-arerror", "pleaseselectonefrommenu"));
	        aedto.setPleaseinsertvalue(dbParams.get("dbgvms-arerror", "pleaseinsertvalue"));
	        aedto.setRedemptionfailed(dbParams.get("dbgvms-arerror", "redemptionfailed"));
	        aedto.setRedemtoncomplete(dbParams.get("dbgvms-arerror", "redemtoncomplete"));
	        aedto.setNotauthorized(dbParams.get("dbgvms-arerror", "notauthorized"));
	        aedto.setRightactnumber(dbParams.get("dbgvms-arerror", "rightactnumber")); 
	        aedto.setNotrightactnumber(dbParams.get("dbgvms-arerror", "notrightactnumber")); 
	        aedto.setExpiredactivnumber(dbParams.get("dbgvms-arerror", "expiredactivnumber")); 
	        aedto.setPleaseinsertpassandcinf(dbParams.get("dbgvms-arerror", "pleaseinsertpassandcinf")); 
	        aedto.setPleasechangepassduetoinhistory(dbParams.get("dbgvms-arerror", "pleasechangepassduetoinhistory")); 
	        aedto.setUpdatepasscomplete (dbParams.get("dbgvms-arerror", "updatepasscomplete"));
	        aedto.setAlreadyRedeemed(dbParams.get("dbgvms-arerror", "alreadyredeemed"));                        


	        
	    }
	    catch(Exception e)
	    {
	       
	    }
	    return aedto;
	  
	  }
}
