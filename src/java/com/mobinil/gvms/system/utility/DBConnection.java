package com.mobinil.gvms.system.utility;

import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import oracle.jdbc.driver.OracleDriver; //import java.io.File;

public class DBConnection {

    public static Connection con = null;
    public static String strCon = "";
    public static String userName = "";
    public static String password = "";
    static int connectionsCounter = 0;
    static int threadConnectionsCounter = 0;
    static ArrayList<ConnectionDTO> arrayOfConnection = new ArrayList<ConnectionDTO>();

    static {
        IniEditor dbParams = new IniEditor();

        try {
            String x = new File("GVMS/DB_Config.ini").getAbsolutePath();
//          String x = new File("DB_Config.ini").getAbsolutePath();

            //System.out.println("ini path is " + x);
            dbParams.load(x);

            strCon = dbParams.get("dbgvms-prod", "Connection");
            userName = dbParams.get("dbgvms-prod", "userName");
            password = dbParams.get("dbgvms-prod", "password");

            //System.out.println("x is " + x);
        } catch (Exception e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }

    }

    public static synchronized void cleanResultSet(ResultSet rs) {

        Statement st = null;
        try {
            if (rs != null) {
                st = rs.getStatement();
            }
            st.close();
            rs.close();
        } catch (SQLException e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
            rs = null;
            st = null;
        } finally {
            rs = null;
            st = null;
        }


    }

    public static synchronized String getSequenceNextVal(Connection con, String argSequenceName) throws SQLException {
        Long lNextVal = new Long(0);
        try {
            String strGetNextValQuery = "select " + argSequenceName + ".nextval from dual";

            ResultSet newResultSet = ExecuteQueries.executeQuery(con, strGetNextValQuery);
            while (newResultSet.next()) {
                lNextVal = new Long((long) newResultSet.getInt(1));
            }
            cleanResultSet(newResultSet);
        } catch (Exception objExp) {
            new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
            //System.out.println(objExp.getMessage());
        }
        return lNextVal + "";
    }//end of getSequenceNextVal

    public static synchronized Connection getConnection()
            throws SQLException {





//      String dbString = "jdbc:oracle:thin:@" + hostName + ":1524:" + databaseName;

        java.util.Properties connAttr = new Properties();


        connAttr.setProperty("user", userName);
        connAttr.setProperty("password", password);


        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection(strCon, connAttr); // sand conn String
            String sessionId = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession(false).getId();
            ConnectionDTO cdto = new ConnectionDTO();
            cdto.setCon(con);
            cdto.setSessionId(sessionId);
            arrayOfConnection.add(cdto);
            connectionsCounter++;
//        //System.out.println ( "New connction opend conn "+connectionsCounter );
            if (con != null) {
                FileLogger.logString("DATABASE CONNECTION CREATED");
            } else {
                FileLogger.log((Object) "DATABSE CONNECTION NOT CREATED, PLEASE CHECK YOUR DATABASE");
            }
        } catch (Exception objExp) {
            new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
//            objExp.printStackTrace();
        }

        return con;
    }

    public static synchronized Connection getConnectionForThread()
            throws SQLException {





//    String dbString = "jdbc:oracle:thin:@" + hostName + ":1524:" + databaseName;

        java.util.Properties connAttr = new Properties();


        connAttr.setProperty("user", userName);
        connAttr.setProperty("password", password);


        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection(strCon, connAttr); // sand conn String

            threadConnectionsCounter++;
//      //System.out.println ( "New Thread connction opend conn "+threadConnectionsCounter );
//            if (con != null) {
//                FileLogger.logString("DATABASE CONNECTION CREATED");
//            } else {
//                FileLogger.log((Object) "DATABSE CONNECTION NOT CREATED, PLEASE CHECK YOUR DATABASE");
//            }
        } catch (Exception objExp) {
           new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
        }

        return con;
    }

    public static synchronized void closeConnection(Connection connection) {
        boolean isBreak=false;
        for (int i = 0; i < arrayOfConnection.size(); i++) {
            if (connection!=null && connection == arrayOfConnection.get(i).getCon()) {
                try {
                    arrayOfConnection.get(i).getCon().close();
                    arrayOfConnection.remove(i);
                    isBreak = true;
                } catch (SQLException ex) {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);
                }
            }
            if (isBreak) break;
        }
    }

    public static synchronized void closeConnections(Connection connection)
            throws SQLException {
        try {
//      //System.out.println ("Array of connection that will close is "+arrayOfConnection.size ( ));
            String sessionId = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession(false).getId();
            ArrayList<Integer> conRemoveIds = new ArrayList<Integer>();
            for (int i = 0; i < arrayOfConnection.size(); i++) {

                ConnectionDTO cdto = arrayOfConnection.get(i);

                if (cdto.getSessionId().compareTo(sessionId) == 0) {
                    connection = cdto.getCon();
                    if (connection != null) {
                        conRemoveIds.add(i);
                        connection.close();
                        connection = null;
                        connectionsCounter--;
//               //System.out.println ( "Old connction closed conn "+connectionsCounter );
                    }
                }

//         ConnectionDTO cdto1 = new ConnectionDTO();
//         cdto1.setCon ( null );
//         cdto1.setSessionId ( "" );


            }
            for (int i = 0; i < conRemoveIds.size(); i++) {
                arrayOfConnection.remove(conRemoveIds.get(i));
            }
        } catch (Exception objExp) {

           new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
        }


    }

    public static synchronized void closeThreadConnections(Connection connection)
            throws SQLException {
        try {


            if (connection != null) {
                connection.close();
                connection = null;
                threadConnectionsCounter--;
//             //System.out.println ( "Old connction closed conn "+threadConnectionsCounter);
            }







        } catch (Exception objExp) {
            connection = null;
            threadConnectionsCounter--;
            new com.mobinil.gvms.system.utility.PrintException().printException(objExp);
        }


    }
//  public static void main(String[] args)
//  {
//    try
//      {
////        
//        try
//              {
//                
//              }
//            catch (Exception e)
//              {
//              }
//
//      }
//    catch (Exception e)
//      {
//      }
//  }
}
