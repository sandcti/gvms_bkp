package com.mobinil.gvms.system.utility;





import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * Useful CSV utilities.
 * 
 * @author BalusC
 * @link http://balusc.blogspot.com/2006/06/parse-csv-upload.html
 */
public class CsvUtil
{

  // Init ---------------------------------------------------------------------------------------

  // Defaults.
  public static final char DEFAULT_CSV_SEPARATOR = ',';
  public static final String DEFAULT_LINE_SEPARATOR = "\r\n"; // CRLF.

  public CsvUtil()
  {
    // Utility class, hide the constructor.
  }

  // Parsers ------------------------------------------------------------------------------------

  /**
   * CSV content parser. Convert an InputStream with the CSV contents to a two-dimensional List
   * of Strings representing the rows and columns of the CSV. Each CSV record is expected to be
   * separated by the default CSV field separator, a comma.
   * @param csvInput The InputStream with the CSV contents.
   * @return A two-dimensional List of Strings representing the rows and columns of the CSV.
   */
  public static List<List<String>> parseCsv(InputStream csvInput,String validationType)
  {
    return parseCsv(csvInput, DEFAULT_CSV_SEPARATOR,validationType);
  }

  /**
   * CSV content parser. Convert an InputStream with the CSV contents to a two-dimensional List
   * of Strings representing the rows and columns of the CSV. Each CSV record is expected to be
   * separated by the specified CSV field separator.
   * @param csvInput The InputStream with the CSV contents.
   * @param csvSeparator The CSV field separator to be used.
   * @return A two-dimensional List of Strings representing the rows and columns of the CSV.
   */
  public static List<List<String>> parseCsv(InputStream csvInput, char csvSeparator,String validationType)
  {

    // Prepare.
    BufferedReader csvReader = new BufferedReader(new InputStreamReader(csvInput));
    List<List<String>> csvList = new ArrayList<List<String>>();
    String csvRecord;

    // Process records.
    try
      {
    	int counter=0;
        while ((csvRecord = csvReader.readLine()) != null)
          {
        	if (counter!=0)
        	{
        		if (csvRecord.startsWith(" "))
                {
        			csvRecord = csvRecord.trim();
                }

                if (csvRecord.compareTo("") != 0)
                {
                	List<String> oo = null;
                	if (validationType.compareTo("Integer") == 0)
                    {
                      try
                      {
                        Float ff = Float.parseFloat(csvRecord);
                        //write code here
                        oo = parseCsvRecord(csvRecord, csvSeparator);
                        if (!csvList.contains(oo))csvList.add(oo);

                      }
                      catch (Exception ex)
                      {
                        
                        new com.mobinil.gvms.system.utility.PrintException().printException(ex);
                      }
                    }
                	if (validationType.compareTo("String") == 0)
                    {                      
                		oo = parseCsvRecord(csvRecord, csvSeparator);
                        if (!csvList.contains(oo))csvList.add(oo);

                    }
                	
                }
            
        	}
        	counter++;
          }        
      }
    catch (IOException e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
        // This exception should never occur however as this should already be covered by the
        // source which feeds the InputStream to this method.
        throw new RuntimeException("Reading CSV failed.", e);
      }

    return csvList;
  }

//  public static ArrayList<csvFieldClass> parsCsv(java.io.InputStream csvInput, 
//                                                 Integer voucherStatus)
//  {
//
//    // Prepare.
//    BufferedReader csvReader = new BufferedReader(new InputStreamReader(csvInput));
//    ArrayList<csvFieldClass> csvList = new ArrayList<csvFieldClass>();
//    String csvRecord;
//
//    // Process records.
//    try
//      {
//        Integer Counter = 0;
//        FileLogger.logString("Date b4 parsing file " + new Date().toString());
//        while ((csvRecord = csvReader.readLine()) != null)
//          {
//            //          //System.out.println("voucher status is "+voucherStatus);
//            csvFieldClass cfc = new csvFieldClass();
//            String[] fields = csvRecord.split(",");
//            if (voucherStatus == 2)
//              {
//                cfc.setDialNumber(Integer.parseInt(fields[0]));
//                cfc.setVoucherNumber(Integer.parseInt(fields[1]));
//              }
//            //            if (voucherStatus==3){
//            //              cfc.setDialNumber(Integer.parseInt(fields[0]));
//            ////              cfc.setVoucherNumber(fields[1]);
//            //             }
//            csvList.add(cfc);
//            Counter++;
//            //System.out.println("counter in row " + Counter);
//          }
//        FileLogger.logString("Date after parsing file " + new Date().toString());
//      }
//    catch (IOException e)
//      {
//        // This exception should never occur however as this should already be covered by the
//        // source which feeds the InputStream to this method.
//        throw new RuntimeException("Reading CSV failed.", e);
//      }
//
//    return csvList;
//  }

  /**
   * CSV record parser. Convert a CSV record to a List of Strings representing the fields of the
   * CSV record. The CSV record is expected to be separated by the specified CSV field separator.
   * @param record The CSV record.
   * @param csvSeparator The CSV field separator to be used.
   * @return A List of Strings representing the fields of each CSV record.
   */
  public static List<String> parseCsvRecord(String record, char csvSeparator)
  {

    // Prepare.
    boolean quoted = false;
    StringBuilder csvBuilder = new StringBuilder();
    List<String> fields = new ArrayList<String>();

    // Process fields.
    for (int i = 0; i < record.length(); i++)
      {
        char c = record.charAt(i);
        csvBuilder.append(c);

        if (c == '"')
          {
            quoted = !quoted; // Detect nested quotes.
          }

        if // The separator ..
          ((!quoted && c == csvSeparator) || i + 1 == record.length()) // .. or, the end of record.
          {
            String field = // Obtain the field, ..
              // .. trim ending semicolon, ..
              // .. trim surrounding quotes, ..
              csvBuilder.toString().replaceAll(csvSeparator + "$", "").replaceAll("^\"|\"$", 
                                                                                  "").replace("\"\"", 
                                                                                              "\""); // .. and re-escape quotes.
            fields.add(field.trim()); // Add field to List.
            csvBuilder = new StringBuilder(); // Reset.
          }
      }

    return fields;
  }

  // Formatters --------------------------------------------------------------------------------

  /**
   * CSV content formatter. Convert a two-dimensional List of Objects to a CSV in an InputStream.
   * The value of each Object will be obtained by its toString() method. The fields of each CSV 
   * record will be separated by the default CSV field separator, a comma.
   * @param csvList A two-dimensional List of Objects representing the rows and columns of the
   * CSV.
   * @return The InputStream containing the CSV contents (actually a ByteArrayInputStream).
   */
  public static <T extends Object> InputStream formatCsv(List<List<T>> csvList)
  {
    return formatCsv(csvList, DEFAULT_CSV_SEPARATOR);
  }

  /**
   * CSV content formatter. Convert a two-dimensional List of Objects to a CSV in an InputStream.
   * The value of each Object will be obtained by its toString() method. The fields of each CSV
   * record will be separated by the specified CSV field separator.
   * @param csvList A two-dimensional List of Objects representing the rows and columns of the
   * CSV.
   * @param csvSeparator The CSV field separator to be used.
   * @return The InputStream containing the CSV contents (actually a ByteArrayInputStream).
   */
  public static <T extends Object> InputStream formatCsv(List<List<T>> csvList, char csvSeparator)
  {

    // Prepare.
    StringBuilder csvContent = new StringBuilder();

    // Process records.
    for (List<T> csvRecord: csvList)
      {
        if (csvRecord != null)
          {
            csvContent.append(formatCsvRecord(csvRecord, csvSeparator));
          }

        // Add default line separator.
        csvContent.append(DEFAULT_LINE_SEPARATOR);
      }

    return new ByteArrayInputStream(csvContent.toString().getBytes());
  }

  /**
   * CSV record formatter. Convert a List of Objects representing the fields of a CSV record to a
   * String representing the CSV record. The value of each Object will be obtained by its
   * toString() method. The fields of the CSV record will be separated by the specified CSV field
   * separator.
   * @param csvRecord A List of Objects representing the fields of a CSV reecord.
   * @param csvSeparator The CSV field separator to be used.
   * @return A String representing a CSV record.
   */
  public static <T extends Object> String formatCsvRecord(List<T> csvRecord, char csvSeparator)
  {

    // Prepare.
    StringBuilder fields = new StringBuilder();
    String separator = String.valueOf(csvSeparator);

    // Process fields.
    for (Iterator<T> iter = csvRecord.iterator(); iter.hasNext(); )
      {
        T object = iter.next();

        if (object != null)
          {
            String field = object.toString().replace("\"", "\"\""); // Escape quotes.

            if (field.contains(separator) || field.contains("\""))
              {
                field = "\"" + field + "\""; // Surround with quotes.
              }

            fields.append(field);
          }

        if (iter.hasNext())
          {
            fields.append(separator); // Add field separator.
          }
      }

    return fields.toString();
  }

}
