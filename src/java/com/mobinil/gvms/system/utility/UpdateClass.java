/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.gvms.system.utility;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.db.DBUtil;
import java.sql.Connection;

/**
 *
 * @author mabdelaal
 */
public class UpdateClass {
 static LoginDTO adminInfo =   (LoginDTO) jsfUtils.getFromSession("adminInfo");

    public static void updateAdminInfo( String adminId,  Boolean isModified) {
        StringBuilder query = new StringBuilder("update GVMS_ADMIN set ");
        if (!isModified) {
            query.append("ADMIN_CREATED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), ADMIN_CREATION_DATE = sysdate ");
        } else {
            query.append("ADMIN_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),ADMIN_MODIFIED_DATE = sysdate");
        }
        query.append(" where ADMIN_ID='");
        query.append(adminId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateCampaignInfo( String campaignId,  Boolean isModified) {
        StringBuilder query = new StringBuilder("update GVMS_CAMPAIGN set ");
        if (!isModified) {
            query.append("CAMPAIGN_CREATEDBY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), CAMPAIGN_CREATION_DATE = sysdate ");
        } else {
            query.append("CAMPAIGN_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),CAMPAIGN_LAST_MODIFIED = sysdate");
        }
        query.append(" where CAMPAIGN_ID='");
        query.append(campaignId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateCSInfo( String csId,  Boolean isModified) {
        StringBuilder query = new StringBuilder("update GVMS_CS set ");
        if (!isModified) {
            query.append("USER_CREATED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), USER_CREATION_DATE = sysdate ");
        } else {
            query.append("USER_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),USER_MODIFIED_DATE = sysdate");
        }
        query.append(" where USER_ID='");
        query.append(csId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateGiftInfo( String giftId,  Boolean isModified) {

        StringBuilder query = new StringBuilder("update GVMS_GIFT set ");
        if (!isModified) {
            query.append("GIFT_CREATEDBY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), GIFT_CREATION_DATE = sysdate ");
        } else {
            query.append("GIFT_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),GIFT_MODIFIED_DATE = sysdate");
        }
        query.append(" where GIFT_ID='");
        query.append(giftId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateGiftTypeInfo( String giftTypeId,  Boolean isModified) {

        StringBuilder query = new StringBuilder("update GVMS_GIFT_TYPE set ");
        if (!isModified) {
            query.append("GIFT_CREATED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), GIFT_TYPE_CREATION = sysdate ");
        } else {
            query.append("GIFT_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),GIFT_MODIFIED_DATE = sysdate");
        }
        query.append(" where GIFT_TYPE_ID='");
        query.append(giftTypeId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateUserInfo( String userId,  Boolean isModified) {

        StringBuilder query = new StringBuilder("update GVMS_USER set ");
        if (!isModified) {
            query.append("USER_CREATED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), USER_CREATION_DATE = sysdate ");
        } else {
            query.append("USER_MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),USER_MODIFIED_DATE = sysdate");
        }
        query.append(" where USER_ID='");
        query.append(userId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateUserTypeInfo( String userTypeId,  Boolean isModified) {

        StringBuilder query = new StringBuilder("update GVMS_USER_TYPE set ");
        if (!isModified) {
            query.append("CREATED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), CREATION_DATE = sysdate ");
        } else {
            query.append("MODIFIED_BY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'),MODIFIED_DATE = sysdate");
        }
        query.append(" where USER_TYPE_ID='");
        query.append(userTypeId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
    public static void updateCampaignUserInfo(String campId ,Boolean isModified) {

        StringBuilder query = new StringBuilder("update GVMS_CAMPAIGN_USER set ");
        if (!isModified) {
            query.append("CAMPAIGN_USER_CREATEDBY = (select system_user_name from GVMS_SYSTEM_USER where SYSTEM_USER_ID='");
            query.append(adminInfo.getSysUserId());
            query.append("'), CAMPAIGN_USER_CREATION_DATE = sysdate ");
        } else {
            query.append("CAMPAIGN_USER_LAST_MODIFIED = sysdate");
        }
        query.append(" where CAMPAIGN_ID='");
        query.append(campId);
        query.append("'");
        DBUtil.executeSQLStat(query.toString());
    }
}
