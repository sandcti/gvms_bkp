package com.mobinil.gvms.system.utility;

import java.sql.*;
import java.sql.SQLException;

public class cleanDB
{
   
   
   public static void cleanDB() throws SQLException{
      Connection con = DBConnection.getConnectionForThread ( );
      Statement st = con.createStatement ( );
      st.execute ( "TRUNCATE TABLE GVMS_VALIDATION_LIST_ITEM"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_CAMPAIGN_USER"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_CAMPAIGN_GIFT"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_TEMP_CURRENT"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_CS_LOGS"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_CURRENT_VOUCHER_TEMP"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_CURRENT_VOUCHER"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_VALIDATION_LIST_ITEM"  );
      
      st.execute ( "TRUNCATE TABLE GVMS_VALIDATION_LIST_ITEM"  );
      
      st.executeUpdate ( "delete from GVMS_GIFT" );
      
      st.executeUpdate ( "delete from GVMS_GIFT_TYPE" );
      
      st.executeUpdate ( "delete from GVMS_ADDITIONAL_FIELD" );
      
      st.executeUpdate ( "delete from GVMS_VALIDATION_LIST" );
      
      st.executeUpdate ( "delete from GVMS_CAMPAIGN" );
      
          
      
      st.execute ( "DROP SEQUENCE GVMS.GVMS_REDEEMED_CAMPAIGN_SEQ_ID"  );
      
      st.execute ( "DROP SEQUENCE GVMS.GVMS_GIFT_TYPE_SEQ_ID"  );
      
      st.execute ( "DROP SEQUENCE GVMS.GVMS_GIFT_SEQ_ID"  );
      
      st.execute ( "DROP SEQUENCE GVMS.GVMS_ADDITIONAL_FIELD_SEQ_ID"  );
      
      st.execute ( "DROP SEQUENCE GVMS.GVMS_CAMPAIGN_SEQ_ID"  );

      
String sql = "CREATE SEQUENCE GVMS.GVMS_REDEEMED_CAMPAIGN_SEQ_ID"+
        " START WITH 1"+
        " MAXVALUE 99999999999999"+
        " MINVALUE 1"+
        " NOCYCLE"+
        " NOCACHE"+
        " NOORDER";
        
st.execute (sql);
        

        sql = " CREATE SEQUENCE GVMS.GVMS_GIFT_TYPE_SEQ_ID"+
        " START WITH 1"+
        " MAXVALUE 99999999999999"+
        " MINVALUE 1"+
        " NOCYCLE"+
        " NOCACHE"+
        " NOORDER";
        st.execute (sql);

     

sql = "CREATE SEQUENCE GVMS.GVMS_GIFT_SEQ_ID"+
        " START WITH 1"+
        " MAXVALUE 999999999999999"+
        " MINVALUE 1"+
        " NOCYCLE"+
        " NOCACHE"+
        " NOORDER";
st.execute (sql);

     

sql = "CREATE SEQUENCE GVMS.GVMS_ADDITIONAL_FIELD_SEQ_ID"+
        " START WITH 1"+
        " MAXVALUE 999999999"+
        " MINVALUE 1"+
        " NOCYCLE"+
        " NOCACHE"+
        " NOORDER";
        
st.execute (sql);
        



sql = "CREATE SEQUENCE GVMS.GVMS_CAMPAIGN_SEQ_ID"+
        " START WITH 1"+
        " MAXVALUE 99999999999999"+
        " MINVALUE 1"+
        " NOCYCLE"+
        " NOCACHE"+
        " NOORDER";

st.execute (sql);

        

      
      st.executeUpdate ( "INSERT INTO GVMS_GIFT ( GIFT_ID, GIFT_NAME, GIFT_TYPE_ID, GIFT_VALUE, GIFT_DESCRIPTON,"+
      "GIFT_CREATION_DATE, GIFT_STATUS_ID, GIFT_CREATEDBY, GIFT_MODIFIED_BY, GIFT_MODIFIED_DATE,"+
      "GIFT_NAME_ARABIC ) VALUES ( "+
      "0, 'Any', NULL, 0, NULL, NULL, 3, NULL, NULL, NULL, NULL)" );
      
      
      st.close ( );
      DBConnection.closeThreadConnections  ( con );
      
   }
   
   
   /**
    * @param args
    * @throws SQLException 
    */
   public static void main(String[] args) throws SQLException
   {
      cleanDB();
      
      
      // TODO Auto-generated method stub
      
   }
   
}
