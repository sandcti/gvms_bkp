package com.mobinil.gvms.system.utility;

public interface ConstantsKeys
{
   public static final Integer CAMPAIGN_STATUS_DIAL_AND_GENERATE_VOUCHER = 3;
   public static final Integer CAMPAIGN_STATUS_VOUCHER_ONLY = 1;
   public static final Integer CAMPAIGN_STATUS_DIAL_AND_VOUCHER = 2;
}
