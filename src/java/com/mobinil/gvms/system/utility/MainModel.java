package com.mobinil.gvms.system.utility;

import java.io.Serializable;
import java.sql.*;

public abstract class MainModel implements Serializable {

    /**
     *
     */
    public static final long serialVersionUID = 1L;

    public abstract void fillInstance(ResultSet res);
}
