package com.mobinil.gvms.system.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;


public class FileLogger
{
  
  public static String path="";
  static {
    path = jsfUtils.getParamterFromIni("logfile");    
    FileLogger.log((Object) "loading class : Logger");
  }
  

  public static synchronized void log(Object obj)
  {
    if (obj instanceof String)
      {
        logString((String) obj);
      }
    else if (obj instanceof Exception)
      {
        logException((Exception) obj);
      }
  }

  public static void logString(String s)
  {
    BufferedWriter bufferedwriter = null;
    try
      {      

        


        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date1 = new java.util.Date();
        bufferedwriter = 
            new BufferedWriter(new FileWriter(path + formatter.format(date1) + "-SAND.log", true));
        bufferedwriter.write(now() + "  " + new String(s.getBytes("UTF-8")));
        bufferedwriter.newLine();
        bufferedwriter.flush();
      }
    catch (IOException ioexception)
      {
        //System.out.println("Error Writing to Log File: 1 :" + ioexception.getMessage());
//        ioexception.printStackTrace();
      }
    finally
      {
        if (bufferedwriter != null)
          try
            {
              bufferedwriter.close();
            }
          catch (IOException _ex)
            {
            }
      }
  }

  public static void logException(Exception e)
  {
    BufferedWriter bufferedwriter = null;
    try
      {
        

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date1 = new java.util.Date();
        bufferedwriter = 
            new BufferedWriter(new FileWriter(path + formatter.format(date1) + "-SAND.log", true));
        PrintWriter printWriter = new PrintWriter(bufferedwriter);
        e.printStackTrace(printWriter);

        bufferedwriter.newLine();
        bufferedwriter.flush();
        printWriter.close();
      }
    catch (IOException ioexception)
      {
        //System.out.println("Error Writing to Log File: 2 :" + ioexception.getMessage());
        
      }
    finally
      {
        if (bufferedwriter != null)
          try
            {
              bufferedwriter.close();
            }
          catch (IOException _ex)
            {
            }
      }
  }


  public static String now()
  {
    Calendar calendar = Calendar.getInstance();
    long l = System.currentTimeMillis();
    String s = 
      calendar.get(1) + "-" + (calendar.get(2) + 1) + "-" + calendar.get(5) + " " + calendar.get(11) + 
      ":" + calendar.get(12) + ":" + calendar.get(13);
    return s;
  }
}
