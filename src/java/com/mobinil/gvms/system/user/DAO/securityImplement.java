package com.mobinil.gvms.system.user.DAO;

import com.mobinil.gvms.system.user.beans.Login;
import com.mobinil.gvms.system.utility.*;

import java.io.UnsupportedEncodingException;

import java.security.NoSuchAlgorithmException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class securityImplement {
    

    public static String checkPass(String path, String password, 
                                   String userName) throws SQLException {

       Connection conn = DBConnection.getConnection();
//        ResultSet errorRS = 
//            ExecuteQueries.executeQuery(conn,"Select ERROR_DESCRIPTION from GVMS_ERROR where ERROR_PAGE_NAME='EditArabic' order by id");
//        ArrayList<String> errorArray = new ArrayList<String>();
//
//        path = (String)jsfUtils.getFromSession("userTypeInterface");
//        try {
//            while (errorRS.next()) {
//                errorArray.add(errorRS.getString("ERROR_DESCRIPTION"));
//            }
//            errorRS.close();
//        } catch (SQLException e) {
//
//        }


        if (!new PasswordUtils().checkLength(conn,(password))) {
            if (path.compareTo("Ar")==0) {
               DBConnection.closeConnections ( conn );
                return Login.getArabicErrorDTO().getPasslessthanlength();

            } else {
               DBConnection.closeConnections ( conn );
                return "invalid password length.";


            }


        } else if (!new PasswordUtils().checkSequanceAlphabetic(password)) {
            if (path.compareTo("Ar")==0) {
               DBConnection.closeConnections ( conn );
                return Login.getArabicErrorDTO().getPasshavetwoseqchar();

            } else {DBConnection.closeConnections ( conn );
                return "Invalid password sequence characters.";


            }


        } else if (!new PasswordUtils().checkIdInPassword(password, userName)) {
            if (path.compareTo("Ar")==0) {
               DBConnection.closeConnections ( conn );
                return Login.getArabicErrorDTO().getInvalidpassword();

            } else {DBConnection.closeConnections ( conn );
                return "Invalid password.";

            }


        } else if (!new PasswordUtils().checkSimilarChar(password)) {
            if (path.compareTo("Ar")==0) {
               DBConnection.closeConnections ( conn );
                return Login.getArabicErrorDTO().getPasshavetwosimilarchar();

            } else {DBConnection.closeConnections ( conn );
                return "Invalid password similar characters.";


            }


        } else if (!new PasswordUtils().checkStringWithTxtAndNum(password)) {
            if (path.compareTo("Ar")==0) {
               DBConnection.closeConnections ( conn );
                return Login.getArabicErrorDTO().getPassdoenothavenumorchar();

            } else {
               DBConnection.closeConnections ( conn );
                return "Invalid password not contain text or numbers.";


            }


        }

        else if (!new PasswordUtils().checkUpLow(password)) {
        	
        	
        	 if (path.compareTo("Ar")==0) {
        		 DBConnection.closeConnections ( conn );
        		 return  "Invalid password lower and upper cases.";
        	 }
        	 else{
        	
           DBConnection.closeConnections ( conn );
            return "Invalid password lower and upper cases.";}


        }
        DBConnection.closeConnections ( conn );
        return "";

    }

    public static String checkPasswordInDB(String newPassword, 
                                           Integer sysUserId) {

        int lastPass = 0;
        int updateUser = 0;
        int expireDaysCount =0;
        try {
           Connection conn = DBConnection.getConnection();
            String passEncripted = MD5Class.MD5(newPassword);

            ResultSet lastPassRS = 
                ExecuteQueries.executeQuery(conn,"select PROPERTIES from GVMS_PROPERTIES where reasone='LAST_PASSWORD_COUNT'");
            if (lastPassRS.next()) {
                lastPass = lastPassRS.getInt("PROPERTIES");

            }
          lastPassRS.getStatement().close();;
            lastPassRS.close();
            
          ResultSet expireDaysRS = 
              ExecuteQueries.executeQuery(conn,"select PROPERTIES from GVMS_PROPERTIES where reasone='DAYS_FOR_EXPIRED_LOGIN'");
          if (expireDaysRS.next()) {
              expireDaysCount = expireDaysRS.getInt("PROPERTIES");

          }
          expireDaysRS.getStatement().close();;
          expireDaysRS.close();

            ResultSet checkPassExists = 
                ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_ID from gvms_PASS_TRACE  where PASSWORD='" + 
                                            passEncripted + 
                                            "' and SYSTEM_USER_ID = " + 
                                            sysUserId);


            if (checkPassExists.next()) {
DBConnection.closeConnections ( conn );
                return ("this password included in your last 6 passwords.");

            }
            checkPassExists.getStatement().close();
            checkPassExists.close();

            ResultSet chckLst6Pswrd = 
                ExecuteQueries.executeQuery(conn,"select Max(PASSWORD_INDEX) passCount from gvms_PASS_TRACE where SYSTEM_USER_ID = " + 
                                            sysUserId + "");


            if (chckLst6Pswrd.next()) {
                if (chckLst6Pswrd.getInt("passCount") > 0 && 
                    chckLst6Pswrd.getInt("passCount") < (lastPass + 1)) {
                    if (chckLst6Pswrd.getInt("passCount") == lastPass) {                         
                      String SQL = 
                        "update GVMS_SYSTEM_USER set IS_LOCKED=0,USER_CODE='',SYSTEM_LOGIN_END_DATE=sysdate+" + 
                        expireDaysCount + ",SYSTEM_USER_PASSWORD='" + MD5Class.MD5(newPassword) + 
                        "' where SYSTEM_USER_ID=" + sysUserId;
                     updateUser= ExecuteQueries.executeNoneQuery(SQL);

                        if (updateUser == 0) {
                           DBConnection.closeConnections ( conn );
                            return ("== 6 Invalid update system user table");

                        } else {

                            int updatePassTrace = 
                                ExecuteQueries.executeNoneQuery("insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
                                                                sysUserId + 
                                                                ",'" + 
                                                                passEncripted + 
                                                                "'," + 
                                                                (chckLst6Pswrd.getInt("passCount") + 
                                                                 1) + ")");
                            if (updatePassTrace == 0) {
                               DBConnection.closeConnections ( conn );
                                return ("== 6 Invalid update Pass trace table");

                            } else {

                                //System.out.println("result of upadate system  user is " + 
//                                                   updateUser);

                                updatePassTrace = 
                                        ExecuteQueries.executeNoneQuery("update gvms_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-1 where SYSTEM_USER_ID=" + 
                                                                        sysUserId);
                                if (updatePassTrace == 0) {
                                   DBConnection.closeConnections ( conn );
                                    return ("== 6 Invalid update Pass trace table to set 0 with 1");

                                } else {
                                    int deletePasstrace = 
                                        ExecuteQueries.executeNoneQuery("delete from gvms_PASS_TRACE where SYSTEM_USER_ID =" + 
                                                                        sysUserId + 
                                                                        " and PASSWORD_INDEX=0");
                                    if (deletePasstrace == 0) {
                                       DBConnection.closeConnections ( conn );
                                        return ("== 6 Invalid delete Pass trace table row 0");

                                    } else {
                                       DBConnection.closeConnections ( conn );
                                        return "Changing information complete";


                                    }
                                }
                            }
                        }


                    } else {
                      String SQL = 
                        "update GVMS_SYSTEM_USER set IS_LOCKED=0,USER_CODE='',SYSTEM_LOGIN_END_DATE=sysdate+" + 
                        expireDaysCount + ",SYSTEM_USER_PASSWORD='" + MD5Class.MD5(newPassword) + 
                        "' where SYSTEM_USER_ID=" + sysUserId;
                      updateUser= ExecuteQueries.executeNoneQuery(SQL);

                        if (updateUser == 0) {
                           DBConnection.closeConnections ( conn );
                            return ("> 6 Invalid update system user table");

                        } else {

                            int updatePassTrace = 
                                ExecuteQueries.executeNoneQuery("insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
                                                                sysUserId + 
                                                                ",'" + 
                                                                passEncripted + 
                                                                "'," + 
                                                                (chckLst6Pswrd.getInt("passCount") + 
                                                                 1) + ")");
                            if (updatePassTrace == 0) {
                               DBConnection.closeConnections ( conn );
                                return ("> 6 Invalid update Pass trace table");

                            } else {
                               DBConnection.closeConnections ( conn );

                                return "Changing information complete";


                            }
                        }


                    }
                }
            }
            chckLst6Pswrd.getStatement().close();
            chckLst6Pswrd.close();
            DBConnection.closeConnections ( conn );
        } catch (UnsupportedEncodingException e) {

        } catch (NoSuchAlgorithmException e) {

        } catch (SQLException e) {

        }
        return "";

    }
}
