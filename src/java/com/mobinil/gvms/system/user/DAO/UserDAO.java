package com.mobinil.gvms.system.user.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.jsfUtils;

public class UserDAO {
public static boolean checkUserTypeStatus(String userTypeId) throws SQLException
{
   
boolean userTypeStatus = false;
Connection conn = DBConnection.getConnection();
ResultSet rs = ExecuteQueries.executeQuery(conn,"Select user_type_status_id from GVMS_User_Type where user_type_id ="+userTypeId);

	while (rs.next())
	{
	int status = rs.getInt(1);	
	if (status !=2)userTypeStatus=true;
	}
DBConnection.cleanResultSet(rs);
DBConnection.closeConnections ( conn );
return userTypeStatus;
}
public static boolean checkVoucherExpiry(String voucherNumber)  throws SQLException
{
 
 Timestamp currentDate = null;
 Timestamp voucherExpiryDate = null;
 String defaultDaysToExpireCount = jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" );
 
 String sql = "select VOUCHER_EXPIRY_DATE,SYSDATE,(GC.CAMPAIGN_START_DATE + "+defaultDaysToExpireCount+") defaultDaysExpire from GVMS_CURRENT_VOUCHER GCV,GVMS_CAMPAIGN GC where CURRENT_VOUCHER_NUMBER='"+voucherNumber+"'" +
      " and GCV.CAMPAIGN_ID = GC.CAMPAIGN_ID ";
 Connection conn = DBConnection.getConnection();
 ResultSet checkExpiryRS = ExecuteQueries.executeQuery(conn,sql);
 if (checkExpiryRS.next ( )){
    currentDate =checkExpiryRS.getTimestamp ( "SYSDATE" );
    voucherExpiryDate =checkExpiryRS.getTimestamp ( "VOUCHER_EXPIRY_DATE" );      
 }
 DBConnection.cleanResultSet (  checkExpiryRS );
 DBConnection.closeConnections ( conn );
     if (voucherExpiryDate==null)voucherExpiryDate = checkExpiryRS.getTimestamp ( "defaultDaysExpire" );
   
     if (currentDate.after ( voucherExpiryDate )) return false;
     else return true;
}
}
