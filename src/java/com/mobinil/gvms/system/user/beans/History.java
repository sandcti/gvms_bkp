package com.mobinil.gvms.system.user.beans;



import com.mobinil.gvms.system.user.DTO.HistoryDTO;

import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;


public class History
{
  public HtmlDataTable dataTable1;
  public ArrayList<HistoryDTO> historyArray;
  public String sortField = null;
  public boolean sortAscending = true;
  public Integer rowCount = 10;
  public Date filterFromDate;
  public Date filterToDate;
    public String usrIdHiden;
  
  public String msgWarn;
  String userTypeId;
  String userId;
  String path;
  int pageIndex = 0;
  ArrayList<Integer> campaignsIds;

  public History() throws SQLException
  {
  String path = (String)jsfUtils.getFromSession("userTypeInterface");
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {try
            {
              FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
            }
          catch (Exception e)
            {new com.mobinil.gvms.system.utility.PrintException().printException(e);
               
            }} 

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
               {
                 FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
               }
             catch (Exception e)
               {
                  new com.mobinil.gvms.system.utility.PrintException().printException(e);
               }
         }
      }


    filterFromDate = new Date();
    //System.out.println(filterFromDate);
    filterToDate = new Date();
    //System.out.println(filterToDate );
    historyArray = new ArrayList<HistoryDTO>();
    campaignsIds = new ArrayList<Integer>();

    userTypeId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeId");

    userId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId");

    String SQL = "Select campaign_id from GVMS_campaign_User where user_type_id=" + userTypeId;
    Connection conn = DBConnection.getConnection();
    try
      {
       
      ResultSet camapignsIdsRS = ExecuteQueries.executeQuery(conn,SQL);
        while (camapignsIdsRS.next())
          {
            campaignsIds.add(camapignsIdsRS.getInt("campaign_id"));
          }
        camapignsIdsRS.getStatement().close();
        camapignsIdsRS.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    //    for (int count = 0; count < campaignsIds.size(); count++)
    //      {
    SQL = 
        "select t1.CURRENT_VOUCHER_DIAL_NUMBER,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY') VOUCHER_DATE_TIME ,t2.GIFT_VALUE,t2.GIFT_NAME from GVMS_CURRENT_VOUCHER" + 
        " t1 left join GVMS_GIFT t2 on t1.GIFT_ID = t2.GIFT_ID where t1.USER_TYPE_ID=" + 
        userTypeId + " AND t1.USER_ID=" + userId + " and t1.CURRENT_VOUCHER_END_DATE >=" + 
        convertDate(filterFromDate) + " And t1.CURRENT_VOUCHER_END_DATE <= (" + 
        convertDate(filterToDate) + "+1) and t1.VOUCHER_STATUS_ID=1";
    //System.out.println("in Con" + SQL);
    ResultSet historyRs = ExecuteQueries.executeQuery(conn,SQL);

    try
      {
        while (historyRs.next())
          {
            HistoryDTO hc = new HistoryDTO();
            hc.setDialNumber(historyRs.getInt("CURRENT_VOUCHER_DIAL_NUMBER"));
            hc.setGiftName(historyRs.getString("GIFT_NAME"));
            hc.setGiftValue(historyRs.getInt("GIFT_VALUE"));
            hc.setVoucherDate(historyRs.getString("VOUCHER_DATE_TIME"));
            historyArray.add(hc);

          }
        historyRs.getStatement().close();
        historyRs.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    //
    //      }

DBConnection.closeConnections ( conn );
  }

  public String goFilter_Action()
  {

    historyArray.clear();


    if (filterFromDate != null && filterToDate != null)
      {
        //    for (int count = 0; count < campaignsIds.size(); count++)
        //      {
        String SQL = 
          "select t1.CURRENT_VOUCHER_DIAL_NUMBER,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY') VOUCHER_DATE_TIME ,t2.GIFT_VALUE,t2.GIFT_NAME from GVMS_CURRENT_VOUCHER" + 
          " t1 left join GVMS_GIFT t2 on t1.GIFT_ID = t2.GIFT_ID where t1.USER_TYPE_ID=" + 
          userTypeId + " AND t1.USER_ID=" + userId + " and t1.CURRENT_VOUCHER_END_DATE >=" + 
          convertDate(filterFromDate) + " And t1.CURRENT_VOUCHER_END_DATE <= (" + 
          convertDate(filterToDate) + "+1) and t1.VOUCHER_STATUS_ID=1";

        //System.out.println("in go filter" + SQL);
        

        try
          {
           Connection conn = DBConnection.getConnection();
           ResultSet historyRs = ExecuteQueries.executeQuery(conn,SQL);
            while (historyRs.next())
              {
                HistoryDTO hc = new HistoryDTO();
                hc.setDialNumber(historyRs.getInt("CURRENT_VOUCHER_DIAL_NUMBER"));
                hc.setGiftName(historyRs.getString("GIFT_NAME"));
                hc.setGiftValue(historyRs.getInt("GIFT_VALUE"));
                hc.setVoucherDate(historyRs.getString("VOUCHER_DATE_TIME"));
                historyArray.add(hc);

              }
            historyRs.getStatement().close();
            historyRs.close();
            DBConnection.closeConnections ( conn );
          }
        catch (SQLException e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }


        //      }
      }
    else
      {
        msgWarn = "Please insert From date To date";
      }


    return null;

  }

  public String convertDate(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'mm/dd/yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;

        values = "to_date('" + month + "/" + day + "/" + year + "', 'mm/dd/yyyy')";

        return values;
      }
  }


  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {

        try
          {
            Collections.sort(historyArray, new DTOComparator(sortField, sortAscending));
          }
        catch (Exception e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setHistoryArray(ArrayList<HistoryDTO> historyArray)
  {
    this.historyArray = historyArray;
  }

  public ArrayList<HistoryDTO> getHistoryArray()
  {
    return historyArray;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }

  public void setFilterFromDate(Date filterFromDate)
  {
    this.filterFromDate = filterFromDate;
  }

  public Date getFilterFromDate()
  {
    return filterFromDate;
  }

  public void setFilterToDate(Date filterToDate)
  {
    this.filterToDate = filterToDate;
  }

  public Date getFilterToDate()
  {
    return filterToDate;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }
}
