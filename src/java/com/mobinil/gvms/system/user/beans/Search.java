package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;

import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class Search
{
  public String voucherNumber;
  public final int ISNEW = 4;
  public final int ISACTIVE = 2;
  public final int ISEXPIRED = 3;
  public final int ISREDEEMED = 1;
  public String error;
  public HtmlOutputText errorEn;
    public String usrIdHiden;
  
  
  public String dialNumber;
  String path;

  public Search()
  {
    path = (String) jsfUtils.getFromSession("userTypeInterface");
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
      //System.out.println("hiden id in search is "+usrIdHiden);
      
    //System.out.println("path in search is " + path);
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar") == 0)
    {
      if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
        {
            try
            {
              FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
            }
            catch (Exception e)
            {
               
            }
        }

    }

    else
    {
      if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
        {
            try
            {
              FacesContext.getCurrentInstance().getExternalContext().redirect("faces/Login.jsp");
            }
            catch (Exception e)
            {
               
            }
        }
    }

  }


  public String search()
  {
    // Add event code here...
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Unredeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Expired_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Redeemed_Bean");
    Timestamp currentDate = null;
    Timestamp voucherExpiryDate = null;
    String defaultDaysToExpireCount = jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" );
    try
    {
      


      if (voucherNumber.compareTo("") == 0)
      {
        if (path.compareTo("Ar") == 0)
        {

          error = Login.getArabicErrorDTO().getPleaseinsertvouchernumber();
          //System.out.println(error);
          return null;
        }
        else
        {
          errorEn.setValue("Please insert voucher number");
          return null;
        }

      }

      try
      {
        Long.valueOf (voucherNumber);
      }
      catch (Exception e)
      {

        if (path.compareTo("Ar") == 0)
        {

          error = Login.getArabicErrorDTO().getInvalidvouchernumber();
          //System.out.println(error);
          return null;
        }
        else
        {
          errorEn.setValue("Invalid voucher number, please refer to Mobinil customer service.");
          return null;
        }
      }
      
//      if ()
      
      Connection conn = DBConnection.getConnection();
      ResultSet result = 
        ExecuteQueries.executeQuery(conn,"select t1.*,t2.CAMPAIGN_STATUS_ID status,t2.CAMPAIGN_NAME cam_name,t2.VOUCHER_TYPE_ID camp_type_id ,SYSDATE,(t2.CAMPAIGN_START_DATE + "+defaultDaysToExpireCount+") defaultDaysExpire from GVMS_CURRENT_VOUCHER t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID = t2.CAMPAIGN_ID where t2.CAMPAIGN_STATUS_ID!=1 and  t2.CAMPAIGN_STATUS_ID!=4 and t1.CURRENT_VOUCHER_NUMBER = '" + 
                                    voucherNumber + "'");

      if (result.next())
      {
        String campaignId = result.getString("CAMPAIGN_ID");
        String giftId = result.getString("GIFT_ID");
        String vouNumber = result.getString("CURRENT_VOUCHER_NUMBER");
        String customerCampaignId = result.getString("CURRENT_VOUCHER_ID");
        int status = result.getInt("VOUCHER_STATUS_ID");
        int campaignStatus = result.getInt("status");
        dialNumber = "" + result.getInt("camp_type_id");
        if (campaignStatus == 3 || campaignStatus == 4)
        {
          status = 3;
        }
        
        currentDate =result.getTimestamp ( "SYSDATE" );
        voucherExpiryDate =result.getTimestamp ( "VOUCHER_EXPIRY_DATE" );      
        
        if (voucherExpiryDate==null)voucherExpiryDate = result.getTimestamp ( "defaultDaysExpire" );
          
//        //System.out.println ("voucherExpiryDate is "+voucherExpiryDate);
//        //System.out.println ("currentDate is "+currentDate);
//        //System.out.println ("dialNumber is "+dialNumber);
//        //System.out.println ("status is "+status);
//        
            if (currentDate.after ( voucherExpiryDate ) || campaignStatus == 5)
            {
               if (path.compareTo("Ar") == 0)
               {

                 error = ((com.mobinil.gvms.system.user.DTO.ArabicNamesDTO)jsfUtils.getFromSession ( "ArabicParamtersObj" )).getTheTimeGone();                 
                 return null;
               }
               else
               {
                  errorEn.setValue("This voucher expired.");
                 return null;
               }
            
            
            }
            
        
        //System.out.println(campaignId + "/" + giftId + "/" + vouNumber + 
//                           "/" + customerCampaignId + "/" + status + "/");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ccid", 
                                                                                   "" + 
                                                                                   customerCampaignId);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("cid", 
                                                                                   "" + 
                                                                                   campaignId);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("gid", 
                                                                                   "" + 
                                                                                   giftId);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("campaignName", 
              result.getString("cam_name"));

//        ResultSet rs = 
//          ExecuteQueries.executeQuery("select * from GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
//                                      campaignId);
//        if (rs.next())
//        {
//          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("campaignName", 
//                                                                                     rs.getString("CAMPAIGN_NAME"));
//          dialNumber = "" + rs.getInt("VOUCHER_TYPE_ID");
//          //System.out.println(dialNumber);
//        }
//        rs.getStatement().close();
//        rs.close();
//          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(usrIdHiden,"systemUserId"); 
      if (status == ISACTIVE)
        {

          try
          {

            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("vouNum", 
                                                                                       "" + 
                                                                                       vouNumber);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("dialNum", 
                                                                                       "" + 
                                                                                       dialNumber);
                                                                                       

          }
          catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

//          try
//          {
            if (path.compareTo("Ar") == 0)
            {

              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/UnredeemedArabic.jsp");
              return "unredeemedAr";

            }
            else
            {
              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/Unredeemed.jsp");
              return "unredeemed";
            }
//          }
//          catch (IOException e)
//          {
//             
//          }
        }
        else if (status == ISEXPIRED)
        {
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("cid", 
                                                                                     "" + 
                                                                                     campaignId);
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("vouNum", 
                                                                                     "" + 
                                                                                     vouNumber);


//          try
//          {
            if (path.compareTo("Ar") == 0)
            {

              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/ExpiredArabic.jsp");
              return "expiredAr";
            }
            else
            {
              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/expired.jsp");
              return "expired";
            }
//          }
//          catch (IOException e)
//          {
//             
//          }


        }
        else if (status == ISREDEEMED)
        {
          try
          {


            if (path.compareTo("Ar") == 0)
            {

              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/RedeemedArabic.jsp");
               FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("vouNum", 
                                                                                          "" + 
                                                                                          vouNumber);
              return "redeemedAr";
            }

            else
            {
              //                            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/Redeemed.jsp");
               FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("vouNum", 
                                                                                          "" + 
                                                                                          vouNumber);
              return "redeemed";
            }


            

          }
          catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
        }

      }
      result.getStatement().close();
      result.close();
      DBConnection.closeConnections ( conn ) ;
    }
    catch (SQLException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      //            FileLogger.log(e);
       
    }
    if (path.compareTo("Ar") == 0)
    {
    	error = Login.getArabicErrorDTO().getInvalidvouchernumber();
      //System.out.println(error);
      return null;
    }
    else
    {
      errorEn.setValue("Invalid voucher number, please refer to Mobinil customer service.");
      return null;
    }
  }


  public void setVoucherNumber(String voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public String getVoucherNumber()
  {
    return voucherNumber;
  }


  public void setError(String error)
  {
    this.error = error;
  }

  public String getError()
  {
    return error;
  }


  public void setErrorEn(HtmlOutputText errorEn)
  {
    this.errorEn = errorEn;
  }

  public HtmlOutputText getErrorEn()
  {
    return errorEn;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }
}
