package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.application.GVMSAppContextListener;
import com.mobinil.gvms.system.application.UserDestroyDTO;
import com.mobinil.gvms.system.user.DTO.ArabicErrorDTO;
import com.mobinil.gvms.system.user.DTO.ArabicNamesDTO;

import com.mobinil.gvms.system.user.DTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class Login
{
  public String userName;
  public String password;
  public String error;
  public String arabicName;
  public ArrayList<ArabicNamesDTO> namesArray;
  public static ArabicErrorDTO arabicErrorDTO ;
  public static ArabicErrorDTO getArabicErrorDTO() {
	return arabicErrorDTO;
}

public static void setArabicErrorDTO(ArabicErrorDTO arabicErrorDTO) {
	Login.arabicErrorDTO = arabicErrorDTO;
}

String path;
  String systemUserId;
  int loginFailed = 0;
  
  public Login()
  {
    HttpServletRequest req = 
      (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      
      
      //System.out.println(req.getContextPath());
      //System.out.println(req.getPathInfo());
      //System.out.println(req.getRequestURI());
      //System.out.println(req.getRequestURL());
      
      path = (String)jsfUtils.getFromSession("userTypeInterface");
    //System.out.println("path in Login bean is " + path);
    if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    namesArray = new ArrayList<ArabicNamesDTO>();
    ArabicNamesDTO aNC = new ArabicNamesDTO();
    arabicErrorDTO = jsfUtils.getArabicErrorFromIni();
    
    aNC = jsfUtils.getArabicParamterFromIni();
    jsfUtils.putInSession ( aNC , "ArabicParamtersObj" );
        namesArray.add(aNC);


  }

  public String gotoArabicLogin()
  {

//    try
//      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Login_Bean");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");
      
//      String path = (String)jsfUtils.getFromSession("userTypeInterface");
      
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
//      }
//    catch (IOException e)
//      {
//
//      }

    return "arabicLoginPage";
  }

  public String gotoEnglishLogin()
  {
//
//    try
//      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Login_Bean");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
//      }
//    catch (IOException e)
//      {
//
//      }
    return "goToEnglish";
  }


  public String checkUserInfo()
  {
    // Add event code here...
    int availableLockTimes = 0;
    Integer expireDaysCount = 0;

    
    ArrayList<LoginDTO> userInfo = new ArrayList<LoginDTO>();
//      path = (String)jsfUtils.getFromSession("userTypeInterface");
    if (path.compareTo("Ar")==0)
      {
    
      }

    if (userName.compareTo("") == 0 || password.compareTo("") == 0)
      {
        if (path.compareTo("Ar")==0)
          {
            error = arabicErrorDTO.getPleasetypeusername();


          }
        else
          {
            error = "Please provide your user name and password.";


          }
        return null;
      }

    try
      {
       Connection conn = DBConnection.getConnection();
        ResultSet result = 
          ExecuteQueries.executeQuery(conn,"select Max(t4.PASSWORD_INDEX) SYSTEM_USER_PASS_INDEX,t1.SYSTEM_LOGIN_END_DATE,t1.SYSTEM_USER_ID,t1.SYSTEM_USER_PASSWORD," + 
                                      "t1.SYSTEM_LOGIN_COUNT,t1.SYSTEM_USER_TYPE,t1.SYSTEM_USER_NAME,t1.IS_LOCKED,t2.user_type_id,t2.USER_ID,t2.USER_FULL_NAME from GVMS_SYSTEM_USER t1 left join GVMS_USER t2 on t1.SYSTEM_USER_ID=t2.SYSTEM_USER_ID left join GVMS_PASS_TRACE t4 on t1.SYSTEM_USER_ID=t4.SYSTEM_USER_ID  where t1.USER_STATUS_ID=1 and t1.SYSTEM_USER_TYPE = 'user' and SYSTEM_USER_NAME='" + 
                                      userName.trim().toLowerCase() + 
                                      "' group by t1.SYSTEM_LOGIN_END_DATE,t1.SYSTEM_USER_ID,t1.SYSTEM_USER_PASSWORD,t1.SYSTEM_LOGIN_COUNT,t1.SYSTEM_USER_TYPE,t1.SYSTEM_USER_NAME,t1.IS_LOCKED,t2.user_type_id,t2.USER_ID,t2.USER_FULL_NAME");
        String encripteCurrPass = MD5Class.MD5(password);
        if (result.next())
          {
            LoginDTO lc = new LoginDTO();
            lc.setEndDate(result.getDate("SYSTEM_LOGIN_END_DATE"));
            lc.setFullName(result.getString("USER_FULL_NAME"));
            lc.setLoginCount(result.getInt("SYSTEM_LOGIN_COUNT"));
            lc.setPassIndex(result.getInt("SYSTEM_USER_PASS_INDEX"));
            lc.setPassword(result.getString("SYSTEM_USER_PASSWORD"));
            lc.setSysUserId(result.getInt("SYSTEM_USER_ID"));
            lc.setSysUserType(result.getString("SYSTEM_USER_TYPE"));
            lc.setUserId(result.getInt("USER_ID"));
            lc.setUserLocked(result.getString("IS_LOCKED"));
            lc.setUserName(result.getString("SYSTEM_USER_NAME"));
            lc.setUserTypeId(result.getInt("user_type_id"));

            userInfo.add(lc);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userInfo",
                                                                                                   userInfo);
          }
        else
          {

            if (path.compareTo("Ar")==0)
              {
                error = arabicErrorDTO.getInvalidusernameorpass();
                return null;
              }
            else
              {
                error = "Invalid user name or password.";

                return null;
              }


          }
        result.getStatement().close();
        result.close();


        for (int userCounter = 0; userCounter < userInfo.size(); userCounter++)
          {
            String existPass = userInfo.get(userCounter).getPassword();

            if (existPass.compareTo(encripteCurrPass) != 0)
              {

                loginFailed++;
                //System.out.println("loginFailed is " + loginFailed);
                ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set IS_LOCKED=" + 
                                                loginFailed + " where SYSTEM_USER_ID=" + 
                                                userInfo.get(userCounter).getSysUserId());
                if (path.compareTo("Ar")==0)
                  {
                    error = arabicErrorDTO.getInvalidpassword();


                  }
                else
                  {
                    error = "Invalid password.";


                  }

                return null;
              }
            else
              { // security constrains

                ResultSet lockTimesRS = 
                  ExecuteQueries.executeQuery(conn,"SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES'");
                if (lockTimesRS.next())
                  {
                    availableLockTimes = lockTimesRS.getInt("PROPERTIES");

                  }
                lockTimesRS.getStatement().close();
                lockTimesRS.close();

                Integer userTried = Integer.parseInt(userInfo.get(userCounter).getUserLocked());

                if (userTried >= availableLockTimes)
                  {
                    if (path.compareTo("Ar")==0)
                      {
                    	error = arabicErrorDTO.getThisuserlocked();
                      }
                    else
                      {
                        error = "This account is locked."; // go to any page to unlock


                      }

                    return null;

                  }

                Integer numberOfLogin = userInfo.get(userCounter).getLoginCount();
                if (numberOfLogin == 0)
                  {
                    ResultSet ExpireDayRS = 
                      ExecuteQueries.executeQuery(conn,"SELECT PROPERTIES FROM GVMS_PROPERTIES where REASONE='DAYS_FOR_EXPIRED_LOGIN'");

                    while (ExpireDayRS.next())
                      {
                        expireDaysCount = ExpireDayRS.getInt("PROPERTIES");
                      }
                    ExpireDayRS.getStatement().close();
                    ExpireDayRS.close();
                    Date expireDate = new Date();
                    expireDate.setDate(expireDate.getDate() + expireDaysCount);

                    int test = 
                      ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set SYSTEM_LOGIN_END_DATE=" + 
                                                      new DateClass().convertDate(expireDate) + 
                                                      " where SYSTEM_USER_ID=" + 
                                                      userInfo.get(userCounter).getSysUserId());
                    //System.out.println(test);


                  }
                else
                  {
                    Date userExpireDate = userInfo.get(userCounter).getEndDate();
                    if (userExpireDate.before(new Date()))
                      {
                      
                        Boolean userTypeGenerate=true;
                        Boolean adminTypeGenerate=false;
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("adminTypeGenerate", 
                                                                                      adminTypeGenerate); // go to any page to unlock
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeGenerate", 
                                                                                      userTypeGenerate); // go to any page to unlock 
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userInfo", 
                                                                                                   userInfo);
                        if (path.compareTo("Ar")==0)
                          {
                        	error = arabicErrorDTO.getUserexpired();                            
                            return "toNewPassAr";
                          }
                        else
                          {
                            error = ("This account is expired."); // go to any page to unlock
                             return "toNewPass";
                          }


                        
                      }

                  }


                //            if(result.getString("IS_LOCKED").compareTo("0")==0){
                //              error="";
                //              error="This account is locked.";
                //              return null;
                //            
                //            }


                systemUserId = userInfo.get(userCounter).getSysUserId()+"";
                String userType = userInfo.get(userCounter).getSysUserType();


                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username", 
                                                                                           userInfo.get(userCounter).getFullName()); // this for editing user information secuity 
                
                  FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("systemUserId", 
                                                                                             "" + 
                                                                                             systemUserId);
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("systemUserType", 
                                                                                           "" + 
                                                                                           userType);

                if (userType.compareTo("user") == 0)
                  {
                    //                ResultSet resultset = 
                    //                  ExecuteQueries.executeQuery("select user_type_id,USER_ID,USER_FULL_NAME  from GVMS_USER where SYSTEM_USER_ID=" + 
                    //                                              systemUserId);

                    int userTypeId = userInfo.get(userCounter).getUserTypeId();
                    int userId = userInfo.get(userCounter).getUserId();
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeId", 
                                                                                               "" + 
                                                                                               userTypeId);

                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userId", 
                                                                                               "" + 
                                                                                               userId);
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userName", 
                                                                                               userInfo.get(userCounter).getFullName());

                    //                    take care of this next lines
                    //                else
                    //                  {
                    //                    if (path.compareTo("Ar")==0)
                    //                      {
                    //                        error=errorArray.get(0);
                    //                        return null;
                    //                      }else{
                    //                    error="Invalid user name or password.";
                    //                    return null;}
                    //                  }
                    //                resultset.close();


                    try
                      {
                        ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set SYSTEM_LOGIN_COUNT=" + 
                                                        (userInfo.get(userCounter).getLoginCount() + 
                                                         1) + " where SYSTEM_USER_ID=" + 
                                                        systemUserId);
                        ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set IS_LOCKED='0' where SYSTEM_USER_ID=" + 
                                                        systemUserId);
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(systemUserId,"systemUserId");
                        String sessionId = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession(false).getId();
                        ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();
                        for (int i = 0; i < users.size(); i++) {
                        	UserDestroyDTO udd = users.get(i);
                        	if (udd.getSessionId().compareTo(sessionId) == 0 && udd.getDestroy()) {
                        		udd.setDestroy(false);
                        	}            	
                        	users.remove(i);
                        	users.add(udd);
                        }
                        GVMSAppContextListener.setUserNavigation(users);
                        if (path.compareTo("Ar")==0)
                          {
//                            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/ArabicWelcome.jsp");
                              return "welcomeUserAr";
                          }
                        else
                          {
//                            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Welcome.jsp");
                            return "welcomeUser";
                          }
                      }
                    catch (Exception e)
                      {
                         new com.mobinil.gvms.system.utility.PrintException().printException(e);
                      }
                    
                  }


              }
          }

DBConnection.closeConnections ( conn );
      }
    catch (Exception e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);      }


    return null;
  }


  public void cleanBean(String beanName, String url)
  {
    Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    if (sessionMap.containsKey(beanName))
      {
        sessionMap.remove(beanName);
      }
//    try
//      {
//        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
//      }
//    catch (IOException e)
//      {
//         
//      }

  }

  public void logoutAr()
  {

    Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    HttpSession session = 
      (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

    String[] beanNames = session.getValueNames();
    for (int x = 0; x < beanNames.length; x++)
      {
//        if (beanNames[x].compareTo("Login_Bean") != 0)
//          {

            if (sessionMap.containsKey(beanNames[x]))
              {
                sessionMap.remove(beanNames[x]);
              }

//          }


      }
    
    
    
  }
 public String Search()
 {
       FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean"); 
       //System.out.println("usrIdHiden in search is "+systemUserId);
      
   return "userSearch";
 }
   public String history()
   {
         FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("History_Bean");    
       //System.out.println("usrIdHiden in history is "+systemUserId);
        
     return "userHistory";
   }
   public String edit()
   {
         FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Edit_Bean");    
       //System.out.println("usrIdHiden in edit is "+systemUserId);        
     return "userEdit";
   }

  public String forgetPassAction()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("ForgetPass_Bean");


    return "forget";
  }

  public String searchAr()
  {
    cleanBean("Search_Bean", "/GVMS/faces/ArabicInterface/SearchArabic.jsp");
//      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(systemUserId,"systemUserId"); 
      //System.out.println("userIdHiden is "+systemUserId);
return "userSearchAr";

  }

  public String historyAr()
  {
    cleanBean("History_Bean", "/GVMS/faces/ArabicInterface/HistoryArabic.jsp");
//      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(systemUserId,"systemUserId"); 
      return "userHistoryAr";
  }

  public String accountAr()
  {
    cleanBean("Edit_Bean", "/GVMS/faces/ArabicInterface/EditArabic.jsp");
//      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(systemUserId,"systemUserId"); 
      return "userEditAr";
  }

  public String exitAr()
  {
    logoutAr();
//    return "/GVMS/faces/ArabicInterface/LoginArabic.jsp";
return "logout_ar";
  }
  
  public String exitEn()
  {
    logoutAr();
  //    return "/GVMS/faces/ArabicInterface/LoginArabic.jsp";
  return "logout_en";
  }


  public String forget_Action()
  {


    return "forget_ar";
  }


  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setNamesArray(ArrayList<ArabicNamesDTO> namesArray)
  {
    this.namesArray = namesArray;
  }

  public ArrayList<ArabicNamesDTO> getNamesArray()
  {
    return namesArray;
  }

  public void setError(String error)
  {
    this.error = error;
  }

  public String getError()
  {
    return error;
  }

public void setArabicName(String arabicName) {
	this.arabicName = arabicName;
}

public String getArabicName() {
	return arabicName;
}


}
