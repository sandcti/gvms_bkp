package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.user.DAO.*;
import com.mobinil.gvms.system.user.DTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class Edit
{
  
  public String oldPassword;
  public String newPassword;
  public String reNewPassword;
  //  Integer userId;
  public String msgWarn;
  public String msgWarn1;
  public String colorMsgWarn;
  public Boolean displayWarnigs = false;
  public Boolean displayWarnigsAr = false;
  public String header;
  public String line1;
  public String line2;
  public String line3;
  public String line4;
  public String line5;
  public String line6;
  public String line7;
  String path;
    public String usrIdHiden;

  public Edit()
  {
    path = FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath();
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
    //System.out.println("path in edit cons is " + path);
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
                        {
                          FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
                        }
                      catch (Exception e)
                        {
                           new com.mobinil.gvms.system.utility.PrintException().printException(e);
                        }
         }

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         { try
            {
              FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
            }
          catch (Exception e)
            {
               new com.mobinil.gvms.system.utility.PrintException().printException(e);
            }
            }
      }

  }


  public String editPassword() throws SQLException
  {
    
    HttpServletRequest req = 
      (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      path = (String)jsfUtils.getFromSession("userTypeInterface");

    //System.out.println("path in edit  is " + path);
    if (path.compareTo("Ar")==0)
      {
        

        //      for (int i=0;i<errorArray.size();i++){
        //      //System.out.println("errorArray in row "+i+" is "+errorArray.get(i));
        //      }


        header = Login.getArabicErrorDTO().getFollowsteps();
        line1 = Login.getArabicErrorDTO().getStep1();
        line2 = Login.getArabicErrorDTO().getStep2();
        line3 = Login.getArabicErrorDTO().getStep3();
        line4 = Login.getArabicErrorDTO().getStep4();
        line5 = Login.getArabicErrorDTO().getStep5();
        line6 = Login.getArabicErrorDTO().getStep6();
        line7 = Login.getArabicErrorDTO().getStep7();


      }

    String sysUserId = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId");
    int lastPass = 0;
    String userName = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("username");
    if (oldPassword.compareTo("") == 0 || newPassword.compareTo("") == 0 || 
        reNewPassword.compareTo("") == 0)
      {
        if (path.compareTo("Ar")==0)
          {

            msgWarn1 = Login.getArabicErrorDTO().getPleaseinsetvalue();
            displayWarnigsAr = true;
          }
        else
          {
            msgWarn1 = "Value is required.";
            displayWarnigs = true;

          }

        return null;
      }

   


    //    if (newPassword.compareTo("") == 0 || reNewPassword.compareTo("") == 0)
    //      {
    //
    //        msgWarn = "Please insert values.";
    //        return null;
    //
    //
    //      }


    path = FacesContext.getCurrentInstance().getExternalContext().getRequestPathInfo();

    String userPass = new String();
    int updateUser = 0;
ArrayList<LoginDTO> userInfo =(ArrayList<LoginDTO>) jsfUtils.getFromSession("userInfo");


    if (newPassword.compareTo(reNewPassword) != 0)
      {

        colorMsgWarn = "color:red;";
        if (path.compareTo("Ar")==0)
          {
            //System.out.println("the path1 is : " + path);
            msgWarn = Login.getArabicErrorDTO().getPassandconfnotequal();
            displayWarnigsAr = true;
          }
        else
          {
            msgWarn = 
                "The new password and the confirmation password don't match. Please type the same password in both boxes.";
            displayWarnigs = true;
          }
        return null;
      }
    
//    ResultSet rs =
//      ExecuteQueries.executeQuery(conn,"Select SYSTEM_USER_PASSWORD FROM GVMS_SYSTEM_USER WHERE SYSTEM_USER_ID =" +
//                                  sysUserId);
                                  
      String x = securityImplement.checkPass(path, newPassword, userName);
      if (x.compareTo("") != 0)
        {

          msgWarn = x;
          displayWarnigs = true;
          return null;

        }

      Connection conn = DBConnection.getConnection();
      userPass = userInfo.get(0).getPassword();
//    try
//      {
//        if (rs.next())
//          {
//            userPass = rs.getString("SYSTEM_USER_PASSWORD");
//          }
//        rs.getStatement().close();
//        rs.close();
//      }
//    catch (SQLException e)
//      {
//         new com.mobinil.gvms.system.utility.PrintException().printException(e);
//      }

    try
      {
        if (userPass.compareTo(MD5Class.MD5(oldPassword)) == 0)
          {

            String passEncripted = MD5Class.MD5(newPassword);

            ResultSet lastPassRS = 
              ExecuteQueries.executeQuery(conn,"select PROPERTIES from GVMS_PROPERTIES where reasone='LAST_PASSWORD_COUNT'");
            if (lastPassRS.next())
              {
                lastPass = lastPassRS.getInt("PROPERTIES");

              }
            lastPassRS.getStatement().close();
            lastPassRS.close();

            ResultSet checkPassExists = 
              ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_ID from gvms_PASS_TRACE  where PASSWORD='" + 
                                          passEncripted + "' and SYSTEM_USER_ID = " + sysUserId);


            if (checkPassExists.next())
              {
                if (path.compareTo("Ar")==0)
                  {

                    msgWarn = Login.getArabicErrorDTO().getPassinlasthistory();
                    displayWarnigsAr = true;
                  }
                else
                  {
                    msgWarn = ("this password included in your last 6 passwords.");
                    displayWarnigs = true;

                  }

                return null;

              }
            checkPassExists.getStatement().close();
            checkPassExists.close();

            ResultSet chckLst6Pswrd = 
              ExecuteQueries.executeQuery(conn,"select Max(PASSWORD_INDEX) passCount from gvms_PASS_TRACE where SYSTEM_USER_ID = " + 
                                          sysUserId + "");


            if (chckLst6Pswrd.next())
              {
                if (chckLst6Pswrd.getInt("passCount") > 0 && 
                    chckLst6Pswrd.getInt("passCount") < (lastPass + 1))
                  {
                    if (chckLst6Pswrd.getInt("passCount") == lastPass)
                      {
                        updateUser = 
                            ExecuteQueries.executeNoneQuery("Update GVMS_SYSTEM_USER set SYSTEM_USER_PASSWORD = '" + 
                                                            MD5Class.MD5(newPassword) + 
                                                            "' where SYSTEM_USER_ID=" + sysUserId);
//                        msgWarn = ("New password updated to "+userInfo.get(0).getUserName()+" account.");

                        if (updateUser == 0)
                          {
                            msgWarn = ("== 6 Invalid update system user table");
                            DBConnection.closeConnections ( conn );
                            return null;
                          }
                        else
                          {

                            int updatePassTrace = 
                              ExecuteQueries.executeNoneQuery("insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
                                                              sysUserId + ",'" + passEncripted + 
                                                              "'," + 
                                                              (chckLst6Pswrd.getInt("passCount") + 
                                                               1) + ")");
                            if (updatePassTrace == 0)
                              {
                                msgWarn = ("== 6 Invalid update Pass trace table");
                                DBConnection.closeConnections ( conn );
                                return null;
                              }
                            else
                              {

                                //System.out.println("result of upadate system  user is " + 
//                                                   updateUser);

                                updatePassTrace = 
                                    ExecuteQueries.executeNoneQuery("update gvms_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-1 where SYSTEM_USER_ID=" + 
                                                                    sysUserId);
                                if (updatePassTrace == 0)
                                  {
                                    msgWarn = 
                                        ("== 6 Invalid update Pass trace table to set 0 with 1");
                                    DBConnection.closeConnections ( conn );
                                    return null;
                                  }
                                else
                                  {
                                    int deletePasstrace = 
                                      ExecuteQueries.executeNoneQuery("delete from gvms_PASS_TRACE where SYSTEM_USER_ID =" + 
                                                                      sysUserId + 
                                                                      " and PASSWORD_INDEX=0");
                                    if (deletePasstrace == 0)
                                      {
                                        msgWarn = ("== 6 Invalid delete Pass trace table row 0");
                                        DBConnection.closeConnections ( conn );
                                        return null;
                                      }
                                    else
                                      {
                                        colorMsgWarn = "color:green;";
                                        if (path.compareTo("Ar")==0)
                                          {
                                            //System.out.println("the path2 is : " + path);
                                            msgWarn = Login.getArabicErrorDTO().getUpdateinfocomplete();
                                          }
                                        else
                                          {
                                            msgWarn = "Changing information complete";
                                          }
                                        DBConnection.closeConnections ( conn );
                                        return null;


                                      }
                                  }
                              }
                          }


                      }
                    else
                      {
                        updateUser = 
                            ExecuteQueries.executeNoneQuery("Update GVMS_SYSTEM_USER set SYSTEM_USER_PASSWORD = '" + 
                                                            MD5Class.MD5(newPassword) + 
                                                            "' where SYSTEM_USER_ID=" + sysUserId);


                        if (updateUser == 0)
                          {

                            msgWarn = ("> 6 Invalid update system user table");
                            DBConnection.closeConnections ( conn );
                            return null;
                          }
                        else
                          {
//String sql ="insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" +
//                                                              sysUserId + ",'" + passEncripted +
//                                                              "'," +
//                                                              (chckLst6Pswrd.getInt("passCount") +
//                                                               1) + ")";
//                              System.out.println("sql iss "+sql);
//                            int updatePassTrace =
//                              ExecuteQueries.executeNoneQuery(sql);
//                            if (updatePassTrace == 0)
//                              {
//                                msgWarn = ("> 6 Invalid update Pass trace table");
//                                DBConnection.closeConnections ( conn );
//                                return null;
//                              }
//                            else
                              {
                                colorMsgWarn = "color:green;";
                                if (path.compareTo("Ar")==0)
                                  {
                                    //System.out.println("the path2 is : " + path);
                                    msgWarn = Login.getArabicErrorDTO().getUpdateinfocomplete();
                                  }
                                else
                                  {
                                    msgWarn = "Changing information complete";
                                  }
                                DBConnection.closeConnections ( conn );
                                return null;
                              }
                          }


                      }
                  }
              }


          }
        else
          {
            if (path.compareTo("Ar")==0)
              {
                msgWarn1 = Login.getArabicErrorDTO().getInvalidoldpass();
              }
            else
              {
                msgWarn1 = "Invalid old password";
              }
          }

        DBConnection.closeConnections ( conn );
      }
    catch (Exception e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    //    if (rows == 0)
    //      {
    //        //System.out.println("2");
    //        return "failed3";
    //      }
    //System.out.println("the path3 is : " + path);
    return "edit";
  }


  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setMsgWarn1(String msgWarn1)
  {
    this.msgWarn1 = msgWarn1;
  }

  public String getMsgWarn1()
  {
    return msgWarn1;
  }


  public void setColorMsgWarn(String colorMsgWarn)
  {
    this.colorMsgWarn = colorMsgWarn;
  }

  public String getColorMsgWarn()
  {
    return colorMsgWarn;
  }

  public void setOldPassword(String oldPassword)
  {
    this.oldPassword = oldPassword;
  }

  public String getOldPassword()
  {
    return oldPassword;
  }

  public void setNewPassword(String newPassword)
  {
    this.newPassword = newPassword;
  }

  public String getNewPassword()
  {
    return newPassword;
  }

  public void setReNewPassword(String reNewPassword)
  {
    this.reNewPassword = reNewPassword;
  }

  public String getReNewPassword()
  {
    return reNewPassword;
  }

  public void setDisplayWarnigs(Boolean displayWarnigs)
  {
    this.displayWarnigs = displayWarnigs;
  }

  public Boolean getDisplayWarnigs()
  {
    return displayWarnigs;
  }

  public void setDisplayWarnigsAr(Boolean displayWarnigsAr)
  {
    this.displayWarnigsAr = displayWarnigsAr;
  }

  public Boolean getDisplayWarnigsAr()
  {
    return displayWarnigsAr;
  }

  public void setHeader(String header)
  {
    this.header = header;
  }

  public String getHeader()
  {
    return header;
  }

  public void setLine1(String line1)
  {
    this.line1 = line1;
  }

  public String getLine1()
  {
    return line1;
  }

  public void setLine2(String line2)
  {
    this.line2 = line2;
  }

  public String getLine2()
  {
    return line2;
  }

  public void setLine3(String line3)
  {
    this.line3 = line3;
  }

  public String getLine3()
  {
    return line3;
  }

  public void setLine4(String line4)
  {
    this.line4 = line4;
  }

  public String getLine4()
  {
    return line4;
  }

  public void setLine5(String line5)
  {
    this.line5 = line5;
  }

  public String getLine5()
  {
    return line5;
  }

  public void setLine6(String line6)
  {
    this.line6 = line6;
  }

  public String getLine6()
  {
    return line6;
  }

  public void setLine7(String line7)
  {
    this.line7 = line7;
  }

  public String getLine7()
  {
    return line7;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }
}
