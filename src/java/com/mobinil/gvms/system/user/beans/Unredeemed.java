package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.user.DAO.UserDAO;
import com.mobinil.gvms.system.user.DTO.AdditionalFieldDTO;
import com.mobinil.gvms.system.user.DTO.DatatableDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletRequest;


public class Unredeemed
{
  
  public Date date;
  public Integer carNumber;
  public String voucher_Number;
  public String campaignId;
  public String custCampaignId;
  public String userId;
  public String dialNumber;
  public String giftId;
  public String giftName;
  public Integer voucherStatus;
  String userTypeId;
  String totalFields = new String();
  String totalValues = new String();
  ArrayList<Integer> campaignsIds;
  public Boolean renderDialNumber = false;
  public Boolean renderGift = true;

  public ArrayList<SelectItem> gifts  ;  
  public ArrayList < SelectItem > getGifts()
{
   return gifts;
}




public void setGifts(ArrayList < SelectItem > gifts)
{
   
   this.gifts = gifts;
}



ResultSet rs;


  public ArrayList<DatatableDTO> dt;


  public ArrayList<SelectItem> items = new ArrayList<SelectItem>();
  ArrayList<AdditionalFieldDTO> row = new ArrayList<AdditionalFieldDTO>();
  public int increVar;
  public String redeemFailAr;


  public String field0Value;
  int Indx = 0;


  public HtmlOutputLabel voucherOutputLabel1;
  public HtmlOutputLabel outputLabel2;
  public HtmlOutputLabel dialOutputLabel;
  public HtmlOutputLabel field1OutputLabel;
  public HtmlOutputLabel dialNumberError;


  public String dialInputText;

String giftTypeId;
  public HtmlOutputLabel field2Validator;
  public HtmlCommandButton commandButton1;
  public HtmlDataTable dataTable1;
  public String redeemFail;
  public HtmlOutputText checkLbl;
  String path;
    public String usrIdHiden;

  public Unredeemed() throws SQLException
  {
      path = (String)jsfUtils.getFromSession("userTypeInterface");
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {          try
            {
              FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
            }
          catch (Exception e)
            {
               
            }}

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
          {
              try
                {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
                }
              catch (Exception e)
                {
                   
                }
          }
      }

    increVar = 0;


    Integer rowCount = 0;
    int listId = 0;
    int count = 0;

    setDt(new ArrayList<DatatableDTO>());

    voucherStatus = 
        (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("VoucherStatus");

    voucher_Number = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("vouNum");
    //System.out.println(voucher_Number);
    campaignId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cid");
    custCampaignId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("ccid");
    userTypeId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeId");
    userId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId");
    giftId = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("gid");

    dialNumber = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("dialNum");
    if (dialNumber.compareTo("3") == 0)
      {
        dialNumber = "2";
      }
    //System.out.println("Dn is" + dialNumber);

    if (dialNumber.compareTo("1") != 0)
      {
        renderDialNumber = true;
      }
    String giftNameField = "";
    if (path.compareTo("Ar")==0)
      {
        giftNameField = "GIFT_NAME_ARABIC";
      }
    else
      {
        giftNameField = "GIFT_NAME";
      }
    if (giftId.compareTo ( "0" )==0)
    {
       renderGift = false;
       gifts = getCampaigGift ( campaignId );
    }
    Connection conn = DBConnection.getConnection();
    ResultSet GiftNameRs = 
      ExecuteQueries.executeQuery(conn,"Select t2." + giftNameField + " GIFT_NAME , t2.GIFT_TYPE_ID from GVMS_CURRENT_VOUCHER t1 left join GVMS_GIFT t2 on t1.GIFT_ID=t2.GIFT_ID where t1.CURRENT_VOUCHER_NUMBER=" + 
                                  voucher_Number);
    try
      {
        if (GiftNameRs.next())
          {
            giftName = GiftNameRs.getString("GIFT_NAME");
            
               
            giftTypeId = GiftNameRs.getInt("GIFT_TYPE_ID")+"";
          }
        GiftNameRs.getStatement().close();
        GiftNameRs.close();
      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    rs = 
        ExecuteQueries.executeQuery(conn,"select t1.*,t2.CAMPAIGN_NAME from GVMS_ADDITIONAL_FIELD t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID =  t2.CAMPAIGN_ID where  t1.CAMPAIGN_ID=" + 
                                    campaignId + 
                                    " and FIELD_STATUS_ID != 2 and FIELD_STATUS_ID =1 ORDER BY FIELD_ORDER");

    try
      {
        while (rs.next())
          {
           AdditionalFieldDTO rowValues = new AdditionalFieldDTO();
            rowValues.setCampaignId(rs.getInt("CAMPAIGN_ID"));
            rowValues.setFieldArabicName(rs.getString("FIELD_ARABIC_NAME"));
            rowValues.setFieldDbName(rs.getString("FIELD_DB_NAME"));
            rowValues.setFieldEnglishName(rs.getString("FIELD_ENGLISH_NAME"));
            rowValues.setFieldId(rs.getInt("FIELD_ID"));
            rowValues.setFieldType(rs.getString("FIELD_TYPE"));
            rowValues.setFieldIsMandatory(rs.getString("FIELD_IS_MANDATORY"));
            rowValues.setFieldIsUnique(rs.getString("FIELD_IS_UNIQUE"));
            rowValues.setFieldIsCombo(rs.getString("FIELD_IS_COMBO"));
            rowValues.setFieldIsInputText(rs.getString("FIELD_IS_INPUT_TEXT"));
            rowValues.setFieldIsText(rs.getString("FIELD_IS_TEXT_AREA"));
            rowValues.setCampaignName(rs.getString("CAMPAIGN_NAME"));
            row.add(rowValues);


            String fieldType = rs.getString("FIELD_TYPE");
            String fieldId = rs.getString("FIELD_ID");

            if (rowValues.getFieldIsMandatory().compareTo("0") == 0)
              {

                String enName = rowValues.getFieldEnglishName();
                String arName = rowValues.getFieldArabicName();
                DatatableDTO fDTO = new DatatableDTO();

                fDTO.setValue(enName);
                fDTO.setArabicValue(arName);
                fDTO.setRequire(false);
                fDTO.setId(fieldId);
                fDTO.setFieldType(fieldType);
                if (rowValues.getFieldIsInputText().compareTo("1") == 0)
                  {


                    fDTO.setRenderInputText(true);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderInputDate(false);
                    fDTO.setRenderCombo(false);

                  }
                if (rowValues.getFieldIsCombo().compareTo("1") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderCombo(true);
                    fDTO.setRenderInputDate(false);
                    ResultSet rsltSet = 
                      ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ID,VALIDATION_LIST_ROW_COUNT from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                                  fieldId);

                    try
                      {
                        if (rsltSet.next())
                          {
                            rowCount = rsltSet.getInt("VALIDATION_LIST_ROW_COUNT");
                            listId = rsltSet.getInt("VALIDATION_LIST_ID");
                            
                            if (rowCount==null)rowCount=0;
                            
                            items = new ArrayList<SelectItem>();
                            items.add(new SelectItem("--please Select--"));
                            fDTO.setCompoList(items);
                            
                            
                            if (rowCount >= 30)
                              {
                                for (int counter = 0; counter < dt.size(); counter++)
                                  {
                                    if (dt.get(counter).getRenderCombo() == true)
                                      {
                                        dt.get(counter).setRenderCombo(false);
                                        dt.get(counter).setRenderInputText(true);
                                      }

                                  }

                              }
                            else
                              {
                                ResultSet rset = 
                                  ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ITEM from GVMS_VALIDATION_LIST_ITEM where VALIDATION_LIST_ID=" + 
                                                              listId);

                                items = new ArrayList<SelectItem>();
                                int x = 0;
                                while (rset.next())
                                  {
                                    if (x == 0)
                                      {
                                        items.add(new SelectItem("--please Select--"));
                                      }
                                    items.add(new SelectItem(rset.getString("VALIDATION_LIST_ITEM")));

                                    x++;
                                  }
                                  fDTO.setCompoList(items);
                                rset.getStatement().close();
                                rset.close();

                              }
                          }
                        rsltSet.getStatement().close();
                        rsltSet.close();
                      }
                    catch (SQLException e)
                      {
                         new com.mobinil.gvms.system.utility.PrintException().printException(e);
                      }

                  }
                if (rowValues.getFieldIsText().compareTo("1") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(true);
                    fDTO.setRenderCombo(false);
                    fDTO.setRenderInputDate(false);
                  }


                if (fieldType.compareTo("Date") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderInputDate(true);
                    fDTO.setRenderCombo(false);
                  }

                getDt().add(fDTO);


              }
            else
              {

                String enName = rowValues.getFieldEnglishName() + "*";
                String arName = rowValues.getFieldArabicName() + "*";
                DatatableDTO fDTO = new DatatableDTO();

                fDTO.setValue(enName);
                fDTO.setArabicValue(arName);
                fDTO.setRequire(true);
                fDTO.setId(fieldId);
                fDTO.setFieldType(fieldType);
                if (rowValues.getFieldIsInputText().compareTo("1") == 0)
                  {


                    fDTO.setRenderInputText(true);
                    fDTO.setRenderInputDate(false);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderCombo(false);

                  }
                if (rowValues.getFieldIsCombo().compareTo("1") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderCombo(true);
                    fDTO.setRenderInputDate(false);
                    
                    ResultSet rsltSet = 
                      ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ID,VALIDATION_LIST_ROW_COUNT from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                                  fieldId);

                    try
                      {
                        if (rsltSet.next())
                          {
                            rowCount = rsltSet.getInt("VALIDATION_LIST_ROW_COUNT");
                            listId = rsltSet.getInt("VALIDATION_LIST_ID");

                            if (rowCount >= 30)
                              {
                                for (int counter = 0; counter < dt.size(); counter++)
                                  {
                                    if (dt.get(counter).getRenderCombo() == true)
                                      {
                                        dt.get(counter).setRenderCombo(false);
                                        dt.get(counter).setRenderInputText(true);
                                      }

                                  }

                              }
                            else
                              {
                                ResultSet rset = 
                                  ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ITEM from GVMS_VALIDATION_LIST_ITEM where VALIDATION_LIST_ID=" + 
                                                              listId);


                                int x = 0;
                                items = new ArrayList<SelectItem>();
                                while (rset.next())
                                  {
                                    if (x == 0)
                                      {
                                        items.add(new SelectItem("--please Select--"));
                                      }
                                    items.add(new SelectItem(rset.getString("VALIDATION_LIST_ITEM")));

                                    x++;
                                  }
                                fDTO.setCompoList(items);
                                rset.getStatement().close();
                                rset.close();

                              }
                          }
                        rsltSet.getStatement().close();
                        rsltSet.close();
                      }
                    catch (SQLException e)
                      {
                         new com.mobinil.gvms.system.utility.PrintException().printException(e);
                      }


                  }
                if (rowValues.getFieldIsText().compareTo("1") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(true);
                    fDTO.setRenderCombo(false);
                    fDTO.setRenderInputDate(false);
                  }
                if (fieldType.compareTo("Date") == 0)
                  {
                    fDTO.setRenderInputText(false);
                    fDTO.setRenderTextarea(false);
                    fDTO.setRenderInputDate(true);
                    fDTO.setRenderCombo(false);
                  }

                getDt().add(fDTO);


              }

            //System.out.println("Array of row have data:" + row.size());
            count++;
          }
        rs.getStatement().close();
        rs.close();


      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);

      }
//    ResultSet comboFieldId = 
//      ExecuteQueries.executeQuery("Select FIELD_ID from GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID=" + 
//                                  campaignId + " and FIELD_IS_COMBO=1");
//    try
//      {
//        while (comboFieldId.next())
//          {
//            int s = comboFieldId.getInt("FIELD_ID");
//
//            
//          }
//        comboFieldId.getStatement().close();
//        comboFieldId.close();
//
//      }
//    catch (SQLException e)
//      {
//         
//      }
    DBConnection.closeConnections ( conn );
    //System.out.println("dt has rows:" + dt.size());
  }
  
  
  

  public String backAction()
  {
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");
       

    try
      {
       
        if (path.compareTo("Ar")==0)
          {
//            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/SearchArabic.jsp");
return "userSearch";
          }
        else
          {
//            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Search.jsp");
return "userSearchAr";
          }
          
      }
    catch (Exception e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
return "";
      }

  }

  public String redeemAction() throws SQLException
  {
if (giftName==null ||giftName.compareTo ( "" )==0||giftName.compareTo ( "-1" )==0)
{
   redeemFail = "Please Select gift.";
   return null;
   }
    campaignsIds = new ArrayList<Integer>();
    String values = new String();
    String gValue = new String();
    String userTypeNames = new String();
    
    
    if ((jsfUtils.getFromSession("isRedeemed"))!=null)
    {
    	if(((String)jsfUtils.getFromSession("isRedeemed")).compareTo(voucher_Number)==0)
    	{
    	if (path.compareTo("Ar")==0)
        {
          redeemFail = Login.getArabicErrorDTO().getAlreadyRedeemed();
         
          return null;
        }
      else
        {
    	  redeemFail = "Voucher already redeemed.";
         
          return null;
        }
    	}
    }
    
    if (path.compareTo("Ar")==0)
      {
    
      }
    //    String requiredMsg = "Value is required.";
    //    for (int counter = 0; counter < dt.size(); counter++)
    //      {
    //        if (dt.get(counter).getRequire())
    //          {
    //            if (dt.get(counter).getRenderInputDate())
    //              {
    //                FacesContext.getCurrentInstance().addMessage("unredeemForm", 
    //                                                             new FacesMessage(FacesMessage.SEVERITY_INFO, 
    //                                                                              requiredMsg, null));
    //              }
    //            if (dt.get(counter).getRenderInputText())
    //              {
    //                FacesContext.getCurrentInstance().addMessage("unredeemForm", 
    //                                                             new FacesMessage(FacesMessage.SEVERITY_INFO, 
    //                                                                              requiredMsg, null));
    //              }
    //            if (dt.get(counter).getRenderTextarea())
    //              {
    //                FacesContext.getCurrentInstance().addMessage("unredeemForm", 
    //                                                             new FacesMessage(FacesMessage.SEVERITY_INFO, 
    //                                                                              requiredMsg, null));
    //              }
    //          }
    //
    //
    //      }
    Connection conn = DBConnection.getConnection();
    ArrayList<String> userTypeNameArray = new ArrayList<String>();
    String userTypeId = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeId");

    ResultSet campaigns4UsersTypes = 
      ExecuteQueries.executeQuery(conn,"Select t2.USER_TYPE_ID,t2.USER_TYPE_NAME,t1.campaign_id from GVMS_campaign_User t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID=t2.USER_TYPE_ID where t2.USER_TYPE_STATUS_ID!=2 and t1.campaign_id=" + 
                                  campaignId);
    try
      {
        while (campaigns4UsersTypes.next())
          {
            userTypeNameArray.add(campaigns4UsersTypes.getString("USER_TYPE_ID"));
            //            userTypeNameArray.add(userTypeName);
            campaignsIds.add(campaigns4UsersTypes.getInt("campaign_id"));
            if (campaigns4UsersTypes.isLast())
              {
                userTypeNames += campaigns4UsersTypes.getString("USER_TYPE_NAME");
              }
            else
              {
                userTypeNames += campaigns4UsersTypes.getString("USER_TYPE_NAME") + " or ";
              }

            //            if (!userTypeNameArray.contains(userTypeName)){
            //            userTypeName += userTypeName +" ";
            //            }
          }

        campaigns4UsersTypes.getStatement().close();
        campaigns4UsersTypes.close();
      }
    catch (SQLException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
       DBConnection.closeConnections ( conn );
        return null;
      }


    ResultSet giftValue = 
      ExecuteQueries.executeQuery(conn,"Select GIFT_VALUE from GVMS_GIFT where GIFT_ID=" + giftId);

    try
      {
        if (giftValue.next())
          {
            gValue = "" + giftValue.getLong("GIFT_VALUE");
          }
        giftValue.getStatement().close();
        giftValue.close();

      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    if (dialNumber.compareTo("1") == 0)
      {

        String SQL = "";


        try
          {
            for (int count = 0; count < row.size(); count++)
              {


                String strFieldId = 
                  row.get(count).getFieldId ( )+""; // this String to get correct coulmn name.
                String campaignName =row.get(count).getCampaignName(); 

                
                String field = ("," + campaignName + "_" + campaignId + "_" + strFieldId);
                totalFields += field;
                //System.out.println(totalFields);


                //System.out.println(dt.get(count).getInputValue());
                if (dt.get(count).getInputValue() != null)
                  {
                      
                    values = 
                        ("," + getDynamicValue(dt.get(count).getInputValue())); // to collect values from interface
                      
                  }
                
                         if (dt.get(count).getInputValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  
                  {
                      values = 
                          ("," + "''"); // to collect values from interface
                  }
                if (dt.get(count).getComboValue() != null)
                  {

                    values = 
                        ("," + getDynamicValue(dt.get(count).getComboValue())); // to collect values from interface

                    //                        ResultSet validFieldRS = ExecuteQueries.executeQuery("select t1.VALIDATION_LIST_ITEM,t2.VALIDATION_LIST_VALIDITY from GVMS_VALIDATION_LIST_ITEM t1 left join GVMS_VALIDATION_LIST t2 on t1.VALIDATION_LIST_ID = t2.VALIDATION_LIST_ID where t2.field_id = "+row.get(count).getFieldId()+" and t1.VALIDATION_LIST_ITEM='"++"'");
                  }
                         if (dt.get(count).getComboValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  
                     {
                         values = 
                             ("," + "''"); // to collect values from interface
                     }
                if (dt.get(count).getTextValue() != null)
                  {
                    values = 
                        ("," + getDynamicValue(dt.get(count).getTextValue())); // to collect values from interface
                  }
                             if (dt.get(count).getTextValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  
                         {
                             values = 
                                 ("," + "''"); // to collect values from interface
                         }


                totalValues += values;
                Indx++;
                //System.out.println("totalValues 1-7-2009 issssssss  "+ totalValues);

              }


            // add gift Id and gift values
            if (userTypeNameArray.contains(userTypeId))
              {
            	
                //                SQL = 
                //                    "insert into GVMS_REDEEM_CAMPAIGN_" + campaignId + " (REDEEM_CAMPAIGN_ID,USER_TYPE_ID,USER_ID,CURRENT_VOUCHER_ID,VOUCHER_DATE_TIME,REDEEM_VOUCHER_NUMBER,GIFT_ID,GIFT_VALUE" + 
                //                    totalFields + ") VALUES (GVMS_REDEEM_CAMPAIGN_" + campaignId + 
                //                    "_SEQ_ID.nextval," + userTypeId + "," + userId + "," + custCampaignId + 
                //                    ",to_date('" + getDateTime() + "', 'mm/dd/yyyy HH24:MI:SS')," + 
                //                    voucher_Number + "," + giftId + "," + gValue + totalValues + ")";

                //


//                SQL = 
//                    "insert into GVMS_REDEEM_CAMPAIGN_" + campaignId + " (REDEEM_CAMPAIGN_ID,CURRENT_VOUCHER_ID,REDEEM_VOUCHER_NUMBER" + 
//                    totalFields + ") VALUES (GVMS_REDEEM_CAMPAIGN_" + campaignId + 
//                    "_SEQ_ID.nextval," + custCampaignId + "," + voucher_Number + totalValues + ")";
try {
	//System.out.println("before parse giftName "+giftName);
	Integer.parseInt(giftName);
	giftId = giftName;
	//System.out.println("after parse giftId "+giftId);
} catch (Exception e) {
	//System.out.println("before exception giftName "+giftName);
	Statement getGiftIdST = conn.createStatement();
	ResultSet getGiftIdRS = getGiftIdST.executeQuery("select GIFT_ID from GVMS_GIFT where GIFT_NAME='"+ giftName);
	//System.out.println("before exception giftName select GIFT_ID from GVMS_GIFT where GIFT_NAME='"+ giftName+"'");
	if (getGiftIdRS.next()){
		giftId = getGiftIdRS.getInt(1)+"";	
	}
	//System.out.println("before exception giftId "+ giftId);
	getGiftIdST.close();
	getGiftIdRS.close();
	// TODO: handle exception
}
             SQL = 
                 "insert into GVMS_REDEEM_CAMPAIGN_" + campaignId + " (REDEEM_CAMPAIGN_ID,"+
                 "REDEEM_VOUCHER_NUMBER, CURRENT_VOUCHER_ID"+totalFields+") VALUES (CAMPAIGN_" + campaignId + 
                 "_SEQ.nextval," + voucher_Number + "," + custCampaignId + totalValues + ")";

                //System.out.println(SQL);
                //System.out.println("voucherNumber is " + voucher_Number);
                int rsetInsert = ExecuteQueries.executeNoneQuery(SQL);
                SQL = 
                    "update GVMS_CURRENT_VOUCHER set REDEMPTION_DATE=sysdate,VOUCHER_STATUS_ID=1, USER_TYPE_ID=" + userTypeId + 
                    ", USER_ID=" + userId + " ," + " CURRENT_VOUCHER_END_DATE=sysdate, gift_Type_Id="+giftTypeId+",GIFT_ID = '"+ giftId +"'"+  
                    " , GIFT_VALUE=(select GIFT_VALUE from GVMS_GIFT where GIFT_ID='"+ giftId +"') , CAMPAIGN_ID=" + campaignId + 
                    " where CURRENT_VOUCHER_NUMBER=" + voucher_Number;

                
                if (rsetInsert > 0)
                  {
                	int rset = ExecuteQueries.executeNoneQuery(SQL);
                    if (path.compareTo("Ar")==0)
                      {
                        redeemFail = Login.getArabicErrorDTO().getRedemtoncomplete();
                        jsfUtils.putInSession(voucher_Number, "isRedeemed");
                        jsfUtils.removeFromSession("Search_Bean");
                        DBConnection.closeConnections ( conn );
                        
                        return null;
                      }
                    else
                      {
                        redeemFail = "Voucher Redeemed successfully";
                        jsfUtils.putInSession(voucher_Number, "isRedeemed");
                        jsfUtils.removeFromSession("Search_Bean");
                        DBConnection.closeConnections ( conn );
                        return null;
                      }
                    


                  }
                else
                  {
                    if (path.compareTo("Ar")==0)
                      {
                        redeemFail = Login.getArabicErrorDTO().getRedemptionfailed();
                        DBConnection.closeConnections ( conn );
                        return null;
                      }
                    else
                      {
                        redeemFail = "Voucher Redemption Failed";
                        DBConnection.closeConnections ( conn );
                        return null;
                      }
                  }
              }
            else

              {
                if (path.compareTo("Ar")==0)
                  {
                    redeemFail = Login.getArabicErrorDTO().getNotauthorized() ;
                    DBConnection.closeConnections ( conn );
                    return null;
                  }
                else
                  {
                    redeemFail = 
                        "You are not authorized to redeem this voucher from this shop.";
                    DBConnection.closeConnections ( conn );
                    return null;
                  }
              }


          }
        catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }


    if (dialNumber.compareTo("2") == 0)
      {


        String SQL = "";


        try
          {
            for (int count = 0; count < row.size(); count++)
              {

                String arrayValue = row.get(count).getFieldDbName();
                
                String field = ("," + arrayValue);
                totalFields += field;
                //System.out.println(totalFields);


                //System.out.println(dt.get(count).getInputValue());
                if (dt.get(count).getInputValue() != null)
                  {
                    values = 
                        ("," + getDynamicValue(dt.get(count).getInputValue())); // to collect values from interface
                  }
                      if (dt.get(count).getInputValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)
                      {
                  values = ("," + "''"); // to collect values from interface
                      }
                if (dt.get(count).getComboValue() != null)
                  {
                    values = 
                        ("," + getDynamicValue(dt.get(count).getComboValue())); // to collect values from interface
                  }
                             if (dt.get(count).getComboValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  
                             {
                         values = ("," + "''"); // to collect values from interface
                             }
                if (dt.get(count).getTextValue() != null)
                  {
                    values = 
                        ("," + getDynamicValue(dt.get(count).getTextValue())); // to collect values from interface
                  }
                             if (dt.get(count).getTextValue() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  
                             {
                         values = ("," + "''"); // to collect values from interface
                             }

                if (dt.get(count).getDate() != null)
                  {
                    values = 
                        ("," + convertDate(dt.get(count).getDate())); // to collect values from interface
                  }
                         if (dt.get(count).getDate() == null && row.get(count).getFieldIsMandatory().compareTo("0")==0)  {
                         values = ("," + "''"); // to collect values from interface
                         }

                totalValues += values;
                Indx++;
                  //System.out.println("totalValues 1-7-2009 Dail issssssss  "+ totalValues);

              }
            if (userTypeNameArray.contains(userTypeId))
              {

            	
            	try {
            		//System.out.println("before parse giftName "+giftName);
            		Integer.parseInt(giftName);
            		giftId = giftName;
            		//System.out.println("after parse giftId "+giftId);
            	} catch (Exception e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            		//System.out.println("before exception giftName "+giftName);
            		Statement getGiftIdST = conn.createStatement();
            		ResultSet getGiftIdRS = getGiftIdST.executeQuery("select GIFT_ID from GVMS_GIFT where GIFT_NAME='"+ giftName+"'");
            		
            		if (getGiftIdRS.next()){
            			giftId = getGiftIdRS.getInt(1)+"";	
            		}
            		//System.out.println("before exception giftId "+ giftId);
            		getGiftIdST.close();
            		getGiftIdRS.close();
            		// TODO: handle exception
            	}
            	
            	
                SQL = 
                    "insert into GVMS_REDEEM_CAMPAIGN_" + campaignId + "(REDEEM_CAMPAIGN_ID, REDEEM_VOUCHER_DIAL_NUMBER,"+
                    "REDEEM_VOUCHER_NUMBER, CURRENT_VOUCHER_ID"+totalFields+") VALUES (CAMPAIGN_" + campaignId + 
                    "_SEQ.nextval,"+dialInputText+"," + voucher_Number  + "," + custCampaignId +  
                     totalValues + ")";
                //System.out.println(SQL);
                int rsetInsert = ExecuteQueries.executeNoneQuery(SQL);
                SQL =  
                    "update GVMS_CURRENT_VOUCHER set REDEMPTION_DATE=sysdate,VOUCHER_STATUS_ID=1, USER_TYPE_ID=" + userTypeId + 
                    ", USER_ID=" + userId + " ," + " CURRENT_VOUCHER_END_DATE=sysdate, gift_Type_Id="+giftTypeId+", GIFT_ID = '"+giftId+"'"+
                    " , GIFT_VALUE=(select GIFT_VALUE from GVMS_GIFT where GIFT_ID='"+ giftId +"') , CAMPAIGN_ID=" + campaignId + 
                    " where CURRENT_VOUCHER_NUMBER=" + voucher_Number;
                
                if (rsetInsert > 0)
                  {
                	int rset = ExecuteQueries.executeNoneQuery(SQL);
                    //                        SQL = 
                    //                            "update GVMS_CURRENT_VOUCHER set VOUCHER_STATUS_ID=1 where CURRENT_VOUCHER_NUMBER=" + 
                    //                            voucher_Number;
                    if (path.compareTo("Ar")==0)
                      {
                        redeemFail = Login.getArabicErrorDTO().getRedemtoncomplete();
                        jsfUtils.putInSession(voucher_Number, "isRedeemed");
                        jsfUtils.removeFromSession("Search_Bean");
                        DBConnection.closeConnections ( conn );
                        return null;
                      }
                    else
                      {
                        redeemFail = "Voucher Redeemed successfully";
                        jsfUtils.putInSession(voucher_Number, "isRedeemed");
                        jsfUtils.removeFromSession("Search_Bean");
                        DBConnection.closeConnections ( conn );
                        return null;
                      }

                    


                  }
                else
                  {
                    if (path.compareTo("Ar")==0)
                      {
                        redeemFail = Login.getArabicErrorDTO().getRedemptionfailed();
                        DBConnection.closeConnections ( conn );
                        return null;
                      }
                    else
                      {
                        redeemFail = "Voucher Redemption Failed";
                        DBConnection.closeConnections ( conn );
                        return null;
                      }

                  }


              }
            else
              {
                if (path.compareTo("Ar")==0)
                  {
                    redeemFail = Login.getArabicErrorDTO().getNotauthorized() + " " + userTypeNames;
                    return null;
                  }
                else
                  {
                    redeemFail = 
                        "You are not authorized to redeem this voucher from this shop." ;
                    return null;
                  }
              }
          }

        catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);

            if (path.compareTo("Ar")==0)
              {
                redeemFail = Login.getArabicErrorDTO().getRedemptionfailed();
                DBConnection.closeConnections ( conn );
                return null;
              }
            else
              {
                redeemFail = "Redeeming process Fails";
                DBConnection.closeConnections ( conn );
                return null;
              }
          }


      }

    return null;

  }

  public String convertDate(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;
        int houre = undefineDate.getHours();
        int min = undefineDate.getMinutes();
        int sec = undefineDate.getSeconds();

        values = 
            "to_date('" + month + "/" + day + "/" + year + " " + houre + ":" + min + ":" + sec + 
            "', 'MM/DD/YYYY HH24:MI:SS')";

        return values;
      }
  }

  public String reSearch()
  {
//    try
//      {
//        FacesContext.getCurrentInstance().getExternalContext().redirect("faces/Search.jsp");
//      }
//    catch (IOException e)
//      {
//         
//      }
    boolean found = 
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("Unredeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Unredeemed_Bean");

    //System.out.println("Search is:" + found);
    return "userSearch";
  }


  public String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }


  public String getDynamicValue(String values)
  {
    //System.out.println(Indx);
    if (dt.get(Indx).getFieldType().compareTo("String") == 0 || 
        dt.get(Indx).getFieldType().compareTo(" ") == 0)
      {
        if (values == null)
          {
            values = "";
          }
        values = "'" + values + "'";
        //System.out.println(values);
        return values;
      }
    if (dt.get(Indx).getFieldType().compareTo("Integer") == 0)
      {
        if (values == null)
          {
            values = "0";
          }
        //System.out.println(values);
        return "'"+values+"'";
      }
    if (dt.get(Indx).getFieldType().compareTo("Date") == 0)
      {
        if (values == null)
          {
            ;
            String[] toDate = new String[3];
            toDate[0] = "00";
            toDate[1] = "00";
            toDate[2] = "0000";
            values = 
                "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
            return values;
          }
        else
          {
            String[] toDate = values.split("/");

            values = 
                "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
            //System.out.println(values);
            return values;
          }
      }
    return null;
  }

  public ArrayList<SelectItem> getCampaigGift(String selectedCampaign){
     try{
         gifts = new ArrayList<SelectItem>();
         Connection conn = DBConnection.getConnection();
         //System.out.println("here in getCampaigGift");
         if (selectedCampaign==null||selectedCampaign.compareTo("")==0)
         {
             selectedCampaign="0";
         }
         gifts = CSDao.getGifts(conn,selectedCampaign);
//         gifts.add(0, new SelectItem("-1","---Select One---"));
//     if (gifts.size()>0)
//         {           
//         gifts.add(1, new SelectItem("0","Any"));            
//         }       
     DBConnection.closeConnections ( conn );
     return gifts;
 } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
     // TODO Auto-generated catch block
//     error = "The system coulden't load gifts for selected campaign.";
     return null;
 }
 }
  
  public void setField0Value(String field0Value)
  {
    this.field0Value = field0Value;
  }

  public String getField0Value()
  {
    return field0Value;
  }

  public void setCheckLbl(HtmlOutputText outputText1)
  {
    this.checkLbl = outputText1;
  }

  public HtmlOutputText getCheckLbl()
  {
    return checkLbl;
  }


  public void setItems(ArrayList<SelectItem> items)
  {
    this.items = items;
  }

  public ArrayList<SelectItem> getItems()
  {
    return items;
  }

  public void setCommandButton1(HtmlCommandButton commandButton1)
  {
    this.commandButton1 = commandButton1;
  }

  public HtmlCommandButton getCommandButton1()
  {
    return commandButton1;
  }


  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }


  public ArrayList<DatatableDTO> getDt()
  {


    return dt;
  }

  public void setDt(ArrayList<DatatableDTO> dt)
  {

    this.dt = dt;
  }


  public void setIncreVar(int increVar)
  {
    this.increVar = increVar;
  }

  public int getIncreVar()
  {
    return increVar;
  }

  public void setDate(Date date)
  {
    this.date = date;
  }

  public Date getDate()
  {
    return date;
  }

  public void setCarNumber(Integer carNumber)
  {
    this.carNumber = carNumber;
  }

  public Integer getCarNumber()
  {
    return carNumber;
  }

  public void setVoucher_Number(String voucher_Number)
  {
    this.voucher_Number = voucher_Number;
  }

  public String getVoucher_Number()
  {
    return voucher_Number;
  }


  public void setCampaignId(String campaignId)
  {
    this.campaignId = campaignId;
  }

  public String getCampaignId()
  {
    return campaignId;
  }


  public void setOutputLabel2(HtmlOutputLabel outputLabel2)
  {
    this.outputLabel2 = outputLabel2;
  }

  public HtmlOutputLabel getOutputLabel2()
  {
    return outputLabel2;
  }


  public void setVoucherOutputLabel1(HtmlOutputLabel voucherOutputLabel1)
  {
    this.voucherOutputLabel1 = voucherOutputLabel1;
  }

  public HtmlOutputLabel getVoucherOutputLabel1()
  {
    return voucherOutputLabel1;
  }

  public void setDialOutputLabel(HtmlOutputLabel dialOutputLabel)
  {
    this.dialOutputLabel = dialOutputLabel;
  }

  public HtmlOutputLabel getDialOutputLabel()
  {
    return dialOutputLabel;
  }

  public void setField1OutputLabel(HtmlOutputLabel field1OutputLabel)
  {
    this.field1OutputLabel = field1OutputLabel;
  }

  public HtmlOutputLabel getField1OutputLabel()
  {
    return field1OutputLabel;
  }


  public void setField2Validator(HtmlOutputLabel outputLabel10)
  {
    this.field2Validator = outputLabel10;
  }

  public HtmlOutputLabel getField2Validator()
  {
    return field2Validator;
  }

  public void setRow(ArrayList<AdditionalFieldDTO> row)
  {
    this.row = row;
  }

  public ArrayList<AdditionalFieldDTO> getRow()
  {
    return row;
  }

  public void setRedeemFailAr(String redeemFailAr)
  {
    this.redeemFailAr = redeemFailAr;
  }

  public String getRedeemFailAr()
  {
    return redeemFailAr;
  }

  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }


  public void setDialNumberError(HtmlOutputLabel dialNumberError)
  {
    this.dialNumberError = dialNumberError;
  }

  public HtmlOutputLabel getDialNumberError()
  {
    return dialNumberError;
  }

  public void setVoucherStatus(Integer voucherStatus)
  {
    this.voucherStatus = voucherStatus;
  }

  public Integer getVoucherStatus()
  {
    return voucherStatus;
  }

  public void setRenderDialNumber(Boolean renderDialNumber)
  {
    this.renderDialNumber = renderDialNumber;
  }

  public Boolean getRenderDialNumber()
  {
    return renderDialNumber;
  }

  public void setDialInputText(String dialInputText)
  {
    this.dialInputText = dialInputText;
  }

  public String getDialInputText()
  {
    return dialInputText;
  }

  public void setRedeemFail(String redeemFail)
  {
    this.redeemFail = redeemFail;
  }

  public String getRedeemFail()
  {
    return redeemFail;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }




   public void setRenderGift(Boolean renderGift)
   {
      this.renderGift = renderGift;
   }




   public Boolean getRenderGift()
   {
      return renderGift;
   }
}
