package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.user.DTO.RedeemDTO;

import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class Redeemed
{
  public HtmlForm form1;
  
  public String campaignId;
  ArrayList<String> fieldName = new ArrayList<String>();
  //    ArrayList<String> colName= new ArrayList<String>();
  public ArrayList additionalField = new ArrayList();
  public ArrayList fieldValue = new ArrayList();
  public ArrayList<RedeemDTO> redeemInfo = new ArrayList<RedeemDTO>();
  public String giftId;
  public Boolean renderDial;
  public String giftName;
  public String dialValue;
  public String voucherDate;
  public String posUser;
  public String userName;

  public String voucherNumber;
  public String currenVoucherId;
  public HtmlOutputLabel outputLabel3;
  public HtmlOutputLabel outputLabel4;
  public HtmlOutputLabel outputLabel6;
  public HtmlOutputLabel outputLabel2;
  public HtmlOutputLabel outputLabel1;
  public HtmlOutputLabel outputLabel5;
  public HtmlOutputLabel outputLabel7;
  public HtmlOutputLabel outputText1;
  public HtmlOutputLabel outputLabel8;

  public HtmlDataTable dataTable1;
  public HtmlOutputLabel outputLabel9;
  String path;
    public String usrIdHiden;


  public Redeemed() throws SQLException
  {
    String dialNumber = new String();
    Connection conn = DBConnection.getConnection();
    //        ArrayList fieldsNames = new ArrayList();
       path = (String)jsfUtils.getFromSession("userTypeInterface");
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
          {
              try
                {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
                }
              catch (Exception e)
                {
                   
                }
          }

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
               {
                 FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
               }
             catch (Exception e)
               {
                  
               }
         }
      }
    Map context = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    campaignId = (String) context.get("cid");
    currenVoucherId = (String) context.get("ccid");
    giftId = (String) context.get("gid");
    voucherNumber = (String) context.get("vouNum");

    String giftNameField = "";

    if (path.compareTo("Ar")==0)
      {
        giftNameField = "GIFT_NAME_ARABIC";

      }
    else
      {

        giftNameField = "GIFT_NAME";
      }

    ResultSet rset = 
      ExecuteQueries.executeQuery(conn,"Select " + giftNameField + " from GVMS_GIFT where GIFT_ID=" + 
                                  giftId);
    try
      {
        if (rset.next())
          {
            giftName = rset.getString(giftNameField);
          }
        rset.getStatement().close();
        rset.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    ResultSet result = 
      ExecuteQueries.executeQuery(conn,"Select FIELD_DB_NAME from GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID=" + 
                                  campaignId);

    try
      {
        int count = 0;
        String colNameDb = new String();
        while (result.next())


          {
            colNameDb = result.getString("FIELD_DB_NAME");

            fieldName.add(colNameDb);

//            if (colNameDb.contains("_"))
//              {
//                ResultSet rsGetValue = 
//                  ExecuteQueries.executeQuery(conn,"Select " + fieldName.get(count) + 
//                                              ",REDEEM_VOUCHER_DIAL_NUMBER from GVMS_REDEEM_CAMPAIGN_" + 
//                                              campaignId + " where REDEEM_VOUCHER_NUMBER=" + 
//                                              voucherNumber);
//                if (rsGetValue.next())
//                  {
//                    RedeemDTO fDTO = new RedeemDTO();
//                    dialNumber = "" + rsGetValue.getInt("REDEEM_VOUCHER_DIAL_NUMBER");
//                    String name = colNameDb.replace("_", " ");
//                    String value = rsGetValue.getString(fieldName.get(count));
//                    fDTO.setFieldValue(value);
//                    fDTO.setFieldName(name);
//                    redeemInfo.add(fDTO);
//                  }
//                rsGetValue.getStatement().close();
//                rsGetValue.close();
//
//              }
//            else
//              {
                ResultSet rsGetValue = 
                  ExecuteQueries.executeQuery(conn,"Select " + fieldName.get(count) + 
                                              ",REDEEM_VOUCHER_DIAL_NUMBER from GVMS_REDEEM_CAMPAIGN_" + 
                                              campaignId + " where REDEEM_VOUCHER_NUMBER=" + 
                                              voucherNumber);
                if (rsGetValue.next())
                  {
                    RedeemDTO fDTO = new RedeemDTO();
                    dialNumber = "" + rsGetValue.getInt("REDEEM_VOUCHER_DIAL_NUMBER");
                    String value = rsGetValue.getString(fieldName.get(count));
                    fDTO.setFieldValue(value);
                    fDTO.setFieldName(colNameDb);
                    redeemInfo.add(fDTO);
                  }
                rsGetValue.getStatement().close();
                rsGetValue.close();

//              }

            //                
            count++;
          }
        result.getStatement().close();
        result.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    //System.out.println("DN is" + dialNumber);
    if (dialNumber.compareTo("0") == 0)
      {
        renderDial = false;
      }
    else
      {
        ResultSet rsGetValue = 
          ExecuteQueries.executeQuery(conn,"Select REDEEM_VOUCHER_DIAL_NUMBER from GVMS_REDEEM_CAMPAIGN_" + 
                                      campaignId + " where REDEEM_VOUCHER_NUMBER=" + 
                                      voucherNumber);
        renderDial = true;
        try
          {
            if (rsGetValue.next())
              {
                dialValue = "" + rsGetValue.getInt("REDEEM_VOUCHER_DIAL_NUMBER");
              }
            rsGetValue.getStatement().close();
            rsGetValue.close();


          }
        catch (SQLException e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }

    ResultSet voucherCampaign = 
      ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_ID From GVMS_CURRENT_VOUCHER where CURRENT_VOUCHER_NUMBER='" + 
                                  voucherNumber + "'");
    String camp_ID = "";
    try
      {
        if (voucherCampaign.next())
          {
            camp_ID = voucherCampaign.getInt("CAMPAIGN_ID") + "";
          }
        voucherCampaign.getStatement().close();
        voucherCampaign.close();
      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);

      }

    if (camp_ID.compareTo("") != 0)
      {

//String SQL = "select GCV.USER_ID,to_char(GCV.CURRENT_VOUCHER_END_DATE,'DD-MM-YYYY') VOUCHER_DATE_TIME,GU.USER_FULL_NAME,GU.USER_POS_CODE from GVMS_CURRENT_VOUCHER GCV,GVMS_USER GU,GVMS_SYSTEM_USER GSU where  GCV.USER_ID = GU.USER_ID and GU.SYSTEM_USER_ID = GSU.SYSTEM_USER_ID and GCV.CURRENT_VOUCHER_NUMBER='"+voucherNumber + "'";
        String SQL = 
        "SELECT GCV.USER_ID,TO_CHAR (GCV.CURRENT_VOUCHER_END_DATE, 'DD-MM-YYYY') VOUCHER_DATE_TIME,GU.USER_FULL_NAME, GU.USER_POS_CODE FROM GVMS_CURRENT_VOUCHER GCV, GVMS_USER GU WHERE GU.SYSTEM_USER_ID = GCV.USER_ID AND GCV.CURRENT_VOUCHER_NUMBER ='"+voucherNumber+"'";
//          "Select t1.USER_ID,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD-MM-YYYY') VOUCHER_DATE_TIME,t2.USER_FULL_NAME,t2.USER_POS_CODE from GVMS_CURRENT_VOUCHER" + 
//          " t1 left join GVMS_USER t2 on t1.USER_ID = t2.USER_ID where t1.CURRENT_VOUCHER_NUMBER='" + 
//          voucherNumber + "'";

        ResultSet userInfo = ExecuteQueries.executeQuery(conn,SQL);

        try
          {
           
            if (userInfo.next())
              {
                voucherDate = userInfo.getString("VOUCHER_DATE_TIME");
                userName = userInfo.getString("USER_FULL_NAME");
                posUser = userInfo.getString("USER_POS_CODE");
              }
            userInfo.getStatement().close();
            userInfo.close();
            DBConnection.closeConnections ( conn );
          }
        catch (SQLException e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

      }


  }

  public String reSearch()
  {

//    try
//      {

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/SearchArabic.jsp");
//      }
//    catch (Exception e)
//      {
//         
//      }


    return "userSearchAr";
  }

  public String reSearchEn()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");
       

//    try
//      {
//
//
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Search.jsp");
//      }
//    catch (Exception e)
//      {
//         
//      }


    return "userSearch";
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setCampaignId(String campaignId)
  {
    this.campaignId = campaignId;
  }

  public String getCampaignId()
  {
    return campaignId;
  }


  public void setGiftId(String giftId)
  {
    this.giftId = giftId;
  }

  public String getGiftId()
  {
    return giftId;
  }


  public void setOutputLabel3(HtmlOutputLabel outputLabel3)
  {
    this.outputLabel3 = outputLabel3;
  }

  public HtmlOutputLabel getOutputLabel3()
  {
    return outputLabel3;
  }


  public void setOutputLabel4(HtmlOutputLabel outputLabel4)
  {
    this.outputLabel4 = outputLabel4;
  }

  public HtmlOutputLabel getOutputLabel4()
  {
    return outputLabel4;
  }


  public void setOutputLabel6(HtmlOutputLabel outputLabel6)
  {
    this.outputLabel6 = outputLabel6;
  }

  public HtmlOutputLabel getOutputLabel6()
  {
    return outputLabel6;
  }

  public void setOutputLabel2(HtmlOutputLabel outputLabel2)
  {
    this.outputLabel2 = outputLabel2;
  }

  public HtmlOutputLabel getOutputLabel2()
  {
    return outputLabel2;
  }

  public void setOutputLabel1(HtmlOutputLabel outputLabel1)
  {
    this.outputLabel1 = outputLabel1;
  }

  public HtmlOutputLabel getOutputLabel1()
  {
    return outputLabel1;
  }

  public void setOutputLabel5(HtmlOutputLabel outputLabel5)
  {
    this.outputLabel5 = outputLabel5;
  }

  public HtmlOutputLabel getOutputLabel5()
  {
    return outputLabel5;
  }


  public void setOutputLabel7(HtmlOutputLabel outputLabel7)
  {
    this.outputLabel7 = outputLabel7;
  }

  public HtmlOutputLabel getOutputLabel7()
  {
    return outputLabel7;
  }


  public void setOutputLabel8(HtmlOutputLabel outputLabel8)
  {
    this.outputLabel8 = outputLabel8;
  }

  public HtmlOutputLabel getOutputLabel8()
  {
    return outputLabel8;
  }

  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }

  public void setAdditionalField(ArrayList additionalField)
  {
    this.additionalField = additionalField;
  }

  public ArrayList getAdditionalField()
  {
    return additionalField;
  }


  public void setFieldValue(ArrayList fieldValue)
  {
    this.fieldValue = fieldValue;
  }

  public ArrayList getFieldValue()
  {
    return fieldValue;
  }


  public void setRedeemInfo(ArrayList<RedeemDTO> redeemInfo)
  {
    this.redeemInfo = redeemInfo;
  }

  public ArrayList<RedeemDTO> getRedeemInfo()
  {
    return redeemInfo;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setVoucherNumber(String voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public String getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setOutputLabel9(HtmlOutputLabel outputLabel9)
  {
    this.outputLabel9 = outputLabel9;
  }

  public HtmlOutputLabel getOutputLabel9()
  {
    return outputLabel9;
  }

  public void setRenderDial(Boolean renderDial)
  {
    this.renderDial = renderDial;
  }

  public Boolean getRenderDial()
  {
    return renderDial;
  }

  public void setDialValue(String dialValue)
  {
    this.dialValue = dialValue;
  }

  public String getDialValue()
  {
    return dialValue;
  }

  public void setOutputText1(HtmlOutputLabel outputText1)
  {
    this.outputText1 = outputText1;
  }

  public HtmlOutputLabel getOutputText1()
  {
    return outputText1;
  }


  public void setVoucherDate(String voucherDate)
  {
    this.voucherDate = voucherDate;
  }

  public String getVoucherDate()
  {
    return voucherDate;
  }

  public void setPosUser(String posUser)
  {
    this.posUser = posUser;
  }

  public String getPosUser()
  {
    return posUser;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }
}
