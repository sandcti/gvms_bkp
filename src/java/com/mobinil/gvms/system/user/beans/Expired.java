package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class Expired
{
  
  public String voucherNumber;
  public String expiredDate;
  String path;
    public String usrIdHiden;

  public Expired()
  {

      String path = (String)jsfUtils.getFromSession("userTypeInterface");
      usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
               {
                 FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
               }
             catch (Exception e)
               {
                  new com.mobinil.gvms.system.utility.PrintException().printException(e);
               }
         }

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
               {
                 FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
               }
             catch (Exception e)
               {
                  new com.mobinil.gvms.system.utility.PrintException().printException(e);
               }
         }
      }

    String campaignId = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cid");
    voucherNumber = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("vouNum");
    
    try
      {
       Connection conn = DBConnection.getConnection();
       ResultSet rs = 
         ExecuteQueries.executeQuery(conn,"Select to_char(CAMPAIGN_CLOSED_DATE,'DD-MM-YYYY') CAMPAIGN_CLOSED_DATE from GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                     campaignId);
        if (rs.next())
          {
            expiredDate = rs.getString("CAMPAIGN_CLOSED_DATE");
          }
        rs.getStatement().close();
        rs.close();
        DBConnection.closeConnections ( conn );
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
  }

  public void setVoucherNumber(String voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public String getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setExpiredDate(String expiredDate)
  {
    this.expiredDate = expiredDate;
  }

  public String getExpiredDate()
  {
    return expiredDate;
  }

    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }
}
