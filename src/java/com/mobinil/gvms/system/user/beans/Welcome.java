package com.mobinil.gvms.system.user.beans;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.context.FacesContext;
import com.mobinil.gvms.system.utility.*;
import javax.servlet.http.HttpServletRequest;


public class Welcome
{
  public HtmlOutputLink outputLink1;
  public HtmlOutputLink outputLink2;
  public HtmlForm form1;
  public HtmlForm form2;
  public HtmlOutputLabel userNameLbl;
  public String userNameValue;
  public String logout;
  public String usrIdHiden;


  public Welcome()
  {
    String userName = new String();
    
     usrIdHiden = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemUserId"); 
     //System.out.println("usrIdHiden in constructor is "+usrIdHiden);
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
    //System.out.println(path);
      HttpServletRequest req = 
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    if (path==null){
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","En");      
      if (req.getRequestURI().contains("Arabic")){
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeInterface","Ar");      
      }
          path = (String)jsfUtils.getFromSession("userTypeInterface");
      }
    if (path.compareTo("Ar")==0)
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
          {
              try
                {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
                }
              catch (Exception e)
                {
                   
                }
          }
        

      }

    else
      {
        if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("systemUserId")||usrIdHiden==null )
         {
             try
               {
                 FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
               }
             catch (Exception e)
               {
                  
               }
         }
        
      }
      userName = 
          (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userName");
      userNameValue = userName;

  }

  public void setOutputLink1(HtmlOutputLink outputLink1)
  {
    this.outputLink1 = outputLink1;
  }

  public HtmlOutputLink getOutputLink1()
  {
    return outputLink1;
  }

  public void setOutputLink2(HtmlOutputLink outputLink2)
  {
    this.outputLink2 = outputLink2;
  }

  public HtmlOutputLink getOutputLink2()
  {
    return outputLink2;
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setUserNameValue(String userNameValue)
  {
    this.userNameValue = userNameValue;
  }

  public String getUserNameValue()
  {
    return userNameValue;
  }

  public String searchAgain()
  {
//    try
//      {
//
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/SearchArabic.jsp");
//      }
//    catch (IOException e)
//      {
//         
//      }


    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");

    // Add event code here...
    return "userSearchAr";
  }

  public String searchAgainEn()
  {
//    try
//      {
//
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Search.jsp");
//      }
//    catch (IOException e)
//      {
//         
//      }


    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");

    // Add event code here...
    return "userSearch";
  }

  public String logOutAr()
  {
    //System.out.println("here in logouAR");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Login_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Unredeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Redeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("ForgetPass_Bean");


//    try
//      {
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
//      }
//    catch (IOException e)
//      {
//         
//      }
    return "logout_ar";
  }

 

  public String logOut()
  {
    //System.out.println("here in logoutEN");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Search_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Login_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Unredeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Redeemed_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("ForgetPass_Bean");


//    try
//      {
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
//      }
//    catch (IOException e)
//      {
//         
//      }
    return "logout";
  }


  public void setForm2(HtmlForm form2)
  {
    this.form2 = form2;
  }

  public HtmlForm getForm2()
  {
    return form2;
  }

  public void setLogout(String logout)
  {
    //System.out.println("Am In SEt Logout KLLLLLLLLLLLLLLLLLLLLLL");
    this.logout = logout;
  }

  public String getLogout()
  {
    logout = logOutAr();
    //System.out.println("logout is" + logout);
    return logout;
  }

  public void setUserNameLbl(HtmlOutputLabel userNameLbl)
  {
    this.userNameLbl = userNameLbl;
  }

  public HtmlOutputLabel getUserNameLbl()
  {
    return userNameLbl;
  }

 
    public void setUsrIdHiden(String usrIdHiden) {
        this.usrIdHiden = usrIdHiden;
    }

    public String getUsrIdHiden() {
        return usrIdHiden;
    }

   

//    public String get_search() {
//        String bridge = Search();
//        //System.out.println("bridge is" + bridge);
//
//        return bridge;
//    }
}
