package com.mobinil.gvms.system.user.beans;

import java.io.IOException;

import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

public class RedirectArabic
{
  public HtmlForm form1;

  public RedirectArabic()
  {

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/ArabicInterface/LoginArabic.jsp");
      }
    catch (IOException e)
      {

      }

  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }
}
