package com.mobinil.gvms.system.user.beans;

import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.cs.dto.CSLogsDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.user.DAO.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class ForgetPassword
{
  
  public String userName;
  public String posCode;
  public String phoneNumber;
  public String email;
  public String msgWarn;
  public Boolean renderGetPass = true;
  public Boolean renderConfrim = false;
  public Boolean renderNewPass = false;
  public Boolean renderRelogin = false;
  public String activeCode;
  Integer sysUserId;
  String rightConfirmationCode;
  public String password;
  public String confirmPassword;
  public String msgWarn1;
  String path;
  String resultphone="";
String emailStr ="";

  public ForgetPassword()
  {
    msgWarn = "";
    msgWarn1 = "";    
  }


  public String sendPassword()
  {
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
    Login loginBean = 
      (Login) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Login_Bean");
    String resultUseName = "";
//    , resultPOS = "", resultUseName = "", resultphone = "";
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }


    try
      {

        

        try
          {
           Connection conn = DBConnection.getConnection();
            String SQL1 = 
              "select t1.SYSTEM_USER_ID,t1.SYSTEM_USER_NAME USER_NAME,t1.USER_STATUS_ID USER_STATUS ,t2.USER_E_MAIL EMAIL, t2.USER_PHONE_NUMBER, t2.USER_POS_CODE from GVMS_SYSTEM_USER t1 left join GVMS_USER t2 on t1.System_User_id = t2.System_User_id where t1.USER_STATUS_ID = 1 and t1.SYSTEM_USER_TYPE='user' and t1.SYSTEM_USER_NAME='" + 
              userName + "'";
//              "and t2.USER_E_MAIL ='" + email + "' and  t2.USER_PHONE_NUMBER='" + 
//              phoneNumber + "' and t2.USER_POS_CODE ='" + posCode + "'";
            //System.out.println("SQL1 is " + SQL1);
            ResultSet result = ExecuteQueries.executeQuery(conn,SQL1);

            if (result.next())
              {
//                resultEmail = result.getString("EMAIL");
//                resultPOS = result.getString("USER_POS_CODE");
                resultUseName = result.getString("USER_NAME");
                resultphone = result.getString("USER_PHONE_NUMBER");
                sysUserId = result.getInt("SYSTEM_USER_ID");
emailStr = result.getString("EMAIL");
                String randomPass = "";
                String SQL = "select dbms_random.String('x',15) str from dual";

                try
                  {
                    ResultSet ranPassRS = ExecuteQueries.executeQuery(conn,SQL);
                    if (ranPassRS.next())
                      {
                        randomPass = ranPassRS.getString("str");
                      }
                    ranPassRS.getStatement().close();
                    ranPassRS.close();

                    int removeOldPass = 
                      ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set USER_CODE='" + 
                                                      randomPass + 
                                                      "', EXPIRE_CODE_DATE=sysdate where SYSTEM_USER_ID='" + 
                                                      sysUserId + "'");
                    
//System.out.println("removeOldPass issssssssss "+removeOldPass);
                    if (removeOldPass > 0)
                      {
                    	//System.out.println("USER_PHONE_NUMBER issssssssssss "+resultphone);
                    	//System.out.println("random number is "+randomPass);
//                    	CSDao.sendActivationSMS(resultphone,  "Copy this code and paste it to 'Activation Code', Code is " + 
//                              randomPass + " will expire tomorrow.");
                    	
                        String from = "mabdelaal@sandcti.com";
                        String to = emailStr;
                        String subject = "GVMS activation code";
                        String message = 
                          "Copy this code and paste it to 'Activation Code' \n" + "this code " + 
                          randomPass + " will expire tomorrow.";

                        SendMail sendMail = new SendMail(from, to, subject, message,randomPass);
                        sendMail.send();
                        renderGetPass = false;
                        renderConfrim = true;

                      }

                  }
                catch (Exception e)
                  {
                     

                	new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    msgWarn = "Error in sending mail call support.";
                  }


                result.getStatement().close();
                result.close();
              }
            else
              {
                if (path.compareTo("Ar")==0)
                  {
                    msgWarn = loginBean.getNamesArray().get(0).getInvalidValues();

                  }
                else
                  {
                    msgWarn = "Invalid user information";

                  }
              }
DBConnection.closeConnections ( conn );
            return null;
          }
        catch (SQLException e)
          {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);

          }

      }
    catch (Exception e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        if (path.compareTo("Ar")==0)
          {

            msgWarn = loginBean.getNamesArray().get(0).getInvalidValues();
            //System.out.println("msgWarn  is " + msgWarn);

          }
        else
          {
            msgWarn = "Invalid user information";
            //System.out.println("msgWarn  is in else" + msgWarn);
          }

      }


    return null;


  }

  public String confirmedAction()
  {
    String path = FacesContext.getCurrentInstance().getExternalContext().getRequestPathInfo();
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }

    try
      {
       Connection conn = DBConnection.getConnection();
        ResultSet getCodeExpireDate = 
          ExecuteQueries.executeQuery(conn,"select EXPIRE_CODE_DATE,USER_CODE from GVMS_SYSTEM_USER where SYSTEM_USER_ID='" + 
                                      sysUserId + "'");

        if (getCodeExpireDate.next())
          {
            Date expireCode = getCodeExpireDate.getDate("EXPIRE_CODE_DATE");
            int day = expireCode.getDate();
            int month = expireCode.getMonth() + 1;
            int year = expireCode.getYear() + 1900;
            //System.out.println("year in db is " + year + " current Year is " + 
//                               (new Date().getYear() + 1900));
            //System.out.println("month in db is " + month + " current Month is " + 
//                               (new Date().getMonth() + 1));
            //System.out.println("day in db is " + day + " current Day is " + new Date().getDate());
            if (day == new Date().getDate() && month == (new Date().getMonth() + 1) && 
                year == (new Date().getYear() + 1900))
              {

                rightConfirmationCode = getCodeExpireDate.getString("USER_CODE");

                if (rightConfirmationCode.compareTo(activeCode) == 0)
                  {
                    renderConfrim = false;
                    renderGetPass = false;
                    renderNewPass = true;

                    renderRelogin = true; // login with new password
                    if (path.compareTo("Ar")==0)
                      {
                        msgWarn = Login.getArabicErrorDTO().getRightactnumber();
                      }
                    else
                      {
                        msgWarn = "Activation code is correct.";
                      }
DBConnection.closeConnections ( conn );
                    return null;


                  }
                else
                  {

                    if (path.compareTo("Ar")==0)
                      {
                        msgWarn = Login.getArabicErrorDTO().getNotrightactnumber();
                      }
                    else
                      {
                        msgWarn = "Invalid activation code.";
                      }
                    DBConnection.closeConnections ( conn );
                    return null;


                  }
              }
            else
              {
                if (path.compareTo("Ar")==0)
                  {
                    msgWarn = Login.getArabicErrorDTO().getExpiredactivnumber();
                  }
                else
                  {
                    msgWarn = 
                        "This activation code has been expired, please regenerate another one.";
                  }
                DBConnection.closeConnections ( conn );
                return null;

              }
          }
        getCodeExpireDate.getStatement().close();
        getCodeExpireDate.close();
        DBConnection.closeConnections ( conn );
      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }

  public String reloginAction() throws SQLException
  {
    HttpServletRequest req = 
      (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    path = req.getRequestURI();
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }
    int updateUser = 0;
    //System.out.println("in back action");
    int lastPass = 0;
    if (password.compareTo(confirmPassword) == 0)
      {
        String x = securityImplement.checkPass(path, password, userName);
        if (x.compareTo("") != 0)
          {

            msgWarn1 = x;
            return null;

          }
        if (password.compareTo("") == 0 || confirmPassword.compareTo("") == 0)
          {
            if (path.compareTo("Ar")==0)
              {
                msgWarn1 = Login.getArabicErrorDTO().getPleaseinsertpassandcinf();
              }
            else
              {
                msgWarn1 = "Please insert password and confirm.";
              }
            return null;
          }
        try
          {
           Connection conn = DBConnection.getConnection();
            String passEncripted = MD5Class.MD5(password);
            ResultSet lastPassRS = 
              ExecuteQueries.executeQuery(conn,"select PROPERTIES from GVMS_PROPERTIES where reasone='LAST_PASSWORD_COUNT'");
            if (lastPassRS.next())
              {
                lastPass = lastPassRS.getInt("PROPERTIES");

              }
            lastPassRS.getStatement().close();
            lastPassRS.close();

            ResultSet checkPassExists = 
              ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_ID from gvms_PASS_TRACE  where PASSWORD='" + 
                                          passEncripted + "' and SYSTEM_USER_ID = " + sysUserId);


            if (checkPassExists.next())
              {
                if (path.compareTo("Ar")==0)
                  {
                    msgWarn1 = Login.getArabicErrorDTO().getPleasechangepassduetoinhistory();
                  }
                else
                  {
                    msgWarn1 = ("this password included in your last 6 passwords.");
                  }
                DBConnection.closeConnections ( conn );
                return null;

              }
            checkPassExists.getStatement().close();
            checkPassExists.close();

            ResultSet chckLst6Pswrd = 
              ExecuteQueries.executeQuery(conn,"select Max(PASSWORD_INDEX) passCount from gvms_PASS_TRACE where SYSTEM_USER_ID = " + 
                                          sysUserId + "");


            if (chckLst6Pswrd.next())
              {
            	int passCount = chckLst6Pswrd.getInt("passCount");
                if (passCount > 0 && 
                		passCount < (lastPass + 1))
                  {
                    if (passCount == lastPass)
                      {
                        updateUser = 
                            ExecuteQueries.executeNoneQuery("Update GVMS_SYSTEM_USER set SYSTEM_USER_PASSWORD = '" + 
                                                            MD5Class.MD5(password) + 
                                                            "' where SYSTEM_USER_ID=" + sysUserId);

                        if (updateUser == 0)
                          {
                            msgWarn1 = ("== 6 Invalid update system user table");
                            DBConnection.closeConnections ( conn );
                            return null;
                          }
                        else
                          {

                            int updatePassTrace = 
                              ExecuteQueries.executeNoneQuery("insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
                                                              sysUserId + ",'" + passEncripted + 
                                                              "'," + 
                                                              (passCount + 
                                                               1) + ")");
                            if (updatePassTrace == 0)
                              {
                                msgWarn1 = ("== 6 Invalid update Pass trace table");
                                DBConnection.closeConnections ( conn );
                                return null;
                              }
                            else
                              {

                                //System.out.println("result of upadate system  user is " + 
//                                                   updateUser);

                                updatePassTrace = 
                                    ExecuteQueries.executeNoneQuery("update gvms_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-1 where SYSTEM_USER_ID=" + 
                                                                    sysUserId);
                                if (updatePassTrace == 0)
                                  {
                                    msgWarn1 = 
                                        ("== 6 Invalid update Pass trace table to set 0 with 1");
                                    DBConnection.closeConnections ( conn );
                                    return null;
                                  }
                                else
                                  {
                                    int deletePasstrace = 
                                      ExecuteQueries.executeNoneQuery("delete from gvms_PASS_TRACE where SYSTEM_USER_ID =" + 
                                                                      sysUserId + 
                                                                      " and PASSWORD_INDEX=0");
                                    if (deletePasstrace == 0)
                                      {
                                        msgWarn1 = ("== 6 Invalid delete Pass trace table row 0");
                                        DBConnection.closeConnections ( conn );
                                        return null;
                                      }
                                    else
                                      {
                                        if (path.compareTo("Ar")==0)
                                          {
                                            msgWarn1 = Login.getArabicErrorDTO().getUpdatepasscomplete();
                                          }
                                        else
                                          {
                                            msgWarn1 = "Changing password complete";
                                          }
                                        DBConnection.closeConnections ( conn );
                                        return null;


                                      }
                                  }
                              }
                          }


                      }
                    else
                      {
                        updateUser = 
                            ExecuteQueries.executeNoneQuery("Update GVMS_SYSTEM_USER set SYSTEM_USER_PASSWORD = '" + 
                                                            MD5Class.MD5(password) + 
                                                            "' where SYSTEM_USER_ID=" + sysUserId);


                        if (updateUser == 0)
                          {

                            msgWarn1 = ("> 6 Invalid update system user table");
                            return null;
                          }
                        else
                          {
String sql = "insert into gvms_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
        sysUserId + ",'" + passEncripted + 
        "'," + 
        (passCount + 
         1) + ")";
////System.out.println("insert in pass trace is "+sql);
                            int updatePassTrace = 
                              ExecuteQueries.executeNoneQuery(sql);
                            if (updatePassTrace == 0)
                              {
                                msgWarn1 = ("> 6 Invalid update Pass trace table");
                                return null;
                              }
                            else
                              {


                                if (path.compareTo("Ar")==0)
                                  {
                                    msgWarn1 = Login.getArabicErrorDTO().getUpdatepasscomplete();
                                  }
                                else
                                  {
                                    msgWarn1 = "Changing password complete";
                                  }
                                return null;
                              }
                          }


                      }
                  }
              }
            chckLst6Pswrd.getStatement().close();
            chckLst6Pswrd.close();
            DBConnection.closeConnections ( conn );

          }
        catch (Exception e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }


      }
    else
      {
        if (path.compareTo("Ar")==0)
          {
            msgWarn1 = Login.getArabicErrorDTO().getPassandconfnotequal();
          }
        else
          {
            msgWarn1 = "Please retype password and confirmation.";
          }
        return null;


      }
    //    Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    //    HttpSession session =
    //      (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    //
    //    String[] beanNames = session.getValueNames();
    //    for (int x = 0; x < beanNames.length; x++)
    //      {
    //        if (beanNames[x].contains("Bean"))
    //          {
    //            if (!beanNames[x].contains("WelcomeAdmin_Bean"))
    //              {
    //                if (sessionMap.containsKey(beanNames[x]))
    //                  {
    //
    //                  }
    //              }
    //          }
    //
    //
    //        //System.out.println("bean names in place " + x + " is " + beanNames[x]);
    //      }
    //
    //    try
    //      {
    //
    //      }
    //    catch (Exception e)
    //      {
    //
    //      }
    return "relogin";
  }


  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }


  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

  public void setPosCode(String posCode)
  {
    this.posCode = posCode;
  }

  public String getPosCode()
  {
    return posCode;
  }

  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getEmail()
  {
    return email;
  }

  public void setRenderGetPass(Boolean renderGetPass)
  {
    this.renderGetPass = renderGetPass;
  }

  public Boolean getRenderGetPass()
  {
    return renderGetPass;
  }

  public void setRenderConfrim(Boolean renderConfrim)
  {
    this.renderConfrim = renderConfrim;
  }

  public Boolean getRenderConfrim()
  {
    return renderConfrim;
  }

  public void setRenderNewPass(Boolean renderNewPass)
  {
    this.renderNewPass = renderNewPass;
  }

  public Boolean getRenderNewPass()
  {
    return renderNewPass;
  }

  public void setActiveCode(String activeCode)
  {
    this.activeCode = activeCode;
  }

  public String getActiveCode()
  {
    return activeCode;
  }

  public void setRenderRelogin(Boolean renderRelogin)
  {
    this.renderRelogin = renderRelogin;
  }

  public Boolean getRenderRelogin()
  {
    return renderRelogin;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setConfirmPassword(String confirmPassword)
  {
    this.confirmPassword = confirmPassword;
  }

  public String getConfirmPassword()
  {
    return confirmPassword;
  }

  public void setMsgWarn1(String msgWarn1)
  {
    this.msgWarn1 = msgWarn1;
  }

  public String getMsgWarn1()
  {
    return msgWarn1;
  }
}
