package com.mobinil.gvms.system.user.DTO;

public class RedeemDTO
{
  public String fieldName;
  public String fieldValue;

  public RedeemDTO()
  {
  }

  public void setFieldName(String fieldName)
  {
    this.fieldName = fieldName;
  }

  public String getFieldName()
  {
    return fieldName;
  }

  public void setFieldValue(String fieldValue)
  {
    this.fieldValue = fieldValue;
  }

  public String getFieldValue()
  {
    return fieldValue;
  }
  }
