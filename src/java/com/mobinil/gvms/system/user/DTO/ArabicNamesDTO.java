package com.mobinil.gvms.system.user.DTO;

public class ArabicNamesDTO
{
  public String arabic;
  public String userName;
  public String password;
  public String forgetPassword;
  public String welcome;
  public String search;
  public String history;
  public String account;
  public String exit;
  public String voucherNumber;
  public String voucherNumber_;
  public String mobileNumber;
  public String titleUnredeemed;
  public String titleRedeemed;
  public String thisNumber;
  public String takeVoucher;
  public String titleHistory;
  public String phoneNumber;
  public String redeemtionDate;
  public String gift;
  public String giftValue;
  public String getPassword;
  public String userName_;
  public String phoneNumber_;
  public String posCode_;
  public String email_;
  public String voucherDateExpired;
  public String theTimeGone;
  public String inDate;
  public String editInfo;
  public String currentPassword_;
  public String newPassword_;
  public String confirmPassword_;
  public String titleHello;
  public String titleEnter;
  public String next;
  public String previous;
  public String first;
  public String last;
  public String from;
  public String to;
  public String filter;
  public String logoYear;
  public String logoArabic;
  public String forgetUserName;
  public String forgetPos;
  public String forgetPhone;
  public String forgetEmail;
  public String forgetAll;
  public String insertValue;
  public String emailInvalid1;
  public String emailInvalid2;
  public String emailInvalid3;
  public String invalidValues;
  public String redeemedBy;
  public String redeemedOn;
  public String redeemedGift;
  public String newPassword;
  public String confirmNewPassword;
  public String activationCode;
public String mobileforexample;
  public String getMobileforexample() {
	return mobileforexample;
}

public void setMobileforexample(String mobileforexample) {
	this.mobileforexample = mobileforexample;
}

public ArabicNamesDTO()
  {
  }

  public void setArabic(String arabic)
  {
    this.arabic = arabic;
  }

  public String getArabic()
  {
    return arabic;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setForgetPassword(String forgetPassword)
  {
    this.forgetPassword = forgetPassword;
  }

  public String getForgetPassword()
  {
    return forgetPassword;
  }

  public void setWelcome(String welcome)
  {
    this.welcome = welcome;
  }

  public String getWelcome()
  {
    return welcome;
  }

  public void setSearch(String search)
  {
    this.search = search;
  }

  public String getSearch()
  {
    return search;
  }

  public void setHistory(String history)
  {
    this.history = history;
  }

  public String getHistory()
  {
    return history;
  }

  public void setAccount(String account)
  {
    this.account = account;
  }

  public String getAccount()
  {
    return account;
  }

  public void setExit(String exit)
  {
    this.exit = exit;
  }

  public String getExit()
  {
    return exit;
  }

  public void setVoucherNumber(String voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public String getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setVoucherNumber_(String voucherNumber_)
  {
    this.voucherNumber_ = voucherNumber_;
  }

  public String getVoucherNumber_()
  {
    return voucherNumber_;
  }

  public void setMobileNumber(String mobileNumber)
  {
    this.mobileNumber = mobileNumber;
  }

  public String getMobileNumber()
  {
    return mobileNumber;
  }

  public void setTitleUnredeemed(String titleUnredeemed)
  {
    this.titleUnredeemed = titleUnredeemed;
  }

  public String getTitleUnredeemed()
  {
    return titleUnredeemed;
  }

  public void setTitleRedeemed(String titleRedeemed)
  {
    this.titleRedeemed = titleRedeemed;
  }

  public String getTitleRedeemed()
  {
    return titleRedeemed;
  }

  public void setThisNumber(String thisNumber)
  {
    this.thisNumber = thisNumber;
  }

  public String getThisNumber()
  {
    return thisNumber;
  }

  public void setTakeVoucher(String takeVoucher)
  {
    this.takeVoucher = takeVoucher;
  }

  public String getTakeVoucher()
  {
    return takeVoucher;
  }

  public void setTitleHistory(String titleHistory)
  {
    this.titleHistory = titleHistory;
  }

  public String getTitleHistory()
  {
    return titleHistory;
  }

  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setRedeemtionDate(String redeemtionDate)
  {
    this.redeemtionDate = redeemtionDate;
  }

  public String getRedeemtionDate()
  {
    return redeemtionDate;
  }

  public void setGift(String gift)
  {
    this.gift = gift;
  }

  public String getGift()
  {
    return gift;
  }

  public void setGiftValue(String giftValue)
  {
    this.giftValue = giftValue;
  }

  public String getGiftValue()
  {
    return giftValue;
  }

  public void setGetPassword(String getPassword)
  {
    this.getPassword = getPassword;
  }

  public String getGetPassword()
  {
    return getPassword;
  }

  public void setUserName_(String userName_)
  {
    this.userName_ = userName_;
  }

  public String getUserName_()
  {
    return userName_;
  }

  public void setPhoneNumber_(String phoneNumber_)
  {
    this.phoneNumber_ = phoneNumber_;
  }

  public String getPhoneNumber_()
  {
    return phoneNumber_;
  }

  public void setPosCode_(String posCode_)
  {
    this.posCode_ = posCode_;
  }

  public String getPosCode_()
  {
    return posCode_;
  }

  public void setEmail_(String email_)
  {
    this.email_ = email_;
  }

  public String getEmail_()
  {
    return email_;
  }

  public void setVoucherDateExpired(String voucherDateExpired)
  {
    this.voucherDateExpired = voucherDateExpired;
  }

  public String getVoucherDateExpired()
  {
    return voucherDateExpired;
  }

  public void setTheTimeGone(String theTimeGone)
  {
    this.theTimeGone = theTimeGone;
  }

  public String getTheTimeGone()
  {
    return theTimeGone;
  }

  public void setInDate(String inDate)
  {
    this.inDate = inDate;
  }

  public String getInDate()
  {
    return inDate;
  }

  public void setEditInfo(String editInfo)
  {
    this.editInfo = editInfo;
  }

  public String getEditInfo()
  {
    return editInfo;
  }

  public void setCurrentPassword_(String currentPassword_)
  {
    this.currentPassword_ = currentPassword_;
  }

  public String getCurrentPassword_()
  {
    return currentPassword_;
  }

  public void setNewPassword_(String newPassword_)
  {
    this.newPassword_ = newPassword_;
  }

  public String getNewPassword_()
  {
    return newPassword_;
  }

  public void setConfirmPassword_(String confirmPassword_)
  {
    this.confirmPassword_ = confirmPassword_;
  }

  public String getConfirmPassword_()
  {
    return confirmPassword_;
  }

  public void setTitleHello(String titleHello)
  {
    this.titleHello = titleHello;
  }

  public String getTitleHello()
  {
    return titleHello;
  }

  public void setTitleEnter(String titleEnter)
  {
    this.titleEnter = titleEnter;
  }

  public String getTitleEnter()
  {
    return titleEnter;
  }

  public void setNext(String next)
  {
    this.next = next;
  }

  public String getNext()
  {
    return next;
  }

  public void setPrevious(String previous)
  {
    this.previous = previous;
  }

  public String getPrevious()
  {
    return previous;
  }

  public void setFirst(String first)
  {
    this.first = first;
  }

  public String getFirst()
  {
    return first;
  }

  public void setLast(String last)
  {
    this.last = last;
  }

  public String getLast()
  {
    return last;
  }

  public void setFrom(String from)
  {
    this.from = from;
  }

  public String getFrom()
  {
    return from;
  }

  public void setTo(String to)
  {
    this.to = to;
  }

  public String getTo()
  {
    return to;
  }

  public void setFilter(String filter)
  {
    this.filter = filter;
  }

  public String getFilter()
  {
    return filter;
  }

  public void setLogoYear(String logoYear)
  {
    this.logoYear = logoYear;
  }

  public String getLogoYear()
  {
    return logoYear;
  }

  public void setLogoArabic(String logoArabic)
  {
    this.logoArabic = logoArabic;
  }

  public String getLogoArabic()
  {
    return logoArabic;
  }

  public void setForgetUserName(String forgetUserName)
  {
    this.forgetUserName = forgetUserName;
  }

  public String getForgetUserName()
  {
    return forgetUserName;
  }

  public void setForgetPos(String forgetPos)
  {
    this.forgetPos = forgetPos;
  }

  public String getForgetPos()
  {
    return forgetPos;
  }

  public void setForgetPhone(String forgetPhone)
  {
    this.forgetPhone = forgetPhone;
  }

  public String getForgetPhone()
  {
    return forgetPhone;
  }

  public void setForgetEmail(String forgetEmail)
  {
    this.forgetEmail = forgetEmail;
  }

  public String getForgetEmail()
  {
    return forgetEmail;
  }

  public void setForgetAll(String forgetAll)
  {
    this.forgetAll = forgetAll;
  }

  public String getForgetAll()
  {
    return forgetAll;
  }

  public void setInsertValue(String insertValue)
  {
    this.insertValue = insertValue;
  }

  public String getInsertValue()
  {
    return insertValue;
  }

  public void setEmailInvalid1(String emailInvalid1)
  {
    this.emailInvalid1 = emailInvalid1;
  }

  public String getEmailInvalid1()
  {
    return emailInvalid1;
  }

  public void setEmailInvalid2(String emailInvalid2)
  {
    this.emailInvalid2 = emailInvalid2;
  }

  public String getEmailInvalid2()
  {
    return emailInvalid2;
  }

  public void setEmailInvalid3(String emailInvalid3)
  {
    this.emailInvalid3 = emailInvalid3;
  }

  public String getEmailInvalid3()
  {
    return emailInvalid3;
  }

  public void setInvalidValues(String invalidValues)
  {
    this.invalidValues = invalidValues;
  }

  public String getInvalidValues()
  {
    return invalidValues;
  }

  public void setRedeemedBy(String redeemedBy)
  {
    this.redeemedBy = redeemedBy;
  }

  public String getRedeemedBy()
  {
    return redeemedBy;
  }

  public void setRedeemedOn(String redeemedOn)
  {
    this.redeemedOn = redeemedOn;
  }

  public String getRedeemedOn()
  {
    return redeemedOn;
  }

  public void setRedeemedGift(String redeemedGift)
  {
    this.redeemedGift = redeemedGift;
  }

  public String getRedeemedGift()
  {
    return redeemedGift;
  }


  public void setNewPassword(String newPassword)
  {
    this.newPassword = newPassword;
  }

  public String getNewPassword()
  {
    return newPassword;
  }

  public void setConfirmNewPassword(String confirmNewPassword)
  {
    this.confirmNewPassword = confirmNewPassword;
  }

  public String getConfirmNewPassword()
  {
    return confirmNewPassword;
  }

  public void setActivationCode(String activationCode)
  {
    this.activationCode = activationCode;
  }

  public String getActivationCode()
  {
    return activationCode;
  }
  }
