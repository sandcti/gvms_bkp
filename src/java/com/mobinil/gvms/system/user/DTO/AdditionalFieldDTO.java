package com.mobinil.gvms.system.user.DTO;

public class AdditionalFieldDTO
{

    public Integer fieldId;
    public Integer fieldOrder;
    public Integer campaignId;
    public String fieldDbName;
    public String fieldType;
    public String fieldArabicName;
    public String fieldEnglishName;
    public String fieldIsMandatory;
    public String campaignName;
    public String fieldIsUnique;
    public String fieldIsCombo;
    public String fieldIsText;
    public String fieldIsInputText;


    public AdditionalFieldDTO()
    {
    }

    public void setFieldId(Integer fieldId)
    {
      this.fieldId = fieldId;
    }

    public Integer getFieldId()
    {
      return fieldId;
    }

    public void setFieldOrder(Integer fieldOrder)
    {
      this.fieldOrder = fieldOrder;
    }

    public Integer getFieldOrder()
    {
      return fieldOrder;
    }

    public void setCampaignId(Integer campaignId)
    {
      this.campaignId = campaignId;
    }

    public Integer getCampaignId()
    {
      return campaignId;
    }

    public void setFieldDbName(String fieldDbName)
    {
      this.fieldDbName = fieldDbName;
    }

    public String getFieldDbName()
    {
      return fieldDbName;
    }

    public void setFieldType(String fieldType)
    {
      this.fieldType = fieldType;
    }

    public String getFieldType()
    {
      return fieldType;
    }

    public void setFieldArabicName(String fieldArabicName)
    {
      this.fieldArabicName = fieldArabicName;
    }

    public String getFieldArabicName()
    {
      return fieldArabicName;
    }

    public void setFieldEnglishName(String fieldEnglishName)
    {
      this.fieldEnglishName = fieldEnglishName;
    }

    public String getFieldEnglishName()
    {
      return fieldEnglishName;
    }


    public void setFieldIsMandatory(String fieldIsMandatory)
    {
      this.fieldIsMandatory = fieldIsMandatory;
    }

    public String getFieldIsMandatory()
    {
      return fieldIsMandatory;
    }

    public void setFieldIsUnique(String fieldIsUnique)
    {
      this.fieldIsUnique = fieldIsUnique;
    }

    public String getFieldIsUnique()
    {
      return fieldIsUnique;
    }

    public void setFieldIsCombo(String fieldIsCombo)
    {
      this.fieldIsCombo = fieldIsCombo;
    }

    public String getFieldIsCombo()
    {
      return fieldIsCombo;
    }

    public void setFieldIsText(String fieldIsText)
    {
      this.fieldIsText = fieldIsText;
    }

    public String getFieldIsText()
    {
      return fieldIsText;
    }

    public void setFieldIsInputText(String fieldIsInputText)
    {
      this.fieldIsInputText = fieldIsInputText;
    }

    public String getFieldIsInputText()
    {
      return fieldIsInputText;
    }

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getCampaignName() {
		return campaignName;
	}


    
  }
