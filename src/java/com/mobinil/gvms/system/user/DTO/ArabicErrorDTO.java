package com.mobinil.gvms.system.user.DTO;

public class ArabicErrorDTO {
public String invalidusernameorpass;
public String pleasetypeusername;
public String thisuserlocked;
public String invalidpassword;
public String userexpired;
public String invalidvouchernumber;
public String pleaseinsertvouchernumber;
public String updateinfocomplete;
public String passandconfnotequal;
public String invalidoldpass ;
public String passlessthanlength;
public String passhavetwosimilarchar;
public String passdoenothavenumorchar;
public String passhavetwoseqchar;
public String pleaseinsetvalue;
public String passinlasthistory;
public String followsteps;
public String step1;
public String step2;
public String step3;
public String step4;
public String step5;
public String step6;
public String step7;
public String editinvalidpass;
public String invaliddate;
public String invalidnumber;
public String valuerequired;
public String thisvaluerepeated;
public String invalidtext; 
public String pleaseinsertcusphone;
public String invaliddailwiththisvoucher;
public String pleaseselectonefrommenu;
public String pleaseinsertvalue;
public String redemptionfailed;
public String redemtoncomplete;
public String notauthorized;
public String rightactnumber; 
public String notrightactnumber; 
public String expiredactivnumber; 
public String pleaseinsertpassandcinf; 
public String pleasechangepassduetoinhistory; 
public String updatepasscomplete ;
public String alreadyRedeemed;
public String getAlreadyRedeemed() {
	return alreadyRedeemed;
}
public void setAlreadyRedeemed(String alreadyRedeemed) {
	this.alreadyRedeemed = alreadyRedeemed;
}
public String getInvalidusernameorpass() {
	return invalidusernameorpass;
}
public void setInvalidusernameorpass(String invalidusernameorpass) {
	this.invalidusernameorpass = invalidusernameorpass;
}
public String getPleasetypeusername() {
	return pleasetypeusername;
}
public void setPleasetypeusername(String pleasetypeusername) {
	this.pleasetypeusername = pleasetypeusername;
}
public String getThisuserlocked() {
	return thisuserlocked;
}
public void setThisuserlocked(String thisuserlocked) {
	this.thisuserlocked = thisuserlocked;
}
public String getInvalidpassword() {
	return invalidpassword;
}
public void setInvalidpassword(String invalidpassword) {
	this.invalidpassword = invalidpassword;
}
public String getUserexpired() {
	return userexpired;
}
public void setUserexpired(String userexpired) {
	this.userexpired = userexpired;
}
public String getInvalidvouchernumber() {
	return invalidvouchernumber;
}
public void setInvalidvouchernumber(String invalidvouchernumber) {
	this.invalidvouchernumber = invalidvouchernumber;
}
public String getPleaseinsertvouchernumber() {
	return pleaseinsertvouchernumber;
}
public void setPleaseinsertvouchernumber(String pleaseinsertvouchernumber) {
	this.pleaseinsertvouchernumber = pleaseinsertvouchernumber;
}
public String getUpdateinfocomplete() {
	return updateinfocomplete;
}
public void setUpdateinfocomplete(String updateinfocomplete) {
	this.updateinfocomplete = updateinfocomplete;
}
public String getPassandconfnotequal() {
	return passandconfnotequal;
}
public void setPassandconfnotequal(String passandconfnotequal) {
	this.passandconfnotequal = passandconfnotequal;
}
public String getInvalidoldpass() {
	return invalidoldpass;
}
public void setInvalidoldpass(String invalidoldpass) {
	this.invalidoldpass = invalidoldpass;
}
public String getPasslessthanlength() {
	return passlessthanlength;
}
public void setPasslessthanlength(String passlessthanlength) {
	this.passlessthanlength = passlessthanlength;
}
public String getPasshavetwosimilarchar() {
	return passhavetwosimilarchar;
}
public void setPasshavetwosimilarchar(String passhavetwosimilarchar) {
	this.passhavetwosimilarchar = passhavetwosimilarchar;
}
public String getPassdoenothavenumorchar() {
	return passdoenothavenumorchar;
}
public void setPassdoenothavenumorchar(String passdoenothavenumorchar) {
	this.passdoenothavenumorchar = passdoenothavenumorchar;
}
public String getPasshavetwoseqchar() {
	return passhavetwoseqchar;
}
public void setPasshavetwoseqchar(String passhavetwoseqchar) {
	this.passhavetwoseqchar = passhavetwoseqchar;
}
public String getPleaseinsetvalue() {
	return pleaseinsetvalue;
}
public void setPleaseinsetvalue(String pleaseinsetvalue) {
	this.pleaseinsetvalue = pleaseinsetvalue;
}
public String getPassinlasthistory() {
	return passinlasthistory;
}
public void setPassinlasthistory(String passinlasthistory) {
	this.passinlasthistory = passinlasthistory;
}
public String getFollowsteps() {
	return followsteps;
}
public void setFollowsteps(String followsteps) {
	this.followsteps = followsteps;
}
public String getStep1() {
	return step1;
}
public void setStep1(String step1) {
	this.step1 = step1;
}
public String getStep2() {
	return step2;
}
public void setStep2(String step2) {
	this.step2 = step2;
}
public String getStep3() {
	return step3;
}
public void setStep3(String step3) {
	this.step3 = step3;
}
public String getStep4() {
	return step4;
}
public void setStep4(String step4) {
	this.step4 = step4;
}
public String getStep5() {
	return step5;
}
public void setStep5(String step5) {
	this.step5 = step5;
}
public String getStep6() {
	return step6;
}
public void setStep6(String step6) {
	this.step6 = step6;
}
public String getStep7() {
	return step7;
}
public void setStep7(String step7) {
	this.step7 = step7;
}
public String getEditinvalidpass() {
	return editinvalidpass;
}
public void setEditinvalidpass(String editinvalidpass) {
	this.editinvalidpass = editinvalidpass;
}
public String getInvaliddate() {
	return invaliddate;
}
public void setInvaliddate(String invaliddate) {
	this.invaliddate = invaliddate;
}
public String getInvalidnumber() {
	return invalidnumber;
}
public void setInvalidnumber(String invalidnumber) {
	this.invalidnumber = invalidnumber;
}
public String getValuerequired() {
	return valuerequired;
}
public void setValuerequired(String valuerequired) {
	this.valuerequired = valuerequired;
}
public String getThisvaluerepeated() {
	return thisvaluerepeated;
}
public void setThisvaluerepeated(String thisvaluerepeated) {
	this.thisvaluerepeated = thisvaluerepeated;
}
public String getInvalidtext() {
	return invalidtext;
}
public void setInvalidtext(String invalidtext) {
	this.invalidtext = invalidtext;
}
public String getPleaseinsertcusphone() {
	return pleaseinsertcusphone;
}
public void setPleaseinsertcusphone(String pleaseinsertcusphone) {
	this.pleaseinsertcusphone = pleaseinsertcusphone;
}
public String getInvaliddailwiththisvoucher() {
	return invaliddailwiththisvoucher;
}
public void setInvaliddailwiththisvoucher(String invaliddailwiththisvoucher) {
	this.invaliddailwiththisvoucher = invaliddailwiththisvoucher;
}
public String getPleaseselectonefrommenu() {
	return pleaseselectonefrommenu;
}
public void setPleaseselectonefrommenu(String pleaseselectonefrommenu) {
	this.pleaseselectonefrommenu = pleaseselectonefrommenu;
}
public String getPleaseinsertvalue() {
	return pleaseinsertvalue;
}
public void setPleaseinsertvalue(String pleaseinsertvalue) {
	this.pleaseinsertvalue = pleaseinsertvalue;
}
public String getRedemptionfailed() {
	return redemptionfailed;
}
public void setRedemptionfailed(String redemptionfailed) {
	this.redemptionfailed = redemptionfailed;
}
public String getRedemtoncomplete() {
	return redemtoncomplete;
}
public void setRedemtoncomplete(String redemtoncomplete) {
	this.redemtoncomplete = redemtoncomplete;
}
public String getNotauthorized() {
	return notauthorized;
}
public void setNotauthorized(String notauthorized) {
	this.notauthorized = notauthorized;
}
public String getRightactnumber() {
	return rightactnumber;
}
public void setRightactnumber(String rightactnumber) {
	this.rightactnumber = rightactnumber;
}
public String getNotrightactnumber() {
	return notrightactnumber;
}
public void setNotrightactnumber(String notrightactnumber) {
	this.notrightactnumber = notrightactnumber;
}
public String getExpiredactivnumber() {
	return expiredactivnumber;
}
public void setExpiredactivnumber(String expiredactivnumber) {
	this.expiredactivnumber = expiredactivnumber;
}
public String getPleaseinsertpassandcinf() {
	return pleaseinsertpassandcinf;
}
public void setPleaseinsertpassandcinf(String pleaseinsertpassandcinf) {
	this.pleaseinsertpassandcinf = pleaseinsertpassandcinf;
}
public String getPleasechangepassduetoinhistory() {
	return pleasechangepassduetoinhistory;
}
public void setPleasechangepassduetoinhistory(
		String pleasechangepassduetoinhistory) {
	this.pleasechangepassduetoinhistory = pleasechangepassduetoinhistory;
}
public String getUpdatepasscomplete() {
	return updatepasscomplete;
}
public void setUpdatepasscomplete(String updatepasscomplete) {
	this.updatepasscomplete = updatepasscomplete;
}

}
