package com.mobinil.gvms.system.user.DTO;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

public class DatatableDTO
{
  public String value;
  public String arabicValue;
  public String id;
  public Boolean require;
  public String inputValue;
  public String textValue;
  public String comboValue;
  public Date date;
  public String fieldType;
  public ArrayList<SelectItem> compoList;
  public Boolean renderCombo;
  public Boolean renderTextarea;
  public Boolean renderInputText;
  public Boolean renderInputDate;
  public String giftString;

  public ArrayList<SelectItem> selectItems;

  public DatatableDTO()
  {

  }


  public void setValue(String value)
  {
    this.value = value;
  }

  public String getValue()
  {
    return value;
  }


  public void setRequire(boolean require)
  {
    this.require = require;
  }

  public boolean getRequire()
  {
    return require;
  }

  public void setInputValue(String inputValue)
  {
    this.inputValue = inputValue;
  }

  public String getInputValue()
  {
    return inputValue;
  }

  public boolean isRequire()
  {
    return require;
  }

  public void setFieldType(String fieldType)
  {
    this.fieldType = fieldType;

  }

  public String getFieldType()
  {

    return fieldType;
  }


  public void setRenderCombo(Boolean renderCombo)
  {
    this.renderCombo = renderCombo;
  }

  public Boolean getRenderCombo()
  {
    return renderCombo;
  }

  public void setRenderTextarea(Boolean renderTextarea)
  {
    this.renderTextarea = renderTextarea;
  }

  public Boolean getRenderTextarea()
  {
    return renderTextarea;
  }

  public void setRenderInputText(Boolean renderInputText)
  {
    this.renderInputText = renderInputText;
  }

  public Boolean getRenderInputText()
  {
    return renderInputText;
  }


  public void setComboValue(String comboValue)
  {
    this.comboValue = comboValue;
  }

  public String getComboValue()
  {
    return comboValue;
  }


  public void setSelectItems(ArrayList<SelectItem> selectItems)
  {
    this.selectItems = selectItems;
  }

  public ArrayList<SelectItem> getSelectItems()
  {
    return selectItems;
  }

  public void setTextValue(String textValue)
  {
    this.textValue = textValue;
  }

  public String getTextValue()
  {
    return textValue;
  }

  public void setArabicValue(String arabicValue)
  {
    this.arabicValue = arabicValue;
  }

  public String getArabicValue()
  {
    return arabicValue;
  }

  public void setDate(Date date)
  {
    this.date = date;
  }

  public Date getDate()
  {
    return date;
  }

  public void setRenderInputDate(Boolean renderInputDate)
  {
    this.renderInputDate = renderInputDate;
  }

  public Boolean getRenderInputDate()
  {
    return renderInputDate;
  }

  public void setGiftString(String giftString)
  {
    this.giftString = giftString;
  }

  public String getGiftString()
  {
    return giftString;
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public String getId()
  {
    return id;
  }

  public void setCompoList(ArrayList<SelectItem> compoList)
  {
    this.compoList = compoList;
  }

  public ArrayList<SelectItem> getCompoList()
  {
    return compoList;
  }
  }
