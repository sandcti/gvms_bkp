package com.mobinil.gvms.system.user.DTO;

public class HistoryDTO
{
  public Integer dialNumber;
  public Integer giftValue;
  public String giftName;
  public String voucherDate;

  public HistoryDTO()
  {
  }


  public void setGiftValue(Integer giftValue)
  {
    this.giftValue = giftValue;
  }

  public Integer getGiftValue()
  {
    return giftValue;
  }

  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }


  public void setDialNumber(Integer dialNumber)
  {
    this.dialNumber = dialNumber;
  }

  public Integer getDialNumber()
  {
    return dialNumber;
  }

  public void setVoucherDate(String voucherDate)
  {
    this.voucherDate = voucherDate;
  }

  public String getVoucherDate()
  {
    return voucherDate;
  }
  }
