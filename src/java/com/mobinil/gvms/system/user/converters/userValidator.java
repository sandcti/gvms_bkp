package com.mobinil.gvms.system.user.converters;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


public class userValidator implements Validator
{
  public userValidator()
  {
  }
  
  public void validate(FacesContext context, UIComponent component, Object value)
    throws ValidatorException
  {
    if (value.toString().compareTo("") == 0)
      {
        throw new ValidatorException(new FacesMessage("Value Required."));
      }
  }

}
