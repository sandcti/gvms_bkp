package com.mobinil.gvms.system.user.converters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.mobinil.gvms.system.user.beans.Login;
import com.mobinil.gvms.system.utility.*;


public class UserConverter implements Converter
{
  
  public Object getAsObject(FacesContext context, UIComponent component, String value)
  {

    
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
    //System.out.println(path);

    if (path.compareTo("Ar")==0)
      {
    
    
    

      }

    String filedId = "";
    UIInput textAreaComp = (UIInput) component.findComponent("hiddenArea");
    UIInput textComp = (UIInput) component.findComponent("hiddenText");
    UIInput dateComp = (UIInput) component.findComponent("hiddenDate");
    UIInput menuComp = (UIInput) component.findComponent("hiddenMenu");
    String valueOfTextAreaComp = (String) textAreaComp.getValue();
    String valueOfTextComp = (String) textComp.getValue();
    String valueOfDateComp = (String) dateComp.getValue();
    String valueOfMenuComp = (String) menuComp.getValue();


    //            String requiredMsg = "Value is required.";    
    //    
    //   FacesContext.getCurrentInstance().addMessage("inputTextControlId",new FacesMessage(FacesMessage.SEVERITY_INFO,requiredMsg,null));


    if (valueOfTextAreaComp.compareTo("") != 0)
      {
        filedId = (String) textAreaComp.getValue();
      }
    if (valueOfTextComp.compareTo("") != 0)
      {
        filedId = (String) textComp.getValue();
      }
    if (valueOfDateComp.compareTo("") != 0)
      {
        filedId = (String) dateComp.getValue();
      }
    if (valueOfMenuComp.compareTo("") != 0)
      {
        filedId = (String) menuComp.getValue();
      }


    String SQL = 
      "Select t1.FIELD_IS_UNIQUE,t1.FIELD_IS_MANDATORY,t1.FIELD_TYPE,t1.CAMPAIGN_ID,t1.FIELD_DB_NAME, t2.CAMPAIGN_NAME from GVMS_ADDITIONAL_FIELD t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID = t2.CAMPAIGN_ID  where FIELD_ID=" + 
      filedId;

    String componentTypes = component.getId();

    //System.out.println("componentTypes is " + componentTypes);
    try
      {
       Connection conn = DBConnection.getConnection();
        ResultSet fieldCredential = ExecuteQueries.executeQuery(conn,SQL);
        if (fieldCredential.next())
          {
            String filedType = fieldCredential.getString("FIELD_TYPE");
            String fieldUnique = fieldCredential.getString("FIELD_IS_UNIQUE");
            String campaignId = fieldCredential.getString("CAMPAIGN_ID");
            String campaignName = fieldCredential.getString("CAMPAIGN_NAME");
            String fieldDbName = fieldCredential.getString("FIELD_DB_NAME");
            String fieldMandatory = fieldCredential.getString("FIELD_IS_MANDATORY");
            if (fieldMandatory.compareTo("1") == 0 && value.compareTo("") == 0)
              {
                if (path.compareTo("Ar")==0)
                  {
                    throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getPleaseinsertvalue()));
                  }
                else
                  {
                    throw new ConverterException(new FacesMessage("Please enter a valid value."));

                  }


              }

            if (componentTypes.compareTo("inputTextAreaControlId") == 0)
              {
                //                String ss = (String) component.getAttributes().get("textAreaValue");
                //System.out.println("value in textArea is : " + value);


                value = 
                    value(context, value, filedType, fieldUnique, campaignId, campaignName, fieldDbName);

                //                //System.out.println("value of Area in atrr= " + ss);

              }

            if (componentTypes.compareTo("inputTextControlId") == 0)
              {

                //System.out.println("value in inputText is : " + value);
                value = 
                    value(context, value, filedType, fieldUnique, campaignId, campaignName, fieldDbName);


              }

            if (componentTypes.compareTo("dateControlId") == 0)
              {
                //                String ss = (String) component.getAttributes().get("dateValue");
                value = 
                    value(context, value, filedType, fieldUnique, campaignId, campaignName, fieldDbName);
                //System.out.println("value in date is : " + value);
                //                //System.out.println("value of date in atrr= " + ss);
              }

            if (componentTypes.compareTo("selectMenuControlId") == 0)
              {
                //                String ss = (String) component.getAttributes().get("dateValue");
                if (fieldMandatory.compareTo("1") == 0)
                  {
                    if (value.compareTo("--please Select--") == 0)
                      {
                        if (path.compareTo("Ar")==0)
                          {
                            throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getPleaseselectonefrommenu()));
                          }
                        else
                          {
                            throw new ConverterException(new FacesMessage("Please select one from menu."));

                          }

                      }
                  }

                //                //System.out.println("value of date in atrr= " + ss);
              }

          }
        fieldCredential.getStatement().close();
        fieldCredential.close();
        DBConnection.closeConnections ( conn );
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    //System.out.println("comp type is " + componentTypes);

    return value;
  }

  public String value(FacesContext context, String value, String filedType, String fieldUnique, 
                      String campaignId, String campaignName, String fieldDbName) throws SQLException
  {

    
    String path = context.getExternalContext().getRequestPathInfo();
    
    if (value.compareTo("") == 0)
      {
        value = "0";
        return null;
      }


  
    Connection conn = DBConnection.getConnection();
    if (filedType.compareTo("String") == 0)
      {
        try{
    	if (fieldUnique.compareTo("1") == 0)
          {
    	   
            ResultSet redanantValue = 
              ExecuteQueries.executeQuery(conn,"Select to_char(" + fieldDbName + 
                                          ") fieldname from GVMS_REDEEM_CAMPAIGN_" + campaignId + 
                                          " where "+ 
                                          fieldDbName + "='" + value + "'");
            try
              {
                if (redanantValue.next())
                  {
                    if (path.compareTo("Ar")==0)
                      {DBConnection.closeConnections ( conn );
                        throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getThisvaluerepeated()));
                      }
                    else
                      {DBConnection.closeConnections ( conn );
                        throw new ConverterException(new FacesMessage("This value is dublicated."));
                      }
                  }
                redanantValue.getStatement().close();
                redanantValue.close();
                DBConnection.closeConnections ( conn );
              }
            catch (SQLException e)
              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }
          }
        try
          {

            Integer.parseInt(value);
            //System.out.println("going to show  Invalid Text in String");
            if (path.compareTo("Ar")==0)
              {DBConnection.closeConnections ( conn );
                throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getInvalidtext()));
              }
            else
              {DBConnection.closeConnections ( conn );
                throw new ConverterException(new FacesMessage("Invalid Text."));
              }

          }
        catch (NumberFormatException e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            return value;
          }
      }
    catch (Exception e)
      {
    	
        //System.out.println("going to show  Invalid text in Integer");
        if (path.compareTo("Ar")==0)
          {DBConnection.closeConnections ( conn );
            throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getInvalidtext()));
          }
        else
          {
        	String message = e.getMessage();
        	if (message.contains("dublicated") || message.compareTo(Login.getArabicErrorDTO().getThisvaluerepeated())==0 ) throw new ConverterException(new FacesMessage(message));
        	else {DBConnection.closeConnections ( conn );
        	   throw new ConverterException(new FacesMessage("Invalid Text."));
        	}
          }


      }

      }
    if (filedType.compareTo("Integer") == 0)
      {

        try
          {
        	//System.out.println("value b4 parse is "+value);
            Float.parseFloat(value);
            //System.out.println("value after parse is "+value);
            if (fieldUnique.compareTo("1") == 0)
              {

               
                ResultSet redanantValue = 
                  ExecuteQueries.executeQuery(conn,"Select "+ 
                                              fieldDbName + 
                                              " fieldname from GVMS_REDEEM_CAMPAIGN_" + 
                                              campaignId + " where "+ fieldDbName + " ='" + value+"'");
                try
                  {
                    if (redanantValue.next())
                      {
                        if (path.compareTo("Ar")==0)
                          {DBConnection.closeConnections ( conn );
                            throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getThisvaluerepeated()));
                          }
                        else
                          {DBConnection.closeConnections ( conn );
                            throw new ConverterException(new FacesMessage("This number is dublicated."));
                          }
                      }
                    redanantValue.getStatement().close();
                    redanantValue.close();
                  }
                catch (SQLException e)
                  { new com.mobinil.gvms.system.utility.PrintException().printException(e); DBConnection.closeConnections ( conn );
                    throw new ConverterException(new FacesMessage("Invalid number."));
                  }
              }
            //System.out.println("going to return value in Integer");
            return value;
          }
        catch (Exception e)
          {
        	new com.mobinil.gvms.system.utility.PrintException().printException(e);
            //System.out.println("going to show  Invalid number in Integer");
            if (path.compareTo("Ar")==0)
              {DBConnection.closeConnections ( conn );
                throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getInvalidnumber()));
              }
            else
              {
            	String message = e.getMessage();
            	if (message.contains("dublicated") || message.compareTo(Login.getArabicErrorDTO().getThisvaluerepeated())==0) throw new ConverterException(new FacesMessage(message));
            	else{DBConnection.closeConnections ( conn );
            	   throw new ConverterException(new FacesMessage("Invalid Number."));
            	}
              }


          }
      }
    if (filedType.compareTo("Date") == 0)
      {
        try
          {
            Date.parse(value);
            //System.out.println("Date in converter is "+value);
            if (fieldUnique.compareTo("1") == 0)
            {


              ResultSet redanantValue = 
                ExecuteQueries.executeQuery(conn,"Select " + 
                                            fieldDbName + 
                                            " fieldname from GVMS_REDEEM_CAMPAIGN_" + 
                                            campaignId + " where " + fieldDbName + " >=todate('"+ value+ " 00:00:00', 'MM/DD/YYYY HH24:MI:SS')");
              try
                {
                  if (redanantValue.next())
                    {
                      if (path.compareTo("Ar")==0)
                        {DBConnection.closeConnections ( conn );
                          throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getThisvaluerepeated()));
                        }
                      else
                        {DBConnection.closeConnections ( conn );
                          throw new ConverterException(new FacesMessage("This date is dublicated."));
                        }
                    }
                  redanantValue.getStatement().close();
                  redanantValue.close();
                }
              catch (SQLException e)
                {new com.mobinil.gvms.system.utility.PrintException().printException(e);DBConnection.closeConnections ( conn );
                  throw new ConverterException(new FacesMessage("Invalid date."));
                }
            }
            return value;
          }
        catch (Exception e)
          {new com.mobinil.gvms.system.utility.PrintException().printException(e);
            if (path.compareTo("Ar")==0)
              {DBConnection.closeConnections ( conn );
                throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getInvaliddate()));
              }
            else
              {
            	String message = e.getMessage();
            	if (message.contains("dublicated")|| message.compareTo(Login.getArabicErrorDTO().getThisvaluerepeated())==0) throw new ConverterException(new FacesMessage(message));
            	else{DBConnection.closeConnections ( conn );
            	   throw new ConverterException(new FacesMessage("Invalid Date."));
            	}
              }

          }
      }
    DBConnection.closeConnections ( conn );
    return null;
  }

  public String getAsString(FacesContext context, UIComponent component, Object value)
  {
    //System.out.println(" here in getAsString ");
    //System.out.println("value in getAsString " + ((String) value));
    // Convert the Foo object to its unique String representation.
    return ((String) value);
  }

}
