package com.mobinil.gvms.system.user.converters;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.mobinil.gvms.system.user.beans.Login;
import com.mobinil.gvms.system.utility.*;


public class dialConverter implements Converter
{
 
  public Object getAsObject(FacesContext context, UIComponent component, String value)
  {
    String voucher_Number = "";
    String voucher = "";
    String dial = "";
    
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
      
    //System.out.println(path);
    if (path.compareTo("Ar")==0)
      {
           

        if (value.compareTo("") == 0)
          {

            if (path.compareTo("Ar")==0)
              {
                throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getPleaseinsertcusphone()));
              }
            else
              {
                throw new ConverterException(new FacesMessage("Please insert user dial number"));
              }
          }

      }
    voucher_Number = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("vouNum");
   
    try
      {
       Connection conn = DBConnection.getConnection();
       ResultSet result = 
          ExecuteQueries.executeQuery(conn,"Select CURRENT_VOUCHER_DIAL_NUMBER,CURRENT_VOUCHER_NUMBER from GVMS_CURRENT_VOUCHER where CURRENT_VOUCHER_NUMBER=" + 
                                      voucher_Number);
        if (result.next())
          {

            dial = "" + result.getInt("CURRENT_VOUCHER_DIAL_NUMBER");
            voucher = "" + result.getInt("CURRENT_VOUCHER_NUMBER");

          }
        result.getStatement().close();
        result.close();
        DBConnection.closeConnections ( conn );
        if (dial.compareTo(value) != 0)
          {
            if (path.compareTo("Ar")==0)
              {
                throw new ConverterException(new FacesMessage(Login.getArabicErrorDTO().getInvaliddailwiththisvoucher()));
              }
            else
              {
                throw new ConverterException(new FacesMessage("Error: The voucher number doesn't match dial number."));
              }

          }
        else
          {

            return value;
          }
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    return value;
  }

  public String getAsString(FacesContext context, UIComponent component, Object value)
  {
    //System.out.println(" here in getAsString ");
    //System.out.println("value in getAsString " + value.toString());

    // Convert the Foo object to its unique String representation.
    return value.toString();
  }
}
