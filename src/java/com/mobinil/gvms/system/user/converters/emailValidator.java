package com.mobinil.gvms.system.user.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.mobinil.gvms.system.user.beans.Login;


public class emailValidator implements Converter
{
  
  public Object getAsObject(FacesContext context, UIComponent component, String value)
  {
    String email = (String) component.getAttributes().get("email");
    String path =(String) context.getExternalContext().getSessionMap().get("userTypeInterface");
    Login loginBean = (Login) context.getExternalContext().getSessionMap().get("Login_Bean");

    if (email.compareTo("") != 0)
      {
        //System.out.println("email is " + email);

        if (!value.contains("@"))
          {
            if (path.compareTo("Ar")==0)
              {
                throw new ConverterException(new FacesMessage(loginBean.getNamesArray().get(0).getEmailInvalid1()));
              }
            else
              {
                throw new ConverterException(new FacesMessage("Mail must contain @ character."));
              }

          }
        if (!value.contains("."))
          {
          
            if (path.compareTo("Ar")==0)
              {
                throw new ConverterException(new FacesMessage(loginBean.getNamesArray().get(0).getEmailInvalid2()));
              }
            else
              {
                throw new ConverterException(new FacesMessage("Mail must contain dot character."));
              }

          }
        if (value.contains(" "))
          {
            if (path.compareTo("Ar")==0)
              {
                throw new ConverterException(new FacesMessage(loginBean.getNamesArray().get(0).getEmailInvalid3()));
              }
            else
              {
                throw new ConverterException(new FacesMessage("Mail must not contain spaces."));
              }

          }

      }
    else
      {
        return value;
      }

    //System.out.println("values in final " + value);
    return value;
  }

  public String getAsString(FacesContext context, UIComponent component, Object value)
  {

    return value.toString();
  }
}
