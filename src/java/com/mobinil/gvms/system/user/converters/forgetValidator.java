package com.mobinil.gvms.system.user.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.mobinil.gvms.system.user.beans.Login;
import com.mobinil.gvms.system.utility.jsfUtils;


public class forgetValidator implements Converter
{
  
  public Object getAsObject(FacesContext context, UIComponent component, String value)
  {
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
    //System.out.println("path is " + path);

    Login loginBean = (Login) context.getExternalContext().getSessionMap().get("Login_Bean");

    if (value.compareTo("") == 0)
      {
        if (path.compareTo("Ar")==0)
          {
            throw new ConverterException(new FacesMessage(loginBean.getNamesArray().get(0).getInsertValue()));
          }
        else
          {
            throw new ConverterException(new FacesMessage("Value Is Required."));
          }
      }

    return value;

  }

  public String getAsString(FacesContext context, UIComponent component, Object value)
  {

    return value.toString();
  }
}
