package com.mobinil.gvms.system.user.converters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.mobinil.gvms.system.user.beans.Unredeemed;
import com.mobinil.gvms.system.utility.*;


public class gvmsConverter implements Converter
{
  int count = 0;

  public gvmsConverter()
  {

  }
  Unredeemed unreed = 
    (Unredeemed) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Unredeemed_Bean");

  public Object getAsObject(FacesContext context, UIComponent component, String value)
  {


    Integer aInt;


    ArrayList<String> fieldType = new ArrayList<String>();
    ArrayList<String> fieldUnique = new ArrayList<String>();
    ArrayList<String> fieldName = new ArrayList<String>();
    int indx = unreed.getIncreVar();


    //System.out.println("Index = " + indx);
    if (indx > unreed.getRow().size() - count)
      {
        unreed.setIncreVar(0);
        indx = unreed.getIncreVar();
      }
    count = indx;

    //System.out.println("size of array of fields" + unreed.getRow().size());

    Connection conn = null;
   try
   {
      conn = DBConnection.getConnection();
   }
   catch ( SQLException e1 )
   {
      // TODO Auto-generated catch block
      new com.mobinil.gvms.system.utility.PrintException().printException(e1);
   }
      String path = (String)jsfUtils.getFromSession("userTypeInterface");
    String campaignId = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cid");
    ResultSet rset = 
      ExecuteQueries.executeQuery(conn,"Select FIELD_TYPE,FIELD_IS_UNIQUE,FIELD_DB_NAME from GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID=" + 
                                  campaignId + " ORDER BY FIELD_ORDER");
    ResultSet errorRS = 
      ExecuteQueries.executeQuery(conn,"Select ERROR_DESCRIPTION from GVMS_ERROR where ERROR_PAGE_NAME='SearchArabic'");

    ArrayList<String> errorArray = new ArrayList<String>();

    try
      {
        while (errorRS.next())
          {
            errorArray.add(errorRS.getString("ERROR_DESCRIPTION"));
          }
        errorRS.getStatement().close();
        errorRS.close();
        int counter = 0;
        while (rset.next())
          {
            fieldType.add(rset.getString("FIELD_TYPE"));
            fieldUnique.add(rset.getString("FIELD_IS_UNIQUE"));
            fieldName.add(rset.getString("FIELD_DB_NAME"));
            counter++;
          }
        rset.close();
        if (fieldType.get(count).compareTo("") == 0)
          {
            if (value.compareTo("") == 0)
              {
                unreed.setIncreVar(unreed.getIncreVar() + 1);
                unreed.getDt().get(count).setRequire(false);

                if (path.compareTo("Ar")==0)
                  {
                    throw new ConverterException(new FacesMessage(errorArray.get(4)));
                  }
                else
                  {
                    throw new ConverterException(new FacesMessage("Invalid Text"));
                  }
              }

            else
              {
                unreed.setIncreVar(unreed.getIncreVar() + 1);
                unreed.getDt().get(count).setRequire(false);
                return value;
              }
          }

        if (fieldType.get(count).compareTo("String") == 0)
          {
            if (value.compareTo("") == 0)
              {
                unreed.setIncreVar(unreed.getIncreVar() + 1);
                if (unreed.getDt().get(count).getRequire() == true)
                  {

                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(2)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Value Required"));
                      }
                  }

              }
            if (fieldUnique.get(count).compareTo("1") == 0)
              {

                ResultSet rs = 
                  ExecuteQueries.executeQuery(conn,"Select " + fieldName.get(count) + " from GVMS_REDEEM_CAMPAIGN_" + 
                                              campaignId);
                while (rs.next())
                  {
                    if (rs.getString(fieldName.get(count)).compareTo(value) == 0)
                      {
                        unreed.setIncreVar(unreed.getIncreVar() + 1);

                        if (path.compareTo("Ar")==0)
                          {
                            throw new ConverterException(new FacesMessage(errorArray.get(3)));
                          }
                        else
                          {
                            throw new ConverterException(new FacesMessage("This value is dublicated"));
                          }
                      }

                  }

                rs.getStatement().close();
                rs.close();
                try
                  {
                    aInt = Integer.parseInt(value);

                  }

                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    return value;

                  }


                if (aInt instanceof Integer)
                  {
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(4)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Text"));
                      }
                  }


              }
            else
              {


                try
                  {
                    aInt = Integer.parseInt(value);

                  }

                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    return value;

                  }


                if (aInt instanceof Integer)
                  {
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(4)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Text"));
                      }
                  }


              }
          }

        if (fieldType.get(count).compareTo("Integer") == 0)
          {
            if (value.compareTo("") == 0)
              {
                unreed.setIncreVar(unreed.getIncreVar() + 1);
                if (unreed.getDt().get(count).getRequire() == true)
                  {
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(2)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Value Required"));
                      }
                  }
                else
                  {
                    unreed.getDt().get(count).setRequire(false);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(2)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Value Required"));
                      }
                  }

              }
            if (fieldUnique.get(count).compareTo("1") == 0)
              {

                ResultSet rs = 
                  ExecuteQueries.executeQuery(conn,"Select " + fieldName.get(count) + " from GVMS_REDEEM_CAMPAIGN_" + 
                                              campaignId);
                while (rs.next())
                  {
                    if (rs.getString(fieldName.get(count)).compareTo(value) == 0)
                      {
                        unreed.setIncreVar(unreed.getIncreVar() + 1);
                        if (path.compareTo("Ar")==0)
                          {
                            throw new ConverterException(new FacesMessage(errorArray.get(3)));
                          }
                        else
                          {
                            throw new ConverterException(new FacesMessage("This value is dublicated"));
                          }
                      }
                  }
                rs.getStatement().close();
                rs.close();
                try
                  {
                    aInt = Integer.parseInt(value);
                    if (aInt instanceof Integer)
                      {

                        Integer intVal = Integer.parseInt((String) value);
                        unreed.setIncreVar(unreed.getIncreVar() + 1);
                        return intVal.toString();
                      }


                  }
                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(1)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Number"));
                      }

                  }

              }
            else
              {

                try
                  {
                    aInt = Integer.parseInt(value);
                    if (aInt instanceof Integer)
                      {

                        Integer intVal = Integer.parseInt((String) value);
                        unreed.setIncreVar(unreed.getIncreVar() + 1);
                        return intVal.toString();
                      }


                  }
                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(1)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Number"));
                      }

                  }
              }


          }
        if (fieldType.get(count).compareTo("Date") == 0)
          {
            if (value.compareTo("") == 0)
              {
                unreed.setIncreVar(unreed.getIncreVar() + 1);
                if (unreed.getDt().get(count).getRequire() == true)
                  {
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(2)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Value Required"));
                      }
                  }
                else
                  {
                    unreed.getDt().get(count).setRequire(false);
                  }

              }
            if (fieldUnique.get(count).compareTo("1") == 0)
              {

                ResultSet rs = 
                  ExecuteQueries.executeQuery(conn,"Select " + fieldName.get(count) + " from GVMS_REDEEM_CAMPAIGN_" + 
                                              campaignId);
                while (rs.next())
                  {
                    if (rs.getString(fieldName.get(count)).compareTo(value) == 0)
                      {
                        unreed.setIncreVar(unreed.getIncreVar() + 1);
                        if (path.compareTo("Ar")==0)
                          {
                            throw new ConverterException(new FacesMessage(errorArray.get(3)));
                          }
                        else
                          {
                            throw new ConverterException(new FacesMessage("This value is dublicated"));
                          }
                      }
                  }
                rs.getStatement().close();
                rs.close();
                try
                  {
                    String pDate[] = value.split("/");
                    String day = pDate[0] + "/" + pDate[1] + "/" + pDate[2];
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    //System.out.println(day);
                    return day;
                  }
                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(0)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Date"));
                      }

                  }


              }

            else
              {
                try
                  {
                    String pDate[] = value.split("/");
                    String day = pDate[0] + "/" + pDate[1] + "/" + pDate[2];
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    //System.out.println(day);
                    return day;
                  }
                catch (Exception e)
                  {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                    unreed.setIncreVar(unreed.getIncreVar() + 1);
                    if (path.compareTo("Ar")==0)
                      {
                        throw new ConverterException(new FacesMessage(errorArray.get(0)));
                      }
                    else
                      {
                        throw new ConverterException(new FacesMessage("Invalid Date"));
                      }

                  }
              }


          }


        //System.out.println("MyConverter getAsObject: " + value);
        rset.getStatement().close();
        rset.close();
        DBConnection.closeConnections ( conn );
        return null;
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
        return null;
      }


  }

  public String getAsString(FacesContext context, UIComponent component, Object value)
  {

    //System.out.println("MyConverter getAsString: " + value);
    return (String) value;
  }


}

