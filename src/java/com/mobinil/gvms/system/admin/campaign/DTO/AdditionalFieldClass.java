package com.mobinil.gvms.system.admin.campaign.DTO;
public class AdditionalFieldClass
{
  public String fieldType;
  public String unique;
  public String status;  
  public String mandatory;
  public Integer order;
  public Integer fieldId;
  public String englishName;
public String campiagnName;
  public String fieldControl;

  public Boolean selected;
  public Boolean renderUpdate;
  public Integer listCount;
  public String ArabicName;
public String fieldDBName;
  public String getFieldDBName()
{
   return fieldDBName;
}

public void setFieldDBName(String fieldDBName)
{
   this.fieldDBName = fieldDBName;
}

public AdditionalFieldClass()
  {
  }

  public void setFieldType(String fieldType)
  {
    this.fieldType = fieldType;
  }

  public String getFieldType()
  {
    return fieldType;
  }


  public void setOrder(Integer order)
  {
    this.order = order;
  }

  public Integer getOrder()
  {
    return order;
  }

  public void setEnglishName(String englishName)
  {
    this.englishName = englishName;
  }

  public String getEnglishName()
  {
    return englishName;
  }


  public void setUnique(String unique)
  {
    this.unique = unique;
  }

  public String getUnique()
  {
    return unique;
  }

  public void setMandatory(String mandatory)
  {
    this.mandatory = mandatory;
  }

  public String getMandatory()
  {
    return mandatory;
  }

  public void setFieldId(Integer fieldId)
  {
    this.fieldId = fieldId;
  }

  public Integer getFieldId()
  {
    return fieldId;
  }

  public void setSelected(Boolean selected)
  {
    this.selected = selected;
  }

  public Boolean getSelected()
  {
    return selected;
  }

  public void setRenderUpdate(Boolean renderUpdate)
  {
    this.renderUpdate = renderUpdate;
  }

  public Boolean getRenderUpdate()
  {
    return renderUpdate;
  }

  public void setListCount(Integer listCount)
  {
    this.listCount = listCount;
  }

  public Integer getListCount()
  {
    return listCount;
  }

  public void setFieldControl(String fieldControl)
  {
    this.fieldControl = fieldControl;
  }

  public String getFieldControl()
  {
    return fieldControl;
  }

  public void setArabicName(String arabicName)
  {
    this.ArabicName = arabicName;
  }

  public String getArabicName()
  {
    return ArabicName;
  }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

  public void setCampiagnName(String campiagnName)
  {
    this.campiagnName = campiagnName;
  }

  public String getCampiagnName()
  {
    return campiagnName;
  }
}
