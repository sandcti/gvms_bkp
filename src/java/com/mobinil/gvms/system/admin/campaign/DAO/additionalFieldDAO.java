package com.mobinil.gvms.system.admin.campaign.DAO;

import com.mobinil.gvms.system.admin.campaign.DTO.AdditionalFieldClass;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;

import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

public class additionalFieldDAO
{
  public additionalFieldDAO()
  {
  }
  
  public static ArrayList<AdditionalFieldClass> getFieldsInfos (int campaignId , String orderBy){
  
    ArrayList<AdditionalFieldClass> fieldArray = new ArrayList<AdditionalFieldClass>();
    
    
    String SQL = "SELECT   GAF.FIELD_DB_NAME,GAF.FIELD_ID, GAF.FIELD_STATUS_ID, GAF.FIELD_TYPE, " + 
    "         GAF.FIELD_IS_UNIQUE, GAF.FIELD_IS_MANDATORY, GAF.FIELD_ORDER, " + 
    "         FIELD_ENGLISH_NAME, GAF.FIELD_IS_COMBO, GAF.FIELD_IS_TEXT_AREA, " + 
    "         GAF.FIELD_IS_INPUT_TEXT, GAF.FIELD_ARABIC_NAME,GVL.VALIDATION_LIST_ROW_COUNT,GC.CAMPAIGN_NAME " + 
    "    FROM GVMS_ADDITIONAL_FIELD GAF, GVMS_VALIDATION_LIST GVL , GVMS_CAMPAIGN GC " + 
    "   WHERE GAF.FIELD_ID = GVL.FIELD_ID (+) AND GAF.CAMPAIGN_ID = "+campaignId+" and GAF.CAMPAIGN_ID = GC.CAMPAIGN_ID " + 
    "ORDER BY GAF."+orderBy;
    
    
    try
      {
       Connection conn = DBConnection.getConnection();
       ResultSet fieldRS = ExecuteQueries.executeQuery(conn,SQL);
        while (fieldRS.next())
          {
           int listCount = fieldRS.getInt("VALIDATION_LIST_ROW_COUNT");
           
            if (fieldRS.getInt("FIELD_STATUS_ID") != 3)
              {
                AdditionalFieldClass af = new AdditionalFieldClass();
                af.setRenderUpdate(false);
                if (fieldRS.getInt("FIELD_STATUS_ID")==1)
                {
                    af.setStatus("Active");
                }
                else{af.setStatus("In Active");
                }
                if (fieldRS.getString("FIELD_IS_UNIQUE").compareTo("1") == 0)
                  {
                    af.setUnique("Yes");
                  }
                else
                  {
                    af.setUnique("No");
                  }

                if (fieldRS.getString("FIELD_IS_MANDATORY").compareTo("1") == 0)
                  {
                    af.setMandatory("Yes");
                  }
                else
                  {
                    af.setMandatory("No");
                  }


                if (fieldRS.getString("FIELD_IS_TEXT_AREA").compareTo("1") == 0)
                  {
                    af.setFieldControl("Multi-Line");
                  }
                if (fieldRS.getString("FIELD_IS_INPUT_TEXT").compareTo("1") == 0)
                  {
                    af.setFieldControl("One-Line");
                  }
                if (fieldRS.getString("FIELD_IS_COMBO").compareTo("1") == 0)
                  {
                    af.setFieldControl("List");
                    af.setRenderUpdate(true);
                    af.setListCount(listCount); 
                  }


                af.setEnglishName(fieldRS.getString("FIELD_ENGLISH_NAME"));

                af.setFieldType(fieldRS.getString("FIELD_TYPE"));

                af.setOrder(fieldRS.getInt("FIELD_ORDER"));
                af.setFieldId(fieldRS.getInt("FIELD_ID"));
                
                af.setArabicName(fieldRS.getString("FIELD_ARABIC_NAME"));

               af.setFieldDBName ( fieldRS.getString ( "FIELD_DB_NAME" ) );
                   


               


        
                fieldArray.add(af);
              }
          }
        
        fieldRS.getStatement().close();
        fieldRS.close();
        DBConnection.closeConnections ( conn );
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
      return fieldArray;
  
  }
  
  public static String insertListInfo (int rowCount,boolean validtyValue,Integer fieldId,String userName) throws SQLException{
  
    int validtyVal = 0;
    String currDate = "to_date('" + getDateTime() + "','mm/dd/yyyy HH24:MI:SS')";

    if (validtyValue)
      {
        validtyVal = 1;
      }

    
    String SQL;
     
    Connection conn = DBConnection.getConnection();
    String validListId  = DBConnection.getSequenceNextVal(conn,"GVMS_VALIDATION_INFO_SEQ_ID");
    DBConnection.closeConnections ( conn );
        SQL = 
            "insert into GVMS_VALIDATION_LIST(VALIDATION_LIST_ID,VALIDATION_LIST_ROW_COUNT,VALIDATION_LIST_CREATION_DATE," + 
            "VALIDATION_LIST_LAST_MODIFIED,VALIDATION_LIST_CREATED_BY,VALIDATION_LIST_VALIDITY,FIELD_ID) " + 
            "values    ("+validListId+"," + rowCount + "," + currDate + "," + 
            currDate + ",'" + userName + "','" + validtyVal + "'," + fieldId + " )";
    

    int insertionStatus = ExecuteQueries.executeNoneQuery(SQL);
    if (insertionStatus == 0)
      {       

        return "";
      }
    else
      {
        

        return validListId;
      }
  
  }
  public static void deleteBeforeInsertList (Integer fieldId) {
     
    
    try
      {
       Connection conn = DBConnection.getConnection();
       ResultSet validListIdRS = 
          ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ID from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                      fieldId);
        if (validListIdRS.next())
          {
            int validListId = validListIdRS.getInt("VALIDATION_LIST_ID");
            ExecuteQueries.executeNoneQuery ("delete from GVMS_VALIDATION_LIST_ITEM where VALIDATION_LIST_ID=" + 
                                   validListId);
            ExecuteQueries.executeNoneQuery("delete from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                   fieldId);            
    
          }
        validListIdRS.getStatement().close();
        validListIdRS.close();
        DBConnection.closeConnections ( conn );
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      } 
  
  }
    public static void deleteListInfo (Integer fieldId) throws SQLException{
        ExecuteQueries.executeNoneQuery ( "delete from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                               fieldId); 
    }
  public static void insertList (List<List<String>> list,String validationListId) throws SQLException{
    for (int counter = 0; counter < list.size(); counter++)
      {
        
    	if (!((String)list.get(counter).get(0)).startsWith("﻿")){
        String insertValidationList = "insert into GVMS_VALIDATION_LIST_ITEM(VALIDATION_LIST_ITEM_ID,VALIDATION_LIST_ITEM,VALIDATION_LIST_ID) Values " + 
                                          "(GVMS_VALIDATION_LIST_SEQ_ID.nextval,'"+(String)list.get(counter).get(0)+"'," + validationListId + 
                                          ")";
                                          
                                          
        
         ExecuteQueries.executeNoneQuery(insertValidationList);
    	}
        
      }
  }
  
  public static void updateListRowErrors (ArrayList<Integer> errors,String validationListId) throws SQLException{
    ExecuteQueries.executeNoneQuery("update GVMS_VALIDATION_LIST set VALIDATION_LIST_ROW_COUNT=VALIDATION_LIST_ROW_COUNT-"+errors.size()+" where VALIDATION_LIST_ID= "+validationListId );
  }
  
  public static String getOrder (Connection conn,Integer campaignId) throws SQLException{
	  String orderValue = "";
	  ResultSet orderRS = 
	      ExecuteQueries.executeQuery(conn,"Select max(FIELD_ORDER) as ID from GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID=" + 
	                                  campaignId);
	    //System.out.println("campaign ID is" + campaignId);
	    

	        while (orderRS.next())
	          {
	            Integer tempOrder = orderRS.getInt("ID");


	            orderValue = tempOrder + 1 + "";


	          }
	        orderRS.getStatement().close();
	        orderRS.close();
	      
	    
	    return orderValue;
	  
  }
  
    public static boolean getDublicateField (Connection conn,String fieldName, Integer campaignId) throws SQLException{
        ResultSet orderRS = 
            ExecuteQueries.executeQuery(conn,"Select FIELD_ENGLISH_NAME from GVMS_ADDITIONAL_FIELD where FIELD_ENGLISH_NAME = '"+fieldName+"' and CAMPAIGN_ID=" + 
                                        campaignId);
        
        if (orderRS==null)
        {
        return true;
        }
        else 
        if (orderRS.next())
        {
            if (orderRS.getString(1)==null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return true;
        
        
    
    
    }
  public static String insertField (Connection conn,String control, int mandatoryValue, int unqiueValue, 
          Integer campaignId , String type , String englishInput , HtmlInputText arabicInput ,String fieldStatus,campaignTableClass ctc) throws SQLException{
	  
          if (getDublicateField(conn,englishInput,campaignId))
          {
          
	  String SQL="";
	  String fieldDbName ="";
	  String sq = DBConnection.getSequenceNextVal(conn,"GVMS_ADDITIONAL_FIELD_SEQ_ID");
	  String orderValue = getOrder(conn,campaignId);
	  String fieldDBName = "";
	  if (ctc.getCampaignName ( )!=null)
	  {
	     if (ctc.getCampaignName ( ).contains ( " " ))
	     {
	     fieldDBName = ctc.getCampaignName ( ).replace ( " " , "_" )+"_"+campaignId+"_"+sq;
	     }
	     else
	     {
	        fieldDBName = ctc.getCampaignName ( )+"_"+campaignId+"_"+sq;
	     }
	  }
  	   
      SQL = 
          "insert into GVMS_ADDITIONAL_FIELD(FIELD_ID,FIELD_STATUS_ID,FIELD_DB_NAME,FIELD_TYPE,FIELD_IS_UNIQUE," + 
          "FIELD_IS_MANDATORY,FIELD_ORDER,FIELD_ARABIC_NAME,FIELD_ENGLISH_NAME,CAMPAIGN_ID,FIELD_IS_COMBO," + 
          "FIELD_IS_TEXT_AREA,FIELD_IS_INPUT_TEXT) values" + 
          "("+sq+"," + fieldStatus + 
          ",'"+fieldDBName+"','" + type + "','" + unqiueValue + "'," + "'" + 
          mandatoryValue + "'," + orderValue + "," + "?,'" + englishInput + 
          "'," + campaignId + "," + control + ")";


      boolean insertField = 
        ExecuteQueries.executeNoneQueryPreparedOracle(conn,SQL,  ((String)arabicInput.getValue()).trim());

      //System.out.println("status of insertField " + insertField);
      if (insertField == false)
        {    	  
                  String campaignName = ctc.getCampaignName(); 
         
                  if (campaignName.contains(" "))
                    {
                      campaignName = campaignName.replace(" ", "_");
                    }

                  fieldDbName = 
                      campaignName + "_" + campaignId + "_" + sq;


                          
          
          if (type.compareTo("Integer") == 0)
            {
              type = "INTEGER";
            }

          if (type.compareTo("String") == 0)
            {
              type = "VARCHAR2(50)";
            }

          if (type.compareTo("Date") == 0)
            {
              type = "DATE";
            }
          
          SQL = 
              "ALTER TABLE GVMS_REDEEM_CAMPAIGN_" + campaignId + " ADD " + fieldDbName + " " + type;

          //System.out.println(SQL);

          boolean alter = ExecuteQueries.execute(conn,SQL);
          if (alter == false)
            {
              //System.out.println("Table Altered");
                AdditionalFieldClass dd= new AdditionalFieldClass();
                dd.setFieldId(Integer.parseInt(sq));
              jsfUtils.putInSession(dd,"fieldInfo");
              return "One field inserted";
            }
          else
            {
        	  ExecuteQueries.executeNoneQuery("delete from GVMS_ADDITIONAL_FIELD where FIELD_ID = "+sq);        	  
              //System.out.println("Table not Altered");
              return"Faild to add field";
            }


        }
      else
        {
    	  return"Faild to add field";
          
        }
        }
        else return"This field is dublicated in this campaign.";
        
        
	  
  }
  
  public static ArrayList<SelectItem> getFieldInOrder (Connection conn,ArrayList<SelectItem> fieldName, Integer campaignId) throws SQLException{
	  ResultSet fieldNameRS = 
	      ExecuteQueries.executeQuery(conn,"Select FIELD_ENGLISH_NAME,FIELD_ORDER  FROM GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID=" + 
	                                  campaignId + "and FIELD_STATUS_ID =1 ");
	   
	        while (fieldNameRS.next())
	          {
	            if (fieldNameRS.getString("FIELD_ENGLISH_NAME").equals(null))
	              {
	                fieldName.add(new SelectItem("Empty"));
	              }
	            else
	              {
	                fieldName.add(new SelectItem(fieldNameRS.getInt("FIELD_ORDER") + "", 
	                                             fieldNameRS.getString("FIELD_ENGLISH_NAME")));
	              }


	          }
	        fieldNameRS.getStatement().close();
	        fieldNameRS.close();
	   return fieldName;
  }
  
  public static boolean updateField (Connection conn,ArrayList<String>fieldInfo , String newOrderforCurrentField , Integer newOrderforsecondField, String fieldsControl,int  replaceFieldId, Integer fieldId) throws SQLException{
	  boolean update = false;
	  String SQL = 
	      "update GVMS_ADDITIONAL_FIELD set FIELD_ARABIC_NAME=? ,FIELD_ENGLISH_NAME='" + fieldInfo.get(0).trim() + 
	      "',FIELD_STATUS_ID='"+fieldInfo.get(5)+"',FIELD_ORDER=" + newOrderforCurrentField + fieldsControl + " ,FIELD_TYPE='" + fieldInfo.get(1) + 
	      "' ,FIELD_IS_UNIQUE='" + fieldInfo.get(2) + "' ,FIELD_IS_MANDATORY='" + fieldInfo.get(3) + 
	      "' where FIELD_ID =" + fieldId;
//	    //System.out.println(SQL);
	  
	     update = ExecuteQueries.executeNoneQueryPreparedOracle(conn,SQL, fieldInfo.get(4).trim());
	    
	    if (!update)
	    	{SQL = 
	        "update GVMS_ADDITIONAL_FIELD set FIELD_ORDER=" + newOrderforsecondField + " where FIELD_ID=" + replaceFieldId;
	    int replaceUpdate = ExecuteQueries.executeNoneQuery(SQL);
	    if (replaceUpdate!=1)update = true;
	    	}
	    
	    return update;
	  
  }
  
  public static String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }
//  public static void main(String[] args) {
//      try {
//          //System.out.println(getDublicateField("SS",81));
//      }
//      catch (SQLException e) {
//          
//      }
//  }
  
}
