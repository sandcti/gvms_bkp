package com.mobinil.gvms.system.admin.campaign.DTO;

public class VoucherClass
{
  public String dial;
  public Long voucherNumber;
  public String voucherStatus;

  public VoucherClass()
  {
  }

  public void setDial(String dial)
  {
    this.dial = dial;
  }

  public String getDial()
  {
    return dial;
  }

  public void setVoucherNumber(Long voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public Long getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setVoucherStatus(String voucherStatus)
  {
    this.voucherStatus = voucherStatus;
  }

  public String getVoucherStatus()
  {
    return voucherStatus;
  }
}
