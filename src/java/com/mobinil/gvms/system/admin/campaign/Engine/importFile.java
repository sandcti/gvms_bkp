package com.mobinil.gvms.system.admin.campaign.Engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.mobinil.gvms.system.utility.CsvUtil;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.FileLogger;
import com.mobinil.gvms.system.utility.FileUtil;
import com.mobinil.gvms.system.utility.jsfUtils;


public class importFile
{
  public importFile()
  {
  }
  
  public static void insertListDial(InputStream aFile, String validationType, 
                                   int fieldLength,Integer camp_id, Integer gift_id,Integer giftTypeId)
  {
    //...checks on aFile are elided
     FileInputStream fis = null;
     PreparedStatement ps = null;
     Connection conn = null;
     int insertValue = 0;
     boolean fileInsertStatus = false;
     int errorCounter = 0;
     int counter1 = 0;    
     List<List<String>> csvList = new ArrayList<List<String>>();
     DateTime dt = new DateTime();
     DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
     String campaignName="";
     
     String SQL = 
       "insert into GVMS_current_voucher(current_voucher_ID,campaign_id,voucher_status_id,gift_id,CURRENT_VOUCHER_END_DATE,current_voucher_number,current_voucher_dial_number,gift_type_id,VOUCHER_EXPIRY_DATE) values (GVMS_CURRENT_VOUCHER_SEQ_ID.nextval,"+camp_id+",2,"+gift_id+",sysdate,?,?,"+giftTypeId+",(SYSDATE + (nvl( (select VOUCHER_EXPIRE_DAYS from GVMS_CAMPAIGN  where CAMPAIGN_ID='"+camp_id+"') ,"+jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" )+"))) )";
    try
    {
      conn = DBConnection.getConnectionForThread ( );
      ps = conn.prepareStatement(SQL);
      
      ResultSet campaigNameRS = 
        ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME FROM GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                    camp_id);
      
          if (campaigNameRS.next())
            {

              campaignName = campaigNameRS.getString("CAMPAIGN_NAME");

            }
          campaigNameRS.getStatement().close();
          campaigNameRS.close();
      
    }
    catch (SQLException e)
    {
      new com.mobinil.gvms.system.utility.PrintException().printException(e);
    }
    String filename = campaignName + "-" + dt.toString(fmt);
    Vector errorLines = new Vector();
    try
    {
      StringBuilder contents = new StringBuilder();

      //use buffering, reading one line at a time
      //FileReader always assumes default encoding is OK!
      BufferedReader input = new BufferedReader(new InputStreamReader(aFile));
      try
      {
        String line = null; //not declared within while loop
        String oldLine = ""; //not declared within while loop
        /*
          * readLine is a bit quirky :
          * it returns the content of a line MINUS the newline.
          * it returns null only for the END of the stream.
          * it returns an empty String if two newlines appear in a row.
          */
        Float counter = 0F;
        while ((line = input.readLine()) != null)
        {
           if (counter >0)
           {
           
          String[] arrray = line.split(",");
          ArrayList<Boolean> flags = new ArrayList<Boolean>();
          for (int k = 0; k < arrray.length; k++)
          {   
            line = arrray[k];
            if (line == null)
            {
              errorLines.add("Error in Line " + (counter + 1) + 
                             " segment " + (k + 1) + " is empty.");
                                       
              flags.add(false);
            }
            if (line != null)
            {
              if (line.startsWith(" "))
              {
                line = line.trim();
              }

              if (line.compareTo("") == 0)
              {
                
                errorLines.add("Error in Line " + (counter + 1) + 
                               "segment " + (k + 1) + "  is empty.");
                flags.add(false);
              }
              else
              {
                if (line.length() > fieldLength)
                {
                  errorLines.add("Error in Line " + (counter + 1) + 
                                 " segment " + (k + 1) + "  is exceeds " + 
                                 fieldLength + " charachers.");
                  flags.add(false);
                }
                else
                {
                  if (validationType.compareTo("Integer") == 0)
                  {
                    try
                    {
                      Float ff = Float.parseFloat(line);
                      //write code here
                      flags.add(true);

                    }
                    catch (Exception ex)
                    {new com.mobinil.gvms.system.utility.PrintException().printException(ex);
                      errorLines.add("Error in Line " + (counter + 1) + 
                                     " segment " + (k + 1) + 
                                     "  is not number or more than 39 charachers.");
                      flags.add(false);
                    }
                  }
                  if (validationType.compareTo("String") == 0)
                  {
                    if (fieldLength > 39 && line.length() > 39)
                    flags.add(false);
                    else
                    flags.add(true);
                  }


                }
              }
            }

            //          line = "";
          }
             
          if (!flags.contains(false))
          {
          
          if (counter <= 1000000){

//            for (int o = 0; o < arrray.length; o++)
//            {
//              oldLine += arrray[o] + ",";
//            }
//            if (oldLine != null)
//              oldLine = oldLine.substring(0, (oldLine.length() - 1));


           
           try
             {
               
          
          
               
                if (counter1 == 0)
                     {
                       ExecuteQueries.executeNoneQuery("update GVMS_CAMPAIGN set CAMPAIGN_FILE='',CAMPAIGN_ERROR_FILE=0 where campaign_ID=" + 
                                                       camp_id);
                       csvList.add(Arrays.asList(new String[]
                             { "Voucher number", "Error message" }));
                     }
                   
                   
                   ps.setLong(1, Long.valueOf (arrray[0]));
                   ps.setInt(2, counter1);
          
                   try
                     {
                       insertValue = ps.executeUpdate();
                     }
                   catch (Exception e)
                     {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                       //System.out.println(e.getMessage());                
                       errorCounter++;
                       csvList.add(Arrays.asList(new String[]
                             { arrray[0] + ",", 
                               "voucher number repeated in this campaign" }));
                     }
                   
                 
             }
           catch (SQLException e)
             {new com.mobinil.gvms.system.utility.PrintException().printException(e);
          
             }
           
          
          




            //insert here 

            oldLine = "";
            //              //System.out.println("Line number " + counter +
            //                                 " Line is " + line);
            counter1++;
          }
          else
          {
            errorLines.add("Error in Line " + (counter + 1) + 
                           " is more than 1000000.");
          }
          }
        }
          counter++;
        }
        if (errorCounter != 0 || errorLines.size()!=0 )
          {
          
        
//                  filename = "/home/sand/jboss-4.2.3.GA/jboss-4.2.3.GA/server/VoucherError/" + filename + ".csv";
            for (int g =0  ;g<errorLines.size() ; g++) 
            {
              csvList.add(Arrays.asList(new String[]
                    { ((String)errorLines.get(g)) + ",", 
                      " " }));
            }
            
            
          
          
          
        
                    filename = jsfUtils.getParamterFromIni("defualtPath")+"VoucherError/" + filename + ".csv";
            InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
            
            try
              {
                FileUtil.write(new File(filename), csvInput);
        
        
                conn.setAutoCommit(false);
        
                String INSERT_TEXT = 
                  "Update GVMS_CAMPAIGN set CAMPAIGN_ERROR_FILE=1,CAMPAIGN_FILE=? where campaign_Id=" + 
                  camp_id;
                File file = new File(filename);
        
                fis = new FileInputStream(file);
                ps = conn.prepareStatement(INSERT_TEXT);
                ps.setBinaryStream(1, fis, (int) file.length());
                ps.executeUpdate();
                conn.commit();
                fileInsertStatus = true;
        
        
                
              }
            catch (Exception e)
              {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                FileLogger.logString("Error in insert invalid voucher number in runnable class");
              }
          }


      }
      finally
      {
        input.close();
        DBConnection.closeThreadConnections ( conn );
      }
    }
    catch (Exception ex)
    {
      new com.mobinil.gvms.system.utility.PrintException().printException(ex);
    }

    
  }
  
  
  public static void insertListVoucherDial(InputStream aFile, String validationType, 
                                   int fieldLength,Integer camp_id, Integer gift_id,Integer giftTypeId)
  {
    //...checks on aFile are elided
     FileInputStream fis = null;
     PreparedStatement ps = null;
     Connection conn = null;
     int insertValue = 0;
     boolean fileInsertStatus = false;
     int errorCounter = 0;
     int counter1=0;    
     List<List<String>> csvList = new ArrayList<List<String>>();
     DateTime dt = new DateTime();
     DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
     String campaignName="";
     
     String SQL = 
       "insert into GVMS_current_voucher(current_voucher_ID,campaign_id,voucher_status_id,gift_id,CURRENT_VOUCHER_END_DATE,current_voucher_number,current_voucher_dial_number,gift_type_id,VOUCHER_EXPIRY_DATE) values (GVMS_CURRENT_VOUCHER_SEQ_ID.nextval,"+camp_id+",2,"+gift_id+",sysdate,?,?,"+giftTypeId+",(SYSDATE + (nvl( (select VOUCHER_EXPIRE_DAYS from GVMS_CAMPAIGN  where CAMPAIGN_ID='"+camp_id+"') ,"+jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" )+"))))";
    try
    {
      conn = DBConnection.getConnectionForThread ( );
      ps = conn.prepareStatement(SQL);
      
      ResultSet campaigNameRS = 
        ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME FROM GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                    camp_id);
      
          if (campaigNameRS.next())
            {

              campaignName = campaigNameRS.getString("CAMPAIGN_NAME");

            }
          campaigNameRS.getStatement().close();
          campaigNameRS.close();
      
    }
    catch (SQLException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      
    }
    String filename = campaignName + "-" + dt.toString(fmt);
    Vector errorLines = new Vector();
    try
    {
      StringBuilder contents = new StringBuilder();

      //use buffering, reading one line at a time
      //FileReader always assumes default encoding is OK!
      BufferedReader input = new BufferedReader(new InputStreamReader(aFile));
      try
      {
        String line = null; //not declared within while loop
        String oldLine = ""; //not declared within while loop
        /*
          * readLine is a bit quirky :
          * it returns the content of a line MINUS the newline.
          * it returns null only for the END of the stream.
          * it returns an empty String if two newlines appear in a row.
          */
        Float counter = 0F;
        while ((line = input.readLine()) != null)
        {
          if (counter>0)
          {
          String[] arrray = line.split(",");
          ArrayList<Boolean> flags = new ArrayList<Boolean>();
          for (int k = 0; k < arrray.length; k++)
          {   
            line = arrray[k];
            if (line == null)
            {
              errorLines.add("Error in Line " + (counter + 1) + 
                             " segment " + (k + 1) + " is empty.");
                                       
              flags.add(false);
            }
            if (line != null)
            {
              if (line.startsWith(" "))
              {
                line = line.trim();
              }

              if (line.compareTo("") == 0)
              {
                
                errorLines.add("Error in Line " + (counter + 1) + 
                               "segment " + (k + 1) + "  is empty.");
                flags.add(false);
              }
              else
              {
                if (line.length() > fieldLength)
                {
                  errorLines.add("Error in Line " + (counter + 1) + 
                                 " segment " + (k + 1) + "  is exceeds " + 
                                 fieldLength + " charachers.");
                  flags.add(false);
                }
                else
                {
                  if (validationType.compareTo("Integer") == 0)
                  {
                    try
                    {
                      Float ff = Float.parseFloat(line);
                      //write code here
                      flags.add(true);

                    }
                    catch (Exception ex)
                    {new com.mobinil.gvms.system.utility.PrintException().printException(ex);
                      errorLines.add("Error in Line " + (counter + 1) + 
                                     " segment " + (k + 1) + 
                                     "  is not number or more than 39 charachers.");
                      flags.add(false);
                    }
                  }
                  if (validationType.compareTo("String") == 0)
                  {
                    if (fieldLength > 39 && line.length() > 39)
                    flags.add(false);
                    else
                    flags.add(true);
                  }


                }
              }
            }

            //          line = "";
          }
             
          if (!flags.contains(false))
          {
          
          if (counter <= 1000000){

  //            for (int o = 0; o < arrray.length; o++)
  //            {
  //              oldLine += arrray[o] + ",";
  //            }
  //            if (oldLine != null)
  //              oldLine = oldLine.substring(0, (oldLine.length() - 1));


           
           try
             {
               
          
          
               
               if (counter1 == 0)
                 {
                   ExecuteQueries.executeNoneQuery("update GVMS_CAMPAIGN set CAMPAIGN_FILE='',CAMPAIGN_ERROR_FILE=0 where campaign_ID=" + 
                                                   camp_id);
                   csvList.add(Arrays.asList(new String[]
                         { "Dial number", "Voucher number", "Error message" }));
                 }

               
               try
                 {
                       ps.setString(1, arrray[0]);
                       ps.setString(2, arrray[1]);
                       try
                         {
                           insertValue = ps.executeUpdate();
                         }
                       catch (Exception e)
                         {
                         new com.mobinil.gvms.system.utility.PrintException().printException(e);
                           //System.out.println(e.getMessage());

                           String errorMessage = e.getMessage();
                           if (errorMessage.contains("GVMS_CURRENT_VOUCHER_U01"))
                             {
                               FileLogger.logString("--------------Error Voucher Number is "+arrray[0]+"-----------------");
                               errorCounter++;


                               csvList.add(Arrays.asList(new String[]
                                     { arrray[0] + ",", 
                                       arrray[1] + "", 
                                       "voucher number repeated in this campaign" }));
                             }
                           if (errorMessage.contains("GVMS_UNIQUEDIAL"))
                             {
                               FileLogger.logString("--------------Error Dial Number is "+arrray[1]+"-----------------");
                               errorCounter++;
                               csvList.add(Arrays.asList(new String[]
                                     { arrray[0] + ",", 
                                       arrray[1] + "", 
                                       "dial number repeated in this campaign" }));
                             }
                         }             

                 }
               catch (SQLException e)
                 {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
                 }
                   
                 
             }
           catch (Exception e)
             {
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
             }
           
          
          




            //insert here 

            oldLine = "";
            //              //System.out.println("Line number " + counter +
            //                                 " Line is " + line);
            counter1++;
          }
          else
          {
            errorLines.add("Error in Line " + (counter + 1) + 
                           " is more than 1000000.");
          }
          }
        }
          counter++;
        }
        
        if (errorCounter != 0 || errorLines.size()!=0 )
          {
          
          
            for (int g =0  ;g<errorLines.size() ; g++) 
            {
              csvList.add(Arrays.asList(new String[]
                    { ((String)errorLines.get(g)) + ",", 
                       " ", 
                      " " }));
            }
        
                    filename = jsfUtils.getParamterFromIni("defualtPath")+"VoucherError/" + filename + ".csv";

            InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
            try
              {
                FileUtil.write(new File(filename), csvInput);
                conn.setAutoCommit(false);

                String INSERT_TEXT = 
                  "Update GVMS_CAMPAIGN set CAMPAIGN_ERROR_FILE=1,CAMPAIGN_FILE=? where campaign_Id=" + 
                  camp_id;
                File file = new File(filename);

                fis = new FileInputStream(file);
                ps = conn.prepareStatement(INSERT_TEXT);
                ps.setBinaryStream(1, fis, (int) file.length());
                ps.executeUpdate();

                conn.commit();
                fileInsertStatus = true;
                
              }
            catch (Exception e)
              {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                FileLogger.logString("Error in inserting invalid voucher number in runnable class");
              }

          }
        
        


      }
      finally
      {
        input.close();
        DBConnection.closeThreadConnections  ( conn );
      }
    }
    catch (Exception ex)
    {new com.mobinil.gvms.system.utility.PrintException().printException(ex);
//      ex.printStackTrace();
    }

    
  }
  
  
  public static void insertListDialGenerate(InputStream aFile, String validationType, 
                                   int fieldLength,Integer camp_id, Integer gift_id,Integer giftTypeid)
  {
    //...checks on aFile are elided
     FileInputStream fis = null;
     PreparedStatement ps = null;
     Connection conn = null;
     int insertValue = 0;
     boolean fileInsertStatus = false;
     int errorCounter = 0;
     int counter1=0;    
     List<List<String>> csvList = new ArrayList<List<String>>();
     DateTime dt = new DateTime();
     DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
     String campaignName="";
     
    String SQL = 
      "insert into GVMS_current_voucher(current_voucher_ID,campaign_id,voucher_status_id,gift_id,CURRENT_VOUCHER_END_DATE,current_voucher_dial_number, current_voucher_number,GIFT_TYPE_ID,VOUCHER_EXPIRY_DATE) values (GVMS_CURRENT_VOUCHER_SEQ_ID.nextval," + 
      camp_id + ",2," + gift_id + ",sysdate,?,round(dbms_random.value(10000000, 99999999)),(select GIFT_TYPE_ID from GVMS_GIFT where gift_id="+gift_id+"),(SYSDATE + (nvl( (select VOUCHER_EXPIRE_DAYS from GVMS_CAMPAIGN  where CAMPAIGN_ID='"+camp_id+"') ,"+jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" )+"))))";
    //System.out.println ("Gift isssssssssss "+gift_id);
    
    try
      {
        conn = DBConnection.getConnectionForThread ( );
        ps = conn.prepareStatement(SQL);
      
      
      ResultSet campaigNameRS = 
        ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME FROM GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                    camp_id);
      
          if (campaigNameRS.next())
            {

              campaignName = campaigNameRS.getString("CAMPAIGN_NAME");

            }
          campaigNameRS.getStatement().close();
          campaigNameRS.close();
      
    }
    catch (SQLException e)
    {
      new com.mobinil.gvms.system.utility.PrintException().printException(e);
    }
    String filename = campaignName + "-" + dt.toString(fmt);
    Vector errorLines = new Vector();
    try
    {
      StringBuilder contents = new StringBuilder();

      //use buffering, reading one line at a time
      //FileReader always assumes default encoding is OK!
      BufferedReader input = new BufferedReader(new InputStreamReader(aFile));
      try
      {
        String line = null; //not declared within while loop
        String oldLine = ""; //not declared within while loop
        /*
          * readLine is a bit quirky :
          * it returns the content of a line MINUS the newline.
          * it returns null only for the END of the stream.
          * it returns an empty String if two newlines appear in a row.
          */
        Float counter = 0F;
        while ((line = input.readLine()) != null)
        {
          if ( counter >0)
          {
          String[] arrray = line.split(",");
          ArrayList<Boolean> flags = new ArrayList<Boolean>();
          for (int k = 0; k < arrray.length; k++)
          {   
            line = arrray[k];
            if (line == null)
            {
              errorLines.add("Error in Line " + (counter + 1) + 
                             " segment " + (k + 1) + " is empty.");
                                       
              flags.add(false);
            }
            if (line != null)
            {
              if (line.startsWith(" "))
              {
                line = line.trim();
              }

              if (line.compareTo("") == 0)
              {
                
                errorLines.add("Error in Line " + (counter + 1) + 
                               "segment " + (k + 1) + "  is empty.");
                flags.add(false);
              }
              else
              {
                if (line.length() > fieldLength)
                {
                  errorLines.add("Error in Line " + (counter + 1) + 
                                 " segment " + (k + 1) + "  is exceeds " + 
                                 fieldLength + " charachers.");
                  flags.add(false);
                }
                else
                {
                  if (validationType.compareTo("Integer") == 0)
                  {
                    try
                    {
                      Float ff = Float.parseFloat(line);
                      //write code here
                      flags.add(true);

                    }
                    catch (Exception ex)
                    {new com.mobinil.gvms.system.utility.PrintException().printException(ex);
                      errorLines.add("Error in Line " + (counter + 1) + 
                                     " segment " + (k + 1) + 
                                     "  is not number or more than 39 charachers.");
                      flags.add(false);
                    }
                  }
                  if (validationType.compareTo("String") == 0)
                  {
                    if (fieldLength > 39 && line.length() > 39)
                    flags.add(false);
                    else
                    flags.add(true);
                  }


                }
              }
            }

            //          line = "";
          }
             
          if (!flags.contains(false))
          {
          
          if (counter <= 1000000){

  //            for (int o = 0; o < arrray.length; o++)
  //            {
  //              oldLine += arrray[o] + ",";
  //            }
  //            if (oldLine != null)
  //              oldLine = oldLine.substring(0, (oldLine.length() - 1));


           
           try
             {
               
          
          
               
               if (counter1 == 0)
                 {
                   ExecuteQueries.executeNoneQuery("update GVMS_CAMPAIGN set CAMPAIGN_FILE='',CAMPAIGN_ERROR_FILE=0 where campaign_ID=" + 
                                                   camp_id);
                   csvList.add(Arrays.asList(new String[]
                         { "Dial number", "Error message" }));
                 }

               
               try
                 {
                   ps.setString(1, arrray[0]);

                   try
                     {
                       insertValue = ps.executeUpdate();
                     }
                   catch (Exception e)
                     {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                       //System.out.println(e.getMessage());

                       String errorMessage = e.getMessage();
                       if (errorMessage.contains("GVMS_CURRENT_VOUCHER_U01"))
                         {
                           FileLogger.logString("------------------------------------");
                           counter = counter - 1;
                         }
                       if (errorMessage.contains("GVMS_UNIQUEDIAL"))
                         {
                           errorCounter++;
                           csvList.add(Arrays.asList(new String[]
                                 { arrray[0] + ",", 
                                   "dial number repeated in this campaign" }));
                         }
                     }            

                 }
               catch (SQLException e)
                 {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
                 }
                   
                 
             }
           catch (Exception e)
             {
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
             }
           
          
          




            //insert here 

            oldLine = "";
            //              //System.out.println("Line number " + counter +
            //                                 " Line is " + line);
            counter1++;
          }
          else
          {
            errorLines.add("Error in Line " + (counter + 1) + 
                           " is more than 1000000.");
          }
          }
        }
          counter++;
        }
        
        if (errorCounter != 0 || errorLines.size()!=0 )
          {
          
          
            for (int g =0  ;g<errorLines.size() ; g++) 
            {
              csvList.add(Arrays.asList(new String[]
                    { ((String)errorLines.get(g)) + ",", 
                      " " }));
            }
         
                     filename = jsfUtils.getParamterFromIni("defualtPath")+"VoucherError/" + filename + ".csv";

                 InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
                 try
                   {
                     FileUtil.write(new File(filename), csvInput);
                     conn.setAutoCommit(false);

                     String INSERT_TEXT = 
                       "Update GVMS_CAMPAIGN set CAMPAIGN_ERROR_FILE=1,CAMPAIGN_FILE=? where campaign_Id=" + 
                       camp_id;
                     File file = new File(filename);

                     fis = new FileInputStream(file);
                     ps = conn.prepareStatement(INSERT_TEXT);
                     ps.setBinaryStream(1, fis, (int) file.length());
                     ps.executeUpdate();

                     conn.commit();
                     fileInsertStatus = true;
                     
                   }
                 catch (Exception e)
                   {new com.mobinil.gvms.system.utility.PrintException().printException(e);
                     FileLogger.logString("Error in inserting invalid voucher number in runnable class");
                   }

          }
        
        


      }
      finally
      {
        input.close();
        DBConnection.closeThreadConnections  ( conn );
      }
    }
    catch (Exception ex)
    {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);    }

    
  }
  
  public static void main(String[] args)
{
//   //System.out.println (Integer.valueOf ( "23445554324" ));
   //System.out.println (Long.valueOf ( "23445554324" ));
}
}
