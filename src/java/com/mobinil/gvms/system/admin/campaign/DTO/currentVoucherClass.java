package com.mobinil.gvms.system.admin.campaign.DTO;

import java.util.Date;

public class currentVoucherClass
{

  public Integer dialNumber;
  public Integer voucherNumber;
  public Date redeemedDate;
  public String status;
  public String redeemedBy;

  public currentVoucherClass()
  {


  }

  public void setDialNumber(Integer dialNumber)
  {
    this.dialNumber = dialNumber;
  }

  public Integer getDialNumber()
  {
    return dialNumber;
  }

  public void setVoucherNumber(Integer voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public Integer getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setRedeemedDate(Date redeemedDate)
  {
    this.redeemedDate = redeemedDate;
  }

  public Date getRedeemedDate()
  {
    return redeemedDate;
  }


  public void setRedeemedBy(String redeemedBy)
  {
    this.redeemedBy = redeemedBy;
  }

  public String getRedeemedBy()
  {
    return redeemedBy;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }
}
