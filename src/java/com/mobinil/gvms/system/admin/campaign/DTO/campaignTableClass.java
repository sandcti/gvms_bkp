package com.mobinil.gvms.system.admin.campaign.DTO;



import oracle.sql.BLOB;

public class campaignTableClass
{
  public int campaignId;
  public int dtId;
  public java.util.HashMap campaignGifts;
  public int voucherTypeId;

  public String startDate;
  public String endDate;
  public String creationDate;
  public int campaignStatusId;
  public String createdBy;
  public String giftName;
  public String voucherTypeName;
  public Integer voucherStatus;
  public String campaignStatus;
  public String errorFileFlag;
  public Boolean isClosed=false;
  public String campaignName;
  public String campaignDescription;
  public String campaignColor;
  public Integer campaignUsers;
  public Boolean selected;
  public Boolean renderUpload = false;
  public Boolean renderErrorFile=false;
  public BLOB errorFilePath;
  public String voucherExpireDays;
  
  public campaignTableClass()
  {
  }

  public void setCampaignId(int campaignId)
  {
    this.campaignId = campaignId;
  }

  public int getCampaignId()
  {
    return campaignId;
  }

  public void setDtId(int dtId)
  {
    this.dtId = dtId;
  }

  public int getDtId()
  {
    return dtId;
  }

  

  public void setVoucherTypeId(int voucherTypeId)
  {
    this.voucherTypeId = voucherTypeId;
  }

  public int getVoucherTypeId()
  {
    return voucherTypeId;
  }

 
  
  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }

  public String getCreatedBy()
  {
    return createdBy;
  }

  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }

  public void setVoucherTypeName(String voucherTypeName)
  {
    this.voucherTypeName = voucherTypeName;
  }

  public String getVoucherTypeName()
  {
    return voucherTypeName;
  }

  public void setVoucherStatus(Integer voucherStatus)
  {
    this.voucherStatus = voucherStatus;
  }

  public Integer getVoucherStatus()
  {
    return voucherStatus;
  }

  public void setCampaignStatus(String campaignStatus)
  {
    this.campaignStatus = campaignStatus;
  }

  public String getCampaignStatus()
  {
    return campaignStatus;
  }

  public void setCampaignName(String campaignName)
  {
    this.campaignName = campaignName;
  }

  public String getCampaignName()
  {
    return campaignName;
  }

  public void setCampaignDescription(String campaignDescription)
  {
    this.campaignDescription = campaignDescription;
  }

  public String getCampaignDescription()
  {
    return campaignDescription;
  }

  public void setCampaignColor(String campaignColor)
  {
    this.campaignColor = campaignColor;
  }

  public String getCampaignColor()
  {
    return campaignColor;
  }

  public void setCampaignUsers(Integer campaignUsers)
  {
    this.campaignUsers = campaignUsers;
  }

  public Integer getCampaignUsers()
  {
    return campaignUsers;
  }

  public void setSelected(Boolean selected)
  {
    this.selected = selected;
  }

  public Boolean getSelected()
  {
    return selected;
  }

  public void setRenderUpload(Boolean renderUpload)
  {
    this.renderUpload = renderUpload;
  }

  public Boolean getRenderUpload()
  {
    return renderUpload;
  }

  public void setRenderErrorFile(Boolean renderErrorFile)
  {
    this.renderErrorFile = renderErrorFile;
  }

  public Boolean getRenderErrorFile()
  {
    return renderErrorFile;
  }

  public void setErrorFilePath(BLOB errorFilePath)
  {
    this.errorFilePath = errorFilePath;
  }

  public BLOB getErrorFilePath()
  {
    return errorFilePath;
  }

  public void setErrorFileFlag(String errorFileFlag)
  {
    this.errorFileFlag = errorFileFlag;
  }

  public String getErrorFileFlag()
  {
    return errorFileFlag;
  }

  public void setCampaignStatusId(int campaignStatusId)
  {
    this.campaignStatusId = campaignStatusId;
  }

  public int getCampaignStatusId()
  {
    return campaignStatusId;
  }


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

	public void setCampaignGifts(java.util.HashMap campaignGifts) {
		this.campaignGifts = campaignGifts;
	}

	public java.util.HashMap getCampaignGifts() {
		return campaignGifts;
	}

	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}

	public Boolean getIsClosed() {
		return isClosed;
	}

   public void setVoucherExpireDays(String voucherExpireDays)
   {
      this.voucherExpireDays = voucherExpireDays;
   }

   public String getVoucherExpireDays()
   {
      return voucherExpireDays;
   }
}
