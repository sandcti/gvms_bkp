package com.mobinil.gvms.system.admin.campaign.DAO;

import com.mobinil.gvms.system.admin.beans.Campaign;
import com.mobinil.gvms.system.admin.campaign.DTO.GenericLookUpDTO;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.admin.campaign.DTO.currentVoucherClass;
import com.mobinil.gvms.system.utility.*;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import java.sql.SQLException;

import java.util.*;


import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;


public class campaignDAO
{
  static final String newColor = "blue";
  static final String activeColor = "green";
  static final String closedColor = "gray";
  

  public campaignDAO()
  {
  }

  public static ArrayList<campaignTableClass> getCampaignsInfos(Connection conn)
    throws SQLException
  {
    ArrayList<campaignTableClass> currCampaign = 
      new ArrayList<campaignTableClass>();
    StringBuffer SQL = new StringBuffer("SELECT GC.CAMPAIGN_ID, GVMS_GET_GIFTS(GC.CAMPAIGN_ID) giftIds, TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') CAMPAIGN_START_DATE,");
      
    SQL.append(" TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') CAMPAIGN_CLOSED_DATE,");  
    SQL.append(" TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') CAMPAIGN_CREATION_DATE,");  
    SQL.append(" GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   ");
    SQL.append(" GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED,  ");
    SQL.append(" GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME , ");
    SQL.append(" (COUNT (GCV.CURRENT_VOUCHER_NUMBER)-(select count(CURRENT_VOUCHER_NUMBER) from GVMS_CURRENT_VOUCHER where VOUCHER_STATUS_ID =5 AND CAMPAIGN_ID=GC.CAMPAIGN_ID)) TOTAL_CURRENT_VOUCHER_NUMBER, ");
    SQL.append(" GVT.VOUCHER_TYPE_NAME,");  
    SQL.append(" GCS.CAMPAIGN_STATUS,GC.VOUCHER_EXPIRE_DAYS    ");
    SQL.append(" FROM GVMS_CAMPAIGN GC , GVMS_CURRENT_VOUCHER GCV , GVMS_VOUCHER_TYPE GVT , GVMS_CAMPAIGN_STATUS GCS");   
    SQL.append(" WHERE GC.CAMPAIGN_STATUS_ID !=4 and GC.CAMPAIGN_ID = GCV.CAMPAIGN_ID (+) and GC.VOUCHER_TYPE_ID = GVT.VOUCHER_TYPE_ID");    
    SQL.append(" and GCS.CAMPAIGN_STATUS_ID = GC.CAMPAIGN_STATUS_ID");    
    SQL.append(" group by     GC.CAMPAIGN_ID,   ");
    SQL.append(" TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') ,  ");
    SQL.append(" TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') ,  ");
    SQL.append(" TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') ,  ");
    SQL.append(" GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   ");
    SQL.append(" GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED,");  
    SQL.append(" GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME, "); 
    SQL.append(" GVT.VOUCHER_TYPE_NAME , GCS.CAMPAIGN_STATUS,GC.VOUCHER_EXPIRE_DAYS");
    String defaultDaysToExpire=jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" );
    ResultSet campaignRS = ExecuteQueries.executeQuery(conn,SQL.toString());
    while (campaignRS.next())
    {
      campaignTableClass ctc = new campaignTableClass();
      ctc.setCampaignId(campaignRS.getInt("CAMPAIGN_ID"));
      int dd = campaignRS.getInt("total_CURRENT_VOUCHER_NUMBER");
      if (dd != 0)
        ctc.setCampaignUsers(campaignRS.getInt("total_CURRENT_VOUCHER_NUMBER"));
      else
        ctc.setCampaignUsers(0);
      String gifts = campaignRS.getString("giftIds");
      HashMap campaignGifts = new HashMap();
      StringBuffer giftName = new StringBuffer("");
      String giftsArray[] = gifts.split(",");
      for (int i = 0; i < giftsArray.length; i++) {
		String giftsIdsAndNames =  giftsArray[i];
		String giftIdsNamesArray [] = giftsIdsAndNames.split("-");
		campaignGifts.put(giftIdsNamesArray[0], giftIdsNamesArray[1]);
		giftName.append(giftIdsNamesArray[1]+System.getProperty("line.separator"));
	}
      String voucherExpireDays = campaignRS.getString ( "VOUCHER_EXPIRE_DAYS" );
      if (voucherExpireDays==null||voucherExpireDays.compareTo ( "" )==0)voucherExpireDays =defaultDaysToExpire;
      
      ctc.setVoucherExpireDays ( voucherExpireDays );
      ctc.setGiftName(giftName.toString());
      ctc.setCampaignGifts(campaignGifts);
      ctc.setVoucherStatus(campaignRS.getInt("VOUCHER_TYPE_ID"));
      ctc.setCreatedBy(campaignRS.getString("CAMPAIGN_CREATEDBY"));
      ctc.setStartDate(campaignRS.getString("CAMPAIGN_START_DATE"));
      ctc.setEndDate(campaignRS.getString("CAMPAIGN_CLOSED_DATE"));
      ctc.setCreationDate(campaignRS.getString("CAMPAIGN_CREATION_DATE"));
      ctc.setCampaignName(campaignRS.getString("CAMPAIGN_NAME"));
      ctc.setCampaignDescription(campaignRS.getString("CAMPAIGN_DESCRIPTION"));
      ctc.setVoucherTypeId(campaignRS.getInt("VOUCHER_TYPE_ID"));
      ctc.setVoucherTypeName(campaignRS.getString("VOUCHER_TYPE_NAME"));
      ctc.setCampaignStatus(campaignRS.getString("CAMPAIGN_STATUS"));
      String errorFileFlag = campaignRS.getString("CAMPAIGN_ERROR_FILE");
      ctc.setErrorFileFlag(errorFileFlag);
      if (errorFileFlag.compareTo("1") == 
          0) 
      {
    	  ctc.setRenderErrorFile(true);
    	  ctc.setErrorFilePath(campaignDAO.getCampaigntErrorFile(conn,ctc.getCampaignId()));
      }
      
      int campaignStatusId = campaignRS.getInt("CAMPAIGN_STATUS_ID");
      
      ctc.setCampaignStatusId(campaignStatusId);
      
      if (campaignStatusId == Campaign.closedCampaign)ctc.setIsClosed(true);
      
      if (campaignStatusId == Campaign.newCampaign || campaignStatusId == Campaign.activeCampaign)
      {
    	  ctc.setRenderUpload(true);
    	  ctc.setCampaignColor("color:" + newColor + ";");
      }
       if (campaignStatusId == Campaign.activeCampaign)ctc.setCampaignColor("color:" + activeColor + ";");
       
       if (campaignStatusId == Campaign.closedCampaign)ctc.setCampaignColor("color:" + closedColor + ";");
      
      
      currCampaign.add(ctc);

    }
    DBConnection.cleanResultSet(campaignRS);


    return currCampaign;


  }
  
  
  public static HashMap/* ArrayList < SelectItem > */getCampaigns(String typeOfCall)
  throws SQLException
{


//if (jsfUtils.containInSession ( "allCampaignHashMap" )) return (HashMap)jsfUtils.getFromSession ( "allCampaignHashMap" );
  
ArrayList < SelectItem > campaigns = new ArrayList < SelectItem > ( );      

Connection connection = null;
Statement statement = null;
HashMap hm = new HashMap ( );
StringBuffer SQL = new StringBuffer (
     "SELECT GC.CAMPAIGN_ID,GVMS_GET_GIFTS(GC.CAMPAIGN_ID)  giftIds,GVMS_GET_CAMPAIGN_USERS(GC.CAMPAIGN_ID) user_type_ids,TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') CAMPAIGN_START_DATE," );      
SQL.append ( " TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') CAMPAIGN_CLOSED_DATE," );
SQL.append ( " TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') CAMPAIGN_CREATION_DATE," );
SQL.append ( " GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   " );
SQL.append ( " GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED,  " );
SQL.append ( " GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME , COUNT(GCV.CURRENT_VOUCHER_NUMBER) TOTAL_CURRENT_VOUCHER_NUMBER , GVT.VOUCHER_TYPE_NAME," );
SQL.append ( " GCS.CAMPAIGN_STATUS    " );
SQL.append ( " FROM GVMS_CAMPAIGN GC , GVMS_CURRENT_VOUCHER GCV , GVMS_VOUCHER_TYPE GVT , GVMS_CAMPAIGN_STATUS GCS" );
SQL.append ( " WHERE GC.CAMPAIGN_STATUS_ID =2 and GC.CAMPAIGN_ID = GCV.CAMPAIGN_ID (+) and GC.VOUCHER_TYPE_ID = GVT.VOUCHER_TYPE_ID" );
SQL.append ( " and GCS.CAMPAIGN_STATUS_ID = GC.CAMPAIGN_STATUS_ID" );
SQL.append ( " group by     GC.CAMPAIGN_ID,   " );
SQL.append ( " TO_CHAR (GC.CAMPAIGN_START_DATE, 'DD-MM-YYYY') ,  " );
SQL.append ( " TO_CHAR (GC.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') ,  " );
SQL.append ( " TO_CHAR (GC.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') ,  " );
SQL.append ( " GC.VOUCHER_TYPE_ID, GC.CAMPAIGN_STATUS_ID, GC.CAMPAIGN_CREATEDBY,   " );
SQL.append ( " GC.CAMPAIGN_ERROR_FILE, GC.CAMPAIGN_MODIFIED_BY, GC.CAMPAIGN_LAST_MODIFIED," );
SQL.append ( " GC.CAMPAIGN_DESCRIPTION, GC.CAMPAIGN_NAME, " );
SQL.append ( " GVT.VOUCHER_TYPE_NAME , GCS.CAMPAIGN_STATUS" );
connection = DBConnection.getConnection ( );
statement = connection.createStatement ( );
ResultSet campaignRS = statement.executeQuery ( SQL.toString ( ) );

while ( campaignRS.next ( ) )
{
  String camId =campaignRS.getString ( "CAMPAIGN_ID" );
  String camName =campaignRS.getString ( "CAMPAIGN_NAME" ); 
  campaigns.add ( new SelectItem (
        camId ,  camName) );
  
  HashMap dd = new HashMap();
  dd.put ( "gifts" , getGifts(connection,camId,typeOfCall) );
  dd.put ( "channels" , getChannels(connection,camId,typeOfCall) );
  
  hm.put(camId,dd);
  
  
}
if (typeOfCall.compareTo ( "home" )==0)
  campaigns.add (0, new SelectItem ("-1" ,  "---Select One---") );         

if (typeOfCall.compareTo ( "history" )==0)
  campaigns.add (0, new SelectItem ("-1" ,  "*") );

hm.put ( "all" , campaigns );
jsfUtils.putInSession ( hm , "allCampaignHashMap" );
DBConnection.cleanResultSet ( campaignRS );
connection.close ( );
connection=null;
return hm;
}
  public static ArrayList < SelectItem > getGifts(Connection con,
        String campaginId,String typeOfCall) throws SQLException
  {
     ArrayList < SelectItem > gifts = new ArrayList < SelectItem > ( );
     if (typeOfCall.compareTo ( "home" )==0)
     {
        gifts.add( new SelectItem("-1","---Select One---"));
        gifts.add( new SelectItem("0","Any"));
     }         
     
     if (typeOfCall.compareTo ( "history" )==0)
        gifts.add (new SelectItem ("-1" ,  "*") );
     
     
     String SQL = "Select GVMS_GET_GIFTS('" + campaginId+ "') giftIds from dual ";
     Statement st = con.createStatement ( );
     ResultSet giftsRS = st.executeQuery ( SQL.toString ( ) );
     
     while ( giftsRS.next ( ) )
     {
        String strGifts = giftsRS.getString ( "giftIds" );
        //System.out.println ( "strGifts isssss " + strGifts + "result set is "
//              + giftsRS );
        if ( strGifts != null )
        {
           String giftsArray[] = strGifts.split ( "," );
           for ( int i = 0 ; i < giftsArray.length ; i ++ )
           {
              String giftsIdsAndNames = giftsArray[i];
              String giftIdsNamesArray[] = giftsIdsAndNames.split ( "-" );
              gifts.add ( new SelectItem ( giftIdsNamesArray[0] ,
                    giftIdsNamesArray[1] ) );               
           }
        }
     }
     
     DBConnection.cleanResultSet ( giftsRS );
     return gifts;
  }
  
  public static ArrayList < SelectItem > getChannels(Connection con,
        String campaginId,String typeOfCall) throws SQLException
  {
     ArrayList < SelectItem > channels = new ArrayList < SelectItem > ( );
     if (typeOfCall.compareTo ( "home" )==0)
     {
        channels.add( new SelectItem("-1","---Select One---"));
        channels.add( new SelectItem("0","Any"));
     }         
     
     if (typeOfCall.compareTo ( "history" )==0)
        channels.add (new SelectItem ("-1" ,  "*") );
     
     
     String SQL = "Select GVMS_GET_CAMPAIGN_USERS('" + campaginId+ "') channelIds from dual ";
     Statement st = con.createStatement ( );
     ResultSet channelsRS = st.executeQuery ( SQL.toString ( ) );
     
     while ( channelsRS.next ( ) )
     {
        String strChannels = channelsRS.getString ( "channelIds" );         
        if ( strChannels != null )
        {
           String channelsArray[] = strChannels.split ( "," );
           for ( int i = 0 ; i < channelsArray.length ; i ++ )
           {
              String channelsIdsAndNames = channelsArray[i];
              String channelsNamesArray[] = channelsIdsAndNames.split ( "-" );
              channels.add ( new SelectItem ( channelsNamesArray[0] ,
                    channelsNamesArray[1] ) );
              
           }
        }
     }
     DBConnection.cleanResultSet ( channelsRS );
     return channels;
  }
  
  
  public static int closeCampaign(int campaignId)
  throws SQLException
{


  int rowsCount = ExecuteQueries.executeNoneQuery("update GVMS_CAMPAIGN set CAMPAIGN_STATUS_ID=5,CAMPAIGN_CLOSED_DATE=sysdate  where CAMPAIGN_ID=" +
                                         campaignId);
  if (rowsCount>0)
  {
  UpdateClass.updateCampaignInfo(campaignId+"", true);
  }
    return rowsCount;
}
  public static oracle.sql.BLOB getCampaigntErrorFile(Connection conn,int campaignId)
    throws SQLException
  {
    String sql = 
      "SELECT CAMPAIGN_FILE FROM GVMS_CAMPAIGN WHERE CAMPAIGN_ID = " + 
      campaignId;
    ResultSet campaignRS = ExecuteQueries.executeQuery(conn,sql);
    oracle.sql.BLOB blob = null;
    if (campaignRS.next())
      blob = 
          ((oracle.jdbc.OracleResultSet) campaignRS).getBLOB("CAMPAIGN_FILE");
    DBConnection.cleanResultSet(campaignRS);
    return blob;


  }

  public static int deleteCampaign(int campaignId)
    throws SQLException
  {

    return ExecuteQueries.executeNoneQuery("update GVMS_CAMPAIGN set CAMPAIGN_STATUS_ID=4,CAMPAIGN_NAME=concat(CAMPAIGN_NAME,'_del') where CAMPAIGN_ID=" + 
                                           campaignId);

  }

  public static ArrayList<campaignTableClass> fillCurrCampaign(Connection conn,ResultSet filterBy, 
                                                               int campaignId, 
                                                               ArrayList<campaignTableClass> currCampaign)
  {
    
    try
    {

      campaignTableClass cTC = new campaignTableClass();
      String campaignStatus = filterBy.getString("CAMPAIGN_STATUS");
      cTC.setCampaignStatus(campaignStatus);
      
      int campaignStatusId = filterBy.getInt("CAMPAIGN_STATUS_ID");
      
      if (campaignStatusId == Campaign.closedCampaign)cTC.setIsClosed(true);
      
      if (campaignStatusId == Campaign.newCampaign || campaignStatusId == Campaign.activeCampaign)
      {
    	  cTC.setRenderUpload(true);
    	  cTC.setCampaignColor("color:" + newColor + ";");
      }
      
      
      if (campaignStatus.compareTo("ACTIVE") == 0)
      {
        cTC.setCampaignColor("color:" + activeColor + ";");
      }
      else if (campaignStatus.compareTo("CLOSED") == 0)
      {
    	  cTC.setIsClosed(true);
        cTC.setCampaignColor("color:" + closedColor + ";");
      }
      cTC.setCampaignDescription(filterBy.getString("CAMPAIGN_DESCRIPTION"));
      cTC.setCampaignName(filterBy.getString("CAMPAIGN_NAME"));

      ResultSet campaignUsersCountRS = 
        ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_REPORT_TOTAL_VOUCHER from GVMS_CAMPAIGN_REPORT where CAMPAIGN_ID =" + 
                                    campaignId);
      if (campaignUsersCountRS.next())
      {
        cTC.setCampaignUsers(campaignUsersCountRS.getInt("CAMPAIGN_REPORT_TOTAL_VOUCHER"));
      }
      campaignUsersCountRS.getStatement().close();
      campaignUsersCountRS.close();

      cTC.setCreatedBy(filterBy.getString("CAMPAIGN_CREATEDBY"));
      cTC.setCreationDate(filterBy.getString("CAMPAIGN_CREATION_DATE"));
      cTC.setEndDate(filterBy.getString("CAMPAIGN_CLOSED_DATE"));
      cTC.setStartDate(filterBy.getString("CAMPAIGN_START_DATE"));
      cTC.setGiftName(filterBy.getString("GIFT_NAME"));
      
      String gifts = filterBy.getString("GIFT_NAME");
      HashMap campaignGifts = new HashMap();
      StringBuffer giftName = new StringBuffer("");
      String giftsArray[] = gifts.split(",");
      for (int i = 0; i < giftsArray.length; i++) {
		String giftsIdsAndNames =  giftsArray[i];
		String giftIdsNamesArray [] = giftsIdsAndNames.split("-");
		campaignGifts.put(giftIdsNamesArray[0], giftIdsNamesArray[1]);
		giftName.append(giftIdsNamesArray[1]+System.getProperty("line.separator"));
	}
      cTC.setGiftName(giftName.toString());
      cTC.setCampaignGifts(campaignGifts);
      
      cTC.setVoucherTypeName(filterBy.getString("VOUCHER_TYPE_NAME"));

      currCampaign.add(cTC);

    }
    catch (SQLException e)
    {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
    }

    return currCampaign;


  }

  public static ArrayList<campaignTableClass> filter_Action(Connection conn,String divValue, 
                                                            ArrayList<campaignTableClass> currCampaign, 
                                                            String inputString, 
                                                            Date toDate, 
                                                            Date fromDate)
  {
    
    String condition = "";
    if (divValue.compareTo("x_0") == 0)
    {
      condition = "CAMPAIGN_CREATION_DATE";
    }

    if (divValue.compareTo("x_2") == 0)
    {
      condition = "CAMPAIGN_NAME";
    }
    if (divValue.compareTo("x_3") == 0)
    {
      condition = "CAMPAIGN_START_DATE";
    }
    if (divValue.compareTo("x_4") == 0)
    {
      condition = "CAMPAIGN_CLOSED_DATE";
    }
    if (divValue.compareTo("x_5") == 0)
    {
      condition = "Voucher_type_Name";
    }
    if (divValue.compareTo("x_6") == 0)
    {
      condition = "GIFT_NAME";
    }
    if (divValue.compareTo("x_7") == 0)
    {
      condition = "T3.CAMPAIGN_STATUS";
    }
    if (divValue.compareTo("*") == 0)
    {
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Campaign_Bean");
      new Campaign();
    }
    if (divValue.compareTo("x_0") == 0 || divValue.compareTo("x_3") == 0 || 
        divValue.compareTo("x_4") == 0)
    {
      currCampaign.clear();
      ResultSet filterBy = 
        ExecuteQueries.executeQuery(conn,"Select t1.*,GVMS_GET_GIFTS(t1.CAMPAIGN_ID) GIFT_NAME,t3.CAMPAIGN_STATUS,t4.VOUCHER_TYPE_NAME from GVMS_CAMPAIGN t1  left join GVMS_CAMPAIGN_STATUS t3 on t1.CAMPAIGN_STATUS_ID = t3.CAMPAIGN_STATUS_ID left join GVMS_VOUCHER_TYPE t4 on t1.VOUCHER_TYPE_ID = t4.VOUCHER_TYPE_ID where t1.CAMPAIGN_STATUS_ID!=4 and " + 
                                    condition + " between " + 
                                    convertDate(fromDate) + " and " + 
                                    convertDate(toDate));
      try
      {
        while (filterBy.next())
        {
          int campaignId = filterBy.getInt("CAMPAIGN_ID");
          fillCurrCampaign(conn,filterBy, campaignId, currCampaign);

        }
        filterBy.getStatement().close();
        filterBy.close();
      }
      catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    }
    if (divValue.compareTo("x_1") == 0)
    {

    }
    if (divValue.compareTo("x_2") == 0 || divValue.compareTo("x_5") == 0 || 
        divValue.compareTo("x_6") == 0 || divValue.compareTo("x_7") == 0)
    {
      currCampaign.clear();
      StringBuffer sql = new StringBuffer("");

      sql.append("SELECT distinct (T1.CAMPAIGN_ID), TO_CHAR (T1.CAMPAIGN_START_DATE, 'DD-MM-YYYY') CAMPAIGN_START_DATE, TO_CHAR (T1.CAMPAIGN_CLOSED_DATE, 'DD-MM-YYYY') CAMPAIGN_CLOSED_DATE, TO_CHAR (T1.CAMPAIGN_CREATION_DATE, 'DD-MM-YYYY') CAMPAIGN_CREATION_DATE,"); 
      sql.append("  T1.VOUCHER_TYPE_ID, T1.CAMPAIGN_STATUS_ID, T1.CAMPAIGN_CREATEDBY,");
      sql.append(" T1.CAMPAIGN_NAME, T1.CAMPAIGN_DESCRIPTION, T1.CAMPAIGN_LAST_MODIFIED, T1.CAMPAIGN_MODIFIED_BY, T1.CAMPAIGN_ERROR_FILE,");  
      sql.append(" GVMS_GET_GIFTS (T1.CAMPAIGN_ID) GIFT_NAME, T3.CAMPAIGN_STATUS,T4.VOUCHER_TYPE_NAME");
      sql.append(" FROM GVMS_CAMPAIGN T1, GVMS_CAMPAIGN_STATUS T3, GVMS_VOUCHER_TYPE T4, GVMS_CAMPAIGN_GIFT T5, GVMS_GIFT T6");   
      sql.append(" WHERE T1.CAMPAIGN_STATUS_ID = T3.CAMPAIGN_STATUS_ID");
      sql.append(" AND T5.GIFT_ID in (select GIFT_ID from GVMS_CAMPAIGN_GIFT where campaign_id=T1.CAMPAIGN_ID)"); 
      sql.append(" AND T5.GIFT_ID =T6.GIFT_ID  ");
      sql.append(" AND T1.VOUCHER_TYPE_ID = T4.VOUCHER_TYPE_ID");
      sql.append(" AND T1.CAMPAIGN_STATUS_ID != 4 AND ");
      sql.append(condition + "='" + inputString + "'");

      
      ResultSet filterBy = 
        ExecuteQueries.executeQuery(conn,sql.toString());
      try
      {
        while (filterBy.next())
        {
          int campaignId = filterBy.getInt("CAMPAIGN_ID");
          fillCurrCampaign(conn,filterBy, campaignId, currCampaign);


        }
        filterBy.getStatement().close();
        filterBy.close();
      }
      catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    }
    return currCampaign;
  }

  public static ArrayList<GenericLookUpDTO> getLookUpData(Connection conn,String cols, 
                                                          String tableName)
    throws SQLException
  {

    ArrayList<GenericLookUpDTO> alglu = new ArrayList<GenericLookUpDTO>();
    
    ResultSet rs = 
      ExecuteQueries.executeQuery(conn,"Select " + cols + " from " + tableName);
    while (rs.next())
    {
      GenericLookUpDTO gludto = new GenericLookUpDTO();
      gludto.setId(rs.getInt(1) + "");
      gludto.setName(rs.getString(2));
      alglu.add(gludto);
    }
    DBConnection.cleanResultSet(rs);
    return alglu;
  }

  public static ArrayList<GenericLookUpDTO> getCampaignData(Connection conn,int typeOfData, 
                                                            String paramter)
    throws SQLException
  {
    ArrayList<GenericLookUpDTO> ss = null;
    if (typeOfData == 1)
    {
      ss = 
          getLookUpData(conn,"t1.USER_TYPE_ID,t2.USER_TYPE_NAME", "GVMS_CAMPAIGN_USER t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID = t2.USER_TYPE_ID where CAMPAIGN_ID=" + 
                        paramter);
    }
    if (typeOfData == 2)
    {
      ss = 
          getLookUpData(conn,"*", "GVMS_USER_TYPE where user_type_status_id=1");
    }
    if (typeOfData == 3)
    {
      ss = getLookUpData(conn,"*", "GVMS_VOUCHER_TYPE");
    }
    if (typeOfData == 4)
    {
      ss = getLookUpData(conn,"*", "GVMS_GIFT where gift_status_id=1");
    }
    return ss;

  }

  public static boolean getDublicateName(Connection conn,String name)
    throws SQLException
  {

    ResultSet rs = 
      ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME from GVMS_CAMPAIGN where CAMPAIGN_NAME = '" + 
                                  name + "'");
    if (rs != null)
    {
      if (rs.next())
      {
        if (rs.getString(1) == null)
          return true;
        else
          return false;
      }
      else
        return false;
    }
    else
      return true;

  }

  public static String getDateTime()
  {
    Date currentDate = new Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + 
      currentHour + ":" + currentMin + ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }

  public static String createCampaign(Connection conn,String campaignName, 
                                      String campaignDescription, 
                                      String gValue, String vTValue, 
                                      String campaignStatusId, 
                                      String creatorName, 
                                      ArrayList<String> UTSelectedValue, 
                                      Date startDate, Date endDate,String daysToExpire)
    throws SQLException
  {
    String currDate = 
      "to_date('" + getDateTime() + "','mm/dd/yyyy HH24:MI:SS')";
    String campaignId = 
      DBConnection.getSequenceNextVal(conn,"GVMS_CAMPAIGN_SEQ_ID");
    String SQL = 
      "insert into GVMS_CAMPAIGN (CAMPAIGN_ID,CAMPAIGN_NAME,CAMPAIGN_DESCRIPTION,GIFT_ID,CAMPAIGN_START_DATE,CAMPAIGN_CREATION_DATE,VOUCHER_TYPE_ID,CAMPAIGN_STATUS_ID,CAMPAIGN_CREATEDBY,CAMPAIGN_ERROR_FILE)" + 
      "values(" + campaignId + ",'" + campaignName.trim() + "','" + 
      campaignDescription + "'," + gValue + "," + convertDate(startDate) + 
      "," + currDate + "," + vTValue + "," + 
      campaignStatusId + ",'" + creatorName + "','0')";


    int inserCampaginRS = ExecuteQueries.executeNoneQuery(SQL);
    SQL.equals(null);


    if (inserCampaginRS == 1)
    {
      for (int count = 0; count < UTSelectedValue.size(); count++)
      {

        ExecuteQueries.executeNoneQuery("insert into GVMS_CAMPAIGN_USER(CAMPAIGN_USER_ID,CAMPAIGN_ID,USER_TYPE_ID,CAMPAIGN_USER_CREATION_DATE,CAMPAIGN_USER_CREATEDBY,VOUCHER_EXPIRE_DAYS) Values" + 
                                        "(GVMS_CAMPAIGN_USER_SEQ_ID.nextval," + 
                                        campaignId + "," + 
                                        UTSelectedValue.get(count) + "," + 
                                        currDate + ",'" + creatorName +"','" + daysToExpire +"')");
      }


      //System.out.println("Start Create Process");
      SQL = 
          "CREATE TABLE GVMS_REDEEM_CAMPAIGN_" + campaignId + "(\n" + "  REDEEM_CAMPAIGN_ID          INTEGER           NOT NULL,\n" + 
          "  REDEEM_VOUCHER_DIAL_NUMBER  INTEGER,\n" + 
          "  REDEEM_VOUCHER_NUMBER       INTEGER,\n" + 
          "  CURRENT_VOUCHER_ID          INTEGER)";


      boolean createTable = ExecuteQueries.execute(conn,SQL);
      Boolean createSeq = null;
      SQL = "";
      //System.out.println("Process Result:" + createTable);
      //System.out.println("End Create Process");

      //System.out.println("Start Alter Process");

      if (!createTable)
      {
      SQL = 
          "ALTER TABLE GVMS_REDEEM_CAMPAIGN_" + campaignId + " ADD (\n" + "  CONSTRAINT GVMS_CUSTOMER_CAMP_PK\n" + 
          " PRIMARY KEY\n" + " (REDEEM_CAMPAIGN_ID));\n" + "\n" + "\n";
      createTable = ExecuteQueries.execute(conn,SQL);
      
      SQL.equals(null);

      //System.out.println("Process Result:" + createTable);
      //System.out.println("End Alter Process");


      SQL = 
          "CREATE SEQUENCE CAMPAIGN_" + campaignId + "_SEQ\n" + 
          "  START WITH 1\n" + "  MAXVALUE 99999999999999999\n" + 
          "  MINVALUE 1\n";
      

       createSeq = ExecuteQueries.execute(conn,SQL);
      }
      else{
        ExecuteQueries.executeNoneQuery("delete from GVMS_CAMPAIGN where CAMPAIGN_ID="+campaignId);
      }
      

      if (createTable == false && createSeq == false)
      {
        //System.out.println("Table and sequance create");
        return "Campaign Created";

      }
      else
      {
        //System.out.println("Table and sequance doesn't create");
        return "Creation Failed";
      }

    }

    else
    {

      return "Creation Failed";
    }
    

  }
  public static String editCampaign(String campaignName, 
                                      String campaignDescription, 
                                      String gValue, String vTValue, 
                                      String campaignStatusId, 
                                      String creatorName, 
                                      ArrayList<String> UTSelectedValue, 
                                      Date startDate, Date endDate, String campaignId,String bridg,ArrayList<SelectItem> UTSelectedArray)
  { 
    
    String currDate = 
      "to_date('" + getDateTime() + "','mm/dd/yyyy HH24:MI:SS')";
    
      
    
    String SQLUpadateCampaign = "";
    String SQLDeleteUserTypes = "";
    
    

    if (gValue.compareTo("--Select One--") == 0)
    {
      return "Please select gift for " + campaignName + ".";
      
    }
    if (campaignName.compareTo("") == 0 || campaignName == null)
    {
      return "campaign must have name.";
      
    }
    if (vTValue.compareTo("--Select One--") == 0)
    {
      return  "Please select voucher type for " + campaignName + ".";
      
    }
    if (campaignDescription.length() > 1001)
    {
      return "Maximum space for description is 1000 character.";
      
    }
    if (UTSelectedArray.size() == 0)
    {
      return "Please insert at least one user type.";
      
    }

    if (bridg.compareTo("NEW") == 0)
    {
      SQLUpadateCampaign = 
          "update GVMS_CAMPAIGN set GIFT_ID=" + gValue + " ,CAMPAIGN_START_DATE=" + 
          convertDate(startDate) + ", VOUCHER_TYPE_ID=" + vTValue + 
          " ,CAMPAIGN_NAME='" + campaignName.trim() + 
          "' ,CAMPAIGN_DESCRIPTION='" + campaignDescription + 
          "' ,CAMPAIGN_LAST_MODIFIED=" + currDate + 
          ",CAMPAIGN_MODIFIED_BY='" + creatorName + 
          "' ,CAMPAIGN_STATUS_ID=" + campaignStatusId + 
          " where CAMPAIGN_ID=" + campaignId;


      SQLDeleteUserTypes = 
          "Delete from GVMS_CAMPAIGN_USER where CAMPAIGN_ID=" + campaignId;
      int deleteUserTypes = 
        ExecuteQueries.executeNoneQuery(SQLDeleteUserTypes);


      for (int counter = 0; counter < UTSelectedValue.size(); counter++)
      {
        ExecuteQueries.executeNoneQuery("insert into GVMS_CAMPAIGN_USER(CAMPAIGN_USER_ID,CAMPAIGN_ID,USER_TYPE_ID,CAMPAIGN_USER_CREATION_DATE,CAMPAIGN_USER_CREATEDBY) Values" + 
                                        "(GVMS_CAMPAIGN_USER_SEQ_ID.nextval," + 
                                        campaignId + "," + 
                                        UTSelectedValue.get(counter) + 
                                        "," + currDate + ",'" + 
                                        creatorName + "')");
      }


    }
    if (bridg.compareTo("ACTIVE") == 0)
    {
      SQLUpadateCampaign = 
          "update GVMS_CAMPAIGN set "+ 
          "CAMPAIGN_DESCRIPTION='" + campaignDescription + 
          "' ,CAMPAIGN_LAST_MODIFIED=" + currDate + 
          ",CAMPAIGN_MODIFIED_BY='" + creatorName + 
          "',CAMPAIGN_STATUS_ID=" + campaignStatusId + 
          " where CAMPAIGN_ID=" + campaignId;

      SQLDeleteUserTypes = 
          "Delete from GVMS_CAMPAIGN_USER where CAMPAIGN_ID=" + campaignId;
      int deleteUserTypes = 
        ExecuteQueries.executeNoneQuery(SQLDeleteUserTypes);
      for (int counter = 0; counter < UTSelectedValue.size(); counter++)
      {


        ExecuteQueries.executeNoneQuery("insert into GVMS_CAMPAIGN_USER(CAMPAIGN_USER_ID,CAMPAIGN_ID,USER_TYPE_ID,CAMPAIGN_USER_CREATION_DATE,CAMPAIGN_USER_CREATEDBY) Values" + 
                                        "(GVMS_CAMPAIGN_USER_SEQ_ID.nextval," + 
                                        campaignId + "," + 
                                        UTSelectedArray.get(counter).getValue() + 
                                        "," + currDate + ",'" + 
                                        creatorName + "')");
      }

    }


    int update =

      ExecuteQueries.executeNoneQuery(SQLUpadateCampaign);
    if (update == 0)
    {
      return  "Update Fails";

    }
    else
    {
      return"Update Complete";

    }
  
  }
  public static ArrayList<currentVoucherClass> getCampaignUsers (Connection conn,ArrayList<currentVoucherClass> totalVouchers, Integer campaignId, String campaignStatus) throws SQLException{
    if (campaignStatus.compareTo("Expired") == 0)
      {
        String SQL = 
          "Select VOUCHER_DATE_TIME,REDEEM_VOUCHER_DIAL_NUMBER,REDEEM_VOUCHER_NUMBER from GVMS_REDEEM_CAMPAIGN_" + 
          campaignId + " where CAMPAIGN_ID =" + campaignId;
        String SQL1 = 
          "Select EXPIRED_VOUCHER_DIAL_NUMBER,EXPIRED_VOUCHER_NUMBER from GVMS_EXPIRED_VOUCHER  where CAMPAIGN_ID =" + 
          campaignId;

        ResultSet expiredVouchersRS = ExecuteQueries.executeQuery(conn,SQL);

        ResultSet redeemVouchersRS = ExecuteQueries.executeQuery(conn,SQL1);
       
            while (expiredVouchersRS.next())
              {
                currentVoucherClass a = new currentVoucherClass();
                a.setDialNumber(expiredVouchersRS.getInt("EXPIRED_VOUCHER_DIAL_NUMBER"));
                a.setVoucherNumber(expiredVouchersRS.getInt("EXPIRED_VOUCHER_NUMBER"));
                a.setStatus("Expired");
                totalVouchers.add(a);

              }
            expiredVouchersRS.getStatement().close();
            expiredVouchersRS.close();

            while (redeemVouchersRS.next())
              {
                currentVoucherClass a = new currentVoucherClass();
                a.setDialNumber(redeemVouchersRS.getInt("REDEEM_VOUCHER_DIAL_NUMBER"));
                a.setVoucherNumber(redeemVouchersRS.getInt("REDEEM_VOUCHER_NUMBER"));
                a.setRedeemedDate(redeemVouchersRS.getDate("VOUCHER_DATE_TIME"));
                a.setStatus("Redeemed");
                totalVouchers.add(a);

              }
            redeemVouchersRS.getStatement().close();
            redeemVouchersRS.close();

         
      }
    if (campaignStatus.compareTo("Active") == 0)
      {
        String SQL = 
          "Select CURRENT_VOUCHER_DIAL_NUMBER,CURRENT_VOUCHER_NUMBER from GVMS_CURRENT_VOUCHER where CAMPAIGN_ID= " + 
          campaignId;
        String SQL1 = 
          "Select VOUCHER_DATE_TIME,REDEEM_VOUCHER_DIAL_NUMBER,REDEEM_VOUCHER_NUMBER from GVMS_REDEEM_CAMPAIGN_" + 
          campaignId;

        ResultSet currentVouchersRS = ExecuteQueries.executeQuery(conn,SQL);

        ResultSet redeemVouchersRS = ExecuteQueries.executeQuery(conn,SQL1);
        
            while (currentVouchersRS.next())
              {
                currentVoucherClass a = new currentVoucherClass();
                a.setDialNumber(currentVouchersRS.getInt("CURRENT_VOUCHER_DIAL_NUMBER"));
                a.setVoucherNumber(currentVouchersRS.getInt("CURRENT_VOUCHER_NUMBER"));
                a.setStatus("Active");
                totalVouchers.add(a);

              }
            currentVouchersRS.getStatement().close();
            currentVouchersRS.close();
            while (redeemVouchersRS.next())
              {
                currentVoucherClass a = new currentVoucherClass();
                a.setDialNumber(redeemVouchersRS.getInt("REDEEM_VOUCHER_DIAL_NUMBER"));
                a.setVoucherNumber(redeemVouchersRS.getInt("REDEEM_VOUCHER_NUMBER"));
                a.setRedeemedDate(redeemVouchersRS.getDate("VOUCHER_DATE_TIME"));
                a.setStatus("Redeemed");
                totalVouchers.add(a);
              }
            redeemVouchersRS.getStatement().close();
            redeemVouchersRS.close();
         

      }

    if (campaignStatus.compareTo("New") == 0)
      {
        String SQL = 
          "Select CURRENT_VOUCHER_DIAL_NUMBER,CURRENT_VOUCHER_NUMBER from GVMS_CURRENT_VOUCHER where CAMPAIGN_ID= " + 
          campaignId;


        ResultSet currentVouchersRS = ExecuteQueries.executeQuery(conn,SQL);


        
            while (currentVouchersRS.next())
              {
                currentVoucherClass a = new currentVoucherClass();
                a.setDialNumber(currentVouchersRS.getInt("CURRENT_VOUCHER_DIAL_NUMBER"));
                a.setVoucherNumber(currentVouchersRS.getInt("CURRENT_VOUCHER_NUMBER"));
                a.setStatus("New");
                totalVouchers.add(a);

              }
            currentVouchersRS.getStatement().close();
            currentVouchersRS.close();

         
      }
      return totalVouchers;
  }

  public static String convertDate(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
    {

      String[] toDate = new String[3];
      toDate[0] = "00";
      toDate[1] = "00";
      toDate[2] = "0000";
      values = 
          "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + 
          "', 'mm/dd/yyyy')";
      return values;
    }
    else
    {
      int day = undefineDate.getDate();
      int year = undefineDate.getYear() + 1900;
      int month = undefineDate.getMonth() + 1;

      values = 
          "to_date('" + day + "/" + month + "/" + year + "', 'dd/mm/yyyy')";

      return values;
    }
  }
}
