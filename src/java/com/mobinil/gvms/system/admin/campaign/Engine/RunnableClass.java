package com.mobinil.gvms.system.admin.campaign.Engine;




import com.mobinil.gvms.system.utility.*;

import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;

import org.apache.myfaces.custom.fileupload.UploadedFile;


public class RunnableClass extends Thread
{
  
  UploadedFile uploadedFile;
  Integer giftId;
  Integer campaignId;
  Integer voucherStatus;  
  String campaignName = "";
  Integer giftTypeId; 
  public Integer counter;

  public RunnableClass(UploadedFile uploadedFile, Integer giftId, Integer campaignId, 
                       Integer voucherStatus)
  {

    this.uploadedFile = uploadedFile;
    this.giftId = giftId;
    this.campaignId = campaignId;
    this.voucherStatus = voucherStatus;
    
  }

  public RunnableClass(Integer campaignId, Integer voucherStatus)
  {
    this.campaignId = campaignId;
    this.voucherStatus = voucherStatus;
  }


  public void run()
  {
     Connection conn = null;
     try
   {
      conn = DBConnection.getConnectionForThread ( );
   
   
        if (voucherStatus == 4)
          {
            PreparedStatement ps = null;
            
            ResultSet campaignVouchers = 
              ExecuteQueries.executeQuery(conn,"Select * from GVMS_CURRENT_VOUCHER where CAMPAIGN_ID=" + 
                                          campaignId);
             
            
            try
              {
                //    
                 FileLogger.logString("date b4 insert to temp_current is " + new Date());
                String SQL = "insert into GVMS_TEMP_CURRENT (CURRENT_VOUCHER_ID,CURRENT_VOUCHER_DIAL_NUMBER,CURRENT_VOUCHER_NUMBER,CAMPAIGN_ID,VOUCHER_STATUS_ID,GIFT_ID,CURRENT_VOUCHER_END_DATE,GIFT_VALUE,USER_TYPE_ID,USER_ID,GIFT_TYPE_ID) values (?,?,?,"+campaignId+",?,?,?,?,?,?,?)";
              
                ps = conn.prepareStatement(SQL);
                while (campaignVouchers.next())
                  {

        //                ExecuteQueries.executeNoneQuery("insert into GVMS_TEMP_CURRENT (CURRENT_VOUCHER_ID,CURRENT_VOUCHER_DIAL_NUMBER,CURRENT_VOUCHER_NUMBER,CAMPAIGN_ID,VOUCHER_STATUS_ID,GIFT_ID,CURRENT_VOUCHER_END_DATE,GIFT_VALUE,USER_TYPE_ID,USER_ID,GIFT_TYPE_ID) values " +
        //                                                "(" +
        //                                                 +
        //                                                "," +
        //                                                 +
        //                                                "," +
        //                                                campaignVouchers.getInt("CURRENT_VOUCHER_NUMBER") +
        //                                                "," + campaignId + "," +
        //                                                campaignVouchers.getInt("VOUCHER_STATUS_ID") +
        //                                                "," + campaignVouchers.getInt("GIFT_ID") + "," +
        //                                                "to_date('" +
        //                                                campaignVouchers.getDate("CURRENT_VOUCHER_END_DATE") +
        //                                                "','YYYY-MM-DD')," +
        //                                                campaignVouchers.getInt("GIFT_VALUE") + "," +
        //                                                campaignVouchers.getInt("USER_TYPE_ID") + "," +
        //                                                campaignVouchers.getInt("USER_ID") + "," +
        //                                                campaignVouchers.getInt("GIFT_TYPE_ID") + ")");

                    ps.setInt(1,campaignVouchers.getInt("CURRENT_VOUCHER_ID"));
                    ps.setInt(2,campaignVouchers.getInt("CURRENT_VOUCHER_DIAL_NUMBER"));
                    ps.setInt(3,campaignVouchers.getInt("CURRENT_VOUCHER_NUMBER"));
                    ps.setInt(4,campaignVouchers.getInt("VOUCHER_STATUS_ID"));
                    ps.setInt(5,campaignVouchers.getInt("GIFT_ID"));
                    ps.setDate(6,campaignVouchers.getDate("CURRENT_VOUCHER_END_DATE"));
                    ps.setInt(7,campaignVouchers.getInt("GIFT_VALUE"));
                    ps.setInt(8,campaignVouchers.getInt("USER_TYPE_ID"));
                    ps.setInt(9,campaignVouchers.getInt("USER_ID"));
                    ps.setInt(10,campaignVouchers.getInt("GIFT_TYPE_ID"));
                    ps.executeUpdate();
                    
                  }
                FileLogger.logString("date after insert to temp_current is " + new Date());
                FileLogger.logString("date b4 delete from current voucher is " + new Date());
                
                ExecuteQueries.executeNoneQuery("Delete from GVMS_CURRENT_VOUCHER where CAMPAIGN_ID=" + 
                                                campaignId);
                FileLogger.logString("date after delete from current voucher is " + new Date());
                campaignVouchers.getStatement().close();
                campaignVouchers.close();

              }
            catch (SQLException e)
              {
              new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }


          }
        ResultSet giftTypeRS = 
            ExecuteQueries.executeQuery(conn,"select GIFT_TYPE_ID from GVMS_GIFT where GIFT_ID =" + 
                                        giftId);
          try
            {
              if (giftTypeRS.next())
                {

                  giftTypeId = giftTypeRS.getInt("CAMPAIGN_NAME");

                }
              giftTypeRS.getStatement().close();
              giftTypeRS.close();
            }
          catch (SQLException e)
            {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
            }

    if (voucherStatus == 3)
      {

        //        GenerateVoucherNumbersAgent aa = new GenerateVoucherNumbersAgent();
        //        aa.generateVoucherNumbers(campaignId, voucherStatus, giftId);

        ResultSet campaigNameRS = 
          ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME FROM GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                      campaignId);
        try
          {
            if (campaigNameRS.next())
              {

                campaignName = campaigNameRS.getString("CAMPAIGN_NAME");

              }
            campaigNameRS.getStatement().close();
            campaigNameRS.close();
          }
        catch (SQLException e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
        try
        {
          insertListDialGenerate(uploadedFile.getInputStream(),campaignId, giftId,giftTypeId);
        }
        catch (IOException e)
        {
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }


      }
    if (voucherStatus == 2)
      {
        try
          {            
            ResultSet campaigNameRS = 
              ExecuteQueries.executeQuery(conn,"Select CAMPAIGN_NAME FROM GVMS_CAMPAIGN where CAMPAIGN_ID=" + 
                                          campaignId);
            try
              {
                if (campaigNameRS.next())
                  {

                    campaignName = campaigNameRS.getString("CAMPAIGN_NAME");

                  }
                campaigNameRS.getStatement().close();
                campaigNameRS.close();
              }
            catch (SQLException e)
              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }
            insertListVoucherDial(uploadedFile.getInputStream(), campaignId, giftId,giftTypeId);
          }
        catch (IOException e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }
    if (voucherStatus == 1)
      {
        try
        {
          insertListDial(uploadedFile.getInputStream(), campaignId, giftId,giftTypeId);
        }
        catch (IOException e)
        {
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
      }
    DBConnection.closeThreadConnections ( conn );
  }
    catch ( SQLException e1 )
    {
       // TODO Auto-generated catch block
new com.mobinil.gvms.system.utility.PrintException().printException(e1);    }

  }

  public void insertListDialGenerate(InputStream dd, int camp_id, int gift_id,Integer giftTypeId)
  {
    importFile.insertListDialGenerate(dd,"Integer",20, camp_id, gift_id,giftTypeId);

  }

  public void insertListVoucherDial(InputStream dd, int camp_id, int gift_id,Integer giftTypeId)
  {
    importFile.insertListVoucherDial(dd,"Integer",20, camp_id, gift_id,giftTypeId);




  }


  public void insertListDial(InputStream dd, Integer camp_id, Integer gift_id,Integer giftTypeId)
  {

 importFile.insertListDial(dd,"Integer",20, camp_id, gift_id,giftTypeId);

  }


  

  public void setcounter(Integer dD)
  {
    this.counter = dD;
  }

  public Integer getcounter()
  {
    return counter;
  }

  public int getCounter()
  {
    return this.counter;
  }
}