package com.mobinil.gvms.system.admin.campaign.DAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.mobinil.gvms.system.admin.campaign.DTO.VoucherClass;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;

public class VoucherDAO {
public static ArrayList<VoucherClass> fillVoucherTable (String selectedValueRedeemed, int campaignVoucherId, Integer campaignId,boolean flag) throws SQLException {
	ArrayList<VoucherClass> voucherArray  = new ArrayList<VoucherClass>();
	String SQL = "";
	  String dialNumber = "";
	  String selectVoucherStatus = "";
	  selectVoucherStatus = selectedValueRedeemed;
	  
	  if (campaignVoucherId == 2 || campaignVoucherId == 3) dialNumber = "t1.CURRENT_VOUCHER_DIAL_NUMBER,";
	    
	  
	  SQL = 
        "select "+dialNumber+"t1.CURRENT_VOUCHER_NUMBER,t2.VOUCHER_STATUS from GVMS_CURRENT_VOUCHER t1 left join GVMS_VOUCHER_STATUS t2 on t1.VOUCHER_STATUS_ID = t2.VOUCHER_STATUS_ID where t1.voucher_Status_id not in (5) and t1.CAMPAIGN_ID=" + 
        campaignId;
	  
	  if (flag){
	  if (selectedValueRedeemed.compareTo("*") != 0)
	    {
	      selectVoucherStatus = 
	          " and t1.voucher_Status_id=" + selectedValueRedeemed;
	      SQL += selectVoucherStatus;
	    }
	  }
  //System.out.println(SQL);
  Connection conn = DBConnection.getConnection();
  ResultSet vouchersRS = ExecuteQueries.executeQuery(conn,SQL);



    while (vouchersRS.next())
    {

      VoucherClass vc = new VoucherClass();
      vc.setVoucherNumber(vouchersRS.getLong("CURRENT_VOUCHER_NUMBER"));

      if (campaignVoucherId == 2 || campaignVoucherId == 3)
      {

        vc.setDial(vouchersRS.getString("CURRENT_VOUCHER_DIAL_NUMBER"));
      }

      vc.setVoucherStatus(vouchersRS.getString("VOUCHER_STATUS"));
      voucherArray.add(vc);
    }
    DBConnection.cleanResultSet(vouchersRS);
    DBConnection.closeConnections ( conn );

return voucherArray;
	
}
}
