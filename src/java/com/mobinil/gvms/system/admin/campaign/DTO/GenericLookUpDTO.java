package com.mobinil.gvms.system.admin.campaign.DTO;

public class GenericLookUpDTO
{
public String id;
  public String name;
  public GenericLookUpDTO()
  {
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public String getId()
  {
    return id;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }
}
