package com.mobinil.gvms.system.admin.reports.DTO;

import java.util.Date;

public class CampaignReportDTO
{

    public Integer id;
    public Integer campainId;
    public Integer giftId;
    public Integer userTypeId;
    public Integer userId;
    public Integer redeemedCount;
    public Integer totalCost;
    public Integer hour;
    public String date;
    public String dayOfWeek;
    public String dayOfMonth;
    public String campaignName;
    public String giftName;
    public String userTypeName;
    public String typeName;
    public String statusName;
    public String userName;
    public String posCode;
    public String giftType;
    public String giftTypeName;

    public String typeId;/**/
    public String wsCampainId;/**/
    public String wsGiftId;/**/
    public String statusId;/**/
    public String dialNumber;/**/
    public String comment;/**/
    public String actionDate;/**/
    public Date wsFromDate;
    public Date wsToDate;

    public Double giftValue;
    public Double countGiftValue;





    public CampaignReportDTO()
    {

    }

    public void setId(Integer id)
    {
      this.id = id;
    }

    public Integer getId()
    {
      return id;
    }

    public void setCampainId(Integer campainId)
    {
      this.campainId = campainId;
    }

    public Integer getCampainId()
    {
      return campainId;
    }

    public void setGiftId(Integer giftId)
    {
      this.giftId = giftId;
    }

    public Integer getGiftId()
    {
      return giftId;
    }

    public void setUserTypeId(Integer userTypeId)
    {
      this.userTypeId = userTypeId;
    }

    public Integer getUserTypeId()
    {
      return userTypeId;
    }

    public void setUserId(Integer userId)
    {
      this.userId = userId;
    }

    public Integer getUserId()
    {
      return userId;
    }

    public void setRedeemedCount(Integer redeemedCount)
    {
      this.redeemedCount = redeemedCount;
    }

    public Integer getRedeemedCount()
    {
      return redeemedCount;
    }

    public void setTotalCost(Integer totalCost)
    {
      this.totalCost = totalCost;
    }

    public Integer getTotalCost()
    {
      return totalCost;
    }

    public void setHour(Integer hour)
    {
      this.hour = hour;
    }

    public Integer getHour()
    {
      return hour;
    }


    public void setDayOfWeek(String dayOfWeek)
    {
      this.dayOfWeek = dayOfWeek;
    }

    public String getDayOfWeek()
    {
      return dayOfWeek;
    }

    public void setDayOfMonth(String dayOfMonth)
    {
      this.dayOfMonth = dayOfMonth;
    }

    public String getDayOfMonth()
    {
      return dayOfMonth;
    }

    public void setCampaignName(String campaignName)
    {
      this.campaignName = campaignName;
    }

    public String getCampaignName()
    {
      return campaignName;
    }

    public void setGiftName(String giftName)
    {
      this.giftName = giftName;
    }

    public String getGiftName()
    {
      return giftName;
    }

    public void setUserTypeName(String userTypeName)
    {
      this.userTypeName = userTypeName;
    }

    public String getUserTypeName()
    {
      return userTypeName;
    }

    public void setUserName(String userName)
    {
      this.userName = userName;
    }

    public String getUserName()
    {
      return userName;
    }

    public void setDate(String date)
    {
      this.date = date;
    }

    public String getDate()
    {
      return date;
    }


    


    public void setGiftType(String giftType)
    {
      this.giftType = giftType;
    }

    public String getGiftType()
    {
      return giftType;
    }

    public void setGiftTypeName(String giftTypeName)
    {
      this.giftTypeName = giftTypeName;
    }

    public String getGiftTypeName()
    {
      return giftTypeName;
    }

	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	public String getPosCode() {
		return posCode;
	}

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the wsCampainId
     */
    public String getWsCampainId() {
        return wsCampainId;
    }

    /**
     * @param wsCampainId the wsCampainId to set
     */
    public void setWsCampainId(String wsCampainId) {
        this.wsCampainId = wsCampainId;
    }

    /**
     * @return the wsGiftId
     */
    public String getWsGiftId() {
        return wsGiftId;
    }

    /**
     * @param wsGiftId the wsGiftId to set
     */
    public void setWsGiftId(String wsGiftId) {
        this.wsGiftId = wsGiftId;
    }

    /**
     * @return the statusId
     */
    public String getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the dialNumber
     */
    public String getDialNumber() {
        return dialNumber;
    }

    /**
     * @param dialNumber the dialNumber to set
     */
    public void setDialNumber(String dialNumber) {
        this.dialNumber = dialNumber;
    }

    /**
     * @return the wsFromDate
     */
    public Date getWsFromDate() {
        return wsFromDate;
    }

    /**
     * @param wsFromDate the wsFromDate to set
     */
    public void setWsFromDate(Date wsFromDate) {
        this.wsFromDate = wsFromDate;
    }

    /**
     * @return the wsToDate
     */
    public Date getWsToDate() {
        return wsToDate;
    }

    /**
     * @param wsToDate the wsToDate to set
     */
    public void setWsToDate(Date wsToDate) {
        this.wsToDate = wsToDate;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the TypeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param TypeName the TypeName to set
     */
    public void setTypeName(String TypeName) {
        this.typeName = TypeName;
    }

    /**
     * @return the statusName
     */
    public String getStatusName() {
        return statusName;
    }

    /**
     * @param statusName the statusName to set
     */
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    /**
     * @return the giftValue
     */
    public Double getGiftValue() {
        return giftValue;
    }

    /**
     * @param giftValue the giftValue to set
     */
    public void setGiftValue(Double giftValue) {
        this.giftValue = giftValue;
    }

    /**
     * @return the countGiftValue
     */
    public Double getCountGiftValue() {
        return countGiftValue;
    }

    /**
     * @param countGiftValue the countGiftValue to set
     */
    public void setCountGiftValue(Double countGiftValue) {
        this.countGiftValue = countGiftValue;
    }

    /**
     * @return the actionDate
     */
    public String getActionDate() {
        return actionDate;
    }

    /**
     * @param actionDate the actionDate to set
     */
    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }
  }
