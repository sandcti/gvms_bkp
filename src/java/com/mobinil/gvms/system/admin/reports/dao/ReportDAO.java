/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.gvms.system.admin.reports.dao;

import com.mobinil.gvms.system.admin.reports.DTO.CampaignReportDTO;
import com.mobinil.gvms.system.utility.db.DBUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mabdelaal
 */
public class ReportDAO {

    public static HashMap<String, String> getLookupTable(Connection con, String tableName, String fields, String conditions) {
        conditions += "1=1";
        String sql = "select " + fields + " from " + tableName + " where " + conditions;
        return DBUtil.getMap(con, sql);


    }

    public static Double calcGiftValues(ArrayList<CampaignReportDTO> filteredValue) {
        Double countGifts = 0D;
        if (filteredValue != null && !filteredValue.isEmpty()) {
            for (CampaignReportDTO campaignReportDTO : filteredValue) {
                countGifts += campaignReportDTO.getGiftValue();
            }
        }
        return countGifts;

    }

    public static ArrayList<CampaignReportDTO> filterWebServiceLogs(Connection con, CampaignReportDTO filter) {
        ArrayList<CampaignReportDTO> result = new ArrayList<CampaignReportDTO>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(buildWSSql(filter));
            CampaignReportDTO newRecord = null;
            while (rs.next()) {
                newRecord = new CampaignReportDTO();
                newRecord.setActionDate(rs.getString("act_date"));
                newRecord.setComment(rs.getString("COMMENTS"));
                newRecord.setDialNumber(rs.getString("DIAL_NUMBER"));
                newRecord.setCampaignName(rs.getString("CAMPAIGN_NAME"));
                newRecord.setStatusName(rs.getString("STATUS_NAME"));
                newRecord.setTypeName(rs.getString("TYPE_NAME"));
                newRecord.setGiftName(rs.getString("GIFT_NAME"));
                try {
                    newRecord.setGiftValue(Double.parseDouble(rs.getString("GIFT_VALUE")));
                } catch (Exception e) {
                    newRecord.setGiftValue(0d);
                    new com.mobinil.gvms.system.utility.PrintException().printException(e);
                }



                result.add(newRecord);
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            new com.mobinil.gvms.system.utility.PrintException().printException(ex);
        }

        return result;
    }

    public static String buildWSSql(CampaignReportDTO filter) {

        StringBuilder sql = new StringBuilder("");
        sql.append(" SELECT to_char(ACTION_DATE,'Mon dd,yyyy') act_date , COMMENTS, DIAL_NUMBER,CAMPAIGN_NAME,STATUS_NAME,TYPE_NAME, GIFT_NAME , GIFT_VALUE ");
        sql.append(" FROM GVMS_WS_LOGS WL, GVMS_CAMPAIGN GC , GVMS_WS_STATUS WS , GVMS_WS_TYPE WT, GVMS_GIFT GG ");
        sql.append(" WHERE  ");
        sql.append(" WL.CAMPAIGN_ID = GC.CAMPAIGN_ID (+) ");
        sql.append(" AND  ");
        sql.append(" WL.STATUS_ID = WS.STATUS_ID ");
        sql.append(" AND ");
        sql.append(" WL.TYPE_ID = WT.TYPE_ID ");
        sql.append(" AND  ");
        sql.append(" WL.GIFT_ID = GG.GIFT_ID (+)   ");
        if (checkNullString(filter.getWsCampainId())) {
            sql.append(" AND GC.CAMPAIGN_NAME='");
            sql.append(filter.getWsCampainId());
            sql.append("'");
        }
        if (checkNullString(filter.getWsGiftId())) {
            sql.append(" AND GG.GIFT_NAME='");
            sql.append(filter.getWsGiftId());
            sql.append("'");
        }
//        if (checkNullString(filter.getWsGiftId())) {
//            sql.append(" AND WL.GIFT_ID='");
//            sql.append(filter.getWsGiftId());
//            sql.append("'");
//        }
        if (checkNullString(filter.getStatusId())) {
            sql.append(" AND WL.STATUS_ID='");
            sql.append(filter.getStatusId());
            sql.append("'");
        }
        if (checkNullString(filter.getTypeId())) {
            sql.append(" AND WL.TYPE_ID='");
            sql.append(filter.getTypeId());
            sql.append("'");
        }
        if (checkNullString(filter.getDialNumber())) {
            sql.append(" AND WL.DIAL_NUMBER like ('%");
            sql.append(filter.getDialNumber());
            sql.append("%')");
        }
        if (filter.getWsFromDate() != null) {
            sql.append(" AND WL.ACTION_DATE >= ");
            sql.append(convertDate(filter.getWsFromDate(), "from"));
        }
        if (filter.getWsToDate() != null) {
            sql.append(" AND WL.ACTION_DATE <= ");
            sql.append(convertDate(filter.getWsToDate(), "to"));
        }
        sql.append(" order by WL.ACTION_DATE");
        //System.out.println("ws sql iss " + sql);
        return sql.toString();
    }

    public static String convertDate(Date undefineDate, String type) {
        String values = "";
        if (undefineDate == null) {

            String[] toDate = new String[3];
            toDate[0] = "00";
            toDate[1] = "00";
            toDate[2] = "0000";
            values = "to_date('" + toDate[1] + "-" + toDate[0] + "-" + toDate[2] + "', 'dd-mm-yyyy')";
            return values;
        } else {
            int day = undefineDate.getDate();
            int year = undefineDate.getYear() + 1900;
            int month = undefineDate.getMonth() + 1;
            if (type.compareTo("from") == 0) {
                values = "to_date('" + day + "/" + month + "/" + year + " 00:00:00', 'dd/mm/yyyy HH24:MI:SS')";
            }
            if (type.compareTo("to") == 0) {
                values = "to_date('" + day + "/" + month + "/" + year + " 23:59:59', 'dd/mm/yyyy HH24:MI:SS')";
            }
            // values = "to_date('" + day + "-" + month + "-" + year + "', 'dd-mm-yyyy')";

            return values;
        }
    }

    public static boolean checkNullString(String filterStr) {
        return filterStr != null && filterStr.compareTo("") != 0 && filterStr.compareTo("-1") != 0 && filterStr.compareTo("*") != 0;
    }
}
