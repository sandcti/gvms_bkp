package com.mobinil.gvms.system.admin.reports.DTO;

import java.sql.Timestamp;

public class VoucherReportDTO
{
  public String dialNumber;
  public Integer voucherNumber;
  public String status;
  public Integer redeemcount;
  public String giftName;
  public Integer giftValue;
  public String campaignName;
  public String accountNumber;
  public String channel;
  public String subChannel;
  public String personName;
  public Timestamp creationDate;
  public Timestamp redemptionDate;
  public Timestamp expiryDate;
  private Timestamp deletedDate;

  public VoucherReportDTO()
  {
  }


  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }


  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }


  public void setCampaignName(String campaignName)
  {
    this.campaignName = campaignName;
  }

  public String getCampaignName()
  {
    return campaignName;
  }


  public void setVoucherNumber(Integer voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public Integer getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setGiftValue(Integer giftValue)
  {
    this.giftValue = giftValue;
  }

  public Integer getGiftValue()
  {
    return giftValue;
  }

  public void setRedeemcount(Integer redeemcount)
  {
    this.redeemcount = redeemcount;
  }

  public Integer getRedeemcount()
  {
    return redeemcount;
  }

  public void setDialNumber(String dialNumber)
  {
    this.dialNumber = dialNumber;
  }

  public String getDialNumber()
  {
    return dialNumber;
  }


public void setSubChannel(String subChannel)
{
   this.subChannel = subChannel;
}


public String getSubChannel()
{
   return subChannel;
}


public void setChannel(String channel)
{
   this.channel = channel;
}


public String getChannel()
{
   return channel;
}


public void setAccountNumber(String accountNumber)
{
   this.accountNumber = accountNumber;
}


public String getAccountNumber()
{
   return accountNumber;
}


public void setCreationDate(Timestamp creationDate)
{
   this.creationDate = creationDate;
}


public Timestamp getCreationDate()
{
   return creationDate;
}


public void setRedemptionDate(Timestamp redemptionDate)
{
   this.redemptionDate = redemptionDate;
}


public Timestamp getRedemptionDate()
{
   return redemptionDate;
}


public void setExpiryDate(Timestamp expiryDate)
{
   this.expiryDate = expiryDate;
}


public Timestamp getExpiryDate()
{
   return expiryDate;
}

    /**
     * @return the personName
     */
    public String getPersonName() {
        return personName;
    }

    /**
     * @param personName the personName to set
     */
    public void setPersonName(String personName) {
        this.personName = personName;
    }

    /**
     * @return the deletedDate
     */
    public Timestamp getDeletedDate() {
        return deletedDate;
    }

    /**
     * @param deletedDate the deletedDate to set
     */
    public void setDeletedDate(Timestamp deletedDate) {
        this.deletedDate = deletedDate;
    }
  }
