package com.mobinil.gvms.system.admin.reports.DTO;
public class TimeChartDTO
{
  public String giftValue;
  public Integer time;
  public String giftCount;
  public String dayOfWeekName;

  public TimeChartDTO()
  {
  }


  public void setGiftValue(String giftValue)
  {
    this.giftValue = giftValue;
  }

  public String getGiftValue()
  {
    return giftValue;
  }

  public void setTime(Integer time)
  {
    this.time = time;
  }

  public Integer getTime()
  {
    return time;
  }


  public void setDayOfWeekName(String dayOfWeekName)
  {
    this.dayOfWeekName = dayOfWeekName;
  }

  public String getDayOfWeekName()
  {
    return dayOfWeekName;
  }

  public void setGiftCount(String giftCount)
  {
    this.giftCount = giftCount;
  }

  public String getGiftCount()
  {
    return giftCount;
  }
}
