package com.mobinil.gvms.system.admin.usertype;

public class UserTypeDTO {
    public String name;
    public String description;
    public String status;
    public Integer id;
    public Integer statusId;
    public Boolean selected;

    public UserTypeDTO()
    {

    }

    public void setName(String name)
    {
      this.name = name;
    }

    public String getName()
    {
      return name;
    }

    public void setDescription(String description)
    {
      this.description = description;
    }

    public String getDescription()
    {
      return description;
    }

    public void setStatus(String status)
    {
      this.status = status;
    }

    public String getStatus()
    {
      return status;
    }

    public void setId(Integer id)
    {
      this.id = id;
    }

    public Integer getId()
    {
      return id;
    }

    public void setSelected(Boolean selected)
    {
      this.selected = selected;
    }

    public Boolean getSelected()
    {
      return selected;
    }

    public void setStatusId(Integer statusId)
    {
      this.statusId = statusId;
    }

    public Integer getStatusId()
    {
      return statusId;
    }
    }
