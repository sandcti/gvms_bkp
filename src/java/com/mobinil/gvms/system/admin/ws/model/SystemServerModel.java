/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mobinil.gvms.system.admin.ws.model;

import com.mobinil.gvms.system.utility.MainModel;
import java.sql.ResultSet;

/**
 *
 * @author mabdelaal
 */
public class SystemServerModel extends MainModel {
    public String campaignId;
    public String campaignName;
    public String userName;
    public String password;
    public Boolean selected;

    @Override
    public void fillInstance(ResultSet res) {
        try {
           setCampaignId(res.getString("CAMPAIGN_ID"));
           setPassword(res.getString("SERV_PASSWORD"));
           setUserName(res.getString("SERV_USER_NAME"));
           setCampaignName(res.getString("CAMPAIGN_NAME"));
           setSelected(false);
        } catch (Exception e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        
    }

    /**
     * @return the campaignId
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the selected
     */
    public Boolean getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the campaignName
     */
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * @param campaignName the campaignName to set
     */
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

}
