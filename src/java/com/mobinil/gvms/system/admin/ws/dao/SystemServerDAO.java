/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.gvms.system.admin.ws.dao;

import com.mobinil.gvms.system.admin.ws.model.SystemServerModel;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.db.DBUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mabdelaal
 */
public class SystemServerDAO {

    public static ArrayList<SystemServerModel> getAllServers() {
        Connection con = null;
        ArrayList<SystemServerModel> getAllServersVec = null;
        try {
            con = DBConnection.getConnection();
            getAllServersVec = DBUtil.executeSqlQueryMultiValueAL("select GVMS_SYSTEM_SERVER.CAMPAIGN_ID, SERV_USER_NAME, SERV_PASSWORD, GVMS_CAMPAIGN.CAMPAIGN_NAME from GVMS_SYSTEM_SERVER , GVMS_CAMPAIGN where CAMPAIGN_STATUS_ID = 2 and GVMS_SYSTEM_SERVER.CAMPAIGN_ID = GVMS_CAMPAIGN.CAMPAIGN_ID", SystemServerModel.class, con);
            DBConnection.closeConnection(con);
        } catch (SQLException ex) {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);        }
        return getAllServersVec;

    }

    public static void deleteServer(String campiagnId, String userName, String password, Statement st) {
        String sql = "delete from GVMS_SYSTEM_SERVER where CAMPAIGN_ID='" + campiagnId + "' and SERV_USER_NAME='" + userName + "' and SERV_PASSWORD='" + password + "'";
        try {
            DBUtil.executeSQL(sql, st);
        } catch (SQLException ex) {
           new com.mobinil.gvms.system.utility.PrintException().printException(ex);
        }

    }

    public static HashMap<String, String> getAllCampaigns() {
        HashMap<String, String> campiagns = new HashMap<String, String>();
        try {
            Connection con = DBConnection.getConnection();
            campiagns = DBUtil.getMap(con, "select campaign_id,campaign_name from GVMS_CAMPAIGN where CAMPAIGN_STATUS_ID = 2");
            DBConnection.closeConnection(con);
        } catch (SQLException ex) {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);        }
        return campiagns;
    }

    public static Boolean checkDublicatedServerName(String name, String password,String campId) {
        boolean returnValue = false;
        try {


            Connection conn = DBConnection.getConnection();
            String sql = "select * from GVMS_SYSTEM_SERVER where SERV_USER_NAME='"+name+"' and SERV_PASSWORD='"+password+"' and CAMPAIGN_ID='"+campId+"'";
            returnValue = DBUtil.executeSQLExistCheck(sql, conn);
            DBConnection.closeConnection(conn);
        } catch (Exception e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return returnValue;
    }

    public static int updateServer(SystemServerModel newSystemServerModel, SystemServerModel oldSystemServerModel) {
int resultInt = 0;
        try {
            Connection conn = DBConnection.getConnection();
            String sql = "update GVMS_SYSTEM_SERVER set SERV_USER_NAME='" + newSystemServerModel.getUserName() + "' , SERV_PASSWORD='" + newSystemServerModel.getPassword() + "' , CAMPAIGN_ID='" + newSystemServerModel.getCampaignId()
                    + "' where SERV_USER_NAME='" + oldSystemServerModel.getUserName() + "' and SERV_PASSWORD='" + oldSystemServerModel.getPassword() + "' and CAMPAIGN_ID='" + oldSystemServerModel.getCampaignId() + "'";
            //System.out.println(sql);
           resultInt = DBUtil.executeSQL(sql, conn);
            DBConnection.closeConnection(conn);
        } catch (Exception e) {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return resultInt;
    }

    public static int insertServer(SystemServerModel newSystemServerModel) {
int resultInt = 0;
        try {
            Connection conn = DBConnection.getConnection();
            String sql = "insert into GVMS_SYSTEM_SERVER (CAMPAIGN_ID,SERV_USER_NAME,SERV_PASSWORD) values ('" + newSystemServerModel.getCampaignId() + "','" + newSystemServerModel.getUserName() + "','" + newSystemServerModel.getPassword() + "')";
            //System.out.println(sql);
           resultInt= DBUtil.executeSQL(sql, conn);
            DBConnection.closeConnection(conn);
        } catch (Exception e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return resultInt;

    }
}
