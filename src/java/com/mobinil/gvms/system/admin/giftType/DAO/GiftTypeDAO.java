package com.mobinil.gvms.system.admin.giftType.DAO;

import com.mobinil.gvms.system.admin.giftType.DTO.GiftTypeDTO;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.DateClass;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.UpdateClass;
import com.mobinil.gvms.system.utility.db.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class GiftTypeDAO {
    public GiftTypeDAO() {
    }
    public static ArrayList<GiftTypeDTO> getGiftTypeInfo() throws SQLException {
        ArrayList<GiftTypeDTO> agto = new ArrayList<GiftTypeDTO>();
        Connection conn = DBConnection.getConnection();
        String SQL = 
          "Select   t1.GIFT_TYPE_ID,t1.GIFT_TYPE_STATUS_ID,t1.GIFT_TYPE_NAME,t1.GIFT_TYPE_DESCRIPTION,t1.GIFT_TYPE_CREATION,t1.GIFT_CREATED_BY,t2.Gift_Type_STATUS from GVMS_Gift_Type t1 left join GVMS_Gift_Type_STATUS t2 on  t1.Gift_Type_STATUS_ID =  t2.Gift_Type_STATUS_ID where t1.gift_type_status_id!=3 order by t1.GIFT_TYPE_ID";
        ResultSet GiftTypeRS = ExecuteQueries.executeQuery(conn,SQL);
        

            while (GiftTypeRS.next())
              {
                int giftId = GiftTypeRS.getInt("GIFT_TYPE_ID");
                GiftTypeDTO g = new GiftTypeDTO();

                g.setGiftTypeDescription(GiftTypeRS.getString("GIFT_TYPE_DESCRIPTION"));
                g.setGiftTypeName(GiftTypeRS.getString("GIFT_TYPE_NAME"));
                g.setGiftTypeStatus(GiftTypeRS.getString("Gift_Type_STATUS"));
                g.setGiftTypeId(giftId);
                g.setCreatedBy(GiftTypeRS.getString("GIFT_CREATED_BY"));
                //System.out.println("GiftTypeCreationDate" + GiftTypeRS.getDate("GIFT_TYPE_CREATION"));
                g.setCreation(GiftTypeRS.getDate("GIFT_TYPE_CREATION").toString());
                g.setGiftTypeStatusId(GiftTypeRS.getInt("GIFT_TYPE_STATUS_ID"));
                  agto.add(g);
              }
        GiftTypeRS.getStatement().close();
        GiftTypeRS.close();
        DBConnection.closeConnections ( conn );
        return agto;
    }
    public static int deleteGiftType(Integer id) throws SQLException {
       return  ExecuteQueries.executeNoneQuery("update GVMS_GIFT_TYPE set GIFT_TYPE_STATUS_ID=3 where GIFT_TYPE_ID=" + 
                                        id);
    }
    
    public static String getActiveGiftTypes(Integer id) throws SQLException {
       Connection conn = DBConnection.getConnection();
        ResultSet giftRS = 
          ExecuteQueries.executeQuery(conn,"Select GIFT_STATUS_ID from GVMS_GIFT where GIFT_TYPE_ID=" + 
                                      id);

        
            String activeGifts = "";

            while (giftRS.next())
              {

                if (giftRS.getInt("GIFT_STATUS_ID") == 1)
                  {
                    activeGifts += giftRS.getInt("GIFT_STATUS_ID");
                  }
              }
            giftRS.getStatement().close();
            giftRS.close();
            DBConnection.closeConnections ( conn );
            return activeGifts;
    }
    public static String checkDublicatedGiftTypeName(String name,Integer id) throws SQLException {
       Connection conn = DBConnection.getConnection();
    String def = "";
    if (id!=0)
    {
    def=" and GIFT_TYPE_ID!=" + id;
    }
        ResultSet repeatGiftType = 
          ExecuteQueries.executeQuery(conn,"Select GIFT_TYPE_NAME from GVMS_GIFT_TYPE where GIFT_TYPE_NAME='" + 
                                      name + "'"+def);String dd;
if (repeatGiftType.next())                                      
{
dd= "this gift type already exists.";
}
else dd="";
        repeatGiftType.getStatement().close();
        repeatGiftType.close();  
        DBConnection.closeConnections ( conn );
        return dd;             
                                      
                                      
    }
    public static int updateGiftType(String name,String status, String creatorName, String desc, Integer  GiftTypeId ) throws SQLException {

        Connection con = DBConnection.getConnectionForThread();
        int rowCount = DBUtil.executeSQL("update GVMS_Gift_Type set Gift_Type_NAME='" +
                                        name + 
                                        "',GIFT_TYPE_STATUS_ID=" + status + 
                                        ",GIFT_MODIFIED_BY='" + creatorName + 
                                        "' ,GIFT_MODIFIED_DATE=" + 
                                        new DateClass().convertDate(new DateClass().getDateTime()) + 
                                        ",Gift_Type_DESCRIPTION='" + 
                                        desc + 
                                        "' " + "where Gift_Type_ID =" + GiftTypeId,con);
        UpdateClass.updateGiftTypeInfo(GiftTypeId+"", true);
        
        return rowCount;

    }
    public static int insrtGiftType(String name,String status, String creatorName, String desc) throws SQLException {
        Connection con = DBConnection.getConnectionForThread();
        String seqGiftTypeId = DBConnection.getSequenceNextVal(con, "GVMS_GIFT_TYPE_SEQ_ID");
    String SQL = 
        "insert into GVMS_GIFT_TYPE(GIFT_TYPE_ID,GIFT_TYPE_NAME,GIFT_TYPE_DESCRIPTION,GIFT_TYPE_STATUS_ID,GIFT_TYPE_CREATION,GIFT_CREATED_BY) values(" + 
        seqGiftTypeId+",'" + name +
        "','" + desc + "'," + status + "," + 
        new DateClass().convertDate(new DateClass().getDateTime()) + ",'" + 
        creatorName + "')";
    int rowsCount = DBUtil.executeSQL(SQL, con);
    UpdateClass.updateGiftTypeInfo(seqGiftTypeId+"", false);
    DBConnection.closeThreadConnections(con);
        return rowsCount;
    }
}


