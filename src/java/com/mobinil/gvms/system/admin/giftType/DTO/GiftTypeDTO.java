package com.mobinil.gvms.system.admin.giftType.DTO;

public class GiftTypeDTO {
    public String giftTypeName;
    public String giftTypeDescription;
    public String createdBy;
    public String creation;
    public String giftTypeStatus;
    public Integer giftTypeStatusId;
    public Boolean selected;

    public Integer giftTypeId;


    public GiftTypeDTO()
    {
    }


    public void setCreatedBy(String createdBy)
    {
      this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
      return createdBy;
    }


    public void setGiftTypeName(String giftTypeName)
    {
      this.giftTypeName = giftTypeName;
    }

    public String getGiftTypeName()
    {
      return giftTypeName;
    }

    public void setGiftTypeDescription(String giftTypeDescription)
    {
      this.giftTypeDescription = giftTypeDescription;
    }

    public String getGiftTypeDescription()
    {
      return giftTypeDescription;
    }

    public void setGiftTypeStatus(String giftTypeStatus)
    {
      this.giftTypeStatus = giftTypeStatus;
    }

    public String getGiftTypeStatus()
    {
      return giftTypeStatus;
    }

    public void setGiftTypeId(Integer giftTypeId)
    {
      this.giftTypeId = giftTypeId;
    }

    public Integer getGiftTypeId()
    {
      return giftTypeId;
    }


    public void setSelected(Boolean selected)
    {
      this.selected = selected;
    }

    public Boolean getSelected()
    {
      return selected;
    }

    public void setCreation(String creation)
    {
      this.creation = creation;
    }

    public String getCreation()
    {
      return creation;
    }

    public void setGiftTypeStatusId(Integer giftTypeStatusId)
    {
      this.giftTypeStatusId = giftTypeStatusId;
    }

    public Integer getGiftTypeStatusId()
    {
      return giftTypeStatusId;
    }
    }
