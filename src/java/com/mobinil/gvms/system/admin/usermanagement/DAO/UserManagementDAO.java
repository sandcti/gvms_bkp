package com.mobinil.gvms.system.admin.usermanagement.DAO;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.admin.usermanagement.DTO.UserDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.utility.db.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserManagementDAO {

    public UserManagementDAO() {
    }

    public static ArrayList<UserDTO> getAdmins(String conditions) {
        ArrayList<UserDTO> userArray = new ArrayList<UserDTO>();

        try {
            Connection conn = DBConnection.getConnection();
           String SQL =
               "Select t1.*,t4.IS_LOCKED,(SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES') lockTimes,T4.SYSTEM_USER_NAME,T4.SYSTEM_USER_PASSWORD,t2.ADMIN_STATUS FROM GVMS_ADMIN t1 left join GVMS_ADMIN_STATUS t2 on t1.ADMIN_STATUS_ID = t2.ADMIN_STATUS_ID left join GVMS_SYSTEM_USER t4 on t1.SYSTEM_USER_ID = t4.SYSTEM_USER_ID where 1=1 and t4.SYSTEM_USER_TYPE='admin'";
//            StringBuilder sql = new StringBuilder("");
//            sql.append(" SELECT T1.*, T4.IS_LOCKED, (SELECT PROPERTIES ");
//            sql.append(" FROM GVMS_PROPERTIES ");
//            sql.append(" WHERE REASONE = 'LOCK_TIMES') LOCKTIMES, ");
//            sql.append(" T4.SYSTEM_USER_NAME, T4.SYSTEM_USER_PASSWORD, T2.ADMIN_STATUS ");
//            sql.append(" FROM  GVMS_SYSTEM_USER T4 LEFT JOIN GVMS_ADMIN T1 ON T1.SYSTEM_USER_ID = T4.SYSTEM_USER_ID LEFT JOIN GVMS_ADMIN_STATUS T2 ");
//            sql.append(" ON T1.ADMIN_STATUS_ID = T2.ADMIN_STATUS_ID ");
//            sql.append(" WHERE 1 = 1 ");


            if (conditions.compareTo("") != 0) {
                SQL+=conditions;
            }
            ResultSet adminRS = ExecuteQueries.executeQuery(conn, SQL.toString());
            while (adminRS.next()) {
                if (adminRS.getInt("ADMIN_STATUS_ID") != 3) {
                    UserDTO a = new UserDTO();
                    a.setId(adminRS.getInt("ADMIN_ID"));
                    a.setEmail(adminRS.getString("ADMIN_EMAIL"));
                    a.setName(adminRS.getString("ADMIN_NAME"));
                    a.setPhone(adminRS.getString("ADMIN_PHONE"));
                    a.setStatus(adminRS.getString("ADMIN_STATUS"));
                    a.setStatusId(adminRS.getInt("ADMIN_STATUS_ID"));
                    a.setSystemUserId(adminRS.getInt("SYSTEM_USER_ID"));
                    a.setPassword(adminRS.getString("SYSTEM_USER_PASSWORD"));
                    a.setUserName(adminRS.getString("SYSTEM_USER_NAME"));
                    Integer temp = Integer.parseInt(adminRS.getString("IS_LOCKED"));
                    int availableLockTimes = adminRS.getInt("lockTimes");
                    if (temp <= availableLockTimes) {
                        a.setUserLocked("Not Locked");
                    } else {
                        a.setUserLocked("Locked");
                    }


                    userArray.add(a);
                }
            }
            adminRS.getStatement().close();
            adminRS.close();
            DBConnection.closeConnections(conn);
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return userArray;
    }

    public static ArrayList<UserDTO> getReports(String conditions) {
        ArrayList<UserDTO> userArray = new ArrayList<UserDTO>();

        try {
            Connection conn = DBConnection.getConnection();
//           String SQL =
//               "Select t1.*,t4.IS_LOCKED,(SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES') lockTimes,T4.SYSTEM_USER_NAME,T4.SYSTEM_USER_PASSWORD,t2.ADMIN_STATUS FROM GVMS_ADMIN t1 left join GVMS_ADMIN_STATUS t2 on t1.ADMIN_STATUS_ID = t2.ADMIN_STATUS_ID left join GVMS_SYSTEM_USER t4 on t1.SYSTEM_USER_ID = t4.SYSTEM_USER_ID where 1=1 ";
            StringBuilder sql = new StringBuilder("");
            sql.append(" SELECT T1.*, T4.IS_LOCKED, (SELECT PROPERTIES ");
            sql.append(" FROM GVMS_PROPERTIES ");
            sql.append(" WHERE REASONE = 'LOCK_TIMES') LOCKTIMES, ");
            sql.append(" T4.SYSTEM_USER_NAME, T4.SYSTEM_USER_PASSWORD, T2.ADMIN_STATUS ");
            sql.append(" FROM  GVMS_SYSTEM_USER T4 LEFT JOIN GVMS_ADMIN T1 ON T1.SYSTEM_USER_ID = T4.SYSTEM_USER_ID LEFT JOIN GVMS_ADMIN_STATUS T2 ");
            sql.append(" ON T1.ADMIN_STATUS_ID = T2.ADMIN_STATUS_ID ");
            sql.append(" WHERE 1 = 1 and T4.SYSTEM_USER_TYPE='reporter' and t4.USER_STATUS_ID!=3 and t1.ADMIN_STATUS_ID!=3");


            if (conditions.compareTo("") != 0) {
                sql.append(conditions);
            }
            ResultSet adminRS = ExecuteQueries.executeQuery(conn, sql.toString());
            while (adminRS.next()) {
                if (adminRS.getInt("ADMIN_STATUS_ID") != 3) {
                    UserDTO a = new UserDTO();
                    a.setId(adminRS.getInt("ADMIN_ID"));
                    a.setEmail(adminRS.getString("ADMIN_EMAIL"));
                    a.setName(adminRS.getString("ADMIN_NAME"));
                    a.setPhone(adminRS.getString("ADMIN_PHONE"));
                    a.setStatus(adminRS.getString("ADMIN_STATUS"));
                    a.setStatusId(adminRS.getInt("ADMIN_STATUS_ID"));
                    a.setSystemUserId(adminRS.getInt("SYSTEM_USER_ID"));
                    a.setPassword(adminRS.getString("SYSTEM_USER_PASSWORD"));
                    a.setUserName(adminRS.getString("SYSTEM_USER_NAME"));
                    Integer temp = Integer.parseInt(adminRS.getString("IS_LOCKED"));
                    int availableLockTimes = adminRS.getInt("lockTimes");
                    if (temp <= availableLockTimes) {
                        a.setUserLocked("Not Locked");
                    } else {
                        a.setUserLocked("Locked");
                    }


                    userArray.add(a);
                }
            }
            adminRS.getStatement().close();
            adminRS.close();
            DBConnection.closeConnections(conn);
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return userArray;
    }

    public static ArrayList<UserDTO> getUsers(String conditions) {
        ArrayList<UserDTO> userArray = new ArrayList<UserDTO>();

        try {
            Connection conn = DBConnection.getConnection();
            String SQL =
                    "Select (SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES') lockTimes,T4.SYSTEM_USER_NAME,T4.SYSTEM_USER_PASSWORD,t1.USER_FULL_NAME,t1.SYSTEM_USER_ID,t1.USER_STATUS_ID,t4.IS_LOCKED,t3.USER_STATUS,t1.USER_ID,t1.USER_E_MAIL,t1.USER_PHONE_NUMBER,t1.USER_POS_CODE,t2.USER_TYPE_NAME FROM GVMS_USER t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID=t2.USER_TYPE_ID left join GVMS_USER_STATUS t3 on t1.USER_STATUS_ID = t3.USER_STATUS_ID left join GVMS_SYSTEM_USER t4 on t1.SYSTEM_USER_ID = t4.SYSTEM_USER_ID where T4.USER_STATUS_ID!=3 and 1=1 ";
            if (conditions.compareTo("") != 0) {
                SQL += conditions;
            }
            ResultSet userRS = ExecuteQueries.executeQuery(conn, SQL);
            while (userRS.next()) {
                if (userRS.getInt("USER_STATUS_ID") != 3) {
                    UserDTO u = new UserDTO();
                    u.setId(userRS.getInt("USER_ID"));
                    u.setSystemUserId(userRS.getInt("SYSTEM_USER_ID"));
                    u.setEmail(userRS.getString("USER_E_MAIL"));
                    u.setName(userRS.getString("USER_FULL_NAME"));
                    u.setPhone(userRS.getString("USER_PHONE_NUMBER"));
                    u.setPos(userRS.getString("USER_POS_CODE"));
                    u.setUserType(userRS.getString("USER_TYPE_NAME"));
                    u.setStatus(userRS.getString("USER_STATUS"));
                    u.setStatusId(userRS.getInt("USER_STATUS_ID"));
                    u.setPassword(userRS.getString("SYSTEM_USER_PASSWORD"));
                    u.setUserName(userRS.getString("SYSTEM_USER_NAME"));
                    Integer temp = Integer.parseInt(userRS.getString("IS_LOCKED"));
                    int availableLockTimes = userRS.getInt("lockTimes");
                    if (temp <= availableLockTimes) {
                        u.setUserLocked("Not Locked");
                    } else {
                        u.setUserLocked("Locked");
                    }

                    userArray.add(u);
                }
            }

            userRS.getStatement().close();
            userRS.close();
            DBConnection.closeConnections(conn);
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return userArray;

    }

    public static ArrayList<UserDTO> getCustomerSupports(String conditions) {
        ArrayList<UserDTO> userArray = new ArrayList<UserDTO>();
        StringBuffer SQL = new StringBuffer("SELECT (SELECT properties FROM GVMS_PROPERTIES WHERE REASONE = 'LOCK_TIMES') LOCKTIMES, ");
        SQL.append("T1.USER_FULL_NAME,T1.SYSTEM_USER_ID,T4.SYSTEM_USER_NAME,T4.SYSTEM_USER_PASSWORD, ");
        SQL.append("T1.USER_STATUS_ID, T4.IS_LOCKED, T3.USER_STATUS, ");
        SQL.append("T1.USER_ID, T1.USER_E_MAIL, T1.USER_PHONE_NUMBER  ");
        SQL.append("FROM GVMS_CS T1 ");
        SQL.append("LEFT JOIN GVMS_USER_STATUS T3 ON T1.USER_STATUS_ID = T3.USER_STATUS_ID ");
        SQL.append("LEFT JOIN GVMS_SYSTEM_USER T4 ON T1.SYSTEM_USER_ID = T4.SYSTEM_USER_ID ");
        SQL.append("WHERE T4.USER_STATUS_ID!=3 and 1 = 1 ");


        try {
            Connection conn = DBConnection.getConnection();
            if (conditions.compareTo("") != 0) {
                SQL.append(conditions);
            }
            ResultSet userRS = ExecuteQueries.executeQuery(conn, SQL.toString());

            while (userRS.next()) {
                if (userRS.getInt("USER_STATUS_ID") != 3) {
                    UserDTO u = new UserDTO();
                    u.setId(userRS.getInt("USER_ID"));
                    u.setSystemUserId(userRS.getInt("SYSTEM_USER_ID"));
                    u.setEmail(userRS.getString("USER_E_MAIL"));
                    u.setName(userRS.getString("USER_FULL_NAME"));
                    u.setPhone(userRS.getString("USER_PHONE_NUMBER"));
                    u.setStatus(userRS.getString("USER_STATUS"));
                    u.setStatusId(userRS.getInt("USER_STATUS_ID"));
                    u.setPassword(userRS.getString("SYSTEM_USER_PASSWORD"));
                    u.setUserName(userRS.getString("SYSTEM_USER_NAME"));
                    Integer temp = Integer.parseInt(userRS.getString("IS_LOCKED"));
                    int availableLockTimes = userRS.getInt("lockTimes");
                    if (temp <= availableLockTimes) {
                        u.setUserLocked("Not Locked");
                    } else {
                        u.setUserLocked("Locked");
                    }

                    userArray.add(u);
                }
            }

            userRS.getStatement().close();
            userRS.close();
            DBConnection.closeConnections(conn);
        } catch (SQLException e) {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
        return userArray;

    }

    public static boolean updateSystemUserPassword(String pass, String argUserId) {
        String sql = "update GVMS_SYSTEM_USER set SYSTEM_USER_PASSWORD='" + pass + "' where SYSTEM_USER_ID=" + argUserId;
        int dd = ExecuteQueries.executeNoneQuery(sql);
        if (dd > 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean updateUserInfo(String argUserId, String adminId, String passwordInput, String userNameInput, String userStatusSelected,
            String emailInput, String phoneInput, String fullNameInput, String posInput, String userName, int userTypeId, String userLockSelected, String user) throws SQLException {
        String pass = "";
        if (passwordInput.compareTo("") != 0) {
            pass = ", SYSTEM_USER_PASSWORD='" + passwordInput + "'";
        }
        String SQL = "";
        String sql =
                "update GVMS_SYSTEM_USER set IS_LOCKED='" + userLockSelected
                + "',SYSTEM_USER_NAME='" + userNameInput.trim() + "'" + pass
                + ", USER_STATUS_ID=" + userStatusSelected
                + " where SYSTEM_USER_ID=" + argUserId;
        int dd = ExecuteQueries.executeNoneQuery(sql);
        if (dd > 0) {
            if (user.compareTo("user") == 0) {
                SQL =
                        "update GVMS_USER set USER_FULL_NAME='" + fullNameInput + "', USER_E_MAIL='"
                        + emailInput + "', USER_PHONE_NUMBER=" + phoneInput
                        + ", USER_STATUS_ID=" + userStatusSelected + ", USER_POS_CODE='"
                        + posInput + "', USER_TYPE_ID=" + userTypeId
                        + " ,User_Modified_Date="
                        + new DateClass().convertDate(new DateClass().getDateTime())
                        + ", User_Modified_by='" + userName + "' where USER_ID="
                        + adminId;

            }
            if (user.compareTo("cs") == 0) {
                SQL =
                        "update GVMS_CS set USER_FULL_NAME='" + fullNameInput + "', USER_E_MAIL='"
                        + emailInput + "', USER_PHONE_NUMBER=" + phoneInput
                        + ", USER_STATUS_ID=" + userStatusSelected
                        + " ,User_Modified_Date="
                        + new DateClass().convertDate(new DateClass().getDateTime())
                        + ", User_Modified_by='" + userName + "' where USER_ID="
                        + adminId;

            }

            if (user.compareTo("admin") == 0) {
                SQL =
                        "update GVMS_ADMIN set ADMIN_NAME='" + fullNameInput + "', ADMIN_EMAIL='"
                        + emailInput + "', ADMIN_PHONE=" + phoneInput + ", ADMIN_STATUS_ID="
                        + userStatusSelected + ",ADMIN_MODIFIED_DATE="
                        + new DateClass().convertDate(new DateClass().getDateTime())
                        + " ,ADMIN_Modified_BY='" + userName + "'where ADMIN_ID=" + adminId;
            }
        }



        if (ExecuteQueries.executeNoneQuery(SQL) > 0 && dd > 0) {
            LoginDTO adminInfo =   (LoginDTO) jsfUtils.getFromSession("adminInfo");
             adminInfo = adminInfo==null ? (LoginDTO) jsfUtils.getFromSession("repInfo") : adminInfo;

            if(user.compareTo("user")==0)UpdateClass.updateUserInfo(adminId,  true);
            if(user.compareTo("cs")==0)UpdateClass.updateCSInfo(adminId,  true);
            if(user.compareTo("admin")==0)UpdateClass.updateAdminInfo(adminId,  true);
            return true;
        }


        return false;
    }

    public static boolean setNewUser(String argUserId, String passwordInput, String userNameInput, String userStatusSelected,
            String emailInput, String phoneInput, String fullNameInput, String posInput, String userName, int userTypeId,String userLockSelected ,String user) throws SQLException {
        int insertUserInfo = 0;
        String SQL =
                "insert into GVMS_SYSTEM_USER(SYSTEM_USER_ID,SYSTEM_USER_PASSWORD,SYSTEM_USER_NAME,USER_STATUS_ID,SYSTEM_USER_TYPE,IS_LOCKED,SYSTEM_LOGIN_COUNT,SYSTEM_LOGIN_END_DATE) Values ("
                + argUserId + ",'" + passwordInput + "','" + userNameInput.trim().toLowerCase()
                + "'," + userStatusSelected + ",'" + user + "','"+userLockSelected+"',0,sysdate+(select PROPERTIES from GVMS_PROPERTIES where REASONE='DAYS_FOR_EXPIRED_LOGIN'))";

        int insertSystemUser = ExecuteQueries.executeNoneQuery(SQL);
        Connection con = DBConnection.getConnectionForThread();
        Long userId = DBUtil.getSequenceNextVal(con, "GVMS_USER_SEQ_ID");
        DBConnection.closeThreadConnections(con);
        if (insertSystemUser > 0) {
            if (user.compareTo("user") == 0) {
                SQL =
                        "insert into GVMS_USER (USER_ID,USER_TYPE_ID,USER_FULL_NAME,USER_E_MAIL,USER_PHONE_NUMBER,USER_POS_CODE,SYSTEM_USER_ID,USER_STATUS_ID,User_creation_Date,User_Created_By) "
                        + "values " + "("+userId+"," + userTypeId + ",'" + fullNameInput
                        + "','" + emailInput + "','" + phoneInput + "','" + posInput + "',"
                        + argUserId + "," + userStatusSelected + ","
                        + new DateClass().convertDate(new DateClass().getDateTime()) + ",'"
                        + userName + "')";
            }
            if (user.compareTo("cs") == 0) {
                SQL =
                        "insert into GVMS_CS (USER_ID,USER_FULL_NAME,USER_E_MAIL,USER_PHONE_NUMBER,SYSTEM_USER_ID,USER_STATUS_ID,User_creation_Date,User_Created_By) "
                        + "values " + "("+userId+", '" + fullNameInput
                        + "','" + emailInput + "','" + phoneInput + "',"
                        + argUserId + "," + userStatusSelected + ","
                        + new DateClass().convertDate(new DateClass().getDateTime()) + ",'"
                        + userName + "')";
            }
            if (user.compareTo("admin") == 0) {
                SQL =
                        "insert into GVMS_ADMIN (ADMIN_ID,ADMIN_NAME,ADMIN_EMAIL,ADMIN_PHONE,SYSTEM_USER_ID,ADMIN_STATUS_ID,ADMIN_CREATION_DATE,ADMIN_CREATED_BY) "
                        + "values " + "("+userId+",'" + fullNameInput + "','"
                        + emailInput + "','" + phoneInput + "'," + argUserId + ","
                        + userStatusSelected + ","
                        + new DateClass().convertDate(new DateClass().getDateTime()) + ",'"
                        + userName + "')";
            }
            if (user.compareTo("reporter") == 0) {
                SQL =
                        "insert into GVMS_ADMIN (ADMIN_ID,ADMIN_NAME,ADMIN_EMAIL,ADMIN_PHONE,SYSTEM_USER_ID,ADMIN_STATUS_ID,ADMIN_CREATION_DATE,ADMIN_CREATED_BY) "
                        + "values " + "("+userId+",'" + fullNameInput + "','"
                        + emailInput + "','" + phoneInput + "'," + argUserId + ","
                        + userStatusSelected + ","
                        + new DateClass().convertDate(new DateClass().getDateTime()) + ",'"
                        + userName + "')";
            }
            insertUserInfo = ExecuteQueries.executeNoneQuery(SQL);
        }


        if (insertUserInfo > 0 && insertSystemUser > 0) {
            LoginDTO adminInfo =   (LoginDTO) jsfUtils.getFromSession("adminInfo");
             adminInfo = adminInfo==null ? (LoginDTO) jsfUtils.getFromSession("repInfo") : adminInfo;
            if(user.compareTo("user")==0)UpdateClass.updateUserInfo(userId+"",  false);
            if(user.compareTo("cs")==0)UpdateClass.updateCSInfo(userId+"",  false);
            if(user.compareTo("admin")==0)UpdateClass.updateAdminInfo(userId+"",  false);
            if(user.compareTo("reporter")==0)UpdateClass.updateAdminInfo(userId+"",  false);
            return true;
        } else {
            ExecuteQueries.executeNoneQuery("delete from GVMS_SYSTEM_USER where SYSTEM_USER_ID=" + argUserId);
            return false;
        }



    }

    public static void updateUserLastPass(String userId, String argEncreptedPass) {
        if (argEncreptedPass.compareTo("") != 0) {
            try {
                Connection conn = DBConnection.getConnection();
                //  String strPasswordChangeQuery = "update (select * from GEN_PERSON,GEN_USER where USER_ID=PERSON_ID) t1 set PASSWORD = '"+argNewPassword+"'  where PERSON_EMAIL='"+argUserEmail+"'";
                int lastPass = com.mobinil.gvms.system.security.DAO.securityDAO.getProps("LAST_PASSWORD_COUNT").intValue();
                int updateUser = 0;
                ResultSet chckLst6Pswrd =
                        ExecuteQueries.executeQuery(conn, "select Max(PASSWORD_INDEX) passCount from GVMS_PASS_TRACE where SYSTEM_USER_ID =" + userId);
                //            //System.out.println("select Max(PASSWORD_INDEX) passCount from GVMS_PASS_TRACE where SYSTEM_USER_ID ="+userId);

                if (chckLst6Pswrd.next()) {
                    if (chckLst6Pswrd.getInt("passCount") != 0) {
                        if (chckLst6Pswrd.getInt("passCount") > 0
                                && chckLst6Pswrd.getInt("passCount") < (lastPass + 1)) {
                            if (chckLst6Pswrd.getInt("passCount") == lastPass) {

                                int updatePassTrace =
                                        ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + userId
                                        + ",'" + argEncreptedPass + "',"
                                        + (chckLst6Pswrd.getInt("passCount") + 1)
                                        + ")");

                                //                            //System.out.println(updatePassTrace);



                                updatePassTrace =
                                        ExecuteQueries.executeNoneQuery("update GVMS_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-1 where SYSTEM_USER_ID=" + userId);
                                //                            //System.out.println(updatePassTrace);


                                int deletePasstrace =
                                        ExecuteQueries.executeNoneQuery("delete from GVMS_PASS_TRACE where SYSTEM_USER_ID =" + userId
                                        + " and PASSWORD_INDEX=0");
                                ////System.out.println(deletePasstrace);
                            } else {
                                int updatePassTrace =
                                        ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values ("
                                        + userId + ",'" + argEncreptedPass + "',"
                                        + (chckLst6Pswrd.getInt("passCount")
                                        + 1) + ")");
                                //                                                            //System.out.println(updatePassTrace);


                            }
                        }
                    } else {
                        int updatePassTrace =
                                ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + userId
                                + ",'" + argEncreptedPass + "',"
                                + 1
                                + ")");
                        //                                                         //System.out.println(updatePassTrace);
                    }
                }

                DBConnection.closeConnections(conn);
            } catch (Exception ex) {
                new com.mobinil.gvms.system.utility.PrintException().printException(ex);
            }
        }
    }
}
