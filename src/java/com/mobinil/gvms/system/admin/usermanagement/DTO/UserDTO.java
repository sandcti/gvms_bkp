package com.mobinil.gvms.system.admin.usermanagement.DTO;

public class UserDTO
{
  public String userType;
  public String name;
  public String email;
  public String phone;
  public String pos;
  public Boolean selected;
  public Integer Id;
  public String status;
  public Integer statusId;
  public String userLocked;
  public Integer systemUserId;
  public String password;
  public String userName;

  public UserDTO()
  {
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public String getUserType()
  {
    return userType;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getEmail()
  {
    return email;
  }

  public void setPhone(String phone)
  {
    this.phone = phone;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setPos(String pos)
  {
    this.pos = pos;
  }

  public String getPos()
  {
    return pos;
  }

  public void setSelected(Boolean selected)
  {
    this.selected = selected;
  }

  public Boolean getSelected()
  {
    return selected;
  }

  public void setId(Integer id)
  {
    this.Id = id;
  }

  public Integer getId()
  {
    return Id;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatusId(Integer statusId)
  {
    this.statusId = statusId;
  }

  public Integer getStatusId()
  {
    return statusId;
  }

  public void setUserLocked(String userLocked)
  {
    this.userLocked = userLocked;
  }

  public String getUserLocked()
  {
    return userLocked;
  }

public void setSystemUserId(Integer systemUserId) {
	this.systemUserId = systemUserId;
}

public Integer getSystemUserId() {
	return systemUserId;
}

public void setPassword(String password) {
	this.password = password;
}

public String getPassword() {
	return password;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getUserName() {
	return userName;
}
  }
