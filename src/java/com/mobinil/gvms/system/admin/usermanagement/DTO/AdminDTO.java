package com.mobinil.gvms.system.admin.usermanagement.DTO;

public class AdminDTO
{
  public String name;
  public String email;
  public String phone;
  public String status;
  public Boolean selected;
  public Integer Id;

  public AdminDTO()
  {
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getEmail()
  {
    return email;
  }

  public void setPhone(String phone)
  {
    this.phone = phone;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setSelected(Boolean selected)
  {
    this.selected = selected;
  }

  public Boolean getSelected()
  {
    return selected;
  }

  public void setId(Integer id)
  {
    this.Id = id;
  }

  public Integer getId()
  {
    return Id;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }
  }
