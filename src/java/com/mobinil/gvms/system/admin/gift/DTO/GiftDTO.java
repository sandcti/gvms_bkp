package com.mobinil.gvms.system.admin.gift.DTO;

public class GiftDTO {
    public GiftDTO() {
    }

      public String giftName;
      public String giftNameAr;
      public String giftDescription;
      public String createdBy;
      public String giftType;
      public Integer giftValue;
      public String giftStatus;
      public String creation;
      public Integer giftId;
      public Boolean selected;
      public String statusId;



      public void setGiftName(String giftName)
      {
        this.giftName = giftName;
      }

      public String getGiftName()
      {
        return giftName;
      }

      public void setGiftDescription(String giftDescription)
      {
        this.giftDescription = giftDescription;
      }

      public String getGiftDescription()
      {
        return giftDescription;
      }

      public void setCreatedBy(String createdBy)
      {
        this.createdBy = createdBy;
      }

      public String getCreatedBy()
      {
        return createdBy;
      }


      public void setGiftValue(Integer giftValue)
      {
        this.giftValue = giftValue;
      }

      public Integer getGiftValue()
      {
        return giftValue;
      }


      public void setGiftStatus(String giftStatus)
      {
        this.giftStatus = giftStatus;
      }

      public String getGiftStatus()
      {
        return giftStatus;
      }

      public void setGiftType(String giftType)
      {
        this.giftType = giftType;
      }

      public String getGiftType()
      {
        return giftType;
      }

      public void setGiftId(Integer giftId)
      {
        this.giftId = giftId;
      }

      public Integer getGiftId()
      {
        return giftId;
      }

      public void setSelected(Boolean selected)
      {
        this.selected = selected;
      }

      public Boolean getSelected()
      {
        return selected;
      }

      public void setCreation(String creation)
      {
        this.creation = creation;
      }

      public String getCreation()
      {
        return creation;
      }

      public void setGiftNameAr(String giftNameAr)
      {
        this.giftNameAr = giftNameAr;
      }

      public String getGiftNameAr()
      {
        return giftNameAr;
      }

      public void setStatusId(String statusId)
      {
        this.statusId = statusId;
      }

      public String getStatusId()
      {
        return statusId;
      }
    }
