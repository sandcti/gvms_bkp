package com.mobinil.gvms.system.admin.gift.DAO;

import com.mobinil.gvms.system.admin.gift.DTO.GiftDTO;

import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.UpdateClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class GiftDAO {
    public GiftDAO() {
    }
    
    public static ArrayList<GiftDTO> getGeftInfos() throws SQLException{
        ArrayList<GiftDTO> giftArray = new ArrayList<GiftDTO> ();
    
        Connection conn = DBConnection.getConnection();
        String SQL = 
          "Select t1.GIFT_STATUS_ID,t1.GIFT_NAME_ARABIC,t1.GIFT_ID,t1.GIFT_NAME,t1.GIFT_TYPE_ID,t1.GIFT_VALUE,t1.GIFT_DESCRIPTON,to_char(t1.GIFT_CREATION_DATE,'DD-MM-YYYY') GIFT_CREATION_DATE,t1.GIFT_STATUS_ID,t1.GIFT_CREATEDBY,t2.GIFT_STATUS,t3.GIFT_TYPE_NAME from GVMS_GIFT t1 left join GVMS_GIFT_STATUS t2 on  t1.GIFT_STATUS_ID =  t2.GIFT_STATUS_ID left join GVMS_GIFT_TYPE t3 on t1.GIFT_TYPE_ID = t3.GIFT_TYPE_ID";
        ResultSet giftRS = ExecuteQueries.executeQuery(conn,SQL);
      

            while (giftRS.next())
              {
                if (giftRS.getInt("GIFT_STATUS_ID") != 3)
                  {
                    GiftDTO g = new GiftDTO();
                    g.setCreatedBy(giftRS.getString("GIFT_CREATEDBY"));
                    g.setCreation(giftRS.getString("GIFT_CREATION_DATE"));
                    g.setGiftNameAr(giftRS.getString("GIFT_NAME_ARABIC"));
                    g.setGiftDescription(giftRS.getString("GIFT_DESCRIPTON"));
                    g.setGiftName(giftRS.getString("GIFT_NAME"));
                    g.setGiftStatus(giftRS.getString("GIFT_STATUS"));
                    g.setGiftType(giftRS.getString("GIFT_TYPE_NAME"));
                    g.setGiftValue(giftRS.getInt("GIFT_VALUE"));
                    g.setGiftId(giftRS.getInt("GIFT_ID"));
                    g.setStatusId(giftRS.getInt("GIFT_STATUS_ID") + "");
                    giftArray.add(g);
                  }
              }
            giftRS.getStatement().close();
            giftRS.close();
         
            DBConnection.closeConnections ( conn );
    return giftArray;
    }
    public static String deleteGift(Integer id) throws SQLException {
    	String delete = checkBeforeDeleteGift(id);
        if (delete.compareTo("")==0){
        
        	ExecuteQueries.executeNoneQuery("update GVMS_GIFT set GIFT_STATUS_ID=3 where GIFT_ID=" + 
                                        id);
                UpdateClass.updateGiftInfo(id+"", Boolean.TRUE);
        }
        return delete;
    }
    public static String checkBeforeDeleteGift(Integer id) throws SQLException {
    	String temp = "";
    	Connection conn = DBConnection.getConnection();
    	String SQL = 
            "select * from GVMS_CAMPAIGN GC , GVMS_CAMPAIGN_GIFT GCG where GC.CAMPAIGN_ID = GCG.CAMPAIGN_ID and CAMPAIGN_STATUS_ID =2 and GIFT_ID ="+id;
          ResultSet giftRS = ExecuteQueries.executeQuery(conn,SQL);
        

              if (giftRS.next())
                {
            	  temp =giftRS.getString("CAMPAIGN_NAME");
                }
              giftRS.getStatement().close();
              giftRS.close();
              DBConnection.closeConnections ( conn );
    	return temp;
    }
    
}
