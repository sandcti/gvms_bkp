package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.admin.campaign.DAO.*;


import com.mobinil.gvms.system.admin.campaign.DTO.AdditionalFieldClass;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.custom.fileupload.UploadedFile;



public class uploadValidationList
{

  // Init ---------------------------------------------------------------------------------------

  public UploadedFile uploadedFile;
  public String fileName;
  public Boolean validtyValue;
  public HtmlSelectBooleanCheckbox selectBooleanCheckbox1;
  

  // Actions ------------------------------------------------------------------------------------

  public String submit()
  {



      if (uploadedFile==(null)){  
               FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                            new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                             "Missing File Please Pick A Txt File.", 
                                                                             null));                
               return null;
             }
             if (uploadedFile.getContentType().compareTo("text/plain")!=0){
               FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                            new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                             "Missing File Type Please Check File Type To Be text.", 
                                                                             null));             
                 return null;
             }
             if (!uploadedFile.getName().contains("txt")){              
               FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                            new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                             "Missing File Type Please Check File Type To Have Extension txt.", 
                                                                             null));             
               
                 return null;
             } 
    // Just to demonstrate what information you can get from the uploaded file.
    //System.out.println("File type: " + uploadedFile.getContentType());    
    //System.out.println("File name: " + uploadedFile.getName());
    //System.out.println("File size: " + uploadedFile.getSize() + " bytes");

    try
      {
    	 String type = (String)jsfUtils.getFromSession("fieldType");
        List<List<String>> parsFile = CsvUtil.parseCsv(uploadedFile.getInputStream(), ',',type);
 
        //System.out.println("file line len is "+parsFile.size());
        

if (parsFile.size()==0)
{
  FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                               new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                "Uploaded File Cannot be empty.", 
                                                                null));             

}
else{

  ArrayList<Integer> errors = null;
  int fieldId = ((AdditionalFieldClass)jsfUtils.getFromSession("fieldInfo")).getFieldId();
  String adminFullName = ((LoginDTO)jsfUtils.getFromSession("adminInfo")).getFullName();
  
  errors=  insertList(parsFile,fieldId,adminFullName);
  
  // FileUtil.write(uniqueFile, uploadedFile.getInputStream());
  //fileName = uniqueFile.getName();
  fileName = "ggg";
  // Show succes message.
  String errorString = "";
  
  
  if (errors.size()>0)
  {
  for (int i=0 ;i<errors.size() ;i++ ) 
  {
  
    errorString+= (Integer)errors.get(i)+1+",";
  }
    errorString = errorString.substring(0,(errorString.length()-1));
    FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                 new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                  "File upload succeed! but there are some errors in lines "+errorString, 
                                                                  null));

  }
  else
  {
  FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                               new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                "File upload succeed!", 
                                                                null));
  }

  jsfUtils.removeFromSession("AdditionalField_Bean");
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/AdditionalField.jsp");
      }
    catch (IOException e)
      {
         
      }

}

        
       

      }
    catch (IOException e)
      {

        // Show error message.
        FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                     new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                      "File upload failed with I/O error.", 
                                                                      null));

        // Always log stacktraces.
         
      }
   
      return null;
  }


  public ArrayList<Integer> insertList(List<List<String>> list,Integer fieldId,String userName)
  {
    
    
    int counter;
     String validationListId = "";
    ArrayList<Integer> errors = new ArrayList<Integer>();
    
    
   
      additionalFieldDAO.deleteBeforeInsertList(fieldId);
   
     validationListId = insertInfoList(list.size(), fieldId, userName);
    
   

    for (counter = 0; counter < list.size(); counter++)
      {
        String ss = (String)list.get(counter).get(0);
        if (ss.length()>20) 
        {
        list.remove(counter);
        errors.add(counter);
        }
      }
      try
      {
    if (validationListId.compareTo("")!=0){
      
        additionalFieldDAO.insertList(list,validationListId);
      
    
    }
      
      if (errors.size()!=0)
      {
        additionalFieldDAO.updateListRowErrors(errors,validationListId);

      }
    }
    catch (SQLException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        try {
            additionalFieldDAO.deleteListInfo(fieldId);
        }
        catch (SQLException ee) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
    }


    


    return errors;
  }

public void downloadTemplateList (){
  
  FacesContext fc = FacesContext.getCurrentInstance();  
  HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
  String fileName = "templateList.txt";  
          String filePath =jsfUtils.getParamterFromIni("defualtPath")+"VoucherError/";
  
  File csvFile = new File(filePath, fileName);
  try
    {
      HttpServletUtil.downloadFile(response, csvFile, true);
    }
  catch (IOException e)
    {
  //System.out.println(e.toString());
    }
  fc.responseComplete();


}

  public String insertInfoList(int rowCount,Integer fieldId,String userName)
  {
    
    try
    {
      String ss = additionalFieldDAO.insertListInfo(rowCount,validtyValue,fieldId,userName);
      if(ss.compareTo("")==0)
      {
        FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                     new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                      "File upload succeed!", 
                                                                      null));
        return "";
      }
      else
      {
      return ss;
      }
    }
    catch (SQLException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      return "";
    }
    


  }

  
  // Getters ------------------------------------------------------------------------------------

  public UploadedFile getUploadedFile()
  {
    return uploadedFile;
  }

  public String getFileName()
  {
    return fileName;
  }

  // Setters ------------------------------------------------------------------------------------

  public void setUploadedFile(UploadedFile uploadedFile)
  {
    this.uploadedFile = uploadedFile;
  }

  public void setSelectBooleanCheckbox1(HtmlSelectBooleanCheckbox selectBooleanCheckbox1)
  {
    this.selectBooleanCheckbox1 = selectBooleanCheckbox1;
  }

  public HtmlSelectBooleanCheckbox getSelectBooleanCheckbox1()
  {
    return selectBooleanCheckbox1;
  }

  public void setValidtyValue(Boolean validtyValue)
  {
    this.validtyValue = validtyValue;
  }

  public Boolean getValidtyValue()
  {
    return validtyValue;
  }
}
