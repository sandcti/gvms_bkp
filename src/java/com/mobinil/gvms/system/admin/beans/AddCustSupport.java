package com.mobinil.gvms.system.admin.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.MD5Class;
import com.mobinil.gvms.system.utility.PasswordUtils;
import com.mobinil.gvms.system.utility.SecurityUtils;
import com.mobinil.gvms.system.utility.jsfUtils;

public class AddCustSupport {

	  
	  public String userNameInput;
	  public String passwordInput;
	  public String fullNameInput;
	  public String emailInput;
	  public String phoneInput;
	  
	  public String oldPasswordInput;
	  public String confirmPasswordInput;
	  public String msgWarn;
	  public HtmlForm form1;
	  public String passLabel;
	  String userName;
	  Integer userId;
	  Integer sysUserId;
	  public String userLockSelected;
	  public String maximumLockNum;

	  public Boolean renderOldPass;
	  public Boolean renderAdd;
	  public Boolean renderUpdate;

	  public String userStatusSelected;
	  
	  public HtmlCommandButton addUser_cmd;

	  public AddCustSupport() throws SQLException
	  {
	      new leftMenu().checkUserInSession();
	      Connection conn = DBConnection.getConnection();
	    String bridg = 
	      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editUser");
	    int count = 0;

	    	    userName = 
	        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userNameAdmin");

	    ResultSet lockTimesRS = 
	      ExecuteQueries.executeQuery(conn,"SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES'");

	    if (bridg.compareTo("newCS") == 0)
	      {
	        renderAdd = true;
	        renderOldPass = false;
	        renderUpdate = false;
	        passLabel = "Password *";
	        try
	          {
	            if (lockTimesRS.next())
	              {
	                maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);

	              }
	            lockTimesRS.close();
	          }
	        catch (SQLException e)
	          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
	          }
	      }
	    else
	      {
	        passLabel = "Password";
	        renderAdd = false;
	        renderOldPass = false;
	        renderUpdate = true;

	        sysUserId = (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("system_user_id");
	        userId = 
	            (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_id");
	        fullNameInput = 
	            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_full_name");
	        userNameInput = 
	            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_name");	        
	        emailInput = 
	            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_email");
	        phoneInput = 
	            "" + (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_phone");
	        userStatusSelected = 
	            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user_status");
	        String aa = 
	          (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("lockUserStatus");
	        try
	          {
	            if (lockTimesRS.next())
	              {
	                if (aa.compareTo("Not Locked") == 0)
	                  {

	                    maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);
	                    userLockSelected = "0";
	                  }
	                else
	                  {


	                    maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);
	                    userLockSelected = maximumLockNum;


	                  }
	              }
	            lockTimesRS.close();
	          }
	        catch (SQLException e)
	          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
	          }


	      }
	    DBConnection.closeConnections ( conn );

	  }

	  public String update_Action() throws SQLException
	  {

	    int userStatusId = 0;
	    
	    int lastPass = 0;
	    userNameInput = userNameInput.trim();

	    if (userNameInput.compareTo("") == 0 ||userNameInput == null)
	      {
	        msgWarn = "Please fill star boxes.";	        
	        return null;
	      }
	    
	    if (emailInput!=null)
	      {
	    	  if (!emailInput.contains("@"))
	    	  {
	    		  msgWarn ="Please enter a valid mail.";
	              return null;
	    	  }
	    	  if (!emailInput.contains("."))
	    	  {
	    		  msgWarn ="Please enter a valid mail.";
	              return null;
	    	  }
	      }
	    Connection conn = DBConnection.getConnection();
	    ResultSet repeatUserName = 
	      ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_NAME from GVMS_SYSTEM_USER where SYSTEM_USER_NAME='" + 
	                                  userNameInput.trim() + 
	                                  "' and SYSTEM_USER_TYPE='user' and SYSTEM_USER_ID!=" + sysUserId);
	    try
	      {
	        if (repeatUserName.next())
	          {

	            msgWarn = "this user name already exists.";
	            DBConnection.closeConnections ( conn );
	            return null;

	          }
	        repeatUserName.close();
	      }
	    catch (SQLException e)
	      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
	      }
	    DBConnection.closeConnections ( conn );
	    if (passwordInput.compareTo(confirmPasswordInput) != 0)
	      {
	        msgWarn = "Please retype password and confirmation.";	        
	        return null;
	      }


	   


	    Integer systemUserId = ((LoginDTO)jsfUtils.getFromSession("adminInfo")).getSysUserId();
	      


	    if (userStatusSelected.compareTo("0") != 0)
	      {
	        
	            String SQLAdmin = "";
	            String SQLSystem = "";
	            String SQL = "";


	            try
	              {

	                String passEncripted = MD5Class.MD5(passwordInput);

	                
	                String SecurityError = "";
	                if (passwordInput!=null&&passwordInput.compareTo("")!=0)
	                {
	                	SecurityError =  SecurityUtils.checkPass(passwordInput, passEncripted, 
                                userNameInput.trim(), 
                                sysUserId + "");
       
	                }
	                else passEncripted="";
	            	
	            	

	                

	                                        
	              if (SecurityError.compareTo("")!=0)
	              {
	                msgWarn = SecurityError;
	              }
	              else
	              {
	                if(UserManagementDAO.updateUserInfo(sysUserId+"",userId+"",passEncripted,userNameInput,userStatusSelected,emailInput,phoneInput,fullNameInput,"",userName,0,userLockSelected,"cs"))
	                {
	                UserManagementDAO.updateUserLastPass(systemUserId+"",passEncripted);
	                msgWarn="Your new password was updated to your account.";
	                }
	              }
	                                               

	              }
	            catch (Exception e)
	              {
	                 new com.mobinil.gvms.system.utility.PrintException().printException(e);
	              }


	        
	        
	      }
	    else
	      {

	        msgWarn = "Please select status for " + userNameInput + ".";
	      }

	    return null;
	  }


	  public String AddUser() throws SQLException
	  {
	      String systemUserId = "";    
	      
	      int userStatusId = 0;
	      String SQL;
	    userNameInput = userNameInput.trim();
	    if (userNameInput.compareTo("") == 0 || 
	        passwordInput.compareTo("") == 0 || userNameInput == null ||passwordInput == null)
	      {

	        msgWarn = "Please fill star boxes.";
	        return null;
	      }
	    Connection conn = DBConnection.getConnection();
	      ResultSet repeatUserName = ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_NAME from GVMS_SYSTEM_USER where SYSTEM_USER_NAME='"+userNameInput.trim()+"' and SYSTEM_USER_TYPE='user'");
	      try
	      {
	        if (repeatUserName.next())
	        {
	        
	          msgWarn = "this user name already exists.";
	          DBConnection.closeConnections ( conn );
	          return null;
	        
	        }
	        repeatUserName.getStatement().close();
	        repeatUserName.close();
	      }
	      catch (SQLException e)
	      {
	        new com.mobinil.gvms.system.utility.PrintException().printException(e);
	      }
	      
	      if (emailInput!=null)
	      {
	    	  if (!emailInput.contains("@"))
	    	  {
	    		  msgWarn ="Please enter a valid mail.";
	              return null;
	    	  }
	    	  if (!emailInput.contains("."))
	    	  {
	    		  msgWarn ="Please enter a valid mail.";
	              return null;
	    	  }
	      }
//	      if (!new PasswordUtils().checkLength((passwordInput)))
//	        {
//	          msgWarn="invalid password length.";
//	          
//	          return null;
//	        }
	       if (!new PasswordUtils().checkSequanceAlphabetic(passwordInput))
	        {
	          msgWarn="Invalid sequance characters.";
	          
	          return null;
	        }
	      else if (!new PasswordUtils().checkIdInPassword( passwordInput, userNameInput))
	        {
	          msgWarn="Invalid password.";
	          
	          return null;
	        }
	      else if (!new PasswordUtils().checkSimilarChar( passwordInput))
	        {
	          msgWarn="Invalid password similar characters.";
	          
	          return null;
	        }
	      else if (!new PasswordUtils().checkStringWithTxtAndNum( passwordInput))
	        {
	          msgWarn="Invalid password not contain text or numbers.";
	          
	          return null;
	        }
	        
	      else if (!new PasswordUtils().checkUpLow(passwordInput))
	        {
	          msgWarn="Invalid password lower and upper cases.";
	          
	          return null;
	        }
	      
	      
	      if (passwordInput.compareTo(confirmPasswordInput) != 0)
	        {

	          msgWarn = "Please re-type password and its confirmation.";
	          return null;
	        }


	      
	       

	      try
	        {
	          if (userStatusSelected.compareTo("0") != 0)
	            {
	              
	                
	                systemUserId = DBConnection.getSequenceNextVal(conn,"GVMS_SYSTEM_USER_SEQ_ID");
	                
	                  String passEncripted = MD5Class.MD5(passwordInput);
	                  String SecurityError =  SecurityUtils.checkPass(passwordInput, passEncripted, 
	                                                  userNameInput.trim(), 
	                                                  systemUserId + "");
	                                                  
	                  if (SecurityError.compareTo("")!=0)
	                  {
	                   msgWarn = SecurityError;
	                  }
	                  else
	                  {

	                   if(UserManagementDAO.setNewUser(systemUserId,passEncripted,userNameInput,userStatusSelected,emailInput,phoneInput,fullNameInput,"",userName,0,"0","cs"))
	                   {
	                   UserManagementDAO.updateUserLastPass(systemUserId,passEncripted);
	                   msgWarn="One User Added.";
	                   }
	                  }
	                  
	              
	            }
	          else
	            {

	              msgWarn = "Please select status for " + userNameInput + ".";
	            }


	        }
	      catch (Exception e)
	        {
	           new com.mobinil.gvms.system.utility.PrintException().printException(e);
	        }
	      DBConnection.closeConnections ( conn );
	      //    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddNormalUser_Bean");
	      //    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");
	      return null;


	  }

	  public String back_Action()
	  {

	    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddNormalUser_Bean");
	    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");

	    return "backToCS";
	  }

	  public void setForm1(HtmlForm form1)
	  {
	    this.form1 = form1;
	  }

	  public HtmlForm getForm1()
	  {
	    return form1;
	  }


	  public void setUserStatusSelected(String userStatusSelected)
	  {
	    this.userStatusSelected = userStatusSelected;
	  }

	  public String getUserStatusSelected()
	  {
	    return userStatusSelected;
	  }



	  

	  public void setAddUser_cmd(HtmlCommandButton commandButton1)
	  {
	    this.addUser_cmd = commandButton1;
	  }

	  public HtmlCommandButton getAddUser_cmd()
	  {
	    return addUser_cmd;
	  }


	  public void setRenderOldPass(Boolean renderOldPass)
	  {
	    this.renderOldPass = renderOldPass;
	  }

	  public Boolean getRenderOldPass()
	  {
	    return renderOldPass;
	  }

	  public void setRenderAdd(Boolean renderAdd)
	  {
	    this.renderAdd = renderAdd;
	  }

	  public Boolean getRenderAdd()
	  {
	    return renderAdd;
	  }

	  public void setOldPasswordInput(String oldPasswordInput)
	  {
	    this.oldPasswordInput = oldPasswordInput;
	  }

	  public String getOldPasswordInput()
	  {
	    return oldPasswordInput;
	  }

	  public void setUserNameInput(String userNameInput)
	  {
	    this.userNameInput = userNameInput;
	  }

	  public String getUserNameInput()
	  {
	    return userNameInput;
	  }

	  public void setPasswordInput(String passwordInput)
	  {
	    this.passwordInput = passwordInput;
	  }

	  public String getPasswordInput()
	  {
	    return passwordInput;
	  }

	  public void setFullNameInput(String fullNameInput)
	  {
	    this.fullNameInput = fullNameInput;
	  }

	  public String getFullNameInput()
	  {
	    return fullNameInput;
	  }

	  public void setEmailInput(String emailInput)
	  {
	    this.emailInput = emailInput;
	  }

	  public String getEmailInput()
	  {
	    return emailInput;
	  }

	  public void setPhoneInput(String phoneInput)
	  {
	    this.phoneInput = phoneInput;
	  }

	  public String getPhoneInput()
	  {
	    return phoneInput;
	  }

	  

	  public void setConfirmPasswordInput(String confirmPasswordInput)
	  {
	    this.confirmPasswordInput = confirmPasswordInput;
	  }

	  public String getConfirmPasswordInput()
	  {
	    return confirmPasswordInput;
	  }

	  public void setMsgWarn(String msgWarn)
	  {
	    this.msgWarn = msgWarn;
	  }

	  public String getMsgWarn()
	  {
	    return msgWarn;
	  }

	  public void setRenderUpdate(Boolean renderUpdate)
	  {
	    this.renderUpdate = renderUpdate;
	  }

	  public Boolean getRenderUpdate()
	  {
	    return renderUpdate;
	  }

	  public void setPassLabel(String passLabel)
	  {
	    this.passLabel = passLabel;
	  }

	  public String getPassLabel()
	  {
	    return passLabel;
	  }

	  public void setUserLockSelected(String userLockSelected)
	  {
	    this.userLockSelected = userLockSelected;
	  }

	  public String getUserLockSelected()
	  {
	    return userLockSelected;
	  }

	  public void setMaximumLockNum(String maximumLockNum)
	  {
	    this.maximumLockNum = maximumLockNum;
	  }

	  public String getMaximumLockNum()
	  {
	    return maximumLockNum;
	  }

}
