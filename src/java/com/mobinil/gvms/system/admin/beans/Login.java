package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.login.loginDAO.LoginDAO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.application.GVMSAppContextListener;
import com.mobinil.gvms.system.application.UserDestroyDTO;
import com.mobinil.gvms.system.utility.*;

import java.io.IOException;
import java.sql.*;


import java.util.ArrayList;


import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class Login {

    public HtmlInputText userName;
    public HtmlInputSecret password;
    public HtmlOutputLabel error;
    int loginFailed = 0;

    public String checkAdminInfo() {

        String pass = (String) password.getValue();
        LoginDTO adminInfo = new LoginDTO();
        String strUserName = ((String) userName.getValue()).trim();
        if (strUserName != null || pass != null) {
            if (strUserName.compareTo("") == 0 || pass.compareTo("") == 0) {
                error.setValue("Please provide user name and password. ");
                return null;
            }
        } else {
            error.setValue("Please provide user name and password. ");
            return null;
        }
        adminInfo = LoginDAO.authUser((String) userName.getValue());
        String errorMessage = adminInfo.getErrorMessage();

        if (errorMessage.compareTo("") == 0) {
            try {


                String encripteCurrPass = MD5Class.MD5(pass);
                if (encripteCurrPass.compareTo(adminInfo.getPassword()) == 0) {
                    String securityError =
                            SecurityUtils.checkPassInLogin(pass, encripteCurrPass,
                            strUserName,
                            adminInfo.getSysUserId() + "");
                    if (securityError.compareTo("") == 0) {
                        Connection con = DBConnection.getConnection();
                        LoginDAO.setIncrementLogin(con, adminInfo.getSysUserId() + "");
                        LoginDAO.setDefaultLock(con, adminInfo.getSysUserId() + "");
                        con.close();
                        if (adminInfo.getUserType().compareTo("admin") == 0) {
                            jsfUtils.putInSession(adminInfo, "adminInfo");
                        } else if (adminInfo.getUserType().compareTo("reporter") == 0) {
                            jsfUtils.putInSession(adminInfo, "repInfo");
                        }
                        String sessionId = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession(false).getId();
                        //System.out.println("session Id in login bean is " + sessionId);

                        ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();
                        for (int i = 0; i < users.size(); i++) {
                            UserDestroyDTO udd = users.get(i);
                            if (udd.getSessionId().compareTo(sessionId) == 0 && udd.getDestroy()) {
                                udd.setDestroy(false);
                            }
                            users.remove(i);
                            users.add(udd);
                        }
                        GVMSAppContextListener.setUserNavigation(users);

                        if (adminInfo.getUserType().compareTo("admin") == 0) {
                            return "welcomeAdmin";
                        } else if (adminInfo.getUserType().compareTo("reporter") == 0) {
                            return "welcomeRep";
                        }
                        else{
                            error.setValue("Invalid login information.");
                            return null;}

                    } else {
                        error.setValue(securityError);
                        return null;
                    }

                } else {
                    Connection con = DBConnection.getConnection();
                    LoginDAO.setIncrementLock(con, adminInfo.getSysUserId() + "");
                    con.close();
                    error.setValue("Invalid password.");
                    return null;
                }
            } catch (Exception e) {
                //System.out.println(e.getMessage());
                return null;
            }

        } else {
            error.setValue(errorMessage);
            return null;
        }


    }

    public void cleanDB() throws SQLException {
        cleanDB.cleanDB();
    }

    public String forgetPass() {
        try {

            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AdminForgetPassword_Bean");
            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/AdminForgetPassword.jsp");
        } catch (IOException e) {
        }
        return "forgetAdmin";
    }

    public String forget_Action() {
        return null;
    }

    public void setUserName(HtmlInputText userName) {
        this.userName = userName;
    }

    public HtmlInputText getUserName() {
        return userName;
    }

    public void setPassword(HtmlInputSecret password) {
        this.password = password;
    }

    public HtmlInputSecret getPassword() {
        return password;
    }

    public void setError(HtmlOutputLabel error) {
        this.error = error;
    }

    public HtmlOutputLabel getError() {
        return error;
    }
}
