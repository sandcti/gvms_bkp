package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.campaign.DAO.additionalFieldDAO;
import com.mobinil.gvms.system.admin.campaign.DTO.AdditionalFieldClass;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.utility.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class AdditionalField
{
	
  public ArrayList<AdditionalFieldClass> fieldArray = new ArrayList<AdditionalFieldClass>();
  AdditionalFieldClass dataItem;
  public HtmlForm form1;
  public HtmlDataTable dataTable1;
  public Integer rowCount = 10;
  public List<AdditionalFieldClass> selectedDataList;
  public String msgWarn;
  public Boolean renderUpdateList;
  int pageIndex = 0;
  public String sortField = null;
  public Boolean renderButtonNavigation = false;
  public boolean sortAscending = true;
  public String campaignName;
  public boolean activateRemoveFields = true;
  Integer campaignStatus;
  public Boolean disableDataTableCol = false;
  Integer campaignId;

  public AdditionalField()
  {
    new leftMenu().checkUserInSession();
    
    
    campaignTableClass ctc= (campaignTableClass) jsfUtils.getFromSession("campaignInfo");
    campaignStatus = ctc.getCampaignStatusId();           
    campaignId = ctc.getCampaignId();        
    campaignName = ctc.getCampaignName(); 
      
   
if (campaignStatus==2)activateRemoveFields=false;

    fieldArray = additionalFieldDAO.getFieldsInfos(campaignId,"FIELD_ORDER");
    
    
    if (campaignStatus == 3)disableDataTableCol = true;

      
    if (fieldArray.size() > 11) renderButtonNavigation = true;
      
  }

  public void exportHtmlTableAsText()
    throws IOException, SQLException
  {

    //System.out.println("in  as Excel ");
    
    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);
    FacesContext fc = FacesContext.getCurrentInstance();
    
    String templatePath = jsfUtils.getParamterFromIni("defualtPath");   

     
    filename = campaignName + "-" + filename;

    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".txt";

    boolean result = exportTemplateText(templatePath + "AdditionalFieldTXT/" + filename);

    File excelFile = new File(templatePath + "AdditionalFieldTXT/", filename);

    try
      {


        HttpServletUtil.downloadFile(response, excelFile, true);
      }
    catch (IOException e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
        //System.out.println("Download file failed: " + e.getMessage());
         
        return;
      }    

    fc.responseComplete();
  }

  public boolean exportTemplateText(String template) throws SQLException
  {
    
    String fieldInfo = "";
    ArrayList<AdditionalFieldClass> exportFieldArray = additionalFieldDAO.getFieldsInfos(campaignId,"FIELD_DB_NAME");
    
    Connection conn = DBConnection.getConnection();
        for (int j=0 ;j<exportFieldArray.size() ;j++ ) 
          {
           AdditionalFieldClass  afc = exportFieldArray.get(j);
           String campaignName = afc.getCampiagnName();
           int fieldId = afc.getFieldId();
          
                fieldInfo +=afc.getFieldDBName ( ) + ",";
          


            //System.out.println("field info in array" + j + " is " + fieldInfo);
            

          }
        fieldInfo = fieldInfo.substring ( 0,(fieldInfo.length ( ) -1));
    

    ResultSet fieldDataRS = 
      ExecuteQueries.executeQuery(conn,"select " + fieldInfo + " from GVMS_REDEEM_CAMPAIGN_" + 
                                  campaignId);

    String[] fieldColumnName = fieldInfo.split(",");


    try
      {
        //System.out.println("template =" + template);


        BufferedWriter tempIn = new BufferedWriter(new FileWriter(template, true));

        int counter = 0;
        while (fieldDataRS.next())
          {

            if (counter == 0)
              {
                for (int x = 0; x < fieldArray.size(); x++)
                  {
                    if (x == (fieldArray.size() - 1))
                      {
                        //System.out.println(fieldArray.get(x).getEnglishName());
                        tempIn.write(fieldArray.get(x).getEnglishName());
                        tempIn.newLine();
                      }
                    else
                      {
                        //System.out.println(fieldArray.get(x).getEnglishName());
                        tempIn.write(fieldArray.get(x).getEnglishName() + ",");
                      }
                  }
              }


            for (int x = 0; x < fieldColumnName.length; x++)
              {
                //System.out.println(fieldColumnName.length);
                //System.out.println(x);
                if (x == (fieldColumnName.length - 1))
                  {
                    //System.out.println(fieldDataRS.getObject(fieldColumnName[x]));
                    Object fieldValueObj = fieldDataRS.getObject(fieldColumnName[x]);
                    if (fieldValueObj == null)
                      {
                        tempIn.write("");
                      }
                    else
                      {
                        tempIn.write(fieldValueObj.toString());
                      }


                    tempIn.newLine();
                  }
                else
                  {
                    //System.out.println(fieldDataRS.getObject(fieldColumnName[x]));
                    Object fieldValueObj = fieldDataRS.getObject(fieldColumnName[x]);

                    if (fieldValueObj == null)
                      {
                        tempIn.write("" + ",");
                      }
                    else
                      {
                        tempIn.write(fieldValueObj.toString() + ",");
                      }
                  }
              }


            counter++;
            //            fieldDataRS.beforeFirst();


          }
        fieldDataRS.getStatement().close();
        fieldDataRS.close();
        tempIn.close();

        DBConnection.closeConnections ( conn );
        return true;
      }
    catch (Exception e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
       DBConnection.closeConnections ( conn );
        return false;
      }
  }


  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(fieldArray, new DTOComparator(sortField, sortAscending));
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public void refactorFields()
  {
    dataItem = (AdditionalFieldClass) dataTable1.getRowData();
    jsfUtils.removeFromSession("fieldInfo"); 
    jsfUtils.putInSession( dataItem,"fieldInfo");
  }

  public String validationListUpdate()
  {
    refactorFields();
    jsfUtils.putInSession( dataItem.getFieldType(),"fieldType");
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/ValidationList/AddValidationList.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    return null;
  }

  public String getSelectedItems() throws SQLException
  {

    // Get selected items.
    selectedDataList = new ArrayList<AdditionalFieldClass>();
    int count = 1;
    int delete = 0;
    Connection conn = DBConnection.getConnection();
    for (int counter = (dataTable1.getRows() * pageIndex); counter < fieldArray.size(); counter++)


      {
        if (counter == (dataTable1.getRows() * (pageIndex + 1)))
          {
            break;
          }

        dataItem = fieldArray.get(counter);
        if (dataItem.getSelected())
          {
            selectedDataList.add(dataItem);
            dataItem.setSelected(false); // Reset.
            int id = dataItem.getFieldId();

            ResultSet checkField = 
              ExecuteQueries.executeQuery(conn,"Select VALIDATION_LIST_ID,Validation_list_row_count from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                          id);
            try
              {
                if (checkField.next())
                  {
//                  int listCount = checkField.getInt("Validation_list_row_count");
//                    if (campaignStatus == 2 && listCount!=0)
//                      {
//                        msgWarn = "Delete " + dataItem.getEnglishName() + " failed due to active validation list.";
//                        return null;
//                      }
                    String validationListId = checkField.getString("VALIDATION_LIST_ID");

                    ExecuteQueries.executeNoneQuery("Delete from GVMS_VALIDATION_LIST where FIELD_ID=" + 
                                                    id);
                    ExecuteQueries.executeNoneQuery("Delete from GVMS_VALIDATION_LIST_ITEM where VALIDATION_LIST_ID=" + 
                                                    validationListId);
                  }
                checkField.getStatement().close();
                checkField.close();
                delete = 
                    ExecuteQueries.executeNoneQuery("update GVMS_ADDITIONAL_FIELD set FIELD_STATUS_ID=3 where Field_ID=" + 
                                                    id);
                ResultSet redeemedCampaign = 
                  ExecuteQueries.executeQuery(conn,"Select * from GVMS_REDEEM_CAMPAIGN_" + campaignId);

                ResultSetMetaData columsNames = redeemedCampaign.getMetaData();
                int columnCount = columsNames.getColumnCount() + 1;
                for (int colCount = 1; colCount < columnCount; colCount++)
                  {

                    String columnName = columsNames.getColumnName(colCount);
                    if (columnName.contains(id+""))
                      {

                        Boolean alter = 
                          ExecuteQueries.execute(conn," alter table GVMS_REDEEM_CAMPAIGN_" + 
                                                 campaignId + " drop column " + columnName);
                        //System.out.println("table GVMS_REDEEM_CAMPAIGN_" + campaignId + 
//                                           " delete from it " + columnName +
//                                           " status of delete is " + alter);

                      }

                  }
                redeemedCampaign.getStatement().close();
                redeemedCampaign.close();
                checkField.close();

              }
            catch (SQLException e)
              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }


            if (delete == 1)
              {
                msgWarn = "Field " + id + "deleted";
              }
            else
              {
                msgWarn = "Field not deleted";
              }
          }
        count++;

      }
    DBConnection.closeConnections ( conn );
    msgWarn = "Field deleted";
    // Do your thing with the MyData items in List selectedDataList.
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AdditionalField_Bean");
    return null; // Navigation case.
  }

  public String editAdd_Action()
  {

    
    refactorFields();

    return "editField";
  }

  public String back_Action()
  {

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/Campaign.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setFieldArray(ArrayList<AdditionalFieldClass> fieldArray)
  {
    this.fieldArray = fieldArray;
  }

  public ArrayList<AdditionalFieldClass> getFieldArray()
  {
    return fieldArray;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }

  public void setSelectedDataList(List<AdditionalFieldClass> selectedDataList)
  {
    this.selectedDataList = selectedDataList;
  }

  public List<AdditionalFieldClass> getSelectedDataList()
  {
    return selectedDataList;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setRenderUpdateList(Boolean renderUpdateList)
  {
    this.renderUpdateList = renderUpdateList;
  }

  public Boolean getRenderUpdateList()
  {
    return renderUpdateList;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }

  public void setRenderButtonNavigation(Boolean renderButtonNavigation)
  {
    this.renderButtonNavigation = renderButtonNavigation;
  }

  public Boolean getRenderButtonNavigation()
  {
    return renderButtonNavigation;
  }


  public void setDisableDataTableCol(Boolean disableDataTableCol)
  {
    this.disableDataTableCol = disableDataTableCol;
  }

  public Boolean getDisableDataTableCol()
  {
    return disableDataTableCol;
  }

  public void setCampaignName(String campaignName)
  {
    this.campaignName = campaignName;
  }

  public String getCampaignName()
  {
    return campaignName;
  }

  public void setActivateRemoveFields(boolean activateRemoveFields)
  {
    this.activateRemoveFields = activateRemoveFields;
  }

  public boolean getActivateRemoveFields()
  {
    return activateRemoveFields;
  }
}
