package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.campaign.DAO.campaignDAO;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.admin.campaign.Engine.RunnableClass;

import com.mobinil.gvms.system.utility.ConstantsKeys;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.FileUtil;
import com.mobinil.gvms.system.utility.HttpServletUtil;
import com.mobinil.gvms.system.utility.jsfUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class UploadVoucherList
{
	
  public String fileName;
  public String campaigName;
  public UploadedFile uploadedFile;
  public RunnableClass t;
  public String msgWarn;
  campaignTableClass ctc = null;
  StringBuffer filePath =null;
  String filename = "";
public UploadVoucherList()
{
  new leftMenu().checkUserInSession();
   ctc = 
    (campaignTableClass) jsfUtils.getFromSession("campaignInfo");
  campaigName = ctc.getCampaignName();
  
  
  filePath = new StringBuffer( new java.io.File("").getAbsolutePath ( ));
  filePath.append ( System.getProperty("file.separator") );
  filePath.append ("GVMS");
  filePath.append ( System.getProperty("file.separator") );
  filePath.append ( "Templates" );
  filePath.append ( System.getProperty("file.separator") );
  
  if (ctc.getVoucherStatus ( )== ConstantsKeys.CAMPAIGN_STATUS_DIAL_AND_GENERATE_VOUCHER)
  {
     filename = "GenerateVoucherTemplate.csv";
//     filePath.append ( filename );
  }
  if (ctc.getVoucherStatus ( )== ConstantsKeys.CAMPAIGN_STATUS_DIAL_AND_VOUCHER)
  {
     filename ="DialAndVoucherTemplate.csv";
//     filePath.append ( filename );  
  }
  if (ctc.getVoucherStatus ( )== ConstantsKeys.CAMPAIGN_STATUS_VOUCHER_ONLY)
  {
     filename ="VoucherOnlyTemplate.csv";
//     filePath.append ( filename );  
  }
  
  
      
}
  public String submit()
  {
        
       if (uploadedFile==(null)){  
                FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                              "Missing File Please Pick A CSV File.", 
                                                                              null));                
                return null;
              }
              if (uploadedFile.getContentType().compareTo("application/vnd.ms-excel")!=0){
                FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                              "Missing File Type Please Check File Type To Be CVS.", 
                                                                              null));             
                  return null;
              }
              if (!uploadedFile.getName().contains("csv")){              
                FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                              "Missing File Type Please Check File Type To Have Extension csv.", 
                                                                              null));             
                
                  return null;
              } 

    Integer campaignId = ctc.getCampaignId();
      
    Integer giftId = 0;
    //Integer giftId = ctc.getGiftId();
    
 
      
    


    Integer voucherStatus = ctc.getVoucherStatus();
      
    try
      {
        
        
        t = new RunnableClass(uploadedFile, giftId, campaignId, voucherStatus);
        //System.out.println("b4 thread process");
        t.start();
        //System.out.println("after thread");


        FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                     new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                      "File upload succeed!", 
                                                                      null));

        return "insertionStatus";
        
      }
    catch (Exception e)
      {


        FacesContext.getCurrentInstance().addMessage("uploadForm", 
                                                     new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                                                                      "File upload failed with I/O error.", 
                                                                      null));

        // Always log stacktraces.
         
        return null;
      }
  }
  
  public void downloadTemplateFile()
  {
    
    FacesContext fc = FacesContext.getCurrentInstance();
    
    HttpServletResponse response = 
      (HttpServletResponse) fc.getExternalContext().getResponse();
    
    
    File csvFile = new File(filePath.toString ( ), filename);
    try
    {
      HttpServletUtil.downloadFile(response, csvFile, true);
    }
    catch (IOException e)
    {
      //System.out.println(e.toString());
    }
    fc.responseComplete();

  }
  
  public void setUploadedFile(UploadedFile uploadedFile)
  {
    this.uploadedFile = uploadedFile;
  }

  public UploadedFile getUploadedFile()
  {
    return uploadedFile;
  }

  public String getFileName()
  {
    return fileName;
  }


 


  public void setT(RunnableClass t)
  {
    this.t = t;
  }

  public Thread getT()
  {
    return t;
  }

    public void setMsgWarn(String msgWarn) {
        this.msgWarn = msgWarn;
    }

    public String getMsgWarn() {
        return msgWarn;
    }

  public void setCampaigName(String campaigName)
  {
    this.campaigName = campaigName;
  }

  public String getCampaigName()
  {
    return campaigName;
  }
}
