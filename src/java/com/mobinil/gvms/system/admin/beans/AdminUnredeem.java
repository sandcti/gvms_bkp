package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;




public class AdminUnredeem
{
	
  public String voucherNumber;
  public String dialNumber;
  public String warnMsg;  
  public AdminUnredeem()
  {
      new leftMenu().checkUserInSession();
  }

  public String unredeem_Action() throws SQLException
  {

     
     if (voucherNumber == null || dialNumber== null)
     {
       warnMsg = "Please provide dial and voucher numbers.";
       return null;
     }

    if (voucherNumber.compareTo("") == 0 || dialNumber.compareTo("") == 0)
      {
       warnMsg = "Please provide dial and voucher numbers.";
        return null;
      }

    changeVoucherStatus(2);
   
        return null;
            




      

    
  }
  public String delete_Action() throws SQLException
  {

     
     if (voucherNumber == null || dialNumber== null)
     {
       warnMsg = "Please provide dial and voucher numbers.";
       return null;
     }

    if (voucherNumber.compareTo("") == 0 || dialNumber.compareTo("") == 0)
      {
       warnMsg = "Please provide dial and voucher numbers.";
        return null;
      }

    changeVoucherStatus(5);
   
        return null;
            




      

    
  }
  
  public void changeVoucherStatus(int status) throws SQLException
  {
     Connection conn = DBConnection.getConnection();
     LoginDTO adminInfo = (LoginDTO)jsfUtils.getFromSession("adminInfo");
//     Integer voucherNumberFromRS = 0;
//     Integer campaignId = 0;
     String SQL = 
       "select Current_Voucher_Number,CAMPAIGN_ID from GVMS_Current_Voucher where CURRENT_VOUCHER_NUMBER = " + 
       voucherNumber+ " and CURRENT_VOUCHER_DIAL_NUMBER='"+dialNumber+"'" ;
     ResultSet voucherNumberRs = ExecuteQueries.executeQuery(conn,SQL);
     try
       {
         if (voucherNumberRs.next())
           {
            SQL = 
               "update GVMS_CURRENT_VOUCHER set VOUCHER_STATUS_ID="+status+" , USER_ID="+adminInfo.getSysUserId()+" where Current_Voucher_Number=" +
               voucherNumber;
            
           int resultUpdate = ExecuteQueries.executeNoneQuery(SQL);
           if (resultUpdate <= 0)
             {
               warnMsg = "The system couldn't delete this voucher number.";
               
               
             }
           else
             {
              if (status==2) warnMsg = "Unredeem complete";
              if (status==5) 
              {
                  ExecuteQueries.executeNoneQuery("insert into GVMS_VOUCHER_CHNG_STUS values((select CURRENT_VOUCHER_ID from GVMS_CURRENT_VOUCHER where Current_Voucher_Number='"+voucherNumber+"'),'"+status+"',sysdate)");
                  warnMsg = "Delete complete";
                 }
//               SQL = 
//                   "delete from GVMS_Redeem_Campaign_" + voucherNumberRs.getInt ( "CAMPAIGN_ID" ) + " where REDEEM_VOUCHER_NUMBER =" + 
//                   voucherNumber;
//               int resultDelete = ExecuteQueries.executeNoneQuery(SQL);
//               if (resultDelete <= 0 )
//                 {
//                   warnMsg = "The system couldn't delete this voucher number completely.";
//                 }
//               else
//                 {
//                  if (status==2) warnMsg = "Unredeem complete";
//                  if (status==5) warnMsg = "Delete complete";
//                 }
               
               
             }
           }
         else
         {
            warnMsg = "Invalid dial number or voucher number.";
         }
         DBConnection.cleanResultSet ( voucherNumberRs );
       }
     catch (SQLException e)
       {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
       }
     DBConnection.closeConnections ( conn );
     
  }

  public void setVoucherNumber(String voucherNumber)
  {
    this.voucherNumber = voucherNumber;
  }

  public String getVoucherNumber()
  {
    return voucherNumber;
  }

  public void setWarnMsg(String warnMsg)
  {
    this.warnMsg = warnMsg;
  }

  public String getWarnMsg()
  {
    return warnMsg;
  }

public void setDialNumber(String dialNumber)
{
   this.dialNumber = dialNumber;
}

public String getDialNumber()
{
   return dialNumber;
}
}
