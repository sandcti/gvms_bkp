package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.utility.db.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;


public class CreateGift
{
	
  public HtmlForm form1;
  public HtmlInputText giftNameInput;
  public HtmlInputText giftNameArabicInput;
  public HtmlInputText giftValueInput;
  public HtmlInputTextarea inputTextarea1;
  public HtmlCommandButton createGift_cmd;  
  public ArrayList<SelectItem> giftStatusItem = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> giftTypeItem = new ArrayList<SelectItem>();
  public String giftTypeSelected;
  public String giftStatusSelected;

  ResultSet giftTypeRS = null;
    Connection conn = null;

  public HtmlOutputLabel msgWarn;
  public HtmlCommandButton backToGift;


  public CreateGift() throws SQLException
  {

        new leftMenu().checkUserInSession();
        conn = DBConnection.getConnection();
        giftTypeRS = 
           ExecuteQueries.executeQuery(conn,"Select * FROM GVMS_GIFT_TYPE where GIFT_TYPE_STATUS_ID=1");
    try
      {
        int count = 0;


        while (giftTypeRS.next())
          {

            if (count == 0)
              {
                giftTypeItem.add(new SelectItem("--Select One--"));
              }

            giftTypeItem.add(new SelectItem(giftTypeRS.getString("GIFT_TYPE_NAME")));

            count++;
          }
        giftTypeRS.beforeFirst();

      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


  }

  public String createGift() throws SQLException
  {
	  String giftName = ((String) giftNameInput.getValue()).trim();
if (giftName.contains("-"))
{
	msgWarn.setValue("Please remove dash character ( - ) from gift name.");
    return null;
}
    ResultSet repeatGift = 
      ExecuteQueries.executeQuery(conn,"Select GIFT_NAME from GVMS_GIFT where GIFT_NAME='" + 
    		  giftName+ "'");
    try
      {
        if (repeatGift.next())
          {
            msgWarn.setValue("this gift already exists.");
            return null;
          }
        else
          {


            int gitTypeId = 0;
            int gitStatusId = 0;
            String SQL;
            if (((String) inputTextarea1.getValue()).length() > 1000)
              {
                msgWarn.setValue("Maximum characters for description are 1000.");

                return null;
              }

            if (((String) giftValueInput.getValue()).compareTo("") == 0 || 
                giftName.compareTo("") == 0 ||
                ((String) giftNameArabicInput.getValue()).trim().compareTo("") == 0 
                )
              {

                msgWarn.setValue("Please fill star boxes.");
                return null;
              }


            try
              {
                while (giftTypeRS.next())
                  {
                    if (giftTypeRS.getString("GIFT_TYPE_NAME").compareTo(giftTypeSelected) == 0)
                      {
                        gitTypeId = giftTypeRS.getInt("GIFT_TYPE_ID");
                      }
                  }
              }
            catch (SQLException e)
              {
                 new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }


              String creatorName = 
                ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
            if (giftStatusSelected.compareTo("0") != 0)
              {
                if (giftTypeSelected.compareTo("--Select One--") != 0)
                  {
                    Long seqGiftId = DBUtil.getSequenceNextVal(conn, "GVMS_GIFT_SEQ_ID");
                    SQL = 
                        "Insert into GVMS_GIFT(GIFT_ID,GIFT_NAME,GIFT_TYPE_ID,GIFT_VALUE,GIFT_DESCRIPTON,GIFT_CREATION_DATE,GIFT_STATUS_ID,GIFT_CREATEDBY,GIFT_NAME_ARABIC) " + 
                        "values ("+seqGiftId+",'" + giftName +
                        "'," + gitTypeId + "," + (String) giftValueInput.getValue() + ",'" + 
                        (String) inputTextarea1.getValue() + "'," + 
                        new DateClass().convertDate(new DateClass().getDateTime()) + "," + 
                        giftStatusSelected + ",'" + creatorName + "',?)";

                    Boolean insertGift = 
                      ExecuteQueries.executeNoneQueryPreparedOracle(conn,SQL, ((String) giftNameArabicInput.getValue()).trim());
                    if (insertGift == false)
                      {
                        UpdateClass.updateGiftInfo(seqGiftId+"", false);
                        msgWarn.setValue("One new gift inserted");
                      }
                    else
                      {
                        msgWarn.setValue("Creation failed");
                      }
                  }
                else
                  {
                    msgWarn.setValue("Please select gift type for " + 
                    		giftName + ".");
                  }
              }
            else
              {
                msgWarn.setValue("Please select status for " + giftName + 
                                 ".");
              }


            try
              {

                giftTypeRS.getStatement().close();
                giftTypeRS.close();
              }
            catch (SQLException e)
              {
                 new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }
            repeatGift.getStatement().close();
            repeatGift.close();
          }
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );
    return null;
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setGiftNameInput(HtmlInputText inputText1)
  {
    this.giftNameInput = inputText1;
  }

  public HtmlInputText getGiftNameInput()
  {
    return giftNameInput;
  }

  public void setGiftValueInput(HtmlInputText inputText1)
  {
    this.giftValueInput = inputText1;
  }

  public HtmlInputText getGiftValueInput()
  {
    return giftValueInput;
  }


  public void setInputTextarea1(HtmlInputTextarea inputTextarea1)
  {
    this.inputTextarea1 = inputTextarea1;
  }

  public HtmlInputTextarea getInputTextarea1()
  {
    return inputTextarea1;
  }

  public void setCreateGift_cmd(HtmlCommandButton commandButton1)
  {
    this.createGift_cmd = commandButton1;
  }

  public HtmlCommandButton getCreateGift_cmd()
  {
    return createGift_cmd;
  }

  public void setGiftStatusItem(ArrayList<SelectItem> giftStatusItem)
  {
    this.giftStatusItem = giftStatusItem;
  }

  public ArrayList<SelectItem> getGiftStatusItem()
  {
    return giftStatusItem;
  }

  public void setGiftTypeItem(ArrayList<SelectItem> giftTypeItem)
  {
    this.giftTypeItem = giftTypeItem;
  }

  public ArrayList<SelectItem> getGiftTypeItem()
  {
    return giftTypeItem;
  }

  public void setGiftTypeSelected(String giftTypeSelected)
  {
    this.giftTypeSelected = giftTypeSelected;
  }

  public String getGiftTypeSelected()
  {
    return giftTypeSelected;
  }

  public void setGiftStatusSelected(String giftStatusSelected)
  {
    this.giftStatusSelected = giftStatusSelected;
  }

  public String getGiftStatusSelected()
  {
    return giftStatusSelected;
  }


  public void setBackToGift(HtmlCommandButton backToGift)
  {
    this.backToGift = backToGift;
  }

  public HtmlCommandButton getBackToGift()
  {
    return backToGift;
  }

  public String back_Action()
  {
    // Add event code here...
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Gift_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CreateGift_Bean");
    return "backToGift";
  }

  public void setMsgWarn(HtmlOutputLabel msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }

  public void setGiftNameArabicInput(HtmlInputText giftNameArabicInput)
  {
    this.giftNameArabicInput = giftNameArabicInput;
  }

  public HtmlInputText getGiftNameArabicInput()
  {
    return giftNameArabicInput;
  }
}
