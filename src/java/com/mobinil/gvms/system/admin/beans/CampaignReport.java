package com.mobinil.gvms.system.admin.beans;
import com.mobinil.gvms.system.admin.campaign.DAO.campaignDAO;
import com.mobinil.gvms.system.admin.reports.DTO.*;
import com.mobinil.gvms.system.admin.reports.dao.ReportDAO;
import com.mobinil.gvms.system.cs.beans.HistoryBean;
import com.mobinil.gvms.system.cs.dao.CSDao;
import com.mobinil.gvms.system.utility.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class CampaignReport
{



 


  //  public ArrayList<campaignTableClass> campaignArray = new ArrayList<campaignTableClass>();
  public ArrayList<SelectItem> campaignItems = new ArrayList<SelectItem>();

  ArrayList<String> getIdArray;
  ArrayList<String> getGiftIdArray;
  ArrayList<String> getGiftTypeIdArray;
  ArrayList<String> getUserTypeIdArray;
  String campaignId;

  public ArrayList<SelectItem> giftTypeItems = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> giftItems = new ArrayList<SelectItem>();

  public ArrayList<SelectItem> userTypeItems = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> statusItems = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> typeItems = new ArrayList<SelectItem>();
  public ArrayList<CampaignReportDTO> filterResult;
  public ArrayList<VoucherReportDTO> posMoneyArray;
  public ArrayList<VoucherReportDTO> voucherResult;
  public ArrayList<TimeChartDTO> timeResult;

  public String selectedItemCampaignName;
  public String selectedItemGiftName;
  public String selectedItemUserTypeName;
  public String selectedStatusName;
  public String selectedTypeName;
  public String selectedItemGiftTypeName;
  public String selectedItemTimeOption;
  public String selectedItemGiftOption;
  public String selectedItemVoucherStatus;
  public String totalGiftValues;

  public HtmlInputHidden input1;


  int pageIndex = 0;

  public String sortField = null;
  public boolean sortAscending = true;

  public HtmlDataTable dataTable1;
  public HtmlDataTable dataTable2;
  public HtmlDataTable dataTable3;
  public HtmlDataTable dataTable4;
  public HtmlDataTable dataTable6;
  public Boolean renderChart = false;
  public Boolean renderBarChart = false;

  public Boolean renderFilter = false;
  public Boolean renderDataTable = false;
  public Boolean renderDataTable6 = false;
  public Boolean renderDataTable2 = false;
  public Boolean renderExportToCSV = false;
  public Boolean renderPOS = false;
  public Boolean renderUser = false;
  public Boolean renderdayOfWeek = false;
  public Boolean renderNormalField = false;
  
  

  public Boolean renderCleanDate = false;


  public Boolean renderCampaignChart = false;
  public HtmlForm form1;
  public Integer totalVoucher;
  public Integer redeemVoucher;
  public Integer expiredVoucher;
  int campaignStatus;
  public String moreVoucherValue;
  public String dialValue;
  public String posValue;
  public String userValue;
  public String statusValue;
  public String hiddenValue;
  public String timeTableName;
  public String timeTableValue;
  public Date filterFromDate;
  public Date filterToDate;
  int report = 0;
  String path;
  String SQLTotalVoucher = "";
  String SQLRedeemVoucher = "";
  String SQLExpiredVoucher = "";
  String SQLCampaign = "";
  public ArrayList<SelectItem> campaigns = new ArrayList<SelectItem>();    
  public ArrayList<SelectItem> gifts = null;
  public ArrayList<SelectItem> channels = null;
  public ArrayList<SelectItem> subChannels = null;
  public String selectedCampaign;
  public String selectedGift;
  public String selectedChannel;
  public String selectedSubChannel;
  public String accountNumber;
  public String dialNumber;
  public HashMap campaignAndGifts;


public CampaignReport() throws SQLException
  {
      new leftMenu().checkUserInSession();
      Connection conn = DBConnection.getConnection();
    path = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRequestURI();
    //System.out.println("path3 is "+path);
    
    ResultSet campaignRS = 
       ExecuteQueries.executeQuery(conn,"SELECT CAMPAIGN_NAME,CAMPAIGN_ID FROM GVMS_CAMPAIGN where CAMPAIGN_STATUS_ID !=4");
      ResultSet userTypeRS = 
        ExecuteQueries.executeQuery(conn,"SELECT USER_TYPE_NAME,USER_TYPE_ID FROM GVMS_USER_TYPE where USER_TYPE_STATUS_ID!=3");

      ResultSet giftRS = 
        ExecuteQueries.executeQuery(conn,"SELECT GIFT_NAME,GIFT_ID FROM GVMS_GIFT where GIFT_STATUS_ID!=3");
      ResultSet giftTypeRS = 
        ExecuteQueries.executeQuery(conn,"SELECT GIFT_TYPE_NAME,GIFT_TYPE_ID FROM GVMS_GIFT_TYPE where GIFT_TYPE_STATUS_ID!=3");

    
    int counter = 0;
    getIdArray = new ArrayList<String>();
    getGiftIdArray = new ArrayList<String>();
    getUserTypeIdArray = new ArrayList<String>();
    getGiftTypeIdArray = new ArrayList<String>();
    
    campaigns = new ArrayList<SelectItem>();
    campaigns.add(0, new SelectItem("-1","*"));
    
    gifts = new ArrayList<SelectItem>();
  gifts.add(0, new SelectItem("-1","*"));
    
    channels = new ArrayList<SelectItem>();
    channels.add(0, new SelectItem("-1","*"));
    
    subChannels = new ArrayList<SelectItem>();
    subChannels.add(0, new SelectItem("-1","*"));

    
    try {    
       campaignAndGifts = campaignDAO.getCampaigns("history");
    } catch (SQLException e) {
       
       new com.mobinil.gvms.system.utility.PrintException().printException(e);
    } 
  
  
  
  campaigns = (ArrayList <SelectItem>)campaignAndGifts.get ( "all" );
  
    try
      {
    	campaignItems.add(new SelectItem("*"));
        while (campaignRS.next())
          {           
            campaignItems.add(new SelectItem(campaignRS.getString("CAMPAIGN_NAME")));
            getIdArray.add(campaignRS.getString("CAMPAIGN_NAME") + 
                           campaignRS.getInt("CAMPAIGN_ID"));
           
          }
        campaignRS.beforeFirst();

        counter = 0;
        while (userTypeRS.next())
          {
            if (counter == 0)
              {
                userTypeItems.add(new SelectItem("*"));
              }
            //System.out.println("value in usertypearray is : " + 
//                               userTypeRS.getString("USER_TYPE_NAME") +
//                               userTypeRS.getInt("USER_TYPE_ID"));
            userTypeItems.add(new SelectItem(userTypeRS.getString("USER_TYPE_NAME")));
            getUserTypeIdArray.add(userTypeRS.getString("USER_TYPE_NAME") + 
                                   userTypeRS.getInt("USER_TYPE_ID"));
            counter++;
          }
        userTypeRS.beforeFirst();

        counter = 0;
        while (giftRS.next())
          {
            if (counter == 0)
              {
                giftItems.add(new SelectItem("*"));
              }
            giftItems.add(new SelectItem(giftRS.getString("GIFT_NAME")));
            getGiftIdArray.add(giftRS.getString("GIFT_NAME") + giftRS.getInt("GIFT_ID"));
            counter++;
          }
        giftRS.beforeFirst();


        counter = 0;
        while (giftTypeRS.next())
          {
            if (counter == 0)
              {
                giftTypeItems.add(new SelectItem("*"));
              }
            //System.out.println("GIFT_TYPE_NAME is: " + giftTypeRS.getString("GIFT_TYPE_NAME"));
            //System.out.println("GIFT_TYPE_ID is: " + giftTypeRS.getString("GIFT_TYPE_ID"));
            giftTypeItems.add(new SelectItem(giftTypeRS.getString("GIFT_TYPE_NAME")));
            getGiftTypeIdArray.add(giftTypeRS.getString("GIFT_TYPE_NAME") + 
                                   giftTypeRS.getInt("GIFT_TYPE_ID"));
            counter++;
          }
        giftTypeRS.beforeFirst();
      }
    catch (SQLException e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  statusItems = getItemsFromHashMap(ReportDAO.getLookupTable(conn, "GVMS_WS_STATUS", "STATUS_ID,STATUS_NAME", ""), true);
  typeItems = getItemsFromHashMap(ReportDAO.getLookupTable(conn, "GVMS_WS_TYPE", "TYPE_ID,TYPE_NAME", ""), true);

    DBConnection.closeConnections ( conn );
  }

public String getCampaigGift(){
   gifts=null;
    gifts = new ArrayList<SelectItem>();
  
  //System.out.println("here in getCampaigGift");
  if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
  {
    selectedCampaign="0";
  }
  //System.out.println ("selected campaign id is "+ selectedCampaign);

  HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
  if (ff!=null)
  gifts = (ArrayList <SelectItem>)(ff).get ( "gifts" );
  
  getCampaigChannel();

return null;
}

public void getCampaigChannel(){
 channels = new ArrayList<SelectItem>();
  
  
  if (selectedCampaign==null||selectedCampaign.compareTo("")==0||selectedCampaign.compareTo("-1")==0)
  {
      selectedCampaign="0";
  }
  
  HashMap ff =((HashMap)campaignAndGifts.get(selectedCampaign));
  
  if (ff!=null){      
  channels = (ArrayList <SelectItem>)(ff).get ( "channels" );
  
  }
  
  



} 

public void getSubChannel(){
 subChannels = new ArrayList<SelectItem>();
 
 
 if (selectedChannel!=null && selectedChannel.compareTo ( "-1" )!=0){
     try
    {
       subChannels = CSDao.getSubChannel ( selectedChannel,"history" );
       
    }
    catch ( SQLException e )
    {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
       subChannels.add(0, new SelectItem("-1","error"));
     
    }
 }

}

  public String timeChart_Action() throws SQLException
  {

    renderBarChart = true;
    renderDataTable = true;

    timeResult = new ArrayList<TimeChartDTO>();
    String condition = "";

    if (selectedItemCampaignName.compareTo("*") != 0)
      {

        for (int count = 0; count < getIdArray.size(); count++)
          {
            if (getIdArray.get(count).contains((CharSequence) selectedItemCampaignName))
              {
                campaignId = getIdArray.get(count).replaceFirst(selectedItemCampaignName, "");
                
              }
          }
        condition += " AND t1.CAMPAIGN_ID=" + campaignId;
      }


    if (selectedItemGiftName.compareTo("*") != 0)
      {
        String giftId = "";
        for (int count = 0; count < getGiftIdArray.size(); count++)
          {
            if (getGiftIdArray.get(count).contains((CharSequence) selectedItemGiftName))
              {
                giftId = getGiftIdArray.get(count).replaceFirst(selectedItemGiftName, "");

              }
          }
        condition += " AND t1.GIFT_ID=" + giftId;
      }

    if (selectedItemUserTypeName.compareTo("*") != 0)
      {
        String userTypeId = "";
        for (int count = 0; count < getUserTypeIdArray.size(); count++)
          {
            if (getUserTypeIdArray.get(count).contains((CharSequence) selectedItemUserTypeName))
              {
                userTypeId = 
                    getUserTypeIdArray.get(count).replaceFirst(selectedItemUserTypeName, "");

              }
          }
        condition += " AND t1.USER_TYPE_ID=" + userTypeId;
      }

    if (selectedItemGiftTypeName.compareTo("*") != 0)
      {
        String GiftTypeId = "";
        for (int count = 0; count < getGiftTypeIdArray.size(); count++)
          {
            if (getGiftTypeIdArray.get(count).contains((CharSequence) selectedItemGiftTypeName))
              {
                GiftTypeId = 
                    getGiftTypeIdArray.get(count).replaceFirst(selectedItemGiftTypeName, "");

              }
          }
        condition += " AND t1.GIFT_TYPE_ID=" + GiftTypeId;
      }

    if (filterFromDate != null && filterToDate != null)
      {

        condition += 
            " AND t1.CURRENT_VOUCHER_END_DATE >= " + convertDate(filterFromDate,"from") + " and t1.CURRENT_VOUCHER_END_DATE <= " + 
            convertDate(filterToDate,"to");
      }
    String SQL = "";
    String ss = "";
    if (selectedItemGiftOption.compareTo("count") == 0)
      {
        ss = "count(t3.gift_name";
        timeTableValue = "Count";
      }
    else
      {
        ss = "sum(t3.gift_value";
        timeTableValue = "Value";
      }

    if (selectedItemTimeOption.compareTo("day") == 0)
      {
        renderdayOfWeek = false;
        renderNormalField = true;
        timeTableName = "HOUR";
        SQL = 
            "Select to_char(t1.CURRENT_VOUCHER_END_DATE,'hh24') hour ," + ss + ") aa from GVMS_CURRENT_VOUCHER t1 left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_GIFT_TYPE t2 on t1.GIFT_TYPE_ID =t2.GIFT_TYPE_ID   where 1=1 and t1.VOUCHER_STATUS_ID = 1" + 
            condition + " group by to_char(t1.CURRENT_VOUCHER_END_DATE,'hh24')";
      }

    if (selectedItemTimeOption.compareTo("year") == 0)
      {
        renderdayOfWeek = true;
        renderNormalField = false;
        timeTableName = "YEAR";
        SQL = 
            "Select to_char(t1.CURRENT_VOUCHER_END_DATE,'Mon-YY') Year ," + ss + ") aa from GVMS_CURRENT_VOUCHER t1 left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_GIFT_TYPE t2 on t1.GIFT_TYPE_ID=t2.GIFT_TYPE_ID   where 1=1 and t1.VOUCHER_STATUS_ID = 1" + 
            condition + 
            " group by to_char(t1.CURRENT_VOUCHER_END_DATE,'Mon-YY') order by to_Date(to_char(t1.CURRENT_VOUCHER_END_DATE,'Mon-YY'),'mon-yy')";
      }
    if (selectedItemTimeOption.compareTo("week") == 0)
      {
        renderdayOfWeek = true;
        renderNormalField = false;
        timeTableName = "WEEKLY";
        SQL = 
            "Select to_char(t1.CURRENT_VOUCHER_END_DATE,'Day') week," + ss + ") aa from GVMS_CURRENT_VOUCHER t1 left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_GIFT_TYPE t2 on t1.GIFT_TYPE_ID =t2.GIFT_TYPE_ID   where 1=1 and t1.VOUCHER_STATUS_ID = 1" + 
            condition + 
            " group by to_char(t1.CURRENT_VOUCHER_END_DATE,'Day') ,to_char(t1.CURRENT_VOUCHER_END_DATE,'d') order by to_char(t1.CURRENT_VOUCHER_END_DATE,'d')";
      }

    if (selectedItemTimeOption.compareTo("month") == 0)
      {
        renderdayOfWeek = false;
        renderNormalField = true;
        timeTableName = "MONTHLY";
        SQL = 
            "Select to_char(to_date(t1.CURRENT_VOUCHER_END_DATE),'DD') week ," + ss + ") aa from GVMS_CURRENT_VOUCHER t1 left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_GIFT_TYPE t2 on t1.GIFT_TYPE_ID =t2.GIFT_TYPE_ID  where 1=1 and t1.VOUCHER_STATUS_ID = 1" + 
            condition + " group by to_char(to_date(t1.CURRENT_VOUCHER_END_DATE),'DD')";
      }

    Connection conn = DBConnection.getConnection();
    ResultSet resultFilter = ExecuteQueries.executeQuery(conn,SQL);
    try
      {

        if (selectedItemCampaignName.compareTo("*") != 0&&campaignId.compareTo("")!=0&&getCampaignStatus(conn,campaignId,"Campaign_status_id")==3)
        {
        SQL = SQL.replace("GVMS_CURRENT_VOUCHER","GVMS_TEMP_CURRENT");
         resultFilter = ExecuteQueries.executeQuery(conn,SQL);
          while (resultFilter.next())
            {              
              TimeChartDTO tcc = new TimeChartDTO();
              tcc.setDayOfWeekName(resultFilter.getString(1));
              //                System.out.pritnln("test = "+selectedItemGiftOption);
//              if (selectedItemGiftOption.compareTo("count") == 0)
//                {
//                  tcc.setGiftCount(resultFilter.getInt("aa") + "");
//                }
//              else
//                {
                  tcc.setGiftValue(resultFilter.getInt("aa") + "");
//                }


              timeResult.add(tcc);
            }
        
        }
        else{
        while (resultFilter.next())
          {

            TimeChartDTO tcc = new TimeChartDTO();
            tcc.setDayOfWeekName(resultFilter.getString(1));
            //                System.out.pritnln("test = "+selectedItemGiftOption);
//            if (selectedItemGiftOption.compareTo("count") == 0)
//              {
//                tcc.setGiftCount(resultFilter.getInt("aa") + "");
//              }
//            else
//              {
                tcc.setGiftValue(resultFilter.getInt("aa") + "");
//              }


            timeResult.add(tcc);
          }
        }
          
        resultFilter.getStatement().close();
        resultFilter.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );
    return null;
  }

  public String campaign_Action()
  {

    campaignItems.set(0, new SelectItem("--Select one--"));
    renderDataTable = false;
    renderUser = false;
    renderPOS = false;
    renderCampaignChart = true;
    renderFilter = false;
    renderCleanDate = true;

    return null;
  }


  public void emptyValues()
  {
    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("CampaignReport_Bean"))
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CampaignReport_Bean");
      }
    jsfUtils.putInSession ( false , "isDeleted" );
  }

  public String find_Action() throws SQLException //voucher chart
  {
    renderDataTable2 = true;
    voucherResult = new ArrayList<VoucherReportDTO>();

    StringBuffer condition = new StringBuffer("");
    
    if (dialValue!=null&&dialValue.compareTo("") != 0)
      {    	
        condition.append (  " AND t1.CURRENT_VOUCHER_DIAL_NUMBER='");
        condition.append (  dialValue );
        condition.append (  "'" );
      }
//    if (filterFromDate != null && filterToDate != null)
//      {
//
//        condition += 
//            " AND t1.CURRENT_VOUCHER_END_DATE >= " + convertDate(filterFromDate,"from") + " and t1.CURRENT_VOUCHER_END_DATE <= " + 
//            convertDate(filterToDate,"to");
//      }
    boolean bool = (Boolean)jsfUtils.getFromSession ( "isDeleted" );
    if (bool)selectedItemVoucherStatus="5";
    
    if (selectedCampaign!=null && selectedCampaign.compareTo("-1")!=0&&selectedCampaign.compareTo("")!=0)
      {
       condition.append (  " AND t1.CAMPAIGN_ID=");
       condition.append (  selectedCampaign );
      }
    if (selectedGift!=null && selectedGift.compareTo("-1")!=0&&selectedGift.compareTo("")!=0)
      { 
        condition.append (  " AND t1.GIFT_ID=");
        condition.append (  selectedGift );
      }
    if (selectedItemVoucherStatus!=null && selectedItemVoucherStatus.compareTo("0") != 0)
      {
        condition.append (  " AND t2.VOUCHER_STATUS_ID=");
        condition.append (  selectedItemVoucherStatus );
      }
//    if (accountNumber!=null && accountNumber.compareTo ( "" )!=0)
//    {
//       condition.append (  " AND T7.ACCOUNT_NUMBER='");
//       condition.append (  accountNumber );
//       condition.append (  "'" );
//    }
    
    if (selectedChannel!=null && selectedChannel.compareTo("-1")!=0 && selectedChannel.compareTo("")!=0)
    {
     condition.append (  " AND t1.USER_TYPE_ID=");
     condition.append (  selectedChannel );
    }
    
    if (selectedSubChannel!=null && selectedSubChannel.compareTo("-1")!=0 && selectedSubChannel.compareTo("")!=0)
    {
     condition.append (  " AND t1.USER_ID=");
     condition.append (  selectedSubChannel );
    }
    
    
    
    
    
    

    

    
    
    Connection conn = DBConnection.getConnection();
    {
       
       
//    if (moreVoucherValue.compareTo("") != 0)
//      {    	
//        SQL = 
//            "select count(CURRENT_VOUCHER_DIAL_NUMBER ) REDEEMCOUNT,sum(GIFT_VALUE) cost ,to_char(CURRENT_VOUCHER_DIAL_NUMBER) dial  from GVMS_CURRENT_VOUCHER group by CURRENT_VOUCHER_DIAL_NUMBER having count(CURRENT_VOUCHER_DIAL_NUMBER)>" + 
//            moreVoucherValue;
//       
//
//        ResultSet findDialRS = ExecuteQueries.executeQuery(conn,SQL);
//        try
//          {
//
//            renderDataTable6 = false;
//            if (selectedCampaign!=null &&selectedCampaign.compareTo("-1") != 0&&selectedCampaign.compareTo("")!=0)
//            {
//            	boolean dd=false;
//            	if (getCampaignStatus(conn,selectedCampaign,"Campaign_status_id")==3)SQL = SQL.replace("GVMS_CURRENT_VOUCHER","GVMS_TEMP_CURRENT");
//            	if (getCampaignStatus(conn,selectedCampaign,"VOUCHER_TYPE_ID")==1)dd = true;
//              
//              findDialRS = ExecuteQueries.executeQuery(conn,SQL);
//              while (findDialRS.next())
//                {
//                
//                  VoucherReportDTO vrc = new VoucherReportDTO();
//                  if(dd) vrc.setDialNumber("");
//                  else vrc.setDialNumber(findDialRS.getString("dial"));
//                  vrc.setGiftValue(findDialRS.getInt("cost"));
//                  vrc.setRedeemcount(findDialRS.getInt("REDEEMCOUNT"));
//
//                  voucherResult.add(vrc);
//                }
//            }
//            else{
//            
//            
//            while (findDialRS.next())
//              {
//            
//                VoucherReportDTO vrc = new VoucherReportDTO();
//                vrc.setDialNumber(findDialRS.getString("dial"));
//                vrc.setGiftValue(findDialRS.getInt("cost"));
//                vrc.setRedeemcount(findDialRS.getInt("REDEEMCOUNT"));
//
//                voucherResult.add(vrc);
//              }
//          }
//              
//            findDialRS.getStatement().close();
//            findDialRS.close();
//          }
//        catch (SQLException e)
//          {
//             
//          }
//
//      }
//    else
//      {        
        renderDataTable2 = true;
        StringBuffer searchVoucherSQL =new StringBuffer("");


        searchVoucherSQL.append(" SELECT   T4.GIFT_VALUE COST, T2.VOUCHER_STATUS_ID, T3.CAMPAIGN_STATUS_ID,");
        searchVoucherSQL.append(" T3.VOUCHER_TYPE_ID,");
        searchVoucherSQL.append(" TO_CHAR (T1.CURRENT_VOUCHER_DIAL_NUMBER) CURRENT_VOUCHER_DIAL_NUMBER,");
        searchVoucherSQL.append(" T2.VOUCHER_STATUS, COUNT (*) AS REDEEMCOUNT, T3.CAMPAIGN_NAME,");
        searchVoucherSQL.append(" T4.GIFT_NAME, T5.USER_TYPE_NAME, (select USER_POS_CODE from GVMS_USER where SYSTEM_USER_ID=T1.USER_ID)USER_POS_CODE, ");
        searchVoucherSQL.append(" T1.REDEMPTION_DATE, T1.CURRENT_VOUCHER_END_DATE CREATION_DATE,");
        searchVoucherSQL.append(" T1.VOUCHER_EXPIRY_DATE,");
        searchVoucherSQL.append(" T7.VOUCHER_CHANGE_DATE,");
        searchVoucherSQL.append(" NVL((SELECT CONCAT('Admin-',ADMIN_NAME) FROM GVMS_ADMIN WHERE SYSTEM_USER_ID=T1.USER_ID),(SELECT CONCAT('CS-',USER_FULL_NAME) FROM GVMS_CS WHERE SYSTEM_USER_ID=T1.USER_ID)) PERSONDELETENAME ");
        if (getCampaignStatus(conn, selectedCampaign, "Campaign_status_id") == 3) {
            searchVoucherSQL.append(" FROM GVMS_TEMP_CURRENT T1, ");
        } else {
            searchVoucherSQL.append(" FROM GVMS_CURRENT_VOUCHER T1, ");
        }
        searchVoucherSQL.append(" GVMS_VOUCHER_STATUS T2,");
        searchVoucherSQL.append(" GVMS_CAMPAIGN T3,");
        searchVoucherSQL.append(" GVMS_GIFT T4,");
        searchVoucherSQL.append(" GVMS_USER_TYPE T5,");
        searchVoucherSQL.append(" GVMS_SYSTEM_USER T6,         ");
        searchVoucherSQL.append(" gvms_voucher_chng_stus t7         ");
        searchVoucherSQL.append(" WHERE T1.VOUCHER_STATUS_ID = T2.VOUCHER_STATUS_ID");
        searchVoucherSQL.append(" AND T1.CAMPAIGN_ID = T3.CAMPAIGN_ID");
        searchVoucherSQL.append(" AND T1.GIFT_ID = T4.GIFT_ID");
        searchVoucherSQL.append(" AND T1.USER_TYPE_ID = T5.USER_TYPE_ID");
        searchVoucherSQL.append(" AND T1.USER_ID = T6.SYSTEM_USER_ID");
        if (!bool){
        searchVoucherSQL.append("(+)");
        }
        searchVoucherSQL.append(condition);
        searchVoucherSQL.append(" and t7.VOUCHER_ID (+) = t1.CURRENT_VOUCHER_ID");
        searchVoucherSQL.append(" GROUP BY T2.VOUCHER_STATUS_ID,");
        searchVoucherSQL.append(" T4.GIFT_VALUE,");
        searchVoucherSQL.append(" T1.CURRENT_VOUCHER_DIAL_NUMBER,");
        searchVoucherSQL.append(" T2.VOUCHER_STATUS,");
        searchVoucherSQL.append(" T3.CAMPAIGN_NAME,");
        searchVoucherSQL.append(" T4.GIFT_NAME,");
        searchVoucherSQL.append(" T5.USER_TYPE_NAME,");
//        searchVoucherSQL.append(" T6.USER_POS_CODE,    ");
        searchVoucherSQL.append(" T3.CAMPAIGN_STATUS_ID,");
        searchVoucherSQL.append(" T3.VOUCHER_TYPE_ID,");
        searchVoucherSQL.append(" T1.REDEMPTION_DATE,");
        searchVoucherSQL.append(" T1.CURRENT_VOUCHER_END_DATE,");
        searchVoucherSQL.append(" T1.VOUCHER_EXPIRY_DATE,");
        searchVoucherSQL.append(" T1.USER_ID,");
        searchVoucherSQL.append(" T7.VOUCHER_CHANGE_DATE");


//        searchVoucherSQL.append ( " SELECT   T4.GIFT_VALUE COST, T2.VOUCHER_STATUS_ID,T3.CAMPAIGN_STATUS_ID,T3.VOUCHER_TYPE_ID,");
//        searchVoucherSQL.append ( "TO_CHAR (T1.CURRENT_VOUCHER_DIAL_NUMBER) CURRENT_VOUCHER_DIAL_NUMBER,");
//        searchVoucherSQL.append ( "T2.VOUCHER_STATUS, COUNT (*) AS REDEEMCOUNT, T3.CAMPAIGN_NAME,");
//        searchVoucherSQL.append ( "T4.GIFT_NAME,T5.USER_TYPE_NAME,T6.USER_POS_CODE,T7.ACCOUNT_NUMBER");
//        searchVoucherSQL.append ( ",T1.REDEMPTION_DATE,T1.CURRENT_VOUCHER_END_DATE Creation_date,T1.VOUCHER_EXPIRY_DATE");
//
//
//        if (getCampaignStatus(conn,selectedCampaign,"Campaign_status_id")==3) searchVoucherSQL.append ( " FROM GVMS_TEMP_CURRENT T1 ");
//        else searchVoucherSQL.append ( " FROM GVMS_CURRENT_VOUCHER T1 ");
//
//
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_VOUCHER_STATUS T2 ON T1.VOUCHER_STATUS_ID = T2.VOUCHER_STATUS_ID");
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_CAMPAIGN T3 ON T1.CAMPAIGN_ID = T3.CAMPAIGN_ID");
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_GIFT T4 ON T1.GIFT_ID = T4.GIFT_ID         ");
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_USER_TYPE T5 ON T1.USER_TYPE_ID = T5.USER_TYPE_ID");
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_USER T6 ON T1.USER_ID = T6.USER_ID         ");
//        searchVoucherSQL.append ( " LEFT JOIN GVMS_CS_LOGS T7 ON T1.CURRENT_VOUCHER_NUMBER = T7.VOUCHER_NUMBER");
//        searchVoucherSQL.append ( " WHERE 1 = 1 ");
//        searchVoucherSQL.append ( condition );
//        searchVoucherSQL.append ( " GROUP BY T2.VOUCHER_STATUS_ID,");
//        searchVoucherSQL.append ( " T4.GIFT_VALUE,");
//        searchVoucherSQL.append ( " T1.CURRENT_VOUCHER_DIAL_NUMBER,");
//        searchVoucherSQL.append ( " T2.VOUCHER_STATUS,");
//        searchVoucherSQL.append ( " T3.CAMPAIGN_NAME,");
//        searchVoucherSQL.append ( " T4.GIFT_NAME,T5.USER_TYPE_NAME,T6.USER_POS_CODE");
//        searchVoucherSQL.append ( " ,T7.ACCOUNT_NUMBER,T3.CAMPAIGN_STATUS_ID,T3.VOUCHER_TYPE_ID");
//        searchVoucherSQL.append ( " ,T1.REDEMPTION_DATE,T1.CURRENT_VOUCHER_END_DATE ,T1.VOUCHER_EXPIRY_DATE");
        
        
        System.out.println(searchVoucherSQL);

        
        try
          {
           ResultSet findDialRS = ExecuteQueries.executeQuery(conn,searchVoucherSQL.toString ( ));  
            
            
               
               boolean dialMoreThanFlag = false;
              int intDialMoreThan = 0;              
               
               if (moreVoucherValue!=null && moreVoucherValue.compareTo ( "" )!=0 && moreVoucherValue.compareTo ( "0" )!=0) 
                  {
                  intDialMoreThan = Integer.parseInt ( moreVoucherValue );
                  dialMoreThanFlag=true;
                  }
               
               
                  
               
               
            while (findDialRS.next())
              {
              int redeemedCount = findDialRS.getInt("REDEEMCOUNT");
              int campaignType = findDialRS.getInt("VOUCHER_TYPE_ID");
//              int campaignStatus = findDialRS.getInt("CAMPAIGN_STATUS_ID");
              
              
              
                VoucherReportDTO vrc = new VoucherReportDTO();
                vrc.setDialNumber(findDialRS.getString("CURRENT_VOUCHER_DIAL_NUMBER"));                
                vrc.setCreationDate ( findDialRS.getTimestamp ("Creation_date"));
                vrc.setRedemptionDate ( findDialRS.getTimestamp ("REDEMPTION_DATE"));
                vrc.setExpiryDate  ( findDialRS.getTimestamp ("VOUCHER_EXPIRY_DATE"));                
                vrc.setDeletedDate( findDialRS.getTimestamp ("VOUCHER_CHANGE_DATE"));
                vrc.setStatus(findDialRS.getString("VOUCHER_STATUS"));
                vrc.setRedeemcount(redeemedCount);
                vrc.setGiftValue(findDialRS.getInt("cost"));
                vrc.setGiftName(findDialRS.getString("GIFT_NAME"));
                vrc.setCampaignName(findDialRS.getString("CAMPAIGN_NAME"));
                vrc.setAccountNumber ( /*findDialRS.getString("ACCOUNT_NUMBER")*/"" );
                vrc.setChannel  ( findDialRS.getString("USER_TYPE_NAME") );
                vrc.setSubChannel ( findDialRS.getString("USER_POS_CODE") );
                vrc.setPersonName(findDialRS.getString("PERSONDELETENAME"));
                if (campaignType==1)vrc.setDialNumber("");
                
                if (dialMoreThanFlag)
                {
                   if (redeemedCount>=intDialMoreThan) voucherResult.add(vrc);
                }
                else voucherResult.add(vrc);
              
              }
            
               
            findDialRS.getStatement().close();
            findDialRS.close();
          }
        catch (SQLException e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

      }

    DBConnection.closeConnections ( conn );
    


    return null;


  }

  public String goFilter_Action() throws SQLException
  {

    campaign_Report();

    return null;
  }

  public void chart_Action() throws SQLException
  {

    renderChart = true;
    String selectedItem = "";
    ResultSet campaignInfoRS = null;
    ResultSet totalVoucherRS = null;
    ResultSet redeemVoucherRS = null;
    ResultSet expiredVoucherRS = null;
    String campaignID = "";
    boolean campaignDeletedFlag = false;
    Connection conn = DBConnection.getConnection();
    if (selectedItemCampaignName.compareTo("*") == 0)
      {
        selectedItem = "";
        SQLTotalVoucher = 
            "select count (*) CAMPAIGN_REPORT_TOTAL_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 AND t2.CAMPAIGN_STATUS_ID !=4" + 
            selectedItem;
        totalVoucherRS = ExecuteQueries.executeQuery(conn,SQLTotalVoucher);
        SQLRedeemVoucher = 
            "select count (*) CAMPAIGN_REPORT_REDEEM_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 AND VOUCHER_STATUS_ID = 1  AND t2.CAMPAIGN_STATUS_ID !=4 " + 
            selectedItem;
        redeemVoucherRS = ExecuteQueries.executeQuery(conn,SQLRedeemVoucher);

        SQLExpiredVoucher = 
            "select count (*) CAMPAIGN_REPORT_EXPIR_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 AND VOUCHER_STATUS_ID = 2 AND t2.CAMPAIGN_STATUS_ID !=4" + 
            selectedItem;
        expiredVoucherRS = ExecuteQueries.executeQuery(conn,SQLExpiredVoucher);


        try
          {
            if (totalVoucherRS.next() && redeemVoucherRS.next() && expiredVoucherRS.next())
              {
                totalVoucher = totalVoucherRS.getInt("CAMPAIGN_REPORT_TOTAL_VOUCHER");
                redeemVoucher = redeemVoucherRS.getInt("CAMPAIGN_REPORT_REDEEM_VOUCHER");
                expiredVoucher = expiredVoucherRS.getInt("CAMPAIGN_REPORT_EXPIR_VOUCHER");
              }

            totalVoucherRS.getStatement().close();
            totalVoucherRS.close();
            redeemVoucherRS.getStatement().close();
            redeemVoucherRS.close();
            expiredVoucherRS.getStatement().close();
            expiredVoucherRS.close();
          }
        catch (SQLException e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

      }
    else
      {
            try
              {
       SQLCampaign = 
            " SELECT CAMPAIGN_ID,CAMPAIGN_STATUS_ID from gvms_Campaign WHERE CAMPAIGN_NAME='" + 
            selectedItemCampaignName + "'";
        campaignInfoRS = ExecuteQueries.executeQuery(conn,SQLCampaign);
      if (campaignInfoRS.next())
      {
        campaignID = campaignInfoRS.getInt(1)+"";
      }
      
        if (campaignID.compareTo("")!=0&&getCampaignStatus(conn,campaignID,"Campaign_status_id")==3)
        {
          campaignDeletedFlag = true;
        }
        
        selectedItem = "and t2.CAMPAIGN_Name = '" + selectedItemCampaignName + "'";
        SQLTotalVoucher = 
            "select count (*) CAMPAIGN_REPORT_TOTAL_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 " + 
            selectedItem;
//      if(campaignDeletedFlag)SQLTotalVoucher=SQLTotalVoucher.replace("gvms_current_voucher","GVMS_TEMP_CURRENT");

        SQLRedeemVoucher = 
            "select count (*) CAMPAIGN_REPORT_REDEEM_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 AND VOUCHER_STATUS_ID = 1" + 
            selectedItem;
//            if(campaignDeletedFlag)SQLRedeemVoucher=SQLRedeemVoucher.replace("gvms_current_voucher","GVMS_TEMP_CURRENT");
        
        SQLExpiredVoucher = 
            "select count (*) CAMPAIGN_REPORT_EXPIR_VOUCHER from gvms_current_voucher t1 LEFT JOIN GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID=t2.CAMPAIGN_ID WHERE 1=1 AND VOUCHER_STATUS_ID = 2" + 
            selectedItem;
//            if(campaignDeletedFlag)SQLExpiredVoucher=SQLExpiredVoucher.replace("gvms_current_voucher","GVMS_TEMP_CURRENT");
            
        campaignInfoRS = ExecuteQueries.executeQuery(conn,SQLCampaign);            
        redeemVoucherRS = ExecuteQueries.executeQuery(conn,SQLRedeemVoucher);    
        expiredVoucherRS = ExecuteQueries.executeQuery(conn,SQLExpiredVoucher);
        totalVoucherRS = ExecuteQueries.executeQuery(conn,SQLTotalVoucher);

        
            if (campaignInfoRS.next() && totalVoucherRS.next() && redeemVoucherRS.next() && 
                expiredVoucherRS.next())
              {
                totalVoucher = totalVoucherRS.getInt("CAMPAIGN_REPORT_TOTAL_VOUCHER");
                redeemVoucher = redeemVoucherRS.getInt("CAMPAIGN_REPORT_REDEEM_VOUCHER");
                expiredVoucher = expiredVoucherRS.getInt("CAMPAIGN_REPORT_EXPIR_VOUCHER");
                campaignStatus = campaignInfoRS.getInt("CAMPAIGN_STATUS_ID");
                campaignId = campaignInfoRS.getInt("CAMPAIGN_ID") + "";
              }
            campaignInfoRS.getStatement().close();
            campaignInfoRS.close();
            totalVoucherRS.getStatement().close();
            totalVoucherRS.close();
            redeemVoucherRS.getStatement().close();
            redeemVoucherRS.close();
            expiredVoucherRS.getStatement().close();
            expiredVoucherRS.close();
          }
        catch (SQLException e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

      }
    DBConnection.closeConnections ( conn );

    //System.out.println("campaign id is " + campaignId);


  }


  public void customer_Report()
  {
  }

  public String exportToCSVVoucher()
  {
    List<List<String>> csvList = new ArrayList<List<String>>();
    String reportName = "";

    if (path.contains("POSMoneyReport"))
    {
       reportName = "POSMoney-report";
       for (int counter = 0; counter < posMoneyArray.size(); counter++)
         {
           if (counter == 0)
             {
               csvList.add(Arrays.asList(new String[]
                     { "Channel", "Sub Channel","Gift Name","Redeem Count","Total Cost" }));
             }


           csvList.add(Arrays.asList(new String[]
                 { 
                 posMoneyArray.get ( counter).getChannel ( )==null ? "": posMoneyArray.get ( counter).getChannel ( ), 
                 posMoneyArray.get ( counter).getSubChannel ( )==null ? "": posMoneyArray.get ( counter).getSubChannel ( ), 
                 posMoneyArray.get ( counter).getGiftName ( ) ==null ? "": posMoneyArray.get ( counter).getGiftName ( ), 
                 posMoneyArray.get ( counter).getRedeemcount ( )+"" ==null ? "": posMoneyArray.get ( counter).getRedeemcount ( )+"",                    
                 posMoneyArray.get ( counter).getGiftValue ( )==null ? "": posMoneyArray.get ( counter).getGiftValue ( )+""
                    }));
         }

    }
    if (path.contains("VoucherReport")&& !path.contains("DeletedVoucherReport"))
      {
        reportName = "voucher-report";
        for (int counter = 0; counter < voucherResult.size(); counter++)
          {
            if (counter == 0)
              {
                csvList.add(Arrays.asList(new String[]
                      { "Dial number", "Campaign name", "Gift Name", "Gift Value", "Creation Date","Redemption Date","Expire Date", 
                      "Redemption count"," Account No","Channel","Sub Channel",
                        "Voucher Status" }));
              }


            csvList.add(Arrays.asList(new String[]
                  { voucherResult.get(counter).getDialNumber(), 
                    voucherResult.get(counter).getCampaignName(), 
                    voucherResult.get(counter).getGiftName(), 
                    voucherResult.get(counter).getGiftValue() + "",                    
                    voucherResult.get(counter).getCreationDate ( )==null ? "": voucherResult.get(counter).getCreationDate ( ).toString ( ),
                    voucherResult.get(counter).getRedemptionDate ( )==null ? "": voucherResult.get(counter).getRedemptionDate ( ).toString ( ),
                    voucherResult.get(counter).getExpiryDate ( )==null ? "": voucherResult.get(counter).getExpiryDate ( ).toString ( ),                    
                    voucherResult.get(counter).getRedeemcount ( ) + "",
                    voucherResult.get(counter).getAccountNumber ( ) ,
                    voucherResult.get(counter).getChannel ( ) ,
                    voucherResult.get(counter).getSubChannel ( ),                    
                    voucherResult.get(counter).getStatus() + "" }));
          }
      }
    if (path.contains("DeletedVoucherReport"))
      {
        reportName = "deleted-voucher-report";
        for (int counter = 0; counter < voucherResult.size(); counter++)
          {
            if (counter == 0)
              {
                csvList.add(Arrays.asList(new String[]
                      { "Dial number", "Campaign name", "Gift Name", "Gift Value", "Creation Date","Redemption Date", 
                      "Redemption count","Deleted By","Deleted Date ","Channel","Sub Channel",
                        "Voucher Status" }));
              }


            csvList.add(Arrays.asList(new String[]
                  { voucherResult.get(counter).getDialNumber(), 
                    voucherResult.get(counter).getCampaignName(), 
                    voucherResult.get(counter).getGiftName(), 
                    voucherResult.get(counter).getGiftValue() + "",                    
                    voucherResult.get(counter).getCreationDate ( )==null ? "": voucherResult.get(counter).getCreationDate ( ).toString ( ),
                    voucherResult.get(counter).getRedemptionDate ( )==null ? "": voucherResult.get(counter).getRedemptionDate ( ).toString ( ),
                    voucherResult.get(counter).getRedeemcount()+"",                    
                    voucherResult.get(counter).getPersonName(),
                    voucherResult.get(counter).getDeletedDate()==null ? "": voucherResult.get(counter).getDeletedDate().toString ( ),                    
                    voucherResult.get(counter).getChannel ( ) ,
                    voucherResult.get(counter).getSubChannel ( ),                    
                    voucherResult.get(counter).getStatus() + "" }));
          }
      }
    if (path.contains("TimeChart"))
      {
        reportName = "time-chart";
        for (int counter = 0; counter < timeResult.size(); counter++)
          {


//            if (selectedItemGiftOption.compareTo("count") == 0)
//              {
//                if (counter == 0)
//                  {
//                    csvList.add(Arrays.asList(new String[]
//                          { "Time", "Count" }));
//                  }
//
//                csvList.add(Arrays.asList(new String[]
//                      { timeResult.get(counter).getDayOfWeekName() + "", 
//                        timeResult.get(counter).getGiftCount() + "" }));
//              }
//            else
//              {
                if (counter == 0)
                  {
                    csvList.add(Arrays.asList(new String[]
                          { "Time", "Value" }));
                  }

                csvList.add(Arrays.asList(new String[]
                      { timeResult.get(counter).getDayOfWeekName() + "", 
                        timeResult.get(counter).getGiftValue() + "" }));
//              }


          }
      }
    if (path.contains("userSummryReport"))
      {
        reportName = "user-summary-report";

        for (int counter = 0; counter < filterResult.size(); counter++)
          {
            if (counter == 0)
              {
                csvList.add(Arrays.asList(new String[]
                      { "Redeemed Count", "Total Cost", "User Name", "Pos Code" }));
              }
            csvList.add(Arrays.asList(new String[]
                  { filterResult.get(counter).getRedeemedCount() + "", 
                    filterResult.get(counter).getTotalCost() + "", 
                    filterResult.get(counter).getUserName() + "", 
                    filterResult.get(counter).getPosCode() + "", }));
          }
      }
    if (path.contains("WSReport"))
      {
        reportName = "web-service-report";

        for (int counter = 0; counter < filterResult.size(); counter++)
          {
            if (counter == 0)
              {

                csvList.add(Arrays.asList(new String[]
                      { "Campaign Name","Dial Number","Status","Type","Gift Name","Action Date"  }));
              }
            csvList.add(Arrays.asList(new String[]
                  { filterResult.get(counter).getCampaignName() + "",
                    filterResult.get(counter).getDialNumber() + "",
                    filterResult.get(counter).getStatusName() + "",
                    filterResult.get(counter).getTypeName() + "",
                    filterResult.get(counter).getGiftName() + "",
                    filterResult.get(counter).getActionDate() + "",

                     }));
          }
      }

    if (path.contains("posReport"))
      {
        reportName = "pos-report";
        for (int counter = 0; counter < filterResult.size(); counter++)
          {
            if (counter == 0)
              {
                csvList.add(Arrays.asList(new String[]
                      { "Campaign Name", "Gift Type", "Gift Name", "Date", "Value", "User type", 
                        "POS Code" }));
              }


            csvList.add(Arrays.asList(new String[]
                  { filterResult.get(counter).getCampaignName() + "", 
                    filterResult.get(counter).getGiftType() + "", 
                    filterResult.get(counter).getGiftName() + "", 
                    filterResult.get(counter).getDate() + "", 
                    filterResult.get(counter).getTotalCost() + "", 
                    filterResult.get(counter).getUserTypeName() + "", 
                    filterResult.get(counter).getPosCode() + "" }));

          }
      }

    if (path.contains("userReport"))
      {
        reportName = "user-report";
        for (int counter = 0; counter < filterResult.size(); counter++)
          {
            if (counter == 0)
              {
                csvList.add(Arrays.asList(new String[]
                      { "Campaign Name", "Gift Type", "Gift Name", "Date", "Value", "User Type", 
                        "User Name", "POS Code" }));
              }


            csvList.add(Arrays.asList(new String[]
                  { filterResult.get(counter).getCampaignName() + "", 
                    filterResult.get(counter).getGiftType() + "", 
                    filterResult.get(counter).getGiftName() + "", 
                    filterResult.get(counter).getDate() + "", 
                    filterResult.get(counter).getTotalCost() + "", 
                    filterResult.get(counter).getUserTypeName() + "", 
                    filterResult.get(counter).getUserName() + "", 
                    filterResult.get(counter).getPosCode() + "" }));

          }
      }

    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);


    
    FacesContext fc = FacesContext.getCurrentInstance();



    String templatePath = jsfUtils.getParamterFromIni("defualtPath");   


    //System.out.println("tempPath " + templatePath);
    //System.out.println("File Name Is =" + filename);
    filename = reportName + "-" + filename; // report name

    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".csv";


    InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
    try
      {
        FileUtil.write(new File(templatePath + "ReportsCSV/" + filename), csvInput);
        File csvFile = new File(templatePath + "ReportsCSV/", filename);
        //System.out.println("File report is " + csvFile.getAbsolutePath());
        HttpServletUtil.downloadFile(response, csvFile, false);
      }
    catch (IOException e)
      {
        //System.out.println("Download file failed: " + e.getMessage());
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    fc.responseComplete();
    return null;
  }

  

  public void campaign_Report() throws SQLException
  {


    renderDataTable = true;
    renderExportToCSV = true;


    filterResult = new ArrayList<CampaignReportDTO>();
    
    String SQL = 
      "Select sum(t3.GIFT_VALUE) SUMMRY_REPORT_TOTAL_COST,count(t1.CAMPAIGN_ID) SUMMRY_REPORT_REDEEMED_COUNT,t1.CAMPAIGN_ID,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY') SUMMRY_REPORT_DATE,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD') SUMMRY_REPORT_DAY_OF_MONTH, to_char(t1.CURRENT_VOUCHER_END_DATE,'D') SUMMRY_REPORT_DAY_OF_WEEK,to_char(t1.CURRENT_VOUCHER_END_DATE,'hh') SUMMRY_REPORT_HOUR,t2.Campaign_Name,t3.Gift_Name,t4.USER_TYPE_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME,t7.GIFT_TYPE_NAME" +
      " from GVMS_CURRENT_VOUCHER t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID = t2.CAMPAIGN_ID " + 
      "left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID " + 
      "left join GVMS_USER_TYPE t4 on t1.USER_TYPE_ID = t4.USER_TYPE_ID " + 
      "left join GVMS_USER t5 on t1.USER_ID = t5.SYSTEM_USER_ID " + 
      "left join GVMS_SYSTEM_USER t6 on t5.SYSTEM_USER_ID = t6.SYSTEM_USER_ID " + 
      "left join GVMS_GIFT_TYPE t7 on t3.GIFT_TYPE_ID = t7.GIFT_TYPE_ID where 1=1 and t1.VOUCHER_STATUS_ID = 1 AND t2.CAMPAIGN_STATUS_ID !=4 ";

    //System.out.println("SQL conditions " + SQL);


    if (selectedItemCampaignName.compareTo("*") != 0)
      {
        String campaignId = "";
        for (int count = 0; count < getIdArray.size(); count++)
          {
            if (getIdArray.get(count).contains((CharSequence) selectedItemCampaignName))
              {
                campaignId = getIdArray.get(count).replaceFirst(selectedItemCampaignName, "");
                this.campaignId = campaignId;

              }
          }
        SQL += " AND t1.CAMPAIGN_ID=" + campaignId;
      }

    if (selectedItemGiftName.compareTo("*") != 0)
      {
        String giftId = "";
        for (int count = 0; count < getGiftIdArray.size(); count++)
          {
            if (getGiftIdArray.get(count).contains((CharSequence) selectedItemGiftName))
              {
                giftId = getGiftIdArray.get(count).replaceFirst(selectedItemGiftName, "");

              }
          }
        SQL += " AND t1.GIFT_ID=" + giftId;
      }

    if (selectedItemGiftTypeName.compareTo("*") != 0)
      {
        String giftTypeId = "";
        for (int count = 0; count < getGiftTypeIdArray.size(); count++)
          {
            //System.out.println("size of getGiftIdArray" + getGiftIdArray.size());
            //System.out.println("value of getGiftIdArray in cell " + count + " " + 
//                               getGiftTypeIdArray.get(count));
            //System.out.println("combo value of selected user for gift Type  " + count + " " + 
//                               selectedItemGiftTypeName);
            if (getGiftTypeIdArray.get(count).contains((CharSequence) selectedItemGiftTypeName))
              {
                giftTypeId = 
                    getGiftTypeIdArray.get(count).replaceFirst(selectedItemGiftTypeName, "");

              }
          }
        SQL += " AND t1.GIFT_TYPE_ID=" + giftTypeId;
      }


    if (selectedItemUserTypeName.compareTo("*") != 0)
      {
        String userTypeId = "";
        for (int count = 0; count < getUserTypeIdArray.size(); count++)
          {
            if (getUserTypeIdArray.get(count).contains((CharSequence) selectedItemUserTypeName))
              {
                userTypeId = 
                    getUserTypeIdArray.get(count).replaceFirst(selectedItemUserTypeName, "");

              }
          }
        SQL += " AND t1.USER_TYPE_ID=" + userTypeId;
      }
    posValue = posValue.trim();
    //System.out.println("pos value control is " + posValue);
    
    if (posValue.compareTo("") != 0)
      {
        //System.out.println("in pos Value");
        if (posValue.contains((CharSequence) ","))

          {
            //System.out.println("in pos Value condition replace");
            posValue.replace((CharSequence) ",", (CharSequence) "");
          }
        //System.out.println("in pos Value b4 and");
        SQL += " AND t5.USER_POS_CODE='" + posValue + "'";
      }
    else
      {
        posValue = "";
      }
    userValue = userValue.trim();
    //System.out.println("user value control is " + userValue);
    if (userValue.compareTo("") != 0)
      {

        SQL += " AND t6.SYSTEM_USER_NAME='" + userValue + "'";
      }
    else
      {
        userValue = "";
      }


    if (filterFromDate != null && filterToDate != null)
      {

        SQL += 
            " AND t1.CURRENT_VOUCHER_END_DATE >= " + convertDate(filterFromDate,"from") + " and t1.CURRENT_VOUCHER_END_DATE <= " + 
            convertDate(filterToDate,"to");
      }
    //System.out.println("SQL b4 group by" + SQL);
    SQL += 
        " group by t1.CAMPAIGN_ID,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY'),to_char(t1.CURRENT_VOUCHER_END_DATE,'DD'), to_char(t1.CURRENT_VOUCHER_END_DATE,'D'),to_char(t1.CURRENT_VOUCHER_END_DATE,'hh'),t2.Campaign_Name,t3.Gift_Name,t4.USER_TYPE_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME,t7.GIFT_TYPE_NAME";

    Connection conn = DBConnection.getConnection();
    if (getCampaignStatus(conn,campaignId,"Campaign_status_id")==3){
        SQL = SQL.replace("GVMS_CURRENT_VOUCHER","GVMS_TEMP_CURRENT");
        	}
    
    ResultSet resultFilter = ExecuteQueries.executeQuery(conn,SQL);

    try
      {
      
      //System.out.println("selectedItemCampaignName is "+selectedItemCampaignName);
      //System.out.println("campaignId is "+campaignId);
        if (selectedItemCampaignName.compareTo("*") != 0&&campaignId.compareTo("")!=0)
        {
        	
        
        while (resultFilter.next())
          {
          
            //
            CampaignReportDTO crc = new CampaignReportDTO();
            crc.setCampaignName(resultFilter.getString("CAMPAIGN_NAME"));
            crc.setDate(resultFilter.getString("SUMMRY_REPORT_DATE"));
            crc.setGiftName(resultFilter.getString("GIFT_NAME"));
            crc.setRedeemedCount(resultFilter.getInt("SUMMRY_REPORT_REDEEMED_COUNT"));
            crc.setTotalCost(resultFilter.getInt("SUMMRY_REPORT_TOTAL_COST"));
            crc.setUserTypeName(resultFilter.getString("USER_TYPE_NAME"));
            crc.setPosCode(resultFilter.getString("USER_POS_CODE"));
            crc.setGiftType(resultFilter.getString("GIFT_TYPE_NAME"));
            crc.setUserName(resultFilter.getString("SYSTEM_USER_NAME"));


            filterResult.add(crc);
          }
        
        }
        else
        {
        
        while (resultFilter.next())
          {
          
            //
            CampaignReportDTO crc = new CampaignReportDTO();
            crc.setCampaignName(resultFilter.getString("CAMPAIGN_NAME"));
            crc.setDate(resultFilter.getString("SUMMRY_REPORT_DATE"));
            crc.setGiftName(resultFilter.getString("GIFT_NAME"));
            crc.setRedeemedCount(resultFilter.getInt("SUMMRY_REPORT_REDEEMED_COUNT"));
            crc.setTotalCost(resultFilter.getInt("SUMMRY_REPORT_TOTAL_COST"));
            crc.setUserTypeName(resultFilter.getString("USER_TYPE_NAME"));
            crc.setPosCode(resultFilter.getString("USER_POS_CODE"));
            crc.setGiftType(resultFilter.getString("GIFT_TYPE_NAME"));
            crc.setUserName(resultFilter.getString("SYSTEM_USER_NAME"));


            filterResult.add(crc);
          }
      }
   
        resultFilter.getStatement().close();
        resultFilter.close();
      }
    catch (SQLException e)
      {
       new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );
    //System.out.println("filterResult size is "+filterResult.size());
  }
  public String goFilter_posMoney() throws SQLException
  {

    renderDataTable = true;
    renderExportToCSV = true;


    posMoneyArray = new ArrayList<VoucherReportDTO>();

StringBuffer condition = new StringBuffer("");

    if (selectedCampaign!=null && selectedCampaign.compareTo("-1")!=0&&selectedCampaign.compareTo("")!=0)
      {
       condition.append (  " AND t1.CAMPAIGN_ID=");
       condition.append (  selectedCampaign );
      }
    if (selectedGift!=null && selectedGift.compareTo("-1")!=0&&selectedGift.compareTo("")!=0)
      { 
        condition.append (  " AND t1.GIFT_ID=");
        condition.append (  selectedGift );
      }
    
    
    if (selectedChannel!=null && selectedChannel.compareTo("-1")!=0 && selectedChannel.compareTo("")!=0)
    {
     condition.append (  " AND t1.USER_TYPE_ID=");
     condition.append (  selectedChannel );
    }
    
    if (selectedSubChannel!=null && selectedSubChannel.compareTo("-1")!=0 && selectedSubChannel.compareTo("")!=0)
    {
     condition.append (  " AND t1.USER_ID=");
     condition.append (  selectedSubChannel );
    }
    
        
    Connection conn = DBConnection.getConnection();
    
    StringBuffer searchVoucherSQL =new StringBuffer("");
    searchVoucherSQL.append ( " SELECT distinct (t1.USER_TYPE_ID),sum(T4.GIFT_VALUE)  COST, T2.VOUCHER_STATUS_ID,Max(rownum) REDEEMCOUNT,");
    searchVoucherSQL.append ( " T2.VOUCHER_STATUS, T3.CAMPAIGN_NAME,");
    searchVoucherSQL.append ( " T4.GIFT_NAME,T5.USER_TYPE_NAME,T6.USER_POS_CODE,t1.USER_ID,t1.CAMPAIGN_ID");
    
    if (getCampaignStatus(conn,selectedCampaign,"Campaign_status_id")==3) searchVoucherSQL.append ( " FROM GVMS_TEMP_CURRENT T1 ");
    else searchVoucherSQL.append ( " FROM GVMS_CURRENT_VOUCHER T1 ");
    
    searchVoucherSQL.append ( " LEFT JOIN GVMS_VOUCHER_STATUS T2 ON T1.VOUCHER_STATUS_ID = T2.VOUCHER_STATUS_ID");
    searchVoucherSQL.append ( " LEFT JOIN GVMS_CAMPAIGN T3 ON T1.CAMPAIGN_ID = T3.CAMPAIGN_ID");
    searchVoucherSQL.append ( " LEFT JOIN GVMS_GIFT T4 ON T1.GIFT_ID = T4.GIFT_ID");
    searchVoucherSQL.append ( " LEFT JOIN GVMS_USER_TYPE T5 ON T1.USER_TYPE_ID = T5.USER_TYPE_ID");
    searchVoucherSQL.append ( " LEFT JOIN GVMS_USER T6 ON T1.USER_ID = T6.USER_ID");
    searchVoucherSQL.append ( " LEFT JOIN GVMS_CS_LOGS T7 ON T1.CURRENT_VOUCHER_NUMBER = T7.VOUCHER_NUMBER");
    searchVoucherSQL.append ( " WHERE 1 = 1 AND T1.VOUCHER_STATUS_ID=1");
    searchVoucherSQL.append ( condition );
    searchVoucherSQL.append ( " GROUP BY T2.VOUCHER_STATUS_ID,T3.CAMPAIGN_STATUS_ID,");
    searchVoucherSQL.append ( " T2.VOUCHER_STATUS, T3.CAMPAIGN_NAME,");    
    searchVoucherSQL.append ( " T4.GIFT_NAME,T5.USER_TYPE_NAME,T6.USER_POS_CODE");
    searchVoucherSQL.append ( " ,t1.USER_ID,t1.CAMPAIGN_ID,t1.USER_TYPE_ID");
    
    
    
    //System.out.println(searchVoucherSQL);
    
    try
    {
     ResultSet findDialRS = ExecuteQueries.executeQuery(conn,searchVoucherSQL.toString ( ));   
      while (findDialRS.next())
        {
          VoucherReportDTO vrc = new VoucherReportDTO();
          vrc.setStatus(findDialRS.getString("VOUCHER_STATUS"));
          vrc.setGiftValue (findDialRS.getInt("cost"));
          vrc.setGiftName(findDialRS.getString("GIFT_NAME"));
          vrc.setCampaignName(findDialRS.getString("CAMPAIGN_NAME"));          
          vrc.setChannel  ( findDialRS.getString("USER_TYPE_NAME") );
          vrc.setSubChannel ( findDialRS.getString("USER_POS_CODE") );
          vrc.setRedeemcount ( findDialRS.getInt("REDEEMCOUNT") );
          posMoneyArray.add(vrc);
        
        }
      
         
      findDialRS.getStatement().close();
      findDialRS.close();
    }
  catch (SQLException e)
    {
       new com.mobinil.gvms.system.utility.PrintException().printException(e);
    }
    
    

    

    
    
    
    DBConnection.closeConnections ( conn );
    return null;
  }

  public void goFilter_WebService() {
      renderDataTable = true;
    
        try {
            Connection conn = DBConnection.getConnection();
            CampaignReportDTO filter = new CampaignReportDTO();
            filter.setWsFromDate(filterFromDate);
            filter.setWsToDate(filterToDate);
            filter.setWsCampainId(selectedItemCampaignName);
            filter.setWsGiftId(selectedItemGiftName);
            filter.setStatusId(selectedStatusName);
            filter.setDialNumber(dialValue);
            filter.setTypeId(selectedTypeName);
            filterResult = ReportDAO.filterWebServiceLogs(conn, filter);
            totalGiftValues = ReportDAO.calcGiftValues(filterResult)+"";
            if (filterResult.size()>0){renderExportToCSV = true;}
            DBConnection.closeConnections(conn);
        } catch (SQLException ex) {
            new com.mobinil.gvms.system.utility.PrintException().printException(ex);
        }
  }
  public String goFilter_userSummary() throws SQLException
  {

    renderDataTable = true;
    renderExportToCSV = true;


    filterResult = new ArrayList<CampaignReportDTO>();

    String condition = 
      " group by t1.GIFT_TYPE_ID,t1.USER_ID,t1.User_type_ID,t1.GIFT_ID,t1.CAMPAIGN_ID,to_date(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY'),to_char(t1.CURRENT_VOUCHER_END_DATE,'DD'), to_char(t1.CURRENT_VOUCHER_END_DATE,'D'),to_char(t1.CURRENT_VOUCHER_END_DATE,'hh'),t2.Campaign_Name,t3.Gift_Name,t4.USER_TYPE_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME,t7.GIFT_TYPE_NAME) t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID = t2.CAMPAIGN_ID left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_USER_TYPE t4 on t1.USER_TYPE_ID = t4.USER_TYPE_ID left join GVMS_USER t5 on t1.USER_ID = t5.SYSTEM_USER_ID left join GVMS_SYSTEM_USER t6 on t5.SYSTEM_USER_ID = t6.SYSTEM_USER_ID left join GVMS_GIFT_TYPE t7 on t3.GIFT_TYPE_ID = t7.GIFT_TYPE_ID group by t1.GIFT_TYPE_NAME,t1.GIFT_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME";

    String SQL = 
      "Select t1.GIFT_TYPE_NAME,t1.GIFT_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME,sum(t1.SUMMRY_REPORT_REDEEMED_COUNT) redeemed_COunt,sum(t1.SUMMRY_REPORT_TOTAL_COST)totalCost  from (Select sum(t3.GIFT_VALUE) SUMMRY_REPORT_TOTAL_COST,count(t1.CAMPAIGN_ID) SUMMRY_REPORT_REDEEMED_COUNT,t1.GIFT_TYPE_ID,t1.USER_ID,t1.User_type_ID,t1.GIFT_ID,t1.CAMPAIGN_ID,to_date(t1.CURRENT_VOUCHER_END_DATE,'DD/MM/YYYY') SUMMRY_REPORT_DATE,to_char(t1.CURRENT_VOUCHER_END_DATE,'DD') SUMMRY_REPORT_DAY_OF_MONTH, to_char(t1.CURRENT_VOUCHER_END_DATE,'D') SUMMRY_REPORT_DAY_OF_WEEK,to_char(t1.CURRENT_VOUCHER_END_DATE,'hh') SUMMRY_REPORT_HOUR,t2.Campaign_Name,t3.Gift_Name,t4.USER_TYPE_NAME,t5.USER_POS_CODE,t6.SYSTEM_USER_NAME,t7.GIFT_TYPE_NAME from GVMS_CURRENT_VOUCHER t1 left join GVMS_CAMPAIGN t2 on t1.CAMPAIGN_ID = t2.CAMPAIGN_ID left join GVMS_GIFT t3 on t1.GIFT_ID=t3.GIFT_ID left join GVMS_USER_TYPE t4 on t1.USER_TYPE_ID = t4.USER_TYPE_ID left join GVMS_USER t5 on t1.USER_ID = t5.USER_ID left join GVMS_SYSTEM_USER t6 on t5.SYSTEM_USER_ID = t6.SYSTEM_USER_ID left join GVMS_GIFT_TYPE t7 on t3.GIFT_TYPE_ID = t7.GIFT_TYPE_ID where 1=1 and t1.VOUCHER_STATUS_ID = 1 AND t2.CAMPAIGN_STATUS_ID !=4 ";
   


    if (selectedItemCampaignName.compareTo("*") != 0)
      {
        String campaignId = "";
        for (int count = 0; count < getIdArray.size(); count++)
          {
            if (getIdArray.get(count).contains((CharSequence) selectedItemCampaignName))
              {
                campaignId = getIdArray.get(count).replaceFirst(selectedItemCampaignName, "");
                this.campaignId = campaignId;
              }
          }
        SQL += " AND t2.CAMPAIGN_ID=" + campaignId;
      }

    if (selectedItemGiftName.compareTo("*") != 0)
      {
        String giftId = "";
        for (int count = 0; count < getGiftIdArray.size(); count++)
          {
            if (getGiftIdArray.get(count).contains((CharSequence) selectedItemGiftName))
              {
                giftId = getGiftIdArray.get(count).replaceFirst(selectedItemGiftName, "");

              }
          }
        SQL += " AND t1.GIFT_ID=" + giftId;
      }

    if (selectedItemGiftTypeName.compareTo("*") != 0)
      {
        String giftTypeId = "";
        for (int count = 0; count < getGiftTypeIdArray.size(); count++)
          {
            if (getGiftTypeIdArray.get(count).contains((CharSequence) selectedItemGiftTypeName))
              {
                giftTypeId = 
                    getGiftTypeIdArray.get(count).replaceFirst(selectedItemGiftTypeName, "");

              }
          }
        SQL += " AND t1.GIFT_TYPE_ID=" + giftTypeId;
      }

    if (selectedItemUserTypeName.compareTo("*") != 0)
      {
        String userTypeId = "";
        for (int count = 0; count < getUserTypeIdArray.size(); count++)
          {
            if (getUserTypeIdArray.get(count).contains((CharSequence) selectedItemUserTypeName))
              {
                userTypeId = 
                    getUserTypeIdArray.get(count).replaceFirst(selectedItemUserTypeName, "");

              }
          }
        SQL += " AND t1.USER_TYPE_ID=" + userTypeId;
      }

    posValue = posValue.trim();
    if (posValue.compareTo("") != 0)
      {
        //System.out.println("in pos Value");
        if (posValue.contains((CharSequence) ","))

          {
            //System.out.println("in pos Value condition replace");
            posValue.replace((CharSequence) ",", (CharSequence) "");
          }
        //System.out.println("in pos Value b4 and");
        SQL += " AND t5.USER_POS_CODE='" + posValue + "'";
      }
    userValue = userValue.trim();
    if (userValue.compareTo("") != 0)
      {

        SQL += " AND t6.SYSTEM_USER_NAME='" + userValue + "'";
      }


    if (filterFromDate != null && filterToDate != null)
      {

        SQL += 
            " AND t1.CURRENT_VOUCHER_END_DATE >= " + convertDate(filterFromDate,"from") + " and t1.CURRENT_VOUCHER_END_DATE <= " + 
            convertDate(filterToDate,"to");
      }

    
    SQL += condition;

    Connection conn = DBConnection.getConnection();
    ResultSet resultFilterRS = ExecuteQueries.executeQuery(conn,SQL);

    try
      {
      
    
         if (selectedItemCampaignName.compareTo("*") != 0&&campaignId.compareTo("")!=0)
         {
        	 if (getCampaignStatus(conn,campaignId,"Campaign_status_id")==3){
         SQL = SQL.replace("GVMS_CURRENT_VOUCHER","GVMS_TEMP_CURRENT");
        	 }
         resultFilterRS = ExecuteQueries.executeQuery(conn,SQL);
         while (resultFilterRS.next())
           {
           
             CampaignReportDTO crc = new CampaignReportDTO();
             crc.setRedeemedCount(resultFilterRS.getInt("REDEEMED_COUNT"));
             crc.setTotalCost(resultFilterRS.getInt("TOTALCOST"));
             crc.setPosCode(resultFilterRS.getString("USER_POS_CODE"));
             crc.setUserName(resultFilterRS.getString("SYSTEM_USER_NAME"));
             crc.setGiftTypeName(resultFilterRS.getString("GIFT_TYPE_NAME"));
             crc.setGiftName(resultFilterRS.getString("GIFT_NAME"));

             filterResult.add(crc);
           }
         }
         else{
        while (resultFilterRS.next())
          {
          
            CampaignReportDTO crc = new CampaignReportDTO();
            crc.setRedeemedCount(resultFilterRS.getInt("REDEEMED_COUNT"));
            crc.setTotalCost(resultFilterRS.getInt("TOTALCOST"));
            crc.setPosCode(resultFilterRS.getString("USER_POS_CODE"));
            crc.setUserName(resultFilterRS.getString("SYSTEM_USER_NAME"));
            crc.setGiftTypeName(resultFilterRS.getString("GIFT_TYPE_NAME"));
            crc.setGiftName(resultFilterRS.getString("GIFT_NAME"));

            filterResult.add(crc);
          }}
            
        resultFilterRS.getStatement().close();
        resultFilterRS.close();
      }
    catch (SQLException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        //System.out.println(e);
        // 
      }
    DBConnection.closeConnections ( conn );
    return null;
  }


  public void exportHtmlTableAsExcel()
    throws IOException
  {

    //System.out.println("in  as Excel ");


    //Set the filename
    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);


    //Setup the output
    String contentType = "application/vnd.ms-excel";
    FacesContext fc = FacesContext.getCurrentInstance();

    HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();

    String templatePath = jsfUtils.getParamterFromIni("defualtPath");  
    templatePath += "CampaignReportExcel/";

    //System.out.println("tempPath " + templatePath);
    //System.out.println("File Name Is =" + filename);

    if (selectedItemCampaignName.compareTo("*") == 0)
      {
        filename = "AllCampaigns" + "-" + filename;
      }
    else
      {
        filename = selectedItemCampaignName + "-" + filename;
      }


    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".xls";

    boolean result = exportTemplate(templatePath + "templat.xls", templatePath + filename);

    File excelFile = new File(templatePath, filename);

    try
      {


        HttpServletUtil.downloadFile(response, excelFile, false);
      }
    catch (IOException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        // Do your error handling thing.
        //System.out.println("Download file failed: " + e.getMessage());
         
        return;
      }
    //System.out.println("file creation is " + result);
    //      fc.getExternalContext().redirect("/GVMS/"+ filename);


    fc.responseComplete();
  }

  public boolean exportTemplate(String template, String fileName)
  {
    try
      {
        //System.out.println("template =" + template);
        FileInputStream tempIn = new FileInputStream(template);


        HSSFWorkbook wb = new HSSFWorkbook(tempIn);

        HSSFSheet campaignNameSheet = wb.getSheet("CampaignReport");

        fillColumn("Total Vouchers", campaignNameSheet, 0, 0);
        fillColumn("Redeemed Voucher", campaignNameSheet, 1, 0);
        fillColumn(statusValue, campaignNameSheet, 2, 0);


        if (campaignStatus == 3 || campaignStatus == 4)
          {
            fillColumn(totalVoucher, redeemVoucher, expiredVoucher, 
                       "CAMPAIGN_REPORT_TOTAL_VOUCHER", "CAMPAIGN_REPORT_REDEEM_VOUCHER", 
                       "CAMPAIGN_REPORT_EXPIR_VOUCHER", campaignNameSheet, 0, 1);

          }
        else
          {

            fillColumn(totalVoucher, redeemVoucher, expiredVoucher, 
                       "CAMPAIGN_REPORT_TOTAL_VOUCHER", "CAMPAIGN_REPORT_REDEEM_VOUCHER", 
                       "CAMPAIGN_REPORT_TOTAL_VOUCHER,CAMPAIGN_REPORT_REDEEM_VOUCHER", 
                       campaignNameSheet, 0, 1);

          }


        campaignNameSheet.setSelected(true);

        FileOutputStream fileOut = new FileOutputStream(fileName);
        //System.out.println("File out= " + fileName);
        //System.out.println("file path= " + (new File(fileName)).getAbsolutePath());

        wb.write(fileOut);
        fileOut.close();

        return true;
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
        return false;
      }
  }

  public boolean fillColumn(Integer totalVoucher, Integer redeemedVoucher, Integer ExpiredVoucher, 
                             String total, String redeemed, String status, HSSFSheet sheet, 
                             int colNumber, int startRow)
  {

    //        ResultSet resultSet = ExecuteQueries.executeQuery(sql);
    int index = startRow;
    int counter = 0;

    String[] fieldArray = status.split(",");

    //System.out.println("field 0 of array is " + fieldArray[0]);


    HSSFRow row = sheet.createRow((short) index);
    HSSFCell cell = null;

    cell = row.createCell((short) colNumber);
    cell.setCellValue(totalVoucher);
    cell = row.createCell((short) (colNumber + 1));
    cell.setCellValue(redeemedVoucher);
    if (campaignStatus == 3 || campaignStatus == 4)
      {

        cell = row.createCell((short) (colNumber + 2));
        cell.setCellValue(ExpiredVoucher);
      }
    else
      {


        cell = row.createCell((short) (colNumber + 2));
        cell.setCellValue(ExpiredVoucher);
      }

    index++;
    counter++;


    return true;


  }

  public boolean fillColumn(String culName, HSSFSheet sheet, int colNumber, int startRow)
  {


    int index = startRow;
    int counter = 0;


    HSSFRow row = sheet.createRow((short) index);
    HSSFCell cell = null;

    cell = row.createCell((short) colNumber);
    cell.setCellValue(culName);


    index++;
    counter++;


    return true;


  }

  public String convertDate(Date undefineDate,String type)
  {
    String values = "";
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[1] + "-" + toDate[0] + "-" + toDate[2] + "', 'dd-mm-yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;
        if (type.compareTo("from")==0)
        values = "to_date('" + day + "/" + month + "/" + year + " 00:00:00', 'dd/mm/yyyy HH24:MI:SS')";
        if (type.compareTo("to")==0)
            values = "to_date('" + day + "/" + month + "/" + year + " 23:59:59', 'dd/mm/yyyy HH24:MI:SS')";
        // values = "to_date('" + day + "-" + month + "-" + year + "', 'dd-mm-yyyy')";

        return values;
      }
  }


  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(filterResult, new DTOComparator(sortField, sortAscending));
      }


  }

  public void sortDataTable3(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(timeResult, new DTOComparator(sortField, sortAscending));
      }


  }

  public void sortDataTable2(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(voucherResult, new DTOComparator(sortField, sortAscending));
      }


  }


  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public void pageFirst()
  {

    if (path.contains("userReport")|| path.contains("WSReport"))
      {
        dataTable1.setFirst(0);
      }
    if (path.contains("posReport"))
      {
        dataTable3.setFirst(0);
      }
    if (path.contains("VoucherReport"))
      {
        dataTable2.setFirst(0);
      }

    pageIndex = 0;
  }

  public void pagePrevious()
  {
    if (path.contains("userReport")|| path.contains("WSReport"))
      {
        dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
      }
    if (path.contains("posReport"))
      {
        dataTable3.setFirst(dataTable3.getFirst() - dataTable3.getRows());
      }
    if (path.contains("VoucherReport"))
      {
        dataTable2.setFirst(dataTable2.getFirst() - dataTable2.getRows());
      }
    pageIndex--;
  }

  public void pageNext()
  {
    if (path.contains("userReport")|| path.contains("WSReport"))
      {

        dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());


      }
    if (path.contains("posReport"))
      {

        dataTable3.setFirst(dataTable3.getFirst() + dataTable3.getRows());


      }
    if (path.contains("VoucherReport"))
      {

        dataTable2.setFirst(dataTable2.getFirst() + dataTable2.getRows());


      }
    pageIndex++;

  }

  public void pageLast()
  {
    if (path.contains("userReport")|| path.contains("WSReport"))
      {
        pageIndex = dataTable1.getRowCount();
        int count = dataTable1.getRowCount();
        int rows = dataTable1.getRows();
        dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
      }
    if (path.contains("posReport"))
      {
        pageIndex = dataTable3.getRowCount();
        int count = dataTable3.getRowCount();
        int rows = dataTable3.getRows();
        dataTable3.setFirst(count - ((count % rows != 0)? count % rows: rows));
      }
    if (path.contains("VoucherReport"))
      {
        pageIndex = dataTable2.getRowCount();
        int count = dataTable2.getRowCount();
        int rows = dataTable2.getRows();
        dataTable2.setFirst(count - ((count % rows != 0)? count % rows: rows));
      }
  }


  public DefaultPieDataset getPieDataset()
  {
    DefaultPieDataset pieDataSet = new DefaultPieDataset();
    //System.out.println(totalVoucher);
    //System.out.println(redeemVoucher);
    //System.out.println(expiredVoucher);
    Float redeemPercent = 00F;
    Float expiredPercent = 00F;
    if (redeemVoucher == 0 || totalVoucher == 0)
      {
        redeemPercent = 00F;
      }
    else
      {
        redeemPercent = ((redeemVoucher.floatValue() * 100) / totalVoucher.floatValue());
      }


    if (expiredVoucher == 0 || totalVoucher == 0)
      {
        expiredPercent = 00F;
      }
    else
      {

        expiredPercent = ((expiredVoucher.floatValue() * 100) / totalVoucher.floatValue());
      }


    //    if (redeemPercent + expiredPercent != 100)
    //      {
    //        if (redeemPercent > expiredPercent)
    //          {
    //            redeemPercent += 1;
    //          }
    //        else
    //          {
    //            expiredPercent += 1;
    //          }
    //
    //      }

    pieDataSet.setValue("Redeemed= " + Math.round(redeemPercent.doubleValue()) + "%", 
                        Math.round(redeemVoucher.doubleValue()));

    //System.out.println("Redeemed with round " + Math.round(redeemPercent.doubleValue()));
    //System.out.println("Redeemed with float " + redeemPercent.floatValue());

    if (campaignStatus == 3 || campaignStatus == 4)
      {
        statusValue = "Expired Vouchers";
        //System.out.println("Expired with round " + Math.round(expiredPercent.doubleValue()));
        //System.out.println("Expired without float " + expiredPercent.floatValue());
        pieDataSet.setValue("Expired= " + Math.round(expiredPercent.doubleValue()) + "%", 
                            Math.round(expiredVoucher.doubleValue()));
      }
    else
      {
        statusValue = "Active Vouchers";
        //System.out.println("Active with round " + Math.round(expiredPercent.doubleValue()));
        //System.out.println("Active without float " + expiredPercent.floatValue());
        pieDataSet.setValue("Active= " + Math.round(expiredPercent.doubleValue()) + "%", 
                            Math.round(expiredVoucher.doubleValue()));
      }
    //    pieDataSet.setValue("Active= " + (totalVoucher-(redeemVoucher+expiredVoucher)), ((totalVoucher-(redeemVoucher+expiredVoucher))%totalVoucher));


    return pieDataSet;
  }

  public DefaultCategoryDataset getCategoryDataset()
  {
    //    String category1 = "A";
    //    String series1 = "First";

    DefaultCategoryDataset categoryDataSet;
    categoryDataSet = new DefaultCategoryDataset();
    for (int counter = 0; counter < timeResult.size(); counter++)
      {
        //System.out.println("option is =" + selectedItemGiftOption);
//        if (selectedItemGiftOption.compareTo("count") == 0)
//          {
//            Integer tempCount = Integer.parseInt(timeResult.get(counter).getGiftCount());
//            //System.out.println("gift count is in " + counter + " is" + tempCount.intValue());
//
//            //System.out.println("day of week name  " + timeResult.get(counter).getDayOfWeekName());
//            categoryDataSet.addValue(tempCount, "", timeResult.get(counter).getDayOfWeekName());
//
//
//          }
//        else
//          {

            categoryDataSet.addValue(Integer.parseInt(timeResult.get(counter).getGiftValue()), "", 
                                     timeResult.get(counter).getDayOfWeekName());

            //System.out.println("gift count is in " + counter + " is" + 
//                               timeResult.get(counter).getGiftValue());
            //System.out.println("day of week name  " + timeResult.get(counter).getDayOfWeekName());
//          }

      }
   
    return categoryDataSet;

  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setCampaignItems(ArrayList<SelectItem> campaignItems)
  {
    this.campaignItems = campaignItems;
  }

  public ArrayList<SelectItem> getCampaignItems()
  {
    return campaignItems;
  }

  public void setRenderChart(Boolean renderChart)
  {
    this.renderChart = renderChart;
  }

  public Boolean getRenderChart()
  {
    return renderChart;
  }

  public void setTotalVoucher(Integer totalVoucher)
  {
    this.totalVoucher = totalVoucher;
  }

  public Integer getTotalVoucher()
  {
    return totalVoucher;
  }

  public void setRedeemVoucher(Integer redeemVoucher)
  {
    this.redeemVoucher = redeemVoucher;
  }

  public Integer getRedeemVoucher()
  {
    return redeemVoucher;
  }

  public void setExpiredVoucher(Integer expiredVoucher)
  {
    this.expiredVoucher = expiredVoucher;
  }

  public Integer getExpiredVoucher()
  {
    return expiredVoucher;
  }

  public void setStatusValue(String statusValue)
  {
    this.statusValue = statusValue;
  }

  public String getStatusValue()
  {
    return statusValue;
  }


  public void setRenderCampaignChart(Boolean renderCampaignChart)
  {
    this.renderCampaignChart = renderCampaignChart;
  }

  public Boolean getRenderCampaignChart()
  {
    return renderCampaignChart;
  }

  public void setGiftItems(ArrayList<SelectItem> giftItems)
  {
    this.giftItems = giftItems;
  }

  public ArrayList<SelectItem> getGiftItems()
  {
    return giftItems;
  }


  public void setUserTypeItems(ArrayList<SelectItem> userTypeItems)
  {
    this.userTypeItems = userTypeItems;
  }

  public ArrayList<SelectItem> getUserTypeItems()
  {
    return userTypeItems;
  }

  public void setGiftTypeItems(ArrayList<SelectItem> giftTypeItems)
  {
    this.giftTypeItems = giftTypeItems;
  }

  public ArrayList<SelectItem> getGiftTypeItems()
  {
    return giftTypeItems;
  }

  public void setRenderFilter(Boolean renderFilter)
  {
    this.renderFilter = renderFilter;
  }

  public Boolean getRenderFilter()
  {
    return renderFilter;
  }


  public void setFilterFromDate(Date filterFromDate)
  {
    this.filterFromDate = filterFromDate;
  }

  public Date getFilterFromDate()
  {
    return filterFromDate;
  }

  public void setFilterToDate(Date filterToDate)
  {
    this.filterToDate = filterToDate;
  }

  public Date getFilterToDate()
  {
    return filterToDate;
  }

  public void setSelectedItemCampaignName(String selectedItemCampaignName)
  {
    this.selectedItemCampaignName = selectedItemCampaignName;
  }

  public String getSelectedItemCampaignName()
  {
    return selectedItemCampaignName;
  }

  public void setSelectedItemGiftName(String selectedItemGiftName)
  {
    this.selectedItemGiftName = selectedItemGiftName;
  }

  public String getSelectedItemGiftName()
  {
    return selectedItemGiftName;
  }

  public void setSelectedItemUserTypeName(String selectedItemUserTypeName)
  {
    this.selectedItemUserTypeName = selectedItemUserTypeName;
  }

  public String getSelectedItemUserTypeName()
  {
    return selectedItemUserTypeName;
  }

  public void setSelectedItemGiftTypeName(String selectedItemGiftTypeName)
  {
    this.selectedItemGiftTypeName = selectedItemGiftTypeName;
  }

  public String getSelectedItemGiftTypeName()
  {
    return selectedItemGiftTypeName;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setRenderDataTable(Boolean renderDataTable)
  {
    this.renderDataTable = renderDataTable;
  }

  public Boolean getRenderDataTable()
  {
    return renderDataTable;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }

  public void setFilterResult(ArrayList<CampaignReportDTO> filterResult)
  {
    this.filterResult = filterResult;
  }

  public ArrayList<CampaignReportDTO> getFilterResult()
  {
    return filterResult;
  }

  public void setRenderPOS(Boolean renderPOS)
  {
    this.renderPOS = renderPOS;
  }

  public Boolean getRenderPOS()
  {
    return renderPOS;
  }

  public void setRenderUser(Boolean renderUser)
  {
    this.renderUser = renderUser;
  }

  public Boolean getRenderUser()
  {
    return renderUser;
  }


  public void setUserValue(String userValue)
  {
    this.userValue = userValue;
  }

  public String getUserValue()
  {
    return userValue;
  }

  public void setPosValue(String posValue)
  {
    this.posValue = posValue;
  }

  public String getPosValue()
  {
    return posValue;
  }


  public void setRenderCleanDate(Boolean renderCleanDate)
  {
    this.renderCleanDate = renderCleanDate;
  }

  public Boolean getRenderCleanDate()
  {
    return renderCleanDate;
  }


  public void setRenderExportToCSV(Boolean renderExportToCSV)
  {
    this.renderExportToCSV = renderExportToCSV;
  }

  public Boolean getRenderExportToCSV()
  {
    return renderExportToCSV;
  }

  public void setHiddenValue(String hiddenValue)
  {
    this.hiddenValue = hiddenValue;
  }

  public String getHiddenValue()
  {
    return hiddenValue;
  }

  public void setInput1(HtmlInputHidden input1)
  {
    this.input1 = input1;
  }

  public HtmlInputHidden getInput1()
  {
    return input1;
  }

  public void setDataTable2(HtmlDataTable dataTable2)
  {
    this.dataTable2 = dataTable2;
  }

  public HtmlDataTable getDataTable2()
  {
    return dataTable2;
  }

  public void setVoucherResult(ArrayList<VoucherReportDTO> voucherResult)
  {
    this.voucherResult = voucherResult;
  }

  public ArrayList<VoucherReportDTO> getVoucherResult()
  {
    return voucherResult;
  }

  public void setRenderDataTable2(Boolean renderDataTable2)
  {
    this.renderDataTable2 = renderDataTable2;
  }

  public Boolean getRenderDataTable2()
  {
    return renderDataTable2;
  }

  public void setMoreVoucherValue(String moreVoucherValue)
  {
    this.moreVoucherValue = moreVoucherValue;
  }

  public String getMoreVoucherValue()
  {
    return moreVoucherValue;
  }

  public void setDialValue(String dialValue)
  {
    this.dialValue = dialValue;
  }

  public String getDialValue()
  {
    return dialValue;
  }

  public void setDataTable3(HtmlDataTable dataTable3)
  {
    this.dataTable3 = dataTable3;
  }

  public HtmlDataTable getDataTable3()
  {
    return dataTable3;
  }

  public void setSelectedItemTimeOption(String selectedItemTimeOption)
  {
    this.selectedItemTimeOption = selectedItemTimeOption;
  }

  public String getSelectedItemTimeOption()
  {
    return selectedItemTimeOption;
  }

  public void setSelectedItemGiftOption(String selectedItemGiftOption)
  {
    this.selectedItemGiftOption = selectedItemGiftOption;
  }

  public String getSelectedItemGiftOption()
  {
    return selectedItemGiftOption;
  }

  public void setDataTable4(HtmlDataTable dataTable4)
  {
    this.dataTable4 = dataTable4;
  }

  public HtmlDataTable getDataTable4()
  {
    return dataTable4;
  }

  public void setTimeResult(ArrayList<TimeChartDTO> timeResult)
  {
    this.timeResult = timeResult;
  }

  public ArrayList<TimeChartDTO> getTimeResult()
  {
    return timeResult;
  }

  public void setRenderdayOfWeek(Boolean renderdayOfWeek)
  {
    this.renderdayOfWeek = renderdayOfWeek;
  }

  public Boolean getRenderdayOfWeek()
  {
    return renderdayOfWeek;
  }

  public void setRenderNormalField(Boolean renderNormalField)
  {
    this.renderNormalField = renderNormalField;
  }

  public Boolean getRenderNormalField()
  {
    return renderNormalField;
  }

  public void setTimeTableName(String timeTableName)
  {
    this.timeTableName = timeTableName;
  }

  public String getTimeTableName()
  {
    return timeTableName;
  }

  public void settimeTableValue(String timeTableValue)
  {
    this.timeTableValue = timeTableValue;
  }

  public String getTimeTableValue()
  {
    return timeTableValue;
  }

  public void setRenderBarChart(Boolean renderBarChart)
  {
    this.renderBarChart = renderBarChart;
  }

  public Boolean getRenderBarChart()
  {
    return renderBarChart;
  }


  public void cleanValuesCampaignReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/campaignReport.jsp";


    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
  }


  public void cleanValuesPOSReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/posReport.jsp";

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  }


  public void cleanValuesUserReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/userReport.jsp";

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  }


  public void cleanValuesUSERSummaryReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/userSummryReport.jsp";

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  }
  public void cleanValuesPOSMONEYReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/POSMoneyReport.jsp";

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  }

  public void cleanValuesVoucherReport()
  {
    emptyValues();
    String path = "/GVMS/faces/Admin/Campaign/Report/VoucherReport.jsp";
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


  }
  
  public void cleanValuesDeletedVoucherReport()
  {
    emptyValues();
    jsfUtils.putInSession ( true , "isDeleted" );
    String path = "/GVMS/faces/Admin/Campaign/Report/DeletedVoucherReport.jsp";
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
      }
    catch (IOException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
         
      }


  }
  public void cleanValuesCSReport() throws SQLException
  {
     jsfUtils.putInSession( "admin","csReportFrom");   
    jsfUtils.putInSession( new CampaignReport(),"CampaignReport_Bean");
    jsfUtils.putInSession( new HistoryBean(),"CSHistory_Bean");
    
    jsfUtils.putInSession( "admin","csReportFrom");
    jsfUtils.redirect("Admin/Campaign/Report/CSReport", "GVMS");
    
    
//    if (jsfUtils.containInSession ( "CSHistory_Bean" ))jsfUtils.removeFromSession ( "CSHistory_Bean" );
//    
//    String path = "/GVMS/faces/Admin/Campaign/Report/CSReport.jsp";
//    try
//      {
//        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
//      }
//    catch (IOException e)
//      {
//         
//      }


  }

  public void cleanValuesTimeChart()
  {
    emptyValues();
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/Report/TimeChart.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
  }
  public void cleanValuesWS()
  {
    emptyValues();
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/Report/WSReport.jsp");
      }
    catch (IOException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
  }


  public void setDataTable6(HtmlDataTable dataTable6)
  {
    this.dataTable6 = dataTable6;
  }

  public HtmlDataTable getDataTable6()
  {
    return dataTable6;
  }

  public void setRenderDataTable6(Boolean renderDataTable6)
  {
    this.renderDataTable6 = renderDataTable6;
  }

  public Boolean getRenderDataTable6()
  {
    return renderDataTable6;
  }

  public void setSelectedItemVoucherStatus(String selectedItemVoucherStatus)
  {
    this.selectedItemVoucherStatus = selectedItemVoucherStatus;
  }

  public String getSelectedItemVoucherStatus()
  {
    return selectedItemVoucherStatus;
  }

  public ArrayList < SelectItem > getCampaigns()
{
   return campaigns;
}

public void setCampaigns(ArrayList < SelectItem > campaigns)
{
   this.campaigns = campaigns;
}

public ArrayList < SelectItem > getGifts()
{
   return gifts;
}

public void setGifts(ArrayList < SelectItem > gifts)
{
   this.gifts = gifts;
}

public ArrayList < SelectItem > getChannels()
{
   return channels;
}

public void setChannels(ArrayList < SelectItem > channels)
{
   this.channels = channels;
}

public ArrayList < SelectItem > getSubChannels()
{
   return subChannels;
}

public void setSubChannels(ArrayList < SelectItem > subChannels)
{
   this.subChannels = subChannels;
}

public String getSelectedCampaign()
{
   return selectedCampaign;
}

public void setSelectedCampaign(String selectedCampaign)
{
   this.selectedCampaign = selectedCampaign;
}

public String getSelectedGift()
{
   return selectedGift;
}

public void setSelectedGift(String selectedGift)
{
   this.selectedGift = selectedGift;
}

public String getSelectedChannel()
{
   return selectedChannel;
}

public void setSelectedChannel(String selectedChannel)
{
   this.selectedChannel = selectedChannel;
}

public String getSelectedSubChannel()
{
   return selectedSubChannel;
}

public void setSelectedSubChannel(String selectedSubChannel)
{
   this.selectedSubChannel = selectedSubChannel;
}

public String getAccountNumber()
{
   return accountNumber;
}

public void setAccountNumber(String accountNumber)
{
   this.accountNumber = accountNumber;
}

public String getDialNumber()
{
   return dialNumber;
}

public void setDialNumber(String dialNumber)
{
   this.dialNumber = dialNumber;
}

public HashMap getCampaignAndGifts()
{
   return campaignAndGifts;
}

public void setCampaignAndGifts(HashMap campaignAndGifts)
{
   this.campaignAndGifts = campaignAndGifts;
}

  public int getCampaignStatus (Connection conn,String campaignId,String searchCritera){
  
    String sql = "SELECT "+searchCritera+" FROM GVMS_CAMPAIGN where campaign_id="+campaignId;
    int campaignID = 0;
    ResultSet resultFilter = ExecuteQueries.executeQuery(conn,sql);
    try 
    {
      if (resultFilter.next())
      {
        campaignID = resultFilter.getInt(1);
      }
      
      resultFilter.getStatement().close();
      resultFilter.close();
    } catch (Exception ex) 
    {
      new com.mobinil.gvms.system.utility.PrintException().printException(ex);;
    } 
    
    return campaignID;
  
  }

public void setPosMoneyArray(ArrayList<VoucherReportDTO> posMoneyArray)
{
   this.posMoneyArray = posMoneyArray;
}

public ArrayList<VoucherReportDTO> getPosMoneyArray()
{
   return posMoneyArray;
}

    /**
     * @return the statusItems
     */
    public ArrayList<SelectItem> getStatusItems() {
        return statusItems;
    }

    /**
     * @param statusItems the statusItems to set
     */
    public void setStatusItems(ArrayList<SelectItem> statusItems) {
        this.statusItems = statusItems;
    }

    /**
     * @return the selectedStatusName
     */
    public String getSelectedStatusName() {
        return selectedStatusName;
    }

    /**
     * @param selectedStatusName the selectedStatusName to set
     */
    public void setSelectedStatusName(String selectedStatusName) {
        this.selectedStatusName = selectedStatusName;
    }

    /**
     * @return the TypeItems
     */
    public ArrayList<SelectItem> getTypeItems() {
        return typeItems;
    }

    /**
     * @param TypeItems the TypeItems to set
     */
    public void setTypeItems(ArrayList<SelectItem> TypeItems) {
        this.typeItems = TypeItems;
    }

    /**
     * @return the selectedTypeName
     */
    public String getSelectedTypeName() {
        return selectedTypeName;
    }

    /**
     * @param selectedTypeName the selectedTypeName to set
     */
    public void setSelectedTypeName(String selectedTypeName) {
        this.selectedTypeName = selectedTypeName;
    }

    public ArrayList<SelectItem> getItemsFromHashMap(HashMap<String, String> lookupHM, boolean addStarFirstRow) {
        ArrayList<SelectItem> items = new ArrayList<SelectItem>();
        SelectItem st = null;
        if (addStarFirstRow) {
            st = new SelectItem("-1", "*");
            items.add(st);
        }
        if (lookupHM != null && !lookupHM.isEmpty()) {
            for (String id : lookupHM.keySet()) {
                st = new SelectItem(id,lookupHM.get(id));
                items.add(st);
            }

        }
        return items;
    }

    /**
     * @return the totalGiftValues
     */
    public String getTotalGiftValues() {
        return totalGiftValues;
    }

    /**
     * @param totalGiftValues the totalGiftValues to set
     */
    public void setTotalGiftValues(String totalGiftValues) {
        this.totalGiftValues = totalGiftValues;
    }
  
}
