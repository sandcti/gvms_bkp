/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.ws.dao.SystemServerDAO;
import com.mobinil.gvms.system.admin.ws.model.SystemServerModel;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.DTOComparator;
import com.mobinil.gvms.system.utility.jsfUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.ArrayList;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

/**
 *
 * @author mabdelaal
 */
public class WSManagement_Bean {

    public ArrayList<SystemServerModel> WSManageArray = new ArrayList<SystemServerModel>();
    public ArrayList<SelectItem> campaignList = new ArrayList<SelectItem>();
    SystemServerModel dataItem;
    public HtmlDataTable dataTable1;
    public HtmlOutputLabel msgWarn;
    public Integer rowCount = 10;
    int pageIndex = 0;
    public List<SystemServerModel> selectedDataList;
    public String sortField = null;
    public boolean sortAscending = true;
    public HtmlInputText userName = new HtmlInputText();
    public HtmlInputText password = new HtmlInputText();
    public String campId = "";

    /** Creates a new instance of WSManagement_Bean */
    public WSManagement_Bean() {
        new leftMenu().checkUserInSession();
        WSManageArray = SystemServerDAO.getAllServers();
    }

    public void editWS() {

        dataItem = (SystemServerModel) dataTable1.getRowData();
        jsfUtils.removeFromSession("WebServiceInfo");
        password.setValue(dataItem.getPassword());
        userName.setValue(dataItem.getUserName());
        campId = dataItem.getCampaignId();
        jsfUtils.putInSession(dataItem, "WebServiceInfo");
        fillCampignList();
        jsfUtils.redirectGVMS("Admin/ws/EditWS.jsp");

    }

    public void newWS() {
        fillCampignList();
        jsfUtils.redirectGVMS("Admin/ws/CreateWS.jsp");
    }

    public void getSelectedItems() {
        try {


            // Get selected items.
            selectedDataList = new ArrayList<SystemServerModel>();
            int count = 1;
            Connection con = DBConnection.getConnection();
            Statement st = null;
            for (int counter = (dataTable1.getRows() * pageIndex); counter < WSManageArray.size();
                    counter++) {

                if (counter == (dataTable1.getRows() * (pageIndex + 1))) {
                    break;
                }
                SystemServerModel dataItems = new SystemServerModel();
                dataItems = WSManageArray.get(counter);
                if (dataItems.getSelected()) {
                    if (st == null) {
                        st = con.createStatement();
                    }

                    selectedDataList.add(dataItems);
                    dataItems.setSelected(false); // Reset.
                    String id = dataItems.getCampaignId();
                    String userName = dataItems.getUserName();
                    String password = dataItems.getPassword();
                    SystemServerDAO.deleteServer(id, userName, password, st);

                }
                count++;

            }
            st.close();
            DBConnection.closeConnection(con);
            
            // Do your thing with the MyData items in List selectedDataList.
            back_Action();
            msgWarn.setValue("Server(s) deleted");
        } catch (Exception e) {
            new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }

    }

    public void pageFirst() {
        dataTable1.setFirst(0);
        pageIndex = 0;
    }

    public void pagePrevious() {
        dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
        pageIndex--;
    }

    public void pageNext() {
        dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
        pageIndex++;
        //System.out.println("get rows " + dataTable1.getRows());
        //System.out.println("get row count " + dataTable1.getRowCount());
    }

    public void pageLast() {
        pageIndex = dataTable1.getRowCount();
        int count = dataTable1.getRowCount();
        int rows = dataTable1.getRows();
        dataTable1.setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public void sortDataTable(ActionEvent event) {
        String sortFieldAttribute = getAttribute(event, "sortField");

        // Get and set sort field and sort order.
        if (getSortField() != null && getSortField().equals(sortFieldAttribute)) {
            setSortAscending(!isSortAscending());
        } else {
            setSortField(sortFieldAttribute);
            setSortAscending(true);
        }

        // Sort results.
        if (getSortField() != null) {
            Collections.sort(WSManageArray, new DTOComparator(getSortField(), isSortAscending()));
        }


    }

    public void updateServer() {
        String validateResult = validateInputs();
        if (validateResult != null) {
            msgWarn.setValue(validateResult);
        } else {
            String userName = ((String) getUserName().getValue()).trim();
            String password = ((String) getPassword().getValue()).trim();
            SystemServerModel newSystemServerModel = new SystemServerModel();
            newSystemServerModel.setCampaignId(campId);
            newSystemServerModel.setUserName(userName);
            newSystemServerModel.setPassword(password);
            int resultInt = SystemServerDAO.updateServer(newSystemServerModel, (SystemServerModel) jsfUtils.getFromSession("WebServiceInfo"));
            if (resultInt > 0) {
                msgWarn.setValue("Update complete.");
            } else {
                msgWarn.setValue("Update failed.");
            }
        }
    }

    public void newServer() {
        String validateResult = validateInputs();
        if (validateResult != null) {
            msgWarn.setValue(validateResult);
        } else {
            String userName = ((String) getUserName().getValue()).trim();
            String password = ((String) getPassword().getValue()).trim();
            SystemServerModel newSystemServerModel = new SystemServerModel();
            newSystemServerModel.setCampaignId(campId);
            newSystemServerModel.setUserName(userName);
            newSystemServerModel.setPassword(password);
            int resultInt = SystemServerDAO.insertServer(newSystemServerModel);
            if (resultInt > 0) {
                msgWarn.setValue("Creation complete.");
            } else {
                msgWarn.setValue("Creation failed.");
            }
        }
    }

    public void back_Action() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("WSManagement_Bean");
        jsfUtils.redirectGVMS("Admin/ws/WebManagement.jsp");

    }

    public static String getAttribute(ActionEvent event, String name) {
        return (String) event.getComponent().getAttributes().get(name);
    }

    /**
     * @return the WSManageArray
     */
    public ArrayList<SystemServerModel> getWSManageArray() {
        return WSManageArray;
    }

    /**
     * @param WSManageArray the WSManageArray to set
     */
    public void setWSManageArray(ArrayList<SystemServerModel> WSManageArray) {
        this.WSManageArray = WSManageArray;
    }

    /**
     * @return the dataTable1
     */
    public HtmlDataTable getDataTable1() {
        return dataTable1;
    }

    /**
     * @param dataTable1 the dataTable1 to set
     */
    public void setDataTable1(HtmlDataTable dataTable1) {
        this.dataTable1 = dataTable1;
    }

    /**
     * @return the rowCount
     */
    public Integer getRowCount() {
        return rowCount;
    }

    /**
     * @param rowCount the rowCount to set
     */
    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * @return the sortAscending
     */
    public boolean isSortAscending() {
        return sortAscending;
    }

    /**
     * @param sortAscending the sortAscending to set
     */
    public void setSortAscending(boolean sortAscending) {
        this.sortAscending = sortAscending;
    }

    /**
     * @return the msgWarn
     */
    public HtmlOutputLabel getMsgWarn() {
        return msgWarn;
    }

    /**
     * @param msgWarn the msgWarn to set
     */
    public void setMsgWarn(HtmlOutputLabel msgWarn) {
        this.msgWarn = msgWarn;
    }

    /**
     * @return the userName
     */
    public HtmlInputText getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(HtmlInputText userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public HtmlInputText getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(HtmlInputText password) {
        this.password = password;
    }

    /**
     * @return the campId
     */
    public String getCampId() {
        return campId;
    }

    /**
     * @param campId the campId to set
     */
    public void setCampId(String campId) {
        this.campId = campId;
    }

    public String validateInputs() {
        String userName = "";
        String password = "";
        String retrunString = null;
        if (getUserName() == null || ((String) getUserName().getValue()).compareTo("") == 0) {
            retrunString = "please fill user name.";
        } else if (getPassword() == null || ((String) getPassword().getValue()).compareTo("") == 0) {
            retrunString = "please fill password.";
        }
        if (retrunString != null) {
            return retrunString;
        }

        userName = ((String) getUserName().getValue()).trim();
        password = ((String) getPassword().getValue()).trim();
        SystemServerModel oldModel = (SystemServerModel) jsfUtils.getFromSession("WebServiceInfo");
        if (oldModel!=null && oldModel.getCampaignId().compareTo(campId)==0 && oldModel.getPassword().compareTo(password)==0 && oldModel.getUserName().compareTo(userName)==0)
        {
            return "Update Complete";
        }
        
        if (SystemServerDAO.checkDublicatedServerName(userName, password,campId)) {
            retrunString = "User name and password already exists for another server.";
        } else {
            retrunString = null;
        }

        return retrunString;
    }

    public void fillCampignList() {
        setCampaignList(new ArrayList<SelectItem>());
        HashMap<String, String> campaignsHM = SystemServerDAO.getAllCampaigns();
        if (campaignsHM != null && !campaignsHM.isEmpty()) {
            for (String campaignId : campaignsHM.keySet()) {
                getCampaignList().add(new SelectItem(campaignId, campaignsHM.get(campaignId)));
            }
        }
    }

    /**
     * @return the campaignList
     */
    public ArrayList<SelectItem> getCampaignList() {
        return campaignList;
    }

    /**
     * @param campaignList the campaignList to set
     */
    public void setCampaignList(ArrayList<SelectItem> campaignList) {
        this.campaignList = campaignList;
    }
}
