package com.mobinil.gvms.system.admin.beans;




import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

import com.mobinil.gvms.system.utility.DBConnection;


public class AddUser
{
	

  public String systemUserSelected;

  public AddUser()
  {
	  
      new leftMenu().checkUserInSession();
  }

  public String view_Action()
  {
    //System.out.println("Selected value is: " + systemUserSelected);
    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("User_Bean"))
    {
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddUser_Bean");

    }
    if (systemUserSelected.compareTo("reporter") == 0)
      {



        try
          {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/User.jsp");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userLogin",
                                                                                       "reporter");
          }
        catch (Exception e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }
    if (systemUserSelected.compareTo("admin") == 0)
      {

        

        try
          {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/User.jsp");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userLogin", 
                                                                                       "admin");
          }
        catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      }
    if (systemUserSelected.compareTo("user") == 0)
      {
        try
          {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/User.jsp");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userLogin", 
                                                                                       "user");
          }
        catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

      }
    if (systemUserSelected.compareTo("cs") == 0)
    {
      try
        {
          FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/User.jsp");
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userLogin", 
                                                                                     "cs");
        }
      catch (Exception e)
        {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }

    }
    return null;
  }


  public HtmlForm form1;

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setSystemUserSelected(String systemUserSelected)
  {
    this.systemUserSelected = systemUserSelected;
  }

  public String getSystemUserSelected()
  {
    return systemUserSelected;
  }


}
