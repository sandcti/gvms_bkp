package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.campaign.DTO.*;
import com.mobinil.gvms.system.admin.campaign.DAO.*;


import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.jsfUtils;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;



public class CampaignUsers
{
	
  public HtmlForm form1;

  public ArrayList<currentVoucherClass> totalVouchers = new ArrayList<currentVoucherClass>();
  public HtmlDataTable dataTable1;
  
  public CampaignUsers()
  {
    campaignTableClass campaignInfo= 
      (campaignTableClass) jsfUtils.getFromSession("campaignInfo");
      int campaignId = campaignInfo.getCampaignId();
    String campaignStatus = campaignInfo.getCampaignStatus();      

   try
   {
      Connection conn = DBConnection.getConnection();
     totalVouchers = campaignDAO.getCampaignUsers(conn,totalVouchers,campaignId,campaignStatus);
     DBConnection.closeConnections ( conn );
   }
   catch (SQLException e)
   {
     new com.mobinil.gvms.system.utility.PrintException().printException(e);
   }
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
  }

  public void pageLast()
  {
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setTotalVouchers(ArrayList<currentVoucherClass> totalVouchers)
  {
    this.totalVouchers = totalVouchers;
  }

  public ArrayList<currentVoucherClass> getTotalVouchers()
  {
    return totalVouchers;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }
}
