package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.jsfUtils;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class leftMenu {

    public String userNameLbl;

    public leftMenu() {
        //System.out.println("");
        checkUserInSession();
        LoginDTO ldto = (LoginDTO) jsfUtils.getFromSession("adminInfo");
        ldto = ldto == null ? (LoginDTO) jsfUtils.getFromSession("repInfo") : ldto;
        userNameLbl = ldto.getFullName();
    }

    public String logout_Action() {

        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        HttpSession session =
                (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        String[] beanNames = session.getValueNames();
        for (int x = 0; x < beanNames.length; x++) {

            if (sessionMap.containsKey(beanNames[x])) {
                sessionMap.remove(beanNames[x]);
            }




            //System.out.println("bean names in place " + x + " is " + beanNames[x]);
        }





        try {

            FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/AdminLogin.jsp");
        } catch (IOException e) {
        }


        return "logout";
    }

    public String campaign() {
        return "createCampaign";
    }

    public String gift() {
        return "gift";
    }

    public String giftType() {
        return "giftType";
    }

    public String user() {
        return "user";
    }

    public String userType() {
        return "userType";
    }

    public void setUserNameLbl(String userNameLbl) {
        this.userNameLbl = userNameLbl;
    }

    public String clean(String url, String beanName, String key, Object value) {


        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();


        if (sessionMap.containsKey(beanName)) {
            sessionMap.remove(beanName);
        }
        sessionMap.put(key, value);



        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);

        } catch (IOException e) {
        }
        return url;


    }

    public void cleanGift() {
        clean("/GVMS/faces/Admin/Gift/Gift.jsp", "Gift_Bean", "GoToGiftConstructor", " ");
    }

    public void cleanGiftType() {
        clean("/GVMS/faces/Admin/Gift/GiftType/GiftType.jsp", "GiftType_Bean", "GoToGftTypeConstructor", " ");
    }

    public void cleanUser() {
        clean("/GVMS/faces/Admin/User/AddUser.jsp", "AddUser_Bean", "GoToUserConstructor", " ");
    }

    public void cleanUserType() {
        clean("/GVMS/faces/Admin/User/UserType/UserType.jsp", "UserType_Bean", "GoToUserTypeConstructor", " ");
    }

    public void cleanWebService() {
        clean("/GVMS/faces/Admin/ws/WebManagement.jsp", "WSManagement_Bean", "GoToWSConstructor", " ");
    }

    public void cleanUnredeem() {
        clean("/GVMS/faces/Admin/Unredeem/AdminUnredeem.jsp", "AdminUnredeem_Bean", "GoToUnredeemConstructor", " ");
    }

    public void cleanCampaign() {
        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

        HttpSession session =
                (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        String[] beanNames = session.getValueNames();
        for (int x = 0; x < beanNames.length; x++) {
            if (beanNames[x].contains("Additional") || beanNames[x].contains("Validation")
                    || beanNames[x].contains("Campaign") || beanNames[x].contains("Vouchers")) {

                if (sessionMap.containsKey(beanNames[x])) {
                    sessionMap.remove(beanNames[x]);
                }

            }


            //System.out.println("bean names in place " + x + " is " + beanNames[x]);
        }




        clean("/GVMS/faces/Admin/Campaign/Campaign.jsp", "Campaign_Bean", "GoToCampaignConstructor", " ");
    }

    public String getUserNameLbl() {
        return userNameLbl;
    }

    public void checkUserInSession() {

        LoginDTO ldto = (LoginDTO) jsfUtils.getFromSession("adminInfo");
        ldto = ldto == null ? (LoginDTO) jsfUtils.getFromSession("repInfo") : ldto;
        boolean userChecking = jsfUtils.containInSession((String) "adminInfo");
        userChecking = !userChecking ? jsfUtils.containInSession((String) "repInfo") : userChecking;

        if (ldto == null) {
            logout_Action();
        }

        if (!userChecking) {
            logout_Action();
        }



    }
}
