package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.admin.usermanagement.DTO.AdminDTO;
import com.mobinil.gvms.system.admin.usermanagement.DTO.UserDTO;
import com.mobinil.gvms.system.utility.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class User
{
	
  String user4mSession;
  public Boolean dtAdminRender = true;
  public Boolean dtUserRender = true;
  
  public ArrayList<UserDTO> userArray;
  public HtmlDataTable dataTable1;
  public String newUser;
  UserDTO dataItem;
  public String userFiledSelected;
  public String valueForSearch;

  public List selectedDataList;
  public String msgWarn;
  public Integer rowCount = 10;
  int pageIndex = 0;
  public String sortField = null;
  public boolean sortAscending = true;
  public boolean adminCheck;
  public boolean menuRender = false;

  public static final String reporterString = "reporter";


  public User()
  {
    new leftMenu().checkUserInSession();
    
    

   

    user4mSession = 
    ((String) jsfUtils.getFromSession("userLogin"));
//System.out.println("user4mSession issssssssssss "+user4mSession);

    if (user4mSession.compareTo("admin") == 0)
      {
        adminCheck = false;
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", 
                                                                                   "newAdmin");
        menuRender = true;
        dtUserRender = false;
        newUser = "New Admin";

        userArray = new ArrayList<UserDTO>();
        userArray=UserManagementDAO.getAdmins("");


      }
if (user4mSession.compareTo(reporterString) == 0)
      {
        adminCheck = true;
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user",
                                                                                   "newReporter");
        menuRender = true;
        dtUserRender = false;
        newUser = "New Reporter";

        userArray = new ArrayList<UserDTO>();
        userArray=UserManagementDAO.getReports("");


      }
    if (user4mSession.compareTo("user") == 0)
      {
        adminCheck = true;
        dtAdminRender = false;
        newUser = "New User";
        userArray = new ArrayList<UserDTO>();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", 
                                                                                   "newUser");
        userArray=UserManagementDAO.getUsers("");

        
      }
    if (user4mSession.compareTo("cs") == 0)
    {
    	menuRender = true;
    	dtUserRender = false;
    	newUser = "New Customer Support";
    	adminCheck = true;
    	userArray=UserManagementDAO.getCustomerSupports("");
    }


  }


  public void search()
  {

    userArray.clear();
    String constrainsSearch = "";
    //System.out.println("user4mSession issssssssss "+user4mSession);
//System.out.println("userFiledSelected isssss "+userFiledSelected);

    if (user4mSession.compareTo("admin") == 0)
      {
        adminCheck = false;
        dtUserRender = false;
        newUser = "New Admin";
        userArray = new ArrayList<UserDTO>();
        if (userFiledSelected.compareTo("0") != 0)
          {
            if (userFiledSelected.compareTo("1") == 0)
              {
                constrainsSearch += "and Admin_Name='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("4") == 0)
              {
                constrainsSearch += "and ADMIN_EMAIL='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("5") == 0)
              {
                constrainsSearch += "and ADMIN_PHONE='" + valueForSearch + "'";
              }

          }
        else
          {
            valueForSearch = "";
            constrainsSearch="";

          }     
        userArray=UserManagementDAO.getAdmins(constrainsSearch);


      }
if (user4mSession.compareTo(reporterString) == 0)
      {
        adminCheck = false;
        dtUserRender = false;
        newUser = "New Reporter";
        userArray = new ArrayList<UserDTO>();
        if (userFiledSelected.compareTo("0") != 0)
          {
            if (userFiledSelected.compareTo("1") == 0)
              {
                constrainsSearch += "and Admin_Name='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("4") == 0)
              {
                constrainsSearch += "and ADMIN_EMAIL='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("5") == 0)
              {
                constrainsSearch += "and ADMIN_PHONE='" + valueForSearch + "'";
              }

          }
        else
          {
            valueForSearch = "";
            constrainsSearch="";

          }
        userArray=UserManagementDAO.getReports(constrainsSearch);


      }
    if (user4mSession.compareTo("cs") == 0)
    {
      adminCheck = true;
      dtUserRender = false;
      newUser = "New Customer Support";
      userArray = new ArrayList<UserDTO>();
      if (userFiledSelected.compareTo("0") != 0)
        {
          if (userFiledSelected.compareTo("1") == 0)
            {
              constrainsSearch += "and USER_FULL_NAME='" + valueForSearch + "'";
            }
          if (userFiledSelected.compareTo("4") == 0)
            {
              constrainsSearch += "and USER_E_MAIL='" + valueForSearch + "'";
            }
          if (userFiledSelected.compareTo("5") == 0)
            {
              constrainsSearch += "and USER_PHONE_NUMBER='" + valueForSearch + "'";
            }

        }
      else
        {
          valueForSearch = "";
          constrainsSearch="";

        }     
      userArray=UserManagementDAO.getCustomerSupports(constrainsSearch);


    }
    if (user4mSession.compareTo("user") == 0)
      {
        adminCheck = true;
        dtAdminRender = false;
        newUser = "New User";
        userArray = new ArrayList<UserDTO>();

        if (userFiledSelected.compareTo("0") != 0)
          {
            if (userFiledSelected.compareTo("1") == 0)
              {
                constrainsSearch += "and USER_FULL_NAME='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("2") == 0)
              {
                constrainsSearch += "and USER_TYPE_NAME='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("3") == 0)
              {
                constrainsSearch += "and USER_POS_CODE='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("4") == 0)
              {
                constrainsSearch += "and USER_E_MAIL='" + valueForSearch + "'";
              }
            if (userFiledSelected.compareTo("5") == 0)
              {
                constrainsSearch += "and USER_PHONE_NUMBER='" + valueForSearch + "'";
              }

          }
        else
          {
            valueForSearch = "";
            constrainsSearch="";

          }


        userArray=UserManagementDAO.getUsers(constrainsSearch);
      }


  }

  public void exportToCSV()
  {

    List<List<String>> csvList = new ArrayList<List<String>>();
    String reportName = "users";

    for (int counter = 0; counter < userArray.size(); counter++)
      {
        if (counter == 0)
          {
            csvList.add(Arrays.asList(new String[]
                  { "Name", "Type", "POS Code", "E-mail", "Phone" }));
          }

        if (user4mSession.compareTo("admin") == 0 || user4mSession.compareTo(reporterString) == 0)
          {
            csvList.add(Arrays.asList(new String[]
                  { userArray.get(counter).getName(), userArray.get(counter).getEmail() + "", 
                    userArray.get(counter).getPhone() + "" }));
          }
        else
          {
            csvList.add(Arrays.asList(new String[]
                  { userArray.get(counter).getName(), userArray.get(counter).getUserType(), 
                    userArray.get(counter).getPos(), userArray.get(counter).getEmail() + "", 
                    userArray.get(counter).getPhone() + "" }));

          }


      }
    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);

    FacesContext fc = FacesContext.getCurrentInstance();

    

    String templatePath = jsfUtils.getParamterFromIni("defualtPath");   

    //System.out.println("tempPath " + templatePath);
    //System.out.println("File Name Is =" + filename);
    filename = reportName + "-" + filename; // report name
    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".csv";


    InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
    try
      {
        FileUtil.write(new File(templatePath + "UserFiles/" + filename), csvInput);
        File csvFile = new File(templatePath + "UserFiles/", filename);
        //System.out.println("File report is " + csvFile.getAbsolutePath());
        HttpServletUtil.downloadFile(response, csvFile, false);
      }
    catch (IOException e)
      {new com.mobinil.gvms.system.utility.PrintException().printException(e);
        //System.out.println("Download file failed: " + e.getMessage());
         
      }
    fc.responseComplete();

  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(userArray, new DTOComparator(sortField, sortAscending));
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public String edit_Action()
  {

    // Add event code here...
    String SQL = "";
    if (user4mSession.compareTo("admin") == 0 ||user4mSession.compareTo(reporterString) == 0)
      {
        int adminId = 0;
        dataItem = (UserDTO) dataTable1.getRowData();
        adminId = dataItem.getId();
       
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_user_name", 
                		dataItem.getUserName());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_pass", 
                                                                                           dataItem.getPassword());
                
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("system_user_id", 
                		dataItem.getSystemUserId());
       
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_sys_id", 
                dataItem.getSystemUserId());

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_email", 
                                                                                   dataItem.getEmail());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_name", 
                                                                                   dataItem.getName());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_phone", 
                                                                                   dataItem.getPhone());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_status", 
                                                                                   dataItem.getStatusId() + 
                                                                                   "");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                  user4mSession.compareTo(reporterString) == 0 ? "editReporter" : "editAdmin");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("admin_id", 
                                                                                   adminId);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("lockAdminStatus", 
                                                                                   dataItem.getUserLocked());

        return "AddAdmin";
      }
    if (user4mSession.compareTo("user") == 0)
      {
        int userId = 0;
        dataItem = (UserDTO) dataTable1.getRowData();
        userId = dataItem.getId();
    
        //System.out.println("userId isssssss "+userId);

                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("system_user_id", 
                		dataItem.getSystemUserId());
                
                //System.out.println("dataItem.getSystemUserId() isssssssssss "+dataItem.getSystemUserId());
                //System.out.println("dataItem.getUserName() isssssssssss "+dataItem.getUserName());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_name", 
                		dataItem.getUserName());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_pass", 
                		dataItem.getPassword());
           
           

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_email", 
                                                                                   dataItem.getEmail());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_full_name", 
                                                                                   dataItem.getName());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_type", 
                                                                                   dataItem.getUserType());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_phone", 
                                                                                   dataItem.getPhone());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_pos", 
                                                                                   dataItem.getPos());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_status", 
                                                                                   dataItem.getStatusId() + 
                                                                                   "");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                   "editUser");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_id", 
                                                                                   userId);

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("lockUserStatus", 
                                                                                   dataItem.getUserLocked());

        return "AddUser";
      }
    
    if (user4mSession.compareTo("cs") == 0)
    {
      int userId = 0;
      dataItem = (UserDTO) dataTable1.getRowData();
      userId = dataItem.getId();
    

              FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("system_user_id", 
            		  dataItem.getSystemUserId());
              FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_name", 
            		  dataItem.getUserName());
              FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_pass", 
            		  dataItem.getPassword());
              
        
        
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_email", 
                                                                                 dataItem.getEmail());
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_full_name", 
                                                                                 dataItem.getName());      
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_phone", 
                                                                                 dataItem.getPhone());      
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_status", 
                                                                                 dataItem.getStatusId() + 
                                                                                 "");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                 "editCS");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user_id", 
                                                                                 userId);

      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("lockUserStatus", 
                                                                                 dataItem.getUserLocked());

      return "AddCS";
    }
    return "";


  }

  public String getSelectedItems() throws SQLException
  {
    if (user4mSession.compareTo("user") == 0)removeUser("GVMS_User");
    
    if (user4mSession.compareTo("cs") == 0)removeUser("GVMS_CS");
      
    if (user4mSession.compareTo("admin") == 0||user4mSession.compareTo(reporterString) == 0)
      {
        // Get selected items.
        selectedDataList = new ArrayList<AdminDTO>();
        int count = 1;
        Connection conn = DBConnection.getConnection();
        for (int counter = (dataTable1.getRows() * pageIndex); counter < userArray.size(); 
             counter++)


          {
            if (counter == (dataTable1.getRows() * (pageIndex + 1)))
              {
                break;
              }
            UserDTO dataItems = new UserDTO();
            dataItems = userArray.get(counter);
            if (dataItems.getSelected())
              {
                selectedDataList.add(dataItems);
                dataItems.setSelected(false); // Reset.
                int id = dataItems.getId();
                int delete = 
                  ExecuteQueries.executeNoneQuery("update GVMS_Admin set Admin_STATUS_ID=3 where Admin_ID=" + 
                                                  id);
                ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set User_STATUS_ID=3 where system_User_ID="
                        + id);

                if (delete == 1)
                  {
                    UpdateClass.updateAdminInfo(id+"", true);
                    msgWarn = "Admin " + id + "deleted";
                  }
                else
                  {
                    msgWarn = "Admin not deleted";
                  }
              }
            count++;

          }
        DBConnection.closeConnections ( conn );
        msgWarn = "Admin deleted";
        // Do your thing with the MyData items in List selectedDataList.


      }

    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");
    return "refreshUser"; // Navigation case.
  }
  
  public void removeUser(String tableName) throws SQLException{
	  

      // Get selected items.
      selectedDataList = new ArrayList<UserDTO>();
      int count = 1;
      Connection conn = DBConnection.getConnection();
      for (int counter = (dataTable1.getRows() * pageIndex); counter < userArray.size(); 
           counter++)


        {
          if (counter == (dataTable1.getRows() * (pageIndex + 1)))
            {
              break;
            }
          UserDTO dataItems = new UserDTO();
          dataItems = userArray.get(counter);
          if (dataItems.getSelected())
            {
              selectedDataList.add(dataItems);
              dataItems.setSelected(false); // Reset.
              int id = dataItems.getId();
              int delete = 
                ExecuteQueries.executeNoneQuery("update "+tableName+" set User_STATUS_ID=3 where User_ID=" + 
                                                id);


              ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set User_STATUS_ID=3 where system_User_ID=" + 
                                              id);

              if (delete == 1)
                {
                  if (tableName.compareTo("GVMS_User")==0)UpdateClass.updateUserInfo(id+"", true);
                  if (tableName.compareTo("GVMS_CS")==0)UpdateClass.updateCSInfo(id+"", true);
                  msgWarn = "User " + id + "deleted";
                }
              else
                {
                  msgWarn = "User not deleted";
                }
            }
          count++;

        }
      DBConnection.closeConnections ( conn );
      msgWarn = "User deleted";
      // Do your thing with the MyData items in List selectedDataList.
      //        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");


    
  }

  public String newUser_Action()
  {
     if (user4mSession.compareTo("admin") == 0 || user4mSession.compareTo(reporterString) == 0)
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                   user4mSession.compareTo(reporterString) == 0 ? "newReporter":"newAdmin");
        return "AddAdmin";
      }
    else if (user4mSession.compareTo("user") == 0)
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                   "newUser");
        return "AddUser";
      }
    else if (user4mSession.compareTo("cs") == 0)
    {
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editUser", 
                                                                                 "newCS");
      return "AddCS";
    }
    return "";

  }


  public String back_Action()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");
    return "backToUser";
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setUserArray(ArrayList<UserDTO> userArray)
  {
    this.userArray = userArray;
  }

  public ArrayList<UserDTO> getUserArray()
  {
    return userArray;
  }


  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }


  public void setDtAdminRender(Boolean dtAdminRender)
  {
    this.dtAdminRender = dtAdminRender;
  }

  public Boolean getDtAdminRender()
  {
    return dtAdminRender;
  }

  public void setDtUserRender(Boolean dtUserRender)
  {
    this.dtUserRender = dtUserRender;
  }

  public Boolean getDtUserRender()
  {
    return dtUserRender;
  }

  public void setNewUser(String newUser)
  {
    this.newUser = newUser;
  }

  public String getNewUser()
  {
    return newUser;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }


  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }

  public void setAdminCheck(boolean adminCheck)
  {
    this.adminCheck = adminCheck;
  }

  public boolean isAdminCheck()
  {
    return adminCheck;
  }

  public void setUserFiledSelected(String userFiledSelected)
  {
    this.userFiledSelected = userFiledSelected;
  }

  public String getUserFiledSelected()
  {
    return userFiledSelected;
  }

  public void setValueForSearch(String valueForSearch)
  {
    this.valueForSearch = valueForSearch;
  }

  public String getValueForSearch()
  {
    return valueForSearch;
  }

  public void setMenuRender(boolean menuRender)
  {
    this.menuRender = menuRender;
  }

  public boolean isMenuRender()
  {
    return menuRender;
  }
}
