package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpSession;


public class AdminForgetPassword
{
	
  public String inputValue;

  public String msgWarn;
  public String msgWarn1;
  String rightConfirmationCode;
  String adminId;
  public Boolean renderRelogin = false;
  public Boolean renderNewPass = false;
  public String password;
  public String confirmPassword;
  String userName;
  Integer sysUserId;
  public String emailConfirmLabel;
  public Boolean renderConfirmButton;
  public Boolean renderGetPassButton = true;

  public AdminForgetPassword()
  {

    emailConfirmLabel = "E-mail:";
    renderConfirmButton = false;
    msgWarn = "";
    msgWarn1 = "";

  }


  public String adminConfirmation_Action() throws SQLException
  {
  
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }
    Connection conn = DBConnection.getConnection();
    ResultSet emailRS = 
      ExecuteQueries.executeQuery(conn,"select t1.ADMIN_EMAIL,t2.SYSTEM_USER_NAME,t2.SYSTEM_USER_ID from GVMS_ADMIN t1 left join GVMS_SYSTEM_USER t2 on t1.SYSTEM_USER_ID = t2.SYSTEM_USER_ID where t1.ADMIN_EMAIL = '" + 
                                  inputValue + "'");
    try
      {
        if (emailRS.next())
          {
            userName = emailRS.getString("SYSTEM_USER_NAME");
            sysUserId = emailRS.getInt("SYSTEM_USER_ID");
            String SQL = "select dbms_random.String('x',15) str from dual";

            ResultSet generatePWRS = ExecuteQueries.executeQuery(conn,SQL);
            if (generatePWRS.next())
              {
                rightConfirmationCode = generatePWRS.getString("STR");
                adminId = emailRS.getInt("SYSTEM_USER_ID") + "";

                //                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("generatedPassword",adminId);
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("code", 
                                                                                           rightConfirmationCode);
                ExecuteQueries.executeNoneQuery("update GVMS_SYSTEM_USER set USER_CODE='" + 
                                                rightConfirmationCode + 
                                                "', EXPIRE_CODE_DATE=sysdate where SYSTEM_USER_ID='" + 
                                                sysUserId + "'");
                String from = "Mobinil";
                String to = inputValue;
                String subject = "GVMS admin password";
                String message = "Your new password is " + rightConfirmationCode;

                SendMail sendMail = new SendMail(from, to, subject, message,rightConfirmationCode);
                sendMail.send();
                inputValue = "";
                emailConfirmLabel = "Activation Code:";
                renderConfirmButton = true;
                renderGetPassButton = false;


              }
            

          }
        else
          {
            msgWarn = "This email does not exists.";
            DBConnection.closeConnections ( conn );
            return null;

          }
        
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );

    return null;
  }

  public String confirmedAction()
  {
    
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }

    try
      {
       Connection conn = DBConnection.getConnection();
        ResultSet getCodeExpireDate = 
          ExecuteQueries.executeQuery(conn,"select EXPIRE_CODE_DATE,USER_CODE from GVMS_SYSTEM_USER where SYSTEM_USER_ID='" + 
                                      sysUserId + "'");

        if (getCodeExpireDate.next())
          {
            Date expireCode = getCodeExpireDate.getDate("EXPIRE_CODE_DATE");
            int day = expireCode.getDate();
            int month = expireCode.getMonth() + 1;
            int year = expireCode.getYear() + 1900;
            if (day == new Date().getDate() && month == (new Date().getMonth() + 1) && 
                year == (new Date().getYear() + 1900))
              {

                rightConfirmationCode = getCodeExpireDate.getString("USER_CODE");

                if (rightConfirmationCode.compareTo(inputValue) == 0)
                  {

                    renderNewPass = true;

                    renderRelogin = true; // login with new password
                    msgWarn = "Activation code is correct.";
                    DBConnection.closeConnections ( conn );

                    return null;


                  }
                else
                  {

                    msgWarn = "Invalid activation code.";
                    DBConnection.closeConnections ( conn );
                    return null;


                  }
              }
            else
              {
                msgWarn = "This activation code expired, please regenerate another one.";
                DBConnection.closeConnections ( conn );
                return null;

              }
          }

      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }


  public String reloginAction()
  {
    
    if (msgWarn.compareTo("") != 0 || msgWarn1.compareTo("") != 0)
      {
        msgWarn = "";
        msgWarn1 = "";
      }    
    //System.out.println("in back action");
    
    if (password.compareTo(confirmPassword) == 0)
      { 

        if (password.compareTo("") == 0 || confirmPassword.compareTo("") == 0)
          {

            msgWarn1 = "Please insert password and confirm.";
            return null;
          }
        try
          {
            String passEncripted = MD5Class.MD5(password);
            String SecurityError =  SecurityUtils.checkPass(password, passEncripted, 
                                            userName.trim(), 
                                            sysUserId + "");
                                            
            if (SecurityError.compareTo("")!=0)
            {
             msgWarn1 = SecurityError;             
             return null;
            }
            else
            {
             if(UserManagementDAO.updateSystemUserPassword( passEncripted,sysUserId+""))
             {
             UserManagementDAO.updateUserLastPass(sysUserId+"",passEncripted);
             msgWarn1 = "Your new password was updated to your account.";
             }
            }


          }
        catch (Exception e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }


      }
    else
      {
        msgWarn1 = "Please retype password and confirmation.";
        return null;


      }
   
    return "relogin";
  }


  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }


  public void setRenderRelogin(Boolean renderRelogin)
  {
    this.renderRelogin = renderRelogin;
  }

  public Boolean getRenderRelogin()
  {
    return renderRelogin;
  }

  public void setRenderNewPass(Boolean renderNewPass)
  {
    this.renderNewPass = renderNewPass;
  }

  public Boolean getRenderNewPass()
  {
    return renderNewPass;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setConfirmPassword(String confirmPassword)
  {
    this.confirmPassword = confirmPassword;
  }

  public String getConfirmPassword()
  {
    return confirmPassword;
  }

  public void setMsgWarn1(String msgWarn1)
  {
    this.msgWarn1 = msgWarn1;
  }

  public String getMsgWarn1()
  {
    return msgWarn1;
  }

  public void setEmailConfirmLabel(String emailConfirmLabel)
  {
    this.emailConfirmLabel = emailConfirmLabel;
  }

  public String getEmailConfirmLabel()
  {
    return emailConfirmLabel;
  }

  public void setRenderConfirmButton(Boolean renderConfirmButton)
  {
    this.renderConfirmButton = renderConfirmButton;
  }

  public Boolean getRenderConfirmButton()
  {
    return renderConfirmButton;
  }

  public void setRenderGetPassButton(Boolean renderGetPassButton)
  {
    this.renderGetPassButton = renderGetPassButton;
  }

  public Boolean getRenderGetPassButton()
  {
    return renderGetPassButton;
  }

  public void setInputValue(String inputValue)
  {
    this.inputValue = inputValue;
  }

  public String getInputValue()
  {
    return inputValue;
  }
}
