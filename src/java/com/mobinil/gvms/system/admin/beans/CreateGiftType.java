package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.giftType.DAO.GiftTypeDAO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;


public class CreateGiftType
{
	
  public HtmlForm form1;
  public HtmlInputText giftTypeNameInput;
  public HtmlInputTextarea giftTypeDesTexArea;

  public String status;

  public String giftStatusSelected;
  public HtmlCommandButton addGT_cmd;
  public HtmlOutputLabel msgWarn;


  public CreateGiftType()
  {
      new leftMenu().checkUserInSession();
  }

  public String back_Action()
  {

    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("GiftType_Bean");
    return "backtoGiftType";
  }

  public String AddGiftType()
  {
    String giftTypeName = ((String) giftTypeNameInput.getValue()).trim();
    String desc = (String) giftTypeDesTexArea.getValue();
	  if (desc.length() > 1000)
      {
        msgWarn.setValue("Maximum characters for description are 1000.");

        return null;
      }   
   
    if (giftTypeName==null || giftTypeName.compareTo("") == 0 ||
         status.compareTo("0") == 0)
      {

        msgWarn.setValue("Please fill star boxes.");
        return null;
      }
    
      
    try
      {
          String dd= GiftTypeDAO.checkDublicatedGiftTypeName(giftTypeName,0);
          if (dd.compareTo("")!=0)
          {
            msgWarn.setValue("this gift type already exists.");
            return null;
          }
        else
          {

            if (status.compareTo("0") != 0)
              {
                  String creatorName = 
                    ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
                  
                int insertGiftType =  GiftTypeDAO.insrtGiftType(giftTypeName, status,creatorName,(String) giftTypeDesTexArea.getValue());

                if (insertGiftType == 1)
                  {
                    msgWarn.setValue("One new gift Type inserted");
                  }
                else
                  {
                    msgWarn.setValue("Gift type name is missing");
                  }


              }
            else
              {
                msgWarn.setValue("Please select status for this gift type.");
              }
            
          }
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    return null;
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setGiftTypeNameInput(HtmlInputText inputText1)
  {
    this.giftTypeNameInput = inputText1;
  }

  public HtmlInputText getGiftTypeNameInput()
  {
    return giftTypeNameInput;
  }

  public void setGiftTypeDesTexArea(HtmlInputTextarea inputTextarea1)
  {
    this.giftTypeDesTexArea = inputTextarea1;
  }

  public HtmlInputTextarea getGiftTypeDesTexArea()
  {
    return giftTypeDesTexArea;
  }


  public void setGiftStatusSelected(String giftStatusSelected)
  {
    this.giftStatusSelected = giftStatusSelected;
  }

  public String getGiftStatusSelected()
  {
    return giftStatusSelected;
  }

  public void setAddGT_cmd(HtmlCommandButton commandButton1)
  {
    this.addGT_cmd = commandButton1;
  }

  public HtmlCommandButton getAddGT_cmd()
  {
    return addGT_cmd;
  }


  public void setMsgWarn(HtmlOutputLabel outputLabel1)
  {
    this.msgWarn = outputLabel1;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }


  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }
}
