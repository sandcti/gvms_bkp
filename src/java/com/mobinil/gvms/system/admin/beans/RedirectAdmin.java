package com.mobinil.gvms.system.admin.beans;

import java.io.IOException;

import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

public class RedirectAdmin
{
  public HtmlForm form1;

  public RedirectAdmin()
  {

    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/AdminLogin.jsp");
      }
    catch (IOException e)
      {

      }
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }
}
