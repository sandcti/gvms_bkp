package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;


public class CreateCampaign
{
	
  public HtmlCommandButton createCampaign;


  public String cmd_value;
  public String vTValue;
  public String gValue;
  public String javaScript;
  public String daysToExpire;

  public String cSValue;
  public Boolean renderCreate = false;
  public Boolean renderUpdate = false;
  public Boolean disableStartDate = false;
  public Boolean disableEndDate = false;
  public Boolean disableExpiredCampaign = false;


  public Integer userTypeListSize;

  ArrayList<SelectItem> oldUserTypeNames;
  public ArrayList<String> UTValue = new ArrayList<String>();
  public ArrayList<String> giftValue = new ArrayList<String>();
  public ArrayList<String> giftSelected = new ArrayList<String>();
  public ArrayList<String> UTSelectedValue = new ArrayList<String>();
  public ArrayList<SelectItem> vTArray = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> giftLabels = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> giftSelectedLabels = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> gArray = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> UTArray = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> cSArray = new ArrayList<SelectItem>();
  public ArrayList<SelectItem> UTSelectedArray = new ArrayList<SelectItem>();

  public ArrayList<Boolean> readOnlyArray = new ArrayList<Boolean>();
  public ArrayList<String> sessionValue = new ArrayList<String>();

  String voucherId;
  String giftId;
  String userTypeId;
  String campaignStatusId;
  String bridg;
  String giftName;
  String creatorName;
  String voucherTypeName;
  String campaignStatus;
  Integer campaignId;
  Date start;
  Date end;
  Date creation;
  ResultSet voucherRS =null;
  ResultSet giftRS = null;
  ResultSet userTypeRS =null; 
  Connection conn = null;

  public String createLbl;
  public HtmlCommandButton updateButton;
  public HtmlOutputText campaignStatusLbl;
  public Boolean renderStatus = true;
  public HtmlCommandButton commandButton1;
  public HtmlGraphicImage graphicImage1;
  public String campaignName;
  public String campaignDescription;
  public String javaScriptAlert = "";
  public String javaScriptConfirmDeleteVouchers = "";

  public Date startDate;
  
  campaignTableClass ctc=null;

  public CreateCampaign() throws SQLException
  {

            new leftMenu().checkUserInSession();
    
            conn = DBConnection.getConnection();

             voucherRS = ExecuteQueries.executeQuery(conn,"Select * from GVMS_VOUCHER_TYPE ");
             giftRS = ExecuteQueries.executeQuery(conn,"Select * from GVMS_GIFT where gift_status_id=1");
             userTypeRS = ExecuteQueries.executeQuery(conn,"Select * from GVMS_USER_TYPE where user_type_status_id=1");
      
            
            
//    for (int y = 0; y < UTSelectedArray.size(); y++)
//      {
//
//        //System.out.println("value in UTSelectedArray " + UTSelectedArray.get(y) + " in cell " + y);
//
//      }

    bridg = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("campaignStatus");
    if (bridg.compareTo("CLOSED") == 0 || bridg.compareTo("NEW") == 0 || 
        bridg.compareTo("ACTIVE") == 0)
      {
           ctc = 
                (campaignTableClass) jsfUtils.getFromSession("campaignInfo");
//          giftId = ctc.getGiftId()+"";
          giftId = 0+"";
                  creatorName = ctc.getCreatedBy();
                  voucherTypeName = ctc.getVoucherTypeName()+"";
                  campaignStatus = ctc.getCampaignStatus();
                  campaignId = ctc.getCampaignId();
                  campaignName = ctc.getCampaignName();
                  campaignDescription = ctc.getCampaignDescription();
                  daysToExpire = ctc.getVoucherExpireDays ( );
                  if (campaignDescription==null)campaignDescription="";                	  
                   HashMap campaignGifts= ctc.getCampaignGifts();
          DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                  try
                  {
                    startDate = df.parse(ctc.getStartDate());
                   // endDate = df.parse(ctc.getEndDate());
                    creation = df.parse(ctc.getCreationDate());
                  }
                  catch (ParseException e)
                  {
                   new com.mobinil.gvms.system.utility.PrintException().printException(e);
                  }
//                  startDate = ctc.getStartDate();
//                  endDate = ctc.getEndDate();
//                  creation =ctc.getCreationDate();
                  
          

//        if (bridg.compareTo("CLOSED") == 0)
//          {
//            disableExpiredCampaign = true;
//            disableStartDate = true;
//            disableEndDate = true;
//            fillControls();
//            renderUpdate = false;
//            renderCreate = false;
//
//            readOnlyArray.add(true);
//            sessionValue.add(creatorName);
//            readOnlyArray.add(true);
//            readOnlyArray.add(true);
//            vTArray.add(new SelectItem(voucherTypeName));
//            gArray.add(new SelectItem(giftName));
//            cSArray.add(new SelectItem(campaignStatus));
//            campiagnUserTypes();
//          }
        if (bridg.compareTo("NEW") == 0)
          {
        	javaScriptConfirmDeleteVouchers="if (!confirm('This operation may remove campaign voucher due to changing voucher type!')) return false";
            renderUpdate = true;
            renderCreate = false;
            readOnlyArray.add(true);
            disableExpiredCampaign = false;
            disableStartDate = false;
            disableEndDate = false;
            sessionValue.add(creatorName);
            readOnlyArray.add(false);
            readOnlyArray.add(false);
            constructPage(false);
            campiagnUserTypes();
            campiagnGifts();
            fillControls();

          }
        if (bridg.compareTo("ACTIVE") == 0)
          {
            disableExpiredCampaign = true;
            renderUpdate = true;

            renderCreate = false;
            disableStartDate = true;
            disableEndDate = false;
            readOnlyArray.add(true);
            sessionValue.add(creatorName);
            readOnlyArray.add(true);
            readOnlyArray.add(false);
            constructPage(false);
            fillControls();
            campiagnGifts();
            campiagnUserTypes();
          }

      }

    if (bridg.compareTo("Create Campaign")==0)
      {
        //System.out.println("Create New");
                creatorName = 
                    ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
        renderUpdate = false;
        renderCreate = true;
        disableExpiredCampaign = false;
        disableStartDate = false;
        disableEndDate = false;
        startDate = new Date();
        readOnlyArray.add(true);


        readOnlyArray.add(false);


        readOnlyArray.add(false);
        constructPage(true);

      }
    for (int y = 0; y < UTArray.size(); y++)
      {

        //System.out.println("value in UTArray " + UTArray.get(y).getLabel() + " in cell " + y);

      }

    userTypeListSize = UTArray.size();
  }

  public void selectionChanged(ValueChangeEvent evt)
  {

  }

  public void campiagnUserTypes()
  {
    UTArray.clear();
    oldUserTypeNames = new ArrayList<SelectItem>();
    ArrayList<Integer> id = new ArrayList<Integer>();

    for (int y = 0; y < UTSelectedArray.size(); y++)
      {

        //System.out.println("value in UTSelectedArray " + UTSelectedArray.get(y) + " in cell " + y);

      }

    ResultSet numCampaignUserRS = 
      ExecuteQueries.executeQuery(conn,"Select t1.USER_TYPE_ID,t2.USER_TYPE_NAME from GVMS_CAMPAIGN_USER t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID = t2.USER_TYPE_ID where CAMPAIGN_ID=" + 
                                  campaignId);

    try
      {
        while (numCampaignUserRS.next())
          {
            id.add(numCampaignUserRS.getInt("USER_TYPE_ID"));
            //System.out.println("id is " + numCampaignUserRS.getInt("USER_TYPE_ID"));

            UTSelectedArray.add(new SelectItem(numCampaignUserRS.getString("USER_TYPE_ID"), 
                                               numCampaignUserRS.getString("USER_TYPE_NAME")));
            oldUserTypeNames.add(new SelectItem(numCampaignUserRS.getString("USER_TYPE_ID"), 
                                                numCampaignUserRS.getString("USER_TYPE_NAME")));
          }

        userTypeRS.beforeFirst();
        while (userTypeRS.next())
          {

            if (!id.contains(userTypeRS.getInt("USER_TYPE_ID")))
              {
                //System.out.println("user type ID is " + userTypeRS.getInt("USER_TYPE_ID"));
                UTArray.add(new SelectItem(userTypeRS.getString("USER_TYPE_ID"), 
                                           userTypeRS.getString("USER_TYPE_NAME")));
              }
          }

        numCampaignUserRS.getStatement().close();
        numCampaignUserRS.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    //for (int i =0;i<UTSelectedArray.size();i++){
    ////System.out.println("user type UTSelectedArray in cell "+i+" is "+UTSelectedArray.get(i).getLabel());
    //
    //}
    //    for (int i =0;i<UTArray.size();i++){
    //    //System.out.println("user type UTArray in cell "+i+" is "+UTArray.get(i).getLabel());
    //
    //    }
  }
  public void campiagnGifts()
  {
	  giftLabels.clear();    
    ArrayList<String> id = new ArrayList<String>();
    
    ResultSet numCampaignGiftRS = 
      ExecuteQueries.executeQuery(conn,"SELECT T1.GIFT_ID, T2.GIFT_NAME  FROM GVMS_CAMPAIGN_GIFT T1 ,GVMS_GIFT T2 WHERE T1.GIFT_ID = T2.GIFT_ID and CAMPAIGN_ID = " + 
                                  campaignId);

    try
      {
        while (numCampaignGiftRS.next())
          {
            id.add(numCampaignGiftRS.getString("GIFT_ID"));
            giftSelectedLabels.add(new SelectItem(numCampaignGiftRS.getString("GIFT_ID"), 
            		numCampaignGiftRS.getString("GIFT_NAME")));            
          }

        giftRS.beforeFirst();
        while (giftRS.next())
          {

            if (!id.contains(giftRS.getString("GIFT_ID")))
              {
                
                giftLabels.add(new SelectItem(giftRS.getString("GIFT_ID"), 
                		giftRS.getString("GIFT_NAME")));
              }
          }

        numCampaignGiftRS.getStatement().close();
        numCampaignGiftRS.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    //for (int i =0;i<UTSelectedArray.size();i++){
    ////System.out.println("user type UTSelectedArray in cell "+i+" is "+UTSelectedArray.get(i).getLabel());
    //
    //}
    //    for (int i =0;i<UTArray.size();i++){
    //    //System.out.println("user type UTArray in cell "+i+" is "+UTArray.get(i).getLabel());
    //
    //    }
  }

  public void fillControls()
  {
    vTValue = voucherTypeName;
    //gValue = giftName;


  }


  public String create_Action() throws SQLException
  {

    //System.out.println("date is " + startDate);
    //System.out.println("length of description is " + campaignDescription.length());
    int giftId = 0;

    int voucherTypeId = 0;
    int campaignStatusId = 2;

    String currDate = "to_date('" + getDateTime() + "','mm/dd/yyyy HH24:MI:SS')";
    ;
    String campaignId ;
    campaignName= campaignName.trim();
    boolean checkCampaignName = jsfUtils.haveSpecialCharacter ( campaignName );
    if (checkCampaignName ) 
    {
       
       createLbl = "Please remove any special character or white spaces from campaign name.";
       return null;
       
    }

    try
      {

        voucherRS.beforeFirst();
//        giftRS.beforeFirst();


        while (voucherRS.next())
          {
            if (voucherRS.getString("VOUCHER_TYPE_NAME").compareTo(vTValue) == 0)
              {
                voucherTypeId = voucherRS.getInt("VOUCHER_TYPE_ID");
              }
          }

//        while (giftRS.next())
//          {
//            if (giftRS.getString("GIFT_NAME").compareTo(gValue) == 0)
//              {
//                giftId = giftRS.getInt("GIFT_ID");
//              }
//          }

      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    Date currentDate = new Date();
    //    if (startDate.compareTo(endDate) == 0 || endDate.compareTo(currentDate) == 0)
    //      {
    //        campaignStatusId = 3;
    //      }
    //        if (startDate.equals(currentDate) || startDate.before(currentDate) || 
    //            currentDate.before(endDate) || startDate.before(endDate) || endDate.after(currentDate) )
    //          {
    //            campaignStatusId = 2;
    //          }
    //System.out.println("start date is" + startDate);
    
    //System.out.println("curr date is" + currentDate);
    
    if ( startDate != null)
      {
       
            if (startDate.before(currentDate))
              {
                campaignStatusId = 2;
              }
            if (startDate.after(currentDate))
              {
                campaignStatusId = 1;
              }
            

          

          
      }
    else
      {
        createLbl = "Please choose Start Date.";
        return null;
      }
    if (daysToExpire!=null && daysToExpire.compareTo ( "" )!=0)
    {
    Integer intDaysToExpire = Integer.parseInt ( daysToExpire );
    if (intDaysToExpire==0)
    {
       createLbl = "Please type value in \"Voucher Days Expire\" more than 0 or leave it empty.";
       return null;
    }
    }

    //    if (currentDate.before(startDate) || 
    //        (currentDate.before(endDate) && currentDate.before(startDate)) || 
    //        startDate.before(endDate) || endDate.after(currentDate))
    //      {
    //        campaignStatusId = 1;
    //      }


    //    if (endDate.before(currentDate) || endDate.before(startDate))
    //      {
    //        createLbl="Invalid date");
    //        return null;
    //      }
    //System.out.println("length of description is " + campaignDescription.length());
    //System.out.println("Campaign Status Is " + campaignStatusId);
    //System.out.println("user type seleted value " + UTValue + "size is " + UTValue.size());
    //System.out.println("gifts seleted value " + giftSelected + "size is " + giftSelected.size());

    //    // Take care  user input is null cause there is no control to user name. "--Select One--"

    if (giftSelected.size() != 0)
      {
        if (campaignName.compareTo("") == 0 || campaignName == null)
          {
            createLbl = "campaign must have name.";
            return null;
          }
        if (vTValue.compareTo("--Select One--") != 0)
          {
            if (campaignDescription.length() < 1001)
              {
                if (UTSelectedArray.size() != 0)
                  {
                   if (daysToExpire==null || daysToExpire.compareTo ( "" )==0)daysToExpire=jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" );
                   
                   String SQL = 
                      "";
                  
                  SQL = "";
                  campaignId = DBConnection.getSequenceNextVal ( conn , "GVMS_CAMPAIGN_SEQ_ID" );

                     SQL = 
                      "insert into GVMS_CAMPAIGN (CAMPAIGN_ID,CAMPAIGN_NAME,CAMPAIGN_DESCRIPTION,CAMPAIGN_START_DATE,CAMPAIGN_CREATION_DATE,VOUCHER_TYPE_ID,CAMPAIGN_STATUS_ID,CAMPAIGN_CREATEDBY,CAMPAIGN_ERROR_FILE,VOUCHER_EXPIRE_DAYS)" + 
                      "values("+campaignId+",'" + campaignName.trim() + "','" + 
                      campaignDescription + "',"  + convertDate(startDate) + "," + 
                      currDate + "," + voucherTypeId + "," + 
                      campaignStatusId + ",'" + creatorName + "','0','"+daysToExpire+"')";


                    int inserCampaginRS = ExecuteQueries.executeNoneQuery(SQL);
                    SQL.equals(null);


                    if (inserCampaginRS == 1)
                      {
                        UpdateClass.updateCampaignInfo(campaignId, false);
                        
                        for (int count = 0; count < giftSelected.size(); count++)
                        {

                          ExecuteQueries.executeNoneQuery("insert into GVMS_CAMPAIGN_GIFT (CAMPAIGN_ID,GIFT_ID ) Values(" +                                                            
                                                          campaignId + "," + 
                                                          giftSelected.get(count) +")");
                        }

                        for (int count = 0; count < UTSelectedValue.size(); count++)
                          {

                            ExecuteQueries.executeNoneQuery("insert into GVMS_CAMPAIGN_USER(CAMPAIGN_USER_ID,CAMPAIGN_ID,USER_TYPE_ID,CAMPAIGN_USER_CREATION_DATE,CAMPAIGN_USER_CREATEDBY) Values" + 
                                                            "(GVMS_CAMPAIGN_USER_SEQ_ID.nextval," + 
                                                            campaignId + "," + 
                                                            UTSelectedValue.get(count) + "," + 
                                                            currDate + ",'" + creatorName + "')");
                          }


                        //System.out.println("Start Create Process");
                        //        String fieldName;
                        //        String fieldType;
                        //        String totalFieldStatment;

                        //            "  VOUCHER_DATE_TIME           DATE,\n" + 
                        //            "  USER_ID                     INTEGER,\n" + 
                        SQL = 
                            "CREATE TABLE GVMS_REDEEM_CAMPAIGN_" + campaignId + "(\n" + 
                            "  REDEEM_CAMPAIGN_ID          INTEGER           NOT NULL,\n" + 
                            "  REDEEM_VOUCHER_DIAL_NUMBER  INTEGER,\n" + 
                            "  REDEEM_VOUCHER_NUMBER       INTEGER,\n" + 
                            "  CURRENT_VOUCHER_ID          INTEGER)";
                        //            "  USER_TYPE_ID                INTEGER,\n" + 
                        //            "  GIFT_ID                     INTEGER,\n" + 
                        //            "  GIFT_VALUE                  INTEGER\n" + ")\n"


                        boolean createTable = ExecuteQueries.execute(conn,SQL);
                        SQL = "";
                        //System.out.println("Process Result:" + createTable);
                        //System.out.println("End Create Process");

                        //System.out.println("Start Alter Process");

                        SQL = 
                            "ALTER TABLE GVMS_REDEEM_CAMPAIGN_" + campaignId + " ADD (\n" + "  CONSTRAINT GVMS_CUSTOMER_CAMP_PK\n" + 
                            " PRIMARY KEY\n" + " (REDEEM_CAMPAIGN_ID));\n" + "\n" + "\n";
                        createTable = ExecuteQueries.execute(conn,SQL);
                        SQL.equals(null);

                        //System.out.println("Process Result:" + createTable);
                        //System.out.println("End Alter Process");


                        SQL = 
                            "CREATE SEQUENCE CAMPAIGN_" + campaignId + "_SEQ \n" + "  START WITH 1\n" + 
                            "  MAXVALUE 99999999999999999\n" + "  MINVALUE 1\n";


                        boolean createSeq = ExecuteQueries.execute(conn,SQL);
                        SQL.equals(null);

                        if (createTable == false && createSeq == false)
                          {
                            //System.out.println("Table and sequance create");
                            createLbl = "Campaign Created";
//                            String tempcampaignName = campaignName.replace("'", "");
//                            tempcampaignName = campaignName.replace("\"", "");
//                            javaScript = "<script>"+tempcampaignName+" Campaign Created Successfully javaScript</script>";
//                            javaScriptAlert = "<script>"+tempcampaignName+" Campaign Created Successfully javaScriptAlert</script>";
                            jsfUtils.removeFromSession("Campaign_Bean");
                            jsfUtils.removeFromSession("CreateCampaign_Bean");
                            try
                              {
                                FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/Campaign.jsp");
                              }
                            catch (IOException e)
                              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
                              }
                          }
                        else
                          {
                            //System.out.println("Table and sequance doesn't create");
                            createLbl = "Creation Failed";
                          }
                        DBConnection.closeConnections ( conn );

                      }

                    else
                      {

                        createLbl = "Creation Failed";
                      }
                    try
                      {
                        voucherRS.getStatement().close();
                        voucherRS.close();
                        giftRS.getStatement().close();
                        giftRS.close();
                        userTypeRS.getStatement().close();
                        userTypeRS.close();
                        return null;
                      }
                    catch (SQLException e)
                      {
                         new com.mobinil.gvms.system.utility.PrintException().printException(e);
                      }

                    //              else{
                    //                createLbl="Please fill End date and Start Date together.");
                    //              }
                  }
                else
                  {
                    createLbl = "Please insert at least one user type.";
                    return null;
                  }
              }
            else
              {
                createLbl = "Maximum space for description is 1000 character.";
                return null;
              }
          }
        else
          {
            createLbl = "Please select voucher type for " + campaignName + ".";
            return null;
          }
      }
    else
      {
        createLbl = "Please select gift for " + campaignName + ".";
        return null;
      }


   
    return null;
  }

  public String getDateTime()
  {
    Date currentDate = new Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }

  public String convertDate(Date undefineDate)
  {
    String values;
    if (undefineDate == null)
      {

        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'dd/mm/yyyy')";
        return values;
      }
    else
      {
        int day = undefineDate.getDate();
        int year = undefineDate.getYear() + 1900;
        int month = undefineDate.getMonth() + 1;
        //        int houre = undefineDate.getHours();
        //        int min = undefineDate.getMinutes();
        //        int sec = undefineDate.getSeconds();

        values = "to_date('" + month + "/" + day + "/" + year + "', 'MM/DD/YYYY HH24:MI:SS')";

        return values;
      }
  }

  public void constructPage(boolean fillUserType)
  {
    try
      {

        int count = 0;
        while (voucherRS.next())
          {

            if (count == 0)
              {
                vTArray.add(new SelectItem("--Select One--"));

              }


            vTArray.add(new SelectItem(voucherRS.getString("VOUCHER_TYPE_NAME")));

            count++;
          }


        
        if (fillUserType == true)
          {
        	while (giftRS.next())
            {            
          	giftLabels.add(new SelectItem(giftRS.getString("GIFT_ID"), 
          			giftRS.getString("GIFT_NAME")));

            }
            while (userTypeRS.next())
              {

                UTArray.add(new SelectItem(userTypeRS.getString("USER_TYPE_ID"), 
                                           userTypeRS.getString("USER_TYPE_NAME")));

              }
          }


      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

  }


  public void setCreateCampaign(HtmlCommandButton commandButton1)
  {
    this.createCampaign = commandButton1;
  }

  public HtmlCommandButton getCreateCampaign()
  {
    return createCampaign;
  }


  public void setVTArray(ArrayList<SelectItem> vTArray)
  {
    this.vTArray = vTArray;
  }

  public ArrayList<SelectItem> getVTArray()
  {
    return vTArray;
  }

  public void setGArray(ArrayList<SelectItem> gArray)
  {
    this.gArray = gArray;
  }

  public ArrayList<SelectItem> getGArray()
  {
    return gArray;
  }

  public void setUTArray(ArrayList<SelectItem> UTArray)
  {
    this.UTArray = UTArray;
  }

  public ArrayList<SelectItem> getUTArray()
  {
    return UTArray;
  }

  public void setCSArray(ArrayList<SelectItem> cSArray)
  {
    this.cSArray = cSArray;
  }

  public ArrayList<SelectItem> getCSArray()
  {
    return cSArray;
  }

  public void setVTValue(String vTValue)
  {
    this.vTValue = vTValue;
  }

  public String getVTValue()
  {
    return vTValue;
  }

  public void setGValue(String gValue)
  {
    this.gValue = gValue;
  }

  public String getGValue()
  {
    return gValue;
  }


  public void setCSValue(String cSValue)
  {
    this.cSValue = cSValue;
  }

  public String getCSValue()
  {
    return cSValue;
  }


  public void setCmd_value(String cmd_value)
  {
    this.cmd_value = cmd_value;
  }

  public String getCmd_value()
  {
    return cmd_value;
  }

  public void setUpdateButton(HtmlCommandButton commandButton1)
  {
    this.updateButton = commandButton1;
  }

  public HtmlCommandButton getUpdateButton()
  {
    return updateButton;
  }

  public String update_cmd() throws SQLException
  {
    // Add event code here...

    for (int y = 0; y < UTArray.size(); y++)
      {

        //System.out.println("value in UTArray " + UTArray.get(y).getLabel() + " in cell " + y);

      }
    //System.out.println("Start date from control is " + startDate);
    //System.out.println("Start date from function is " + convertDate(startDate));
    
    
    String currDate = "to_date('" + getDateTime() + "','mm/dd/yyyy HH24:MI:SS')";    
    String creatorName = 
      (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userNameAdmin");
    int giftId = 0;
    int voucherId = 0;
    
    String SQLUpadateCampaign = "";
    String SQLDeleteUserTypes = "";
    //    if (bridg.compareTo("EXPIRED") == 0)
    //      {
    //        SQLUpadateCampaign = "";
    //      }
    Date currentDate = new Date();
    if (startDate != null)
      {
       if (startDate.before(currentDate))
       {
         campaignStatusId = "2";
         javaScriptAlert = "<script>alert('campaign will be active');</script>";
       }
     if (startDate.after(currentDate))
       {
         campaignStatusId = "1";
         javaScriptAlert = "<script>alert('campaign now is new');</script>";
       }        
//      else
//      {
//            createLbl = "Invalid campaign date";
//            return null;
//      }
      }
    else
      {
        createLbl = "Please fill Start Date.";
        return null;
      }

    
    if (giftSelected.size() == 0)
    {    
    createLbl = "Please select at least one gift.";
    return null;
    }
    
    if (campaignName.compareTo("") == 0 || campaignName == null)
      {
        createLbl = "campaign must have name.";
        return null;
      }
    if (vTValue.compareTo("--Select One--") == 0)
      {
        createLbl = "Please select voucher type for " + campaignName + ".";
        return null;
      }
    if (campaignDescription.length() > 1001)
      {
        createLbl = "Maximum space for description is 1000 character.";
        return null;
      }
    if (UTSelectedArray.size() == 0)
      {
        createLbl = "Please insert at least one user type.";
        return null;
      }
    
    String SQL ="";
//        
//      ResultSet campaignInfoRS = ExecuteQueries.executeQuery(conn,SQL);
//      try
//        {
//          if (campaignInfoRS.next())
//            {
//              giftId = campaignInfoRS.getInt("GIFT_ID");
//              voucherId = campaignInfoRS.getInt("VOUCHER_TYPE_ID");
//
//
//            }
//          campaignInfoRS.getStatement().close();
//          campaignInfoRS.close();
//        }
//      catch (SQLException e)
//        {
//           
//        }
      if (daysToExpire==null || daysToExpire.compareTo ( "" )==0)daysToExpire=jsfUtils.getParamterFromIni ( "DefaultExpireVoucherDays" );
      
      Statement st = conn.createStatement ( );
      
      SQLDeleteUserTypes = "Delete from GVMS_CAMPAIGN_USER where CAMPAIGN_ID=" + campaignId;
      String SQLDeleteGifts = "Delete from GVMS_CAMPAIGN_GIFT where CAMPAIGN_ID=" + campaignId;
       st.executeUpdate(SQLDeleteUserTypes);
       st.executeUpdate(SQLDeleteGifts);

      for (int counter = 0; counter < UTSelectedValue.size(); counter++)
      {
         st.executeUpdate("insert into GVMS_CAMPAIGN_USER(CAMPAIGN_USER_ID,CAMPAIGN_ID,USER_TYPE_ID,CAMPAIGN_USER_CREATION_DATE,CAMPAIGN_USER_CREATEDBY) Values" + 
                                        "(GVMS_CAMPAIGN_USER_SEQ_ID.nextval," + campaignId + 
                                        "," + UTSelectedValue.get(counter) + "," + currDate + 
                                        ",'" + creatorName + "')");
         
        
      }
      
    for (int count = 0; count < giftSelected.size(); count++)
    {

       st.executeUpdate("insert into GVMS_CAMPAIGN_GIFT (CAMPAIGN_ID,GIFT_ID ) Values(" +                                                            
                                      campaignId + "," + 
                                      giftSelected.get(count) +")");
    }
      
      
    if (bridg.compareTo("NEW") == 0)
      {
       if (jsfUtils.haveSpecialCharacter ( campaignName )) 
       {
          createLbl = "Please remove any special character or white spaces from campaign name.";
          return null;
       }
        SQLUpadateCampaign = 
            "update GVMS_CAMPAIGN set CAMPAIGN_START_DATE=" + 
            convertDate(startDate) + 
            " ,VOUCHER_TYPE_ID=(Select voucher_type_id from GVMS_VOUCHER_TYPE t4 where t4.VOUCHER_TYPE_NAME='"+vTValue+"') ,CAMPAIGN_NAME='" + campaignName.trim() + 
            "' ,CAMPAIGN_DESCRIPTION='" + campaignDescription + "' ,CAMPAIGN_LAST_MODIFIED=" + 
            currDate + ",CAMPAIGN_MODIFIED_BY='" + creatorName + "' ,CAMPAIGN_STATUS_ID=" + 
            campaignStatusId + " , VOUCHER_EXPIRE_DAYS='"+daysToExpire+"' where CAMPAIGN_ID=" + campaignId;

        String SQLDeleteVoucher= "Delete from GVMS_CURRENT_VOUCHER where CAMPAIGN_ID=" + campaignId;
        
        if (vTValue.compareTo ( ctc.getVoucherTypeName ( ))!=0)st.executeUpdate(SQLDeleteVoucher);


        


      }
    if (bridg.compareTo("ACTIVE") == 0)
      {
        SQLUpadateCampaign = 
            "update GVMS_CAMPAIGN set CAMPAIGN_DESCRIPTION='" + 
            campaignDescription + "' ,CAMPAIGN_LAST_MODIFIED=" + currDate + 
            ",CAMPAIGN_MODIFIED_BY='" + creatorName + "',CAMPAIGN_STATUS_ID=" + campaignStatusId + 
            " where CAMPAIGN_ID=" + campaignId;
      }


    int update =

       st.executeUpdate(SQLUpadateCampaign);
    if (update == 0)
      {
        createLbl = "Update Fails";

      }
    else
      {
        UpdateClass.updateCampaignInfo(campaignId+"", true);
        UpdateClass.updateCampaignUserInfo(campaignId+"", false);
        createLbl = "Update Complete";

      }
    st.close ( );
    DBConnection.closeConnections ( conn );
    //        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("CreateCampaign_Bean"))
    //          {
    //            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CreateCampaign_Bean");
    //          }
    return null;
  }

  public void setRenderCreate(Boolean renderCreate)
  {
    this.renderCreate = renderCreate;
  }

  public Boolean getRenderCreate()
  {
    return renderCreate;
  }

  public void setRenderUpdate(Boolean renderUpdate)
  {
    this.renderUpdate = renderUpdate;
  }

  public Boolean getRenderUpdate()
  {
    return renderUpdate;
  }

  public void setUTValue(ArrayList<String> UTValue)
  {
    this.UTValue = UTValue;
  }

  public ArrayList<String> getUTValue()
  {
    return UTValue;
  }

  public void setReadOnlyArray(ArrayList<Boolean> readOnlyArray)
  {
    this.readOnlyArray = readOnlyArray;
  }

  public ArrayList<Boolean> getReadOnlyArray()
  {
    return readOnlyArray;
  }

  public void setSessionValue(ArrayList<String> sessionValue)
  {
    this.sessionValue = sessionValue;
  }

  public ArrayList<String> getSessionValue()
  {
    return sessionValue;
  }


  public void setCampaignStatusLbl(HtmlOutputText outputText1)
  {
    this.campaignStatusLbl = outputText1;
  }

  public HtmlOutputText getCampaignStatusLbl()
  {
    return campaignStatusLbl;
  }

  public void setRenderStatus(Boolean renderStatus)
  {
    this.renderStatus = renderStatus;
  }

  public Boolean getRenderStatus()
  {
    return renderStatus;
  }


  public void setCommandButton1(HtmlCommandButton commandButton1)
  {
    this.commandButton1 = commandButton1;
  }

  public HtmlCommandButton getCommandButton1()
  {
    return commandButton1;
  }

  public String back_Action()
  {
    // Add event code here...
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Campaign_Bean");
    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("CreateCampaign_Bean"))
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CreateCampaign_Bean");
      }
    return "backToCampaign";
  }

  public void addAttr()
  {
	  
//    //System.out.println("here in add user type attribute.");
//    
//    for (int i = 0; i < UTValue.size(); i++) {
//    	//System.out.println("UTValue value is "+UTValue.get(i));
//		
//	}
//for (int i = 0; i < UTArray.size(); i++) {
//	//System.out.println("UTArray value is "+UTArray.get(i).getLabel());
//	}
//for (int i = 0; i < UTSelectedArray.size(); i++) {
//	//System.out.println("UTSelectedArray value is "+UTSelectedArray.get(i).getLabel());
//}
	  addAttrFun(UTValue,UTArray,UTSelectedArray);


  }
  
  public void addAttrGift()
  {
    
	  //System.out.println("here in add gift attribute.");
	  addAttrFun(giftValue,giftLabels,giftSelectedLabels);


  }
  
  public void addAttrFun(ArrayList<String> sourceArrayLabel ,ArrayList<SelectItem> sourceArray,ArrayList<SelectItem> destinationArray ){
	  
	  for (int a = 0; a < sourceArrayLabel.size(); a++)
      {
        String temp = sourceArrayLabel.get(a);        
        String tempUtArry = "";
        for (int b = 0; b < sourceArray.size(); b++)
          {
        	String dd = (String) sourceArray.get(b).getValue();
            if (temp.compareTo(dd) == 0)
              {
                tempUtArry = (String) sourceArray.get(b).getValue();
                temp = sourceArray.get(b).getLabel();               
                destinationArray.add(new SelectItem(tempUtArry, temp));
                sourceArray.remove(b);
              }

          }


      }
  } 
  

  public void addAll()
  {
   
	  addAllFun(UTArray,UTSelectedArray);

  }
  
  public void addAllGifts()
  {
   
	  addAllFun(giftLabels,giftSelectedLabels);

  }
  public void addAllFun(ArrayList<SelectItem> sourceArray,ArrayList<SelectItem> destinationArray ){
	  
	  int s = sourceArray.size();
	    for (int a = 0; a < s; a++)
	      {
	        String temp = "";
	        String tempUtArry = "";

	        tempUtArry = (String) sourceArray.get(0).getValue();
	        temp = sourceArray.get(0).getLabel();

	        destinationArray.add(new SelectItem(tempUtArry, temp));
	        sourceArray.remove(0);


	      }
  }
  

  public void removeAll()
  {
	  String sql ="SELECT t1.USER_STATUS_ID,t2.USER_TYPE_NAME from GVMS_USER t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID = t2.USER_TYPE_ID where t1.USER_TYPE_ID = #" + 	                                           
	                                          " and USER_STATUS_ID=1";
	  removeAllFun("USER_TYPE_NAME","users",sql,UTArray,UTSelectedArray);

  }
  public void removeAllGifts()
  {
	  String sql ="";
	  removeAllFun("Gift_NAME","gifts",sql,giftLabels,giftSelectedLabels);

  }

  
  public void removeAllFun(String columnName,String mmessageValue, String sql,ArrayList<SelectItem> sourceArray,ArrayList<SelectItem> destinationArray){
	  
	  int s = destinationArray.size();
	    String errorMsg = "";	    
	    for (int a = 0; a < s; a++)
	      {

	        if (bridg.compareTo("ACTIVE") == 0)
	          {
	        	if (sql.compareTo("")!=0){
	        	sql = sql.replace("#",(String)destinationArray.get(a).getValue() );
	            ResultSet rs = ExecuteQueries.executeQuery(conn,sql);
	            try
	              {
	                if (rs.next())
	                  {
	                    errorMsg += rs.getString(columnName) + ",";
	                    errorMsg = errorMsg.substring(0,(errorMsg.length()-1));

	                    createLbl = "Delete " + errorMsg + " failed due to active "+mmessageValue+".";

	                  }
	                else
	                  {

	                    String temp = "";
	                    String tempUtArry = "";

	                    tempUtArry = (String) destinationArray.get(a).getValue();
	                    temp = destinationArray.get(a).getLabel();

	                    sourceArray.add(new SelectItem(tempUtArry, temp));
	                    destinationArray.remove(a);

	                  }
	                rs.getStatement().close();
	                rs.close();

	              }
	            catch (SQLException e)
	              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
	              }
	          }
	        	else{
	        		String temp = "";
		            String tempUtArry = "";

		            tempUtArry = (String) destinationArray.get(0).getValue();
		            temp = destinationArray.get(0).getLabel();

		            sourceArray.add(new SelectItem(tempUtArry, temp));
		            destinationArray.remove(0);
	        	}

	          }
	        else
	          {
	            String temp = "";
	            String tempUtArry = "";

	            tempUtArry = (String) destinationArray.get(0).getValue();
	            temp = destinationArray.get(0).getLabel();

	            sourceArray.add(new SelectItem(tempUtArry, temp));
	            destinationArray.remove(0);
	          }

	      }
	  
  }
  public void removeAttr()
  {
	  String sql="SELECT t1.USER_STATUS_ID,t2.USER_TYPE_NAME from GVMS_USER t1 left join GVMS_USER_TYPE t2 on t1.USER_TYPE_ID = t2.USER_TYPE_ID where t1.USER_TYPE_ID = " + 
	            		  "# and USER_STATUS_ID=1";
	  removeAttrFun("USER_TYPE_NAME","users",sql,UTSelectedValue,UTSelectedArray,UTArray);
	  
  }
  
  public void removeAttrGift()
  {
	  String sql="";
	  removeAttrFun("GIFT_NAME","gifts",sql,giftSelected,giftSelectedLabels,giftLabels);
	  
  }
  
  
  public void removeAttrFun(String columnName,String mmessageValue, String sql,ArrayList<String> destinationArrayValues,ArrayList<SelectItem> sourceArray,ArrayList<SelectItem> destinationArray){
	  

	    //System.out.println("here in addAttr");
	    ResultSet checkUsers = null;
	    for (int a = 0; a < destinationArrayValues.size(); a++)
	      {
	        if (bridg.compareTo("ACTIVE") == 0)
	          {
	        	if (sql!=null && sql.compareTo("")!=0){
		        	if (sql.contains ( "#" ))sql = sql.replace("#",(String)destinationArray.get(a).getValue() );
		        	
	             checkUsers = 
	              ExecuteQueries.executeQuery(conn,sql);
	            try
	              {
	                if (checkUsers.next())
	                  {
	                    createLbl = 
	                        "Delete " + checkUsers.getString(columnName) + " failed due to active "+mmessageValue+".";

	                  }
	                else
	                  {
	                    createLbl = "";
	                    String temp = destinationArrayValues.get(a);
	                    String tempUtArry = "";
	                    for (int b = 0; b < sourceArray.size(); b++)
	                      {

	                        if (temp.compareTo((String) sourceArray.get(b).getValue()) == 0)
	                          {
	                            tempUtArry = (String) sourceArray.get(b).getValue();
	                            temp = sourceArray.get(b).getLabel();
	                            destinationArray.add(new SelectItem(tempUtArry, temp));
	                            sourceArray.remove(b);
	                          }

	                      }

	                  }
	                checkUsers.getStatement().close();
	                checkUsers.close();
	              }
	            catch (SQLException e)
	              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
	              }

	          }}
	        else
	          {

	            String temp = destinationArrayValues.get(a);
	            String tempUtArry = "";
	            for (int b = 0; b < sourceArray.size(); b++)
	              {

	                if (temp.compareTo((String) sourceArray.get(b).getValue()) == 0)
	                  {
	                    tempUtArry = (String) sourceArray.get(b).getValue();
	                    temp = sourceArray.get(b).getLabel();
	                    destinationArray.add(new SelectItem(tempUtArry, temp));
	                    sourceArray.remove(b);
	                  }

	              }


	          }


	      }
	  
  }


  public void setGraphicImage1(HtmlGraphicImage graphicImage1)
  {
    this.graphicImage1 = graphicImage1;
  }

  public HtmlGraphicImage getGraphicImage1()
  {
    return graphicImage1;
  }


  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }

  public Date getStartDate()
  {
    return startDate;
  }

 

  public void setCampaignName(String campaignName)
  {
    this.campaignName = campaignName;
  }

  public String getCampaignName()
  {
    return campaignName;
  }

  public void setCampaignDescription(String campaignDescription)
  {
    this.campaignDescription = campaignDescription;
  }

  public String getCampaignDescription()
  {
    return campaignDescription;
  }


  public void setDisableStartDate(Boolean disableStartDate)
  {
    this.disableStartDate = disableStartDate;
  }

  public Boolean getDisableStartDate()
  {
    return disableStartDate;
  }

  public void setDisableEndDate(Boolean disableEndDate)
  {
    this.disableEndDate = disableEndDate;
  }

  public Boolean getDisableEndDate()
  {
    return disableEndDate;
  }

  public void setDisableExpiredCampaign(Boolean disableExpiredCampaign)
  {
    this.disableExpiredCampaign = disableExpiredCampaign;
  }

  public Boolean getDisableExpiredCampaign()
  {
    return disableExpiredCampaign;
  }


  public void setUserTypeListSize(Integer userTypeListSize)
  {
    this.userTypeListSize = userTypeListSize;
  }

  public Integer getUserTypeListSize()
  {
    return userTypeListSize;
  }


  public void setUTSelectedValue(ArrayList<String> uTSelectedValue)
  {
    this.UTSelectedValue = uTSelectedValue;
  }

  public ArrayList<String> getUTSelectedValue()
  {
    return UTSelectedValue;
  }

  public void setUTSelectedArray(ArrayList<SelectItem> uTSelectedArray)
  {
    this.UTSelectedArray = uTSelectedArray;
  }

  public ArrayList<SelectItem> getUTSelectedArray()
  {
    return UTSelectedArray;
  }

  public void setCreateLbl(String createLbl)
  {
    this.createLbl = createLbl;
  }

  public String getCreateLbl()
  {
    return createLbl;
  }

  public void setJavaScriptAlert(String javaScriptAlert)
  {
    this.javaScriptAlert = javaScriptAlert;
  }

  public String getJavaScriptAlert()
  {
    return javaScriptAlert;
  }

public void setJavaScriptConfirmDeleteVouchers(
		String javaScriptConfirmDeleteVouchers) {
	this.javaScriptConfirmDeleteVouchers = javaScriptConfirmDeleteVouchers;
}

public String getJavaScriptConfirmDeleteVouchers() {
	return javaScriptConfirmDeleteVouchers;
}
public void setGiftValue(ArrayList<String> giftValue) {
	this.giftValue = giftValue;
}

public ArrayList<String> getGiftSelected() {
	return giftSelected;
}

public void setGiftSelected(ArrayList<String> giftSelected) {
	this.giftSelected = giftSelected;
}

public ArrayList<SelectItem> getGiftLabels() {
	return giftLabels;
}

public void setGiftLabels(ArrayList<SelectItem> giftLabels) {
	this.giftLabels = giftLabels;
}

public ArrayList<SelectItem> getGiftSelectedLabels() {
	return giftSelectedLabels;
}

public void setGiftSelectedLabels(ArrayList<SelectItem> giftSelectedLabels) {
	this.giftSelectedLabels = giftSelectedLabels;
}
public ArrayList<String> getGiftValue() {
	return giftValue;
}

public void setJavaScript(String javaScript) {
	this.javaScript = javaScript;
}

public String getJavaScript() {
	return javaScript;
}

public void setDaysToExpire(String daysToExpire)
{
   this.daysToExpire = daysToExpire;
}

public String getDaysToExpire()
{
   return daysToExpire;
}
}
