package com.mobinil.gvms.system.admin.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mobinil.gvms.system.admin.campaign.DTO.*;
import com.mobinil.gvms.system.admin.campaign.DAO.*;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.admin.campaign.Engine.*;

import com.mobinil.gvms.system.utility.DTOComparator;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletResponse;

import oracle.jdbc.OracleResultSet;

import org.apache.myfaces.custom.calendar.HtmlInputCalendar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Campaign
{
	
  public ArrayList<campaignTableClass> currCampaign = 
    new ArrayList<campaignTableClass>();

  public HtmlDataTable dataTable1;
  public campaignTableClass dataItem;
  public HtmlCommandButton createCampaign;
  public UIComponent inputCalendar1;
  public UIComponent inputDate1;
  public HtmlInputCalendar inputCalendar2;
  public String msgWarn;
  public Integer rowCount = 10;
  public Integer rowSerial;
  int pageIndex = 0;
  Integer campaignId;
  static final String newColor = "blue";
  static final String activeColor = "green";  
  static final String closedColor = "gray";
  public static final int newCampaign = 1;
  public static final int activeCampaign = 2;  
  public static final int closedCampaign = 5;
  public List<campaignTableClass> selectedDataList;
  public String sortField = null;
  public boolean sortAscending = true;
  public String inputString;
  public String inputFromInt;
  public String inputToInt;
  public Date fromDate;
  public Date toDate;
  public String divValue;
  public RunnableClass t;

  public Campaign()
  {
    new leftMenu().checkUserInSession();

    
  

      try
      {
         Connection conn = DBConnection.getConnection();
        currCampaign = campaignDAO.getCampaignsInfos(conn);
        rowSerial = currCampaign.size() + 1;   
        DBConnection.closeConnections ( conn );
      }
      catch (SQLException e)
      {    	  
    	  new com.mobinil.gvms.system.utility.PrintException().printException(e);
    	  jsfUtils.redirectErrorPage();    	  
      }
      
    


  }

  public void downloadErrorFile()
  {
    
    FacesContext fc = FacesContext.getCurrentInstance();
    dataItem = (campaignTableClass) dataTable1.getRowData();
    HttpServletResponse response = 
      (HttpServletResponse) fc.getExternalContext().getResponse();
    String filename = dataItem.getCampaignName();
    Blob errorFile = null;
    String filePath = "";

    DateTime dt = new DateTime();
    DateTimeFormatter fmt = 
      DateTimeFormat.forPattern("yyyy-MM-dd-mm-ss");
    filename = filename + "-" + dt.toString(fmt) + ".csv";
    
    filePath = jsfUtils.getParamterFromIni("campaignErrorFile");
    
    


    try
    {
       Connection conn = DBConnection.getConnection();
      errorFile = 
          campaignDAO.getCampaigntErrorFile(conn,dataItem.getCampaignId());
      
      InputStream csvInput = errorFile.getBinaryStream();
      FileUtil.write(new File(filePath + filename), csvInput);
      DBConnection.closeConnections ( conn );
    }
    catch (Exception e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      //System.out.println(e.toString());
    }

    //System.out.println("fileName is " + filename);
    File csvFile = new File(filePath, filename);
    try
    {
      HttpServletUtil.downloadFile(response, csvFile, true);
    }
    catch (IOException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      //System.out.println(e.toString());
    }
    fc.responseComplete();

  }

  public void filter_Action() throws SQLException
  {
     Connection conn = DBConnection.getConnection();
    currCampaign = 
        campaignDAO.filter_Action(conn,divValue, currCampaign, inputString, 
                                  toDate, fromDate);
    DBConnection.closeConnections ( conn );
  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
    {
      sortAscending = !sortAscending;
    }
    else
    {
      sortField = sortFieldAttribute;
      sortAscending = true;
    }

    // Sort results.
    if (sortField != null)
    {
      Collections.sort(currCampaign, 
                       new DTOComparator(sortField, sortAscending));
    }


  }

  public String getSelectedItems()
  {
    
    // Get selected items.
    selectedDataList = new ArrayList<campaignTableClass>();


    for (int counter = (dataTable1.getRows() * pageIndex); 
         counter < currCampaign.size(); counter++)


    {
      if (counter == (dataTable1.getRows() * (pageIndex + 1)))
      {
        break;
      }

      campaignTableClass dataItem = new campaignTableClass();
      dataItem = currCampaign.get(counter);
      if (dataItem.getSelected())
      {
        selectedDataList.add(dataItem);
        dataItem.setSelected(false); // Reset.
        int id = dataItem.getCampaignId();
        try
        {
          int delete = campaignDAO.deleteCampaign(id);
          t = new RunnableClass(id, 4);
          t.start();
          if (delete == 1)
          {
           UpdateClass.updateCampaignInfo(id+"", true);
            msgWarn = "Campaign " + id + "deleted";
          }
          else
          {
            msgWarn = "Campaign not deleted";
          }
        }
        catch (SQLException e)
        {
          new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
          
        
      }


    }
    jsfUtils.putInSession("GoToCampaignConstructor"," ");
    msgWarn = "Campaign deleted";        
    return null; // Navigation case.
  }

  public void removeCampaign()
  {
   
      jsfUtils.removeFromSession("Campaign_Bean");      
      jsfUtils.removeFromSession("AdditionalField_Bean");

   

   

  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }


  public String upload_voucherList()
  {
      
    refactorCampaignInSession();
      removeCampaign();
    return "UploadVoucherList";
  }
  public void refactorCampaignInSession(){
  
    dataItem = (campaignTableClass) dataTable1.getRowData();
    jsfUtils.removeFromSession("campaignInfo");
    jsfUtils.removeFromSession("CreateCampaign_Bean");    
    jsfUtils.putInSession(dataItem,"campaignInfo");
  }
  public String Edit_cmd()
  {
      dataItem = (campaignTableClass) dataTable1.getRowData();
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("campaignStatus", 
                                                                                 dataItem.getCampaignStatus());
      refactorCampaignInSession();
      removeCampaign();
      return "newCampaign";

//    return "newCampaign";
  }
  
    public String getVoucherFile()
    {    
    refactorCampaignInSession();
      return "vouchers";
    }

  public String addField_cmd()
  {
  
    
    refactorCampaignInSession();
      removeCampaign();

    return "AdditionalField";
  }
  public String closeCampaign()
  {
  
    
    refactorCampaignInSession();
      removeCampaign();
      dataItem = (campaignTableClass) dataTable1.getRowData();
      try {
		int changeCamToClose = campaignDAO.closeCampaign(dataItem.getCampaignId());
		if (changeCamToClose<1)msgWarn = "The system can not close this campaign "+dataItem.getCampaignName();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		jsfUtils.redirectErrorPage();
		new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}
      

    return "";
  }
  public String viewUser_Action()
  {
    refactorCampaignInSession();
      removeCampaign();
    return "viewUsers";
  }

  public String newCampaign()
  {
    // Add event code here...
     FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("campaignStatus", 
                                                                                "Create Campaign");


    return "newCampaign";
  }
  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  

 

  public void setInputCalendar1(UIComponent inputCalendar1)
  {
    this.inputCalendar1 = inputCalendar1;
  }

  public UIComponent getInputCalendar1()
  {
    return inputCalendar1;
  }

  public void setInputDate1(UIComponent inputDate1)
  {
    this.inputDate1 = inputDate1;
  }

  public UIComponent getInputDate1()
  {
    return inputDate1;
  }

  public void setInputCalendar2(HtmlInputCalendar inputCalendar2)
  {
    this.inputCalendar2 = inputCalendar2;
  }

  public HtmlInputCalendar getInputCalendar2()
  {
    return inputCalendar2;
  }

 

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }


  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }


  public void setInputString(String inputString)
  {
    this.inputString = inputString;
  }

  public String getInputString()
  {
    return inputString;
  }

  public void setInputFromInt(String inputFromInt)
  {
    this.inputFromInt = inputFromInt;
  }

  public String getInputFromInt()
  {
    return inputFromInt;
  }

  public void setInputToInt(String inputToInt)
  {
    this.inputToInt = inputToInt;
  }

  public String getInputToInt()
  {
    return inputToInt;
  }

  public void setFromDate(Date fromDate)
  {
    this.fromDate = fromDate;
  }

  public Date getFromDate()
  {
    return fromDate;
  }

  public void setToDate(Date toDate)
  {
    this.toDate = toDate;
  }

  public Date getToDate()
  {
    return toDate;
  }

  public void setDivValue(String divValue)
  {
    this.divValue = divValue;
  }

  public String getDivValue()
  {
    return divValue;
  }


  public void setRowSerial(Integer rowSerial)
  {
    this.rowSerial = rowSerial;
  }

  public Integer getRowSerial()
  {
    return rowSerial;
  }

  public void setCurrCampaign(ArrayList<campaignTableClass> currCampaign)
  {
    this.currCampaign = currCampaign;
  }

  public ArrayList<campaignTableClass> getCurrCampaign()
  {
    return currCampaign;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setDataItem(campaignTableClass dataItem)
  {
    this.dataItem = dataItem;
  }

  public campaignTableClass getDataItem()
  {
    return dataItem;
  }

  public void setCreateCampaign(HtmlCommandButton createCampaign)
  {
    this.createCampaign = createCampaign;
  }

  public HtmlCommandButton getCreateCampaign()
  {
    return createCampaign;
  }

  public void setSelectedDataList(List<campaignTableClass> selectedDataList)
  {
    this.selectedDataList = selectedDataList;
  }

  public List<campaignTableClass> getSelectedDataList()
  {
    return selectedDataList;
  }
}
