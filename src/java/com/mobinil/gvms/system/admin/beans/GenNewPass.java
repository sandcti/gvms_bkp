package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDAO.LoginDAO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.io.UnsupportedEncodingException;

import java.security.NoSuchAlgorithmException;

import java.util.ArrayList;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class GenNewPass
{
  public HtmlForm form1;
  public HtmlCommandButton commandButton1;
  public HtmlInputText inputText1;
  public HtmlInputSecret passwordId;
  public HtmlInputSecret confirmPasswordId;
  public HtmlOutputLabel outputLabel1;
  public HtmlCommandButton commandButton2;
  public String password;
  public String confirmPassword;
  public String msgWarn;
  String path;

  public GenNewPass()
  {
    
    Boolean admin = 
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("adminInfo");
    Boolean user = 
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("userInfo");
    Boolean userTypeGenerate = null;
    Boolean adminTypeGenerate = null;

    try
    {
      adminTypeGenerate = 
          (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("adminTypeGenerate");
      userTypeGenerate = 
          (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeGenerate");
    }
    catch (Exception e)
    {
      new com.mobinil.gvms.system.utility.PrintException().printException(e);
    }


    if (adminTypeGenerate==null)

    {
      if (!admin)
      {
        try
        {
          FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/AdminLogin.jsp");
        }
        catch (Exception e)
        {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }


      }

    }

    if ( userTypeGenerate==null)

    {
      if (!user)
      {
        try
        {
          FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Login.jsp");
        }
        catch (Exception e)
        {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }


      }

    }


  }


  public String genNewPassAction()
  {
    
    LoginDTO adminInfo = null;
    com.mobinil.gvms.system.user.DTO.LoginDTO userInfo = null;
    Boolean userTypeGenerate = null;
    Boolean adminTypeGenerate = null;
String userName="";
String systemUserId="";
    adminTypeGenerate = 
        (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("adminTypeGenerate");
    userTypeGenerate = 
        (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeGenerate");

    if (adminTypeGenerate)
    {
    	adminInfo = 
          (LoginDTO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("adminInfo");
    }
    if (userTypeGenerate)
    {
      userInfo = 
          (com.mobinil.gvms.system.user.DTO.LoginDTO)((ArrayList) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userInfo")).get(0);
          
    }
    
    //System.out.println(userInfo);
    HttpServletRequest req = 
      (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    path = req.getRequestURI();
    if (password.compareTo("") == 0 || confirmPassword.compareTo("") == 0)
    {
      msgWarn = 
          "The new password and the confirmation password does not have value. Please type value in both boxes.";
      return null;
    }

    if (password.compareTo(confirmPassword) == 0)
    {
    	 if (adminTypeGenerate)
    	    {
    		 userName = adminInfo.getUserName();
    		 systemUserId = adminInfo.getSysUserId()+"";
    	    }
    	 if (userTypeGenerate)
    	    {
    		 userName = userInfo.getUserName();
    		 systemUserId = userInfo.getSysUserId()+"";
    	    }
    	 try
         {
      String x = "";
        String ecriptedPass = MD5Class.MD5(password);
          x = SecurityUtils.checkPass(password,ecriptedPass , userName,systemUserId);
        
        
      if (x.compareTo("") != 0)
      {
        msgWarn = x;
        return null;
      }
      else 
    	  {
    	  LoginDAO.updateUserLastPass(systemUserId, ecriptedPass);
    	  LoginDAO.setNewPassword(systemUserId, ecriptedPass);
    	  
    	  }
         }
         catch (Exception e)
         {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
         }

//      String y = 
//        SecurityUtils.checkPasswordInDB(password, userInfo.getSysUserId());
//      if (y.compareTo("") != 0)
//      {
//        msgWarn = y;
//        return null;
//      }      
         msgWarn = 
             "The new password has been updated to your account.";
    }
    else
    {
      msgWarn = 
          "The new password and the confirmation password don't match. Please type the same password in both boxes.";
      return null;
    }
    return null;

  }

  public String backAction()
  {    
    Boolean userTypeGenerate = null;
    Boolean adminTypeGenerate = null;

    adminTypeGenerate = 
        (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("adminTypeGenerate");
    userTypeGenerate = 
        (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeGenerate");


    if (adminTypeGenerate)

    {
      
        try
        {
          FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/AdminLogin.jsp");
        }
        catch (Exception e)
        {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }

    }

    if (userTypeGenerate)

    {
        String lang = (String)jsfUtils.getFromSession("userTypeInterface");
        if (lang.compareTo("Ar")==0){
        return "logout_ar";
        }
        else
        {
        return "logout_en";
        }


      

    }
return null;

  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setCommandButton1(HtmlCommandButton commandButton1)
  {
    this.commandButton1 = commandButton1;
  }

  public HtmlCommandButton getCommandButton1()
  {
    return commandButton1;
  }

  public void setInputText1(HtmlInputText inputText1)
  {
    this.inputText1 = inputText1;
  }

  public HtmlInputText getInputText1()
  {
    return inputText1;
  }

  public void setPasswordId(HtmlInputSecret inputSecret1)
  {
    this.passwordId = inputSecret1;
  }

  public HtmlInputSecret getPasswordId()
  {
    return passwordId;
  }

  public void setConfirmPasswordId(HtmlInputSecret inputSecret2)
  {
    this.confirmPasswordId = inputSecret2;
  }

  public HtmlInputSecret getConfirmPasswordId()
  {
    return confirmPasswordId;
  }

  public void setOutputLabel1(HtmlOutputLabel outputLabel1)
  {
    this.outputLabel1 = outputLabel1;
  }

  public HtmlOutputLabel getOutputLabel1()
  {
    return outputLabel1;
  }

  public void setCommandButton2(HtmlCommandButton commandButton2)
  {
    this.commandButton2 = commandButton2;
  }

  public HtmlCommandButton getCommandButton2()
  {
    return commandButton2;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setConfirmPassword(String confirmPassword)
  {
    this.confirmPassword = confirmPassword;
  }


  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public String getConfirmPassword()
  {
    return confirmPassword;
  }
}
