package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.giftType.DAO.GiftTypeDAO;
import com.mobinil.gvms.system.admin.giftType.DTO.GiftTypeDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpSession;


public class GiftType
{
  public ArrayList<GiftTypeDTO> GiftTypeArray = new ArrayList<GiftTypeDTO>();
  GiftTypeDTO dataItem;
  
  public HtmlForm form1;
  public HtmlDataTable dataTable1;
  public String msgWarn;
  public Integer rowCount = 10;
  int pageIndex = 0;

  public List<GiftTypeDTO> selectedDataList;
  public String sortField = null;
  public boolean sortAscending = true;

  public GiftType()
  {
      new leftMenu().checkUserInSession();
      
    
    try
      {
         GiftTypeArray =  GiftTypeDAO.getGiftTypeInfo();
        for (int k =0 ;k<GiftTypeArray.size() ; k++)          
          {
            
            GiftTypeDTO g = GiftTypeArray.get(k);
              int giftId = g.getGiftTypeId();
            

            try
              {
                Integer id = 
                  (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("rowNumber" + 
                                                                                                       giftId);
                //System.out.println("id is" + id);

                if (id == giftId)
                  {

                    g.setSelected(true);
                    msgWarn = "Delete operation failed due to active gifts.";
                  }
              }
            catch (Exception e)
              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }
            


          }

        
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    for (int counter1 = 0; counter1 < GiftTypeArray.size(); counter1++)
      {
        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        HttpSession session = 
          (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        String[] beanNames = session.getValueNames();
        for (int x = 0; x < beanNames.length; x++)
          {
            if (beanNames[x].contains("rowNumber"))
              {

                if (sessionMap.containsKey(beanNames[x]))
                  {
                    sessionMap.remove(beanNames[x]);
                  }

              }
          }

      }
  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(GiftTypeArray, new DTOComparator(sortField, sortAscending));
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public String getSelectedItems()
  {
    String gifts = "";
    // Get selected items.
    selectedDataList = new ArrayList<GiftTypeDTO>();
    int count = 1;

    for (int counter = (dataTable1.getRows() * pageIndex); counter < GiftTypeArray.size(); 
         counter++)


      {
        if (counter == (dataTable1.getRows() * (pageIndex + 1)))
          {
            break;
          }
        GiftTypeDTO dataItems = new GiftTypeDTO();
        dataItems = GiftTypeArray.get(counter);
        if (dataItems.getSelected())
          {
            selectedDataList.add(dataItems);
            dataItems.setSelected(false); // Reset.
            int id = dataItems.getGiftTypeId();
            String activeGifts="";
                  try {
                      activeGifts = GiftTypeDAO.getActiveGiftTypes(id);
                  
                if (activeGifts.compareTo("") == 0)
                  {
                    int delete = GiftTypeDAO.deleteGiftType(id);
                      
                    if (delete == 1)
                      {
                        UpdateClass.updateGiftTypeInfo(id+"", true);
                        msgWarn = "Gift type " + id + "deleted";
                      }
                    else
                      {
                        msgWarn = "Gift type not deleted";
                      }
                      
                  }
                else
                  {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("rowNumber" + 
                                                                                               dataItems.getGiftTypeId(), 
                                                                                               dataItems.getGiftTypeId());
                    //System.out.println("rowNumber" + counter + " is " + dataItems.getGiftTypeId());
                  }
                  }
                  catch (SQLException e) {
                      new com.mobinil.gvms.system.utility.PrintException().printException(e);
                  }
              


          }
        count++;

      }
    msgWarn = "Gift type deleted";
    // Do your thing with the MyData items in List selectedDataList.
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("GiftType_Bean");
    return "refreshGiftType"; // Navigation case.
  }

  public String newGiftType()
  {

    return "createGiftType";
  }

  public String editGiftType()
  {

    dataItem = (GiftTypeDTO) dataTable1.getRowData();    
    jsfUtils.removeFromSession("GiftTypeInfo");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("GiftTypeInfo", 
                                                                               dataItem);
    


    return "editGiftType";
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setGiftTypeArray(ArrayList<GiftTypeDTO> GiftTypeArray)
  {
    this.GiftTypeArray = GiftTypeArray;
  }

  public ArrayList<GiftTypeDTO> getGiftTypeArray()
  {
    return GiftTypeArray;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }
}
