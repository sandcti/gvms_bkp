package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.gift.DTO.GiftDTO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;


public class EditGift
{
  public HtmlForm form1;
  Gift unreed = 
    (Gift) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Gift_Bean");
  public String giftName;
  
  public String giftDescription;
  public Integer giftValue=0;
  Integer giftId;
  public HtmlInputText nameInputText;
  public HtmlInputText nameArabicInputText;
  public HtmlInputText valueInputText;  
  public String nameAr;  
  public String status;  
  public HtmlCommandButton updateButton;
  public HtmlInputTextarea descriptionInputTextarea;
  public HtmlOutputLabel msgWarn;
  public HtmlCommandButton backToGift;
    GiftDTO gdto = null;

  public EditGift()
  {
      new leftMenu().checkUserInSession();
      gdto = (GiftDTO)jsfUtils.getFromSession("giftInfo");
      
    giftId = gdto.getGiftId();        
    giftName = gdto.getGiftName();
    nameAr = gdto.getGiftNameAr();
    giftValue = gdto.getGiftValue();
    giftDescription = gdto.getGiftDescription();
    status = gdto.getStatusId();
        
  }

  public String updateGift_Action() throws SQLException
  { 
	  giftName = giftName.trim();
	  if (giftDescription.length() > 1000)
      {
        msgWarn.setValue("Maximum characters for description are 1000.");

        return null;
      }
   
   //System.out.println("giftName is "+giftName);
   //System.out.println("nameAr is "+nameAr);
   //System.out.println("giftValue is "+giftValue);
   //System.out.println("status is "+status);
    if (giftName==null || nameAr==null ||giftValue==null || 
    		giftName.compareTo("") == 0 || 
        nameAr.compareTo("") == 0||
         status.compareTo("0") == 0)
      {

        msgWarn.setValue("Please fill star boxes.");
        return null;
      }
    Connection conn = DBConnection.getConnection();
    ResultSet repeatGift = 
      ExecuteQueries.executeQuery(conn,"Select GIFT_NAME from GVMS_GIFT where GIFT_NAME='" + 
    		  						giftName+ "' and GIFT_ID!=" + 
                                  giftId);
    try
      {

        if (repeatGift.next())
          {
            msgWarn.setValue("this gift already exists.");
            DBConnection.closeConnections ( conn );
            return null;
          }
        else
          {
              

            repeatGift.getStatement().close();
            repeatGift.close();
            String creatorName = 
                ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
            String sql="update GVMS_GIFT set GIFT_NAME='" + giftName + 
            "',GIFT_STATUS_ID=" + status + ",GIFT_VALUE=" + 
            valueInputText.getValue().toString() + 
            ",GIFT_NAME_ARABIC=?,GIFT_DESCRIPTON='" + 
            descriptionInputTextarea.getValue().toString() + 
            "',GIFT_MODIFIED_BY='" + creatorName + 
            "',GIFT_MODIFIED_DATE=" + 
            new DateClass().convertDate(new DateClass().getDateTime()) + 
            " where GIFT_ID =" + giftId;
            
            boolean update = 
              ExecuteQueries.executeNoneQueryPreparedOracle(conn,sql,((String) nameArabicInputText.getValue()).trim());
            if (update)
              {
                msgWarn.setValue("Update Fails");
                DBConnection.closeConnections ( conn );
                return null;
              }
            else
              {
                UpdateClass.updateGiftInfo(giftId+"", true);
                msgWarn.setValue("Update Complete");
                DBConnection.closeConnections ( conn );
                return null;
              }

          }
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );
    return null;

  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setGiftName(String giftName)
  {
    this.giftName = giftName;
  }

  public String getGiftName()
  {
    return giftName;
  }

  public void setGiftDescription(String giftDescription)
  {
    this.giftDescription = giftDescription;
  }

  public String getGiftDescription()
  {
    return giftDescription;
  }

  public void setGiftValue(Integer giftValue)
  {
    this.giftValue = giftValue;
  }

  public Integer getGiftValue()
  {
    return giftValue;
  }

  public void setNameInputText(HtmlInputText inputText1)
  {
    this.nameInputText = inputText1;
  }

  public HtmlInputText getNameInputText()
  {
    return nameInputText;
  }

  public void setValueInputText(HtmlInputText inputText2)
  {
    this.valueInputText = inputText2;
  }

  public HtmlInputText getValueInputText()
  {
    return valueInputText;
  }


  public void setUpdateButton(HtmlCommandButton commandButton1)
  {
    this.updateButton = commandButton1;
  }

  public HtmlCommandButton getUpdateButton()
  {
    return updateButton;
  }


  public void setDescriptionInputTextarea(HtmlInputTextarea inputTextarea1)
  {
    this.descriptionInputTextarea = inputTextarea1;
  }

  public HtmlInputTextarea getDescriptionInputTextarea()
  {
    return descriptionInputTextarea;
  }

  public void setMsgWarn(HtmlOutputLabel outputLabel1)
  {
    this.msgWarn = outputLabel1;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }


  

  public void setBackToGift(HtmlCommandButton commandButton1)
  {
    this.backToGift = commandButton1;
  }

  public HtmlCommandButton getBackToGift()
  {
    return backToGift;
  }

  public String back_Action()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Gift_Bean");
    return "backToGift";
  }

  public void setNameArabicInputText(HtmlInputText nameArabicInputText)
  {
    this.nameArabicInputText = nameArabicInputText;
  }

  public HtmlInputText getNameArabicInputText()
  {
    return nameArabicInputText;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }

  public void setNameAr(String nameAr)
  {
    this.nameAr = nameAr;
  }

  public String getNameAr()
  {
    return nameAr;
  }
}
