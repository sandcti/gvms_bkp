package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.giftType.DAO.GiftTypeDAO;
import com.mobinil.gvms.system.admin.giftType.DTO.GiftTypeDTO;
import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;


public class EditGiftType
{
  public HtmlForm form1;  
  public String GiftTypeName;
  public String GiftTypeDescription;
  public Integer GiftTypeValue;
  public String status;
  Integer GiftTypeId;
  public HtmlInputText nameInputText;
  public String name;
  public String value;
  public String description;
  public HtmlCommandButton updateButton;
  public HtmlInputTextarea descriptionInputTextarea;
  public HtmlOutputLabel msgWarn;
  public HtmlCommandButton backToGiftType;

    GiftTypeDTO gtdto = null;
  public EditGiftType()
  {
        new leftMenu().checkUserInSession();
        gtdto = (GiftTypeDTO)jsfUtils.getFromSession("GiftTypeInfo");
        
    GiftTypeId = gtdto.getGiftTypeId();
        
    GiftTypeName = gtdto.getGiftTypeName();
        
    name = GiftTypeName;

    GiftTypeDescription = gtdto.getGiftTypeDescription();
        
    description = GiftTypeDescription;
    status = gtdto.getGiftTypeStatusId()+"";        
  }

  public String updateGiftType_Action()
  {
    // Add event code here...
	  if (description.length() > 1000)
      {
        msgWarn.setValue("Maximum characters for description are 1000.");

        return null;
      }   
	  GiftTypeName = nameInputText.getValue().toString().trim();
	  
    if (GiftTypeName==null || GiftTypeName.compareTo("") == 0 ||
         status.compareTo("0") == 0)
      {

        msgWarn.setValue("Please fill star boxes.");
        return null;
      }
    
    try
      {
          String dd= GiftTypeDAO.checkDublicatedGiftTypeName(nameInputText.getValue().toString().trim(),GiftTypeId);
        if (dd.compareTo("")!=0)
          {
            msgWarn.setValue("this gift type already exists.");
            return null;
          }
        else
          {
            
            String creatorName = 
              ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
            if (status.compareTo("0") != 0)
              {
                int update =
                  GiftTypeDAO.updateGiftType(GiftTypeName, status,creatorName,descriptionInputTextarea.getValue().toString(),GiftTypeId);
                  
                if (update == 0)
                  {
                    msgWarn.setValue("Gift type name is missing");
                    return null;
                  }
                else
                  {
                    msgWarn.setValue("Update Complete");
                    return null;
                  }
              }
            else
              {
                msgWarn.setValue("Please select status for " + name);
                return null;
              }
          }
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }

  public String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }

  public String convertDate(String values)
  {
    if (values == null)
      {
        ;
        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY')";
        return values;
      }
    else
      {
        String[] toDate = values.split("/");

        values = 
            "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY HH24:MI:SS')";
        //System.out.println(values);
        return values;
      }
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setGiftTypeName(String GiftTypeName)
  {
    this.GiftTypeName = GiftTypeName;
  }

  public String getGiftTypeName()
  {
    return GiftTypeName;
  }

  public void setGiftTypeDescription(String GiftTypeDescription)
  {
    this.GiftTypeDescription = GiftTypeDescription;
  }

  public String getGiftTypeDescription()
  {
    return GiftTypeDescription;
  }

  public void setGiftTypeValue(Integer GiftTypeValue)
  {
    this.GiftTypeValue = GiftTypeValue;
  }

  public Integer getGiftTypeValue()
  {
    return GiftTypeValue;
  }

  public void setNameInputText(HtmlInputText inputText1)
  {
    this.nameInputText = inputText1;
  }

  public HtmlInputText getNameInputText()
  {
    return nameInputText;
  }


  public void setUpdateButton(HtmlCommandButton commandButton1)
  {
    this.updateButton = commandButton1;
  }

  public HtmlCommandButton getUpdateButton()
  {
    return updateButton;
  }


  public void setDescriptionInputTextarea(HtmlInputTextarea inputTextarea1)
  {
    this.descriptionInputTextarea = inputTextarea1;
  }

  public HtmlInputTextarea getDescriptionInputTextarea()
  {
    return descriptionInputTextarea;
  }

  public void setMsgWarn(HtmlOutputLabel outputLabel1)
  {
    this.msgWarn = outputLabel1;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }


  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public String getValue()
  {
    return value;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setBackToGiftType(HtmlCommandButton commandButton1)
  {
    this.backToGiftType = commandButton1;
  }

  public HtmlCommandButton getBackToGiftType()
  {
    return backToGiftType;
  }

  public String back_Action()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("GiftType_Bean");
    return "backtoGiftType";
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }
}
