package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.gift.DAO.GiftDAO;
import com.mobinil.gvms.system.admin.gift.DTO.GiftDTO;

import com.mobinil.gvms.system.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;


public class Gift
{
  public ArrayList<GiftDTO> giftArray = new ArrayList<GiftDTO>();
  GiftDTO dataItem;
  
  public HtmlForm form1;
  public HtmlDataTable dataTable1;
  public List<GiftDTO> selectedDataList;
  public String msgWarn;
  public Integer rowCount = 10;
  int pageIndex = 0;
  public String sortField = null;
  public boolean sortAscending = true;

  public Gift()
  {
        new leftMenu().checkUserInSession();
    
     try {
         giftArray= GiftDAO.getGeftInfos();
     }
     catch (SQLException e) {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
     }
  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(giftArray, new DTOComparator(sortField, sortAscending));
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public String getSelectedItems()
  {
	  String campaignNames = "";
	  String giftNamesDeleted="";
	  String giftNamesDidnotDeleted="";
	  
	  String temp = "";
    // Get selected items.
    selectedDataList = new ArrayList<GiftDTO>();
    int count = 1;
    int selected = 0;

    for (int counter = (dataTable1.getRows() * pageIndex); counter < giftArray.size(); counter++)


      {
        if (counter == (dataTable1.getRows() * (pageIndex + 1)))
          {
            break;
          }
        GiftDTO dataItems = new GiftDTO();
        dataItems = giftArray.get(counter);
        if (dataItems.getSelected())
          {
        	selected ++;
            selectedDataList.add(dataItems);
            dataItems.setSelected(false); // Reset.
            int id = dataItems.getGiftId();
            
			try {
				temp = GiftDAO.deleteGift(id);
				if (temp.compareTo("")==0)giftNamesDeleted+=dataItems.getGiftName()+",";				
				else {
					giftNamesDidnotDeleted+=dataItems.getGiftName()+",";
					campaignNames += temp+",";
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				new com.mobinil.gvms.system.utility.PrintException().printException(e);
			}
              
           
          }
        count++;

      }
    if (campaignNames.compareTo("")!=0){
    	campaignNames = campaignNames.substring(0, (campaignNames.length()-1));
    	giftNamesDidnotDeleted = giftNamesDidnotDeleted.substring(0, (giftNamesDidnotDeleted.length()-1));
    	msgWarn = "The system could not delete "+ giftNamesDidnotDeleted+" due to active campaign(s) "+campaignNames+".";
    }
    	
    if (giftNamesDeleted.compareTo("")!=0)
    	{
    	giftNamesDeleted = giftNamesDeleted.substring(0, (giftNamesDeleted.length()-1));
    	if(msgWarn==null)msgWarn="";
    	msgWarn += "The selected gifts have been deleted ("+ giftNamesDeleted+").";
    	}
    
    
    if (selected==0)msgWarn ="Please select at least one gift.";
    
    // Do your thing with the MyData items in List selectedDataList.
//    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Gift_Bean");
    return null; // Navigation case.
  }

  public String newGift()
  {

    return "createGift";
  }

  public String editGift()
  {

    dataItem = (GiftDTO) dataTable1.getRowData();    
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("giftInfo", 
                                                                               dataItem);

    return "editGift";
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setGiftArray(ArrayList<GiftDTO> giftArray)
  {
    this.giftArray = giftArray;
  }

  public ArrayList<GiftDTO> getGiftArray()
  {
    return giftArray;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setSelectedDataList(List<GiftDTO> selectedDataList)
  {
    this.selectedDataList = selectedDataList;
  }

  public List<GiftDTO> getSelectedDataList()
  {
    return selectedDataList;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }
}

