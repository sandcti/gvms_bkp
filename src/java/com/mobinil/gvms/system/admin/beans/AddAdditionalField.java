package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.campaign.DAO.additionalFieldDAO;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.jsfUtils;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import java.util.ArrayList;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;


public class AddAdditionalField
{
	
  public HtmlForm form1;


  public HtmlInputText arabicInput;
  public String englishInput;

  public ArrayList<SelectItem> fieldControlArray = new ArrayList<SelectItem>();
  public String mandatory;
  public String unique;
  public String type;
  public String fieldStatus;
  public String fieldControl;
  public HtmlSelectOneMenu selectOneMenu4;
  public UISelectItems selectItems1;
  public HtmlOutputText msgWarn;
//  public String orderValue;
  public HtmlCommandButton addId;
  campaignTableClass ctc = null;

  public AddAdditionalField()
  {

	  new leftMenu().checkUserInSession();
	fieldControlArray.add(new SelectItem("One Line"));  
    fieldControlArray.add(new SelectItem("List"));    
    fieldControlArray.add(new SelectItem("Multi-Line"));
    ctc = (campaignTableClass)jsfUtils.getFromSession("campaignInfo");
     
      
    


  }

  public String back_Action()
  {
jsfUtils.removeFromSession("AdditionalField_Bean");
    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/AdditionalField.jsp");
      }
    catch (IOException e)
      {
        new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }

  public String addField_Action()
  {    
   
    int unqiueValue = 0;
    int mandatoryValue = 0;
    String control = new String();
    //System.out.println("in addField_Action start");
    //    agreeArray.indexOf(unique);
    Integer campaignId =ctc.getCampaignId();
      
      String englishFieldName = englishInput;
      String arabicFieldName = (String)arabicInput.getValue();
      if (englishFieldName.compareTo("")==0 || englishFieldName==null ||arabicFieldName.compareTo("")==0 || arabicFieldName==null )
      {
      msgWarn.setValue("Please insert name in arabic language and english language.");
      return null;
      }
      
    if (fieldControl.compareTo("List") == 0)
      {
        if (unique.compareTo("Yes") == 0)
          {
            unqiueValue = 1;
          }
        if (mandatory.compareTo("Yes") == 0)
          {
            mandatoryValue = 1;
          }
        control = "'1','0','0'";
        String checkNull = insertField(control, mandatoryValue, unqiueValue, campaignId);
        //System.out.println("string 1 is" + checkNull);
        if (checkNull == null)
          {
        	jsfUtils.putInSession(type, "fieldType");
            return "addList";

          }
        else
          {
              msgWarn.setValue(checkNull);
            return null;
          }

      }
    if (fieldControl.compareTo("One Line") == 0)
      {
        if (unique.compareTo("Yes") == 0)
          {
            unqiueValue = 1;
          }
        if (mandatory.compareTo("Yes") == 0)
          {
            mandatoryValue = 1;
          }
        control = "'0','0','1'";
        String checkNull = insertField(control, mandatoryValue, unqiueValue, campaignId);
        //System.out.println("string 2 is" + checkNull);
        if (checkNull == null)
          {
            return null;

          }
        else
          {
      msgWarn.setValue(checkNull);
           return null;
//            return "backToAdditionalField";
          }


      }

    if (fieldControl.compareTo("Multi-Line") == 0)
      {
        if (unique.compareTo("Yes") == 0)
          {
            unqiueValue = 1;
          }
        if (mandatory.compareTo("Yes") == 0)
          {
            mandatoryValue = 1;
          }
        control = "'0','1','0'";
        String checkNull = insertField(control, mandatoryValue, unqiueValue, campaignId);
        //System.out.println("string 3 is" + checkNull);
        if (checkNull == null)
          {
            return null;

          }
        else
          {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddAdditionalField_Bean");
            msgWarn.setValue(checkNull);
            return null;

//            return "backToAdditionalField";
          }


      }


    return null;
  }
  

  public String insertField(String control, int mandatoryValue, int unqiueValue, 
                            Integer campaignId)
  {    
      
     String message="";

    if (fieldStatus.compareTo("0") != 0)
      {
    	
    	
		try {
		   Connection conn = DBConnection.getConnection();
			message = additionalFieldDAO.insertField(conn,control, mandatoryValue, unqiueValue, campaignId, type, englishInput.trim(), arabicInput, fieldStatus, ctc);
			if (message.compareTo("")!=0 && message.compareTo("One field inserted")!=0)
                        {
	    		 
	    		DBConnection.closeConnections ( conn );
                        return message;
                        }
		} catch (SQLException e) {

			new com.mobinil.gvms.system.utility.PrintException().printException(e);
		}
    	 		
    	
      }
    else
      {
        msgWarn.setValue("Please select status for " + englishInput+ ".");
        return null;
      }
    return message;

  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setArabicInput(HtmlInputText inputText2)
  {
    this.arabicInput = inputText2;
  }

  public HtmlInputText getArabicInput()
  {
    return arabicInput;
  }

  public void setEnglishInput(String inputText3)
  {
    this.englishInput = inputText3;
  }

  public String getEnglishInput()
  {
    return englishInput;
  }

  public void setMandatory(String mandatory)
  {
    this.mandatory = mandatory;
  }

  public String getMandatory()
  {
    return mandatory;
  }

  public void setUnique(String unique)
  {
    this.unique = unique;
  }

  public String getUnique()
  {
    return unique;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getType()
  {
    return type;
  }


  public void setFieldControlArray(ArrayList<SelectItem> fieldControlArray)
  {
    this.fieldControlArray = fieldControlArray;
  }

  public ArrayList<SelectItem> getFieldControlArray()
  {
    return fieldControlArray;
  }

  public void setSelectOneMenu4(HtmlSelectOneMenu selectOneMenu4)
  {
    this.selectOneMenu4 = selectOneMenu4;
  }

  public HtmlSelectOneMenu getSelectOneMenu4()
  {
    return selectOneMenu4;
  }

  public void setSelectItems1(UISelectItems selectItems1)
  {
    this.selectItems1 = selectItems1;
  }

  public UISelectItems getSelectItems1()
  {
    return selectItems1;
  }


  public void setFieldControl(String fieldControl)
  {
    this.fieldControl = fieldControl;
  }

  public String getFieldControl()
  {
    return fieldControl;
  }

  public void setMsgWarn(HtmlOutputText outputText1)
  {
    this.msgWarn = outputText1;
  }

  public HtmlOutputText getMsgWarn()
  {
    return msgWarn;
  }

  

  public void setAddId(HtmlCommandButton commandButton1)
  {
    this.addId = commandButton1;
  }

  public HtmlCommandButton getAddId()
  {
    return addId;
  }

  public void setFieldStatus(String fieldStatus)
  {
    this.fieldStatus = fieldStatus;
  }

  public String getFieldStatus()
  {
    return fieldStatus;
  }
}
