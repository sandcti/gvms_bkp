package com.mobinil.gvms.system.admin.beans;



import com.mobinil.gvms.system.admin.campaign.DAO.additionalFieldDAO;
import com.mobinil.gvms.system.admin.campaign.DTO.AdditionalFieldClass;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.utility.DBConnection;
import com.mobinil.gvms.system.utility.ExecuteQueries;
import com.mobinil.gvms.system.utility.jsfUtils;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;


public class EditAdditionalField
{
	
  Map session = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();


  public ArrayList<String> fieldInfo = new ArrayList<String>(7);


  public String typeInfo;
  public HtmlOutputLabel msgWarn;
  public HtmlCommandButton update1;
  public ArrayList<SelectItem> fieldName;
  public String currentOrder;
  public String fieldsControl;
  Integer fieldId, campaignStatus;
  public String fieldStatus;
  public Boolean disableTypeAndControl = false;
  campaignTableClass ctc = null;
  AdditionalFieldClass afc = null;

  public EditAdditionalField()
  {
	  new leftMenu().checkUserInSession();
	  ctc = (campaignTableClass)jsfUtils.getFromSession("campaignInfo");
	  afc = (AdditionalFieldClass)jsfUtils.getFromSession("fieldInfo");
	  
    fieldName = new ArrayList<SelectItem>();
    campaignStatus = ctc.getVoucherStatus();        
    Integer campaignId = ctc.getCampaignId(); 
    
    try {
       Connection conn = DBConnection.getConnection();
		fieldName = additionalFieldDAO.getFieldInOrder(conn,fieldName, campaignId);
		DBConnection.closeConnections ( conn );
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}
    
    currentOrder = afc.getOrder() + "";
    fieldInfo.add(afc.getEnglishName());
    fieldInfo.add(afc.getFieldType());
    fieldInfo.add(afc.getUnique());
    fieldInfo.add(afc.getMandatory());
    fieldInfo.add(afc.getArabicName());
    String ss =afc.getStatus();
    if (ss.compareTo("Active")==0){
        fieldInfo.add("1");
    }
    else
    {
        fieldInfo.add("2");
    }
    
    
    
    fieldsControl = /*((String) session.get("fieldControl"))*/ afc.getFieldControl ( );
    

    if ( campaignStatus == 2)
      {
        disableTypeAndControl = true;

      }


  }

  public String back_Action()
  {    
    try
      {
    	jsfUtils.removeFromSession("AdditionalField_Bean");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/AdditionalField.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }

  public void update_Action() throws SQLException
  {
    for (int count = 2; count < 4; count++) // unique and mandatory values
      {
        if (fieldInfo.get(count).compareTo("Yes") == 0)
          {
            //System.out.println("change to 1 fieldInfo of " + count + " is " + 
//                               fieldInfo.get(count).toString());
            fieldInfo.set(count, "1");
          }
        else
          {
            //System.out.println("change to 0 fieldInfo of " + count + " is " + 
//                               fieldInfo.get(count).toString());
            fieldInfo.set(count, "0");
          }
      }
    Integer campaignId = ctc.getCampaignId();
      
    String newOrderforCurrentField = currentOrder;
    Integer newOrderforsecondField = afc.getOrder();   
    Connection conn = DBConnection.getConnection();
      ResultSet replaceOrderRS = 
            ExecuteQueries.executeQuery(conn,"Select FIELD_ID FROM GVMS_ADDITIONAL_FIELD where CAMPAIGN_ID ='"+campaignId+"' and FIELD_ORDER=" + 
                                        currentOrder);
            
      int replaceFieldId = 0;

      try
        {
          if (replaceOrderRS.next())
            {
              replaceFieldId = replaceOrderRS.getInt("FIELD_ID");

            }
          replaceOrderRS.getStatement().close();
          replaceOrderRS.close();

        }
      catch (SQLException e)
        {
           new com.mobinil.gvms.system.utility.PrintException().printException(e);
        }
      DBConnection.closeConnections ( conn );
        
        
 boolean flagss = false;
    if (fieldsControl.compareTo("List") == 0)
      {
        fieldsControl = ",FIELD_IS_TEXT_AREA=0,FIELD_IS_COMBO=1,FIELD_IS_INPUT_TEXT=0";
        flagss = true;
      }
    if (fieldsControl.compareTo("One-Line") == 0)
      {
        fieldsControl = ",FIELD_IS_TEXT_AREA=0,FIELD_IS_COMBO=0,FIELD_IS_INPUT_TEXT=1";
      }
    if (fieldsControl.compareTo("Multi-Line") == 0)
      {
        fieldsControl = ",FIELD_IS_TEXT_AREA=1,FIELD_IS_COMBO=0,FIELD_IS_INPUT_TEXT=0";
      }

    fieldId = afc.getFieldId(); 
        
    //System.out.println(fieldsControl);

    boolean update =true;
	try {
	   
		update = additionalFieldDAO.updateField(conn,fieldInfo, newOrderforCurrentField, newOrderforsecondField, fieldsControl, replaceFieldId, fieldId);
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		new com.mobinil.gvms.system.utility.PrintException().printException(e1);
	}
         

    
    if (update == false)
      {        
//        if (fieldsControl.compareTo(",FIELD_IS_TEXT_AREA=0,FIELD_IS_COMBO=1,FIELD_IS_INPUT_TEXT=0") == 
//            0)
//          {
//            try
//              {
//                if (campaignStatus != 2)
//                  {
//                    FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/ValidationList/AddValidationList.jsp");
//                  }
//                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editFieldId", 
//                                                                                           fieldId);
//              }
//            catch (IOException e)
//              {
//                 
//              }
//          }
//        //        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("hasValidList"); for updating
//        if (flagss)
//        {
//          try
//            {
//              FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/AdditionalField/ValidationList/AddValidationList.jsp");
//            }
//          catch (IOException e)
//            {
//               
//            }
//        }
    	msgWarn.setValue("Update "+fieldInfo.get(0).trim()+" Complete.");
      }
    else
      {
        msgWarn.setValue("Update "+fieldInfo.get(0).trim()+" Failed.");
        
      }


  }


  public void setMsgWarn(HtmlOutputLabel outputLabel1)
  {
    this.msgWarn = outputLabel1;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }

  public void setFieldInfo(ArrayList<String> fieldInfo)
  {
    this.fieldInfo = fieldInfo;
  }

  public void setInfo(String ff)
  {
    fieldInfo.set(6, ff);
  }

  public String getInfo()
  {
    return fieldInfo.get(6);
  }

  public ArrayList<String> getFieldInfo()
  {
    return fieldInfo;
  }


  public void setTypeInfo(String typeInfo)
  {
    this.typeInfo = typeInfo;
  }

  public String getTypeInfo()
  {
    return typeInfo;
  }

  public void setUpdate1(HtmlCommandButton commandButton2)
  {
    this.update1 = commandButton2;
  }

  public HtmlCommandButton getUpdate1()
  {
    return update1;
  }


  

  public void setFieldName(ArrayList<SelectItem> fieldName)
  {
    this.fieldName = fieldName;
  }

  public ArrayList<SelectItem> getFieldName()
  {
    return fieldName;
  }

  public void setFieldsControl(String fieldsControl)
  {
    this.fieldsControl = fieldsControl;
  }

  public String getFieldsControl()
  {
    return fieldsControl;
  }


  public void setDisableTypeAndControl(Boolean disableTypeAndControl)
  {
    this.disableTypeAndControl = disableTypeAndControl;
  }

  public Boolean getDisableTypeAndControl()
  {
    return disableTypeAndControl;
  }

    public void setFieldStatus(String fieldStatus) {
        this.fieldStatus = fieldStatus;
    }

    public String getFieldStatus() {
        return fieldStatus;
    }

    public void setCurrentOrder(String currentOrder) {
        this.currentOrder = currentOrder;
    }

    public String getCurrentOrder() {
        return currentOrder;
    }
}
