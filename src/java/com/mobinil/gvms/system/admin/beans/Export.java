package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.utility.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.ResultSet;

import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class Export
{
  public String htmlBuffer;
  

  public Export()
  {

  }

  public void exportHtmlTableToExcel()
    throws IOException
  {
    //System.out.println("html buffer in to : " + htmlBuffer);
    //System.out.println("in to Excel ");
    //Set the filename
    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd_HHmmss");
    String filename = dt.toString(fmt) + ".xls";


    //Setup the output
    String contentType = "application/vnd.ms-excel";
    FacesContext fc = FacesContext.getCurrentInstance();


    filename = "MM-" + filename;
    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    response.setHeader("Content-disposition", "attachment; filename=" + filename);
    response.setContentType(contentType);

    //Write the table back out
    PrintWriter out = response.getWriter();
    out.print(htmlBuffer);
    out.close();
    fc.responseComplete();
  }

  public void exportHtmlTableAsExcel()
    throws IOException
  {
    //System.out.println("html buffer in as : " + htmlBuffer);
    //System.out.println("in  as Excel ");
    int rowCount = 0;
    int colCount = 0;

    //Set the filename
    DateTime dt = new DateTime();
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd_HHmmss");
    String filename = dt.toString(fmt);


    // Create Excel Workbook and Sheet
    HSSFWorkbook wb = new HSSFWorkbook();
    HSSFSheet sheet = wb.createSheet(filename);
    HSSFHeader header = sheet.getHeader();
    header.setCenter(filename);


    //Setup the output
    String contentType = "application/vnd.ms-excel";
    FacesContext fc = FacesContext.getCurrentInstance();

    //System.out.println("File Name Is =" + filename);
    filename = "MM-" + filename;
    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".xls";
    response.setHeader("Content-disposition", "attachment; filename=" + filename);
    response.setContentType(contentType);
    ServletOutputStream out = response.getOutputStream();


    // Do stuff the Excel SpreaSheet
    // Freeze Panes on First Row
    sheet.createFreezePane(0, 1);
    // Row 1 Repeats on each page
    wb.setRepeatingRowsAndColumns(0, 0, 0, 0, 1);

    // Set Print Area, Footer
    wb.setPrintArea(0, 0, colCount, 0, rowCount);
    HSSFFooter footer = sheet.getFooter();
    footer.setCenter("Page " + HSSFFooter.page() + " of " + HSSFFooter.numPages());
    // Fit Sheet to 1 page wide but very long
    sheet.setAutobreaks(true);
    HSSFPrintSetup ps = sheet.getPrintSetup();
    ps.setFitWidth((short) 1);
    ps.setFitHeight((short) 9999);
    sheet.setGridsPrinted(true);
    sheet.setHorizontallyCenter(true);
    ps.setPaperSize(HSSFPrintSetup.LETTER_PAPERSIZE);
    if (colCount > 5)
      {
        ps.setLandscape(true);
      }
    if (colCount > 10)
      {
        ps.setPaperSize(HSSFPrintSetup.LEGAL_PAPERSIZE);
      }
    if (colCount > 14)
      {
        ps.setPaperSize(HSSFPrintSetup.EXECUTIVE_PAPERSIZE);
      }
    // Set Margins
    ps.setHeaderMargin((double) .35);
    ps.setFooterMargin((double) .35);
    sheet.setMargin(HSSFSheet.TopMargin, (double) .50);
    sheet.setMargin(HSSFSheet.BottomMargin, (double) .50);
    sheet.setMargin(HSSFSheet.LeftMargin, (double) .50);
    sheet.setMargin(HSSFSheet.RightMargin, (double) .50);

    //Write out the spreadsheet
    wb.write(out);
    out.close();

    fc.responseComplete();
  }

  public boolean exportTemplate(String template, String fileName)
  {
//    DBConnection.resetConnection();
    try
      {
        FileInputStream tempIn = new FileInputStream(template);
        HSSFWorkbook wb = new HSSFWorkbook(tempIn);

        HSSFSheet lookUpSheet = wb.getSheet("lookup");
        Connection conn = DBConnection.getConnection();
        fillColumn("select SHEET_TYPE_NAME, SHEET_TYPE_ID from VW_CR_SHEET_TYPE order by SHEET_TYPE_NAME ASC", 
                   "SHEET_TYPE_NAME", "SHEET_TYPE_ID", lookUpSheet, 0, 0,conn);
        fillColumn("select PRODUCT_NAME, PRODUCT_ID from VW_GEN_PRODUCT ORDER BY PRODUCT_NAME ASC", 
                   "PRODUCT_NAME", "PRODUCT_ID", lookUpSheet, 2, 0,conn);
        fillColumn("select NAME, ID  from VW_CR_ID_TYPE ORDER BY NAME ASC", "NAME", "ID", 
                   lookUpSheet, 4, 0,conn);

        //  fillColumn("select PRODUCT_NAME from VW_GEN_PRODUCT, "PRODUCT_NAME" , stat, lookUpSheet , 2, 0);
        DBConnection.closeConnections ( conn );
        HSSFSheet dataSheet = wb.getSheet("Data");
        dataSheet.setSelected(true);
        lookUpSheet.setSelected(false);
        dataSheet.setSelected(true);
        dataSheet.createRow(1).createCell((short) 0).setCellValue("");
        FileOutputStream fileOut = new FileOutputStream(fileName);
        wb.write(fileOut);

        fileOut.close();

        return true;
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
        return false;
      }
  }

  public boolean fillColumn(String sql, String culName, String culIDName, HSSFSheet sheet, 
                             int colNumber, int startRow,Connection conn)
  {
    try
      {
        ResultSet resultSet = ExecuteQueries.executeQuery(conn,sql);
        int index = startRow;
        while (resultSet.next())
          {
            HSSFRow row = sheet.createRow((short) index);
            HSSFCell cell = null;
            cell = row.createCell((short) colNumber);
            cell.setCellValue((String) resultSet.getString(culName));
            cell = row.createCell((short) (colNumber + 1));
            cell.setCellValue((String) resultSet.getString(culIDName));

            index++;
          }
        resultSet.getStatement().close();
        resultSet.close();
        return true;
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
        return false;
      }
  }


  public String getHtmlBuffer()
  {
    return htmlBuffer;
  }

  public void setHtmlBuffer(String htmlBuffer)
  {
    this.htmlBuffer = htmlBuffer;
  }

}
