package com.mobinil.gvms.system.admin.beans;

import com.mobinil.gvms.system.admin.usertype.UserTypeDTO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpSession;


public class UserType
{
  
  public ArrayList<UserTypeDTO> userTypeArray;
  public HtmlDataTable dataTable1;
  public HtmlCommandButton createUserType;
  public String msgWarn;
  public List<UserTypeDTO> selectedDataList;
  UserTypeDTO dataItem;

  int pageIndex = 0;
  public Integer rowCount = 10;
  public String sortField = null;
  public boolean sortAscending = true;

  public UserType() throws SQLException
  {
        new leftMenu().checkUserInSession();
    userTypeArray = new ArrayList<UserTypeDTO>();
    Connection conn = DBConnection.getConnection();
    String SQL = 
      "Select t1.USER_TYPE_ID,t1.USER_TYPE_STATUS_ID,t1.USER_TYPE_NAME,t1.USER_TYPE_DESCRIPTION,t2.USER_TYPE_STATUS From GVMS_USER_TYPE t1 left join GVMS_USER_TYPE_STATUS t2 on t1.USER_TYPE_STATUS_ID = t2.USER_TYPE_STATUS_ID where t1.USER_TYPE_STATUS_ID!=3 order by t1.USER_TYPE_ID";
    ResultSet userTypeRS = ExecuteQueries.executeQuery(conn,SQL);
    try
      {
        while (userTypeRS.next())
          {
            int userid = userTypeRS.getInt("USER_TYPE_ID");
            UserTypeDTO ut = new UserTypeDTO();
            ut.setDescription(userTypeRS.getString("USER_TYPE_DESCRIPTION"));
            ut.setId(userid);
            ut.setStatus(userTypeRS.getString("USER_TYPE_STATUS"));
            ut.setName(userTypeRS.getString("USER_TYPE_NAME"));
            ut.setStatusId(userTypeRS.getInt("USER_TYPE_STATUS_ID"));

            try
              {
                Integer id = 
                  (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("rowNumber" + 
                                                                                                       userid);
                //System.out.println("id is" + id);
                //System.out.println("rowNumber" + userid + " is " + id);
                if (id == (userTypeRS.getInt("USER_TYPE_ID")))
                  {

                    ut.setSelected(true);
                    msgWarn = "Delete operation failed due to active users.";
                  }
              }
            catch (Exception e)
              {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }


            userTypeArray.add(ut);

          }
        userTypeRS.getStatement().close();
        userTypeRS.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );
    for (int counter1 = 0; counter1 < userTypeArray.size(); counter1++)
      {
        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        HttpSession session = 
          (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        String[] beanNames = session.getValueNames();
        for (int x = 0; x < beanNames.length; x++)
          {
            if (beanNames[x].contains("rowNumber"))
              {

                if (sessionMap.containsKey(beanNames[x]))
                  {
                    sessionMap.remove(beanNames[x]);
                  }

              }
          }

      }


  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
      {
        sortAscending = !sortAscending;
      }
    else
      {
        sortField = sortFieldAttribute;
        sortAscending = true;
      }

    // Sort results.
    if (sortField != null)
      {
        Collections.sort(userTypeArray, new DTOComparator(sortField, sortAscending));
      }


  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }

  public String getSelectedItems() throws SQLException
  {

    // Get selected items.

    selectedDataList = new ArrayList<UserTypeDTO>();
    int count = 1;
    String msg = "";
    String users = "";
    String msg1 = "";
    Connection conn = DBConnection.getConnection();
    for (int counter = (dataTable1.getRows() * pageIndex); counter < userTypeArray.size(); 
         counter++)


      {

        if (counter == (dataTable1.getRows() * (pageIndex + 1)))
          {
            break;
          }

        UserTypeDTO dataItems = new UserTypeDTO();
        dataItems = userTypeArray.get(counter);
        if (dataItems.getSelected())
          {
            selectedDataList.add(dataItems);
            dataItems.setSelected(false); // Reset.

            int id = dataItems.getId();
            ResultSet userRS = 
              ExecuteQueries.executeQuery(conn,"Select USER_STATUS_ID from GVMS_USER where USER_TYPE_ID=" + 
                                          id);

            try
              {
                String activeUsers = "";
                while (userRS.next())
                  {

                    if (userRS.getInt("USER_STATUS_ID") == 1)
                      {
                        activeUsers += userRS.getInt("USER_STATUS_ID");


                      }
                  }
                userRS.getStatement().close();
                userRS.close();
                if (activeUsers.compareTo("") == 0)
                  {
                    int delete = 
                      ExecuteQueries.executeNoneQuery("update GVMS_USER_TYPE set USER_TYPE_STATUS_ID=3 where USER_TYPE_ID=" + 
                                                      id);

                    if (delete == 1)
                      {
                        UpdateClass.updateUserTypeInfo(id+"", true);
                        msg = dataItems.getName() + ",";
                        msgWarn += "Users type " + msg + "deleted";
                      }
                    else
                      {
                        msg1 = dataItems.getName() + ",";
                        msgWarn += "Users type " + msg1 + "  not deleted";
                      }
                  }
                else
                  {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("rowNumber" + 
                                                                                               dataItems.getId(), 
                                                                                               dataItems.getId());


                  }


              }
            catch (SQLException e)
              {
                 new com.mobinil.gvms.system.utility.PrintException().printException(e);
              }
          }
        count++;


      }
    DBConnection.closeConnections ( conn );

    //      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("")

    // Do your thing with the MyData items in List selectedDataList.
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("UserType_Bean");
    return "refresh"; // Navigation case.
    //    return "/GVMS/faces/Admin/User/UserType/UserType.jsp";
  }

  public String newUserType()
  {
    return "addUserType";
  }

  public String remove_Item()
  {
    return null;
  }

  public String edit_Action()
  {
    dataItem = (UserTypeDTO) dataTable1.getRowData();
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeName", 
                                                                               dataItem.getName());
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeDescription", 
                                                                               dataItem.getDescription());
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeStatus", 
                                                                               "" + 
                                                                               dataItem.getStatusId());
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userTypeId", 
                                                                               dataItem.getId());


    return "editUserType";
  }

  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }


  public void setUserTypeArray(ArrayList<UserTypeDTO> userTypeArray)
  {
    this.userTypeArray = userTypeArray;
  }

  public ArrayList<UserTypeDTO> getUserTypeArray()
  {
    return userTypeArray;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setCreateUserType(HtmlCommandButton createUserType)
  {
    this.createUserType = createUserType;
  }

  public HtmlCommandButton getCreateUserType()
  {
    return createUserType;
  }

  public void setRowCount(Integer rowCount)
  {
    this.rowCount = rowCount;
  }

  public Integer getRowCount()
  {
    return rowCount;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }
}
