package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;


public class EditUserType
{
  
  public String userTypeName;
  public String userTypeDescription;
  public String status;
  public String tabIndex;
  public String msgWarn;
  String userTypeid;
  public HtmlSelectOneMenu selectControl;


  public EditUserType()
  {
        new leftMenu().checkUserInSession();

    userTypeName = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeName");
    userTypeDescription = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeDescription");
    status = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeStatus");
    userTypeid = 
        ((Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userTypeId")).toString();

  }

  public String updateUserType_Action() throws SQLException
  {
    userTypeName = userTypeName.trim();
    if (userTypeDescription.length() > 1000)
    {
      msgWarn="Maximum characters for description are 1000.";

      return null;
    }
 
  if (userTypeName==null ||  
  	userTypeName.compareTo("") == 0 ||
  	status.compareTo("0") == 0)
    {

      msgWarn="Please fill star boxes.";
      return null;
    }
  
  Connection conn = DBConnection.getConnection();
    ResultSet repeatUserType =     
      ExecuteQueries.executeQuery(conn,"Select USER_TYPE_NAME from GVMS_USER_TYPE where USER_TYPE_NAME='" + 
                                  userTypeName.trim() + "' and USER_TYPE_ID !=" + userTypeid);
                                      
    try
      {
        if (repeatUserType.next())
          {
            msgWarn = "this user type already exists.";
            DBConnection.closeConnections ( conn );
            return null;
          }
        else
          {

              String creatorName = 
                ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();

            if (status.compareTo("0") != 0)
              {
                //System.out.println("userTypeName " + userTypeName);
                //System.out.println("userTypeDescription" + userTypeDescription);
                //System.out.println("status" + status);

                int update = 
                  ExecuteQueries.executeNoneQuery("update GVMS_USER_TYPE set USER_TYPE_NAME='" + 
                                                  userTypeName.trim() + "', USER_TYPE_DESCRIPTION='" + 
                                                  userTypeDescription + "',MODIFIED_BY='" + 
                                                  creatorName + "',MODIFIED_DATE=" + 
                                                  new DateClass().convertDate(new DateClass().getDateTime()) + 
                                                  ",USER_TYPE_STATUS_ID=" + status + 
                                                  " where USER_TYPE_ID=" + userTypeid);
                if (update == 1)
                  {
                    UpdateClass.updateUserTypeInfo(userTypeid, Boolean.TRUE);
                    msgWarn = "Update Complete.";
                  }
                else
                  {
                    msgWarn = "Update Fails.";
                  }
              }
            else
              {
                msgWarn = "Please select status for " + userTypeName + ".";
              }
          }
        repeatUserType.getStatement().close();
        repeatUserType.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    DBConnection.closeConnections ( conn );

    return null;
  }

  public String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }

  public String convertDate(String values)
  {
    if (values == null)
      {
        ;
        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY')";
        return values;
      }
    else
      {
        String[] toDate = values.split("/");

        values = 
            "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY HH24:MI:SS')";
        //System.out.println(values);
        return values;
      }
  }

  public String back_Action()
  {
    try
      {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("UserType_Bean"))
          {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("UserType_Bean");
          }
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/UserType/UserType.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }
  public HtmlForm form1;

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setUserTypeName(String userTypeName)
  {
    this.userTypeName = userTypeName;
  }

  public String getUserTypeName()
  {
    return userTypeName;
  }

  public void setUserTypeDescription(String userTypeDescription)
  {
    this.userTypeDescription = userTypeDescription;
  }

  public String getUserTypeDescription()
  {
    return userTypeDescription;
  }


  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setSelectControl(HtmlSelectOneMenu selectControl)
  {
    this.selectControl = selectControl;
  }

  public HtmlSelectOneMenu getSelectControl()
  {
    return selectControl;
  }

  public void setTabIndex(String tabIndex)
  {
    this.tabIndex = tabIndex;
  }

  public String getTabIndex()
  {
    return tabIndex;
  }
}
