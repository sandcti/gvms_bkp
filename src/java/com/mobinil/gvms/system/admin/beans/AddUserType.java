package com.mobinil.gvms.system.admin.beans;
//

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.utility.db.DBUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;


public class AddUserType
{
	
  public HtmlForm form1;
  public HtmlInputText userTypeNameInput;
  public HtmlInputTextarea userTypeDesTexArea;


  
  
  public String userStatusSelected;
  public HtmlCommandButton addUT_cmd;
  public HtmlOutputLabel msgWarn;

  public AddUserType() throws SQLException
  {
        new leftMenu().checkUserInSession();
        
  }

  public String AddUserType_cmd() throws SQLException
  {
      String creatorName = 
        ((LoginDTO) jsfUtils.getFromSession("adminInfo")).getFullName();
      String desc = (String) userTypeDesTexArea.getValue();
      if (desc.length() > 1000)
      {
        msgWarn.setValue("Maximum characters for description are 1000.");

        return null;
      }
   String userTypeName =((String) userTypeNameInput.getValue()).trim();   
    if (userTypeName==null ||  
    	userTypeName.compareTo("") == 0 ||
        userStatusSelected.compareTo("0") == 0)
      {

        msgWarn.setValue("Please fill star boxes.");
        return null;
      }
    Connection conn = DBConnection.getConnection();
    String SQL;

    ResultSet repeatUserType = 
      ExecuteQueries.executeQuery(conn,"Select USER_TYPE_NAME from GVMS_USER_TYPE where USER_TYPE_NAME='" + 
    		  userTypeName + "'");
    try
      {
        if (repeatUserType.next())
          {
            msgWarn.setValue("this user type already exists.");
            DBConnection.closeConnections ( conn );
            return null;
          }
        else
          {

            if (userStatusSelected.compareTo("0") != 0)
              {

        Long userTypeId = DBUtil.getSequenceNextVal(conn, "GVMS_USER_TYPE_SEQ_ID");

                SQL = 
                    "insert into GVMS_USER_TYPE(USER_TYPE_ID,USER_TYPE_NAME,USER_TYPE_DESCRIPTION,USER_TYPE_STATUS_ID,CREATION_DATE,CREATED_BY) values(" + 
                    +userTypeId+",'" + userTypeName +
                    "','" + (String) userTypeDesTexArea.getValue() + "'," + userStatusSelected + 
                    "," + new DateClass().convertDate(new DateClass().getDateTime()) + ",'" + 
                    creatorName + "')";

                int insertGiftType = ExecuteQueries.executeNoneQuery(SQL);

                if (insertGiftType == 1)
                  {
                    msgWarn.setValue("One new user Type inserted");
                    UpdateClass.updateUserTypeInfo(userTypeId+"", false);
                  }
                else
                  {
                    msgWarn.setValue("Creation failed");
                  }

               
              }
            else
              {
                msgWarn.setValue("Please select User Type Status");
              }
          }
        repeatUserType.getStatement().close();
        repeatUserType.close();
      }
    catch (SQLException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    DBConnection.closeConnections ( conn );
    return null;
  }

  public String getDateTime()
  {
    java.util.Date currentDate = new java.util.Date();
    int currentYear;
    currentYear = currentDate.getYear() + 1900;
    int currentMonth = currentDate.getMonth() + 1;
    int currentDay = currentDate.getDate();
    int currentHour = currentDate.getHours();
    int currentMin = currentDate.getMinutes();
    int currentsec = currentDate.getSeconds();
    String strCurrentDate = 
      currentMonth + "/" + currentDay + "/" + currentYear + " " + currentHour + ":" + currentMin + 
      ":" + currentsec;
    //System.out.println(strCurrentDate);
    return strCurrentDate;

  }

  public String convertDate(String values)
  {
    if (values == null)
      {
        ;
        String[] toDate = new String[3];
        toDate[0] = "00";
        toDate[1] = "00";
        toDate[2] = "0000";
        values = "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY')";
        return values;
      }
    else
      {
        String[] toDate = values.split("/");

        values = 
            "to_date('" + toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "', 'MM/DD/YYYY HH24:MI:SS')";
        //System.out.println(values);
        return values;
      }
  }

  public String back_Action()
  {

    try
      {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("UserType_Bean"))
          {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("UserType_Bean");
          }
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/UserType/UserType.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return null;
  }


  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }

  public void setUserTypeNameInput(HtmlInputText userTypeNameInput)
  {
    this.userTypeNameInput = userTypeNameInput;
  }

  public HtmlInputText getUserTypeNameInput()
  {
    return userTypeNameInput;
  }

  public void setUserTypeDesTexArea(HtmlInputTextarea userTypeDesTexArea)
  {
    this.userTypeDesTexArea = userTypeDesTexArea;
  }

  public HtmlInputTextarea getUserTypeDesTexArea()
  {
    return userTypeDesTexArea;
  }


  public void setUserStatusSelected(String userStatusSelected)
  {
    this.userStatusSelected = userStatusSelected;
  }

  public String getUserStatusSelected()
  {
    return userStatusSelected;
  }

  public void setAddUT_cmd(HtmlCommandButton addUT_cmd)
  {
    this.addUT_cmd = addUT_cmd;
  }

  public HtmlCommandButton getAddUT_cmd()
  {
    return addUT_cmd;
  }

  public void setMsgWarn(HtmlOutputLabel msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public HtmlOutputLabel getMsgWarn()
  {
    return msgWarn;
  }
}
