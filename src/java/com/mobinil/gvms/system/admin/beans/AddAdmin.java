package com.mobinil.gvms.system.admin.beans;


import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.admin.usermanagement.DAO.UserManagementDAO;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.SQLException;



import java.io.IOException;

import java.sql.ResultSet;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;


public class AddAdmin
{
  
  public String userNameInput;
  public String passwordInput;
  public String confirmPasswordInput;
  public String fullNameInput;
  public String emailInput;
  public String phoneInput;
  public String oldPasswordInput;
  String pass;
  String userName;
  Integer adminUserId;
  Integer adminId;
  public HtmlForm form1;
  public HtmlCommandButton addAdmin_cmd;
  public Boolean renderOldPass;
  public Boolean renderAdd;
  public Boolean renderUpdate;
  public String userLockSelected;
  public String pageTitle;
  public String maximumLockNum;
public String passwordLabel;
public String passwordConfrimationLabel;
String editAdminPass ;
  public String userStatusSelected;

  public HtmlSelectOneMenu selectOneMenu1;
  public UISelectItems selectItems1;
  public HtmlOutputLabel checkLbl;
  String bridg;
  LoginDTO adminInfo = null;
  public AddAdmin()
  {
    new leftMenu().checkUserInSession();
   try {
      
      Connection conn = DBConnection.getConnection();
	   passwordLabel = "Password";
	   passwordConfrimationLabel = "Confirm Password";
	    adminInfo =   (LoginDTO) jsfUtils.getFromSession("adminInfo");
            adminInfo = adminInfo==null ? (LoginDTO) jsfUtils.getFromSession("repInfo") : adminInfo;

    bridg = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editUser");
    //System.out.println("step a1");
    userName = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userNameAdmin");
    //System.out.println("step a2");
    ResultSet lockTimesRS = 
      ExecuteQueries.executeQuery(conn,"SELECT PROPERTIES from GVMS_PROPERTIES where REASONE='LOCK_TIMES'");

    if (bridg.compareTo("newAdmin") == 0 || bridg.compareTo("newReporter") == 0)
      {
        renderAdd = true;
        renderOldPass = false;
        renderUpdate = false;
        passwordLabel = passwordLabel+"*";
        passwordConfrimationLabel = passwordConfrimationLabel+"*";
        pageTitle = bridg.compareTo("newReporter") == 0 ? "New Reporter" : "New Admin";
        try
          {
            if (lockTimesRS.next())
              {
                maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);

              }
            lockTimesRS.getStatement().close();
            lockTimesRS.close();
          }
        catch (SQLException e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
      } 
    else
      {
        renderUpdate = true;
        renderAdd = false;
        pageTitle = bridg.compareTo("editReporter") == 0 ? "Edit Reporter" : "Edit Admin";
        //System.out.println("step a3");
        adminId = 
            (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_id");
        fullNameInput = 
            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_name");
        adminUserId = 
            (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_sys_id");
        userNameInput = 
            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_user_name");
        
        emailInput = 
            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_email");
        phoneInput = 
            "" + (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_phone");
        userStatusSelected = 
            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_status");
        editAdminPass = 
            (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin_pass");
        String aa = 
          (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("lockAdminStatus");
        //System.out.println("step a4 "+aa);
        try
          {
            if (lockTimesRS.next())
              {
                if (aa.compareTo("Not Locked") == 0)
                  {

                    maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);
                    userLockSelected = "0";
                  }
                else
                  {


                    maximumLockNum = "" + (lockTimesRS.getInt("PROPERTIES") + 1);
                    userLockSelected = maximumLockNum;


                  }
              }
            DBConnection.cleanResultSet( lockTimesRS);
          }
        catch (SQLException e)
          {
        	new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }
        //System.out.println("fullNameInput is " + fullNameInput);
        //System.out.println("system user id from table is " +adminUserId );
        //System.out.println("system user id from session is " +adminInfo.getSysUserId() );

        if (adminUserId.compareTo(adminInfo.getSysUserId())==0)
          {
            renderOldPass =false ;
          }
        else
          {
            renderOldPass = true;
          }

        if (bridg.compareTo("editReporter") == 0)renderOldPass =false ;
        
        //System.out.println("renderOldPass is " +renderOldPass );


      }
    DBConnection.closeConnections ( conn );
    
   } catch (Exception e) {
		// TODO: handle exception
	   new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}
  }

  public String update_Action() throws SQLException
  {    
    Integer systemUserId = adminInfo.getSysUserId();
    userNameInput = userNameInput.trim();
    bridg =
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editUser");
    
    if (emailInput!=null)
    {
  	  if (!emailInput.contains("@"))
  	  {
  		checkLbl.setValue("Please enter a valid mail.");
            return null;
  	  }
  	if (!emailInput.contains("."))
	  {
		checkLbl.setValue("Please enter a valid mail.");
          return null;
	  }
    }
String checkValue= checkPhone();
if (checkValue!=null)
{
checkLbl.setValue(checkValue);
return null;
}
    
    Connection conn = DBConnection.getConnection();
    ResultSet repeatUserName = 
      ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_NAME from GVMS_SYSTEM_USER where SYSTEM_USER_NAME='" + 
                                  userNameInput + 
                                  "' and SYSTEM_USER_TYPE in ('admin','reporter') and SYSTEM_USER_ID!=" + adminUserId);
    try
      {
        if (repeatUserName.next())
          {

            checkLbl.setValue("this user name already exists.");
            return null;

          }
        repeatUserName.getStatement().close();
        repeatUserName.close();
      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    if (passwordInput.compareTo(confirmPasswordInput) != 0)
      {
        checkLbl.setValue("Please retype password and confirmation.");
        return null;
      }
    //System.out.println("userNameInput is "+userNameInput);
    //System.out.println("oldPasswordInput is "+oldPasswordInput);
    //System.out.println("fullNameInput is "+fullNameInput);
    //System.out.println("userStatusSelected is "+userStatusSelected);
    //System.out.println("userLockSelected is "+userLockSelected);
    
      if (userNameInput==null ||  fullNameInput==null || fullNameInput.compareTo("")==0 
    		  ||userStatusSelected.compareTo("0") == 0 ||userLockSelected.compareTo("10001") == 0 || userNameInput.compareTo("")==0 )
          {
          
              checkLbl.setValue("Please fill mandatory fields");
          
            return null;
          }

    try
      {
        if (adminUserId.compareTo(adminInfo.getSysUserId())!=0 && bridg.compareTo("editAdmin")==0)
          {
            if (editAdminPass.compareTo(MD5Class.MD5(oldPasswordInput)) == 0)
              {
                if (oldPasswordInput==null||oldPasswordInput.compareTo("")==0 ||passwordInput.compareTo(confirmPasswordInput) != 0)
                  {
                    checkLbl.setValue("password and confirmation not equal");
                    return null;
                  }
              }
            else
              {
            	if (passwordInput==null||passwordInput.compareTo("")==0 || confirmPasswordInput==null||confirmPasswordInput.compareTo("")==0 )
            	{
            		checkLbl.setValue("Please provide password and confirmation.");
                    return null;
            	}
                checkLbl.setValue("Invalid old password");
                return null;
              }
          }
      }
    catch (Exception e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }


    

        
          


        
        try
          {


            String passEncripted = MD5Class.MD5(passwordInput);

            
            String SecurityError = "";
            if (passwordInput!=null&&passwordInput.compareTo("")!=0)
            {
            	SecurityError =  SecurityUtils.checkPass(passwordInput, passEncripted, 
                        userNameInput.trim(), 
                        adminUserId + "");
            }
            else passEncripted="";
        	
            
                                            
            if (SecurityError.compareTo("")!=0)
            {
             checkLbl.setValue(SecurityError);
            }
            else
            {
             if(UserManagementDAO.updateUserInfo( adminUserId+"",adminId+"",passEncripted,userNameInput,userStatusSelected,emailInput,phoneInput,fullNameInput,"",userName,0,userLockSelected,"admin"))
             {
             UserManagementDAO.updateUserLastPass(systemUserId+"",passEncripted);
             checkLbl.setValue("User data updated successfully.");
             }
            }

//           

          }
        catch (Exception e)
          {
             new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

        DBConnection.closeConnections ( conn );
     
        
    //    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddAdmin_Bean");
    return null;
  }

  public String addAdminCommandAction() throws SQLException
  {
    String systemUserId = "";


    String checkValue= checkPhone();
if (checkValue!=null)
{
checkLbl.setValue(checkValue);
return null;
}

     if (emailInput!=null)
    {
  	  if (!emailInput.contains("@"))
  	  {
  		checkLbl.setValue("Please enter a valid mail.");
            return null;
  	  }
  	if (!emailInput.contains("."))
	  {
		checkLbl.setValue("Please enter a valid mail.");
          return null;
	  }
    }

       Connection conn = DBConnection.getConnection();
    //    int userStatusId = 0;
     userNameInput = userNameInput.trim();


    ResultSet repeatUserName = 
      ExecuteQueries.executeQuery(conn,"select SYSTEM_USER_NAME from GVMS_SYSTEM_USER where SYSTEM_USER_NAME='" + 
                                  userNameInput + "' and SYSTEM_USER_TYPE in ('admin','reporter')");
    try
      {
        if (repeatUserName.next())
          {

            checkLbl.setValue("this user name already exists.");
            DBConnection.closeConnections ( conn );
            return null;

          }
        repeatUserName.close();
      }
    catch (SQLException e)
      {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }
    
   
    //System.out.println("userNameInput is "+userNameInput);
    //System.out.println("passwordInput is "+passwordInput);
    //System.out.println("fullNameInput is "+fullNameInput);
    //System.out.println("userStatusSelected is "+userStatusSelected);
    //System.out.println("userLockSelected is "+userLockSelected);
    if (userNameInput==null || passwordInput==null ||fullNameInput==null || fullNameInput.compareTo("")==0 
  		  ||userStatusSelected.compareTo("0") == 0 ||userLockSelected.compareTo("10001") == 0 || passwordInput.compareTo("")==0 ||userNameInput.compareTo("")==0 )
        {
        
            checkLbl.setValue("Please fill mandatory fields");
            DBConnection.closeConnections ( conn );
          return null;
        }
    String SQL;

    if (passwordInput.compareTo(confirmPasswordInput) != 0)
      {
        checkLbl.setValue("password not equal");
        DBConnection.closeConnections ( conn );
        return null;
        
      }        


        try
          {
            bridg =
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editUser");
            systemUserId = DBConnection.getSequenceNextVal(conn,"GVMS_SYSTEM_USER_SEQ_ID");
            String passEncripted = MD5Class.MD5(passwordInput);
           
           

            String SecurityError =  SecurityUtils.checkPass(passwordInput, passEncripted, 
                                            userNameInput.trim(), 
                                            systemUserId + "");
                                            
            if (SecurityError.compareTo("")!=0)
            {
             checkLbl.setValue(SecurityError);
            }
            else
            {

             if(UserManagementDAO.setNewUser(systemUserId,passEncripted,userNameInput,userStatusSelected,emailInput,phoneInput,fullNameInput,"",userName,0,userLockSelected,bridg.compareTo("newAdmin")==0 ?"admin" : bridg.compareTo("newReporter")==0 ? "reporter" : ""))
             {
             UserManagementDAO.updateUserLastPass(systemUserId,passEncripted);
             checkLbl.setValue("One Admin Added.");
             if (bridg.compareTo("newReporter") == 0 )
             {checkLbl.setValue("One Reporter Added.");}
             }
            }
             

          }
        catch (Exception e)
          {
           DBConnection.closeConnections ( conn );
          }
        DBConnection.closeConnections ( conn );

    //    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");    
    return null;
  }

  public String back_Action()
  {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("User_Bean");
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddAdmin_Bean");
    if (bridg.compareTo("admin") == 0)
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddUser_Bean");

      }
    else
      {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("AddAdmin_Bean");
      }


    try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/User/User.jsp");
      }
    catch (IOException e)
      {
         new com.mobinil.gvms.system.utility.PrintException().printException(e);
      }

    return "backToUser";
  }

  public void setForm1(HtmlForm form1)
  {
    this.form1 = form1;
  }

  public HtmlForm getForm1()
  {
    return form1;
  }


  public void setAddAdmin_cmd(HtmlCommandButton commandButton1)
  {
    this.addAdmin_cmd = commandButton1;
  }

  public HtmlCommandButton getAddAdmin_cmd()
  {
    return addAdmin_cmd;
  }

  public void setSelectOneMenu1(HtmlSelectOneMenu selectOneMenu1)
  {
    this.selectOneMenu1 = selectOneMenu1;
  }

  public HtmlSelectOneMenu getSelectOneMenu1()
  {
    return selectOneMenu1;
  }

  public void setSelectItems1(UISelectItems selectItems1)
  {
    this.selectItems1 = selectItems1;
  }

  public UISelectItems getSelectItems1()
  {
    return selectItems1;
  }


  public void setUserStatusSelected(String userStatusSelected)
  {
    this.userStatusSelected = userStatusSelected;
  }

  public String getUserStatusSelected()
  {
    return userStatusSelected;
  }

  public void setCheckLbl(HtmlOutputLabel outputLabel1)
  {
    this.checkLbl = outputLabel1;
  }

  public HtmlOutputLabel getCheckLbl()
  {
    return checkLbl;
  }


  public void setRenderOldPass(Boolean renderOldPass)
  {
    this.renderOldPass = renderOldPass;
  }

  public Boolean getRenderOldPass()
  {
    return renderOldPass;
  }

  public void setUserNameInput(String userNameInput)
  {
    this.userNameInput = userNameInput;
  }

  public String getUserNameInput()
  {
    return userNameInput;
  }

  public void setPasswordInput(String passwordInput)
  {
    this.passwordInput = passwordInput;
  }

  public String getPasswordInput()
  {
    return passwordInput;
  }

  public void setConfirmPasswordInput(String confirmPasswordInput)
  {
    this.confirmPasswordInput = confirmPasswordInput;
  }

  public String getConfirmPasswordInput()
  {
    return confirmPasswordInput;
  }

  public void setFullNameInput(String fullNameInput)
  {
    this.fullNameInput = fullNameInput;
  }

  public String getFullNameInput()
  {
    return fullNameInput;
  }

  public void setEmailInput(String emailInput)
  {
    this.emailInput = emailInput;
  }

  public String getEmailInput()
  {
    return emailInput;
  }

  public void setPhoneInput(String phoneInput)
  {
    this.phoneInput = phoneInput;
  }

  public String getPhoneInput()
  {
    return phoneInput;
  }

  public void setOldPasswordInput(String oldPasswordInput)
  {
    this.oldPasswordInput = oldPasswordInput;
  }

  public String getOldPasswordInput()
  {
    return oldPasswordInput;
  }


  public void setRenderAdd(Boolean renderAdd)
  {
    this.renderAdd = renderAdd;
  }

  public Boolean getRenderAdd()
  {
    return renderAdd;
  }

  public void setRenderUpdate(Boolean renderUpdate)
  {
    this.renderUpdate = renderUpdate;
  }

  public Boolean getRenderUpdate()
  {
    return renderUpdate;
  }

  public void setUserLockSelected(String userLockSelected)
  {
    this.userLockSelected = userLockSelected;
  }

  public String getUserLockSelected()
  {
    return userLockSelected;
  }

  public void setMaximumLockNum(String maximumLockNum)
  {
    this.maximumLockNum = maximumLockNum;
  }

  public String getMaximumLockNum()
  {
    return maximumLockNum;
  }

public void setPasswordConfrimationLabel(String passwordConfrimationLabel) {
	this.passwordConfrimationLabel = passwordConfrimationLabel;
}

public String getPasswordConfrimationLabel() {
	return passwordConfrimationLabel;
}

public void setPasswordLabel(String passwordLabel) {
	this.passwordLabel = passwordLabel;
}

public String getPasswordLabel() {
	return passwordLabel;
}

    /**
     * @return the pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * @param pageTitle the pageTitle to set
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String checkPhone() {
        String returnStr = null;
        if (phoneInput != null) {
            try {
                Double.parseDouble(phoneInput);
                if (phoneInput.length()<9 || phoneInput.length()>12) {
                    returnStr="Invalid phone number length.";
                }
            } catch (Exception e) {
                new com.mobinil.gvms.system.utility.PrintException().printException(e);
                returnStr = "Invalid phone number.";
            }


        } else {
            returnStr = "Missing phone number.";
        }
        return returnStr;

    }
}
