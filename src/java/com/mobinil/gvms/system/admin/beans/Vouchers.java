package com.mobinil.gvms.system.admin.beans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.component.html.ext.HtmlInputTextarea;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.mobinil.gvms.system.admin.campaign.DAO.VoucherDAO;
import com.mobinil.gvms.system.admin.campaign.DTO.VoucherClass;
import com.mobinil.gvms.system.admin.campaign.DTO.campaignTableClass;
import com.mobinil.gvms.system.utility.*;


public class Vouchers
{  
	
  public ArrayList<VoucherClass> voucherArray;
  public HtmlDataTable dataTable1;
  public String sortField = null;
  public boolean sortAscending = true;
  public String selectedValueRedeemed;
  public Boolean renderDialCol = false;
  public HtmlInputTextarea messageControl;
  public String smsMessage;
  public String selectLang;
  
  public String campaignName;
  Integer campaignId;
  Integer campaignVoucherId;
  int pageIndex = 0;
  ResultSet vouchersRS;
  public String msgWarn;
  campaignTableClass ctc = null;
  public Vouchers()
  {
	  new leftMenu().checkUserInSession();
	   ctc= (campaignTableClass) jsfUtils.getFromSession("campaignInfo");

    voucherArray = new ArrayList<VoucherClass>();
    campaignId =ctc.getCampaignId(); 
        

    campaignVoucherId = ctc.getVoucherTypeId();
        
    campaignName = ctc.getCampaignName(); 
    
    if (campaignVoucherId == 2 || campaignVoucherId == 3)
    {
    	campaignVoucherId = 2;
    	renderDialCol=true;
    	
    }
        
    
    
    try {
		voucherArray = VoucherDAO.fillVoucherTable(selectedValueRedeemed, campaignVoucherId, campaignId,false);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}

  }

  public void filter_Action()
  {    
    voucherArray = new ArrayList<VoucherClass>();
    campaignId = ctc.getCampaignId();
    campaignVoucherId = ctc.getVoucherTypeId();
        
    try {
		voucherArray = VoucherDAO.fillVoucherTable(selectedValueRedeemed, campaignVoucherId, campaignId,true);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		new com.mobinil.gvms.system.utility.PrintException().printException(e);
	}

  }

  

  public void ExportToCSV()
  {

    campaignName = 
        (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("campaignName");
    List<List<String>> csvList = new ArrayList<List<String>>();
    String reportName = "";


    reportName = campaignName + "-voucher";
    for (int counter = 0; counter < voucherArray.size(); counter++)
    {

      if (campaignVoucherId == 2)
      {

        //                  voucherArray.get(counter).getVoucherStatus()
        if (counter == 0)
        {

          csvList.add(Arrays.asList(new String[]
                { "Dial Number", "Voucher Number" }));
        }
        csvList.add(Arrays.asList(new String[]
              { voucherArray.get(counter).getDial() + "", 
                voucherArray.get(counter).getVoucherNumber() + "" }));

      }

      if (campaignVoucherId == 1)
      {
        if (counter == 0)
        {

          csvList.add(Arrays.asList(new String[]
                { "Voucher Number" }));
        }
        //                  voucherArray.get(counter).getVoucherStatus()
        csvList.add(Arrays.asList(new String[]
              { voucherArray.get(counter).getVoucherNumber() + "" }));

      }
    }

    DateTime dt = new DateTime();
    DateTimeFormatter fmt = 
      DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);


    
    FacesContext fc = FacesContext.getCurrentInstance();




    String templatePath = jsfUtils.getParamterFromIni("defualtPath");  

  
    //System.out.println("tempPath " + templatePath);
    //System.out.println("File Name Is =" + filename);
    filename = reportName + "-" + filename; // report name

    HttpServletResponse response = 
      (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".csv";


    InputStream csvInput = CsvUtil.formatCsv(csvList, ',');
    try
    {
      FileUtil.write(new File(templatePath + "CampaignVouchersCSV/" + 
                              filename), csvInput);
      File csvFile = 
        new File(templatePath + "CampaignVouchersCSV/", filename);
      HttpServletUtil.downloadFile(response, csvFile, false);
    }
    catch (IOException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      //System.out.println("Download file failed: " + e.getMessage());
       
    }
    fc.responseComplete();


  }

  public static int getLastIndexOf(String aa)
  {


    int ss = aa.lastIndexOf(" ");


    return ss;
  }

  public String ExportToSms()
  {

    String reportName = "";
    int messageLength = 140;

    reportName = " SMS";

    DateTime dt = new DateTime();
    DateTimeFormatter fmt = 
      DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
    String filename = dt.toString(fmt);


    FacesContext fc = FacesContext.getCurrentInstance();

    

      String templatePath = jsfUtils.getParamterFromIni("defualtPath");
    //System.out.println("tempPath " + templatePath);
    //System.out.println("File Name Is =" + filename);
    filename = reportName + "-" + filename; // report name

    HttpServletResponse response = 
      (HttpServletResponse) fc.getExternalContext().getResponse();
    filename = filename + ".txt";


    try
    {
      BufferedWriter tempIn = 
        new BufferedWriter(new FileWriter(templatePath + "SMSTxt/" + 
                                          filename, true));
      smsMessage = new String(smsMessage.getBytes("UTF-8"));
      for (int voucherCounter = 0; voucherCounter < voucherArray.size(); 
           voucherCounter++)
      {
        if (voucherCounter == 0)
        {
          Date batchDate = new Date();
          String day = "" + batchDate.getDate();
          String month = "" + (batchDate.getMonth() + 1);
          String hour = "" + batchDate.getHours();
          String min = "" + batchDate.getMinutes();
          String second = "" + batchDate.getSeconds();
          String year = "" + (batchDate.getYear() + 1900);


          if ((batchDate.getMonth() + 1 < 10))
            month = "0" + month;
          if (batchDate.getDate() < 10)
            day = "0" + day;
          if (batchDate.getHours() < 10)
            hour = "0" + hour;
          if (batchDate.getMinutes() < 10)
            min = "0" + min;
          if (batchDate.getSeconds() < 10)
            second = "0" + second;


          tempIn.write("F:2	D:" + day + month + year + hour + min + 
                       second+"\r\n");
//          tempIn.newLine();
          tempIn.write("<START>");
//          tempIn.newLine();
           tempIn.write("\r\n");
        }

        String[] mainFixSms = null;
        String[] f = null;


        messageLength = 
            messageLength - (voucherArray.get(voucherCounter).getVoucherNumber() + 
                             "").length();

        int msgNumber = (smsMessage.length() / messageLength) + 1;

        if (msgNumber < 0)
        {
          msgNumber = 1;
        }

        int charPerMessage = smsMessage.length() / msgNumber;
        f = new String[msgNumber];
        if (charPerMessage == messageLength) //messageLength =140
        {

        }
        else
        {
          int counter = 1;
          int placeOfSMS = 0;
          for (int splitCounter = 0; splitCounter <= msgNumber - 1; 
               splitCounter++)
          {
            placeOfSMS = (messageLength * counter);
            if (placeOfSMS > smsMessage.length())
            {
              placeOfSMS = smsMessage.length();
            }
            String xx = 
              smsMessage.substring((splitCounter * messageLength), 
                                   placeOfSMS);
            //System.out.println("xx is " + xx);

            f[splitCounter] = xx;

            counter++;
          }
          mainFixSms = new String[msgNumber];
          for (int smsCounter = 0; smsCounter < f.length; smsCounter++)
          {
            if (smsCounter == 0)
            {
              if (msgNumber == 1)
              {
                mainFixSms[smsCounter] = 
                    f[smsCounter].substring(0, placeOfSMS);
              }

              else
              {
                mainFixSms[smsCounter] = 
                    f[smsCounter].substring(0, getLastIndexOf(f[smsCounter]));
              }
            }
            else
            {
              mainFixSms[smsCounter] = 
                  f[smsCounter - 1].substring(getLastIndexOf(f[smsCounter - 
                                                             1]) + 1, 
                                              f[smsCounter - 1].length()) + 
                  f[smsCounter].substring(0, 
                                          getLastIndexOf(f[smsCounter]));
            }

            //System.out.println("mainFixSms index no" + smsCounter + " is" + 
//                               mainFixSms[smsCounter]);


          }

          messageLength = 140;
        }


        for (int countDial = 0; countDial < msgNumber; countDial++)
        {
          String smsVoucher = mainFixSms[countDial];
          tempIn.write("N:20");
          tempIn.write(voucherArray.get(voucherCounter).getDial() + "");
            tempIn.write("	");
          if (smsMessage.contains("##"))
          {
            smsVoucher = 
                smsVoucher.replace("##", (voucherArray.get(voucherCounter).getVoucherNumber() + 
                                          ""));
          }
          else
          {
            msgWarn = "Please insert ##.";
            return null;
          }
          tempIn.write("M:" + smsVoucher);
          tempIn.write("\t");
          
          tempIn.write("T:U	C:8	S:Y");
          
//          tempIn.newLine();
            tempIn.write("\r\n");


        }


        if (voucherCounter == (voucherArray.size() - 1))
        {
          //System.out.println("voucherCounter in the last of En track is " + 
//                             voucherCounter);
          tempIn.write("<END>");
        }

      }
      tempIn.close();
      //      FileUtil.write(new File(templatePath+"SMSTxt\\"+filename), csvInput);

      File csvFile = new File(templatePath + "SMSTxt/", filename);
      InputStream inStream =   FileUtil.readStream(csvFile);
      HttpServletUtil.downloadFile(response, inStream,filename, true);

    }
    catch (IOException e)
    {new com.mobinil.gvms.system.utility.PrintException().printException(e);
      //System.out.println("Download file failed: " + e.getMessage());
       
    }
    fc.responseComplete();
    return null;

  }

  public void sortDataTable(ActionEvent event)
  {
    String sortFieldAttribute = getAttribute(event, "sortField");

    // Get and set sort field and sort order.
    if (sortField != null && sortField.equals(sortFieldAttribute))
    {
      sortAscending = !sortAscending;
    }
    else
    {
      sortField = sortFieldAttribute;
      sortAscending = true;
    }

    // Sort results.
    if (sortField != null)
    {
      Collections.sort(voucherArray, 
                       new DTOComparator(sortField, sortAscending));
    }


  }


  public void backAction()
  {

    try
    {
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Campaign_Bean");
      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("Vouchers_Bean");
      FacesContext.getCurrentInstance().getExternalContext().redirect("/GVMS/faces/Admin/Campaign/Campaign.jsp");
    }
    catch (IOException e)
    {

    }

  }


  public void pageFirst()
  {
    dataTable1.setFirst(0);
    pageIndex = 0;
  }

  public void pagePrevious()
  {
    dataTable1.setFirst(dataTable1.getFirst() - dataTable1.getRows());
    pageIndex--;
  }

  public void pageNext()
  {
    dataTable1.setFirst(dataTable1.getFirst() + dataTable1.getRows());
    pageIndex++;
    //System.out.println("get rows " + dataTable1.getRows());
    //System.out.println("get row count " + dataTable1.getRowCount());
  }

  public void pageLast()
  {
    pageIndex = dataTable1.getRowCount();
    int count = dataTable1.getRowCount();
    int rows = dataTable1.getRows();
    dataTable1.setFirst(count - ((count % rows != 0)? count % rows: rows));
  }

  public static String getAttribute(ActionEvent event, String name)
  {
    return (String) event.getComponent().getAttributes().get(name);
  }


  public void setVoucherArray(ArrayList<VoucherClass> voucherArray)
  {
    this.voucherArray = voucherArray;
  }

  public ArrayList<VoucherClass> getVoucherArray()
  {
    return voucherArray;
  }

  public void setDataTable1(HtmlDataTable dataTable1)
  {
    this.dataTable1 = dataTable1;
  }

  public HtmlDataTable getDataTable1()
  {
    return dataTable1;
  }

  public void setSortField(String sortField)
  {
    this.sortField = sortField;
  }

  public String getSortField()
  {
    return sortField;
  }

  public void setSortAscending(boolean sortAscending)
  {
    this.sortAscending = sortAscending;
  }

  public boolean isSortAscending()
  {
    return sortAscending;
  }

  public void setSelectedValueRedeemed(String selectedValueRedeemed)
  {
    this.selectedValueRedeemed = selectedValueRedeemed;
  }

  public String getSelectedValueRedeemed()
  {
    return selectedValueRedeemed;
  }

  public void setRenderDialCol(Boolean renderDialCol)
  {
    this.renderDialCol = renderDialCol;
  }

  public Boolean getRenderDialCol()
  {
    return renderDialCol;
  }

  public void setMessageControl(HtmlInputTextarea messageControl)
  {
    this.messageControl = messageControl;
  }

  public HtmlInputTextarea getMessageControl()
  {
    return messageControl;
  }

  public void setSmsMessage(String smsMessage)
  {
    this.smsMessage = smsMessage;
  }

  public String getSmsMessage()
  {
    return smsMessage;
  }

  public void setSelectLang(String selectLang)
  {
    this.selectLang = selectLang;
  }

  public String getSelectLang()
  {
    return selectLang;
  }

  public void setMsgWarn(String msgWarn)
  {
    this.msgWarn = msgWarn;
  }

  public String getMsgWarn()
  {
    return msgWarn;
  }

  public void setCampaignName(String campaignName)
  {
    this.campaignName = campaignName;
  }

  public String getCampaignName()
  {
    return campaignName;
  }
}
