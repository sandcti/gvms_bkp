package com.mobinil.gvms.system.admin.login.loginDAO;

import com.mobinil.gvms.system.admin.login.loginDTO.LoginDTO;
import com.mobinil.gvms.system.utility.*;
import com.mobinil.gvms.system.security.DAO.*;

import java.sql.*;

public class LoginDAO
{
  public LoginDAO()
  {
  }
  
  public static LoginDTO authUser(String username){
    LoginDTO lc = new LoginDTO();
    
    try
      {
       Connection conn = DBConnection.getConnection();
        ResultSet result = 
          ExecuteQueries.executeQuery(conn,"select t1.SYSTEM_USER_NAME AdminUserName,t1.SYSTEM_LOGIN_END_DATE,t1.SYSTEM_USER_PASSWORD,t1.SYSTEM_LOGIN_COUNT,t1.SYSTEM_USER_ID,t1.SYSTEM_USER_TYPE,t2.ADMIN_NAME,IS_LOCKED,Max(t4.PASSWORD_INDEX) SYSTEM_USER_PASS_INDEX from GVMS_SYSTEM_USER t1 left join GVMS_ADMIN t2 on t1.SYSTEM_USER_ID=t2.SYSTEM_USER_ID left join GVMS_PASS_TRACE t4 on t1.SYSTEM_USER_ID=t4.SYSTEM_USER_ID  where t1.SYSTEM_USER_NAME='" + 
                                      username.trim().toLowerCase() + 
                                      "' and t1.USER_STATUS_ID=1 and t1.SYSTEM_USER_TYPE in ('admin','reporter') and rownum=1 group by t1.SYSTEM_USER_NAME,t1.SYSTEM_LOGIN_END_DATE,t1.SYSTEM_USER_PASSWORD,t1.SYSTEM_LOGIN_COUNT,t1.SYSTEM_USER_ID,t1.SYSTEM_USER_TYPE,t2.ADMIN_NAME,IS_LOCKED");
        

        if (result.next())
          {
           
            lc.setEndDate(result.getDate("SYSTEM_LOGIN_END_DATE"));
            lc.setPassword(result.getString("SYSTEM_USER_PASSWORD"));
            lc.setLoginCount(result.getInt("SYSTEM_LOGIN_COUNT"));
            lc.setPassIndex(result.getInt("SYSTEM_USER_PASS_INDEX"));
            lc.setSysUserId(result.getInt("SYSTEM_USER_ID"));
            lc.setUserLocked(result.getString("IS_LOCKED"));
            lc.setFullName(result.getString("ADMIN_NAME"));
            lc.setUserName(result.getString("AdminUserName"));
            lc.setUserType(result.getString("SYSTEM_USER_TYPE"));                 
            lc.setErrorMessage("");
            result.getStatement().close();
            result.close();  
          

          }
        else
          {
            result.getStatement().close();
            result.close();  
            lc.setErrorMessage("Invalid User Name."); 
          }
        DBConnection.closeConnections ( conn );
        return lc;
         
      }
      catch (Exception sqle)
      {new com.mobinil.gvms.system.utility.PrintException().printException(sqle);
        lc.setErrorMessage("Invalid Login due to connection.");
        return lc;
      }
  
  }
  public static boolean setNewPassword(Connection argCon, int argUserID, String argNewPassword)
    {
      try 
      {
        Statement stmtPasswordChange = argCon.createStatement();
    //    String strPasswordChangeQuery = "update GEN_USER"+
      //                                  " set PASSWORD = '"+argNewPassword+"'"+
        //                                " where USER_ID = "+argUserID;
        String strPasswordChangeQuery = "update VW_GEN_USER"+
                                        " set USER_PASSWORD = '"+argNewPassword+"'"+
                                        " where USER_ID = "+argUserID;
        if(stmtPasswordChange.executeUpdate(strPasswordChangeQuery) >0)
        {
          return true;
        }
      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }
      return false;
    }

    public static void setIncrementLock(Connection argCon, String argUserID)
    {
      try 
      {
        Statement stmt = argCon.createStatement();
    //    String strPasswordChangeQuery = "update GEN_USER"+
      //                                  " set PASSWORD = '"+argNewPassword+"'"+
        //                                " where USER_ID = "+argUserID;
        String strIncLock = "update GVMS_SYSTEM_USER set IS_LOCKED = IS_LOCKED+1 where SYSTEM_USER_ID = "+argUserID;
  //      //System.out.println("increment lock query is "+strIncLock);
        stmt.executeUpdate(strIncLock) ;
        stmt.close();

      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }

    }
  public static void setIncrementLogin(Connection argCon, String argUserID)
  {
    try 
    {
      Statement stmt = argCon.createStatement();
  //    String strPasswordChangeQuery = "update GEN_USER"+
    //                                  " set PASSWORD = '"+argNewPassword+"'"+
      //                                " where USER_ID = "+argUserID;
      String strIncLogin = "update GVMS_SYSTEM_USER set SYSTEM_LOGIN_COUNT = SYSTEM_LOGIN_COUNT+1 where SYSTEM_USER_ID = "+argUserID;
  //      //System.out.println("increment lock query is "+strIncLock);
      stmt.executeUpdate(strIncLogin) ;
      stmt.close();

    } 
    catch (Exception ex) 
    {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);    }

  }

    public static void setDefaultLock(Connection argCon, String argUserID)
    {
      try 
      {
        Statement stmt = argCon.createStatement();
    //    String strPasswordChangeQuery = "update GEN_USER"+
      //                                  " set PASSWORD = '"+argNewPassword+"'"+
        //                                " where USER_ID = "+argUserID;
        String strDefLock = "update GVMS_SYSTEM_USER set IS_LOCKED = '0' where SYSTEM_USER_ID = "+argUserID;
  //      //System.out.println("Default lock query is "+strDefLock);
        stmt.executeUpdate(strDefLock) ;

      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }

    }
  /**
     * setNewPassword method:
     * 
     * Sets a new password for this user and returns a boolean indecating 
     * if this change was successful or not.
     * 
     * @param     Connection argCon, String argUserEmail, String argNewPassword
     * @return boolean
     * @throws  
     * @see    
    */    

    public static void updateUserLastPass(String userId,String argEncreptedPass)
    {
      try 
      {
         Connection conn = DBConnection.getConnection();
      //  String strPasswordChangeQuery = "update (select * from GEN_PERSON,GEN_USER where USER_ID=PERSON_ID) t1 set PASSWORD = '"+argNewPassword+"'  where PERSON_EMAIL='"+argUserEmail+"'";
        int lastPass = securityDAO.getProps("LAST_PASSWORD_COUNT").intValue();
              int updateUser = 0;
              ResultSet chckLst6Pswrd = 
                ExecuteQueries.executeQuery(conn,"select Max(PASSWORD_INDEX) passCount from GVMS_PASS_TRACE where SYSTEM_USER_ID ="+userId);
  //            //System.out.println("select Max(PASSWORD_INDEX) passCount from GVMS_PASS_TRACE where SYSTEM_USER_ID ="+userId);

              if (chckLst6Pswrd.next())
                {
                if (chckLst6Pswrd.getInt("passCount")!=0){
                  if (chckLst6Pswrd.getInt("passCount") > 0 && 
                      chckLst6Pswrd.getInt("passCount") < (lastPass + 1))
                    {
                      if (chckLst6Pswrd.getInt("passCount") == lastPass)
                        {
                          
                          int updatePassTrace = 
                        	  ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values ("+userId+
                                                           ",'" + argEncreptedPass + "'," + 
                                                           (chckLst6Pswrd.getInt("passCount") + 1) + 
                                                           ")");

  //                            //System.out.println(updatePassTrace);



                          updatePassTrace = 
                        	  ExecuteQueries.executeNoneQuery("update GVMS_PASS_TRACE set PASSWORD_INDEX= PASSWORD_INDEX-1 where SYSTEM_USER_ID="+userId);
  //                            //System.out.println(updatePassTrace);


                          int deletePasstrace = 
                        	  ExecuteQueries.executeNoneQuery("delete from GVMS_PASS_TRACE where SYSTEM_USER_ID ="+userId+
                                                           " and PASSWORD_INDEX=0");
  ////System.out.println(deletePasstrace);
                        }
                      else
                        {                        
                            int updatePassTrace = 
                            	ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values (" + 
                                                             userId+",'" + argEncreptedPass + "'," + 
                                                             (chckLst6Pswrd.getInt("passCount") + 
                                                              1) + ")");
  //                                                            //System.out.println(updatePassTrace);
                            

                          }
                        }
                }
                else
                    {
                     int updatePassTrace = 
                    	 ExecuteQueries.executeNoneQuery("insert into GVMS_PASS_TRACE(SYSTEM_USER_ID,PASSWORD,PASSWORD_INDEX) Values ("+userId+
                                                           ",'" + argEncreptedPass + "'," + 
                                                           1 + 
                                                           ")");
  //                                                         //System.out.println(updatePassTrace);
                    }
                    }
              DBConnection.cleanResultSet(chckLst6Pswrd);
              DBConnection.closeConnections ( conn );
                    
                
      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }

    }

   public static String getActivationCode(Connection argCon)
    {
        String ActivationCode="";
      try 
      {

      Statement stmtActivation = argCon.createStatement();
      String SQL = "select dbms_random.String('x',15) str from dual";

        
        ResultSet rstActive = stmtActivation.executeQuery(SQL);
        while(rstActive.next())
        {
          ActivationCode = rstActive.getString("str");
        }
        rstActive.close();
        stmtActivation.close();
      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }
      return ActivationCode;
    }

   /**
     * setNewPassword method:
     * 
     * Sets a new password for this user and returns a boolean indecating 
     * if this change was successful or not.
     * 
     * @param     Connection argCon, String argUserEmail, String argNewPassword
     * @return boolean
     * @throws  
     * @see    
    */    

    public static boolean setNewPassword(String argUserId, String argUserCode)
    {
      try 
      {
    	  
        String strPasswordChangeQuery = "update GVMS_SYSTEM_USER set IS_LOCKED='0',SYSTEM_USER_PASSWORD = '"+argUserCode+"',SYSTEM_LOGIN_END_DATE=sysdate+(select properties from GVMS_PROPERTIES where REASONE='DAYS_FOR_EXPIRED_LOGIN') where SYSTEM_USER_ID = "+argUserId;

                                        
        if(ExecuteQueries.executeNoneQuery(strPasswordChangeQuery) >0)
        {
          return true;
        }
      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }
      return false;
    }
    
    /**
     * setNewPassword method:
     * 
     * Sets a new password for this user and returns a boolean indecating 
     * if this change was successful or not.
     * 
     * @param     Connection argCon, String argUserEmail, String argNewPassword
     * @return boolean
     * @throws  
     * @see    
    */    

    public static void setActivationCode(Connection argCon, String argUserId, String argNewPassword)
    {
      try 
      {
        Statement stmtPasswordChange = argCon.createStatement();
      //  String strPasswordChangeQuery = "update (select * from GEN_PERSON,GEN_USER where USER_ID=PERSON_ID) t1 set PASSWORD = '"+argNewPassword+"'  where PERSON_EMAIL='"+argUserEmail+"'";
        String strPasswordChangeQuery = "update VW_GEN_USER"+
                                        " set USER_ACTIV_CODE = '"+argNewPassword+"', USER_EXPIRE_CODE_DATE=sysdate+1"+
                                        " where USER_LOGIN = '"+argUserId+"'";

  //                                      //System.out.println("Query to update activation code "+strPasswordChangeQuery);
        stmtPasswordChange.executeUpdate(strPasswordChangeQuery);

      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }

    }

  /**
     * getPrivilegeID method:
     * 
     * Gets a privilege id as an int depending on the privilege 
     * action name given as a parameter. 
     * If this privilege action name does not exist it returns 0.
     * 
     * @param     Connection argCon, String argPrivilegeActionName
     * @return int
     * @throws  
     * @see    
    */    

    public static String getActivationCode(Connection argCon, String argUserId)
    {
      String privilegeID = null;
      try 
      {
        Statement stmtAction = argCon.createStatement();
        String strActionQuery = "select USER_ACTIV_CODE "+
                              " from VW_GEN_USER"+
                              " where USER_ID = '"+argUserId+"'";
  //                            //System.out.println("query for activation code is "+strActionQuery);

        ResultSet rstAction = stmtAction.executeQuery(strActionQuery);
        while(rstAction.next())
        {
          privilegeID = rstAction.getString(1);
        }
        rstAction.close();
        stmtAction.close();
      } 
      catch (Exception ex) 
      {
new com.mobinil.gvms.system.utility.PrintException().printException(ex);      }
      return privilegeID;
    }
  
}
