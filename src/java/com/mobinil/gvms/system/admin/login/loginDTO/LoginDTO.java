package com.mobinil.gvms.system.admin.login.loginDTO;

import java.util.Date;

public class LoginDTO
{
  public Date endDate;
  public String password;
  public String userName;
  public String fullName;
  public String sysUserType;
  public Integer userTypeId;
  public Integer userId;
  public Integer loginCount;
  public Integer sysUserId;
  public String userLocked;
  public Integer passIndex;
  public String userType;
  public String errorMessage;
  
  public LoginDTO()
  {
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }

  public Date getEndDate()
  {
    return endDate;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getPassword()
  {
    return password;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

  public void setFullName(String fullName)
  {
    this.fullName = fullName;
  }

  public String getFullName()
  {
    return fullName;
  }

  public void setSysUserType(String sysUserType)
  {
    this.sysUserType = sysUserType;
  }

  public String getSysUserType()
  {
    return sysUserType;
  }

  public void setUserTypeId(Integer userTypeId)
  {
    this.userTypeId = userTypeId;
  }

  public Integer getUserTypeId()
  {
    return userTypeId;
  }

  public void setUserId(Integer userId)
  {
    this.userId = userId;
  }

  public Integer getUserId()
  {
    return userId;
  }

  public void setLoginCount(Integer loginCount)
  {
    this.loginCount = loginCount;
  }

  public Integer getLoginCount()
  {
    return loginCount;
  }

  public void setSysUserId(Integer sysUserId)
  {
    this.sysUserId = sysUserId;
  }

  public Integer getSysUserId()
  {
    return sysUserId;
  }

  public void setUserLocked(String userLocked)
  {
    this.userLocked = userLocked;
  }

  public String getUserLocked()
  {
    return userLocked;
  }

  public void setPassIndex(Integer passIndex)
  {
    this.passIndex = passIndex;
  }

  public Integer getPassIndex()
  {
    return passIndex;
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public String getUserType()
  {
    return userType;
  }

  public void setErrorMessage(String errorMessage)
  {
    this.errorMessage = errorMessage;
  }

  public String getErrorMessage()
  {
    return errorMessage;
  }
}
