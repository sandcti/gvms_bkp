package com.mobinil.gvms.system.application;

import com.mobinil.gvms.system.admin.campaign.Engine.RunnableClass;
import com.mobinil.gvms.system.utility.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;

import oracle.sql.TIMESTAMP;

public class appBean extends Thread
{
 
  public void run()
  {

    Thread t = Thread.currentThread();
    String name = t.getName();

    FileLogger.logString("entered loop() - " + name);
    //System.out.println("entered loop() - " + name);

    for (int i = 0; i < (i + 1); i++)
      {
        try
          {
            //        Thread.sleep(10800000);
            Thread.sleep(1000*60*15);
// Thread.sleep(9000);
          }
        catch (InterruptedException x)
          {
            new com.mobinil.gvms.system.utility.PrintException().printException(x);
          }


        
        try
          {
           Connection conn = DBConnection.getConnectionForThread ( );
           ResultSet campaigns = 
              ExecuteQueries.executeQuery(conn,"select CAMPAIGN_ID,floor((CAMPAIGN_START_DATE-sysdate)) campaignStatus from GVMS_CAMPAIGN where CAMPAIGN_STATUS_ID in (1)");
        	 Statement st = conn.createStatement ( );       	
            while (campaigns.next())
              {

               Integer campaignId = campaigns.getInt("CAMPAIGN_ID");
               Integer daysLeft = campaigns.getInt ( "campaignStatus" );
               
               if (daysLeft<0){
               
                      st.executeUpdate ("update GVMS_CAMPAIGN set CAMPAIGN_STATUS_ID=2 where CAMPAIGN_ID=" + 
                                                      campaignId);
               }     
                    
                  
                  

              }
            st.close ( );
            campaigns.close();            
            DBConnection.closeThreadConnections  ( conn );
          }
        catch (SQLException e)
          {
new com.mobinil.gvms.system.utility.PrintException().printException(e);
          }

        FileLogger.logString("leave loop() - " + name);


      }
  }
//  public static void main(String[] args) {
//	  appBean ap = new appBean();
//	  ap.start();
//  }


}
