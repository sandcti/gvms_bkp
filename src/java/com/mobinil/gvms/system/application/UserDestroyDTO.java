package com.mobinil.gvms.system.application;

public class UserDestroyDTO {
public String sessionId;
public Boolean destroy;
public void setDestroy(Boolean destroy) {
	this.destroy = destroy;
}
public Boolean getDestroy() {
	return destroy;
}
public void setSessionId(String sessionId) {
	this.sessionId = sessionId;
}
public String getSessionId() {
	return sessionId;
}
}
