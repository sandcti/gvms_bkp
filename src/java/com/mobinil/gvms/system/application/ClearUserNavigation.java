package com.mobinil.gvms.system.application;

import java.util.ArrayList;
import java.util.Date;

public class ClearUserNavigation extends Thread
{
	public void run()
	  {
		while(true)
		{
			try
	          {
	            //        Thread.sleep(10800000);
	            Thread.sleep(900000);
	// Thread.sleep(9000);
	          }
	        catch (InterruptedException x)
	          {
                    new com.mobinil.gvms.system.utility.PrintException().printException(x);
	          }
	        if (new Date().getHours()==5)
	        {
	        	ArrayList<UserDestroyDTO> users = GVMSAppContextListener.getUserNavigation();
	        	users.clear();
	        	GVMSAppContextListener.setUserNavigation(users);
	        }
		}
	  }
}
