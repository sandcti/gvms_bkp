package com.mobinil.gvms.system.application;

import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class GVMSAppContextListener implements ServletContextListener
{
	public static ArrayList<UserDestroyDTO> userNavigation = new ArrayList<UserDestroyDTO>();
  appBean ab = new appBean();
  ClearUserNavigation cuv= new ClearUserNavigation();
  public void contextInitialized(ServletContextEvent event)
  {

    //System.out.println("here in startUp");    
    ab.start();
    cuv.start();

  }


  public void contextDestroyed(ServletContextEvent event)
  {
    //System.out.println("here in shutdown");
//    ab.destroy();
  }


public static void setUserNavigation(ArrayList<UserDestroyDTO> userNavigation) {
	GVMSAppContextListener.userNavigation = userNavigation;
}


public static ArrayList<UserDestroyDTO> getUserNavigation() {
	return userNavigation;
}

}
